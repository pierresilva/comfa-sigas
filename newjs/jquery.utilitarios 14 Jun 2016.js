/***
 * valida el documento antes de permitir crear persona
 * 
 * @param tipodoc
 * @param doc
 */
function validarIdentificacionInsert(cmbtd,txtd){
	var regexp =/[A-Za-z��s]/; 	var regexp1 = /\W/g;  var regexpsolotexto = /\W/g;
	var error=0;
	var td=cmbtd.val();  var doc=txtd.val();
	
	if(td==1 || td==2 || td==5){
		if((doc.slice(0,1)==0)||(doc.search(regexp) >= 0)||(doc.search(regexp1) >= 0)){ error++; }
	} else {
		if(doc.search(regexpsolotexto) >= 0){ error++; }
	}
	if(error>0){ txtd.val(''); }
	else { validarLongNumIdent(cmbtd.val(), txtd.get(0)); }
}

/***
 * valida Que se haya seleccionado tipo de identificacion
 * 
 * @param tipodoc
 * @param doc
 */
function validarIdentificacion(tipodoc, doc){
	if(tipodoc.val()=='0'){
		tipodoc.addClass("ui-state-error");
		doc.value='';
	} else {
		tipodoc.removeClass("ui-state-error");		
	}	
}

/**
 * Valida que se haya escrito
 * 
 * @param object 		Objeto a validar
 * @param conError 		Contador error actual
 * @returns 			Si hay error suma uno
 */
function validarTexto(object, conError){
	if(object.val().length==0){
		object.addClass("ui-state-error");
		conError++;
	} else { object.removeClass("ui-state-error"); }
	
	return conError;
}

/**
 * Valida que se haya Seleccionado una opcion valida
 * 
 * @param object 		Objeto a validar
 * @param conError 		Contador error actual
 * @returns 			Si hay error suma uno
 */

function validarSelect(object, conError){
	if(object.val()=='0'){
		object.addClass("ui-state-error");
		conError++;
	} else { object.removeClass("ui-state-error"); }
	
	return conError;
}

/**
 * Valida que se haya Seleccionado una opcion
 * 
 * @param object 		Objeto a validar
 * @param conError 		Contador error actual
 * @returns 			Si hay error suma uno
 */

function validarChecked(objectValidar, object, conError){
	if(objectValidar.val()==null){
		object.parent().addClass("ui-state-error");
		conError++;
	} else { object.parent().removeClass("ui-state-error"); }	
	return conError;
}

/**
 * Valida que se haya escrito
 * 
 * @param object 		Objeto a validar
 * @param conError 		Contador error actual
 * @returns 			Si hay error suma uno
 */

function validarTextArea(object, conError){
	if($.trim(object.val()).length==0){
		object.val('');
		object.addClass("ui-state-error");
		conError++;
	} else { object.removeClass("ui-state-error"); }
	
	return conError;
}

/**
 * Valida que el texto introducido sea numerico
 * 
 * @param object 		Objeto a validar
 * @param conError 		Contador error actual
 * @returns 			Si hay error suma uno
 */
function validarNumero(object, conError){
	var conE=validarTexto(object,conError);
	
	if(conE==conError){
		if(isNaN(object.val())){
			object.val('');
			object.addClass("ui-state-error");
			conError++;
		} else { object.removeClass("ui-state-error"); }
	} else
		conError=conE;
	
	return conError;
}

/**
 * Valida que el numero pasado sea numerico
 * 
 * @param valor 		Numero a validar
 * @returns {Boolean} 	retorna falso si no lo es.
 */
function esNumeroRespuesta(valor){	
	if(isNaN(valor)){
		return false;
	} else if(parseInt(valor)>0){
		return true
	} else {
		return false;
	}
}



/**
 * Activa datapicker del Objeto pasado
 * 
 * @param objeto
 */
function actDatepicker(objeto){
	var anoActual = new Date();
	var strYearRange = anoActual.getFullYear()-120 +":"+ anoActual.getFullYear();
	
	objeto.datepicker({
		changeMonth: true,
		changeYear: true,
		maxDate: "+0D"
	});
	
	objeto.datepicker( "option", "yearRange", strYearRange );
}

/**
 * Activa datapicker del Objeto pasado para Periodo
 * 
 * @param objeto
 */
function actDatepickerPeriodo(objeto){
	objeto.datepicker({
		dateFormat: 'yymm',
		changeMonth: true,
		changeYear: true,
		showButtonPanel: true,
		onClose: function(dateText, inst) {
			var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
			var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
			$(this).datepicker('setDate', new Date(year, month, 1));
		} 
	});
	objeto.focus(function () {
		$(".ui-datepicker-calendar").hide();
		$("#ui-datepicker-div").position({
			my: "center top",
			at: "center bottom",
			of: $(this)
		});
	}); 
}

/**
 * Funcion que verifica si una variable esta sin definir
 * 
 *  @param variable 	elemento a validar
 *  @returns 			retorna la variable despues de validar
 */
function esUndefined(variable){
	if(variable==undefined)
		variable='';
	return variable;
}

/**
 * Dialogo para la creacion de persona con datos b�sicos 
 * Se debe incluir la clase js Persona
 * 
 * @param cmbtd 	Select del tipo de documento
 * @param txtd 		Text del documento
 */
function newPersonaSimple(cmbtd,txtd){
	var tipodoc=cmbtd.val();
	var doc =txtd.val();
	
	$("#dialog-persona").dialog({
		height: 228,
		width: 650,
		draggable:false,
		modal: true,
		open: function(){			
			actDatepicker($("#fechaNaceDialogPersona"));
			$('#dialog-persona select,#dialog-persona input[type=text]').val('');
			$("#tDocumento").val(tipodoc);
			$("#txtNumeroP").val(doc);
			buscarPersonaExistente($('#txtNumeroP').val(),$('#spNombre').val(),$('#ssNombre').val(),$('#spApellido').val(),$('#ssApellido').val(),false,0);
		},
		buttons: {
			'Guardar datos': function() {
				var error=0;
				var nPersona=new Persona();
				nPersona=camposPersona(nPersona);
				
				nPersona.idtipodocumento=$("#tDocumento").val();
				nPersona.identificacion=$.trim($("#txtNumeroP").val()); 
				nPersona.papellido=$.trim($("#spApellido").val());
				nPersona.sapellido=$.trim($("#ssApellido").val()); 
				nPersona.pnombre=$.trim($("#spNombre").val());
				nPersona.snombre=$.trim($("#ssNombre").val());
				nPersona.nombrecorto=nombreCorto(nPersona.pnombre, nPersona.snombre, nPersona.papellido, nPersona.sapellido); 
				nPersona.fechanacimiento= $("#fechaNaceDialogPersona").val(); 
				nPersona.sexo=$("#tSexo").val();
				nPersona.idpais=48; 	 
				nPersona.iddepnace=41;
				nPersona.idciunace=41001;
				
				$("table.tablero :input.ui-state-error,select.ui-state-error").removeClass("ui-state-error");
				
				if((nPersona.identificacion).length==0){
					$("#txtNumeroP").addClass("ui-state-error");
					error++;
				}
				if((nPersona.pnombre).length==0){
					$("#spNombre").addClass("ui-state-error");
					error++;
				}
				if((nPersona.papellido).length==0){
					$("#spApellido").addClass("ui-state-error");
					error++;
				}
				if($("#fechaNaceDialogPersona").val()!=0){
					var regexpFechas = /^((\d{4}))[\/|\-](|(0[1-9])|(1[0-2]))[\/|\-]((0[1-9])|(1\d)|(2\d)|(3[0-1]))$/;
					if(!(nPersona.fechanacimiento).match(regexpFechas)){
						$("#fechaNaceDialogPersona").addClass("ui-state-error");
						error++;
					}
				}
				if($("#fechaNaceDialogPersona").val()==0){
					$("#fechaNaceDialogPersona").addClass("ui-state-error");
					error++;
				}
				if((nPersona.sexo)==0){
					$("#tSexo").addClass("ui-state-error");
					error++;
				}
				
				if(error>0){
					alert("Llene los campos obligatorios!");
					return false;
				} 
				str = JSON.stringify(nPersona);
				var URLs=src();
				$.ajax({
					url: URLs + '/phpComunes/guardarPersonaSimple.php',
					type: "POST",
					data: {v0:str},
					async:false,
					success: function(datos){
						var e = parseInt(datos);
						if(e>0){							
							copyTipoDoc=nPersona.idtipodocumento;
							copyDoc=nPersona.identificacion;
							$('#dialog-persona select,#dialog-persona input[type=text]').val('');
							alert("La persona fue creada satisfactoriamente. (ID."+ datos +")");							
							$("#dialog-persona").dialog('close');		
							cmbtd.val(copyTipoDoc);
							txtd.val(copyDoc);					
						}else{
							alert("NO se guardo el registro!");
							txtd.val('');
						}
					}
				});
			}
		},
		close: function() {
			$('#dialog-persona select,#dialog-persona input[type=text]').val('');
			txtd.focus();
			$(this).dialog("destroy");
		}
	});
}

/**
 * Dialogo para la Actualizacion de persona 
 *  
 * @param tipodoc 	Select del tipo de documento
 * @param doc 		Text del documento
 */
function newActualizaPersona(tipodoc,doc){
	
	$("#dialog-actualiza-persona").dialog({
		height: 256,
		width: 650,
		draggable:false,
		modal: true,
		closeOnEscape: false,
		buttons: {
			'Actualizar datos': function() {	
				var error=0;
				var tipodoc = $("#cmbIdTipoDocumento").val();
				var doc = $("#txtIdentificacion").val();					
				
				//AUDITORIA				
				var campos = "";				
				$.ajax({
					url: rutaPHP + 'buscarPersonaAct.php',
					cache: false,
					type: "POST",
					data: {v0:tipodoc,v1:doc},	
					dataType:"json",
					success: function(data){			
												
						$("table.tablero :input.ui-state-error,select.ui-state-error").removeClass("ui-state-error");
						
						if(($("#txtTelefono").val()).length<7){
							if(($("#txtTelefono").val()).length!=''){
							$("#txtTelefono").addClass("ui-state-error");	
							error++;
							alert("Tel�fono - m�nimo 7 digitos");
							}
						}
						if(($("#txtCelular").val()).length<10){
							if(($("#txtCelular").val()).length!=''){
							$("#txtCelular").addClass("ui-state-error");	
							error++;
							alert("Celular - m�nimo 10 digitos");
							}
						}
						if(($("#cboDepto").val())==0){
							$("#cboDepto").addClass("ui-state-error");
							error++;
						}
						if(($("#cboCiudad").val())==0){
							$("#cboCiudad").addClass("ui-state-error");
							error++;
						}
						if(($("#cboZona").val())==0){
							$("#cboZona").addClass("ui-state-error");
							error++;
						}
						if(($("#txtDireccion").val())==0){
							$("#txtDireccion").addClass("ui-state-error");
							error++;
						}
						
						if(error>0){
							alert("Llene los campos obligatorios!");
							return false;
						} 
						campos +='Campos(';
							
							if($.trim($("#txtDireccion").val())!=data.direccion)
							{
							  campos += " Direccion ";
							}
							if($.trim($("#txtTelefono").val())!=data.telefono) 
							{							 
							  campos += " Telefono ";
							}
							if($.trim($("#txtCelular").val())!=data.celular) 
							{
							  campos += " Celular ";
							}
							if($.trim($("#txtEmail").val())!=data.email) 
							{
							  campos += " Email ";
							}
							if($("#cboDepto").val()!=data.iddepresidencia) 
							{							
							 	campos += " Departamento ";
							}
							if($("#cboCiudad").val()!=data.idciuresidencia) 
							{
								campos += " Ciudad ";
							}
							if($("#cboZona").val()!=$.trim(data.idzona)) 
							{
								campos += " Zona ";
							}
							if($("#cboBarrio").val()!=data.idbarrio) 
							{
								campos += " Barrio ";
							}
							campos +=')';
						var idpersona=$("#idPersona").val();
						var observacion=campos;
						var URLs=src();
						 $.ajax({
							   url: URLs + '/aportes/radicacionnew/php/guardarAuditaPersona.php',
							   type:"POST",
							   data:{v0:idpersona,v1:observacion, v2:1},
							   success:function(data){
								   var e = parseInt(data);
								   if(e>0){
									   //alert("Auditado");
									   ActualizarInformacionPersona();
								   }else{
									   alert("Error Auditoria");
								   }
							   }
					     });//ajax
					}
				});	
				// FIN AUDITORIA
				
			}	
		},
	    /*open: function() {
	          //Hide closing "X" for this dialog only.
	          $(this).parent().children().children("a.ui-dialog-titlebar-close").remove();
	    },*/
		close: function() {
			$('#dialog-actualiza-persona select,#dialog-actualiza-persona input[type=text]').val('');
			$(this).dialog("destroy");
		}
	});
}


function  ActualizarInformacionPersona()
{
	var error=0;
	var nPersona=new Persona();			
	nPersona=camposPersona(nPersona);				
	nPersona.idpersona=$("#idPersona").val();	
	
	//Actualiza en la tabla aportes015
	nPersona.direccion=$.trim($("#txtDireccion").val());
	nPersona.idbarrio=$("#cboBarrio").val();
	nPersona.telefono=$.trim($("#txtTelefono").val());
	nPersona.celular=$.trim($("#txtCelular").val());
	nPersona.email=$.trim($("#txtEmail").val());
	nPersona.idciuresidencia=$("#cboCiudad").val();
	nPersona.iddepresidencia=$("#cboDepto").val();
	nPersona.idzona=$.trim($("#cboZona").val());
	
	str = JSON.stringify(nPersona);
	var URLs=src();
	$.ajax({
		url: URLs + '/phpComunes/actualizarPersonaSimple.php',
		type: "POST",
		data: {v0:str},
		async:false,
		success: function(datos){
			var e = parseInt(datos);
			if(e>0){							
				copyTipoDoc=nPersona.idtipodocumento;
				copyDoc=nPersona.identificacion;
				$('#dialog-actualiza-persona select,#dialog-actualiza-persona input[type=text]').val('');
				alert("La persona fue Actualizada.");							
				$("#dialog-actualiza-persona").dialog('close');		
				tipodoc.val(copyTipoDoc);
				doc.val(copyDoc);				
			}else{
				alert("NO se guardo el registro!");
				tipodoc.val('');
			}
		}
	});	
}

/**
 * Busca en la base de datos personas con igual identificacion, o nombre completo
 * 
 * @param num 			Numero Identificacion
 * @param pnom 			Primer Nombre
 * @param snom 			Segundo Nombre
 * @param pape 			Primer Apellido
 * @param sape 			Segundo Apellido
 * @param blurNombre 	Es evento Blur
 * @param existe 		La persona Existe
 * @returns {Boolean}
 */
function buscarPersonaExistente(num,pnom,snom,pape,sape,blurNombre,existe){
	if ( ( existe == 1 || parseInt(existe) > 0 ) || ( blurNombre == true && ( $.trim(pnom).length == 0 || $.trim(pape).length == 0 ) ) ) return false;
	var URLs=src();
	$.ajax({
		url: URLs + 'phpComunes/buscarPersonaExistente.php',
		async: false,
		data:{v0:num,v1:pnom,v2:snom,v3:pape,v4:sape},
		dataType:"json",
		beforeSend: function(objeto){
        	dialogLoading('show');
        },        
        complete: function(objeto, exito){
        	dialogLoading('close');
			if(exito != "success"){
                alert("No se completo el proceso!");
            }            
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("En buscarPersonaExistente Paso lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
        success:function(respuesta){
        	if(respuesta){
				$("#tbDocumentoDoble").empty();
				var cont=0;
				$.each(respuesta,function(i,fila){
					cont++;
					if ( fila.identificacion == num && blurNombre == true ) { cont--; } 
					nom = fila.pnombre + " " +fila.snombre +" "+ fila.papellido + " " + fila.sapellido;
					$("#tbDocumentoDoble").append(
							"<tr>" +
								"<td>" + fila.codigo + "</td>" +
								"<td style='text-align:right'>" + fila.identificacion + "</td>" +
								"<td>" + fila.idpersona + " - " + nom + "</td>" +									
							"</tr>");
				});
				if ( cont > 0 ) {
					alert("Existen personas con datos similares,\nCompruebe si es una de ellas");
					$("#divDocumentoDoble").dialog("open");
				}
				return false;
			}
        }
	});
}

/**
 * A partir de los nombres crea el nombre corto
 * 
 * @param pn 	Primer Nombre
 * @param sn 	Segundo Nombre
 * @param pa 	Primer Apellido
 * @param sa 	Segundo Apellido
 * @returns
 */
function nombreCorto(pn,sn,pa,sa){
	var patron=new RegExp('[\u00F1|\u00D1]');
	for(i=1;i<=pn.length;i++){
		pn=pn.replace(patron,"&");
	}
	for(i=1;i<=sn.length;i++){
		sn=sn.replace(patron,"&");
	}
	for(i=1;i<=pa.length;i++){
		pa=pa.replace(patron,"&");
	}
	for(i=1;i<=sa.length;i++){
		sa=sa.replace(patron,"&");
	}
	txt=pn+" "+sn+" "+pa+" "+sa;
	txt=txt.toUpperCase();
	num=parseInt(txt.length);
	if(num>26){
		if(sn.length>0){
			sn=sn.slice(0,1);
			txt=pn+" "+sn+" "+pa+" "+sa;
			txt=txt.toUpperCase();
			num=parseInt(txt.length);
		}
		if(num>26)
		if(sa.length>0){
			sa=sa.slice(0,1);
			txt=pn+" "+sn+" "+pa+" "+sa;
			txt=txt.toUpperCase();
			num=parseInt(txt.length);
			}
		}
	if(num>26){
		txt=txt.substring(0, 26);
	}
	$("#tncorto").val(txt);
	return txt;
}

/**
 * Funcion que limpia los campos y asigna los combos a sus posiciones predeterminadas
 */
function limpiarCampos(index){
	for (var i=0;i<document.forms[0].elements.length;i++){	
		if(i>=index){
			if (document.forms[0].elements[i].type=="text")
				 document.forms[0].elements[i].value="";
			else if (document.forms[0].elements[i].type=="select-one")
				 document.forms[0].elements[i].value="0";
			else if (document.forms[0].elements[i].type=="textarea")
				document.forms[0].elements[i].value="";		
			else if (document.forms[0].elements[i].type=="hidden")				
				document.forms[0].elements[i].value="";
			else if (document.forms[0].elements[i].type=="checkbox")
				document.forms[0].elements[i].checked=false;
			else if (document.forms[0].elements[i].type=="radio")
				document.forms[0].elements[i].checked=false;			
		}		
	}
	
	jQuery('td[id^=td]').html('');
	jQuery('label[id^=lbl]').html('');	
}