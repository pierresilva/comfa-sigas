/**
 * Funcion-clase que contiene todos los campos existentes en la base de datos de la tabla de los Documentos traidos(aportes029) 
 */
function Documento(){
	var iddetalle028;
	var idregistro;
	var iddocumento;
	var cantidad;
	var nombrearchivo;
	var programa;
	var digitalizado;
	var fechasistema;
	var usuario;
}

/**
 * Funcion que inicializa todos los campos del Documento 
 */
function camposDocumento(objeto){ var obj = new Documento();
	obj.iddetalle028 = esUndefined(objeto.iddetalle028);
	obj.idregistro = esUndefined(objeto.idregistro); 
	obj.iddocumento = esUndefined(objeto.iddocumento);
	obj.cantidad = esUndefined(objeto.cantidad);
	obj.nombrearchivo = esUndefined(objeto.nombrearchivo); 
	obj.programa = esUndefined(objeto.programa); 
	obj.digitalizado = esUndefined(objeto.digitalizado); 
	obj.fechasistema = esUndefined(objeto.fechasistema); 
	obj.usuario = esUndefined(objeto.usuario);
	
	return obj;
}