/**
 * Funcion-clase que contiene todos los campos existentes en la base de datos de la tabla de Tarjeta(aportes101) 
 */
function Tarjeta(){
	var idtarjeta;
	var bono;
	var idpersona;
	var precodigo;
	var codigobarra;
	var programa;
	var tipo;
	var fechaexpedicion;
	var fechasolicitud;
	var fecharecepcion;
	var fechaentrega;
	var fechavence;
	var entregada;
	var idtipoentrega;
	var estado;
	var ubicacion;
	var idetapa;
	var usuarioentrega;
	var flag;
	var tempo1;
	var tempo2;
	var saldo;
	var usuario;
	var fechasistema;
	var fechaetapa;
	var asesor;
	var fechaasesor;
	var ultimomovimiento;
	var control;
	var barraok;
	var impreso;
	var fechasaldo;
	var inventario;
	var fechainventario;
	var usuarioinventario;
}

/**
 * Funcion que inicializa todos los campos de la Tarjeta 
 */
function camposTarjeta(objeto){ var obj = new Tarjeta();
	obj.idtarjeta = esUndefined( objeto.idtarjeta );
	obj.bono = esUndefined( objeto.bono );
	obj.idpersona = esUndefined( objeto.idpersona );
	obj.precodigo = esUndefined( objeto.precodigo );
	obj.codigobarra = esUndefined( objeto.codigobarra );
	obj.programa = esUndefined( objeto.programa );
	obj.tipo = esUndefined( objeto.tipo );
	obj.fechaexpedicion = esUndefined( objeto.fechaexpedicion );
	obj.fechasolicitud = esUndefined( objeto.fechasolicitud );
	obj.fecharecepcion = esUndefined( objeto.fecharecepcion );
	obj.fechaentrega = esUndefined( objeto.fechaentrega );
	obj.fechavence = esUndefined( objeto.fechavence );
	obj.entregada = esUndefined( objeto.entregada );
	obj.idtipoentrega = esUndefined( objeto.idtipoentrega );
	obj.estado = esUndefined( objeto.estado );
	obj.ubicacion = esUndefined( objeto.ubicacion );
	obj.idetapa = esUndefined( objeto.idetapa );
	obj.usuarioentrega = esUndefined( objeto.usuarioentrega );
	obj.flag = esUndefined( objeto.flag );
	obj.tempo1 = esUndefined( objeto.tempo1 );
	obj.tempo2 = esUndefined( objeto.tempo2 );
	obj.saldo = esUndefined( objeto.saldo );
	obj.usuario = esUndefined( objeto.usuario );
	obj.fechasistema = esUndefined( objeto.fechasistema );
	obj.fechaetapa = esUndefined( objeto.fechaetapa );
	obj.asesor = esUndefined( objeto.asesor );
	obj.fechaasesor = esUndefined( objeto.fechaasesor );
	obj.ultimomovimiento = esUndefined( objeto.ultimomovimiento );
	obj.control = esUndefined( objeto.control );
	obj.barraok = esUndefined( objeto.barraok );
	obj.impreso = esUndefined( objeto.impreso );
	obj.fechasaldo = esUndefined( objeto.fechasaldo );
	obj.inventario = esUndefined( objeto.inventario );
	obj.fechainventario = esUndefined( objeto.fechainventario );
	obj.usuarioinventario = esUndefined( objeto.usuarioinventario );
	
	return obj;
}