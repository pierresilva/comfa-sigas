/**
 * Funcion-clase que contiene todos los campos existentes en la base de datos de la tabla de reclamo trabajador(aportes055) 
 */
function ReclamoTrabajador(){
	var idreclamo;
	var idtrabajador;
	var fechareclamo;
	var periodoinicial;
	var periodofinal;
	var numerocuotas;
	var estado;
	var idcausal;
	var fechagiro;
	var periodogiro;
	var usuarioprocesa;
	var notas;
	var usuario;
	var tempo1;
	var pqr;
}

/**
 * Funcion que inicializa todos los campos del reclamo 
 */
function camposReclamoTrabajador(objeto){ var obj = new ReclamoTrabajador();
	obj.idreclamo = esUndefined( objeto.idreclamo );
	obj.idtrabajador = esUndefined( objeto.idtrabajador );
	obj.fechareclamo = esUndefined( objeto.fechareclamo );
	obj.periodoinicial = esUndefined( objeto.periodoinicial );
	obj.periodofinal = esUndefined( objeto.periodofinal );
	obj.numerocuotas = esUndefined( objeto.numerocuotas );
	obj.estado = esUndefined( objeto.estado );
	obj.idcausal = esUndefined( objeto.idcausal );
	obj.fechagiro = esUndefined( objeto.fechagiro );
	obj.periodogiro = esUndefined( objeto.periodogiro );
	obj.usuarioprocesa = esUndefined( objeto.usuarioprocesa );
	obj.notas = esUndefined( objeto.notas );
	obj.usuario = esUndefined( objeto.usuario );
	obj.tempo1 = esUndefined( objeto.tempo1 );
	obj.pqr = esUndefined( objeto.pqr );
	
	return obj;
}