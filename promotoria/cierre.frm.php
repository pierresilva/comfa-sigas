<?php	
	set_time_limit(0);
	ini_set("display_errors",'1');
	date_default_timezone_set("America/Bogota");
	$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
	include_once  $root;
	include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';
	
	$url=$_SERVER['PHP_SELF'];
	include_once $raiz.DIRECTORY_SEPARATOR.'phpComunes'.DIRECTORY_SEPARATOR.'auditoria.php';
	auditar($url);
	
	//Obtener la fecha de la version
	$fecver = date('Ymd h:i:s A',filectime('cierre.frm.php'));
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<title>::Promotoria | Cierre::</title>
		
		<link type="text/css" href="<?php echo URL_PORTAL; ?>newcss/Estilos.css" rel="stylesheet">
		<link type="text/css" href="<?php echo URL_PORTAL; ?>newcss/css.1.8/jquery-ui-1.8.17.custom.css" rel="stylesheet">
		<link type="text/css" href="<?php echo URL_PORTAL; ?>newcss/marco.css" rel="stylesheet">
		
		<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/1.6/jquery-1.6.2.min.js"></script>
		<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/1.6/jquery-ui-1.8.16.custom.min.js"></script>
		<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/comunes.js"></script>
		<script type="text/javascript" src="js/cierre.js"></script>
		
	</head>
	<body>
		<table width="40%" border="0" align="center" cellpadding="0" cellspacing="0">
			<tbody>
			
			  	<!-- ESTILOS SUPERIOR TABLA-->
			  	<tr>
				    <td width="13" height="29" class="arriba_iz">&nbsp;</td>
				    <td class="arriba_ce">
				    	<span class="letrablanca">::&nbsp;Cierre Promotoria&nbsp;::</span>
				    	<div style="text-align:right;float:right;">
								<?php echo 'Versi&oacute;n: ' . $fecver; ?>
						</div>
				    </td>
				    <td width="13" class="arriba_de" align="right">&nbsp;</td>
			  	</tr>
		  	
		  	<!-- ESTILOS ICONOS TABLA-->
			  	<tr>
			    	<td class="cuerpo_iz">&nbsp;</td>
			    	<td class="cuerpo_ce">&nbsp;</td>
			    	<td class="cuerpo_de">&nbsp;</td>
			  	</tr>
			  	
		  	<!-- ESTILOS MEDIO TABLA-->
			  	<tr>
			    	<td class="cuerpo_iz">&nbsp;</td>
			    	<td class="cuerpo_ce"></td>
			    	<td class="cuerpo_de">&nbsp;</td>
			  	</tr>  
			  	
		  	<!-- CONTENIDO TABLA-->
			  	<tr>
			   		<td class="cuerpo_iz">&nbsp;</td>
			   		<td class="cuerpo_ce">
						<table cellspacing="0" width="90%" border="0" class="tablero" align="center">
			      			<tbody>
			      				<tr>
			      					<td colspan="2" style="text-align: center;"><b>ULTIMO CIERRE</b></td>
			      				</tr>
			      				<tr>
			      					<td colspan="2">
			      						<table width="100%" border="0" class="tablero" align="center" id="tblUltimoCierre">
			      							<thead>
			      								<tr>
				      								<th>ID</th>
				      								<th>PERIODO</th>
				      								<th>FECHA CIERRE</th>
				      								<th>ESTADO CIERRE</th>
				      								<th>USUARIO</th>
				      							</tr>
			      							</thead>
			      							<tbody>
			      								<tr>
			      									<td></td>
			      									<td></td>
			      									<td></td>
			      									<td></td>
			      									<td></td>
			      								</tr>
			      							</tbody>
			      						</table>
			      					</td>
			      				</tr>
			      				<tr><td colspan="2"></td></tr>
				    			<tr>
				      				<td >Periodo Cierre</td>
				      				<td >
				      					<input type="text" name="txtPeriodoCierre" id="txtPeriodoCierre" class="boxfecha element-required" readonly="true"/>
				      					<span class="clsObligatorio">&nbsp;&nbsp;&nbsp;</span>
				      					
				      					<input type="hidden" name="hidIdCierre" id="hidIdCierre"/>
				      					<input type="hidden" name="hidUsuario" id="hidUsuario" value="<?php echo $_SESSION["USUARIO"];?>"/>
				      				</td>
				        		</tr>
				        		<tr>
				      				<td >Fecha Cierre</td>
				      				<td >
				      					<input type="text" name="txtFechaCierre" id="txtFechaCierre" class="boxfecha element-required"/>
				      					<span class="clsObligatorio">&nbsp;&nbsp;&nbsp;</span>
				      				</td>
				        		</tr>
				        		<tr>
				      				<td >Estado Cierre</td>
				      				<td >
				      					<select name="cmbEstadoCierre" id="cmbEstadoCierre" class="boxfecha element-required"></select>
				      					<span class="clsObligatorio">&nbsp;&nbsp;&nbsp;</span>
				      				</td>
				        		</tr>
				        		<tr>
				        			<td colspan="2" style="text-align:center;">
				        				<input type="button" name="btnGuardar" id="btnGuardar" value="Guardar"/>
				        				<input type="button" name="btnCerrarPeriodo" id="btnCerrarPeriodo" value="Cerrar Periodo" style="display:none;"/>
				        				<input type="button" name="btnGenerar" id="btnGenerar" value="Generar Datos" style="display:none;"/>
				        			</td>
				        		</tr>
			      			</tbody>
			      		</table>
					</td>
		    		<td class="cuerpo_de">&nbsp;</td>
		  		</tr>		  
	  		<!-- ESTILOS PIE TABLA-->
		  		<tr>
		    		<td class="abajo_iz" >&nbsp;</td>
		    		<td class="abajo_ce" ></td>
		    		<td class="abajo_de" >&nbsp;</td>
		  		</tr>		  
			</tbody>
		</table>
	</body>
</html>