<?php	
	$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
	include_once  $root;
	
	$url=$_SERVER['PHP_SELF'];
	include_once $raiz.DIRECTORY_SEPARATOR.'phpComunes'.DIRECTORY_SEPARATOR.'auditoria.php';
	auditar($url);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<title>::Promotoria | Asesor visita::</title>
		
		<link href="../css/Estilos.css" rel="stylesheet" type="text/css" />
		<link href="../css/marco.css" rel="stylesheet" type="text/css" />
		<link href="../newcss/css.1.8/jquery-ui-1.8.17.custom.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="../newjs/1.6/jquery-1.6.2.min.js"></script>
		<script type="text/javascript" src="../newjs/1.6/jquery-ui-1.8.16.custom.min.js"></script>
		<script type="text/javascript" src="../js/comunes.js"></script>
		<script type="text/javascript" src="js/asesorVisita.js"></script>
	</head>
	<body>
		<table width="70%" border="0" align="center" cellpadding="0" cellspacing="0">
			<tbody>
		  	<!-- ESTILOS SUPERIOR TABLA-->
		  	<tr>
			    <td width="13" height="29" class="arriba_iz">&nbsp;</td>
			    <td class="arriba_ce"><span class="letrablanca">::&nbsp;Asesor Visita&nbsp;::</span></td>
			    <td width="13" class="arriba_de" align="right">&nbsp;</td>
		  	</tr> 
		  	<!-- ESTILOS ICONOS TABLA-->
		  	<tr>
		    	<td class="cuerpo_iz">&nbsp;</td>
		    	<td class="cuerpo_ce">
					<img src="../imagenes/menu/nuevo.png" name="iconNuevo" width="16" height="16" id="iconNuevo" style="cursor:pointer" title="Nuevo" onClick="nuevo();" /> 
					<img width="3" height="1" src="../imagenes/spacer.gif" />
					<img width="3" height="1" src="../imagenes/spacer.gif" />
					<img src="../imagenes/menu/grabar.png" name="iconGuardar" width="16"  height=16 id="iconGuardar" style="cursor:pointer" title="Actualizar" onClick="guardar();" />      
		    	</td>
		    	<td class="cuerpo_de">&nbsp;</td>
		  	</tr>
		  	<!-- ESTILOS MEDIO TABLA-->
		  	<tr>
		    	<td class="cuerpo_iz">&nbsp;</td>
		    	<td class="cuerpo_ce"></td>
		    	<td class="cuerpo_de">&nbsp;</td>
		  	</tr>  
		  	<!-- CONTENIDO TABLA-->
		  	<tr>
		   		<td class="cuerpo_iz">&nbsp;</td>
		   		<td class="cuerpo_ce">
					<table cellspacing="0" width="90%" border="0" class="tablero" align="center">
		      			<tbody>
		      				<tr>
			      				<td >No Visita:</td>
								<td >
									<input type="text" name="txtNoVisita" id="txtNoVisita" class="box1"/>
									<span class="clsObligatorio">&nbsp;&nbsp;&nbsp;</span>
								</td>
			        		</tr>
			    			<tr>
			      				<td width="150">Nit:</td>
								<td >
									<label id="lblNit">&nbsp;</label>
								</td>
			        		</tr>
			        		<tr>
			      				<td >Raz&oacute;n Social:</td>
								<td >
									<label id="lblRazonSocial">&nbsp;</label>
								</td>
			        		</tr>
			        		<tr>
			      				<td colspan="2" id="tdMensaje">&nbsp;</td>
			        		</tr>
			        		<tr>
			      				<td >Asesor Juridico Interno:</td>
								<td >
									<select name="cmbAsesorJuridicoI" id="cmbAsesorJuridicoI" class="box1">
										<option value="0">Seleccione</option>
									</select>
								</td>
			        		</tr>
			        		<tr>
			      				<td >Asesor Juridico Externo:</td>
								<td >
									<select name="cmbAsesorJuridicoE" id="cmbAsesorJuridicoE" class="box1">
										<option value="0">Seleccione</option>
									</select>
								</td>
			        		</tr>
		      			</tbody>
		      		</table>
					</td>
		    		<td class="cuerpo_de">&nbsp;</td>
		  		</tr>		  
		  		<!-- ESTILOS PIE TABLA-->
		  		<tr>
		    		<td class="abajo_iz" >&nbsp;</td>
		    		<td class="abajo_ce" ></td>
		    		<td class="abajo_de" >&nbsp;</td>
		  		</tr>		  
			</tbody>
		</table>
	</body>
</html>