<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once 'clases' . DIRECTORY_SEPARATOR . 'promotoria.class.php';

$objDatosFiltro = $_REQUEST['objDatosFiltro'];
$objPromotoria = new Promotoria();


//$consulta = $objPromotoria->convenio_empresa($idEmpresa);
$consulta = $objPromotoria->buscar_fecha_convenio($objDatosFiltro);
$data = array("Fecha"=>null,"Periodo"=>null);
$conFecha = 0;
$conPeriodo = 0;
$arrIdDetalleConvenio = array();

//Obtener los datos de la fecha del convenio
foreach( $consulta as $row ){
  $data["Fecha"][] = $row;
  $arrIdDetalleConvenio[] =  $row["iddetalleconvenio"];
  $conFecha++;
}

if($conFecha>0){
	//$idDetalleConvenio = trim($idDetalleConvenio,",");
	
	//Eliminar los id duplicados
	$arrIdDetalleConvenio = array_unique($arrIdDetalleConvenio);
	foreach($arrIdDetalleConvenio as $idDetalleConvenio){
		$consulta = $objPromotoria->buscar_periodo_convenio(array("iddetalleconvenio"=>$idDetalleConvenio));
		
		//Obtener los periodos del convenio
		foreach( $consulta as $row ){
			$data["Periodo"][] = $row;
			$conPeriodo++;
		}
	}
}

echo ($conFecha>0 && $conPeriodo>0) ? json_encode($data) : 0;
?>