var URL=src();
$(document).ready(function(){
	
//	Dialog Colaboracion en linea
	$("#dialog-form2").dialog({
		autoOpen: false,
		height: 400,
		width: 500,
		draggable:false,
		modal: true,
		buttons: {
			'Enviar': function() {
				var bValid = true;
				var campo=$('#notas').val();
				var campo0=$.trim(campo);
				if (campo0==""){
					$(this).dialog('close');
					return false;
				}
				var campo1=$('#usuario').val();
				var campo2="empresaSinConv.php";
				$.post(URL+'phpComunes/colaboracion.php',{v0:campo0,v1:campo1,v2:campo2},
						function(datos){
					if(datos=='1'){
						alert("Su comentario fue enviado correctamente!. Gracias por participar en el mejoramiento de SIGAS");
					}else{
						alert(datos);
					}
				});
				$(this).dialog('close');
			},
			Cancelar: function() {
				$(this).dialog('close');
				$("#dialog-form2").dialog("close");
			}
		}
	});
	//Dialog ayuda
	$("#ayuda").dialog({
		autoOpen: false,
		height: 450,
		width: 700,
		modal:false,
		open: function(evt, ui){
			$('#ayuda').html('');
			/*$.get(URL +'help/promotoria/xxx.html',function(data){
							$('#ayuda').html(data);
					});*/
		}
	});
	
	//Dialog Paz y salvo
	$("#dlgPaz").dialog({
		autoOpen: false,
		width: 700,
		modal:true,
		open:function(){
			$("#observacion").focus();
		},
		/*close:function(){
			idvisita=$("#idVisitaSeg").html();
			$("#fila"+idvisita).find(":checkbox").attr("checked",false);
		},*/
		buttons:{
			'Actualizar':function(){

				$("#observacion").removeClass( "ui-state-error" );
				if($("#observacion").val()==''){
					$("#observacion").addClass( "ui-state-error" ).focus();
					return false;
				}
				if($("#observacion").val().length<20){
					alert("Ingrese minimo 20 caracteres.");
					$("#observacion").focus();
					return false;
				}
				//Guardar seguimiento
				if(confirm("Desea dejar a paz y salvo la empresa "+$(".datosE:eq(1)").html()+" con No de visita "+$("#idVisitaSeg").html()+" ?")){
					$.ajax({
						type:'POST',
						url:URL+'promotoria/actualizarEstadoPaz.php',
						data:{v0:$("#idVisitaSeg").html(), v1:$("#observacion").val()},
						success:function(data){
							if(data==0){
								alert("No se pudo actualizar el estado.");
								return false;
							}else if( data == 1 ) {
								$("#dlgEmpresa").dialog('close');
								alert("Registro actualizado satisfactoriamente.");
								location.reload();
							}
						}//succes
					});//ajaxagregar */
				}//
			}//confirm
		}//buttons		 
	});//dialog

	$.ajax({
		url:'cargarEmprActivas.php',
		type:"POST",
		//data:{},
		dataType:"json",
		async:false,
		success:function(data){
			if(data==0){
				MENSAJE("NO existen empresas que deban aportes parafiscales.");
				return false;
			}
			
			var no=data.length;
			$.each(data,function(i,n){ 
				if(n.estado=='A'){
					var estado="ACTIVA";
				}
				
				$("#tablaPaz caption span").html(no+" registros encontrados."); 
				$("#tablaPaz tbody").append("<tr id='fila"+n.idvisita+"'>"+
						"<td>"+n.idvisita+"</td>"+
						//"<td>"+n.idliquidacion+"</td>"+
						"<td name='"+n.idempresa+"'>"+n.nit+"</td>"+
						"<td>"+n.razonsocial+"</td>"+
						"<td>"+estado+"</td>"+
						"<td>"+n.causal+"</td>"+
						"<td style='text-align:center;'>"+n.liquidacion+"</td>"+
						"<td style='text-align:center;'>"+n.agencia+"</td>"+
						"<td style='text-align:center'><img src='../imagenes/menu/modificar.png' onclick='abrir(\""+n.idvisita+"\")' style='cursor:pointer;'/></td></tr>"); 
			});//end each emrpesa Id
		},//succes
		complete:function(){
			if( $( "#tablaPaz tbody" ).html().trim() != 0 ){
				//Actualizar Tabla de sorter 
				$("#tablaPaz tbody tr:odd").addClass("vzebra-odd");
				$("#tablaPaz").tablesorter({headers: {5: { sorter: false}}});
				$("#tablaPaz").tablesorterPager({container: $("#pager"),seperator:" de "});
				$("#tablaPaz").trigger("update");
			}
		}						
	});//ajax 

});//fin ready

//----------------------------------------VENTANA DE DIALOGO DE AYUDA
function mostrarAyuda(){
	$("#ayuda").dialog('open');
}
//----------------------------------------VENTANA DE DIALOGO DE MANUAL
function notas(){
	$("#dialog-form2").dialog('open');
}
//-------------------------------------VENTANA DIALOG PARA CAMBIAR EL ESTADO DE LA EMPRESA
function abrir( numero ){
   
  //Limpio los datos
  $("#tablaPazDatos td.datosE,#idVisitaSeg").html('');
  $("#idempresa").val('');
 
 //Coloco los datos de la fila en la tabla del dialog
  idempresa=$("#fila"+numero).children("td:eq(2)").attr("name");
  
  noVisita=$("#fila"+numero).children("td:eq(0)").text();
  nit=$("#fila"+numero).children("td:eq(2)").html();
  rz=$("#fila"+numero).children("td:eq(3)").html();
  causal=$("#fila"+numero).children("td:eq(4)").html();
  
  //Cargo los datos en la celda
  var arr=[nit,rz,causal];
  $("#tablaPazDatos td.datosE").each(function(i){
	  $(this).html(arr[i]);
  });//each tabla
  
  //Colocar id visita
 $("#idVisitaSeg").html(noVisita);
 //Coloco el idempresa
 $("#idempresa").val(idempresa);
 
 //Abro el dialog
 $("#dlgPaz").dialog('open');
  
}
