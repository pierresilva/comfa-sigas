$(function(){
	$("#txtNoVisita").blur(function(){
		nuevo();
		var idVisita = $(this).val().trim();
		if(!idVisita){return false;}
		
		//Buscar los datos de la visita
		var datosVisita = buscarVisita(idVisita);
		if(typeof datosVisita == "object"){
			var htmlDatosSeguimiento = "",
				datosSeguimietos;
			
			datosVisita = datosVisita[0];
			
			//Asignar los datos al formulario
			$("#lblNit").text(datosVisita.nit);
			$("#lblRazonSocial").text(datosVisita.razonsocial);
			
			//Buscar historial seguimiento
			datosSeguimietos = buscarSeguimiento(idVisita);
			if(typeof datosSeguimietos == "object"){
				$.each(datosSeguimietos,function(i,row){
					htmlDatosSeguimiento += "<tr><td>"+(i+1)+"</td>" +
							"<td>"+row.observacion+"</td>" +
							"<td>"+row.usuario+"</td>" +
							"<td>"+row.fechasistema+"</td></tr>";
				});
			}
			//Adicionar los seguimientos al formulario
			$("#tbSeguimiento").html(htmlDatosSeguimiento);
		}else{
			//No existe la visita
			$("#tdMensaje").html("<b style='color:red;'>La visita no existe o esta a paz y salvo</b>");
		}
	});
});

/**
 * Obtener los datos de la visita
 * @param idVisita
 * @returns
 */
function buscarVisita(idVisita){
	var resultado = null;
	$.ajax({
		url:"buscarVisita.php",
		type:"POST",
		data:{objDatosFiltro:{idvisita:idVisita,estado:"A"}},
		dataType:"json",
		async:false,
		success:function(datos){
			resultado=datos;
		}
	});
	return resultado;
}

/**
 * Obtener los datos de la visita
 * @param idVisita
 * @returns
 */
function buscarSeguimiento(idVisita){
	var resultado = null;
	$.ajax({
		url:"buscarSeguimiento.php",
		type:"POST",
		data:{idvisita:idVisita},
		dataType:"json",
		async:false,
		success:function(datos){
			resultado=datos;
		}
	});
	return resultado;
}

function guardar(){
	var idVisita = $("#txtNoVisita").val().trim();
	var observacion = $("#txaObservacion").val().trim();
	
	//Remover class error de los campos
	$(".ui-state-error").removeClass(".ui-state-error");
	
	if(!idVisita){$("#txtNoVisita").addClass("ui-state-error");return false;}
	if(!observacion){$("#txaObservacion").addClass("ui-state-error");return false;}
	
	$.ajax({
		url:"guardarSeguimiento.php",
		type:"POST",
		data:{datos:{idvisita:idVisita,observacion:observacion}},
		dataType:"json",
		async:false,
		success:function(datos){
			if(datos==0){
				alert("Error al guardar el seguimiento");
				return;
			}
			alert("EL seguimiento se guardo correctamente");
			$("#txtNoVisita").val("");
			nuevo();
		}
	});
}

function nuevo(){
	$("#lblNit,#lblRazonSocial").text("");
	$("#tbSeguimiento,#tdMensaje").html("");
	$("#txaObservacion").val("");
}