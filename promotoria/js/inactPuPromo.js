$(function(){
	//crear el datepicker pera el primer periodo
	datepickerC("PERIODO","#txtPeriodo");

	$("#btnGuardar").button().click(guardar);
	
	$("#txtNit").blur(function(){
		$("#tdMensajeError,#tBody,#txtPeriodo,#lblRazonSocial").html('');
		//$("#tdPeriodo,#tabAportes").css('display','none');
		var nit = $("#txtNit").val().trim();
		if(nit=="")
			return false;
		
		//Obtener los datos de la empresa
		var objDatosEmpresa = buscarDatosEmpresa(nit);
		if(typeof objDatosEmpresa == "object"){
			objDatosEmpresa = objDatosEmpresa[0];
			$("#lblRazonSocial").html(objDatosEmpresa.razonsocial);
			$("#hidIdEmpresa").val(objDatosEmpresa.idempresa);
		}else{
			$("#lblRazonSocial").html("La empresa no existe!!");
		}
		
	});	
	
	$("#btnBuscarAportes")
		.button()
		.click(function(){
			$("#tdMensajeError").html('');
			var periodo   = $("#txtPeriodo").val();
			var idEmpresa = $("#hidIdEmpresa").val(); 
			
			if(periodo==0 || idEmpresa==0)
				return  false;
			
			var objDatosAporte = buscarAporte(idEmpresa,periodo);
			if(typeof objDatosAporte == "object" && objDatosAporte.length>0){
				
				var comboInactivar = "<select name='cmbInactivar' class='box1'><option value=''>Seleccione</option><option value='I' optionI>Inactivo</option><option value='A' optionA>Activo</option></select>"
				
				var tr = "";
				$.each(objDatosAporte,function(i,row){
					var combo = comboInactivar;
					if(row.estado_promotoria=="A"){
						combo = combo.replace("optionA","selected");
						combo = combo.replace("optionI","");
					}else if(row.estado_promotoria=="I"){
						combo = combo.replace("optionI","selected");
						combo = combo.replace("optionA","");
					}else{
						combo = combo.replace("optionI","");
						combo = combo.replace("optionA","");
					}
					
					tr += "<tr id='trAporte"+row.idaporte+"'>"+
							"<td>"+row.numerorecibo+"</td>"+
							"<td>"+row.periodo+"</td>"+
							"<td style='text-align:right;'>"+formatCurrency(row.valornomina)+"</td>"+
							"<td style='text-align:right;'>"+formatCurrency(row.valoraporte)+"</td>"+
							"<td>"+row.fechapago+"</td>"+
							"<td>"+combo+"</td></tr>";
				});
				
				$("#tBody").html(tr);
				
			}else{
				$("#tdMensajeError").html("No existen aportes!!");
			}
		});
});

function buscarDatosEmpresa(nit){
	var datosRetorno = "";	
	$.ajax({
		url:URL+'phpComunes/buscarEmpresaCompleta.php',
		type:"GET",
		data:{v0:nit},
		dataType:"json",
		async:false,
		success:function(datos){
			datosRetorno = datos;
		}
	});
	return datosRetorno;
}

function buscarAporte(idEmpresa,periodo){
	var datosRetorno = "";	
	$.ajax({
		url:'buscarAporte.php',
		type:"POST",
		data:{id_empresa:idEmpresa,periodo:periodo},
		dataType:"json",
		async:false,
		success:function(datos){
			datosRetorno = datos;
		}
	});
	return datosRetorno;
}

function fetchDataElemeFormu(){
	var objDatos = [];
	
	$("#tBody tr").each(function(i,row){
		var idTr = $(this).attr("id");
		var idAporte = idTr.replace("trAporte","");
		var estado = $("#"+idTr+" select[name='cmbInactivar']").val();
		
		if(estado!=0){
			objDatos[objDatos.length] = {
					idaporte:idAporte,
					estado_promotoria:estado
				};
		}
	});
	
	return objDatos;
}

function guardar(){
	var banderaError = $("#tBody select[value!='']").length;
	if(banderaError==0){
		alert("Debe seleccionar el estado para el aporte!!");
		return false;
	}
	
	var objDatos = fetchDataElemeFormu();
	
	$.ajax({
		url:'actuaInactPuPromo.php',
		type:"POST",
		data:{aporte:objDatos},
		dataType:"json",
		async:false,
		success:function(datos){
			if(datos==0){
				alert("No fue posible actualizar los aportes!!");
			}else{
				alert("Actualizacion correctamente!!");
				nuevo();
			}
		}
	});
}

function nuevo(){
	$("#tdMensajeError,#tBody,#lblRazonSocial").html("");
	$("#hidIdEmpresa,#txtNit,#txtPeriodo").val("");
}
	
	