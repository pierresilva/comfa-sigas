 var urlGuardAjustPerioLiqui = "guardAjustPerioLiqui.php"
	 , urlBuscarDetalLiqui = "buscarDetalLiqui.php"
     , urlCausalAjuste = "buscarCausaAjust.php"
     , urlVerifPermiCierr = "verifPermiCierr.php";
 
 
$(function(){
	
	$("#btnNuevo").click(nuevo);
	$("#btnGuardar").click(guardar);
	$("#btnBuscar").button().click(ctrBuscar);
	$("#txtFechaAjuste").change(ctrFechaAjuste);
	
	ctrCausalAjuste();
	datepickerC("FECHA","#txtFechaAjuste");
});


function fetchDetalleLiquidacion(objFiltro){
	var resultado = null;
	
	$.ajax({
		url: urlBuscarDetalLiqui,
		type:"POST",
		data: {objDatosFiltro:objFiltro},
		dataType:"json",
		async: false,
		success: function(data){
			resultado = data;
		}
	});
	
	return resultado;
}

function fetchCausalAjuste(objFiltro){
	var resultado = null;
	
	$.ajax({
		url: urlCausalAjuste,
		type:"POST",
		data: {objDatosFiltro:objFiltro},
		dataType:"json",
		async: false,
		success: function(data){
			resultado = data;
		}
	});
	
	return resultado;
}

function fetchPermisoCierre(objFiltro){
	var resultado = null;
	
	$.ajax({
		url: urlVerifPermiCierr,
		type:"POST",
		data: {objDatosFiltro:objFiltro},
		dataType:"json",
		async: false,
		success: function(data){
			resultado = data;
		}
	});
	
	return resultado;
}

/**
 * METODO: Adiciona evento blur al campo periodo
 * */
function addEventBlurPeriod(idInputPeriodo){
	
	//Obtener el valor del aporte para el periodo
	$("#"+idInputPeriodo).blur(function(){
		
		var idTrPeriodo = $(this).parent().parent().attr("id");
		
		if($(this).val()==0)return false;
		
		var idInputPeriodoActual = "#"+$(this).attr("id");
		var periodo = $(this).val();
		var banderaError = 0;
		//Verificar que el periodo no este duplicado
		
		//Verificar con los periodos que estan en el formulario
		$("#tblPeriodo tbody tr").each(function(i,row){
			var thisTr = $(this);
			var idTr = "#"+thisTr.attr("id");
		    var idInputPeriodo = "#txtPeriodoFinal"+(idTr.replace("#trIdPeriodo",""));
		    
		    //Verifica si el periodo esta duplicado
			if( idInputPeriodo != idInputPeriodoActual && periodo==$(idInputPeriodo).val() ){
				banderaError++;
			}
		});
		
		if(banderaError>0){
			alert("El periodo "+periodo+"  Ya existe en el formulario!!");
			$(this).val("");
			return false;
		}
	});
}

function ctrBuscar(){
	var idLiquidacion = $("#txtNumeroLiquidacion").val().trim();
	
	if(!idLiquidacion) return false; 
	
	var objFiltro = {idliquidacion:idLiquidacion};
	var dataPeriodo = fetchDetalleLiquidacion(objFiltro);
	
	var datosVisita = "";
	var razonSocial = "";
	var htmlPeriodo = "";
	var idVisita = "";
	var indiceLiquidacion = 0;
	var fechaVisita = "";
	
	$("#tblPeriodo tbody").html("");
	if(dataPeriodo && typeof dataPeriodo == "object" && dataPeriodo.length>0){
		
		datosVisita = dataPeriodo[0].fechavisita + " <b>#" + dataPeriodo[0].idvisita+"</b>";
		razonSocial = dataPeriodo[0].razonsocial;
		idVisita = dataPeriodo[0].idvisita;
		indiceLiquidacion = dataPeriodo[0].indicador;
		fechaVisita = dataPeriodo[0].fechavisita;
			
		$.each(dataPeriodo,function(i,row){
			var idTrPeriodo = "trIdPeriodo"+row.iddetalle;
			var idInputPeriodo = "txtPeriodoFinal"+row.iddetalle;
			
			htmlPeriodo = '<tr id="'+idTrPeriodo+'">'
	     				+'<td>'
	     				+	'<input type="text" name="'+idInputPeriodo+'" id="'+idInputPeriodo+'" value="'+row.periodo+'" class="boxfecha" readonly="true"/>'
	     				+	'<input type="hidden" name="hidPeriodoInicial" id="hidPeriodoInicial" value="'+row.periodo+'"/>'
						+	'<input type="hidden" name="hidIdPeriodo" id="hidIdPeriodo" value="'+row.iddetalle+'"/></td>'
			 			+'<td>'
			 			+	'<input type="text" name="txtAporteFinal" id="txtAporteFinal" class="boxfecha clsValidaText" value="'+row.aportes+'" style="text-align:right;"/>'
			 			+	'<input type="hidden" name="hidAporteInicial" id="hidAporteInicial" value="'+row.aportes+'"/>'
			 			+'</td>'
			 			+'<td>'
			 			+	'<input type="text" name="txtBaseFinal" id="txtBaseFinal" class="boxfecha clsValidaText" value="'+row.bruto+'" disabled="disabled" style="text-align:right;"/>'
			 			+	'<input type="hidden" name="hidBaseInicial" id="hidBaseInicial" value="'+row.bruto+'"/>'
			 			+'</td>'
			 			+'<td>'
			 			+	'<input type="text" name="txtTotalFinal" id="txtTotalFinal" class="boxfecha clsValidaText" value="'+row.neto+'" disabled="disabled" style="text-align:right;"/>'
			 			+	'<input type="hidden" name="hidTotalInicial" id="hidTotalInicial" value="'+row.neto+'"/>'
			 			+'</td></tr>';
			
			$("#tblPeriodo tbody").append(htmlPeriodo);
			datepickerC("PERIODO","#tblPeriodo tbody #"+idTrPeriodo+" #"+idInputPeriodo);
			
			//Adicionar el evento para calcular la base y el total
			addEventChangeElemePeriod(idTrPeriodo);
			//Adicionar evento para validar el periodo
			addEventBlurPeriod(idInputPeriodo);
		});
		
	}else{
		razonSocial = "<b>NO EXISTEN DATOS!!</b>";
	}
	
	$("#tdDatosVisita").html(datosVisita);
	$("#tdRazonSocial").html(razonSocial);
	$("#hidIdVisita").val(idVisita);
	$("#hidIndicadorLiquidacion").val(indiceLiquidacion);
	$("#hidFechaVisita").val(fechaVisita);
	
}

function ctrCausalAjuste(){
	var objFiltro = {estado:'A'};
	var dataCausal = fetchCausalAjuste(objFiltro);
	
	var htmlCausal = '<option value="">--SELECCIONES--</option>';
	
	if(dataCausal && typeof dataCausal == "object" && dataCausal.length>0){
		$.each(dataCausal,function(i,row){
			htmlCausal += '<option value="'+row.id_causal_ajuste+'">'+row.nombre+'</option>';
		});
	}
	
	$("#cmbCausalAjuste").html(htmlCausal);
	
}

function ctrFechaAjuste(){
	var fechaAjuste = $("#txtFechaAjuste").val();
	
	if(!fechaAjuste) return false;
	
	var fechaVisita = $("#hidFechaVisita").val();
	
	if(new Date(fechaAjuste)< new Date(fechaVisita)){
		alert("La fecha de ajuste ("+fechaAjuste+") es inferior a la fecha de la visita ("+fechaVisita+")");
		$("#txtFechaAjuste").val("");
	}
}
/**
 * METODO: Calcula el aporte
 * @param idTrPeriodo Id tr de los datos del aplicativo
 */
function calcularAporte(idTrPeriodo){

	var indice = parseFloat($("#hidIndicadorLiquidacion").val()),
	aporte = parseFloat($("#"+idTrPeriodo+" #txtAporteFinal").val());
	
	indice = isNaN(indice)?0:indice;
	aporte = isNaN(aporte)?0:aporte; 
	
	var baseLiquidacion = ( aporte / indice ) * 100;
	
	baseLiquidacion = Math.round(baseLiquidacion);
	
	$("#"+idTrPeriodo+" #txtBaseFinal").val( (baseLiquidacion) );
	$("#"+idTrPeriodo+" #txtTotalFinal").val( (aporte) );
	
}

/**
 * METODO: Adiciona el evento change a los elementos (base liquidacion, aporte, ...) del perido 
 * @param idTrPeriodo Id tr del periodo
 */
function addEventChangeElemePeriod(idTrPeriodo){
	//Evento para el element base liquidacion
	$("#"+idTrPeriodo+" #txtAporteFinal").change(function(){
		
		var idTrPeriodo = $(this).parent().parent().attr("id");
		calcularAporte(idTrPeriodo);
	});
}

function guardar(){
		
	if(validateElementsRequired()>0) return false;
	
	if($("#tblPeriodo tbody tr").length==0){
		alert("Debes cargar los periodos liquidados");
		return false;
	}
	
	var objData = {data_liquidacion:{},data_ajuste:[],data_detalle_liquidacion:[]};
	
	$("#tblPeriodo tbody tr").each(function(i,row){
		var idTr = "#"+$(this).attr("id");
		var idPeriodo = idTr.replace("#trIdPeriodo","");
		
		var periodoInicial = $(idTr + " #hidPeriodoInicial").val().trim();
		var periodoFinal = $(idTr + " input[name^='txtPeriodoFinal']").val().trim();
		
		var aportesInicial = parseInt($(idTr + " #hidAporteInicial").val().trim());
		var aportesFinal = parseInt($(idTr + " #txtAporteFinal").val().trim());
		
		var baseInicial = $(idTr + " #hidBaseInicial").val().trim();
		var baseFinal = $(idTr + " #txtBaseFinal").val().trim();
		
		var netoInicial = $(idTr + " #hidTotalInicial").val().trim();
		var netoFinal = $(idTr + " #txtTotalFinal").val().trim();
		
		if(aportesInicial!=aportesFinal || periodoInicial!=periodoFinal){
			
			var periodoAjuste = "";
			var valorAjuste = 0;
			
			if(aportesInicial!=aportesFinal){
				valorAjuste = aportesFinal-aportesInicial;
			}
			if(periodoInicial!=periodoFinal){
				periodoAjuste = periodoFinal;
			}
			
			//Datos del ajuste
			objData.data_ajuste[objData.data_ajuste.length] = { 
					valor: valorAjuste
					, periodo: periodoAjuste
					, fecha_ajuste: $("#txtFechaAjuste").val().trim()
					, observacion: $("#txaObservacion").val().trim()
					, id_causal: $("#cmbCausalAjuste").val().trim()
					, id_periodo_liquidacion: idPeriodo
			};
			
			//Datos del detalle de la liquidacion
			objData.data_detalle_liquidacion[objData.data_detalle_liquidacion.length] = { 
					bruto: baseFinal
					, aportes: aportesFinal
					, neto: netoFinal
					, periodo: periodoFinal
					, id_detalle: idPeriodo
			};
		}
	});
	//Adicionar los datos de la visita
	objData.data_liquidacion.id_visita = $("#hidIdVisita").val().trim();
	objData.data_liquidacion.id_liquidacion = $("#txtNumeroLiquidacion").val().trim();
	objData.data_liquidacion.fecha_visita = $("#hidFechaVisita").val().trim();
	
	//Verificar si se realizaron ajustes
	if(objData.data_ajuste.length==0){
		alert("Debe realizar el ajuste!!");
		return false;
	}
	
	var objFiltro = {
			usuario: $("#hidUsuario").val().trim() 
			, fecha: $("#txtFechaAjuste").val().trim()
		};
	if(fetchPermisoCierre(objFiltro)==0){
		alert("La fecha del ajuste no corresponde al cierre y el usuario no tiene permisos. \n Por este motivo no se guardo la informacion");
		return false;
	}
	
	var resultado = 0;
	//Hacer la peticion al servidor
	$.ajax({
		url: urlGuardAjustPerioLiqui,
		type:"POST",
		data: {data:objData},
		dataType:"json",
		async: false,
		success: function(data){
			resultado = data;
		}
	});
	
	if(resultado==1){
		nuevo();		
		alert("El ajuste se guardo correctamente!!");
	}else{
		alert("El ajuste no se pudo guardar. Favor reportar al administrador T.I.");
	}
}

function nuevo(){
	limpiarCampos2("input:text, #hidIdVisita, #hidIndicadorLiquidacion, #hidFechaVisita, #tdRazonSocial, #cmbCausalAjuste, #txaObservacion, #tblPeriodo tbody, #tdDatosVisita");
}

