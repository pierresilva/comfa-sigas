/************************
 * NOTA FECHA: 2014-06-17 
 * 	SE DEBE ACTUALIZAR ESTE ARCHIVO POR EL CAMBIO REALIZADO A LOS PERIODOS LIQUIDADOS 
 */



var arrayDatosLiquidacion = new Array();
var totalAlmacenados = 0;
var fechaAlmacenada = new Array(); 
$(function(){
	buscarEmpresasConvenio();
	$("input").numeric();
	
	$("#txtFechaPagar").datepicker({
		showAnim:'slideDown',
		changeMonth:true,
		onSelect: function(textoFecha, objDatepicker){
			$.each(fechaAlmacenada,function(i,row){
				if(textoFecha == row ){
					alert("La fecha de pago " + textoFecha + " ya esta almacenada");
					$("#txtFechaPagar").val("");
				}
			});
		}
	});
	
	$("#txtValorAcuerdo").blur(function(){
		calcularSaldo();
	})
});

/**
 * METODO: Buscar las empresa que tienen liquidaciones con la visita activa
 */
function buscarEmpresasConvenio(){
	var option = "<option value='0'>Seleccione...</option>";
	
	var objDatosLiquidaciones = buscarLiquidacion({estado:"A"});
	if(typeof objDatosLiquidaciones == "object" && objDatosLiquidaciones.length>0){
		//Obtener los option con las empresas que tiene liquidacion
		var arrBandera = [];
		$.each(objDatosLiquidaciones,function(i,row){
			if(arrBandera.indexOf(row.idempresa)<0){
				option += "<option value='" + row.idempresa + "'>" + row.razonsocial + "</option>";
			}
			arrBandera[arrBandera.length] = row.idempresa;
		});
	}
	$("#cmbEmpresa").html(option);
}

function buscarLiquidaciones(){
	var idEmpresa = $("#cmbEmpresa").val();
	limpiarCampo("#tdLiquidaciones, #tblPeriodo tbody");
	if(idEmpresa == 0) return false;
	
	var htmlLiquidacion = "";
	arrayDatosLiquidacion = [];
	
	//Buscar las liquidaciones y detalle de la empresa
	var objDatosDetalLiqui = buscaDetalLiqui({idempresa:idEmpresa});
	if( typeof objDatosDetalLiqui == "object" && objDatosDetalLiqui.length > 0 ){
		
		var datosPeriodo = null;
		var arrBandera = [];
		
		$.each(objDatosDetalLiqui,function(i,row){
			if(row.estadovisita=="A"){
		
				//Buscar los periodos
				/*datosPeriodo = buscarDatosPeriodo(row.idliquidacion,row.iddetalle);
				$.each(datosPeriodo,function(i,rowPeriodo){
					arrDatosPeriodo[arrDatosPeriodo.length] = {idPeriodo:rowPeriodo.idperiodoliquidacion
							, periodo:rowPeriodo.periodo}; 
				});*/
				
				
				//Almacenar los datos del detalle de la liquidacion
				arrayDatosLiquidacion[ arrayDatosLiquidacion.length ] = row;
				
				if( arrBandera.indexOf(row.idliquidacion) < 0 ){
					htmlLiquidacion += "[" + row.idliquidacion + "]<input type='checkbox' name='ckdLiquidacion' " +
							" value='" + row.idliquidacion + "' onclick='seleccionLiquidacion();'/>&nbsp;";
				}
				arrBandera[arrBandera.length] = row.idliquidacion;
			}
		});
		
		$("#tdLiquidaciones").html(htmlLiquidacion);
	}
}

/*function buscarLiquidaciones(){
	var idEmpresa = $("#cmbEmpresa").val();
	limpiarCampo("#tdLiquidaciones");
	if(idEmpresa == 0) return false;
	
	var htmlLiquidacion = "";
	arrayDatosLiquidacion = [];
	
	//Buscar las liquidaciones de la empresa
	var objDatosLiquidacion = buscarLiquidacion({idempresa:idEmpresa,estado:"A"});
	if( typeof objDatosLiquidacion == "object" && objDatosLiquidacion.length > 0 ){
		
		var datosPeriodo = null;
		var arrDatosPeriodo = null;
		
		$.each(objDatosLiquidacion,function(i,row){
			if(row.estado=="A"){
				arrDatosPeriodo = [];
				//Buscar los periodos
				datosPeriodo = buscarDatosPeriodo(row.idliquidacion,row.iddetalle);
				$.each(datosPeriodo,function(i,rowPeriodo){
					arrDatosPeriodo[arrDatosPeriodo.length] = {idPeriodo:rowPeriodo.idperiodoliquidacion
							, periodo:rowPeriodo.periodo}; 
				});
				if( typeof arrayDatosLiquidacion[ "id" + row.idliquidacion ] == "undefined"){	
					htmlLiquidacion += "[" + row.idliquidacion + "]<input type='checkbox' name='ckdLiquidacion' " +
							" value='" + row.idliquidacion + "' onclick='seleccionLiquidacion();'/>&nbsp;";
					//arrayDatosLiquidacion[ "id" + row.idliquidacion ] = {valor:row.bruto,periodos:row.periodos};
					arrayDatosLiquidacion[ "id" + row.idliquidacion ] = {valor:row.bruto,periodos:arrDatosPeriodo};
					
				}else{
					arrayDatosLiquidacion[ "id" + row.idliquidacion ] = {
							valor: (parseInt(arrayDatosLiquidacion[ "id" + row.idliquidacion ].valor) + parseInt(row.bruto))
							,periodos: (arrayDatosLiquidacion[ "id" + row.idliquidacion ].periodos.concat(arrDatosPeriodo))};
							//,periodos: (arrayDatosLiquidacion[ "id" + row.idliquidacion ].periodos + "," + row.periodos)};
				}						
			}
		});
	}
	$("#tdLiquidaciones").html(htmlLiquidacion);
}*/

function buscarLiquidacion(objDatosFiltro){
	var datosRetorno = "";
	$.ajax({
		url:"buscarLiquidacion.php",
		type:"POST",
		data:{objDatosFiltro:objDatosFiltro},
		dataType:"json",
		async:false,
		success:function(datos){
			datosRetorno = datos;
		}
	});
	return datosRetorno;
}

/**
 * METODO: Busca los datos del detalle liquidacion
 * 
 * @param int objDatosFiltro
 * @returns Array  
 * */
function buscaDetalLiqui(objDatosFiltro){
	var datosRetorno = "";
	$.ajax({
		url:"buscarDetalLiqui.php",
		type:"POST",
		data:{objDatosFiltro:objDatosFiltro},
		dataType:"json",
		async:false,
		success:function(datos){
			//************************************
			datosRetorno = datos;
		}
	});
	return datosRetorno;
}

/*function buscarDatosPeriodo(idLiquidacion,idDetalle){
	var datosRetorno = "";
	$.ajax({
		url:"buscarPeriodoLiq.php",
		type:"POST",
		data:{datos:{idliquidacion:idLiquidacion,iddetalleliquidacion:idDetalle}},
		dataType:"json",
		async:false,
		success:function(datos){
			//************************************
			datosRetorno = datos;
		}
	});
	return datosRetorno;
}*/

function seleccionLiquidacion(){
	var valorLiquidaciones = 0,
			 htmlPeriodo = "",
			 imgBotton = '<img src="../imagenes/menu/ico_error.png" style="cursor:pointer;" onclick="controlPeriodo(this);"/>';
	$("#tdLiquidaciones :checkbox:checked").each(function(i,row){
		
		var idLiquidacion = row.value;
		
		
			
		//$.each( arrayDatosLiquidacion[ "id" + row.value ].periodos.split(","), function(j,periodo) {
		$.each( arrayDatosLiquidacion, function(j,rowLiquidacion) {
			
			//Comprobar si es la liquidacion
			if(rowLiquidacion.idliquidacion==idLiquidacion){
				htmlPeriodo += "<tr id='trIdDetalle"+rowLiquidacion.iddetalle+"' class='periodo-activo'>" +
						"<td class='clsBotton'>"+imgBotton+"</td>" +
						"<td class='clsIdLiquidacion'>"+rowLiquidacion.idliquidacion+"</td>" +
						"<td class='clsIdDetalle'>"+rowLiquidacion.iddetalle+"</td>" +
						"<td class='clsPeriodo' style='text-align:center;'>"+rowLiquidacion.periodo+"</td>" +
						"<td class='clsNeto' style='text-align:right;'>"+rowLiquidacion.neto+"</td>" +
						"<td class='clsValorConvenio' style='text-align:center;' >" +
						"<input type='text' name='txtValorConvenio' id='txtValorConvenio"+rowLiquidacion.iddetalle+"' onblur='verifValorConvePerio(\"trIdDetalle"+rowLiquidacion.iddetalle+"\");' />" +
						"</td></tr>";
				
				valorLiquidaciones += parseInt(rowLiquidacion.neto);
			}
			
			/*htmlHeadPeriodo += "<th class='periodo-activo txt_" + rowPeriodo.periodo + "_" + row.value + "' >Liq - " + row.value + 
					" - " + rowPeriodo.periodo + " - " + rowPeriodo.idPeriodo + " - " + imgBotton + "</th>" + ((bandera<5) ? "<td>&nbsp;</td>" : "");
			
			htmlBodyPeriodo += "<td><input type='text' name='txt_" + rowPeriodo.periodo + "_" + row.value + "' " +
					"id='txt_" + rowPeriodo.periodo + "_" + row.value + "' onblur='calcularTotal();' class='box1'/></td>" 
					+ ((bandera<5) ? "<td>&nbsp;</td>" : "");
			
			if( bandera == 5 ){
				htmlPeriodo += "<tr>" + htmlHeadPeriodo + "</tr>";
				htmlPeriodo += "<tr>" + htmlBodyPeriodo + "</tr>";
				htmlPeriodo += "<tr><td colspan='10'></td></tr>";
				htmlHeadPeriodo = "";
				htmlBodyPeriodo = ""; 
				bandera = 0;
			}
			bandera++;*/
		});
	});
	/*if(bandera > 0){
		htmlPeriodo += "<tr>" + htmlHeadPeriodo + "</tr>";
		htmlPeriodo += "<tr>" + htmlBodyPeriodo + "</tr>";
	}*/
	$("#tblPeriodo tbody").html(htmlPeriodo);
	$("#tdValorLiquidacion").text("$" + valorLiquidaciones);
	calcularTotal();
	$("input").numeric();
}

/*
function seleccionLiquidacion(){
	var valorLiquidaciones = 0,
			 htmlHeadPeriodo = "",
			 htmlBodyPeriodo = "",
			 htmlPeriodo = "",
			 bandera = 1,
			 imgBotton = '<img src="../imagenes/menu/ico_error.png" style="cursor:pointer;" onclick="controlPeriodo(this);"/>';
	$("#tdLiquidaciones :checkbox:checked").each(function(i,row){	
		valorLiquidaciones += parseInt(arrayDatosLiquidacion[ "id" + row.value ].valor);
			
		//$.each( arrayDatosLiquidacion[ "id" + row.value ].periodos.split(","), function(j,periodo) {
		$.each( arrayDatosLiquidacion[ "id" + row.value ].periodos, function(j,rowPeriodo) {
			htmlHeadPeriodo += "<th class='periodo-activo txt_" + rowPeriodo.periodo + "_" + row.value + "' >Liq - " + row.value + 
					" - " + rowPeriodo.periodo + " - " + rowPeriodo.idPeriodo + " - " + imgBotton + "</th>" + ((bandera<5) ? "<td>&nbsp;</td>" : "");
			
			htmlBodyPeriodo += "<td><input type='text' name='txt_" + rowPeriodo.periodo + "_" + row.value + "' " +
					"id='txt_" + rowPeriodo.periodo + "_" + row.value + "' onblur='calcularTotal();' class='box1'/></td>" 
					+ ((bandera<5) ? "<td>&nbsp;</td>" : "");
			
			if( bandera == 5 ){
				htmlPeriodo += "<tr>" + htmlHeadPeriodo + "</tr>";
				htmlPeriodo += "<tr>" + htmlBodyPeriodo + "</tr>";
				htmlPeriodo += "<tr><td colspan='10'></td></tr>";
				htmlHeadPeriodo = "";
				htmlBodyPeriodo = ""; 
				bandera = 0;
			}
			bandera++;
		});
	});
	if(bandera > 0){
		htmlPeriodo += "<tr>" + htmlHeadPeriodo + "</tr>";
		htmlPeriodo += "<tr>" + htmlBodyPeriodo + "</tr>";
	}
	$("#tblPeriodo").html(htmlPeriodo);
	$("#tdValorLiquidacion").text("$" + valorLiquidaciones);
	calcularTotal();
	$("input").numeric();
}*/

function controlPeriodo(objImg){
	var urlImg = objImg.src.split("/");
	var accion = urlImg[urlImg.length-1],
		idTr = "#"+$(objImg).parent().parent().attr("id");
	var campo = $( idTr + " input[name='txtValorConvenio']");
		//campo = $( "#" + $(objImg).parent().attr("class").replace(/periodo-activo|periodo-inactivo/gi,"").trim());
	
	switch(accion){
		case "ico_error.png":
			urlImg[urlImg.length-1] = "revisar.png";
			campo.val(0).attr("disabled","disabled").trigger("blur").addClass("ui-state-default");
			$(idTr).removeClass("periodo-activo").addClass("periodo-inactivo");
		break;
		case "revisar.png":
			urlImg[urlImg.length-1] = "ico_error.png";
			campo.val(0).removeAttr("disabled","disabled").removeClass("ui-state-default");
			$(idTr).removeClass("periodo-inactivo").addClass("periodo-activo");
		break;
	}
	objImg.src = urlImg.join("/");	
}

/*
function controlPeriodo(objImg){
	var urlImg = objImg.src.split("/"),
			accion = urlImg[urlImg.length-1],
			campo = $( "#" + $(objImg).parent().attr("class").replace(/periodo-activo|periodo-inactivo/gi,"").trim());
	
	switch(accion){
		case "ico_error.png":
			urlImg[urlImg.length-1] = "revisar.png";
			campo.val(0).attr("disabled","disabled").trigger("blur").addClass("ui-state-default");
			$(objImg).parent().removeClass("periodo-activo").addClass("periodo-inactivo");
		break;
		case "revisar.png":
			urlImg[urlImg.length-1] = "ico_error.png";
			campo.val(0).removeAttr("disabled","disabled").removeClass("ui-state-default");
			$(objImg).parent().removeClass("periodo-inactivo").addClass("periodo-activo");
		break;
	}
	objImg.src = urlImg.join("/");	
}*/

/**
 * METODO: Verifica que el valor del convenio sea <= al liquidado
 * @param idTrPeriodo
 */
function verifValorConvePerio(idTrPeriodo){
	var valorLiquidado = parseInt($("#tblPeriodo #"+idTrPeriodo+" .clsNeto").text().trim());
	var valorConvenio = parseInt($("#tblPeriodo #"+idTrPeriodo+" input[name='txtValorConvenio']").val().trim());
	
	if(valorConvenio>valorLiquidado){
		alert("El valor del convenio debe ser menor o igual al liquidado (Convenio: "+valorConvenio+" <= Liquidado: " +valorLiquidado+ ")");
		$("#tblPeriodo #"+idTrPeriodo+" input[name='txtValorConvenio']").val("");
	}else{
		calcularTotal();
	}
}

/**
 * METODO: Calcular el total de la fecha convenio
 */
function calcularTotal(){
	var total = 0,
		bandera = 0;
	$("#tblPeriodo :text .ui-state-error").removeClass("ui-state-error");
	$("#tblPeriodo :text").each(function(i,row){
		bandera = parseInt(row.value); 
		if(!isNaN(bandera))
			total += bandera;
		else if ( row.value.trim() != "" ){
			$(this).addClass("ui-state-error");
		}
	});
	total += isNaN(parseInt($("#txtValorInteres").val()))? 0 : parseInt($("#txtValorInteres").val());
	$("#txtTotalPagar").val(total);
	calcularSaldo();
}

/*
function calcularTotal(){
	var total = 0,
		bandera = 0;
	$("#tblPeriodo :text .ui-state-error").removeClass("ui-state-error");
	$("#tblPeriodo :text").each(function(i,row){
		bandera = parseInt(row.value); 
		if(!isNaN(bandera))
			total += bandera;
		else if ( row.value.trim() != "" ){
			$(this).addClass("ui-state-error");
		}
	});
	total += isNaN(parseInt($("#txtValorInteres").val()))? 0 : parseInt($("#txtValorInteres").val());
	$("#txtTotalPagar").val(total);
	calcularSaldo();
}*/

/**
 * METODO: Calcular el saldo del acuerdo de pago
 */
function calcularSaldo(){
	var totalAcuerdo = isNaN(parseInt($("#txtValorAcuerdo").val())) ? 0 : parseInt($("#txtValorAcuerdo").val());
	var totalAcumulado = isNaN(parseInt($("#txtTotalPagar").val()))? 0 : parseInt($("#txtTotalPagar").val());
	var saldo = totalAcuerdo-totalAlmacenados-totalAcumulado;
	if(saldo<0){
		$("#txtSaldoDisponible").addClass("ui-state-error");
	}else{
		$("#txtSaldoDisponible").removeClass("ui-state-error");
	}
	$("#txtSaldoDisponible").val(saldo);
}

function guardar(){
	var saldo = isNaN(parseInt($("#txtSaldoDisponible").val())) ? 0 : parseInt($("#txtSaldoDisponible").val());
	$(".ui-state-error").removeClass("ui-state-error");
	if( saldo < 0 ){
		$("#txtSaldoDisponible").addClass("ui-state-error");
		return false;
	}
	
	//Validar que si el usuario chequeo alguna liquidaciones
	$("#tdLiquidaciones .ui-state-error").removeClass("ui-state-error");
	if($("#tdLiquidaciones :checkbox:checked").length == 0){
		$("#tdLiquidaciones").addClass("ui-state-error");
		return false;
	}
	
	if( validaCampoFormu() > 0 )
		return false;
	
	var arrayPeriodo = [],
    		arrayBandera = [],
    		totalValorPeriodos = 0;
	
	//Validar los periodos con acuerdo de pago
	$("#tblPeriodo .periodo-activo").each(function(i,row){
		var idTr = $(this).attr("id");
		objCampo = $("#" + idTr + " input[name='txtValorConvenio']");
		
	    //objCampo = $("#"+$(this).attr("class").replace("periodo-activo","").trim());
	    if(objCampo.val() == 0 ){
	       objCampo.addClass("ui-state-error"); 
	    }else{
	       objCampo.removeClass("ui-state-error");
	       //arrayBandera = $(this).text().split("-");
	       arrayPeriodo[arrayPeriodo.length] = {
	    		   idPeriodo:$("#" + idTr + " .clsIdDetalle").text().trim(),
	    		   /*, periodo:arrayBandera[2].trim()*/
	               /*, idLiquidacion:arrayBandera[1].trim()*/
	               valor:objCampo.val().trim()
	           };
	       totalValorPeriodos += parseInt(objCampo.val().trim());
	    }
	});
	
	if($("#tblPeriodo .ui-state-error").length > 0)
		return false;
	
	var valorAcuerdo = $("#txtValorAcuerdo").val().trim(),
			indice = $("#cmbIndice").val(),
			baderaConvenio = "falso",
			idConvenio = $("#hidIdConvenio").val().trim();
	if( idConvenio == 0){
		baderaConvenio = "verdadero";
	}
	
	var fechaPago = $("#txtFechaPagar").val().trim(),
		valorInteres = ($("#txtValorInteres").val().trim()==0)?0:parseInt($("#txtValorInteres").val().trim());//parseInt($("#txtValorInteres").val().trim());
	var objData = {
			baderaConvenio:baderaConvenio, 
			valorAcuerdo:valorAcuerdo, 
			indice:indice,
			fechaPago:fechaPago,
			valorInteres:valorInteres,
			idConvenio:idConvenio,
			//periodo:(JSON.stringify(arrayPeriodo))
			periodo:arrayPeriodo
	}
	$.ajax({
		url: "guardarConvenio.php",
		type: "POST",
		data: objData,
		dataType: "json",
		async: false,
		success: function(datos){
			if( datos == 0 ){
				alert("Error Al guardar la informaci\xF3n");
			}else{
				$("#hidIdConvenio").val(datos);
				limpiarCampo("#tdValorLiquidacion,#tdLiquidaciones :checkbox,#tblPeriodo tbody,#txtFechaPagar, #txtValorInteres, #txtTotalPagar");
				$("#txtValorAcuerdo,#cmbIndice,#cmbEmpresa").attr("disabled","disabled");
				totalAlmacenados += totalValorPeriodos + valorInteres; 
				fechaAlmacenada[fechaAlmacenada.length] = fechaPago;
				if(!confirm("�Desea guardar otra fecha de pago?")){
					nuevo();
				}
			}
		}
	});
}

function nuevo(){
	arrayDatosLiquidacion = [];
	fechaAlmacenada = [];
	totalAlmacenados = 0;
	limpiarCampo("#tdValorLiquidacion,#tdLiquidaciones,#tblPeriodo tbody,input,select");
	$(".ui-state-error").removeClass("ui-state-error");
	$("#txtValorAcuerdo,#cmbIndice,#cmbEmpresa").removeAttr("disabled","disabled");
}

function validaCampoFormu(){
	var banderaError = 0;
	$( ".ui-state-error" ).removeClass( "ui-state-error" );
	$(".clsValidaText").each(function(i,row){
		if( $( this ).val() == 0 ){
			$( this ).addClass( "ui-state-error" );
			banderaError++;
		}
	});
	return banderaError;
}

function limpiarCampo( classCampo ){
	var usuario = $("#txtUsuario").val();
	var tipoCampo = "";
	$( classCampo ).each(function(i,row){
		
		if( $( this ).get(0).name == "base" ){
			$( this ).val(0);
			return;
		}
		tipoCampo = $( this ).get(0).type;
		
		if( typeof tipoCampo === "undefined" )
			tipoCampo = $( this ).get(0).localName;
		
		if( tipoCampo == "select-one" )
			$( this ).val(0);
		else if( tipoCampo == "text" || tipoCampo == "textarea" || tipoCampo == "hidden" )
			$( this ).val("");
		else if( tipoCampo == "div" || tipoCampo == "td" || tipoCampo == "span" || tipoCampo == "tbody" )
			$( this ).text("");
		else if( tipoCampo == "checkbox" )
			$( this ).attr("checked",false);
		else if( tipoCampo == "table" )
			$( this ).html("");
	});	
}