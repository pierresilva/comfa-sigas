 var urlLogCierre = "cierre.log.php"
	 , urlBuscarCierre = "buscarCierre.php"
 	 , uriDetalleDefinicion = ""
 	 , objIdEstadoCierre = {abierto: 0, cerrado: 0};
 	 
$(function(){
	uriDetalleDefinicion = src()+"phpComunes/buscaDetalDefin.php";
	
	$("#btnGuardar").button().click(guardar);
	$("#btnGenerar").button().click(generarDatos);
	$("#btnCerrarPeriodo").button().click(cerrarPeriodo);
	
	$("#txtPeriodoCierre").blur(ctrBuscarCierre);
	
	init();
	ctrEstadoCierre();
	ctrUltimoCierre();
	
	datepickerC("PERIODO","#txtPeriodoCierre");
	datepickerC("FECHA","#txtFechaCierre");
});

function init(){
	//Obtener el estado cerrado
	var objFiltro = {codigo_definicion:"ESTADO_CIERRE_PROMOTORIA"};
	var dataEstado = fetchDetalleDefinicion(objFiltro);
	
	$.each(dataEstado,function(i,row){
		if(row.codigo=="EST_ABIER")
			objIdEstadoCierre.abierto = row.iddetalledef;
		else if(row.codigo=="EST_CERRA")	
			objIdEstadoCierre.cerrado = row.iddetalledef;
	});	
}

function fetchCierre(objFiltro){
	var resultado = null;
	
	$.ajax({
		url: urlBuscarCierre,
		type:"POST",
		data: {objDatosFiltro:objFiltro},
		dataType:"json",
		async: false,
		success: function(data){
			resultado = data;
		}
	});
	
	return resultado;
}
/**
 * METODO: Busca detalle definicion
 * 
 * @param int objDatosFiltro
 * @returns Array  
 * */
function fetchDetalleDefinicion(objDatosFiltro){
	var datosRetorno = "";
	$.ajax({
		url:uriDetalleDefinicion,
		type:"POST",
		data:{objDatosFiltro:objDatosFiltro},
		dataType:"json",
		async:false,
		success:function(datos){
			//************************************
			datosRetorno = datos;
		}
	});
	return datosRetorno;
}

function ctrEstadoCierre(){
	
	var objFiltro = {codigo_definicion:"ESTADO_CIERRE_PROMOTORIA"};
	var dataEstado = fetchDetalleDefinicion(objFiltro);
	
	var htmlCombo = '<option value="">--SELECCIONES--</option>';
	
	if(dataEstado && typeof dataEstado == "object" && dataEstado.length>0){
		$.each(dataEstado,function(i,row){
			htmlCombo += '<option value="'+row.iddetalledef+'">'+row.detalledefinicion+'</option>';
		});
	}
	
	$("#cmbEstadoCierre").html(htmlCombo);
	
}

function ctrUltimoCierre(){
	var dataCierre = fetchCierre({});
	
	if(dataCierre && typeof dataCierre == "object" && dataCierre.length>0){
		var objData = {};
		var periodo = 0;
		$.each(dataCierre,function(i,row){
			if(parseInt(row.periodo)>periodo){
				objData = row;
				periodo = parseInt(row.periodo)
			}
		});
		
		var htmlCierre = "<tr><td>"+objData.id_cierre+"</td><td>"+objData.periodo+"</td><td>"+objData.fecha_cierre+"</td><td>"+objData.estado+"</td><td>"+objData.usuario_creacion+"</td></tr>";
		$("#tblUltimoCierre tbody").html(htmlCierre);
	}
}

function ctrBuscarCierre(){
	var periodo = $("#txtPeriodoCierre").val();
	
	if(!periodo) return false;
	
	var objFiltro = {periodo:periodo};
	var dataCierre = fetchCierre(objFiltro);
	
	$("#btnGuardar").show();
	$("#btnGenerar,#btnCerrarPeriodo").hide();
	$("#hidIdCierre").val();
	
	if(dataCierre && typeof dataCierre == "object" && dataCierre.length>0){
		
		//Verificar si el cierre esta abierto
		if(dataCierre[0].id_estado==objIdEstadoCierre.cerrado){
			alert("El periodo de cierre ya se encuentra grabado y se esta CERRADO!!");
			nuevo();
			return false;
		}
		
		if(confirm("El periodo de cierre ya se encuentra grabado, desea generar los datos o cerrar el periodo!!")){
			$("#btnGuardar").hide();
			$("#btnGenerar,#btnCerrarPeriodo").show();
			
			$("#hidIdCierre").val(dataCierre[0].id_cierre);
			$("#txtPeriodoCierre").val(dataCierre[0].periodo);
			$("#txtFechaCierre").val(dataCierre[0].fecha_cierre);
			$("#cmbEstadoCierre").val(dataCierre[0].id_estado);
			
		}else{
			nuevo();
		}
	}
}

function guardar(){
		
	if(validateElementsRequired()>0) return false;
	
	var objData = {
			id_cierre: $("#hidIdCierre").val()
			, periodo: $("#txtPeriodoCierre").val()
			, fecha_cierre: $("#txtFechaCierre").val()
			, id_estado: $("#cmbEstadoCierre").val()
		};
	
	var resultado = 0;
	//Hacer la peticion al servidor
	$.ajax({
		url: urlLogCierre,
		type:"POST",
		data: {accion:'I', datos:objData},
		dataType:"json",
		async: false,
		success: function(data){
			if(data.error==0)
				resultado = 1;
			else
				resultado = 0; 
			
		}
	});
	
	if(resultado==1){
		nuevo();		
		alert("El cierre se guardo correctamente!!");
	}else{
		alert("El cierre no se pudo guardar. Favor reportar al administrador T.I.");
	}
}

function cerrarPeriodo(){
	
	if(validateElementsRequired()>0) return false;
	
	$("#cmbEstadoCierre").val(objIdEstadoCierre.cerrado);
	
	var objData = {
			id_cierre: $("#hidIdCierre").val()
			, periodo: $("#txtPeriodoCierre").val()
			, fecha_cierre: $("#txtFechaCierre").val()
			, id_estado: $("#cmbEstadoCierre").val()
		};
	
	var resultado = 0;
	//Hacer la peticion al servidor
	$.ajax({
		url: urlLogCierre,
		type:"POST",
		data: {accion:'CERRAR_PERIODO', datos:objData},
		dataType:"json",
		async: false,
		success: function(data){
			if(data.error==0)
				resultado = 1;
			else
				resultado = 0; 
			
		}
	});
	
	if(resultado==1){
		nuevo();		
		alert("El periodo se cerro correctamente!!");
	}else{
		alert("El periodo no se pudo cerrar. Favor reportar al administrador T.I.");
	}
}

function generarDatos(){
	
	if(validateElementsRequired()>0) return false;
	
	if(!confirm("Esta seguro de generar los datos del cierre!!")) {
		nuevo();
		return false;
	}
	
	var objData = {
			id_cierre: $("#hidIdCierre").val()
			, periodo: $("#txtPeriodoCierre").val()
			, fecha_cierre: $("#txtFechaCierre").val()
			, id_estado: $("#cmbEstadoCierre").val()
		};

	
	var resultado = 0;
	//Hacer la peticion al servidor
	$.ajax({
		url: urlLogCierre,
		type:"POST",
		data: {accion: 'G', datos:objData},
		dataType:"json",
		async: false,
		beforeSend: function(objeto){
        	dialogLoading('show');
        },
        complete: function(objeto, exito){
        	dialogLoading('close');
        },
		success: function(data){
			if(data.error==0)
				resultado = 1;
			else
				resultado = 0; 
		}
	});
	
	if(resultado==1){
		nuevo();		
		alert("Los datos del cierre se generaron correctamente!!");
	}else{
		alert("Los datos del cierre no se generaron. Favor reportar al administrador T.I.");
	}
}

function nuevo(){
	limpiarCampos2("#txtPeriodoCierre, #hidIdCierre, #txtFechaCierre, #cmbEstadoCierre");
	$("#btnGuardar").show();
	$("#btnGenerar,#btnCerrarPeriodo").hide();
}

