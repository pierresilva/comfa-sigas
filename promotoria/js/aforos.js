/* 
* CONTROL DE VALIDACIONES DE LIQUIDACIONES
* Autor: AndrÃ©s Felipe Lara Nieto
* Fecha: 04-Mar-2011
*/
//Variables generales
var URL=src();
//nuevo=0;
$(document).ready(function(){
	$("#txtLiquidacion").focus();
	//Forzar el click del icono de periodos
	$("#ordenarBase").blur(function(){ $("#periodos").trigger("click");});
	
	
//PREGUNTAR SIEMPRE SI EL NO DE VISITA TIENE ALGUN VALIR
if($("#txtLiquidacion").val()!=''){
$("#txtLiquidacion").focus().blur();
	}	
	
	
//Plugin periodos
$("#periodos").periodos({claseMes:'ui-state-default',idDestino:'etiquetas',anioCompleto:true});

//Campos Numericos
$("#ibc,#txtLiquidacion,:text[name='base'],#txtAportesPag").numeric();
$("#tasaInteres").numeric({allow:'.'});	

//Sumar Base liquidaciÃ³n
$(":text[name='base']").blur(sumarBase);

//Sumar los aportes
$(":input[name='chkLiquidacion']").click(function(){
   
   var objAportes=$("#totalLiq");
   var valorBase=parseInt($("#totalBase").html());
     
   if (valorBase==0){
	   alert("La base de liquidaci\u00F3n debe tener un valor.");
	   $(this).attr("checked",false);
       return false;
   }
       
   if($("#chkAportesPag").is(":checked")){
	   if($("#txtAportes").val()==0){
         alert("Seleccione el porcentaje de aporte.");
		 return false;}	
		  
		 $("#txtAportesPag").attr("disabled",false).focus().val('');
   }else{
	     $("#txtAportesPag").attr("disabled",true).val('0'); 
		 $("#txtInteres").val(0);
		 $("#cboAportes").trigger("change");
		 $("#tasaInteres").val('');
		 sumarAportes();
	   }
   
});//end click

$("#iconActualizar").hide();
$("#iconRevisar").hide();
//Limpiar valor al recibir el foco los campos BASE
$(":text[name='base']").focus(function(){ this.value='';}).blur(function(){if(this.value==0)this.value=0;})
//Activo los detapicker en el formulario
//$("#fechaNot,#fechaLiq").datepicker({showAnim:'slideDown',changeMonth:true});
//Dialog ayuda
$("#ayuda").dialog({
		 	      autoOpen: false,
			      height: 450,
			      width: 700,
			      modal:false,
			      open: function(evt, ui){
					$('#ayuda').html('');
					$.get(URL +'help/promotoria/Manualayudaliquidaciones.html',function(data){
					$('#ayuda').html(data);
					});
			      }
});
//Dialog Colaboracion en linea
$("#dialog-form2").dialog({
		autoOpen: false,
		height: 400,
		width: 500,
		draggable:false,
		modal: true,
		buttons: {
		    'Enviar': function() {
			var bValid = true;
			var campo=$('#notas').val();
			var campo0=$.trim(campo);
			if (campo0==""){
				$(this).dialog('close');
					return false;
				}
			var campo1=$('#usuario').val();
			var campo2="liquidaciones.php";
			$.post(URL+'phpComunes/colaboracion.php',{v0:campo0,v1:campo1,v2:campo2},
			function(datos){
				if(datos=='1'){
					alert("Su comentario fue enviado correctamente!. Gracias por participar en el mejoramiento de SIGAS");
				}else{
					alert(datos);
				}
		});
		$(this).dialog('close');
		},
		Cancelar: function() {
			$(this).dialog('close');
			$("#dialog-form2").dialog("close");
		}
	}
});
//Validar Tecla enter del campo NIT
/*$("#nitLiq").keypress(function(event){
  if(event.keyCode==13){
	$("#btnBuscarNit").trigger("click");
	}
});*/
//Dialog Tabla Liquidaciones
/*$("#tablaLiquidaciones").dialog({
		 	      autoOpen: false,
			      width: 1000,
			      modal:true,
				  open:function(){
				   
				   //Limpiar Tabla 
				   $("#liquidaciones tbody tr").remove(); 
				   $("#nitLiq").val('').focus();	  
				   $("#sucursalLiq option").remove();
				   $("#totalLiqTabla").html('');
	              
				  $("#datosEmpresaLiq").html('');
			      $("#tablaLiquidaciones").delegate("#liquidaciones","hover",function(){$(".tablaR tr:even").addClass("zebra");});
					
		  },
		 buttons:{"IMPRIMIR":function(){location.href=URL+"centroReportes/promotoria/reporte002.php?idempresa="+idempresa;}}	  
      });//dialog*/
//Boton buscar nit en dialog liquidaciones
var acum=0;
$("#btnBuscarNit").click(function(){/*
					      
						  nitLiq="";
						  nitLiq=$("#nitLiq").val()
						  //Limpiar Tabla y datos 
				          $("#liquidaciones tbody tr").remove();
						  $("#totalLiqTabla").html('');
						  //$("#sucursalLiq option").remove();
						  $("#datosEmpresaLiq").html('');
						  $("#tablaLiquidaciones .ui-state-error").removeClass("ui-state-error");	  
					   // var arrPromotores=[];
					      promotor="";
						  if(nitLiq==''||isNaN(nitLiq)){
						  $("#nitLiq").addClass("ui-state-error");
						  return false;
						  }
						  //Buscamos el id de la empresa
						  $.ajax({
		    					url:URL+'phpComunes/buscarEmpresa.php', 
								type:"POST",
								data:"v0="+nitLiq, 
								dataType:"json",
								async:false,
								success:function(datos){
	           		      				 if(datos==0){
									   	$("#liquidaciones").append("<tr><td colspan='9' class='Rojo' align='center'>No existe el nit en la base de datos.</td></tr>");
										 return false;
										 }
										$.each(datos,function(i,fila){ idempresa=fila.idempresa;empresa=fila.razonsocial;});//each
										 //Buscamos las liquidaciones realizadas a la empresa seleccionada
				          			
										 $.ajax({
		    								url:'buscarLiqEmpresa.php', 
											type:"POST",
											data:"id="+idempresa, 
											dataType:"json",
											success:function(datos2){
											    if(datos2==0){
					            		 		$("#liquidaciones").append("<tr><td colspan='9' class='Rojo' align='center'>No hay historial de liquidaciones</td></tr>");
						        		 		return false;
												}else{
													var total=0;
													$.each(datos2,function(i,fila2){
											
							  						$.getJSON('buscarPromotorLiq.php',{idp:fila2.idpromotor},function(rta){
						      	  						if(rta==0){
							          					alert("Error al cargar promotores");
							          					$("#tablaLiquidaciones").dialog("close");
							          					return false;
							       						}
								   		  				//Datos de la empresa 
								   		  				$("#datosEmpresaLiq").html(empresa);	
								   		 			   //Cargo y creo el array con los nombres de los promotores
								   		 				promotor=$.trim(rta.pnombre)+" "+$.trim(rta.papellido);
							              				
														//Convierto en array la cadena de periodos separadas por comas..............................
								   		 				var periodos=fila2.periodos.split(",")	;
														var txtperiodos="";
														var con=0;
														//recorro el array para dar un salto de linea por cada 6 items
														for(j=0;j<periodos.length;j++){
														con++;
														//Si contador es 6 se agrega el salto de linea
														if(con==6){
														txtperiodos+=periodos[j]+"\n";
														con=0;
														}else{
															if(j>=periodos.length){
																txtperiodos+=$.trim(periodos[j]);
															}else{
																txtperiodos+=$.trim(periodos[j])+"-";
															}
														}
														}																						 				
														
														$("#liquidaciones").append("<tr>"+
					              		                "<td><a href='#' onClick='buscarLiquidacion(this.text,1)'>"+fila2.idliquidacion+"</a></td>"+
							   				            "<td>"+fila2.fechaliquidacion+"</td>"+
							   	 			            "<td>"+txtperiodos+"</td>"+
							   				            "<td>"+promotor+"</td>"+
				 			   							"<td>"+formatCurrency(fila2.sueldos)+"</td>"+
														"<td>"+formatCurrency(fila2.bruto)+"</td>"+
														"<td>"+parseFloat(fila2.indicador)*100+"%</td>"+
														"<td>"+formatCurrency(fila2.aportes)+"</td>"+
														"<td>"+formatCurrency(fila2.neto)+"</td>"+
						       							"</tr>");
													});//json
													total+=parseFloat(fila2.neto);
						                  		});//each  
										  		//Mostrar total tabla
										  		$("#totalLiqTabla").html("<b>"+formatCurrency(total)+"</b>");
											  }//end else
											}///succes2
										});//ajax2
										 
								}//succes	 
						});//ajax
			  */});//fin click btnBuscarNit		 -----------------------------------------	  
	  
//Clase zebra para la liquidacion
$(".sortable tr:odd").addClass("vzebra-odd");

});//fin ready
//-----------------------------------HALLAR APORTES
function hallarAportes(elm){/*
     obj=jQuery(elm);
	 if($("#totalBase").html()==0){
	 alert("El total base debe tener un valor.");
	 $("#cboAportes").val(0);
	 return false;
	 }
	 
	 if($("#txtAportesPag").val()!=0){
	 $("#chkAportesPag").attr("checked",false);
	 $("#txtAportesPag,#txtCapital,#tasaInteres,#txtInteres").val('');
	 }
	 
	 txtAportes=parseFloat(obj.children("option:selected").val());
	 totalAportes=parseFloat($("#totalBase").html())*txtAportes; 
	 //$("#txtAportes")
	 $("#txtAportes,#txtCapital").val(totalAportes.toFixed(2));	
	 sumarAportes();																	
*/}
//-----------------------------------CALCULAR CAPITAL
function calcularCapital(elm){
 
  obj=jQuery(elm);
  //Limpiar cajas de texto
  $("#txtCapital,#txtInteres").val(0); 
 
  if(obj.val()!=''||obj.val()!=0){
	  var valor=obj.val();
	  var totalCapital=parseFloat($("#txtAportes").val())-parseFloat(valor);
	  $("#txtCapital").val(totalCapital.toFixed(2));
	  sumarAportes();
  }
}
//-----------------------------------CALCULAR INTERES
function validarInteres(interes){
	//ExpresiÃ³n regular para validar numero-punto-numero 
	var patron=new RegExp("^[0-5]{1}(\.[1-9]{1})?$"); 
    $("span.Rojo").empty();
	
	if($("#tasaInteres").val()==''){
	$("#txtInteres").val(0);
	sumarAportes();
	return false;
	}
	
	if(parseFloat(interes)<=0.0||parseFloat(interes) >9.9){
   $("#tasaInteres + span").append("<span class='Rojo'> Tasa inv\u00E1lida.</span>").hide().fadeIn();
	$("#tasaInteres").val('').focus();
    return false;
	}
	
	if(!patron.test(interes.toString())){
	$("#tasaInteres + span").append("<span class='Rojo'> El valor no es v\u00E1lido.</span>").hide().fadeIn();
	$("#tasaInteres").focus().val('');
	return false;
	}
	
	var indiceTasa=parseFloat(interes/100);
	var valTasa=parseFloat($("#txtCapital").val())*indiceTasa;
	$("#txtInteres").val(valTasa.toFixed(2));
	sumarAportes();
}
//-----------------------------------SUMAR BASE LIQUIDACION
function sumarBase(){
    var acum=0;
 // if(nuevo==0){ alert("Haga click primero en Nuevo");return false;}	
   limpiarAportes();   
   for(i=0;i<$(":text[name='base']").size();i++){
	 var valorTemp=$(":text[name='base']").eq(i).val();
	 if(valorTemp!=0)
     acum+=parseFloat(valorTemp);
   }
   //Mostrar total base 
   $("#totalBase").html(acum);
}
//-----------------------------------SUMAR TOTAL APORTES	
function sumarAportes(){
  var total=0;
  //"#txtAportesPag
  if($("#txtCapital").val()>0){
      var valInteres=($("#txtInteres").val()=='' ? -1*parseFloat($("#txtInteres").val()) : parseFloat($("#txtInteres").val()) );
      total=parseFloat($("#txtCapital").val())+parseFloat(valInteres);
  }else{
	   total=parseFloat($("#txtAportes").val());
	   }
  //Mostrar el total de los aportes
  $("#totalLiq").html(total.toFixed(2));
}
//-----------------------------------LIMPIAR CAMPOS DE APORTES
function limpiarAportes(){
   $("#cboAportes,#txtAportes,#txtAportesPag,#txtCapital,#txtInteres").val(0);
   $("#totalLiq").html(0);
   $("#chkAportesPag").attr("checked",false);
}
//-----------------------------------CAMBIAR EL ORDEN DE LAS FILAS DE LOS ITEMS DE LA BASE LIQUIDACION
function ordenBase(opcion){
 $(":text[name='base']").val('0');	
 sumarBase();
 switch (opcion){
   case 'pl': $("#tablaBase tr.privado").hide(); 
              $("#tablaBase tr.publico").show(); 
              break;
   case 'pr': $("#tablaBase tr.publico").hide();
              $("#tablaBase tr.privado").show();  
              break;
   
   default:	 $("#tablaBase tr:hidden").show();
 }//end switch
}
//-----------------------------------NUEVA ACTA DE VISITA
function nuevaLiquidacion(){
  limpiarCampos();
  nuevo=1;
}
//-----------------------------------GUARDAR ACTA DE VISITA
function validarCampos(){
//-----------------No VISITA------------
   if(noVisita==''||isNaN(noVisita)){
   $("#noVisita").addClass("ui-state-error");
   error++;
   }
//-----------------No LIQUIDACION------------
   if(txtLiquidacion==''||isNaN(txtLiquidacion)){
   $("#txtLiquidacion").addClass("ui-state-error");
   error++;
   }
//----------------FECHA LIQUIDACION----------
   if(fechaLiq==''){
   $("#fechaLiq").addClass("ui-state-error");
   error++;
   }	
//------------------NIT----------------------
  if(nit==''||isNaN(nit)){ 
  $("#nit").addClass("ui-state-error");
  error++;
  }
//-----------------TIPO AFILIACION------------
   if(tipoAfiliacion=='0'){
   $("#tipoAfiliacion").addClass("ui-state-error");
   error++;
   }
//-----------------IBC-----------------------
 /*  if(ibc==''){
   $("#ibc").addClass("ui-state-error");
   error++;
   }*/
 //-----------------CAUSAL-------------------
   if(causal=='0'){
   $("#causal").addClass("ui-state-error");
   error++;
   }
//-----------------TIPO NOTIFICACION----------
   if(tipoNotificacion=='0'){
   $("#tipoNotificacion").addClass("ui-state-error");
   error++;
   }
//-----------------FECHA NOTIFICACION---------
   if(fechaNot==''){
   $("#fechaNot").addClass("ui-state-error");
   error++;
   }
//----------------PROMOTOR--------------------
   if(promotor=='0'){
   $("#promotor").addClass("ui-state-error");
   error++;
   }	 
//-----------------PERIODOS -----------------
   if(periodos==''){
   $("#periodos").addClass("ui-state-error");
   error++;
   }
//----------------TOTAL BASE LIQUIDACION-------
   if(totalBase==0){
   var altoBase=$("#totalBase").offset().first;
   $("html").animate({scrollfirst:altoBase},1000,function(){
   $("#totalBase").parent().show("pulsate");
   });
   error++;
   return false;
   }	 
//----------------TOTAL BASE LIQUIDACION-------
   if(totalLiq==0){
    var altoLiq=$("#totalLiq").offset().first;
    $("html").animate({scrollfirst:altoLiq},1000,function(){
	 $("#totalLiq").parent().show("pulsate");
	});
    error++;
    return false;
   }	 
    
}

//-----------------------------------GUARDAR ACTA DE VISITA
function guardarLiquidacion(){/*
      
	  error=0;
	  $(".ui-state-error").removeClass("ui-state-error");//Limpiar la clase de error
	  	  
	  if(nuevo==0){
		alert("Haga click primero en Nuevo");
		return false;
      }
	   //CAMPOS DE LA LIQUIDACION
	   noVisita=$("#noVisita").val();
	   txtLiquidacion=$("#txtLiquidacion").val();
	   fechaLiq=$("#fechaLiq").val();
	   
	   //CAMPOS DEL APORTANTE
	   nit=$("#idempresa").val();
	   ibc=($("#ibc").val()==''?0:$("#ibc").val());
	   tipoAfiliacion=$("#tipoAfiliacion").val();
	   causal=$("#causal").val(); 
	   tipoNotificacion=$("#tipoNotificacion").val();
	   fechaNot=$("#fechaNot").val();
	   observacion=$("#observacion").val();
	   documentacion=$("#documentacion").val();
	  // usuario=$("#usuario").val();
	   promotor=$("#promotor").val();
       periodos=$("#array").val();
	   
	   //CAMPOS BASE
	   totalBase=parseFloat($("#totalBase").html());
	   sueldos=$(":text[name='base']:eq(0)").val();
	   integrales=$(":text[name='base']:eq(1)").val();
	   supernumerarios=$(":text[name='base']:eq(2)").val();
	   extras=$(":text[name='base']:eq(3)").val();
	   nocturnos=$(":text[name='base']:eq(4)").val();
	   antiguedad=$(":text[name='base']:eq(5)").val();
	   jornales=$(":text[name='base']:eq(6)").val();
	   comisiones=$(":text[name='base']:eq(7)").val();
	   vacaciones=$(":text[name='base']:eq(8)").val();
	   primaVacaciones=$(":text[name='base']:eq(9)").val();
	   bonificaciones=$(":text[name='base']:eq(10)").val();
	   primasExtras=$(":text[name='base']:eq(11)").val();
	   unidadTiempo=$(":text[name='base']:eq(12)").val();
	   viaticos=$(":text[name='base']:eq(13)").val();
	   remuneraciones=$(":text[name='base']:eq(14)").val();
	   contratosAgricolas=$(":text[name='base']:eq(15)").val();
	   primaServicios=$(":text[name='base']:eq(16)").val();
	   primaManejo=$(":text[name='base']:eq(17)").val();
	   gastos=$(":text[name='base']:eq(18)").val();
	   bonificacionServicios=$(":text[name='base']:eq(19)").val();
	   subsidioAlimentacion=$(":text[name='base']:eq(20)").val();
	   primaTecnica=$(":text[name='base']:eq(21)").val();
	   horasCatedra=$(":text[name='base']:eq(22)").val();
	   licencias=$(":text[name='base']:eq(23)").val();
	   indemnizaciones=$(":text[name='base']:eq(24)").val();
	   otras=$(":text[name='base']:eq(25)").val();
	   
	   
	   //CAMPOS APORTES
	   indicador=$("#cboAportes").val();
	   aportes=$("#txtAportes").val();
	   aportesPagados=$("#txtAportesPag").val();
	   capital=$("#txtCapital").val();
	   tasaInteres=($("#tasaInteres").val()==''? 0 :parseFloat($("#tasaInteres").val()) );
	   mora=$("#txtInteres").val();
	   totalLiq=parseFloat($("#totalLiq").html());
	   
	   //VALIDAR CAMPOS OBLIGADOS
	   validarCampos();
	   
//Si no hay errores se guarda la visita
 if(error==0){
	 //El orden esta de acuerdo a los campos de la base de datos
	 var datos={
		       v0:txtLiquidacion,
			   v1:noVisita,
			   v2:fechaLiq,
			   v3:nit,
			   v4:tipoAfiliacion,
			   v5:ibc,
			   v6:causal,
			   v7:tipoNotificacion,
			   v8:fechaNot,
			   v9:periodos,
			   v10:sueldos,
			   v11:integrales,
			   v12:supernumerarios,
			   v13:extras,
			   v14:nocturnos,
			   v15:antiguedad,
			   v16:jornales,
			   v17:comisiones,
			   v18:vacaciones,
			   v19:primaVacaciones,
			   v20:bonificaciones,
			   v21:primasExtras,
			   v22:unidadTiempo,
			   v23:viaticos,
			   v24:remuneraciones,
			   v25:contratosAgricolas,
			   v26:primaServicios,
			   v27:primaManejo,
			   v28:gastos,
			   v29:bonificacionServicios,
			   v30:subsidioAlimentacion,
			   v31:primaTecnica,
			   v32:horasCatedra,
			   v33:licencias,
			   v34:indemnizaciones,
			   v35:otras,
			   v36:0,
			   
			   v38:indicador,
			   v39:aportes,
			   v40:aportesPagados,
			   v41:capital,
			   v42:tasaInteres,
			   v43:mora,
			   
			   v45:observacion,
			   v46:documentacion,
			   v47:promotor
			  }
    
	//GUARDAR LOS DATOS
	$.getJSON("guardarLiquidacion.php",datos,function(data){
	 	if(data==0){
	 	alert("No se pudo guardar la liquidaci\u00F3n");
	 	return false;
	 	}
	 	
		alert("Se guardo la liquidaci\u00F3n No "+txtLiquidacion);	 
	 	nuevo=0;
	 	limpiarCampos();   
		});
  }//end if		 
*/}
//---------------------------------------LIMPIAR CAMPOS
function limpiarCampos(){
           $("input:text:not('#usuario'),textarea,:hidden").val('');
		   $(":text[name='base'],:text[name='aportes']").val(0);
	       $("table.tablero select").val(0);
		   $("#empresa,#etiquetas,#repLegal").empty();
		   $(".ui-state-error").removeClass("ui-state-error");
		   $("#bGuardar").show(); //mostrar icono guardar
		   $("#iconActualizar").hide(); //Ocultar icono actualizar
		   $("#iconRevisar").hide();
		   $("#totalBase,#totalLiq").html('0');
	       $("#noVisita,#txtLiquidacion").attr("disabled",false);
		   $("#noVisita").focus();
		   //Msje de empresas que han pagado
		   $("#msjeLiquidado strong").empty();
		   $("#msjeLiquidado p span").empty();
		   $("#msjeLiquidado").hide();
		   limpiarAportes();
}
//----------------------------------------VENTANA DE DIALOGO DE AYUDA
function mostrarAyuda(){
	$("#ayuda").dialog('open');
}
//----------------------------------------VENTANA DE DIALOGO DE MANUAL
function notas(){
	$("#dialog-form2").dialog('open');
}
//----------------------------------------BUSCAR EMPRESA
function buscarEmpresa(obj,obj2){/*
	
	var nit=obj;
	var sucursal=obj2;
	
	//Limpiamos campos
	$("#empresa").html("");
    $("#nit,#sucursal").removeClass("ui-state-error");
	
	if(nit==''||isNaN(nit)){
     $("#nit").addClass("ui-state-error");
      return false;
    }else{
	 //Si esta vacio el campo sucursal
	    if($("#sucursal").val()=='0'){
		     $("#sucursal").addClass("ui-state-error");
			 $("#sucursal").focus();
             return false;
		  }
		  //Si existe la empresa, traemos los datos de la BD
		  $.getJSON(URL+'phpComunes/buscarEmpresaCompleta.php',{v0:nit,v1:$("#sucursal").val()},function(data){
               
			    if(data==0){
                    alert("El nit No existe, lo siento!");
                    limpiarCampos();
                    return false;}
				
                $.each(data, function(i,fila){
				       rs=fila.razonsocial;
			    	   estado=fila.estado;
					   direccion=fila.direccion;
					   telefono=fila.telefono;
					   ciudad=fila.municipio;
					   idRepLegal=fila.identificacion;
					   pa=fila.papellido;
					   sa=fila.sapellido;
					   pn=fila.pnombre;
					   sn=fila.snombre;
					   trabajadores=fila.trabajadores;
					   return;
                });//each
                
				//Concatenar Nombre sin espacios
				nombreRepLegal="";
				$.each([pn,sn,pa,sa],function(i,n){
				        nombreRepLegal+=" "+$.trim(n);
				});
				
				 //Insertamos los datos de la empresa en los campos del formulario
				$("#idempresa").val(idempresa); 
                $("#empresa").html(rs);
				$("#estado").val(estado);
				$("#direccion").val(direccion);
				$("#telefono").val(telefono);
				$("#ciudad").val(ciudad);
				$("#repLegal").html(nombreRepLegal);
				$("#idRepLegal").val(idRepLegal);
				$("#trabajadores").val(trabajadores);
		  });//Fin getJSON
		}//else
*/}//fin buscar empresa	
//----------------------------------------LIQUIDACIONES POR EMPRESA
function liqEmpresa(){
	
     //Abrir Dialog
     $("#tablaLiquidaciones").dialog("open");
 
}//end function
//----------------------------BUSCAR VISITA
function buscarVisita(elm){/*
  var idvisita=parseInt(elm.value);
  $("#noVisita").removeClass("ui-state-error");
  
  if(isNaN(idvisita)){
          $("#noVisita").addClass("ui-state-error").focus();
		   return false;
  }
   
   //Buscamos si la visita no existe en la tabla de liquidaciones 
   $.ajax({
	      type:"POST",
		  url:"buscarVisitaLiq.php",
		  data:"idvisita="+idvisita,
		  async:false,
		  success:function(datos){
			     if(datos==1){
					 MENSAJE("Ya existe una visita almacenada en las liquidaciones realizadas.");
					 $("#noVisita").val('').focus();
					 return false;
				 }
				
				 //Si no existe en la tabla liquidaciones,buscamos si existe una visita con ese cÃ³digo en la tabla de visitas
   				 $.getJSON('buscarVisita.php',{id:idvisita},function(data){
                 //Limpiamos
		         $("#fechaLiq,#txtLiquidacion").val('');
		  		 //Si hay visita con ese numero
		         if(data!=0){
			  	   $.each(data, function(i,fila){
                   idvisita=fila.idvisita;
			  	   fechavisita=fila.fechavisita;
				   idempresa=fila.idempresa;
				   convenio=fila.convenio;
				  return;
                 });//each
			     //Buscar Datos de la empresa segun idempresa 
				 if(convenio=='V'){
				 MENSAJE("Esta visita, est\u00E1 al d\u00EDa.");
				 limpiarCampos();
				 return false;
				 }else{
			     $.getJSON(URL+'phpComunes/buscarEmpresaId.php',{v0:idempresa},function(data){
			     $.each(data, function(i,fila){ nitempresa=fila.nit; sucursal=fila.codigosucursal;}); 
			     $("#nit").val(nitempresa);
			     $("#sucursal").val(sucursal);
			     //Se llama la funcion buscar empresa para traer los datos del representante ,direccion, telefno etc..
			     buscarEmpresa(nitempresa,sucursal);
				});//json 
			   }//else
			 //Cargar Datos traidos del servidor 
		     $("#noVisita").val(idvisita).attr("disabled",true);
		     $("#fechaLiq").val(fechavisita);
			 }else{
		   MENSAJE("NO existe la visita No. "+idvisita);
		   limpiarCampos();
		   $("#noVisita").focus();
		  }//end if data
	});//end json 
			  
			  }
		  });//end ajax
   
    
*/}
//---------------------------------------- BUSCAR LIQUIDACIONES SI EXISTEN O CARGAR LAS LIQUIDACIONES POR EMPRESAS
function buscarLiquidacion(obj,accion){
var idliquidacion=parseInt(obj.value);
if(isNaN(idliquidacion)){
	obj.focus();
	return false
}	
 //accion 0 => Buscar , 1 => Cargar 	
 $("#txtLiquidacion,#noVisita").removeClass("ui-state-error");
 $("#etiquetas").empty();
 $("#bGuardar").show();//mostrar boton guardar
 $("#iconActualizar").hide();
 $("#iconRevisar").hide();
 //Msje de empresas que han pagado
 $("#msjeLiquidado strong").empty();
 $("#msjeLiquidado p span").empty();
 $("#msjeLiquidado").hide();
 
   //Buscamos si existe una liquidacion con ese cÃ³digo
 $.getJSON('buscarLiquidacion.php',{id:idliquidacion},function(data){          
		  //Si hay liquidaciones con ese numero
 	if(data==0){
		MENSAJE("Lo lamento no encontre su LIQUIDACION!");
		obj.value="";
	}else{
		 base=[]
		 $.each(data,function(i,fila){
       	    txtLiquidacion=fila.idliquidacion;
			noVisita=fila.idvisita;
			fechaLiq=fila.fechaliquidacion;
			idempresa=fila.idempresa;
			tipoAfiliacion=fila.idtipoafiliacion;
			ibc=fila.ibc;
			causal=fila.idcausal;
			tipoNotificacion=fila.idtiponotificacion;
			fechaNot=fila.fechanotificacion;
			periodos=fila.periodos;
			//Base
			base[0]=fila.sueldos;
			base[1]=fila.integral;
			base[2]=fila.supernumerario;
			base[3]=fila.festivos;
			base[4]=fila.nocturnos;
			base[5]=fila.antiguedad;
			base[6]=fila.jornales;
			base[7]=fila.comisiones;
			base[8]=fila.vacaciones;
			base[9]=fila.primavacaciones;
			base[10]=fila.bonificaciones;
			base[11]=fila.primasextralegales;
			base[12]=fila.obradestajo;
			base[13]=fila.viaticospermanentes;
			base[14]=fila.remuneracionsocios;
			base[15]=fila.contratosagricolas;
			base[16]=fila.primaservicios;
			base[17]=fila.primamanejo;
			base[18]=fila.gastosrepresentacion;
			base[19]=fila.bonificacionservicios;
			base[20]=fila.subsidioalimentacion;
			base[21]=fila.primatecnica;
			base[22]=fila.horascatedra;
			base[23]=fila.licenciasremuneradas;
			base[24]=fila.indemnizacionvacaciones;
			base[25]=fila.otros1;
			//base[26]=fila.otros2;
			baseTotal=fila.bruto;//total base
						
			indicador=fila.indicador;
			aportes=fila.aportes;
			aportesPagados=fila.aportescancelados;
			capital=fila.capital;
			tasaInteres=fila.tasainteres;
			mora=fila.interesmora;
			neto=fila.neto//total aportes
			observacion=fila.observaciones;
			documentacion=fila.documentacion;
			promotor=fila.idpromotor;
			estado=fila.estado;
			fechaestado=fila.fechaestado;
			motivo=fila.motivo;
			revisada=fila.revisado;
			return;
              });//each 
			if(revisada=='S'){
				alert("La liquidaci&oacute;n ya fu&eacute LIQUIDADA!");
				limpiarCampos();
				return false;
			}
			 //Mostrar datos en caso que accion=1
			 //Mostrar icono actualizar de acuerdo al estado='i' de la empresa
			 if(estado=='I'){
				 $("#iconActualizar").hide();
				 $("#iconRevisar").hide();
				 $("#msjeLiquidado").show("drop");
				 $("#msjeIdLiq").html(txtLiquidacion);
				 $("#msjeIdVis").html(noVisita);
				 $("#msjeMotivo").html(motivo);
				 $("#msjeFechaPag").html(fechaestado);
				 $(".tablero :text").attr("disabled",true);
			}else{
				$("#iconActualizar").show();
				$("#iconRevisar").show();
			} 
			//Cargar Datos traidos del servidor
			
			//CAMPOS DE LA LIQUIDACION
	   		//$("#noVisita").val(noVisita).attr("disabled",true);
	   		$("#txtLiquidacion").val(txtLiquidacion).attr("disabled",true);
	   		$("#fechaLiq").val(fechaLiq);
			//Buscamos uscamos si existe una visita con ese cÃ³digo en la tabla de liquidaciones
			  
	   	//CAMPOS DEL APORTANTE
	 		$("#idempresa").val(idempresa);
	   		$("#ibc").val(ibc);
	   		$("#tipoAfiliacion").val(tipoAfiliacion);
	   		$("#causal").val(causal); 
	  		$("#tipoNotificacion").val(tipoNotificacion);
	  		$("#fechaNot").val(fechaNot);
	   		$("#observacion").val(observacion);
	  		$("#documentacion").val(documentacion);
	 	   	$("#promotor").val(promotor);
         	
			//PERIODOS
			arrPeriodos=periodos.split(",");
			var etiqueta="";
			$.each(arrPeriodos,function(i,n){
				
			//Si inicia en D es aÃ±o completo sino es periodo normal 'De Ene a Dic de xxxx' Ã³ 201101
			if(n.slice(0,1)=='D'){
			     anio=n.slice(-4);
				 etiqueta+="<label class='etiqueta' id='lblAnioCompl' name='"+anio+"'><span>De Ene a Dic del "+anio+"</span><span class='x'>X</span></label>";
			}else{
				anio=n.slice(0,4);
			    mes=n.slice(-2);	
			    etiqueta+="<label class='etiqueta' id='lbl"+n+"'><span>"+anio+"-"+mes+"</span><span class='x'>X</span></label>";
			}	
						
			});
			
			$("#array").val(periodos);
			//Ingresar las etiquetas traidas del servidor
			$("#etiquetas").append(etiqueta);
				   
	   		//CAMPOS BASE
	        $(":text[name='base']").each(function(i){
			 $(this).val(base[i]);
			});//end each base
	 		$("#totalBase").html(baseTotal); 
			 
			//CAMPOS APORTES
	  		$("#cboAportes").val(indicador);
	  		$("#txtAportes").val(aportes);
	   		$("#txtAportesPag").val(aportesPagados);
	   		$("#txtCapital").val(capital);
	  		$("#tasaInteres").val(tasaInteres);
	   		$("#txtInteres").val(mora);
	  		$("#totalLiq").html(neto);
			$("#observaciones").html(observacion);
		 $.ajax({
		    url:URL+'phpComunes/buscarEmpresaId.php', 
			type:"POST",
			data:"v0="+idempresa, 
			dataType:"json",
			async:false,
			success:function(datos){	
		   		$.each(datos,function(i,fila){
		   			$("#nit").val(fila.nit);
					$("#estado").val(fila.estado);
					$("#empresa").html(fila.razonsocial);
					idpersona=fila.idrepresentante;
					return;
		   		});
				$.getJSON(URL+'phpComunes/llenarCampos.php', {v0:idpersona}, function(dato){
					$.each(dato,function(i,f){
						var nombre=f.pnombre+" "+f.snombre+" "+f.papellido+" "+f.sapellido;
						$("#repLegal").html(nombre);
						$("#idRepLegal").val(f.identificacion);
					});
				});
			}
		 });
		  
 }//else
	});//end json 
}
//----------------------------------------ACTUALIZAR LIQUIDACION
function actualizarLiquidacion(){

 error=0;
     //CAMPOS DE LA LIQUIDACION
	   noVisita=$("#noVisita").val();
	   txtLiquidacion=$("#txtLiquidacion").val();
	   fechaLiq=$("#fechaLiq").val();
	   
	   //CAMPOS DEL APORTANTE
	   nit=$("#idempresa").val();
	   ibc=($("#ibc").val()==''?0:$("#ibc").val());
	   tipoAfiliacion=$("#tipoAfiliacion").val();
	   causal=$("#causal").val(); 
	   tipoNotificacion=$("#tipoNotificacion").val();
	   fechaNot=$("#fechaNot").val();
	   observacion=$("#observacion").val();
	   documentacion=$("#documentacion").val();
	  // usuario=$("#usuario").val();
	   promotor=$("#promotor").val();
       periodos=$("#array").val();
	   
	   //CAMPOS BASE
	   totalBase=parseFloat($("#totalBase").html());
	   sueldos=$(":text[name='base']:eq(0)").val();
	   integrales=$(":text[name='base']:eq(1)").val();
	   supernumerarios=$(":text[name='base']:eq(2)").val();
	   extras=$(":text[name='base']:eq(3)").val();
	   nocturnos=$(":text[name='base']:eq(4)").val();
	   antiguedad=$(":text[name='base']:eq(5)").val();
	   jornales=$(":text[name='base']:eq(6)").val();
	   comisiones=$(":text[name='base']:eq(7)").val();
	   vacaciones=$(":text[name='base']:eq(8)").val();
	   primaVacaciones=$(":text[name='base']:eq(9)").val();
	   bonificaciones=$(":text[name='base']:eq(10)").val();
	   primasExtras=$(":text[name='base']:eq(11)").val();
	   unidadTiempo=$(":text[name='base']:eq(12)").val();
	   viaticos=$(":text[name='base']:eq(13)").val();
	   remuneraciones=$(":text[name='base']:eq(14)").val();
	   contratosAgricolas=$(":text[name='base']:eq(15)").val();
	   primaServicios=$(":text[name='base']:eq(16)").val();
	   primaManejo=$(":text[name='base']:eq(17)").val();
	   gastos=$(":text[name='base']:eq(18)").val();
	   bonificacionServicios=$(":text[name='base']:eq(19)").val();
	   subsidioAlimentacion=$(":text[name='base']:eq(20)").val();
	   primaTecnica=$(":text[name='base']:eq(21)").val();
	   horasCatedra=$(":text[name='base']:eq(22)").val();
	   licencias=$(":text[name='base']:eq(23)").val();
	   indemnizaciones=$(":text[name='base']:eq(24)").val();
	   otras=$(":text[name='base']:eq(25)").val();
	   
	   //CAMPOS APORTES
	   indicador=$("#cboAportes").val();
	   aportes=$("#txtAportes").val();
	   aportesPagados=$("#txtAportesPag").val();
	   capital=$("#txtCapital").val();
	   tasaInteres=($("#tasaInteres").val()==''? 0 :parseFloat($("#tasaInteres").val()) );
	   mora=$("#txtInteres").val();
	   totalLiq=parseFloat($("#totalLiq").html());
       //VALIDAR CAMPOS OBLIGADOS
	   validarCampos();

 //Si no hay errores se actualiza la visita
 if(error==0){
    
	 //El orden esta de acuerdo a los campos de la base de datos
	 var datos={
		       v0:txtLiquidacion,
			   v1:noVisita,
			   v2:fechaLiq,
			   v3:nit,
			   v4:tipoAfiliacion,
			   v5:ibc,
			   v6:causal,
			   v7:tipoNotificacion,
			   v8:fechaNot,
			   v9:periodos,
			   v10:sueldos,
			   v11:integrales,
			   v12:supernumerarios,
			   v13:extras,
			   v14:nocturnos,
			   v15:antiguedad,
			   v16:jornales,
			   v17:comisiones,
			   v18:vacaciones,
			   v19:primaVacaciones,
			   v20:bonificaciones,
			   v21:primasExtras,
			   v22:unidadTiempo,
			   v23:viaticos,
			   v24:remuneraciones,
			   v25:contratosAgricolas,
			   v26:primaServicios,
			   v27:primaManejo,
			   v28:gastos,
			   v29:bonificacionServicios,
			   v30:subsidioAlimentacion,
			   v31:primaTecnica,
			   v32:horasCatedra,
			   v33:licencias,
			   v34:indemnizaciones,
			   v35:otras,
			   v36:0,
			   
			   v38:indicador,
			   v39:aportes,
			   v40:aportesPagados,
			   v41:capital,
			   v42:tasaInteres,
			   v43:mora,
			   
			   v45:observacion,
			   v46:documentacion,
			   v47:promotor
			  }
	
	
	$.getJSON("actualizarLiquidacion.php",datos,function(data){
	 if(data==0){
	 alert("No se pudo actualizar la liquidaci\u00F3n");
	 return false;
	 }
	 alert("Se actualiz\u00F3 la liquidaci\u00F3n No "+txtLiquidacion);	 
	 nuevo=0;
	 limpiarCampos();   
	});
  }//end if		 

}

function aforo(){
	var campo0=$("#txtLiquidacion").val();
	if(confirm("Esta seguro de generar AFORO a esta liduidacion?")==true){
		$.getJSON('guardarAforo.php', {v0:campo0}, function(datos){
			if(datos==0){
				alert("No se pudo actulizar el registro!");
			}else{
				var path=URL+'centroReportes/promotoria/reporte005.php?v0='+campo0;
				window.open(path, '_blank');
			}
		})
		
	}
}
/*
Ã� 	&Aacute; 	\u00C1
á 	&aacute; 	\u00E1
Ã‰ 	&Eacute; 	\u00C9
Ã© 	&eacute; 	\u00E9
Ã� 	&Iacute; 	\u00CD
Ã­ 	&iacute; 	\u00ED
Ó 	&Oacute; 	\u00D3
Ã³ 	&oacute; 	\u00F3
Ãš 	&Uacute; 	\u00DA
Ãº 	&uacute; 	\u00FA
Ãœ 	&Uuml;      \u00DC
Ã¼ 	&uuml; 	    \u00FC
á¹„ 	&Ntilde; 	\u00D1
Ã± 	&ntilde; 	\u00F1
*/ 
