
$(function(){
	buscarEmpresasConvenio(); 
	$("#divDatosConvenio").dialog({
		autoOpen:false,
		modal:true,
		width:900,
		title:"Datos acuerdo de pago",
		buttons:{
			"Cerrar acuerdo":function(){
				var saldo = parseInt(formatNumber($("#h4TotalSaldo").text()).replace(",",""));
				var totalAcuerDistr = parseInt(formatNumber($("#h4TotalAcuerDistr").text()).replace(",",""));
				var totalAcuerdo = parseInt(formatNumber($("#h4TotalAcuerdo").text()).replace(",",""));
				
				//Validar si el valor total del acuerdo esta distribuido
				if((totalAcuerDistr-totalAcuerdo)!=0){
					alert("El valor total del acuerdo de pago, falta distribuirlo");
				}else if( saldo != 0 ){
					alert("El acuerdo de pago tiene saldo");
				}else{
					var idConvenio = $("#hidIdConvenio").val(); 
					var motivoEstado = $("#txaMotivoEstado").val().trim();
					
					$("#txaMotivoEstado").removeClass("ui-state-error");
					if(!motivoEstado){$("#txaMotivoEstado").addClass("ui-state-error");return}
					
					if(cerrarConvenio( idConvenio, "C", motivoEstado )){
						$(this).dialog("close");
						alert("El convenio se cerro correctamente");
						window.location.reload();
					}else{
						alert("Error al cerrar el convenio");
					}
				}
			},
			"Inactivar acuerdo":function(){
				var idConvenio = $("#hidIdConvenio").val(); 
				var motivoEstado = $("#txaMotivoEstado").val().trim();
				
				$("#txaMotivoEstado").removeClass("ui-state-error");
				if(!motivoEstado){$("#txaMotivoEstado").addClass("ui-state-error");return}
				
				if(cerrarConvenio( idConvenio, "I", motivoEstado )){
					$(this).dialog("close");
					alert("El convenio se inactivo correctamente");
					window.location.reload();
				}else{
					alert("Error al inactivar el convenio");
				}
			}
		}
	});
});

function buscarEmpresasConvenio(){
	var htmlFilas = "";
	$.ajax({
		url:"buscarConvenio.php",
		type:"POST",
		data:{objDatosFiltro:{estado:"A"}},
		dataType:"json",
		async:false,
		success:function(datos){
			if( datos != 0 ){
				$.each(datos,function(i,row){
					if(row.estado=="A")
						htmlFilas += "<tr><td ><a href='#' style='color:blue;' onclick='buscarConvenio(" + row.idempresa + ", \" " + row.razonsocial+ "\", " + row.idconvenio + ")'>" 
								+ row.nit + "</a></td><td>" + row.razonsocial + "</td>"
								+ "<td>" + row.consecutivo + "</td></tr>";
				});
			}
		}
	});
	htmlFilas = ( htmlFilas == "") ? "<tr><td colspan='3'><h3>No existen convenios activos</h3></td></tr>" : htmlFilas ;
	$("#tablaEmpresa tbody").html(htmlFilas);
}

function buscarConvenio(idEmpresa,razonSocial, idConvenio){
	limpiarCampo("#h4TotalAcuerdo,#tblDatosConvenio tbody,#tdEmpresa,#hidIdConvenio");
	idConvenio = parseInt(idConvenio);
	$("#tdEmpresa").text(razonSocial);
	var htmlFilas = "";
	var totalAcuerdo = 0;
	var claSaldo = 0;
	var totalAcuerDistr = 0;
	//var arrayConsecutivo = new Array();
	$.ajax({
		url:"buscaFechaPeriodConve.php",
		type:"POST",
		data:{objDatosFiltro:{idempresa:idEmpresa,idconvenio:idConvenio}},
		dataType:"json",
		async:false,
		success:function(datos){
			if( datos != 0 ){
				//var periodos;
				//var valorPeriodo;
				$.each(datos.Fecha, function(i,row){
					//if( idConvenio == parseInt(row.idconvenio) ){
						//totalAcuerdo += (arrayConsecutivo.indexOf(row.consecutivo) < 0 ) ? parseInt(row.valor) : 0;
					totalAcuerdo = row.valor;
					//arrayConsecutivo[i] = row.consecutivo;
					var periodos = "",
						valorPeriodo = 0,
						abono = 0;
					
					$.each(datos.Periodo, function(j,rowPeriodo){
						if( rowPeriodo.iddetalleconvenio == row.iddetalleconvenio ){
							periodos += rowPeriodo.periodo + " - ";
							valorPeriodo += parseInt(rowPeriodo.valor);
							abono += (rowPeriodo.abono==0)?0:parseInt(rowPeriodo.abono);	
						}
					});
					var saldoPeriodo = valorPeriodo-abono;
					var abonoInteres = (row.abonointeres==0)?0:parseInt(row.abonointeres);
					var saldoInteres = parseInt(row.interes)-abonoInteres;
					var saldo = saldoPeriodo+saldoInteres;
					
					totalAcuerDistr += parseInt(row.interes) + valorPeriodo;
					claSaldo += saldo;
					
					htmlFilas += "<tr><td>" + row.consecutivo + "</td>" +
							"<td>" + row.fechapago + "</td>" +
							"<td class='claNumero'>" + formatCurrency(row.interes) + "</td>" +
							"<td class='claNumero'>" + formatCurrency(abonoInteres) + "</td>" +
							"<td>" + periodos + "</td>" +
							"<td class='claNumero'>" + formatCurrency(valorPeriodo) + "</td>" +
							"<td class='claNumero'>" + formatCurrency(abono) + "</td>" +
							"<td class='claNumero claSaldo' >" + (formatCurrency( saldo.toString() ) ) + "</td></tr>";
					//}
				});	
			}
		}
	});
	
	$("#hidIdConvenio").val(idConvenio);
	$("#h4TotalAcuerDistr").text(formatCurrency(totalAcuerDistr));
	$("#h4TotalAcuerdo").text(formatCurrency(totalAcuerdo));
	$("#h4TotalSaldo").text(formatCurrency(claSaldo));
	$("#tblDatosConvenio tbody").html(htmlFilas);
	$("#divDatosConvenio").dialog("open");
}

function cerrarConvenio(idConvenio, estado, motivoEstado){
	
	var retorno = false;
	$.ajax({
		url:"actualizarEstadoConvenio.php",
		type:"POST",
		data:{idConvenio:idConvenio,estado:estado, motivoEstado:motivoEstado},
		dataType:"json",
		async:false,
		success:function(datos){
			if(parseInt(datos)>0){
				retorno = true;
			}
		}
	});
	return retorno;
}

function limpiarCampo( classCampo ){
	var usuario = $("#txtUsuario").val();
	var tipoCampo = "";
	$( classCampo ).each(function(i,row){
		
		if( $( this ).get(0).name == "base" ){
			$( this ).val(0);
			return;
		}
		tipoCampo = $( this ).get(0).type;
		
		if( typeof tipoCampo === "undefined" )
			tipoCampo = $( this ).get(0).localName;
		
		if( tipoCampo == "select-one" )
			$( this ).val(0);
		else if( tipoCampo == "text" || tipoCampo == "textarea" || tipoCampo == "hidden" )
			$( this ).val("");
		else if( tipoCampo == "div" || tipoCampo == "td" || tipoCampo == "span" )
			$( this ).text("");
		else if( tipoCampo == "checkbox" )
			$( this ).attr("checked",false);
		else if( tipoCampo == "table" )
			$( this ).html("");
	});	
}