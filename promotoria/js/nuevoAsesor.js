//Variables generales
var URL=src();
var nuevo=0;

$(document).ready(function(){
	
//Activo los detapicker en el formulario
$("#fecFinal").datepicker({ showAnim:'slideDown',changeMonth: true });	
	
//Dialog ayuda
$("#ayuda").dialog({
		 	      autoOpen: false,
			      height: 450,
			      width: 700,
			      modal:false,
			      open: function(evt, ui){
					$('#ayuda').html('');
						$.get(URL +'help/promotoria/manual nuevoAsesor.html',function(data){
							$('#ayuda').html(data);
					});
			      }
		});
		
//Dialog Colaboracion en linea
$("#dialog-form2").dialog({
		autoOpen: false,
		height: 400,
		width: 500,
		draggable:false,
		modal: true,
		buttons: {
		    'Enviar': function() {
			var bValid = true;
			var campo=$('#notas').val();
			var campo0=$.trim(campo);
			if (campo0==""){
				$(this).dialog('close');
					return false;
				}
			var campo1=$('#usuario').val();
			var campo2="nuevoAsesor.php";
			$.post(URL+'phpComunes/colaboracion.php',{v0:campo0,v1:campo1,v2:campo2},
			function(datos){
				if(datos=='1'){
					alert("Su comentario fue enviado correctamente!. Gracias por participar en el mejoramiento de SIGAS");
				}else{
					alert(datos);
				}
		});
		$(this).dialog('close');
		},
		Cancelar: function() {
			$(this).dialog('close');
			$("#dialog-form2").dialog("close");
		}
	}
	});

});//fn ready
//-----------------------------------NUEVO PROMOTOR
function nuevoAsesor(){
   limpiarCampos();
   nuevo=1;
   $("#bGuardar").show();
   $("#iconActualizar").hide();
}
//---------------------------------------LIMPIAR CAMPOS
function limpiarCampos(){
	$("table.tablero input:text:not('#usuario,#fecInicio'),table.tablero textarea,:hidden").val('');
	$("table.tablero select").val(0);
	$("#tNombre").html('');
	$("table.tablero td input:text.ui-state-error,select.ui-state-error").removeClass("ui-state-error");
	$("#tipoI").focus();
}
//-----------------------------------GUARDAR ACTA DE VISITA

function guardarAsesor(){
	$("#bGuardar").hide();
	if(nuevo==0){
		$("#bGuardar").show();
		alert("Haga click primero en Nuevo");
		return false;
	}
	var error = validaCampoFormu();
	if( parseInt(error)>0 ){
		return false;
	}
	
	var idpersona = $("#idpersona").val();
	if( idpersona == 0 ){
		alert("Falta datos del asesor");
		return false;
	}
	
	var idTipoAsesor=$("#cmbTipoAsesor").val();
	var fecInicio=$("#fecInicio").val();
	var fecFinal=$("#fecFinal").val();
	var estado=$("#estado").val();
	var usuario=$("#usuario").val(); 

	//Si no hay errores se guarda el asesor
	if(error==0){ 
		//GUARDAR LOS DATOS
		$.ajax({
			url:'guardarAsesor.php', 
			type:"POST",
			data:{v0:idpersona,v1:fecInicio,v2:fecFinal,v3:estado,v4:idTipoAsesor}, 
			dataType:"json",
			async:false,
			success:function(data){
				if( data==0 ){
					alert("No se guardo el promotor.");	
				}else if( data == 1 ){
					alert("Se guardo el asesor satisfactoriamente.");	
					nuevoAsesor();
				}
			}
		});   
	}//end if		 
}

function actualizarAsesor(){
	var error = validaCampoFormu();
	if( parseInt(error)>0 ){
		return false;
	}
	var idAsesor = $("#hidIdAsesor").val();
	if( idAsesor == 0 ){
		alert("Falta datos del asesor");
		return false;
	}
	var idTipoAsesor=$("#cmbTipoAsesor").val();
	var fecInicio=$("#fecInicio").val();
	var fecFinal=$("#fecFinal").val();
	var estado=$("#estado").val(); 
	$.ajax({
		url:'actualizarAsesor.php', 
		type:"POST",
		data:{v0:idAsesor,v1:idTipoAsesor,v2:fecInicio,v3:fecFinal,v4:estado}, 
		dataType:"json",
		async:false,
		success:function(data){
			if( data == 0 ){
				alert("No se actualizo el asesor.");	
			}else if( data == 1 ){
				alert("Se actualizo el asesor satisfactoriamente.");	
				nuevoAsesor();
			}
		}
	});	 
}

//----------------------------------------BUSCAR LA PERSONA
function buscarPersona(obj){

	if($("#tipoI").val()==0){
		$("#tipoI").addClass("ui-state-error");
		return false;
	}

//	Cedula del funcionario
	var ide=$.trim(obj.value);

//	Limpiamos campos
	$("#tNombre").html("");

//	TIPO  1=promotor 2=asesor
	if(nuevo==0){
		alert("Si es un registro nuevo haga click en NUEVO!!");
		return;
	}else{
		if(ide==''){return false;}

		$.ajax({
			url:URL+'phpComunes/buscarPersona2.php',
			type:"POST",
			data:{v0:$("#tipoI").val(),v1:ide},
			dataType:"json",
			async:false,
			success:function(data){
			
				if(data==0){
					alert("NO se encontraron resultados.");
					return false;
				}
				//Recuperamos los datos de la persona
				var nombre;
				$.each(data,function(i,fila){
					nombre=$.trim(fila.pnombre)+" "+$.trim(fila.snombre)+" "+$.trim(fila.papellido)+" "+$.trim(fila.sapellido);
					idpersona=fila.idpersona;
				});//each
				
				//Verifica si existe el promotor
				var datosAsesor = buscarAsesor({idpersona:idpersona});
				if(datosAsesor){
					if(confirm("Ya existe un asesor con esa identificaci\u00F3n. \n Deseas modificar el asesor")){
						datosAsesor = datosAsesor[0];
						$("#bGuardar").hide();
						$("#iconActualizar").show();
						
						$("#tNombre").text( nombre );
						$("#cmbTipoAsesor").val( datosAsesor.idtipoasesor );
						$("#fecInicio").val( datosAsesor.fechainicio );
						$("#fecFinal").val( datosAsesor.fechafinal );
						$("#estado").val( datosAsesor.estado );
						$("#hidIdAsesor").val( datosAsesor.idasesor );
					}else{					
						limpiarCampos();
					}
				}else{
					$("#tNombre").html(nombre);
					$("#idpersona").val(idpersona);//campo oculto
					$("#estado").val(estado);
				}
			}
		});//end json
	}  
}

/**
 * Metodo para buscar el asesor
 * @param idPersona
 * @param idTipoAsesor
 * @returns multitype:array|null
 */
function buscarAsesor(objDatosFiltro){
	var resultado = null;
	$.ajax({
		url:'buscarAsesor.php',
		type:"POST",
		data:{objDatosFiltro:objDatosFiltro},
		dataType:"json",
		async:false,
		success:function(data){
			if(typeof data == "object" && data.length>0){
				resultado = data;
			}
		}
	});
	return resultado;
}

function validaCampoFormu(){
	var banderaError = 0;
	$( ".ui-state-error" ).removeClass( "ui-state-error" );
	$(".clsValidaText").each(function(i,row){
		if( $( this ).val() == 0 ){
			$( this ).addClass( "ui-state-error" );
			banderaError++;
		}
	});
	return banderaError;
}
//----------------------------------------VENTANA DE DIALOGO DE AYUDA
function mostrarAyuda(){
	$("#ayuda").dialog('open');
}
//----------------------------------------VENTANA DE DIALOGO DE MANUAL
function notas(){
	$("#dialog-form2").dialog('open');
}	
