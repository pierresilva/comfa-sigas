var uri = "",
	uriLogCausalAjuste = "",
	objAccion = {SELECT:"S", INSERT:"I", UPDATE:"U"};

$(function(){
	uri = src();
	uriLogCausalAjuste = uri + "promotoria/causalAjuste.log.php";
	
	$("#txtCodigo").blur(contrCausalAjuste);
});

function nuevo(){
	limpiarCampos2("#txtCodigo,#hidIdCausalAjuste,#txtNombre,#cmbEstado,#txaDescripcion");
}


function fetchCausalAjuste(accion,objFiltro){
	var resultado = null;
	
	$.ajax({
		url:uriLogCausalAjuste,
		type:"POST",
		data:{accion:accion,objDatosFiltro:objFiltro},
		dataType: "json",
		async:false,
		success:function(data){
			resultado = data;
		}
	});
	
	return resultado;
}

function contrCausalAjuste(){
	var codigoCausal = $("#txtCodigo").val().trim();
	
	if(!codigoCausal) return false;
	
	var objFiltro = {codigo:codigoCausal};
	var arrDatos = fetchCausalAjuste(objAccion.SELECT, objFiltro);
	
	if(arrDatos.data && typeof arrDatos.data == "object" && arrDatos.data.length>0){
		
		if(!confirm("La definicion ya existe, desea modificarla!!")){
			nuevo();
			return false;
		}
		
		arrDatos = arrDatos.data[0];
		
		$("#hidIdCausalAjuste").val(arrDatos.id_causal_ajuste);
		$("#txtCodigo").val(arrDatos.codigo);
		$("#txtNombre").val(arrDatos.nombre);
		$("#txaDescripcion").val(arrDatos.descripcion);
		$("#cmbEstado").val(arrDatos.estado);
	}
}

function guardar(){
	
	//Verificar campos obligatorios
	if(validateElementsRequired()) return false;
	
	var objDatos = {
			id_causal_ajuste: $("#hidIdCausalAjuste").val().trim()
			, codigo: $("#txtCodigo").val().trim()
			, nombre: $("#txtNombre").val().trim()
			, estado: $("#cmbEstado").val()
			, descripcion: $("#txaDescripcion").val().trim()
	};
	
	var accion = objDatos.id_causal_ajuste?objAccion.UPDATE:objAccion.INSERT;
	
	$.ajax({
		url:uriLogCausalAjuste,
		type:"POST",
		data:{accion:accion,datos:objDatos},
		dataType: "json",
		async:false,
		success:function(data){
			if(data.error==0){
				alert("La causal del ajuste se guardo correctamente!!");
				nuevo();
				return true;
				
			}
			
			alert("ERROR al guardar la causal del ajuste!!");
		}
	});
}
