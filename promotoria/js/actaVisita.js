//Variables generales
var URL=src()
	, nuevo=0
	, nombrePromotor, idPromotor
	, urlDepartamento
	, urlPersona
	, idPais = 48 //[48][COLOMBIA]
	, urlVerifPermiCierr = "verifPermiCierr.php"
	, urlActualizarEmpresa = "actualizarEmpresa.php";

$(document).ready(function(){
	
	urlDepartamento = URL+"phpComunes/departamentos.php"; 
	urlPersona = URL+"phpComunes/buscarPersona.php"; 
	
	/*$( "#txtFechaVisita" ).datepicker({
		showAnim:'slideDown',
		maxDate: "+0D",
		changeMonth: true 
	});*/
	
	/*$( "#fechaPago" ).datepicker({
		showAnim:'slideDown',
		changeMonth: true
	});*/
	//Activo los detapicker en el formulario
	datepickerC("FECHA","#txtFechaVisita");
	datepickerC("FECHA","#txtFechaApelacion");
	
	//Control del numero de identificacion del representante legal
	$("#txtIdentRepreLegal").blur(ctrIdentRepreLegal);
	
	//Adicionar el promotor
	addPromotorFormulario();
	
	//Adicionar los asesores juridicos
	addAsesoresJuridicos();
	
	//Control de la ciudad de la empresa
	ctrCiudadEmpresa();
	
	//Control de la fecha de apelacion
	$(":radio[name='radApelacion']").change(ctrFechaApelacion);
	ctrFechaApelacion();
	
	//Dialog Tabla visitas
	$("#divVisitas").dialog({
		autoOpen: false,
		width: 900,
		modal:true,
		open: function(){
			//Limpiar Tabla 
			$("#tblVisitas tbody tr").remove();

			var promotor="";
			var objDatosVisita = buscarDatosVisita({idempresa:$("#hidIdEmpresa").val(),estado:"A"});
			if(typeof objDatosVisita == "object" && objDatosVisita.length>0){
				
				var datosPromotor,
					bandera = 0,
					htmlVisitas = "";
				
				$.each(objDatosVisita,function(i,fila){
						
						//Obtener los datos del promotor de la visita
						var objDatosPromotor = buscarPromotor({idpromotor:fila.idpromotor});
						if(typeof objDatosPromotor == "object" && objDatosPromotor.length>0){
							objDatosPromotor = objDatosPromotor[0];
							
							var promotor = objDatosPromotor.pnombre+" "+objDatosPromotor.papellido;
							$("#h5DatosEmpreVisit").html($("#divEmpresa").html()+" - "+$("#txtNit").val());
							
							htmlVisitas += "<tr><td><a href='#' style='color:blue;' onClick='buscarVisita(this.text)'>"+fila.idvisita+"</a></td>"+
									"<td>"+fila.fechavisita+"</td>"+
									"<td>"+promotor+"</td>"+
									"<td>"+fila.conclusion+"</td>"+
									"<td>"+fila.novedad+"</td>"+"</tr>"
							
						}else{
							alert("Error al cargar los datos del promotor!!");
							$("#divVisitas").dialog("close");
							nuevaVisita();
						}
				});
				
				if(htmlVisitas!=""){
					$("#tblVisitas tbody").html(htmlVisitas);
				}else{
					alert("Error al cargar los datos de la visita!!");
					nuevaVisita();
				}
				
			}else{
				$("#tblVisitas").append("<tr><td colspan='6' class='Rojo' align='center'>No hay historial de visitas Activas!!</td></tr>");
				nuevaVisita();
			}
		},
		buttons:{
			"IMPRIMIR":function(){
				if( parseInt( $("#tblVisitas tbody td").length ) > 1 ){
					var idEmpresa = $("#hidIdEmpresa").val();
					var usuario = $("#txtUsuario").val();
					var url="http://"+getCookie("URL_Reportes")+"/promotoria/reporte001.jsp?v0="+idEmpresa+"&v1="+usuario;
					window.open(url,"_NEW");
				}else{
					alert("No hay Registros");
				}
			}
		}	  
	});//dialog
});//fin ready

/* 
* CONTROL DE VALIDACIONES DE PROMOTORIA
*/

// funcion para obtener cookie en javascript
	function getCookie(c_name)
	{
		var i,x,y,ARRcookies=document.cookie.split(";");
		for (i=0;i<ARRcookies.length;i++)
		{
  			x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
  			y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
  			x=x.replace(/^\s+|\s+$/g,"");
  			if (x==c_name)
    			{
    				return unescape(y);
    			}// fin if
		}// fin for
  	}// fin getCookie
//funcion para obtener cookie en javascript
	


/**********************************************************
 * METODOS PARA EL CONTROL DE LOS ELEMENTOS DEL FORMULARIO 
 * ********************************************************/

/**
 * METODO: Adiciona el promotor al formulario de acuerdo al usuario 
 */
function addPromotorFormulario(){
	//Obtenemos el id del promotor
	idPromotor = $("#hidIdPromotor").val();
	nombrePromotor = $("#divPromotor").text().trim(); 
	if( idPromotor == 0 ){
		alert( "El usuario " + $("#txtUsuario").val() + " no es promotor o esta inactivo..." );
	}
}

/**
 * METODO: Adiciona los asesores juridicos al formulario 
 */
function addAsesoresJuridicos(){
	//Buscar los asesores
	var datosAsesor = buscarDatosAsesor({estado:"A"}); 
	
	if(typeof datosAsesor == "object" && datosAsesor.length>0){
		var cmbAsesoJuridInter = "";
		var cmbAsesoJuridExter = "";
		$.each(datosAsesor,function(i,row){
			if(row.idtipoasesor==4225){ //[4225][ASESOR JURIDICO INTERNO]
				cmbAsesoJuridInter += "<option value='"+row.idasesor+"'>"+row.asesor+"</option>";
			}else if(row.idtipoasesor==4226){ //[4226][ASESOR JURIDICO EXTERNO]
				cmbAsesoJuridExter += "<option value='"+row.idasesor+"'>"+row.asesor+"</option>";;
			}
		});
		
		//Adicionar Asesor juridico interno al formulario
		if(cmbAsesoJuridInter!="")
			$("#cmbIdAsesoJuridInter").append(cmbAsesoJuridInter);
		if(cmbAsesoJuridExter!="")
			$("#cmbIdAsesoJuridExter").append(cmbAsesoJuridExter);
	}
}

function ctrIdentRepreLegal(){
	var numeroIdentificacion = $("#txtIdentRepreLegal").val();
	
	$("#divRepresentanteLegal").html("");
	$("#hidIdRepreLegal").val("");
	if(!numeroIdentificacion) return false;
	
	var objDataPersona = fetchPersona(numeroIdentificacion);
	
	if(objDataPersona && typeof objDataPersona == "object"){
		objDataPersona = objDataPersona[0];
		
		
		$("#hidIdRepreLegal").val(objDataPersona.idpersona);
		$("#divRepresentanteLegal").html(objDataPersona.nombrecorto);
	}else{
		alert("No existen datos!!");
		$("#txtIdentRepreLegal").val("");
	}
}

function ctrCiudadEmpresa(){
	var dataDepartamento =  fetchDepartamento(idPais);
	var htmlCombo = "";
	htmlCombo += dataDepartamento;
	
	/*$.each(dataDepartamento,function(i,row){
		
	});*/
	
	$("#cboDepto2").html(htmlCombo);
	
}

function ctrFechaApelacion(){
	var apelacion = $(":radio[name='radApelacion']:checked").val();
	$("#txtFechaApelacion").removeClass("element-required").attr("disabled",true).val("");
	$(".clsFechaApelacion").hide();
	if(apelacion=="S"){
		$("#txtFechaApelacion").addClass("element-required").attr("disabled",false);
		$(".clsFechaApelacion").show();
	}
}

function fetchPersona(numeroIdentificacion){
	var resultado = null;
	
	$.ajax({
		url:urlPersona,
		type:"post",
		data:{v0:numeroIdentificacion},
		dataType:"json",
		async:false,
		success:function(data){
			resultado = data;
		}
	});
	
	return resultado;
}

function fetchDepartamento(idPais){
	var resultado = null;
	
	$.ajax({
		url:urlDepartamento,
		type:"post",
		data:{elegido:idPais},
		/*dataType:"json",*/
		async:false,
		success:function(data){
			resultado = data;
		}
	});
	
	return resultado;
}

/**
 * 
 * @returns {Boolean}
 */
function buscarEmpresa(){
	var nit=$("#txtNit").val().trim();
	
	//Nueva visita
	nuevaVisita();
	
	if(nit==0)return false;
	
	
	$("#txtNit").val(nit);
	
	var objDatosEmpresa = buscarDatosEmpresa(nit);
	if(typeof objDatosEmpresa == "object" && objDatosEmpresa.length>0){
		objDatosEmpresa = objDatosEmpresa[0];
		//Asiganar los datos de la empresa al formulario
		
		var nombreRepLegal = objDatosEmpresa.pnr + " " + objDatosEmpresa.snr + " " + objDatosEmpresa.par + " " + objDatosEmpresa.sar;
		$("#hidIdEmpresa").val(objDatosEmpresa.idempresa);//campo oculto
		$("#divEmpresa").html(objDatosEmpresa.razonsocial);
		$("#divEstado").html(objDatosEmpresa.estado);
		$("#txtDireccionEmpresa").val(objDatosEmpresa.direcorresp);
		$("#hidDireccionEmpresa").val(objDatosEmpresa.direcorresp);
		$("#txtTelefonoEmpresa").val(objDatosEmpresa.telefono);
		$("#hidTelefonoEmpresa").val(objDatosEmpresa.telefono);
		$("#cboDepto2").val(objDatosEmpresa.iddepcorresp).trigger("change");
		$("#hidDepartamentoEmpresa").val(objDatosEmpresa.iddepcorresp);
		$("#cboCiudad2").val(objDatosEmpresa.idciucorresp);
		$("#hidCiudadEmpresa").val(objDatosEmpresa.idciucorresp);
		$("#divRepresentanteLegal").html( nombreRepLegal );
		$("#txtIdentRepreLegal").val(objDatosEmpresa.identificacionrpl);
		$("#hidIdRepreLegal").val(objDatosEmpresa.idrepresentante);
		$("#hidIdRepreLegalInici").val(objDatosEmpresa.idrepresentante);
		
	}else{
		alert("La empresa no existe, lo siento!");
		//limpiarCampo();
	}
	$("#txtIdVisita").focus();
}		

/**
 * METODO: Verifica si la visita ya existe
 */
function verificarVisita(){
	var idVisita = $("#txtIdVisita").val().trim();
	if(idVisita=="") return false;
	
	//Verifica si el numero de la visita es valido
	if(idVisita==0){
		alert("El n\u00FAmero de la visita no es valido!!");
		$("#txtIdVisita").val('').focus();
		return false;
	}
	
	var objDatosVisita = buscarDatosVisita({idvisita:idVisita});
	if(typeof objDatosVisita == "object" && objDatosVisita.length>0){
		objDatosVisita = objDatosVisita[0];
		
		var objDatosEmpresa = buscarDatosEmpresaId(objDatosVisita.idempresa);

		if(typeof objDatosEmpresa == "object" && objDatosEmpresa.length>0){
			objDatosEmpresa = objDatosEmpresa[0];
			
			alert("Ya existe una visita con ese n\u00FAmero:\n\n -NIT: "+objDatosEmpresa.nit+" \n\n -RAZON SOCIAL: "+objDatosEmpresa.razonsocial);
		}else{
			alert("Ya existe una visita, pero la empresa no se encuentra en sigas.");
		}
		$("#txtIdVisita").val('').focus();	
	}
}

/**
 * METODO: Buscar
 * @param idVisita
 */
function buscarVisita(idVisita){
	
	//Verificar si ya existe la visita
	var objDatosVisita = buscarDatosVisita({idvisita:idVisita});
	if(typeof objDatosVisita == "object" && objDatosVisita.length>0){
		
		//Limpiamos los datos de la visita
		$("#txtIdVisita,#txtFechaVisita,#txaConclusion,#txaObservacion").val('');
		$("#cmbCausal,#cmbIdAsesoJuridInter,#cmbIdAgencia,#cmbIdAsesoJuridExter").val(0);
		$("#hidIdPromotor").val(idPromotor);
		$("#divPromotor").val(nombrePromotor);
		
		var promotor = "";
		var idpromotor = 0;
		
		objDatosVisita = objDatosVisita[0];
	
		// ### Cargar los datos para actualizar
		//Control de los botones
		$("#iconActualizar").show();
		$("#bGuardar").hide();
		
		var objDatosPromotor = buscarPromotor( {idpromotor:objDatosVisita.idpromotor} );
		if(typeof objDatosPromotor == "object" && objDatosPromotor.length>0){
			objDatosPromotor = objDatosPromotor[0];
			var promotor = objDatosPromotor.pnombre + " " + objDatosPromotor.snombre + " " + objDatosPromotor.papellido + " " + objDatosPromotor.sapellido;
			
			//Asignar los datos al formulario
			$("#txtIdVisita").val(objDatosVisita.idvisita).attr("disabled",true);
			$("#txtFechaVisita").val(objDatosVisita.fechavisita);
			$("#hidFechaVisita").val(objDatosVisita.fechavisita);
			$("#cmbCausal").val(objDatosVisita.idcausal);
			$(":radio[name='radLiquidacion'][value='"+objDatosVisita.liquidacion+"']").attr('checked',true);
			$(":radio[name='radApelacion'][value='"+objDatosVisita.apelacion+"']").attr('checked',true);
			$("#txtFechaApelacion").val(objDatosVisita.fecha_apelacion);			
			$("#cmbIdAsesoJuridInter").val(objDatosVisita.idabogadointerno);
			$("#cmbIdAgencia").val(objDatosVisita.idagencia);
			$("#cmbIdAsesoJuridExter").val(objDatosVisita.idabogadoexterno);
			$("#txaConclusion").val(objDatosVisita.conclusion);
			$("#txaObservacion").val(objDatosVisita.novedad);
			$("#hidIdPromotor").val(objDatosVisita.idpromotor);
			$("#divPromotor").text(promotor);
			
		}else{
			alert("Error al obtener los datos del promotor!!");
		}
	}else{
		alert("Error al obtener los datos de la visita!!");
	}
	$("#divVisitas").dialog("close");
}

/**
 * METODO: Prepara para consultar las visitas de la empresa
 * @returns {Boolean}
 */
function visitasEmpresa(){
	
	var nit=$("#txtNit").val();
	var idEmpresa=$("#hidIdEmpresa").val();
	
	$("#txtNit").removeClass("ui-state-error");
	
	if(nit==0){
		$("#txtNit").addClass("ui-state-error");
	}else if(idEmpresa==0){
		alert("Falta cargar los datos de la empresa!!");		
	}else{
		$("#divVisitas").dialog("open");
	}
}

function nuevaVisita(){
	var usuario = $("#txtUsuario").val();
	
	limpiarCampos2("table.tablero input:not('#txtUsuario','#hidIdUsuario','#hidIdPromotor',':radio')" +
			",table.tablero textarea,table.tablero select" +
			",#divEmpresa,#divEstado,#divDireccion,#divTelefono,#divCiudad,#divRepresentanteLegal,#txtIdentRepreLegal,#hidIdRepreLegal,#hidIdRepreLegalInici");
	
	$("#txtUsuario").val( usuario );
	$(".ui-state-error").removeClass("ui-state-error");
	
	$("table.tablero :radio[value='N']").attr('checked',true);	
	
	$("#txtIdVisita").attr("disabled",false);
	
	$("#iconActualizar").hide();
	$("#bGuardar").show();
	
	$("#hidIdPromotor").val(idPromotor);
	$("#divPromotor").text(nombrePromotor);
}
/*
function limpiarCampo( classCampo ){
	var usuario = $("#txtUsuario").val();
	var tipoCampo = "";
	$( classCampo ).each(function(i,row){
		
		if( $( this ).get(0).name == "base" ){
			$( this ).val(0);
			return;
		}
		tipoCampo = $( this ).get(0).type;
		
		if( typeof tipoCampo === "undefined" )
			tipoCampo = $( this ).get(0).localName;
		
		if( tipoCampo == "select-one" )
			$( this ).val(0);
		else if( tipoCampo == "text" || tipoCampo == "textarea" || tipoCampo == "hidden" )
			$( this ).val("");
		else if( tipoCampo == "div" || tipoCampo == "td" || tipoCampo == "span" )
			$( this ).text("");
		else if( tipoCampo == "checkbox" )
			$( this ).attr("checked",false);
	});	
	$("#txtUsuario").val( usuario );
}*/

/**
 * METODO: Valida los elementos requeridos del formulario
 * 
 * @returns int Numero de elementos vacios
 */
/*function validaCampoFormu(){
	//En este metodo no se validan los periodos a eliminar o actualizar
	
	var banderaError = 0;
	$( ".ui-state-error" ).removeClass( "ui-state-error" );
	$(".element-required").each(function(i,row){
		//Verifica que los campos esten visibles porque pueden ser periodos eliminados
		if($( this ).val() == 0 && $( this ).is(":visible")){
			$( this ).addClass( "ui-state-error" );
			banderaError++;
		}
	});
	return banderaError;
}*/

/******************************
 * METODOS PARA LAS PETICIONES
 * ****************************/

function buscarDatosAsesor(objDatosFiltro){
	var datosRetorno = "";	
	$.ajax({
		url:"buscarAsesor.php",
		type:"POST",
		data:{objDatosFiltro:objDatosFiltro},
		dataType:"json",
		async:false,
		success:function(datos){
			datosRetorno = datos;
		}
	});
	return datosRetorno;
}

function buscarDatosEmpresa(nit){
	var datosRetorno = "";	
	$.ajax({
		url:URL+'phpComunes/buscarEmpresaCompleta.php',
		type:"GET",
		data:{v0:nit},
		dataType:"json",
		async:false,
		success:function(datos){
			datosRetorno = datos;
		}
	});
	return datosRetorno;
}

/**
 * METODO: Buscar los datos de la empresa por el id
 * 
 * @param idEmpresa
 */
function buscarDatosEmpresaId(idEmpresa){
	var datosRetorno = "";	
	$.ajax({
		url:URL+'phpComunes/buscarEmpresaId.php',
		type:"GET",
		data:{v0:idEmpresa},
		dataType:"json",
		async:false,
		success:function(datos){
			datosRetorno = datos;
		}
	});
	return datosRetorno;
}

/**
 * METODO: Buscar los datos de la visita
 * 
 * @param objDatosFiltro
 * @returns {Array}
 */
function buscarDatosVisita(objDatosFiltro){
	var datosRetorno = "";	
	$.ajax({
		url:"buscarVisita.php",
		type:"POST",
		data:{objDatosFiltro:objDatosFiltro},
		dataType:"json",
		async:false,
		success:function(datos){
			datosRetorno = datos;
		}
	});
	return datosRetorno;
}

function buscarPromotor(objDatosFiltro){		
	var datos ;
	$.ajax({
		url:"buscarPromotorVisita.php",
		type:"GET",
		data:{objDatosFiltro:objDatosFiltro},
		dataType:"json",
		async:false,
		success:function(data){
			datos = data;
		}
	});
	return datos;
}

function fetchPermisoCierre(objFiltro){
	var resultado = null;
	
	$.ajax({
		url: urlVerifPermiCierr,
		type:"POST",
		data: {objDatosFiltro:objFiltro},
		dataType:"json",
		async: false,
		success: function(data){
			resultado = data;
		}
	});
	
	return resultado;
}

function obtenerDatos(){
	var objDatosVisita = {
			idvisita:$("#txtIdVisita").val().trim(),
			fechavisita:$("#txtFechaVisita").val(),
			idempresa:$("#hidIdEmpresa").val(),
			conclusion:$("#txaConclusion").val().trim(),
			novedad:$("#txaObservacion").val().trim(),
			idpromotor:$("#hidIdPromotor").val(),
			idcausal:$("#cmbCausal").val(),
			idabogadoexterno:$("#cmbIdAsesoJuridExter").val(),
			idabogadointerno:$("#cmbIdAsesoJuridInter").val(),
			idagencia:$("#cmbIdAgencia").val(),
			liquidacion:$("input:radio[name='radLiquidacion']:checked").val(),
			apelacion:$("input:radio[name='radApelacion']:checked").val(),
			fecha_apelacion: $("#txtFechaApelacion").val()
			
	};
	
	objDatosEmpresa = {
			idempresa:$("#hidIdEmpresa").val().trim(),
			direcorresp:$("#txtDireccionEmpresa").val().trim(),
			telefono:$("#txtTelefonoEmpresa").val().trim(),
			iddepcorresp:$("#cboDepto2").val().trim(),
			idciucorresp:$("#cboCiudad2").val().trim(),
			idrepresentante:$("#hidIdRepreLegal").val().trim(),
			observacion:"actualiza datos",
			bandera_actualizar:'N'
	};
	
	//Verificar si el usuario actualizo los datos de la empresa
	if(objDatosEmpresa.direcorresp!=$("#hidDireccionEmpresa").val().trim()
			|| objDatosEmpresa.telefono!=$("#hidTelefonoEmpresa").val().trim()
			|| objDatosEmpresa.iddepcorresp!=$("#hidDepartamentoEmpresa").val().trim()
			|| objDatosEmpresa.idciucorresp!=$("#hidCiudadEmpresa").val().trim()
			|| objDatosEmpresa.idrepresentante!=$("#hidIdRepreLegalInici").val().trim()
			){
		objDatosEmpresa.bandera_actualizar = 'S';
	}
	
	return [objDatosVisita,objDatosEmpresa];
}

//-----------------------------------GUARDAR ACTA DE VISITA
function guardarVisita(){
	
	if(validateElementsRequired()>0)return false;
	
	//Verificar permisos del cierre
	var objFiltro = {
			usuario: $("#txtUsuario").val().trim() 
			, fecha: $("#txtFechaVisita").val().trim()
		};
	if(fetchPermisoCierre(objFiltro)==0){
		alert("La fecha de la visita no corresponde al cierre y el usuario no tiene permisos. \n Por este motivo no se guardo la informacion");
		return false;
	}
	
	var arrDatos = obtenerDatos();
	var objDatosVisita = arrDatos[0];
	var objDatosEmpresa = arrDatos[1];
	
	//Actualizar los datos de la empresa
	if(objDatosEmpresa.bandera_actualizar=='S' && !actualizarEmpresa(objDatosEmpresa)){
		alert("Los datos de la empresa no se pueden actualizar!!. Por lo tanto la visita no se puede guardar");
		return false;
	}
	
	$.ajax({
		url:"guardarVisita.php",
		type:"POST",
		data:{objDatosVisita:objDatosVisita},
		dataType:"json",
		async:false,
		success:function(data){
			if(data==0){
				alert("No fue posible guardar la visita!!");
				return false;
			}
			alert("Se guardo la visita No "+objDatosVisita.idvisita);	
			if(objDatosVisita.liquidacion=="S")
				location.href="liquidaciones.php?idv="+objDatosVisita.idvisita;
			else
				nuevaVisita();   
		}
	}); 
}

/**
 * METODO: Actualiza la visita
 * @returns {Boolean}
 */
function actualizarVisita(){
	if(validateElementsRequired()>0)return false;
	
	//Verificar permisos del cierre
	var objFiltro = {
			usuario: $("#txtUsuario").val().trim() 
			, fecha: $("#hidFechaVisita").val().trim()
		};
	if(fetchPermisoCierre(objFiltro)==0){
		alert("La fecha de la visita no corresponde al cierre y el usuario no tiene permisos. \n Por este motivo no se guardo la informacion");
		return false;
	}
	
	var arrDatos = obtenerDatos();
	var objDatosVisita = arrDatos[0]; 
	var objDatosEmpresa = arrDatos[1];
	
	//Actualizar los datos de la empresa
	if(objDatosEmpresa.bandera_actualizar=='S' && !actualizarEmpresa(objDatosEmpresa)){
		alert("Los datos de la empresa no se pueden actualizar!!. Por lo tanto la visita no se puede actualizar");
		return false;
	}
	
	$.ajax({
		url:"ActualizarVisita.php",
		type:"POST",
		data:{objDatosVisita:objDatosVisita},
		async:false,
		success:function(data){
			if(data==0){
				alert("No fue posible actualizar la visita!!");
				return false;
			}
			alert("Se actualiz\u00F3 la visita No "+objDatosVisita.idvisita);	
			nuevaVisita();   
		}
	});	 
}

function actualizarEmpresa(data){
	var resultado = false;
	$.ajax({
		url:urlActualizarEmpresa,
		type:"POST",
		data:{datos:data},
		typeData: "json",
		async: false,
		success: function(data){
			if(data==0){
				resultado = true;
			}			 
		}
	});
	
	return resultado;
}