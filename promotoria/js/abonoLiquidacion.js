$(function(){
	$("input").numeric();
	$("#txtNit").blur(function(){
		var nit = $(this).val().trim();
		if(!nit) return false;
		var datosLiquidacion,
			mensaje = "",
			htmlPeriodos = null;
		
		//Obtener los periodos de la liquidacion
		datosLiquidacion = buscarLiquidacion(nit);
		if(typeof datosLiquidacion=="object"){
			$.each(datosLiquidacion,function(i,row){
				htmlPeriodos += "<tr><td><input type='checkbox' id='chk"+row.idperiodoliquidacion+"' value='"+row.idperiodoliquidacion+"' onchange='chagePeriodo(\"chk"+row.idperiodoliquidacion+"\");'/>" +
						"<td>"+row.idvisita+"</td>" +
						"<td>"+row.idliquidacion+"</td>" +
						"<td>"+row.periodo+"</td>" +
						"<td><input type='text' id='txtAbono"+row.idperiodoliquidacion+"' value='"+row.abono+"' class='box1' disabled='disabled' style='text-align:rigth;'/></td>" +
						"<td style='text-align:center;'>"+row.fechavisita+"</td></tr>";
			});
			//Adicionar el nombre de la empresa al label
			$("#lblRazonSocial").text(datosLiquidacion[0].razonsocial)
			//Adicionar los periodos al formulario
			$("#tbLiquidacion").html(htmlPeriodos);
			//Dejar los input del abono para solo numeros
			$("input").numeric();
		}else{
			//No se encontraron liquidaciones
			mensaje = "<b style='color:red;'>La empresa no tiene periodos para cancelar.</b>";
			nuevo();
		}
		$("#tdMensaje").html(mensaje);
	});
});

function buscarLiquidacion(nit){
	var resultado=null;
	$.ajax({
		url:"buscarLiqAbono.php",
		type:"POST",
		data:{datos:{nit:nit,estado:'A',cancelado:"N"}},
		dataType:"json",
		async:false,
		success:function(datos){
			resultado=datos;
		}
	});
	return resultado;
}

function guardar(){
	var datosPeriodo = [],
		idPeriodoLiquidacion,
		bandera=0;
	
	//Remover el estilo de error
	$(".ui-state-error").removeClass("ui-state-error");
	
	//Validar que existan checkebox seleccionados
	if($("#tbLiquidacion input:checkbox:checked").length==0){
		alert("Debe chequear los periodos a realizar el abono");
		return false;
	}
	
	//Obtener datos
	$("#tbLiquidacion input:checkbox:checked").each(function(){
		idPeriodoLiquidacion = $(this).val();
		valorAbono = parseInt($("#txtAbono"+idPeriodoLiquidacion).val()); 
		//Validar que el valor del abono
		if(isNaN(valorAbono) || valorAbono==0){
			$("#txtAbono"+idPeriodoLiquidacion).addClass("ui-state-error");
			bandera++;
		}else{
			datosPeriodo[datosPeriodo.length]={
	                idperiodoliquidacion:idPeriodoLiquidacion
	                ,abono:valorAbono
	                ,idtipoabono:4228//[4228]ABONO VOLUNTARIO
	                ,cancelado:'S'
	            }
		}
	});
	
	//Validar si existe algun error
	if(bandera>0)return false;
	
	$.ajax({
		url:"guardarAbonoLiq.php",
		type:"POST",
		data:{datos:datosPeriodo},
		dataType:"json",
		async:false,
		success:function(datos){
			if(datos==1){
				alert("Los abonos se guardaron correctamente");
				nuevo();
			}else{
				alert("Error al guardar el abono de los periodos");
			}
		}
	});
}

function chagePeriodo(idCheckbox){
	var objTr = $("#"+idCheckbox).parent().parent();
	var objInputAbono = $("#txtAbono"+($("#"+idCheckbox).val()));

	//Validar si esta seleccionado
	if($("#"+idCheckbox).is(":checked")){
		objTr.addClass("ui-state-default");
		objInputAbono.attr("disabled",false);
	}else{
		objTr.removeClass("ui-state-default");
		objInputAbono.attr("disabled",true);
	}
}

function nuevo(){
	$("#tbLiquidacion").html("");
	$("#lblRazonSocial").text("");
	$("#txtNit").val("");
}