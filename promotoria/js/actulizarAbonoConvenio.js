var arrayDatosFecha = new Array();
var saldo = 0;
$(function(){
	buscarEmpresasConvenio();
	$("#txtValorAbono").numeric();
	$("#tasaInteres").numeric({allow:'.'}); 
});


function buscarEmpresasConvenio(){
	var option = "<option value='0'>Seleccione...</option>";
	var arrayEmpresas = new Array();
	$.ajax({
		url:"buscarEmprConveActivo.php",
		type:"POST",
		dataType:"json",
		async:false,
		success:function(datos){
			if( datos != 0 ){
				$.each(datos,function(i,row){
					if( arrayEmpresas.indexOf(row.idempresa) < 0){
						option += "<option value='" + row.idempresa + "'>" + row.razonsocial + "</option>";
					}
					arrayEmpresas[i] = row.idempresa;
					
				});
			}
		}
	});
	$("#cmbEmpresa").html(option);
}

function buscarFechaConvenio(){
	var idEmpresa = $("#cmbEmpresa").val();
	var option = "<option value='0'>Seleccione...</option>";
	limpiarCampo("#cmbFechaPago");
	
	arrayDatosFecha = [];
	if(idEmpresa == 0) return false;
	$.ajax({
		url:"buscarFechaConvenio.php",
		type:"POST",
		data:{idEmpresa:idEmpresa},
		dataType:"json",
		async:false,
		success:function(datos){
			if( datos != 0 ){
				$.each(datos,function(i,row){
					option += "<option value='" + row.iddetalleconvenio + "'>#" + row.consecutivo 
							+ " - [" + row.fechapago + "]</option>";
					
					arrayDatosFecha["id" + row.iddetalleconvenio] = {
							consecutivo:row.consecutivo,
							interes:row.interes,
							abono:row.abono,
							valor:row.valor
					};
				});
			}
		}
	});
	$("#cmbFechaPago").html(option);
}

function buscarPeriodoConvenio(){
	var idFechaConvenio = $("#cmbFechaPago").val();
	limpiarCampo(".clsDistrFecha,#txaObservacion,#txtValorAbono,#tdSaldoDisponible");
	if(idFechaConvenio == 0) 
		return false;
	var periodos = "",
			valorPeriodos = 0;
	$.ajax({
		url:"buscarPeriodo.php",
		type:"POST",
		data:{idFechaConvenio:idFechaConvenio},
		dataType:"json",
		async:false,
		success:function(datos){
			if( datos != 0 ){
				$.each(datos,function(i,row){
					periodos += " [" + row.periodo + "]" ;
					valorPeriodos += parseInt(row.valor);
				});
			}
		}
	});
	
	saldo = (valorPeriodos + parseInt(arrayDatosFecha["id" + idFechaConvenio].interes)) - parseInt(arrayDatosFecha["id" + idFechaConvenio].abono);
	$("#tdValorAcuerdo").text( formatCurrency(arrayDatosFecha["id" + idFechaConvenio].valor));
	$("#tdValorInteres").text( formatCurrency(arrayDatosFecha["id" + idFechaConvenio].interes) );
	$("#tdValorTotalFecha").text( formatCurrency((valorPeriodos + parseInt(arrayDatosFecha["id" + idFechaConvenio].interes))));
	$("#tdValorAbono").text( formatCurrency(arrayDatosFecha["id" + idFechaConvenio].abono ));
	$("#tdPeriodos").text( periodos );
	calcularSaldo();
}

function actualizar(){
	
	if($("#tdSaldoDisponible.ui-state-error").length > 0 )
		return false;
	if( validaCampoFormu() > 0 )
		return false;
	var valorAbono = parseInt(($("#tdValorAbono").text().trim().toString().replace(/\$|\,/g,'')));
	valorAbono = isNaN(valorAbono)?0:valorAbono;
	var objData = {
			abono: parseInt($("#txtValorAbono").val().trim()) + valorAbono, 
			observacion:$("#txaObservacion").val().trim(), 
			iddetalleconvenio:$("#cmbFechaPago").val().trim()
	};

	$.ajax({
		url: "actulizarAbonoConvenio.php",
		type: "POST",
		data: objData,
		dataType: "json",
		async: false,
		success: function(datos){
			if( datos == 0 ){
				alert("Error Al guardar la informacion");
			}else{
				alert("El abono se guardo correctamente");
				nuevo();
			}
		}
	});
}

function calcularSaldo(){
	var saldoDisponible = saldo - (isNaN(parseInt($("#txtValorAbono").val()))? 0 : parseInt($("#txtValorAbono").val())) ;
	$("#tdSaldoDisponible").text( formatCurrency(saldoDisponible.toString()) );
	$("#tdSaldoDisponible").removeClass("ui-state-error");
	if(saldoDisponible<0)
		$("#tdSaldoDisponible").addClass("ui-state-error");
}
function nuevo(){
	arrayDatosFecha = [];
	saldo = 0;
	$("#cmbFechaPago").html("");
	limpiarCampo(".clsDistrFecha,textarea,input,select,#tdSaldoDisponible");
}

function validaCampoFormu(){
	var banderaError = 0;
	$( ".ui-state-error" ).removeClass( "ui-state-error" );
	$(".clsValidaText").each(function(i,row){
		if( $( this ).val() == 0 ){
			$( this ).addClass( "ui-state-error" );
			banderaError++;
		}
	});
	return banderaError;
}

function limpiarCampo( classCampo ){
	var usuario = $("#txtUsuario").val();
	var tipoCampo = "";
	$( classCampo ).each(function(i,row){
		
		if( $( this ).get(0).name == "base" ){
			$( this ).val(0);
			return;
		}
		tipoCampo = $( this ).get(0).type;
		
		if( typeof tipoCampo === "undefined" )
			tipoCampo = $( this ).get(0).localName;
		
		if( tipoCampo == "select-one" )
			$( this ).val(0);
		else if( tipoCampo == "text" || tipoCampo == "textarea" || tipoCampo == "hidden" )
			$( this ).val("");
		else if( tipoCampo == "div" || tipoCampo == "td" || tipoCampo == "span" )
			$( this ).text("");
		else if( tipoCampo == "checkbox" )
			$( this ).attr("checked",false);
		else if( tipoCampo == "table" )
			$( this ).html("");
	});	
}