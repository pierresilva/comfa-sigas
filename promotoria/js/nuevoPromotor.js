//Variables generales
var URL=src();
var nuevo=0;

$(document).ready(function(){

//	Activo los detapicker en el formulario
	$("#fecInicio").datepicker({
		showAnim:'slideDown',
		changeMonth:true,
		maxDate:"+0D"
	});	
	$("#fecFinal").datepicker({
		showAnim:'slideDown',
		changeMonth:true,
		minDate:"+0D"
	});	
//	Dialog ayuda
	$("#ayuda").dialog({
		autoOpen: false,
		height: 450,
		width: 700,
		modal:false,
		open: function(evt, ui){
			$('#ayuda').html('');
			$.get(URL +'help/promotoria/manualayudaAdministracionpromotor.html',function(data){
				$('#ayuda').html(data);
			});
		}
	});

//	Dialog Colaboracion en linea
	$("#dialog-form2").dialog({
		autoOpen: false,
		height: 400,
		width: 500,
		draggable:false,
		modal: true,
		buttons: {
			'Enviar': function() {
				var bValid = true;
				var campo=$('#notas').val();
				var campo0=$.trim(campo);
				if (campo0==""){
					$(this).dialog('close');
					return false;
				}
				var campo1=$('#usuario').val();
				var campo2="nuevoPromotor.php";
				$.post(URL+'phpComunes/colaboracion.php',{v0:campo0,v1:campo1,v2:campo2},
						function(datos){
					if(datos=='1'){
						alert("Su comentario fue enviado correctamente!. Gracias por participar en el mejoramiento de SIGAS");
					}else{
						alert(datos);
					}
				});
				$(this).dialog('close');
			},
			Cancelar: function() {
				$(this).dialog('close');
				$("#dialog-form2").dialog("close");
			}
		}
	});

});//fn ready
//-----------------------------------NUEVO PROMOTOR
function nuevoPromotor(){
	limpiarCampo( "table.tablero input:text:not('#usuario'),table.tablero textarea,:hidden,table.tablero select,#tNombre" );
	//limpiarCampos();
	$("table.tablero td input:text.ui-state-error,select.ui-state-error").removeClass("ui-state-error");
	$("#tipoI").focus();
	$("#bGuardar").show();
	$("#iconActualizar").hide();
	nuevo=1;
}
//---------------------------------------LIMPIAR CAMPOS
function limpiarCampo( classCampo ){
	var usuario = $("#txtUsuario").val();
	var tipoCampo = "";
	$( classCampo ).each(function(i,row){
		
		if( $( this ).get(0).name == "base" ){
			$( this ).val(0);
			return;
		}
		tipoCampo = $( this ).get(0).type;
		
		if( typeof tipoCampo === "undefined" )
			tipoCampo = $( this ).get(0).localName;
		
		if( tipoCampo == "select-one" )
			$( this ).val(0);
		else if( tipoCampo == "text" || tipoCampo == "textarea" || tipoCampo == "hidden" )
			$( this ).val("");
		else if( tipoCampo == "div" || tipoCampo == "td" || tipoCampo == "span" )
			$( this ).text("");
		else if( tipoCampo == "checkbox" )
			$( this ).attr("checked",false);
	});	
	$( "#txtAportesPag" ).val(0);
	periodosLiquidacion = [];
	$("#txtUsuario").val( usuario );
}
/*
function limpiarCampos(){
	$("table.tablero input:text:not('#usuario,#fecInicio'),table.tablero textarea,:hidden").val('');
	$("table.tablero select").val(0);
	$("#tNombre").html('');
	$("table.tablero td input:text.ui-state-error,select.ui-state-error").removeClass("ui-state-error");
	$("#tipoI").focus();
}*/
//-----------------------------------GUARDAR ACTA DE VISITA

function validaCampoFormu(){
	var banderaError = 0;
	$( ".ui-state-error" ).removeClass( "ui-state-error" );
	$(".clsValidaText").each(function(i,row){
		if( $( this ).val() == 0 ){
			$( this ).addClass( "ui-state-error" );
			banderaError++;
		}
	});
	return banderaError;
}

function guardarPromotor(){
	if(nuevo==0){
		alert("Haga click primero en Nuevo");
		return false;
	}
	var error = validaCampoFormu();
	if( parseInt(error)>0 ){
		return false;
	}
	var idpersona=$("#idpersona").val();//oculto
	if( idpersona == 0 ){
		alert("Los datos de la persona estan incompletos");
		return false;
	}
	var tipoDoc=$("#tipoI").val();
	var cedula=$("#txtNumero").val();
	var fecInicio=$("#fecInicio").val();
	var fecFinal=$("#fecFinal").val();
	var estado=$("#estado").val();
	var usuario=$("#usuario").val(); 
	
	$.getJSON('guardarPromotor.php', {v0:idpersona,v1:fecInicio,v2:fecFinal,v3:estado}, function(data){
		if( data == 0 ){
			alert("No se guardo el promotor.");	
		}else if( data == 1 ){
			alert("Se guardo el promotor satisfactoriamente.");	
			nuevoPromotor();
		}
	});   	 
}

function actualizarPromotor(){
	var error = validaCampoFormu();
	if( parseInt(error)>0 ){
		return false;
	}
	var idPromotor = $("#hidIdPromotor").val();
	if( idPromotor == 0 ){
		alert("Falta datos del promotor");
		return false;
	}
	var fecInicio=$("#fecInicio").val();
	var fecFinal=$("#fecFinal").val();
	var estado=$("#estado").val(); 
	$.getJSON('actualizarPromotor.php', {v0:idPromotor,v1:fecInicio,v2:fecFinal,v3:estado}, function(data){
		if( data == 0 ){
			alert("No se actualizo el promotor.");	
		}else if( data == 1 ){
			alert("Se actualizo el promotor satisfactoriamente.");	
			nuevoPromotor();
		}
	});   	 
}
//----------------------------------------BUSCAR LA PERSONA
function buscarPersona(obj){
	if( $("#hidIdPromotor").val() != 0 )
		return false;
	
	if($("#tipoI").val()=='Seleccione'){
		$("#tipoI").addClass("ui-state-error");
		return false;
	}
	if(nuevo==0){
		alert("Si es un registro nuevo haga click en NUEVO!!");
		return;
	}else{
		var identificacion = $.trim(obj.value);
		var tipoIdentificacion = $("#tipoI").val();
		if( identificacion == '' ){return false;}
		nuevoPromotor();
		$("#tipoI").val( tipoIdentificacion );
		$("#txtNumero").val( identificacion );
		$.getJSON(URL+'phpComunes/buscarPersona2.php',{v0:tipoIdentificacion,v1:identificacion},function(data){
			if(data==0){
				alert("NO se encontraron resultados.");
				return false;
			}
			var nombre,
				idPersona;
			//Recuperamos los datos
			$.each(data,function(i,fila){
				nombre = $.trim(fila.pnombre)+" "+$.trim(fila.snombre)+" "+$.trim(fila.papellido)+" "+$.trim(fila.sapellido);
				idPersona = fila.idpersona;
				//estado=fila.estado
			});//each

			//Buscamos si existe el promotor
			$.getJSON('buscarPromotorVisita.php',{objDatosFiltro:{idpersona:idPersona}},function(data){
				if( data != 0 ){
					data=data[0];
					if(confirm(" Ya existe un promotor con esa identificaci\u00F3n. \n Deseas modificar el promotor")){
						$("#bGuardar").hide();
						$("#iconActualizar").show();
						$("#tNombre").text( nombre );
						$("#fecInicio").val( data.fechainicio );
						$("#fecFinal").val( data.fechafinal );
						$("#estado").val( data.estado );
						$("#hidIdPromotor").val( data.idpromotor );
					}else{
						nuevoPromotor();
					}
					//limpiarCampos(); 
				}else{
					//insertamos los datos traidos del servidor
					$("#tNombre").html(nombre);
					$("#idpersona").val(idPersona);//campo oculto
					//$("#estado").val(estado);
				}
			});//end json 
		});//end json
	}  
}

//----------------------------------------VENTANA DE DIALOGO DE AYUDA
function mostrarAyuda(){
	$("#ayuda").dialog('open');
}
//----------------------------------------VENTANA DE DIALOGO DE MANUAL
function notas(){
	$("#dialog-form2").dialog('open');
}	