/*
* Autor: Andres Felipe Lara Nieto
* Objetivo: Archivo para las validaciones de los reportes del módulo de promotoria
* Fecha: 24-Mar-2011
*/

//Funcion para el reporte001-Visitas por empresa
// funcion para obtener cookie en javascript
	function getCookie(c_name)
	{
		var i,x,y,ARRcookies=document.cookie.split(";");
		for (i=0;i<ARRcookies.length;i++)
		{
  			x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
  			y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
  			x=x.replace(/^\s+|\s+$/g,"");
  			if (x==c_name)
    			{
    				return unescape(y);
    			}// fin if
		}// fin for
  	}// fin getCookie
//funcion para obtener cookie en javascript

URL=src();
function rptBuscarVisitas(nit){
	
	$("#rta").empty();
	$("#nit").removeClass("ui-state-error");
	
	if(nit==''||isNaN(nit)){
	$("#nit").addClass("ui-state-error").focus();
	return false;
	}
	
	$.ajax({
			url:URL+'phpComunes/buscarEmpresa.php', 
			type:"POST",
			data:"v0="+nit, 
			dataType:"json",
			async:false,
			success:function(datos){
	           		if(datos==0){
						$("#rta").html("La empresa no existe en nuestra base de datos.").hide().fadeIn(1000);
					    return false;
					 }
					 $.each(datos,function(i,fila){ idempresa=fila.idempresa;empresa=fila.razonsocial;});//each
					
					 //Buscamos las visitas existentes  realizadas a la empresa seleccionada
				      $.ajax({
		    				  url:URL+'promotoria/buscarVisitasEmpresa.php', 
							  type:"POST",
							  data:"idempresa="+idempresa, 
							  dataType:"json",
							  async:false,
							  success:function(visitas){
								  if(visitas==0){
								  $("#rta").html("NO existen visitas para esta empresa.").hide().fadeIn(1000);
								  return false;
								  }
								  //Si existen visitas generamos el reporte y pasamos el parametro
								  //location.href=URL+"centroReportes/promotoria/reporte001.php?idempresa="+idempresa;
								  var url="http://"+getCookie("URL_Reportes")+"/promotoria/reporte001.jsp?v0="+idempresa+"&v1="+$("#hidUsuario").val();
								  window.open(url,"_BLANK");
							   }
					 });//ajax
			}//succes	 
	});//ajax
	
}
//Funcion para el reporte002-Reporte de liquidaciones por empresa
function rptBuscarLiquidaciones(nit){
    
	$("#rta").empty();
	$("#nit").removeClass("ui-state-error");
	
	if(nit==''||isNaN(nit)){
	$("#nit").addClass("ui-state-error").focus();
	return false;
	}
    
	$.ajax({
			url:URL+'phpComunes/buscarEmpresa.php', 
			type:"POST",
			data:"v0="+nit, 
			dataType:"json",
			async:false,
			success:function(datos){
	           		if(datos==0){
						$("#rta").html("La empresa no existe en nuestra base de datos.").hide().fadeIn(1000);
					    return false;
					 }
					 $.each(datos,function(i,fila){ idempresa=fila.idempresa;empresa=fila.razonsocial;});//each
					
					 //Buscamos las visitas existentes  realizadas a la empresa seleccionada
				      $.ajax({
		    				  url:URL+'promotoria/buscarLiqEmpresa.php', 
							  type:"POST",
							  data:"id="+idempresa, 
							  dataType:"json",
							  async:false,
							  success:function(liq){
								  if(liq==0){
								  $("#rta").html("NO existen liquidaciones para esta empresa.").hide().fadeIn(1000);
								  return false;
								  }
								  //Si existen visitas generamos el reporte y pasamos el parametro
								  location.href=URL+"centroReportes/promotoria/reporte002.php?idempresa="+idempresa;
							   }
					 });//ajax
			}//succes	 
	});//ajax
    

}
//Funcion para reporte mensual de produccion
function rptMensual(){
    
	$("#rta").empty();
	$("#nit").removeClass("ui-state-error");
    
	if($("#promotor").val()=='0'){
    $("#promotor").addClass("ui-state-error").focus();
	return false;
	}
	
	if($("#fecInicial").val()==''){
    $("#fecInicial").addClass("ui-state-error").focus();
	return false;
	}
	
	if($("#fecFinal").val()==''){
    $("#fecFinal").addClass("ui-state-error").focus();
	return false;
	}
	
	var idpromotor=$("#promotor").val();
	var promotor=$.trim($("#promotor option:selected").text());
	var fecI=$("#fecInicial").val();
	var fecF=$("#fecFinal").val();
	
	//Envio los parametros del reporte
	 location.href=URL+"centroReportes/promotoria/reporte003.php?idpromotor="+idpromotor+"&promotor="+promotor+"&fecInicial="+fecI+"&fecFinal="+fecF;
}