<?php
	set_time_limit(0);
	ini_set("display_errors",'1');
	date_default_timezone_set("America/Bogota");
	$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
	include_once  $root;
	include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';
	
	$usuario=$_SESSION['USUARIO'];
	include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'p.definiciones.class.php';
	
	$objClase=new Definiciones();
	$fecver = date('Ymd h:i:s A',filectime('inactPuPromo.php'));
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<title>Inactivar PU Promotoria</title>
		<link type="text/css" href="<?php echo URL_PORTAL; ?>newcss/Estilos.css" rel="stylesheet">
		<link type="text/css" href="<?php echo URL_PORTAL; ?>newcss/css.1.8/jquery-ui-1.8.17.custom.css" rel="stylesheet">
		<link type="text/css" href="<?php echo URL_PORTAL; ?>newcss/marco.css" rel="stylesheet">
		
		<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/1.6/jquery-1.6.2.min.js"></script>
		<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/1.6/jquery-ui-1.8.16.custom.min.js"></script>
		<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/comunes.js"></script>
		<script type="text/javascript" src="js/inactPuPromo.js"></script>
	</head>
	<body>
		<table width="60%" border="0" cellspacing="0" cellpadding="0" style="margin:0 auto;">
			<tr>
				<td class="arriba_iz" >&nbsp;</td>
				<td class="arriba_ce" ><span class="letrablanca">::Inactivar PU Promotoria::</span>
					<div style="text-align:right;float:right;">
						<?php echo 'Versi&oacute;n: ' . $fecver; ?>
					</div>
				</td>
				<td class="arriba_de" >&nbsp;</td>
			</tr>
			<tr>
				<td class="cuerpo_iz">&nbsp;</td>
				<td class="cuerpo_ce">&nbsp;</td>
				<td class="cuerpo_de">&nbsp;</td>
			</tr>
			<tr>
				<td class="cuerpo_iz" >&nbsp;</td>
				<td class="cuerpo_ce" >
					<table border="0" class="tablero" cellspacing="0" width="100%">
			  			<tr>
						    <td width="100" style="text-align:right;">
						    	Nit:
						    	<input type="hidden" name="hidIdEmpresa" id="hidIdEmpresa"/>
						    </td>
						    <td>
						    	<input type="text" name="txtNit" id="txtNit" class="box1"/>
						    	&nbsp; <label id="lblRazonSocial" >&nbsp;</label>
						    </td>
			  			</tr>
			  			<tr>
			    			<td style="text-align:right;">Periodo:</td>
			    			<td id="tdPeriodo">
			    				<input  type="text" readonly="readonly"size="10" name="txtPeriodo" id="txtPeriodo" class="box1"/> 
			    				&nbsp;&nbsp;
			    				<input type="button" value="Buscar" id="btnBuscarAportes"/>
			    			</td>
			  			</tr>
			  			<tr>
			      			<td colspan="2" id="tdMensajeError" style="color:#FF0000"></td>
			  			</tr>
			  			<tr>
			      			<td colspan="2">Aportes de la Empresa</td>
			  			</tr>
			 			<tr>
			 				<td colspan="2">
			 					<table border="0" class="tablero" cellspacing="0" width="100%" id="tabAportes"> 
						  			<thead width="100%">
						  				<tr>
						  					<th>Recibo</th>
						  					<th>Periodo</th>
						  					<th>Nomina</th>
						  					<th>Aporte</th>
						  					<th>Fecha Pago</th>
						  					<th>Estado</th>
						  				</tr>
						  			</thead>
						  			<tbody id="tBody" width="100%">
						  				&nbsp;
						  			</tbody>
								</table>
			 				</td>
			 			</tr>
			 			<tr>
			 				<td colspan="2" style="text-align: center;">
			 					<input type="button" name="btnGuardar" id="btnGuardar" value="Guardar"/>
			 				</td>
			 			</tr>
			 		</table>
				</td>
				<td class="cuerpo_de" >&nbsp;</td>
			</tr>
			<tr>
				<td class="cuerpo_iz" >&nbsp;</td>
				<td class="cuerpo_ce" >&nbsp;</td>
				<td class="cuerpo_de" >&nbsp;</td>
			</tr>
			<tr>
				<td class="abajo_iz" >&nbsp;</td>
				<td class="abajo_ce" ></td>
				<td class="abajo_de" >&nbsp;</td>
			</tr>
		</table>
	</body>
</html>