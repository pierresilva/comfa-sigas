<?php

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$url=$_SERVER['PHP_SELF'];
include_once $raiz.DIRECTORY_SEPARATOR.'phpComunes'.DIRECTORY_SEPARATOR.'auditoria.php';
auditar($url);

include_once 'clases'.DIRECTORY_SEPARATOR. 'promotoria.class.php';
include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'p.definiciones.class.php';
$objPro=new Promotoria;
$objDef=new Definiciones;
$idvisita=$_REQUEST['idv'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>::LIQUIDACIONES DE APORTES PARAFISCALES::</title>
<link href="../css/Estilos.css" rel="stylesheet" type="text/css" />
<link type="text/css" href="../css/sorterStyle.css" rel="stylesheet" />
<link type="text/css" href="../css/estilo_tablas.css" rel="stylesheet"/>
<link type="text/css" href="../css/formularios/base/ui.all.css" rel="stylesheet" />
<link href="../css/marco.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/jquery-1.4.2.js"></script> 
<script type="text/javascript" src="../js/formularios/ui/ui.core.js"></script>
<script type="text/javascript" src="../js/formularios/ui/ui.dialog.js"></script>
<script type="text/javascript" src="../js/formularios/ui/ui.datepicker.js"></script>
<script type="text/javascript" src="../js/jquery.alphanumeric.js"></script>
<script type="text/javascript" src="../js/jquery.periodos.js"></script>
<script type="text/javascript" src="../js/effects.Jquery.js"></script>
<script type="text/javascript" src="../js/comunes.js"></script>
<script type="text/javascript" src="js/aforos.js"></script>

<script type="text/javascript">
shortcut.add("Shift+F",function() {
		                                var URL=src();
		var url=URL+"aportes/trabajadores/consultaTrabajador.php";
    	window.open(url,"_blank");
    },{
	'propagate' : true,
	'target' : document 
        });        
</script>


</head>

<body>
<!-- TABLA OCULTA DE LISTADO  width81%  left:9.2-->
 

<table width="70%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tbody>
 
  <!-- ESTILOS SUPERIOR TABLA-->
  <tr>
    <td width="13" height="29" class="arriba_iz">&nbsp;</td>
     <td class="arriba_ce"> <span class="letrablanca">::&nbsp;Liquidaci&oacute;n Aportes - Aforos&nbsp;::</span></td>
    <td width="13" class="arriba_de" align="right">&nbsp;</td>
  </tr>
  
  <!-- ESTILOS ICONOS TABLA-->
  <tr>
    <td class="cuerpo_iz">&nbsp;</td>
    <td class="cuerpo_ce">
<img src="../imagenes/menu/revisar.png" name="iconRevisar" id="iconRevisar" width="16" height="16" title="Generar AFORO" style="cursor:pointer" onclick="aforo();" />
<img src="../imagenes/menu/modificar.png" name="iconActualizar" width="16"  height=16 id="iconActualizar" style="cursor:pointer" title="Actualizar Liquidacion" onClick="actualizarLiquidacion();" />
<img src="../imagenes/menu/refrescar.png" name="iconLimpiar" width="16"  height=16 id="iconLimpiar" style="cursor:pointer" title="Limpiar Campos" onClick="limpiarCampos();" />
<img src="../imagenes/menu/informacion.png" name="iconInfo" width="16" id="iconInfo" style="border:none; cursor:pointer" title="Manual" onClick="mostrarAyuda();" />
<img src="../imagenes/menu/notas.png" name="iconSugerencia" width="16" height="16" id="iconSugerencia" style="cursor: pointer" title="Colaborac&oacute;on en l&oacute;nea" onClick="notas();" />
                  
    </td>
    <td class="cuerpo_de">&nbsp;</td>
  </tr>
  
  <!-- ESTILOS MEDIO TABLA-->
  <tr>
    <td class="cuerpo_iz">&nbsp;</td>
    <td class="cuerpo_ce"><div id="resultado" style="color:#FF0000"></div></td>
    <td class="cuerpo_de">&nbsp;</td>
  </tr> 
  
  <!-- CONTENIDO TABLA-->
  <tr>
   <td class="cuerpo_iz">&nbsp;</td>
    <td class="cuerpo_ce">
<div id="msjeLiquidado" class="ui-state-highlight ui-corner-all" style="margin:10px;width:730px; padding:8px 5px 5px 25px ;font:11px Verdana, Geneva, sans-serif; color:#333; text-align:left; display:none; line-height:20px;">
<span style="float: left; margin-right: 0.3em;" class="ui-icon ui-icon-info"></span>
La liquidaci&oacute;n <strong id="msjeIdLiq"></strong> con No de visita <strong id="msjeIdVis"></strong> esta a paz y salvo en aportes parafiscales.  
<p><b>Motivo: </b><span id="msjeMotivo"></span></p>
<p><b>Fecha de pago: </b><span id="msjeFechaPag"></span></p>
</div>
	<table cellspacing="0" border="0" class="tablero" width="100%">
      <tbody>
	    <tr>
	      <th colspan="6">DATOS LIQUIDACI&Oacute;N</th>
	      </tr>
	    <tr>
    <td width="25%">Liquidaci&oacute;n No</td>
    <td width="25%"><input name="txtLiquidacion" type="text"  class="boxfecha" id="txtLiquidacion" onBlur="buscarLiquidacion(this,1)" /></td>
    <td width="25%">Fecha Liquidaci&oacute;n</td>
    <td width="25%"><input type="text" id="fechaLiq" class="boxfecha" /></td>
	      </tr>
	    <tr>
	      <th colspan="6">DATOS DEL APORTANTE</th>
	      </tr>
	    <tr>
	      <td colspan="6">
          <table border="0" class="tablero" width="100%">
           <tr>
	      <td width="3%">Nit</td>
	      <td width="21%"><input name="nit" class="box1" id="nit" readonly="readonly" />
	          <input type="hidden" name="idempresa" id="idempresa" /></td>
	      <td width="7%">Estado</td>
	      <td width="4%"><input name="estado" type="text"  class="boxcorto" id="estado" readonly="readonly" /></td>
	      <td width="9%">Empresa</td>
	      <td width="56%"><strong>
          <div  id="empresa" align="left"></div>
        </strong></td>
	      </tr>
          </table>
          </td>
	      </tr>
	   
	    
      <tr>
         <td>Represent&aacute;nte Legal</td>
        <td><div id="repLegal"></div></td>
        <td>Identificaci&oacute;n Rep. Legal</td>
        <td colspan="3"><input name="idRepLegal" type="text"  class="box1" id="idRepLegal" readonly="readonly"></td>
       </tr>
      <tr>
        <td>Tipo Afiliaci&oacute;n</td>
        <td><select name="tipoAfiliacion" class="box1" id="tipoAfiliacion">
          <option value="0">Seleccione</option>
          <?php
	           $consulta=$objDef->mostrar_datos(2, 2);
	           while($row=mssql_fetch_array($consulta)){
		       echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
	           }
	        ?>
          </select>
          <img src="../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
        <td>Causal</td>
        <td colspan="3"><select name="causal" class="box1" id="causal">
          <option value="0">Seleccione</option>
          <?php
	           $consulta=$objDef->mostrar_datos(44, 2);
	           while($row=mssql_fetch_array($consulta)){
		       echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
	           }
	        ?>
          </select>
          <img src="../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
      </tr>
       <tr>
         <th colspan="6">NOTIFICACI&Oacute;N</th>
       </tr>
      <tr>
        <td>Tipo notificaci&oacute;n</td>
        <td><select name="tipoNotificacion" class="box1" id="tipoNotificacion">
          <option value="0" selected="selected">Seleccione</option>
           <?php
	           $consulta=$objDef->mostrar_datos(19, 2);
	           while($row=mssql_fetch_array($consulta)){
		       echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
	           }
	        ?>
        </select>
          <img src="../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
        <td>Fecha Notificaci&oacute;n</td>
        <td colspan="3">
          <input name="fechaNot" type="text" class="boxfecha" id="fechaNot" />
          <img src="../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
      </tr>
      <tr>
        <td>Observaciones</td>
        <td colspan="5"><span id="observaciones"></span></td>
        </tr>
      <tr>
        <td>Promotor(a)</td>
        <td><select name="promotor" class="box1" id="promotor">
          <option value="0">Seleccione</option>
          <?php
         
		$consulta=$objPro->buscar_promotor_combo();
		 while($row=mssql_fetch_array($consulta)){
		 $nombre=$row['pnombre']." ".$row['snombre']." ".$row['papellido']." ".$row['sapellido'];	 
		 echo "<option value=".$row['idpersona'].">".$nombre."</option>";
		 }
		?>
          </select>
          <img src="../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
        <td>Usuario</td>
        <td colspan="3"><input name="usuario" type="text" disabled  class="box1" id="usuario" readonly="readonly" value="<?php echo $_SESSION['USUARIO']; ?>" /></td>
      </tr>
    
      </tbody>
      </table>
      <br />
      <table class="sortable" style="width:85%; margin:auto;" id="tablaBase">
      <tr>
        <td colspan="5" class="total"><strong><center>
              BASE LIQUIDACI&Oacute;N
        </center></strong></td>
        </tr>
      
      <tr>
        <td colspan="4">
          <select name="ordenarBase" class="box1" id="ordenarBase" onchange="ordenBase(this.value);">
            <option value="0">Tipo Empresa</option>
            <option value="pl">P�blico</option>
            <option value="pr">Privado</option>
            </select>  Periodos:       
          &nbsp;<img src="../imagenes/menu/calendario.png" alt="Elegir periodo" width="16" height="16" align="absmiddle" id="periodos" />
          <input type="hidden" id="array" />
          </td>
         </tr>
         <tr>
         <td colspan="4">
         <div id="etiquetas" style="width:600px; height:auto;float:left"></div>
         </td>
         </tr>
      <tr class="privado">
        <td width="25%">1.Sueldos y Sobresueldos</td>
        <td width="25%"><input name="base" type="text" class="box1" id="base1" value="0"  /></td>
        <td width="25%">2.Salario Integral 70%</td>
        <td width="25%"><input name="base2" type="text" class="box1" id="base2" value="0"   /></td>
        </tr>
      <tr class="privado">
        <td>3.Personal supernumerario</td>
        <td><input name="base" type="text" class="box1" id="base3" value="0"   /></td>
        <td>4.H Extras, dominicales y  f</td>
        <td><input name="base3" type="text" class="box1" id="base4" value="0"   /></td>
      </tr>
      <tr class="privado">
        <td>5.Recargo Nocturno</td>
        <td><input name="base" type="text" class="box1" id="base5" value="0"   /></td>
        <td>6.Incremento por antig&uuml;edad (p&uacute;blico)</td>
        <td><input name="base4" type="text" class="box1" id="base6" value="0"   /></td>
      </tr>
      <tr class="privado">
        <td>7.Jornales</td>
        <td><input name="base" type="text" class="box1" id="base7" value="0"   /></td>
        <td>8.Comisiones y porcentaje</td>
        <td><input name="base5" type="text" class="box1" id="base8" value="0"   /></td>
      </tr>
      <tr class="privado">
        <td>9.Vacaciones</td>
        <td><input name="base" type="text" class="box1" id="base9" value="0"   /></td>
        <td>10.Prima de vacaciones</td>
        <td><input name="base6" type="text" class="box1" id="base10" value="0"   /></td>
      </tr>
      <tr class="privado">
        <td>11.Bonificaciones salariales</td>
        <td><input name="base" type="text" class="box1" id="base11" value="0"   /></td>
        <td>12.Primas extralegales y/o  convencionales</td>
        <td><input name="base7" type="text" class="box1" id="base12" value="0"   /></td>
      </tr>
      <tr class="privado">
        <td>13.Por unidad de tiempo de  obra o destajo</td>
        <td><input name="base" type="text" class="box1" id="base13" value="0"   /></td>
        <td>14.Vi&aacute;tico permanen.  (Manutenci&oacute;n y alojamiento)</td>
        <td><input name="base8" type="text" class="box1" id="base14" value="0"   /></td>
      </tr>
      <tr class="privado">
        <td>15.Remuneraci&oacute;n socios  industriales</td>
        <td><input name="base" type="text" class="box1" id="base15" value="0"   /></td>
        <td>16.Contratos agr&iacute;colas</td>
        <td><input name="base9" type="text" class="box1" id="base16" value="0"   /></td>
      </tr>
      <tr class="publico">
        <td>17.Prima de servicios  (p&uacute;blico)</td>
        <td><input name="base" type="text" class="box1" id="base17" value="0"   /></td>
        <td>18.Prima de manejo (p&uacute;blico)</td>
        <td><input name="base10" type="text" class="box1" id="base18" value="0"   /></td>
      </tr>
      <tr class="publico">
        <td>19.Gastos de representaci&oacute;n  (p&uacute;blico)</td>
        <td><input name="base" type="text" class="box1" id="base19" value="0"   /></td>
        <td>20.Bonificaci&oacute;n Servicios  prestados (p&uacute;blico)</td>
        <td><input name="base11" type="text" class="box1" id="base20" value="0"   /></td>
      </tr>
      <tr class="privado">
        <td>21.Subsidio alimentaci&oacute;n y  </td>
        <td><input name="base" type="text" class="box1" id="base21" value="0"  /></td>
        <td>22.Prima t&eacute;cnica salarial</td>
        <td><input name="base12" type="text" class="box1" id="base22" value="0"  /></td>
      </tr>
      <tr class="privado">
        <td>23.Horas c&aacute;tedra</td>
        <td><input name="base" type="text" class="box1" id="base23" value="0"  /></td>
        <td>24.Licencias remuneradas</td>
        <td><input name="base13" type="text" class="box1" id="base24" value="0"  /></td>
      </tr>
      <tr class="privado">
        <td>25.Indemnizaci&oacute;n vacaciones</td>
        <td><input name="base" type="text" class="box1" id="base25" value="0"  /></td>
        <td>26.Otras denominaciones</td>
        <td><input name="base14" type="text" class="box1" id="base26" value="0"  /></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        </tr>
      <tr>
        <td><p align="center"><strong>TOTAL BASE LIQUIDACI&Oacute;N</strong></p></td>
        <td class="RojoGrande" style="text-align:center;font-size:14px;"><span>$</span><span id="totalBase">0</span></td>
        <td class="RojoGrande" style="text-align:center;font-size:14px;">&nbsp;</td>
        <td class="RojoGrande" style="text-align:center;font-size:14px;">&nbsp;</td>
        </tr>
           
      <tr>
        <td colspan="5" class="total"><strong><center>
              LIQUIDACI&Oacute;N
        </center></strong></td>
        </tr>
      <tr>
        <td>&nbsp; Aportes&nbsp;
          <select name="cboAportes" id="cboAportes" class="box1" onchange="hallarAportes(this)">
            <option value="0">Seleccione</option>
            <option value="0.02">2%</option>
            <option value="0.04">4%</option>
          </select></td>
        <td><input name="aportes" type="text" class="box1" id="txtAportes" value="0" readonly="readonly"  /></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
     
        <tr>
          <td>
          <input type="checkbox" name="chkLiquidacion" id="chkAportesPag" />
          Menos aportes pagados</td>
        <td><input  type="text" disabled="disabled" class="box1" id="txtAportesPag" value="0" onblur="calcularCapital(this)"  /></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        </tr>
      <tr>
        <td>
       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; CAPITAL</td>
        <td><input name="aportes" type="text" disabled="disabled" class="box1" id="txtCapital" value="0" readonly="readonly"  /></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; TASA DE INTER&Eacute;S
      </td>
        <td><input name="tasaInteres" type="text" class="box" id="tasaInteres" onblur="validarInteres(this.value)" value="0" size="4" maxlength="3" />
          <span>%</span></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        </tr>
      <tr>
        <td>&nbsp;&nbsp; &nbsp;&nbsp; INTERESES DE MORA</td>
        <td><input name="aportes" type="text" disabled="disabled" class="box1" id="txtInteres" value="0" readonly="readonly"  /></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td><p align="center"><strong>TOTAL A PAGAR</strong></p></td>
        <td class="RojoGrande" style="font-size:14px; text-align:center"><span>$</span><span id="totalLiq">0</span></td>
        <td class="RojoGrande" style="font-size:14px; text-align:center">&nbsp;</td>
        <td class="RojoGrande" style="font-size:14px; text-align:center">&nbsp;</td>
        </tr>
     
    </table>
	</td>
     <td class="cuerpo_de">&nbsp;</td>
  </tr>
  
  <!-- ESTILOS PIE TABLA-->
  <tr>
    <td class="abajo_iz" >&nbsp;</td>
    <td class="abajo_ce" ></td>
    <td class="abajo_de" >&nbsp;</td>
  </tr>
  
</tbody>
</table>

<!-- colaboracion en linea -->

<div id="dialogo-archivo" title="Archivo banco">
<div id="progreso" style="display: none; font-size: 15pt; font-weight: bold;">Procesado(s) <span id="pg">0</span> de <span id="tt"></span> archivo(s)</div>
<div id="log"></div>
</div>
<div id="dialog-form2" title="Colaboraci&oacute;n en l&iacute;nea" style="display:none">
<p>Por favor diligencie este formulario para enviar comentarios, errores o falencias encontradas en el proceso. M&aacute;ximo 250 caracteres </p>
<label>Tus comentarios:</label><br />
<textarea name="notas" id="notas" cols="60", rows="10"></textarea>
</div>
<!--fin colaboracion en linea-->

<!-- Manual Ayuda -->
<div id="ayuda" title="::MANUAL DE LIQUIDACIONES DE APORTES PARAFISCALES::"></div>

<!-- TABLA VISITAS -->

</body>
</html>