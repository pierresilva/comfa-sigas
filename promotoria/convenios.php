<?php	
	set_time_limit(0);
	ini_set("display_errors",'1');
	date_default_timezone_set("America/Bogota");
	$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
	include_once  $root;
	include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';
	
	$url=$_SERVER['PHP_SELF'];
	include_once $raiz.DIRECTORY_SEPARATOR.'phpComunes'.DIRECTORY_SEPARATOR.'auditoria.php';
	auditar($url);
	
	//Obtener la fecha de la version
	$fecver = date('Ymd h:i:s A',filectime('convenios.php'));
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<title>::Promotoria | Convenios::</title>
		
		<link type="text/css" href="<?php echo URL_PORTAL; ?>newcss/Estilos.css" rel="stylesheet">
		<link type="text/css" href="<?php echo URL_PORTAL; ?>newcss/css.1.8/jquery-ui-1.8.17.custom.css" rel="stylesheet">
		<link type="text/css" href="<?php echo URL_PORTAL; ?>newcss/marco.css" rel="stylesheet">
		
		<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/1.6/jquery-1.6.2.min.js"></script>
		<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/1.6/jquery-ui-1.8.16.custom.min.js"></script>
		<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/comunes.js"></script>
		<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/jquery.alphanumeric.js"></script>
		<script type="text/javascript" src="js/convenios.js"></script>
		
		<!-- <link href="../css/Estilos.css" rel="stylesheet" type="text/css" />
		<link href="../css/marco.css" rel="stylesheet" type="text/css" />
		<link href="../newcss/css.1.8/jquery-ui-1.8.17.custom.css" rel="stylesheet" type="text/css" />
		
		<script type="text/javascript" src="../newjs/1.6/jquery-1.6.2.min.js"></script>
		<script type="text/javascript" src="../newjs/1.6/jquery-ui-1.8.16.custom.min.js"></script>
		<script type="text/javascript" src="../js/jquery.alphanumeric.js"></script>-->
	</head>
	<body>
		<table width="75%" border="0" align="center" cellpadding="0" cellspacing="0">
			<tbody>
		  	<!-- ESTILOS SUPERIOR TABLA-->
		  	<tr>
			    <td width="13" height="29" class="arriba_iz">&nbsp;</td>
			    <td class="arriba_ce">
			    	<span class="letrablanca">::&nbsp;Guardar convenio de pago&nbsp;::</span>
			    	<div style="text-align:right;float:right;">
							<?php echo 'Versi&oacute;n: ' . $fecver; ?>
					</div>
			    </td>
			    <td width="13" class="arriba_de" align="right">&nbsp;</td>
		  	</tr> 
		  	<!-- ESTILOS ICONOS TABLA-->
		  	<tr>
		    	<td class="cuerpo_iz">&nbsp;</td>
		    	<td class="cuerpo_ce">
					<img src="../imagenes/menu/nuevo.png" name="iconNuevo" width="16" height="16" id="iconNuevo" style="cursor:pointer" title="Nuevo" onClick="nuevo();" />
					<img width="3" height="1" src="../imagenes/spacer.gif" />
					<img width="3" height="1" src="../imagenes/spacer.gif" />
					<img src="../imagenes/menu/grabar.png" name="iconGuardar" width="16"  height=16 id="bGuardar" style="cursor:pointer" title="Guardar" onClick="guardar();" />     
		    	</td>
		    	<td class="cuerpo_de">&nbsp;</td>
		  	</tr>
		  	<!-- ESTILOS MEDIO TABLA-->
		  	<tr>
		    	<td class="cuerpo_iz">&nbsp;</td>
		    	<td class="cuerpo_ce"></td>
		    	<td class="cuerpo_de">&nbsp;</td>
		  	</tr>  
		  	<!-- CONTENIDO TABLA-->
		  	<tr>
		   		<td class="cuerpo_iz">&nbsp;</td>
		   		<td class="cuerpo_ce">
					<table cellspacing="0" width="90%" border="0" class="tablero" align="center">
		      			<tbody>
			    			<tr>
			      				<th colspan="4">
			      					DATOS DEL ACUERDO DE PAGO
			      					<input type="hidden" name="hidIdConvenio" id="hidIdConvenio"/>
			      				</th>
			      			</tr>
			    			<tr>
			      				<td colspan="4" style="text-align:center;">Empresa
			      					<select name="cmbEmpresa" id="cmbEmpresa" onchange="buscarLiquidaciones();" class="clsValidaText boxlargo"></select>
			      				</td>
			        		</tr>
			    			<tr>
			      				<th colspan="4">
			      					LIQUIDACIONES
			      				</th>
			      			</tr>
			    			<tr>
			      				<td id="tdLiquidaciones" colspan="4" style="text-align:center;" >
			      				</td>
			      			</tr>
			      			<tr>
			      				<td>Valor Liquidaci&oacute;n(es)</td>
			      				<td width="30%" id="tdValorLiquidacion" style="text-align:right;">$</td>
			      				<td width="23%"> </td>
			      				<td width="23%"></td>
			      			</tr>
			      			<tr>
			      				<th colspan="4">
			      					DATOS CONVENIO
			      				</th>
			      			</tr>
			    			<tr> 
			    				<td>Valor Acuerdo de Pago</td>
			    				<td>$<input type="text" name="txtValorAcuerdo" id="txtValorAcuerdo" class="box1 clsValidaText"/></td>
			    				<td>Indice</td>
			    				<td>
			    					<select name="cmbIndice" id="cmbIndice" class="clsValidaText box1">
			    						<option value="0">Seleccione...</option>
			    						<option value="2">2%</option>
			    						<option value="4">4%</option>
			    					</select>
			    				</td>   
			    			</tr>
			    			<tr>
			    				<td>Saldo disponible</td>
			    				<td>$<input type="text" name="txtSaldoDisponible" id="txtSaldoDisponible" class="box1" disabled="disabled"/></td>
			    				<td></td>
			    				<td></td>
			    			</tr>
			    			<tr>
			      				<th colspan="4">
			      					DISTRIBUCION
			      				</th>
			      			</tr>
			      			<tr>
			      				<td>Fecha a pagar</td>
			      				<td>
			      					<input type="text" name="txtFechaPagar" id="txtFechaPagar" readonly="readonly" class="boxfecha clsValidaText"/>
			      				</td>
			      				<td>Valor del Interes</td>
			      				<td>$<input type="text" name="txtValorInteres" id="txtValorInteres" class="box1 " onblur="calcularTotal();"/></td>
			      			</tr>
			      			<tr>
			      				<td colspan="4" style="text-align:center;">
			      					<!-- PERIODOS -->
			      					<br/>
			      					<table align="center" id="tblPeriodo" class="tablero" width="60%">
						      			<thead >
							         		<tr>
							         			<th>&nbsp;</th>
							         			<th >Liquidaci&oacute;n</th>
							         			<th>Detalle</th>
							         			<th>Periodo</th>
							         			<th>Total Liquidaci&oacute;n</th>
							         			<th>Valor Convenio</th>
							         		</tr>
							         	</thead>
							         	<tbody>
						         		</tbody>  
						    		</table>
			      					<br/>
			      				</td>
			      			</tr>
			      			<tr>
			      				<td >Total A Pagar</td>
			      				<td> 
			      					$<input type="text" name="txtTotalPagar" id="txtTotalPagar" readonly="readonly" class="ui-state-default clsValidaText"/>
			      				</td>
			      				<td></td>
			      				<td></td>
			      			</tr>
		      			</tbody>
		      		</table>
					</td>
		    		<td class="cuerpo_de">&nbsp;</td>
		  		</tr>		  
		  		<!-- ESTILOS PIE TABLA-->
		  		<tr>
		    		<td class="abajo_iz" >&nbsp;</td>
		    		<td class="abajo_ce" ></td>
		    		<td class="abajo_de" >&nbsp;</td>
		  		</tr>		  
			</tbody>
		</table>
	</body>
</html>