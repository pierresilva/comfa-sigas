<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once 'clases' . DIRECTORY_SEPARATOR . 'promotoria.class.php';

$objPromotoria = new Promotoria();
$consulta = $objPromotoria->empresa_con_convenio();
$data = array();
$con = 0;
while( ( $row = $consulta->fetch() ) == true ){
  $data[] = $row;
  $con++;
}
echo ($con>0) ? json_encode($data) : 0;

?>