<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
include_once 'clases' . DIRECTORY_SEPARATOR . 'promotoria.class.php';

$objPromotoria = new Promotoria;

//$banderaLiqui = $_REQUEST["v0"];
$datosLiquidacion = $_REQUEST["v0"];
$datosDetalle = $_REQUEST["v1"];
$usuario = $_SESSION["USUARIO"];

$objPromotoria->inicioTransaccion();
$rsLiquidacion = 0;
$rsLiquidacion  = $objPromotoria->guardar_liquidacion( $datosLiquidacion, $usuario );

$rsDetalLiqui = 0;
//Verifica si la liquidacion se guardo correctamente
if($rsLiquidacion>0){
	
	//Guardar el detalle de la liquidacion
	foreach($datosDetalle as $rowPeriodo){
		$rsDetalLiqui = $objPromotoria->guardar_detalle_liquidacion( $rowPeriodo, $usuario );
		if($rsDetalLiqui==0)
			break;
	}
}

if( $rsLiquidacion == 0 || $rsDetalLiqui == 0 ){
	$objPromotoria->cancelarTransaccion();
	echo 0;
}else{
	$objPromotoria->confirmarTransaccion();
	echo 1;
}
?>