<?php
/*Actualizacion del n�mero de reitero de la visita cuando se generan las cartas de cobros pre juridicos*/
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once 'clases' . DIRECTORY_SEPARATOR . 'promotoria.class.php';

/*$idVisita=$_REQUEST["idVisita"];
$fechaI=$_REQUEST['fechaI']; //fecha inicial de la visita
$fechaF=$_REQUEST['fechaF']; //fecha final de la visita
$estado=$_REQUEST["estado"]; //Estado visita
$nit=$_REQUEST['nit'];
$idAgencia=$_REQUEST['idAgencia'];*/

$objDatosFiltro = $_POST["objDatosFiltro"];

$contador = 0;
$datosActualiza = null;
$arrVisita = array();

$objPromotoria = new Promotoria;
$objPromotoria->inicioTransaccion();

//Obtener las visitas para actualizar
//$arrDatosConsulta = array("idvisita"=>$idVisita,"estado"=>$estado,"nit"=>$nit,"fechavisita"=>array("I"=>$fechaI,"F"=>$fechaF),"idagencia"=>$idAgencia); 
//$datosConsulta = $objPromotoria->empresa_liquidacion($arrDatosConsulta);
$datosConsulta = $objPromotoria->buscar_liquidacion($objDatosFiltro);

foreach ($datosConsulta as $rowVisita){
	//Verificar si ya se actualizo la visita
	//La visita puede tener hasta 2 reiteros
	if(!in_array($rowVisita["idvisita"],$arrVisita) && $rowVisita["numeroreitero"]<2){
		$datosActualiza = array("numeroreitero"=>($rowVisita["numeroreitero"]+1),"idvisita"=>$rowVisita["idvisita"]);
		if($objPromotoria->actualizar_numero_reitero_visita( $datosActualiza ) == false ){
			$contador++;
		}
	}
	$arrVisita[]=$rowVisita["idvisita"];
}

if($contador==0){
	$objPromotoria->confirmarTransaccion();
	echo 1;
}else{
	$objPromotoria->cancelarTransaccion();
	echo 0;
} 
?>