<?php
	set_time_limit(0);
	ini_set("display_errors",'1');
	date_default_timezone_set("America/Bogota");
	
	$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
	include_once  $root;
	
	$url=$_SERVER['PHP_SELF'];
	include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'auditoria.php';
	auditar($url);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>::Promotoria | Paz y salvo::</title>
	<link href="../css/Estilos.css" rel="stylesheet" type="text/css" />
	<link href="../css/estilo_tablas.css" rel="stylesheet" type="text/css" />
	<link type="text/css" href="../css/formularios/base/ui.all.css"
		rel="stylesheet" />
	<link href="../css/marco.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../js/jquery-1.4.2.js"></script>
	<script type="text/javascript" src="../js/formularios/ui/ui.core.js"></script>
	<script type="text/javascript" src="../js/formularios/ui/ui.dialog.js"></script>
	<script type="text/javascript" src="../js/effects.Jquery.js"></script>
	<script type="text/javascript" src="../js/comunes.js"></script>
	<script type="text/javascript" src="../js/jquery.tablesorter.js"></script>
	<script type="text/javascript" src="../js/jquery.tablesorter.pager.js"></script>
	<script type="text/javascript" src="js/pazysalvo.js"></script>
	<script type="text/javascript">
				shortcut.add("Shift+F",function() {
						var URL=src();
						var url=URL+"aportes/trabajadores/consultaTrabajador.php";
			    		window.open(url,"_blank");
			    	},{
						'propagate' : true,
						'target' : document 
			    });        
			</script>
	<style type="text/css">
	.sortable {
		border: 1px solid #eaeaea;
		margin: 10px auto
	}
	
	.sortable th {
		background: #f3f3f3;
		text-align: left;
		color: #666;
		border: 1px solid #eaeaea;
		border-right: none;
		padding: 4px
	}
	
	.sortable th h3 {
		padding: 0 18px;
		margin: 0;
		font: 11px Verdana, Geneva, sans-serif
	}
	
	.sortable td {
		padding: 6px 6px 6px 10px;
		border: 1px solid #eaeaea;
		font-size: 10px;
		text-align: left
	}
	
	.sortable .head h3 {
		background: url(../imagenes/imagesSorter/sort.gif) 7px center no-repeat;
		cursor: pointer;
		padding: 0 18x
	}
	
	.sortable .desc h3 {
		background: url(../imagenes/imagesSorter/desc.gif) 7px center no-repeat;
		cursor: pointer;
		padding: 0 18px
	}
	
	.sortable .asc h3 {
		background: url(../imagenes/imagesSorter/asc.gif) 7px center no-repeat;
		cursor: pointer;
		padding: 0 18px
	}
	
	.sortable .head:hover,.sortable .desc:hover,.sortable .asc:hover {
		color: #000
	}
	
	.sortable .evenrow td {
		background: #ecf2f6
	}
	
	.sortable .oddrow td {
		background: #fff
	}
	
	.sortable td.evenselected {
		background: #F8F8F8
	}
	
	.sortable td.oddselected {
		background: #ffffff
	}
	
	#controls {
		margin: 0 auto;
		height: 20px; /*width:800px;*/
		overflow: hidden;
	}
	
	#perpage {
		float: left;
		width: 200px;
		margin-left: 40px;
	}
	
	#perpage select {
		float: left;
		font-size: 11px
	}
	
	#perpage label {
		float: left;
		margin: 2px 0 0 5px;
		font-size: 10px
	}
	
	#navigation {
		float: left; /* width:600px;*/
		text-align: center
	}
	
	#navigation img {
		cursor: pointer
	}
	
	#text {
		float: left;
		width: 200px;
		text-align: left;
		margin-first: 2px;
		font-size: 10px
	}
	
	.pager {
		margin: 10px;
	}
	</style>
</head>
<body>
	<table border="0" align="center" cellpadding="0" cellspacing="0" style="margin-top: 20px;">
		<tbody>
			<!-- ESTILOS SUPERIOR TABLA-->
			<tr>
				<td width="13" height="29" class="arriba_iz">&nbsp;</td>
				<td class="arriba_ce"><span class="letrablanca">::&nbsp;Empresas a Paz y Salvo::</span></td>
				<td width="13" class="arriba_de" align="right">&nbsp;</td>
			</tr>

			<!-- ESTILOS ICONOS TABLA-->
			<tr>
				<td class="cuerpo_iz">&nbsp;</td>
				<td class="cuerpo_ce">
					<!--<img src="../imagenes/menu/informacion.png" name="iconInfo" width="16" id="iconInfo" style="border:none; cursor:pointer" title="Manual" onClick="mostrarAyuda();" /> -->
					<img src="../imagenes/menu/notas.png" name="iconSugerencia"
					width="16" height="16" id="iconSugerencia" style="cursor: pointer"
					title="Colaborac&oacute;on en l&oacute;nea" onClick="notas();" />
				</td>
				<td class="cuerpo_de">&nbsp;</td>
			</tr>

			<!-- ESTILOS MEDIO TABLA-->
			<tr>
				<td class="cuerpo_iz">&nbsp;</td>
				<td class="cuerpo_ce">
					<!-- TABLA DE EMPRESAS SIN CONVENIO DE PAGO	 -->

					<table cellpadding="0" cellspacing="0" border="0"
						class="sortable hover" id="tablaPaz">
						<caption style="text-align: left; padding: 5px; font-size: 11px;">
							<span></span>
						</caption>
						<thead>
							<tr>
								<th class="head"><h3>
										<strong>No visita</strong>
									</h3></th>
								<!-- <th class="head"><h3>
										<strong>No liquidaci&oacute;n</strong>
									</h3></th>-->
								<th class="head"><h3>
										<strong>Nit</strong>
									</h3></th>
								<th class="head"><h3>
										<strong>Empresa</strong>
									</h3></th>
								<th class="head">
									<h3>
										<strong>Estado Visita</strong>
									</h3></th>
								<th class="head"><h3>
										<strong>Causal</strong>
									</h3></th>
								<th class="head"><h3>
										<strong>Liquidaci&oacute;n</strong>
									</h3></th>
								<th class="head"><h3>
										<strong>Agencia</strong>
									</h3></th>
								<th><h3>
										<strong>Seleccionar</strong>
									</h3></th>
							</tr>
						</thead>
						<tbody>

						</tbody>
					</table> <!-- BOTONES DE ORDENACION -->
					<div id="pager" class="pager">
						<div id="perpage">
							<select class="pagesize">
								<option selected="selected" value="5">5</option>
								<option value="20">20</option>
								<option value="30">30</option>
								<option value="40">40</option>
							</select> <label>Registros Por P&aacute;gina</label>
						</div>
						<div id="navigation">
							<img src="../imagenes/imagesSorter/first.gif" class="first" /> <img
								src="../imagenes/imagesSorter/previous.gif" class="prev" /> <img
								src="../imagenes/imagesSorter/next.gif" class="next" /> <img
								src="../imagenes/imagesSorter/last.gif" class="last" /> <label>P&aacute;gina</label>
							<input type="text" class="pagedisplay boxfecha"
								readonly="readonly" />
						</div>

					</div>

				</td>
				<td class="cuerpo_de">&nbsp;</td>
			</tr>

			<!-- CONTENIDO TABLA-->
			<tr>
				<td class="cuerpo_iz">&nbsp;</td>
				<td class="cuerpo_ce"></td>
				<td class="cuerpo_de">&nbsp;</td>
			</tr>

			<!-- ESTILOS PIE TABLA-->
			<tr>
				<td class="abajo_iz">&nbsp;</td>
				<td class="abajo_ce"></td>
				<td class="abajo_de">&nbsp;</td>
			</tr>

		</tbody>
	</table>
	<!-- Dialgo para cmabiar estado de la empresa -->
	<div id="dlgPaz" title="::Datos empresa::">
		<input id="idempresa" type="hidden" />
		<p align="left">
			Modificar estado de la visita No.<strong id="idVisitaSeg"></strong>
		</p>
		<table border="0" cellpadding="5" id="tablaPazDatos" align="center"
			class="tablero" width="100%">
			<tr>
				<td><strong>Nit</strong></td>
				<td class="datosE"></td>
				<td><strong>Empresa</strong></td>
				<td class="datosE"></td>
			</tr>
			<tr>
				<td><strong>Causal</strong></td>
				<td class="datosE"></td>
				<td><strong>Fecha</strong></td>
				<td><?php echo date("m-d-Y"); ?></td>
			</tr>
			<tr>
				<td><strong>Observaci&oacute;n</strong></td>
				<td colspan="3">
					<textarea id="observacion" class="boxlargo" maxlength="95"></textarea>
					<span class="clsObligatorio">&nbsp;&nbsp;&nbsp;</span>
				</td>
			</tr>
		</table>

	</div>

	<!-- colaboracion en linea -->

	<div id="dialogo-archivo" title="Archivo banco">
		<div id="progreso"
			style="display: none; font-size: 15pt; font-weight: bold;">
			Procesado(s) <span id="pg">0</span> de <span id="tt"></span>
			archivo(s)
		</div>
		<div id="log"></div>
	</div>
	<div id="dialog-form2" title="Colaboraci&oacute;n en l&iacute;nea"
		style="display: none">
		<p>Por favor diligencie este formulario para enviar comentarios,
			errores o falencias encontradas en el proceso. M&aacute;ximo 250
			caracteres</p>
		<label>Tus comentarios:</label><br />
		<textarea name="notas" id="notas" cols="60" , rows="10"></textarea>
	</div>
	<!--fin colaboracion en linea-->


	<!-- Manual Ayuda -->
	<div id="ayuda" title="::Manual Paz y salvo de aportes parafiscales::"></div>
</body>
</html>