<?php

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';


$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

$idVisita = $_POST["idVisita"];

if($idVisita==""){
	echo 0;
}else{
	
	$resultado = 0;
	$sentencia = $db->conexionID->prepare ( "EXEC [kardex].[sp_010_Procesar_Periodo_Liquidado_Por_Visita]
			@id_visita = '$idVisita'
			, @usuario = '{$_SESSION["USUARIO"]}'
			, @resultado = :resultado" );
	$sentencia->bindParam ( ":resultado", $resultado, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 4000 );
	$sentencia->execute();
	if ($resultado > 0)
		echo 1;
	else
		echo 0;
}