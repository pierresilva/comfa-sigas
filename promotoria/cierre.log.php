<?php 
/**
 * Script de la logica del formulario definicion_novedad
 *
 * @author Oscar
 * @version 0
 */

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once 'clases' . DIRECTORY_SEPARATOR . 'promotoria.class.php';

/**
 * Contiene los datos de retorno al formulario
 * @var array
 */
$arrMensaje = array('error'=>0,'data'=>null);

/*Verificar si accion existe*/
$accion = isset($_POST['accion'])?$_POST['accion']:'';

$usuario = $_SESSION["USUARIO"];

switch($accion){
    case 'I':
    	
        $datos = $_POST["datos"];
        	
        $objPromotoria = new Promotoria;
        
        $resultado = $objPromotoria->guardar_cierre($datos,$usuario);

        $arrMensaje["error"] = 0;
            
        if($resultado == 0){
        	$arrMensaje["error"] = 1;
       	}

        break;
	case 'CERRAR_PERIODO':
        
    	$datos = $_POST["datos"];
        
    	$objPromotoria = new Promotoria;
    	
    	$resultado = 0;
    	
    	$consulta = $objPromotoria->buscar_cierre( array("id_cierre"=>$datos["id_cierre"]) );
    	if(count($consulta)>0){
    		$consulta = $consulta[0];
    		
    		$arrDatos = array(
    				"id_cierre"=>$consulta["id_cierre"]
    				, "periodo"=>$consulta["periodo"]
    				, "fecha_cierre"=>$consulta["fecha_cierre"]
    				, "id_estado"=>$datos["id_estado"]);
    		
    		$resultado = $objPromotoria->actuliza_cierre($arrDatos,$usuario);
    	}
    	
        	 
    	$arrMensaje["error"] = 0;
        	 
    	if($resultado == 0){
    		$arrMensaje["error"] = 1;
    	}
    case 'G':

    	$datos = $_POST["datos"];
    	 
    	$objPromotoria = new Promotoria;
    	
    	$resultado = $objPromotoria->ejecutar_sp_generar_datos_cierre($datos["periodo"],$usuario);
    	
    	$arrMensaje["error"] = 0;
    	
    	if($resultado == 0){
    		$arrMensaje["error"] = 1;
    	}
    	
        break;
}

echo ($accion != '') ? json_encode($arrMensaje) : "";
?>