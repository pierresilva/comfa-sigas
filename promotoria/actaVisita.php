<?php 
	set_time_limit(0);
	ini_set("display_errors",'1');
	date_default_timezone_set("America/Bogota");
	$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
	include_once  $root;
	include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';
	$url=$_SERVER['PHP_SELF'];
	include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'auditoria.php';
	include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
	include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
	include_once $raiz.DIRECTORY_SEPARATOR.'promotoria'.DIRECTORY_SEPARATOR.'clases' . DIRECTORY_SEPARATOR . 'promotoria.class.php';
	include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'p.definiciones.class.php';
	include_once $raiz.DIRECTORY_SEPARATOR.'seguridad'.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'agencia.php';
	
	auditar($url);
	
	try{
		$db = IFXDbManejador::conectarDB();
		if( $db->conexionID==null ){
			throw new Exception($db->error);
		}
	}catch(Exception $e){
		echo $e->getMessage();
		exit();
	}

	$usuario = $_SESSION["USUARIO"];
	$idPromotor = 0;
	$nombrePromotor = "";
	$comboCausal = "";
	$comboAgencia = "";
	$objDefinicion = new Definiciones;
	$objPromotoria = new Promotoria;
	
	//Buscar el usuario como promotor
	$rsDatosPromotor = $objPromotoria->buscar_usuario_promotor($usuario);
	if(count($rsDatosPromotor)>0){
		$rsDatosPromotor = $rsDatosPromotor[0];
		$idPromotor = $rsDatosPromotor["idpromotor"];
		$nombrePromotor = $rsDatosPromotor["promotor"]; 
	}
	
	//Obtener el combo de las causales
	$consultaCausal = $objDefinicion->mostrar_datos(44, 2);
	while($row=mssql_fetch_array($consultaCausal)){
		$comboCausal .= "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
	}
	
	//Obtener el combo de las agencias 
	$datosAgencias = Agencia::listarTodas();
	foreach ($datosAgencias as $rowAgencia){
		$comboAgencia .= "<option value=".$rowAgencia->getIdAgencia().">".$rowAgencia->getAgencia()."</option>";
	}
	
	//Obtener la fecha de la version
	$fecver = date('Ymd h:i:s A',filectime('actaVisita.php'));
	
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
	<head>
		<title>::Promotoria | Acta visita::</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		
		<link type="text/css" href="<?php echo URL_PORTAL; ?>newcss/Estilos.css" rel="stylesheet">
		<link type="text/css" href="<?php echo URL_PORTAL; ?>newcss/css.1.8/jquery-ui-1.8.17.custom.css" rel="stylesheet">
		<link type="text/css" href="<?php echo URL_PORTAL; ?>newcss/marco.css" rel="stylesheet">
		
		<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/1.6/jquery-1.6.2.min.js"></script>
		<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/1.6/jquery-ui-1.8.16.custom.min.js"></script>
		<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/comunes.js"></script>
		<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/direccion.js"></script>
		<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/jquery.combos.js"></script>
		
		<script type="text/javascript" src="js/actaVisita.js"></script>

	</head>
	<body>
		<table width="85%" border="0" align="center" cellpadding="0" cellspacing="0">
			<tbody>
				<!-- ESTILOS SUPERIOR TABLA-->
				<tr>
					<td width="13" height="29" class="arriba_iz">&nbsp;</td>
					<td class="arriba_ce">
						<span class="letrablanca">::&nbsp;Acta de visita&nbsp;::</span>
						<div style="text-align:right;float:right;">
							<?php echo 'Versi&oacute;n: ' . $fecver; ?>
						</div>
					</td>
					<td width="13" class="arriba_de" align="right">&nbsp;</td>
				</tr>
				<!-- ESTILOS ICONOS TABLA-->
				<tr>
					<td class="cuerpo_iz">&nbsp;</td>
					<td class="cuerpo_ce">
						<img src="../imagenes/menu/nuevo.png" name="iconNuevo" width="16" height="16" id="iconNuevo" style="cursor:pointer" title="Nuevo" onClick="nuevaVisita();" />
						<img src="../imagenes/menu/grabar.png" name="iconGuardar" width="16"  height=16  style="cursor:pointer" title="Guardar Visita" onClick="guardarVisita();" id="bGuardar" />
						<img src="../imagenes/menu/modificar.png" name="iconActualizar" width="16"  height=16 id="iconActualizar" style="cursor:pointer;display:none;" title="Actualizar Visita" onClick="actualizarVisita();" />
						<img src="../imagenes/menu/refrescar.png" name="iconLimpiar" width="16"  height=16 id="iconLimpiar" style="cursor:pointer" title="Limpiar Campos" onClick="limpiarCampos();" />
						<img src="../imagenes/menu/visitas.png" name="iconVisitas" width="16" height="16" id="iconVisitas" style="cursor: pointer" title="Visitas por empresa" onClick="visitasEmpresa();" />         
					</td>
					<td class="cuerpo_de">&nbsp;</td>
				</tr>
				<!-- ESTILOS MEDIO TABLA-->
				<tr>
					<td class="cuerpo_iz">&nbsp;</td>
					<td class="cuerpo_ce"><div id="resultado" style="color:#FF0000"></div></td>
					<td class="cuerpo_de">&nbsp;</td>
				</tr>  
				<!-- CONTENIDO TABLA-->
				<tr>
					<td class="cuerpo_iz">&nbsp;</td>
					<td class="cuerpo_ce">
						<table width="90%" align="center" cellspacing="0" border="0" class="tablero">
							<tbody>
								<tr>
									<th colspan="4">DATOS DEL APORTANTE</th>
								</tr>
								<tr>
									<td width="14%">Nit</td>
									<td width="32%">
										<input type="text" name="txtNit" id="txtNit" class="box1 element-required" onBlur= "buscarEmpresa();" />
										<input type="hidden" name="hidIdEmpresa" id="hidIdEmpresa" /> 
									</td>
									<td width="16%">Empresa</td>
									<td>
										<strong>
											<div  id="divEmpresa" align="center"></div>
										</strong>
									</td>
								</tr>
								<tr>
									<td>Estado</td>
									<td><div id="divEstado"></div></td>
									<td>Direcci&oacute;n</td>
									<td>
										<input type="text" name="txtDireccionEmpresa" id="txtDireccionEmpresa" class="boxlargo element-required" onfocus="direccion(this,document.getElementById('txtTelefonoEmpresa'));"/>
										<input type="hidden" name="hidDireccionEmpresa" id="hidDireccionEmpresa"/>
										<span class="clsObligatorio">&nbsp;&nbsp;&nbsp;</span>
									<!-- <div id="divDireccion"></div>-->
									</td>
								</tr>
								<tr>
									<td>Tel&eacute;fono</td>
									<td>
										<!-- <div id="divTelefono"></div>-->
										<input type="text" name="txtTelefonoEmpresa" id="txtTelefonoEmpresa" class="box1 element-required" />
										<input type="hidden" name="hidTelefonoEmpresa" id="hidTelefonoEmpresa"/>
										<span class="clsObligatorio">&nbsp;&nbsp;&nbsp;</span>
									</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
								<tr>
									<td>Departamento</td>
									<td>
										<select name="cboDepto2" id="cboDepto2" class="box1 element-required"></select>
										<input type="hidden" name="hidDepartamentoEmpresa" id="hidDepartamentoEmpresa"/>
										<span class="clsObligatorio">&nbsp;&nbsp;&nbsp;</span>
									</td>
									<td>Ciudad</td>
									<td>
										<select name="cboCiudad2" id="cboCiudad2" class="box1 element-required"></select>
										<input type="hidden" name="hidCiudadEmpresa" id="hidCiudadEmpresa"/>
										<span class="clsObligatorio">&nbsp;&nbsp;&nbsp;</span>
									</td>
								</tr>
								<tr>
									<td>Identificaci&oacute;n Rep. Legal</td>
									<td>
										<input type="text" name="txtIdentRepreLegal" id="txtIdentRepreLegal" class="box1 element-required" />
										<input type="hidden" name="hidIdRepreLegal" id="hidIdRepreLegal" />
										<input type="hidden" name="hidIdRepreLegalInici" id="hidIdRepreLegalInici" />
										<span class="clsObligatorio">&nbsp;&nbsp;&nbsp;</span>
									</td>
									<td>Represent&aacute;nte Legal</td>
									<td><div id="divRepresentanteLegal"></div></td>
								</tr>
								<tr>
									<th colspan="4">DATOS DE LA VISITA</th>
								</tr>
								<tr>
									<td>No de visita</td>
									<td>
										<input type="text" name="txtIdVisita" id="txtIdVisita"  class="boxfecha element-required" onBlur="verificarVisita();" />
										<span class="clsObligatorio">&nbsp;&nbsp;&nbsp;</span>
									</td>
									<td>Fecha de visita</td>
									<td>
										<input type="text" name="txtFechaVisita" id="txtFechaVisita" class="boxfecha element-required" />
										<input type="hidden" name="hidFechaVisita" id="hidFechaVisita"/>
										
										<span class="clsObligatorio">&nbsp;&nbsp;&nbsp;</span>
									</td>
								</tr>
								<tr>
									<td>Causal</td>
									<td>
										<select name="cmbCausal" id="cmbCausal" class="box1 element-required" >
							          		<option value="0">Seleccione..</option>
								          	<?php echo $comboCausal;?>
								        </select>
								        <span class="clsObligatorio">&nbsp;&nbsp;&nbsp;</span>
									</td>
									<td>&iquest;Liquidaci&oacute;n?</td>
									<td>
										SI <input type="radio" name="radLiquidacion" value="S" />
										NO <input type="radio" name="radLiquidacion" value="N" checked="true"/>
										<span class="clsObligatorio">&nbsp;&nbsp;&nbsp;</span>
									</td>
								</tr>
								<tr>
									<td>&iquest;Apelaci&oacute;n?</td>
									<td>
										SI <input type="radio" name="radApelacion" value="S" />
										NO <input type="radio" name="radApelacion" value="N" checked="true"/>
										<span class="clsObligatorio">&nbsp;&nbsp;&nbsp;</span>
									</td>
									<td>Fecha apelaci&oacute;n</td>
									<td>
										<input type="text" name="txtFechaApelacion" id="txtFechaApelacion" class="boxfecha" readonly="readonly"/>
								        <span class="clsObligatorio clsFechaApelacion" style="display:none;">&nbsp;&nbsp;&nbsp;</span>
									</td>
									
								</tr>
								<tr>
									<td>Asesor Juridico Interno</td>
									<td>
										<select name="cmbIdAsesoJuridInter" id="cmbIdAsesoJuridInter" class="boxmediano">
											<option value="0">Seleccione..</option>
											
										</select>
									</td>
									<td>Agencia</td>
									<td>
										<select name="cmbIdAgencia" id="cmbIdAgencia" class="box1 element-required">
											<option value="0">Seleccione..</option>
											<?php echo $comboAgencia;?>
										</select>
										<span class="clsObligatorio">&nbsp;&nbsp;&nbsp;</span>
									</td>
								</tr>
								<tr>
									<td>Asesor Juridico Externo</td>
									<td>
										<select name="cmbIdAsesoJuridExter" id="cmbIdAsesoJuridExter" class="boxmediano">
											<option value="0">Seleccione..</option>
										</select>
									</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td>Conclusi&oacute;n</td>
									<td colspan="3"><textarea name="txaConclusion" id="txaConclusion" cols="45" rows="2" class="area" maxlength="1000"></textarea></td>
								</tr>
								<tr>
									<td>Observaci&oacute;n</td>
									<td colspan="3"><textarea name="txaObservacion" id="txaObservacion" cols="45" rows="2" class="area" maxlength="1000"></textarea></td>
								</tr>
								<tr>
									<td>Promotor(a)</td>
									<td>
										<input type="hidden" name="hidIdPromotor" id="hidIdPromotor" value="<?php echo $idPromotor; ?>" />
										<div id="divPromotor"><?php echo $nombrePromotor;?></div>
									</td>
									<td>Usuario</td>
									<td>
										<input type="text" name="txtUsuario" id="txtUsuario" disabled="disabled"  class="box1 element-required" value="<?php echo $_SESSION['USUARIO']; ?>" />
									</td>
								</tr>
							</tbody>
						</table>
					<td class="cuerpo_de">&nbsp;</td>
				</tr>
				<!-- ESTILOS PIE TABLA-->
				<tr>
					<td class="abajo_iz" >&nbsp;</td>
					<td class="abajo_ce" ></td>
					<td class="abajo_de" >&nbsp;</td>
				</tr>
			</tbody>
		</table>
		<!-- TABLA VISITAS -->
		<div id="divVisitas" title="::VISITAS POR EMPRESA::" style="display: none;">
			<h5 id="h5DatosEmpreVisit"></h5>
			<table id="tblVisitas" width="100%" border="0" cellpadding="5" cellspacing="0" class="tablero">
				<thead>
					<tr>
						<th width="8%" scope="col"><strong>N&uacute;mero</strong></th>
						<th width="8%" scope="col"><strong>Fecha</strong></th>
						<th width="15%" scope="col"><strong>Promotor</strong></th>
						<th width="35%" scope="col"><strong>Conclusi&oacute;n</strong></th>
						<th width="20%" scope="col"><strong>Novedad</strong></th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
		
		<!-- DIV DIRECCIONES --> 
		<div id="dialog-form" title="Formulario de direcciones"></div>

	</body>
</html>
