<?php
/* autor:       Andres F Lara
 * fecha:       Feb 26 de 2011
 * objetivo:    
 */
date_default_timezone_set('America/Bogota');
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$url=$_SERVER['PHP_SELF'];
include_once $raiz.DIRECTORY_SEPARATOR.'phpComunes'.DIRECTORY_SEPARATOR.'auditoria.php';
auditar($url);

include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'p.definiciones.class.php';
$objClase=new Definiciones();
$fecha=date("m/d/Y");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="es">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<title>::Promotoria | Promotores::</title>
		<link href="../css/Estilos.css" rel="stylesheet" type="text/css" />
		<link type="text/css" href="../css/estilo_tablas.css" rel="stylesheet"/>
		<link type="text/css" href="../css/formularios/base/ui.all.css" rel="stylesheet" />
		<link href="../css/marco.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="../js/jquery-1.4.2.js"></script>
		<script type="text/javascript" src="../js/formularios/ui/ui.core.js"></script>
		<script type="text/javascript" src="../js/formularios/ui/ui.tabs.js"></script>
		<script type="text/javascript" src="../js/formularios/ui/ui.dialog.js"></script>
		<script type="text/javascript" src="../js/formularios/ui/ui.datepicker.js"></script>
		<script type="text/javascript" src="../js/effects.Jquery.js"></script>
		<script type="text/javascript" src="../js/comunes.js"></script>
		<script type="text/javascript" src="js/nuevoPromotor.js"></script>
		<script type="text/javascript">
			shortcut.add("Shift+F",function() {
				var URL=src();
				var url=URL+"aportes/trabajadores/consultaTrabajador.php";
		    	window.open(url,"_blank");
		    },{
				'propagate' : true,
				'target' : document 
		    });        
		</script>
	</head>
	<body>
		<form name="forma">
			<!-- TABLA VISIBLE CON BOTONES -->
			<table width="70%" border="0" cellspacing="0" cellpadding="0" align="center">
        		<tr>
        			<td width="13" height="29" class="arriba_iz">&nbsp;</td>
        			<td class="arriba_ce"><span class="letrablanca">::Administraci&oacute;n Promotores::</span></td>
        			<td width="13" class="arriba_de" align="right">&nbsp;</td>
        		</tr>      
				<tr>
					<td class="cuerpo_iz">&nbsp;</td>
					<td class="cuerpo_ce">
						<img src="../imagenes/tabla/spacer.gif" width="1" height="1">
						<img src="../imagenes/spacer.gif" width="1" height="1">
						<img src="../imagenes/menu/nuevo.png" title="Nuevo" width="16" height="16" style="cursor:pointer" onClick="nuevoPromotor();">
						<img src="../imagenes/spacer.gif" width="1" height="1">
						<img src="../imagenes/menu/grabar.png" width:16 height=16 style="cursor:pointer" title="Guardar" onclick="guardarPromotor()" id="bGuardar"/> 
						<img src="../imagenes/spacer.gif" width="1" height="1">
						<img src="../imagenes/menu/modificar.png" name="iconActualizar" width="16"  height=16 id="iconActualizar" style="cursor:pointer;display:none;" title="Actualizar Liquidacion" onClick="actualizarPromotor();" />
						<img src="../imagenes/spacer.gif" width="1" height="1">
						<img src="../imagenes/menu/refrescar.png" width="16" height="16" style="cursor:pointer" title="Limpiar campos" onClick="limpiarCampos();">
						<img src="../imagenes/spacer.gif" width="1" height="1">
						<img src="../imagenes/menu/informacion.png" width="16" height="16" style="border:none; cursor:pointer" title="Manual" onClick="mostrarAyuda();" />
						<img src="../imagenes/menu/notas.png" width="16" height="16" style="cursor: pointer" title="Colaborac�on en l�nea" onclick="notas();" />
					</td>
 					<td class="cuerpo_de">&nbsp;</td>
				</tr> 
   				<tr>
    				<td class="cuerpo_iz">&nbsp;</td>
    				<td class="cuerpo_ce"><div id="resultado" style="color:#FF0000"></div></td>
    				<td class="cuerpo_de">&nbsp;</td>
  				</tr>  
				<tr>
 					<td class="cuerpo_iz">&nbsp;</td>
 					<td class="cuerpo_ce">
						<table width="90%" align="center" border="0" cellspacing="0" class="tablero">
							<tr>
  								<td width="25%">Tipo Documento</td>
  								<td width="25%">
  									<select name="tipoI" id="tipoI" class="box1" class="clsValidaText">
	  									<option val="0">Seleccione</option>
	  									<?php
											$consulta=$objClase->mostrar_datos(1, 2);	
											while($row=mssql_fetch_array($consulta)){
												echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
											}
										?>
  									</select>
  									<img src="../imagenes/menu/obligado.png" alt="" width="12" height="12" />
  								</td>
 								<td width="25%">N&uacute;mero</td>
  								<td width="25%"><input name="txtNumero" type="text" class="box1 clsValidaText" id="txtNumero" onblur="buscarPersona(this);" />
    								<img src="../imagenes/menu/obligado.png" alt="" width="12" height="12" />
    							</td>
							</tr>
							<tr>
  								<td>Nombres</td>
  								<td colspan="3" id="tNombre"></td>
							</tr>
							<tr>
  								<td>Fecha inicio</td>
  								<td>
  									<input name="fecInicio" type="text" class="boxfecha clsValidaText" id="fecInicio"  value="<?php echo $fecha;?>" readonly="readonly" />
  									<img src="../imagenes/menu/obligado.png" alt="" width="12" height="12" />
  								</td>
  								<td>Fecha final</td>
  								<td>
  									<input name="fecFinal" type="text" class="boxfecha clsValidaText" id="fecFinal" />
  									<img src="../imagenes/menu/obligado.png" alt="" width="12" height="12" />
  								</td>
							</tr>
							<tr>
  								<td>Estado</td>
  								<td>
  									<select name="estado" class="box1 clsValidaText" id="estado">
    									<option value="0">Seleccione</option>
    									<option value="A">ACTIVO</option>
    									<option value="I">INACTIVO</option>
  									</select>
    								<img src="../imagenes/menu/obligado.png" alt="" width="12" height="12" />
    							</td>
  								<td>Usuario</td>
  								<td><input name="usuario" type="text" class="box1" id="usuario" readonly="readonly" value="<?php echo $_SESSION ['USUARIO']; ?>"  /></td>
							</tr>
						</table>
						<input id="idpersona" type="hidden"  />
						<input name="hidIdPromotor" id="hidIdPromotor" type="hidden"  />
						<div id="errores" align="left"></div>
					<td class="cuerpo_de">&nbsp;</td><!-- FONDO DERECHA -->
				</tr>
				<tr>
					<td class="abajo_iz" >&nbsp;</td>
					<td class="abajo_ce" ></td>
					<td class="abajo_de" >&nbsp;</td>
				</tr>
			</table>  
			<!--colaboracion en linea-->
			<div id="dialogo-archivo" title="Archivo banco">
				<div id="progreso" style="display: none; font-size: 15pt; font-weight: bold;">Procesado(s) <span id="pg">0</span> de <span id="tt"></span> archivo(s)</div>
				<div id="log"></div>
			</div>
			<div id="dialog-form2" title="Colaboraci&oacute;n en l&iacute;nea" style="display:none">
				<p>Por favor diligencie este formulario para enviar comentarios, errores o falencias encontradas en el proceso. M&aacute;ximo 250 caracteres </p>
				<label>Tus comentarios:</label><br />
				<textarea name="notas" id="notas" cols="60", rows="10"></textarea>
			</div>
			<!-- fin colaboracion -->
			<!-- ayuda en linea -->
			<div id="ayuda" title="Manual .:.Administraci�n de Promotores" style="background-image:url(../imagenes/FondoGeneral0.png)"></div>
		</form>
	</body>
</html>
