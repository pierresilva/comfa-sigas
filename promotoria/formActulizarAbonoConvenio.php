<?php	
	$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
	include_once  $root;
	
	$url=$_SERVER['PHP_SELF'];
	include_once $raiz.DIRECTORY_SEPARATOR.'phpComunes'.DIRECTORY_SEPARATOR.'auditoria.php';
	auditar($url);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<title>::Promotoria | Abono convenio::</title>
		
		<link href="../css/Estilos.css" rel="stylesheet" type="text/css" />
		<link href="../css/marco.css" rel="stylesheet" type="text/css" />
		<link href="../newcss/css.1.8/jquery-ui-1.8.17.custom.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="../newjs/1.6/jquery-1.6.2.min.js"></script>
		<script type="text/javascript" src="../newjs/1.6/jquery-ui-1.8.16.custom.min.js"></script>
		<script type="text/javascript" src="../js/jquery.alphanumeric.js"></script>
		<script type="text/javascript" src="../js/comunes.js"></script>
		<script type="text/javascript" src="js/actulizarAbonoConvenio.js"></script>
	</head>
	<body>
		<table width="70%" border="0" align="center" cellpadding="0" cellspacing="0">
			<tbody>
		  	<!-- ESTILOS SUPERIOR TABLA-->
		  	<tr>
			    <td width="13" height="29" class="arriba_iz">&nbsp;</td>
			    <td class="arriba_ce"><span class="letrablanca">::&nbsp;Abono convenio de pago&nbsp;::</span></td>
			    <td width="13" class="arriba_de" align="right">&nbsp;</td>
		  	</tr> 
		  	<!-- ESTILOS ICONOS TABLA-->
		  	<tr>
		    	<td class="cuerpo_iz">&nbsp;</td>
		    	<td class="cuerpo_ce">
					<img src="../imagenes/menu/nuevo.png" name="iconNuevo" width="16" height="16" id="iconNuevo" style="cursor:pointer" title="Nuevo" onClick="nuevo();" /> 
					<img width="3" height="1" src="../imagenes/spacer.gif" />
					<img width="3" height="1" src="../imagenes/spacer.gif" />
					<img src="../imagenes/menu/modificar.png" name="iconActualizar" width="16"  height=16 id="iconActualizar" style="cursor:pointer" title="Actualizar" onClick="actualizar();" />      
		    	</td>
		    	<td class="cuerpo_de">&nbsp;</td>
		  	</tr>
		  	<!-- ESTILOS MEDIO TABLA-->
		  	<tr>
		    	<td class="cuerpo_iz">&nbsp;</td>
		    	<td class="cuerpo_ce"></td>
		    	<td class="cuerpo_de">&nbsp;</td>
		  	</tr>  
		  	<!-- CONTENIDO TABLA-->
		  	<tr>
		   		<td class="cuerpo_iz">&nbsp;</td>
		   		<td class="cuerpo_ce">
					<table cellspacing="0" width="90%" border="0" class="tablero" align="center">
		      			<tbody>
			    			<tr>
			      				<th colspan="4">
			      					DATOS DEL ACUERDO DE PAGO
			      				</th>
			      			</tr>
			    			<tr>
			      				<td colspan="4" style="text-align:center;">Empresa
			      					<select name="cmbEmpresa" id="cmbEmpresa" onchange="buscarFechaConvenio();" class="clsValidaText boxlargo"></select>
			      				</td>
			        		</tr>
			    			<tr>
			      				<th colspan="4">
			      					DISTRIBUCION DE LAS FECHAS
			      				</th>
			      			</tr>
			    			<tr>
			      				<td colspan="4" style="text-align:center;">Fecha de pago
			      					<select name="cmbFechaPago" id="cmbFechaPago" onchange="buscarPeriodoConvenio();" class="clsValidaText boxlargo"></select>
			      				</td>
			      			</tr>
			      			<tr>
			      				<td>Valor Acuerdo</td>
			      				<td width="30%" id="tdValorAcuerdo" class="clsDistrFecha">
			      				</td>
			      				<td width="23%">Valor interes</td>
			      				<td width="23%" id="tdValorInteres" class="clsDistrFecha"></td>
			      			</tr>
			      			<tr>
			      				<td>Valor Total Fecha</td>
			      				<td id="tdValorTotalFecha" class="clsDistrFecha"></td>
			      				<td >Valor abono</td>
			      				<td id="tdValorAbono" class="clsDistrFecha"></td>
			      			</tr>
			      			<tr>
			      				<td>Periodos</td>
			      				<td id="tdPeriodos" colspan="3" class="clsDistrFecha"></td>
			      			</tr>
			      			<tr>
			      				<th colspan="4">
			      					DATOS DEL ABONO
			      				</th>
			      			</tr>
			    			<tr>
			      				<td>Valor Abono</td>
			      				<td>
			      					<input type="text" name="txtValorAbono" id="txtValorAbono" class="box1 clsValidaText" onblur="calcularSaldo();"/>
			      				</td>
			      				<td>Saldo disponible</td>
			      				<td id="tdSaldoDisponible"></td>
			      			</tr>
			      			<tr>
			      				<td>Observacion</td>
			      				<td colspan="3">
			      					<textarea class="area clsValidaText" name="txaObservacion" id="txaObservacion" cols="45" rows="2" maxlength="190" ></textarea>
			      				</td>
			      			</tr>
		      			</tbody>
		      		</table>
					</td>
		    		<td class="cuerpo_de">&nbsp;</td>
		  		</tr>		  
		  		<!-- ESTILOS PIE TABLA-->
		  		<tr>
		    		<td class="abajo_iz" >&nbsp;</td>
		    		<td class="abajo_ce" ></td>
		    		<td class="abajo_de" >&nbsp;</td>
		  		</tr>		  
			</tbody>
		</table>
	</body>
</html>