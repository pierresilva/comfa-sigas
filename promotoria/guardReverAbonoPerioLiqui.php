<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz. DIRECTORY_SEPARATOR .'clases' . DIRECTORY_SEPARATOR . 'p.definiciones.class.php';
include_once 'clases' . DIRECTORY_SEPARATOR . 'promotoria.class.php';

$usuario = $_SESSION["USUARIO"];

//Parametros recibidos
$arrDataFormulario = $_POST['data'];

//var_dump($arrDataFormulario);exit();

$objPromotoria = new Promotoria;

$banderaError = 0;
$objPromotoria->inicioTransaccion();

$estadoVisita = "";
//Actualizar el detalle de la liquidacion
foreach($arrDataFormulario["data_detalle_liquidacion"] as $dataDetalle){
	//Consultar para obtener la observacion
	$arrDataDetalle = $objPromotoria->buscar_detalle_liquidacion(array("iddetalle"=>$dataDetalle["id_detalle"]));
	$arrDataDetalle = $arrDataDetalle[0];
	$estadoVisita = $arrDataDetalle["estadovisita"];
	
	$arrDataActualiza = array(
			"iddetalle"=>$arrDataDetalle["iddetalle"]
			, "periodo"=>$arrDataDetalle["periodo"]
			, "bruto"=>$arrDataDetalle["bruto"]
			, "aportes"=>$arrDataDetalle["aportes"]
			, "neto"=>$arrDataDetalle["neto"]
			, "abono"=>$dataDetalle["abono"]
			, "cancelado"=>'N'
			, "fechaabono"=>$arrDataDetalle["fechaabono"]
			, "bruto_inicial"=>$arrDataDetalle["bruto_inicial"]>0?$arrDataDetalle["bruto_inicial"]:0
			, "aporte_inicial"=>$arrDataDetalle["aporte_inicial"]>0?$arrDataDetalle["aporte_inicial"]:0
			, "neto_inicial"=>$arrDataDetalle["neto_inicial"]>0?$arrDataDetalle["neto_inicial"]:0
			, "periodo_inicial"=>$arrDataDetalle["periodo_inicial"]
			, "observacion"=>$arrDataDetalle["observaciondetalle"]
			);
	
	//Actualizar
	if($objPromotoria->actualizar_detalle_liquidacion_completa($arrDataActualiza,$usuario)==0){
			$banderaError = 1;
			break;
	}
}

//Actualizar la visita
if($banderaError==0 && $estadoVisita=="P"){
	//Actualizar estado visita
	$arrData = array(
			"estado"=>"A"
			, "motivo"=>""
			, "usuariopys"=>""
			, "idvisita"=>$arrDataFormulario["data_liquidacion"]["id_visita"]);

	if($objPromotoria->actualizar_estado_visita($arrData,$usuario)==0){
		$banderaError = 1;
	}
}

//Actualizar el abono del periodo liquidado
if($banderaError==0){
	foreach($arrDataFormulario["data_reverso"] as $dataReverso){		
		if($objPromotoria->guardar_reverso_abono_periodo_liquidado($dataReverso,$usuario)==0){
			$banderaError = 1;
			break;
		}
	}
}

if($banderaError==0){
	$objPromotoria->confirmarTransaccion();
	echo 1;
}else{ 
	$objPromotoria->cancelarTransaccion();
	echo 0;
}

