<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz. DIRECTORY_SEPARATOR .'clases' . DIRECTORY_SEPARATOR . 'p.definiciones.class.php';
include_once 'clases' . DIRECTORY_SEPARATOR . 'promotoria.class.php';

$usuario = $_SESSION["USUARIO"];

//Parametros recibidos
$arrDataFromulario = $_POST['data'];

$objPromotoria = new Promotoria;
$objDefiniciones = new Definiciones();

//Obtener id_causal_estado del abono
$dataDefinicion = $objDefiniciones->buscar_definicion_detalle_definicion(
		array("codigo_definicion"=>"CAUSAL_ABONO_PERIODO_LIQ"
		,"codigo_detalle_definicion"=>"AJUSTE_LIQ"));

$idCausalEstadoAbono = 0;
if(count($dataDefinicion)>0){
	$idCausalEstadoAbono = $dataDefinicion[0]["iddetalledef"]; 
}

$banderaError = 0;
$objPromotoria->inicioTransaccion();

$estadoVisita = "";
//Actualizar el detalle de la liquidacion
foreach($arrDataFromulario["data_detalle_liquidacion"] as $dataDetalle){
	//Consultar para obtener la observacion
	$arrDataDetalle = $objPromotoria->buscar_detalle_liquidacion(array("iddetalle"=>$dataDetalle["id_detalle"]));
	$arrDataDetalle = $arrDataDetalle[0];
	$estadoVisita = $arrDataDetalle["estadovisita"];
	
	$arrDataActualiza = array(
			"iddetalle"=>$dataDetalle["id_detalle"]
			, "periodo"=>$dataDetalle["periodo"]
			, "bruto"=>$dataDetalle["bruto"]
			, "aportes"=>$dataDetalle["aportes"]
			, "neto"=>$dataDetalle["neto"]
			, "abono"=>0
			, "cancelado"=>'N'
			, "fechaabono"=>''
			, "bruto_inicial"=>($arrDataDetalle["bruto_inicial"]>0)?$arrDataDetalle["bruto_inicial"]:0
			, "aporte_inicial"=>($arrDataDetalle["aporte_inicial"]>0)?$arrDataDetalle["aporte_inicial"]:0
			, "neto_inicial"=>($arrDataDetalle["neto_inicial"]>0)?$arrDataDetalle["neto_inicial"]:0
			, "periodo_inicial"=>$arrDataDetalle["periodo_inicial"]
			, "observacion"=>$arrDataDetalle["observaciondetalle"]
			);
	
	//Actualizar
	if($objPromotoria->actualizar_detalle_liquidacion_completa($arrDataActualiza,$usuario)>0){
		//Inactivar los abonos
		$arrData = array("estado"=>'I',"id_causal_estado"=>$idCausalEstadoAbono);
		$arrFiltro = array("id_periodo_liquidacion"=>$dataDetalle["id_detalle"]);
		
		if($objPromotoria->actuliza_estado_abono_periodo_liquidacion($arrData,$arrFiltro,$usuario)==0){
			$banderaError = 1;
			break;
		}
	}else{
		$banderaError = 1;
		break;
	}
}



//Actualizar la visita
if($banderaError==0 && $estadoVisita=="P"){
	//Actualizar estado visita
	$arrData = array(
			"estado"=>"A"
			, "motivo"=>""
			, "usuariopys"=>""
			, "idvisita"=>$arrDataFromulario["data_liquidacion"]["id_visita"]);
	
	if($objPromotoria->actualizar_estado_visita($arrData,$usuario)==0){
		$banderaError = 1;
	}		
}

//Guardar el ajuste
if($banderaError==0){
	foreach($arrDataFromulario["data_ajuste"] as $dataAjuste){
		$arrData = array(
				"valor"=>$dataAjuste["valor"]
				, "periodo"=>$dataAjuste["periodo"]
				, "fecha_ajuste"=>$dataAjuste["fecha_ajuste"]
				, "observacion"=>$dataAjuste["observacion"]
				, "id_causal"=>$dataAjuste["id_causal"]
				, "id_periodo_liquidacion"=>$dataAjuste["id_periodo_liquidacion"]
			);
		
		if($objPromotoria->guardar_ajuste_periodo_liquidado($arrData,$usuario)==0){
			$banderaError = 1;
			break;
		}
	}
}

if($banderaError==0){
	$objPromotoria->confirmarTransaccion();
	echo 1;
}else{ 
	$objPromotoria->cancelarTransaccion();
	echo 0;
}

