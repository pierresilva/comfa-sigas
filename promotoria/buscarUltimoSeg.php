<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once 'clases' . DIRECTORY_SEPARATOR . 'promotoria.class.php';

$idvisita = $_REQUEST['v0'];
$objPromotoria = new Promotoria;
$consulta = $objPromotoria->ultimo_seguimiento( $idvisita );

if( ( $row = $consulta->fetch() ) == true ) 
	echo json_encode($row);
else
	echo 0;
?>