<?php
	date_default_timezone_set('America/Bogota');
	$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
	include_once  $root;
	
	include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
	include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
	$db = IFXDbManejador::conectarDB();
	if($db->conexionID==null){
		$cadena = $db->error;
		echo msg_error($cadena);
		exit();
	}

	$idEmpresa = $_POST['id_empresa'];
	$periodo = $_POST['periodo'];

	$sql = "SELECT 
				idaporte, numerorecibo, valornomina, valoraporte,fechapago,periodo,estado_promotoria 
			FROM Aportes011 
			WHERE idempresa=$idEmpresa AND periodo='$periodo'";
	$rs = $db->querySimple($sql);
	
	$resultado = array();
	while($row = $rs->fetch()){
		$resultado[] =$row;
	}
	
	if(count($resultado)==0)
		echo 0;
	else 
		echo json_encode($resultado);
?>