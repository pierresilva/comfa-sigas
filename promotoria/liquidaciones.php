<?php	

	set_time_limit(0);
	ini_set("display_errors",'1');
	date_default_timezone_set("America/Bogota");
	$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
	include_once  $root;
	include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';
	
	$url=$_SERVER['PHP_SELF'];
	include_once $raiz.DIRECTORY_SEPARATOR.'phpComunes'.DIRECTORY_SEPARATOR.'auditoria.php';
	auditar($url);
	
	include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
	include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
	
	include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'p.definiciones.class.php';
	
	$db = IFXDbManejador::conectarDB();
	if($db->conexionID==null){
		$cadena = $db->error;
		echo msg_error($cadena);
		exit();
	}

	$objDef=new Definiciones;
	
	$usuario=$_SESSION['USUARIO'];
	$fecver = date('Ymd h:i:s A',filectime('liquidaciones.php'));
	
	$idvisita=isset($_REQUEST['idv'])?$_REQUEST['idv']:null;
	
	/*Preparar el combo para el tipo de notificacion*/
	$comboTipoNotificacion = "";
	$consulta=$objDef->mostrar_datos(19, 2);
	while($row=mssql_fetch_array($consulta)){
		$comboTipoNotificacion .= "<option value='".$row['iddetalledef']."'>".$row["detalledefinicion"]."</option>";
	}
	
	/*Preparar el combo para el indice*/
	$comboIndice = "";
	$consultaIndicador = $objDef->mostrar_datos(18, 2);
	while( $rowIndicador = mssql_fetch_array($consultaIndicador ) ){
		$codigo = floatval($rowIndicador['codigo']);
		if( $codigo > 0 )
			$comboIndice .= "<option value='".$codigo."'>".$rowIndicador['detalledefinicion']."</option>";
	}
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<title>::Promotoria | Liquidaciones::</title>
		
		<link type="text/css" href="<?php echo URL_PORTAL; ?>newcss/Estilos.css" rel="stylesheet">
		<link type="text/css" href="<?php echo URL_PORTAL; ?>newcss/css.1.8/jquery-ui-1.8.17.custom.css" rel="stylesheet">
		<link type="text/css" href="<?php echo URL_PORTAL; ?>newcss/marco.css" rel="stylesheet">
		
		<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/1.6/jquery-1.6.2.min.js"></script>
		<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/1.6/jquery-ui-1.8.16.custom.min.js"></script>
		<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/comunes.js"></script>
		<script type="text/javascript" src="js/liquidaciones.js"></script>
				
	</head>
	<body>
		<!-- TABLA OCULTA DE LISTADO  width81%  left:9.2-->
		<table width="70%" border="0" align="center" cellpadding="0" cellspacing="0">
			<tbody>
		  	<!-- ESTILOS SUPERIOR TABLA-->
		  	<tr>
			    <td width="13" height="29" class="arriba_iz">&nbsp;</td>
			    <td class="arriba_ce">
			    	<span class="letrablanca">::&nbsp;Liquidaciones de aportes parafiscales&nbsp;::</span>
			    	<div style="text-align:right;float:right;">
						<?php echo 'Versi&oacute;n: ' . $fecver; ?>
						<input type="hidden" name="hidUsuario" id="hidUsuario" value="<?php echo $_SESSION["USUARIO"];?>"/>
					</div>
			    </td>
			    <td width="13" class="arriba_de" align="right">&nbsp;</td>
		  	</tr> 
		  	<!-- ESTILOS ICONOS TABLA-->
		  	<tr>
		    	<td class="cuerpo_iz">&nbsp;</td>
		    	<td class="cuerpo_ce">
					<img src="../imagenes/menu/nuevo.png" name="iconNuevo" width="16" height="16" id="iconNuevo" style="cursor:pointer" title="Nuevo" onClick="nuevaLiquidacion();" />
					<img src="../imagenes/menu/grabar.png" name="iconGuardar" width="16"  height=16 id="bGuardar" style="cursor:pointer" title="Guardar Liquidacion" onClick="guardarLiquidacion();" />
					<img src="../imagenes/menu/modificar.png" name="iconActualizar" width="16"  height=16 id="iconActualizar" style="cursor:pointer" title="Actualizar Liquidacion" onClick="mostrarPerioElimi();" />
					<img src="../imagenes/menu/visitas.png" name="iconVisitas" width="16" height="16" id="iconVisitas" style="cursor: pointer" title="Liquidaciones por empresa" onClick="openDialogBuscaLiquid();" />         
		    	</td>
		    	<td class="cuerpo_de">&nbsp;</td>
		  	</tr>
		  	<!-- ESTILOS MEDIO TABLA-->
		  	<tr>
		    	<td class="cuerpo_iz">&nbsp;</td>
		    	<td class="cuerpo_ce"><div id="resultado" style="color:#FF0000"></div></td>
		    	<td class="cuerpo_de">&nbsp;</td>
		  	</tr>  
		  	<!-- CONTENIDO TABLA-->
		  	<tr>
		   		<td class="cuerpo_iz">&nbsp;</td>
		   		<td class="cuerpo_ce">
					<table cellspacing="0" border="0" class="tablero" align="center">
		      			<tbody>
			    			<tr>
			      				<th colspan="4">DATOS LIQUIDACI&Oacute;N</th>
			      			</tr>
			    			<tr>
			      				<td>No Visita</td>
			      				<td><input type="text"  class="boxfecha element-required" name="txtNumerVisit" id="txtNumerVisit" onBlur="buscarVisita(this.value)" value="<?php echo $idvisita; ?>" />
			        				<span class="clsObligatorio">&nbsp;&nbsp;&nbsp;</span>
			        			</td>
			        			<td>Fecha Visita</td>
			        			<td id="tdFechaVisita"></td>
			        		</tr>
			        		<tr>
			      				<td>No Liquidaci&oacute;n</td>
								<td>
									<input type="text" class="boxfecha clDatosLiquidacion element-required" name="txtNumerLiqui" id="txtNumerLiqui" onblur="verificarLiquidacion();" />
			        				<span class="clsObligatorio">&nbsp;&nbsp;&nbsp;</span>
			        			</td>
			      				<td>Fecha Liquidaci&oacute;n</td>
			      				<td><input type="text" class="boxfecha element-required" id="txtFechaLiqui" name="txtFechaLiqui" />
			        				<span class="clsObligatorio">&nbsp;&nbsp;&nbsp;</span>
			        			</td>
			      			</tr>
			    			<tr>
			      				<th colspan="4">
			      					DATOS DEL APORTANTE
			      					<input type="hidden" name="hdIdEmpresa" id="hdIdEmpresa" />
			      				</th>
			      			</tr>
			    			<tr>
			      				<td>Nit</td>
			      				<td id="tdNit" class="clDatosAportante" ></td>
			      				<td>Estado</td>
			      				<td id="tdEstadoEmpresa" class="clDatosAportante"></td>
			      			</tr>
			    			<tr>
			      				<td>Empresa</td>
			      				<td colspan="3" id="tdRazonSocial" class="clDatosAportante"></td>
			      			</tr>
		        			<tr>
		          				<td>Direcci&oacute;n</td>
		          				<td id="tdDireccion" class="clDatosAportante"></td>
		          				<td>Tel&eacute;fono</td>
		          				<td id="tdTelefono" class="clDatosAportante"></td>
		        			</tr>
		        			<tr>
		        				<td>Ciudad</td>
		        				<td id="tdCiudad" class="clDatosAportante" colspan="3"></td>
		        				<!-- <td>No De Trabajadores</td>
		        				<td id="tdNumerTraba" class="clDatosAportante"></td>-->
		        			</tr>
		      				<tr>
		         				<td>Represent&aacute;nte Legal</td>
		        				<td id="tdRepresentante" class="clDatosAportante"></td>
		        				<td>Identificaci&oacute;n Rep. Legal</td>
		        				<td id="tdIdentRepre" class="clDatosAportante"></td>
		       				</tr>
		      				<tr>
		        				<td>Indice</td>
		        				<td id="tdIndice" class="clDatosAportante"></td>
		        				<td>IBC</td>
		        				<td><input type="text" class="box1" name="txtIngreBase" id="txtIngreBase" /></td>
		      				</tr>
		      				<tr>
		        				<td>Tipo Afiliaci&oacute;n</td>
		        				<td id="tdTipoAfiliacion" class="clDatosAportante"></td>
		        				<td>Tipo Empresa</td>
		        				<td id="tdTipoEmpresa" class="clDatosAportante"></td>
	      					</tr>
	      					<tr>
		         				<th colspan="4">NOTIFICACI&Oacute;N</th>
		       				</tr>
		      				<tr>
		        				<td>Tipo notificaci&oacute;n</td>
		        				<td>
		        					<select name="cmbTipoNotificacion" class="box1 element-required" id="cmbTipoNotificacion">
		          						<option value="0" selected="selected">Seleccione</option>
		           						<?php
			           						echo $comboTipoNotificacion;
			        					?>
		        					</select>
		          					<span class="clsObligatorio">&nbsp;&nbsp;&nbsp;</span>
		          				</td>
		        				<td>Fecha Notificaci&oacute;n</td>
		        				<td>
		          					<input name="txtFechaNotificacion" type="text" class="boxfecha element-required" id="txtFechaNotificacion" />
		          					<span class="clsObligatorio">&nbsp;&nbsp;&nbsp;</span>
		          				</td>
		      				</tr>
		      				<tr>
		        				<td>Estado</td>
		        				<td>
		        					<select name="cmbEstado" id="cmbEstado" class="boxfecha element-required">
		        					</select>
		        					<span class="clsObligatorio">&nbsp;&nbsp;&nbsp;</span>
		        				</td>
		        				<td>Fecha Estado</td>
		        				<td>
		        					<input name="txtFechaEstado" id="txtFechaEstado" type="text" class="boxfecha" readonly="readonly" />
		        					<span class="clsObligatorio" style="display:none;" id="clsFechaEstado">&nbsp;&nbsp;&nbsp;</span>
		        				</td>
	      					</tr>
	      					<tr>
		        				<td>Usuario</td>
		        				<td><input name="txtUsuario" type="text" disabled  class="box1" id="txtUsuario" readonly="readonly" value="<?php echo $_SESSION['USUARIO']; ?>" /></td>
	      						<td>&nbsp;</td>
	      						<td>&nbsp;</td>
	      					</tr>
		      				<tr>
		        				<td>Observaciones</td>
		        				<td colspan="3">
		          					<textarea name="txaObservacion" id="txaObservacion" cols="45" rows="2" class="area element-required" maxlength="245"></textarea>
		          				</td>
		        			</tr>
		      				<tr>
		        				<td>Documentaci&oacute;n Aportada</td>
		        				<td colspan="3">
		          					<textarea name="txaDocumentacion" id="txaDocumentacion" cols="45" rows="2" class="area" maxlength="245"></textarea>
		          				</td>
		        			</tr>
	      					<tr>
		         				<th colspan="4">DETALLE LIQUIDACI&Oacute;N</th>
		       				</tr>
		       				<tr>
								<td colspan="4" style="text-align:center">
									<br/><br/>
							    	Indice:
							        <select name="cmbIndice" id="cmbIndice" class="boxlargo element-required">
							        	<option value="0">Seleccione</option>
							            <?php
							           		echo $comboIndice;
								        ?>
							     	</select>
							  		<br/><br/>   
						      		<table style="width:75%; margin:auto;" id="tblDetalleLiquidacion" border="0" class="tablero" >
						      			<thead>
						      				<tr>
						      					<td colspan="2">
						      						Cantidad Periodos:
						      					</td>
						      					<td colspan="4"> 
						      						<input type="text" name="txtCantidadPeriodos" id="txtCantidadPeriodos" size="3" />
						      					</td>
						      				</tr>
						      				<tr>
						      					<td colspan="2">
						      						Total Base Liquidaci&oacute;n:
						      					</td>
						      					<td colspan="4"> 
						      						<input type="text" name="txtTotalBaseLiqui" id="txtTotalBaseLiqui" />
						      					</td>
						      				</tr>
						      				<tr>
						      					<td colspan="2">
						      						Periodo Inicial:
						      					</td>
						      					<td colspan="4"> 
						      						<input type="text" name="txtPeriodoIncial" id="txtPeriodoIncial" size="10" readonly="readonly"/>
						      						<input type="button" name="btnAgregarPeriodos" id="btnAgregarPeriodos" value="Agregar" onclick="addTrPeriodoMasivo();"/>
						      					</td>
						      				</tr>
							         		<tr>
							         			<th>&nbsp;</th>
							         			<th>Periodo</th>
							         			<th>Aporte</th>
							         			<th>Base Liquidaci&oacute;n</th>
							         			<th>Total a Pagar</th>
							         			<th>Observaci&oacute;n</th>
							         		</tr>
							         	</thead>
							         	<tbody>
						         		</tbody>
						         		<tfoot>
						         			<tr>
						         				<td colspan="9" style="text-align: center;">
						         					<input type="button" name="btnAdicionarTrPeriodo" id="btnAdicionarTrPeriodo" value="Adicionar Periodo" onclick="addTrPeriodo();"/>
						         				</td>
						         			</tr>
						         		</tfoot>	     
						    		</table>
						    		<br/><br/>
					    		</td>
					    	</tr>
					    </tbody>
		      		</table>
					</td>
		    		<td class="cuerpo_de">&nbsp;</td>
		  		</tr>		  
		  		<!-- ESTILOS PIE TABLA-->
		  		<tr>
		    		<td class="abajo_iz" >&nbsp;</td>
		    		<td class="abajo_ce" ></td>
		    		<td class="abajo_de" >&nbsp;</td>
		  		</tr>		  
			</tbody>
		</table>
		<!-- DIV BUSCAR LIQUIDACIONES -->
		<div id="tablaLiquidaciones" title="::LIQUIDACIONES POR EMPRESA::" style="display: none;">
			<br />
		  	<p>Nit a buscar:<input type="text" id="txtNitLiquidacion" name="txtNitLiquidacion" class="box_0" />
		   	<button id="btnBuscarNit" class="ui-state-default">Buscar</button></p>
		  	<h5 id="datosEmpresaLiq"></h5>
		 	<table width="100%" border="0" cellpadding="5" cellspacing="0" class="tablero" id="liquidaciones">
		  		<thead>
		   			<tr>
		    			<th><strong>No. Visita</strong></th>
		    			<th><strong>No. Liquidaci&oacute;n</strong></th>
		    			<th><strong>Fecha Liquidaci&oacute;n</strong></th>
		    			<th><strong>Estado Liquidaci&oacute;n</strong></th>
		    			<th><strong>Periodos</strong></th>
		    			<th><strong>Promotor</strong></th>
		    			<th><strong>Base Liquidaci&oacute;n</strong></th>
		    			<th><strong>Indicador</strong></th>
		    			<th><strong>Aportes</strong></th>
		    			<th><strong>Neto</strong></th>
		   			</tr>
		 		</thead>
			 	<tbody>
	 			</tbody>
		 		<tfoot>
		 			<td colspan="9"><strong>TOTAL</strong></td>
		 			<td class="big" id="totalLiqTabla"></td>
		 		</tfoot>
			</table>
		</div>
		<!-- DIV PERIODOS A ELIMINAR -->
		<div id="tablaPeriodoEliminar" title="::PERIODOS A ELIMINAR::" style="display: none;">
			<br />
		 	<table width="100%" border="0" cellpadding="5" cellspacing="0" class="tablero" id="tblPeriodoEliminar">
		  		<thead>
		   			<tr>
		    			<th><strong>Periodo</strong></th>
		    			<th><strong>Base Liquidaci&oacute;n</strong></th>
		    			<th><strong>Aportes</strong></th>
		    			<th><strong>Total a Pagar</strong></th>
		   			</tr>
		 		</thead>
			 	<tbody>
	 			</tbody>
			</table>
		</div>
	</body>
</html>