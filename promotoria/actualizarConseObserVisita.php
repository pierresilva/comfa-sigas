<?php
/*Actualizacion el consecutivo de la visita y las observaciones de los periodos de la liquidacion*/
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once 'clases' . DIRECTORY_SEPARATOR . 'promotoria.class.php';

$objDatosFiltro = $_POST["objDatosFiltro"];

//Parametros reporte013
//$nit=isset($_REQUEST['nit'])?$_REQUEST['nit']:"";

//Parametros comunes
/*$idVisita=isset($_REQUEST["idVisita"])?$_REQUEST["idVisita"]:"";
$fechaI=isset($_REQUEST['fechaI'])?$_REQUEST['fechaI']:"";        //fecha inicial de la visita
$fechaF=isset($_REQUEST['fechaF'])?$_REQUEST['fechaF']:"";        //fecha final de la visita
$estado=isset($_REQUEST["estado"])?$_REQUEST["estado"]:"";        //Estado visita*/

$objPromotoria = new Promotoria;
$objPromotoria->inicioTransaccion();
$contador = 0;

//Preparar el parametro para obtener las visita para actualizar
/*$arrDatosConsulta = array("idvisita"=>$idVisita,"fechavisita"=>array("I"=>$fechaI,"F"=>$fechaF)
		,"estado"=>$estado,"nit"=>$nit);*/

//Obtener las visitas para actualizar
//$datosConsulta = $objPromotoria->empresa_liquidacion($arrDatosConsulta);
$datosConsulta = $objPromotoria->buscar_liquidacion($objDatosFiltro);

$arrIdVisitas = array();
$arrIdLiquidaciones = array();

//Obtener los datos de la visita y la liquidacion
foreach ($datosConsulta as $rowDatos){
	
	//if($rowDatos["convenio"]!="V"){
		//Obtener las liquidaciones
		$arrIdLiquidaciones[] = $rowDatos["idliquidacion"];
		
		//Obtener las visitas para actualizar el consecutivo
		if($rowDatos["consecutivo"]==0)
			$arrIdVisitas[] = $rowDatos["idvisita"];
	//}
}

//Actualizar las observaciones del periodo
if(count($arrIdLiquidaciones)>0){
	$arrIdLiquidaciones = array_unique($arrIdLiquidaciones);

	foreach ($arrIdLiquidaciones as $rowLiquidacion){
		$rsActuaObserPerioLiqui = $objPromotoria->ejecutar_sp_observacion_liq($rowLiquidacion);
		//Error al actualizar la observacion
		if($rsActuaObserPerioLiqui==0){
			$contador++;
			break;
		}
	}
}

//Actualizar el consecutivo
if(count($arrIdVisitas)>0 && $contador==0){
	$arrIdVisitas = array_unique($arrIdVisitas);
	
	//Obtener el ultimo consecutivo
	$ultimoConsecutivo = $objPromotoria->buscar_consecutivo_visita();
		
	foreach ($arrIdVisitas as $rowVisita){
		$ultimoConsecutivo++;
		//Actualizar consecutivo
		$rsActuaConseVisita = $objPromotoria->actualizar_consecutivo_visita(array("idvisita"=>$rowVisita,"consecutivo"=>$ultimoConsecutivo));
		//Error al actualizar el consecutivo
		if($rsActuaConseVisita==false){
			$contador++;
			break;
		}	
	}
}

if($contador==0){
	$objPromotoria->confirmarTransaccion();
	echo 1;
}else{
	$objPromotoria->cancelarTransaccion();
	echo 0;
}
?>