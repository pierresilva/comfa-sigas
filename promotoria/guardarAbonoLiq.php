<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once 'clases' . DIRECTORY_SEPARATOR . 'promotoria.class.php';

/*Estructura del objeto datos={idperiodoliquidacion:int,abono:int,idtipoabono:int,cancelado:string}*/
$objDatos = $_POST["datos"];
$contador = 0;
$rsAbono = null;
$objPromotoria = new Promotoria;
//Actualizar el abono del periodo en aporte309
$objPromotoria->inicioTransaccion();
foreach ($objDatos as $rowPeriodo){
	//Actualizar abono al periodo de la liquidacion
	$rsAbono = $objPromotoria->cargar_abono_periodo($rowPeriodo,$_SESSION["USUARIO"]);
	if($rsAbono==false){
		$contador++;
		break;
	}
}
if($contador==0){
	$objPromotoria->confirmarTransaccion();
	echo 1;
}else{
	$objPromotoria->cancelarTransaccion();
	echo 0;
}
?>