<?php 
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'promotoria' . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'promotoria.class.php';
$objPro=new Promotoria;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Reporte de visitas por empresa</title>
<link type="text/css" href="../../css/Estilos.css" rel="stylesheet"/>
<link type="text/css" href="../../css/estilo_tablas.css" rel="stylesheet"/>
<link type="text/css" href="../../css/formularios/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="../../js/jquery-1.4.2.js"></script>
<script type="text/javascript" src="../../js/formularios/ui/ui.datepicker.js"></script>
<script type="text/javascript" src="../../js/comunes.js"></script>
<script type="text/javascript" src="../js/reportes.js"></script>
<script type="text/javascript">
$().ready(function(){
$("#fecInicial,#fecFinal").datepicker();	
});
</script>
</head>
<body>
<div align="center">
  <p align="left"><br />
  <img src="../../centroReportes/promotoria/logo_comfamiliar.jpg" width="159" height="54" /> <div align="center"></div><img src="../../centroReportes/promotoria/logo_reporte.png" width="362" height="70" /></p>
  <p><br />
  </p>
  <table width="302"  border="0" align="center" cellpadding="5" cellspacing="0" class="tablaR">
    <tr>
    <th width="100%" scope="col"><strong>INFORME MENSUAL PROMOTOR&Iacute;A</strong></th>
  </tr>
  <tr>
    <td><p align="left">Promotor
      <select name="promotor" class="box" id="promotor">
        <option value="0">Seleccione</option>
        <?php
		$consulta=$objPro->buscar_promotor_combo();
		 while($row=mssql_fetch_array($consulta)){
		 $nombre=trim($row['pnombre'])." ".trim($row['snombre'])." ".trim($row['papellido'])." ".trim($row['sapellido']);	 
		 echo "<option value=".$row['idpersona'].">".$nombre."</option>";
		 }
		?>
      </select></p>
    </td>
    </tr>
  <tr>
    <td><p align="left">Fecha inicial
      <input name="fecInicial" type="text" class="box" id="fecInicial" />
    </p></td>
    </tr>
  <tr>
    <td><p align="left">Fecha final
      <input name="fecFinal" type="text" class="box" id="fecFinal" />
    </p></td>
    </tr>
  <tr>
    <td>&nbsp;&nbsp;&nbsp;
      <input name="buscar" type="button" class="ui-state-default" id="buscar" value="Generar" onclick="rptMensual()" /></td>
  </tr>
 </table>
 <div class="Rojo" align="center" id="rta"></div>
 <p align="center"><a href="../../centroReportes/promotoria/menuPromotoria.php" title="Volver"><img src="../../imagenes/volver.png" width="32" height="32" alt="Volver" style="border:none" /></a></p>
</div>
</body>
</html>