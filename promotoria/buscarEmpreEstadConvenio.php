<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once 'clases' . DIRECTORY_SEPARATOR . 'promotoria.class.php';

$objPromotoria = new Promotoria();
$idEmpresa = $_POST["idEmpresa"];

$consulta = $objPromotoria->convenio_empresa($idEmpresa);
$data = array("Fecha"=>null,"Periodo"=>null);
$conFecha = 0;
$conPeriodo = 0;
$idDetalleConvenio = "";
while( ( $row = $consulta->fetch() ) == true ){
  $data["Fecha"][] = $row;
  $idDetalleConvenio .=  $row["iddetalleconvenio"] . ",";
  $conFecha++;
}

if($conFecha>0){
	$idDetalleConvenio = trim($idDetalleConvenio,",");
	$consulta = $objPromotoria->periodos_fecha_convenio($idDetalleConvenio);
	
	while( ( $row = $consulta->fetch() ) == true ){
		$data["Periodo"][] = $row;
		$conPeriodo++;
	}
	
}

echo ($conFecha>0 && $conPeriodo>0) ? json_encode($data) : 0;
?>