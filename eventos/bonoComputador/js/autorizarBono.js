/*
* @autor:      Ing. Orlando Puentes
* @fecha:      8/10/2010
* objetivo:
*/
// funcion para obtener cookie en javascript
	function getCookie(c_name)
	{
		var i,x,y,ARRcookies=document.cookie.split(";");
		for (i=0;i<ARRcookies.length;i++)
		{
  			x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
  			y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
  			x=x.replace(/^\s+|\s+$/g,"");
  			if (x==c_name)
    			{
    				return unescape(y);
    			}// fin if
		}// fin for
  	}// fin getCookie
//funcion para obtener cookie en javascript
	
var URL=src();
var nuevoR=0;
var idt=0;
var bono=0;
var categoria=0;
$().ready(function(){
//Dialog ayuda
$("#ayuda").dialog({
		 	autoOpen: false,
			height: 500,
			width: 800,
			draggable:true,
			modal:false,
			open: function(evt, ui){
					$('#ayuda').html('');
					$.get(URL +'help/eventos especiales/ManualayudabonosCp.html',function(data){
							$('#ayuda').html(data);
					});
			 }
		});
//Dialog Colaboracion en linea
$("#dialog-form2").dialog({
		autoOpen: false,
		height: 400,
		width: 500,
		modal: true,
		buttons: {
			'Enviar': function() {
			var bValid = true;
			var campo=$('#notas').val();
			var campo0=$.trim(campo);
			if (campo0==""){
				$(this).dialog('close');
					return false;
				}
			var campo1=$('#usuario').val();
			var campo2="bonoCp.php";
			$.post(URL+'phpComunes/colaboracion.php',{v0:campo0,v1:campo1,v2:campo2},
			function(datos){
				if(datos=='1'){
					alert("Su comentario fue enviado correctamente!. Gracias por participar en el mejoramiento de SIGAS");
				}else{
					alert(datos);
				}
		});
		$(this).dialog('close');
		},
		Cancelar: function() {
			$(this).dialog('close');
			$("#dialog-form2").dialog("destroy");
		}
	}
	});


//Dialog Ver mas
$("#verMas").dialog({
		 	autoOpen: false,
			modal: true,
		    width:600,
		   	resizable:false

   });//end dialog

	//Campos solo numericos
	$("#valor,#cedula").numeric();

});//fin ready
//-------------Nueva pignoracion


function nuevaPignoracion(){
limpiarCampos();
//$("#datost").empty(); mientras tanto lo oculto..
$("#datost").hide();
$("#tipoI").focus();
$("#txtfecha").val(fechaHoy());
$("#tdnumero").html("");
nuevoR=1;
$("#bGuardar").show();
}


//Dialog Ayuda
function mostrarAyuda(){
	$("#ayuda").dialog('open' );
	}

function notas(){
	$("#dialog-form2").dialog('open');
	}
//_-------------------------------------------------------

//Dialog ver Mas

function imprimir(idp){
    var url=URL+'centroReportes/pignoracion/reporte001.php?v0='+idp;
    window.open(url,"_blank");
}

function limpiarCampos(){
$("table.tablero input:text").val('');
$("table.tablero  select").val('0');

}

function buscarTrabajador(){
    if($("#numero").val().length==0){
        $("#numero").focus();
        return false;
    }
    var cat;
    if($("#tipoI").val()==0){
        alert("Seleccione tipo de identificaci\u00F3n!");
        $("#tipoI").focus();
        return false;
    }
    var v0=$("#tipoI").val();
    var v1=$("#numero").val();
    if(length.v1==0){
        alert("Digite el número de identificaci\u00F3n");
        $("#numero").focus();
        return false;
    }
    $.getJSON(URL+'phpComunes/buscarPersonaAfiliadaActiva.php', {v0:v1,v1:v0}, function(data){
        if(data==0){
            alert("Lo lamento el trabajador no existe o esta Inactivo!");
            $("#numero").val("");
            $("#numero").focus();
            return false;
        }
        $.each(data, function(i,fila){
            var nom=fila.pnombre+" "+fila.snombre+" "+fila.papellido+" "+fila.sapellido;
			idt=fila.idpersona;
            $("#tdnumero").html(nom);
			$("#txtFecha").val(fila.fechaingreso);
			$("#txtSalario").val(formatCurrency(fila.salario));
            return;
        })
        $.getJSON('buscarEntregas.php', {v0:idt}, function(datas){
        	if(datas==0){
				
			}
			else{
				var vb=datas[0].valorbono;
				var fecha=datas[0].fechasistema;
				alert("El trabajador YA tiene UN BONO asignado por: "+vb+" fecha: "+fecha);
				nuevaPignoracion();
				flag=false;
				return false;
			}
       });
        
        
        
        $.ajax({
			url:'buscarCategoriaAutorizar.php',
			type:"POST",
			data: {v0:idt},
			async:false,
			dataType: "json",
			success: function(datos){
				
				categoria=(datos[0].categoria);
				if(categoria=='A'){
					 alert("El trabajador es de categoria A, tiene derecho a un BONO de $300.000.00");
			         $("#valor").val('$300.000.00');
					 bono=300000;
			         return false;
				}
				else if (categoria=='B') {
					 alert("El trabajador es de categoria B, tiene derecho a un BONO de $200.000.00");
			         $("#valor").val('$200.000.00');
					 bono=200000;
			         return false;
				}
				else if (categoria=='C') {
					alert("La categoria es C no tiene derecho a BONO");
					nuevaPignoracion();
					idt=0;
					bono=0;
					return false;
				}else{
					alert("El Afiliado No Tiene Categoria");
					nuevaPignoracion();
					idt=0;
					bono=0;
					return false;
				}
			}
		});
        
    });
   
}

function guardarBono(){
	$("#bGuardar").hide();
    if(bono==0)return false;
	v0=idt;
	v1=bono;
	v2=$("#txtNotas").val();
	v3=categoria;
	if($.trim(v2)==0){
		alert("Digite el motivo de la autorizaci\u00F3n");
		return false;
		}
	if(idt==0){
		$("#bGuardar").show();
	 	return false;
	}
    $.getJSON('guardarAutorizado.php', {v0:v0,v1:v1,v2:v2,v3:v3}, function(datos){
        if(datos==0){
            alert("No se pudo gurdar el Bono, intende de nuevo!");
            return false;
        }
        if(datos==99){
            alert("Fondo insuficiente!, FIN DE LA PROMOCION");
            return false;
        }
        alert("El bono fu\u00E9 guardado!");
        var idbono=datos;
        imprimirBono(idbono);
    });
}

function imprimirBono(idbono){
    var url="http://"+getCookie("URL_Reportes")+"/eventos/bonosCP/reporte001.jsp?v0="+idbono;
	window.open(url,"_blank");
}