/*
* @autor:      Ing. Orlando Puentes
* @fecha:      8/10/2010
* objetivo:
*/

var URL=src();
var nuevoR=0;
var idt=0;
var bono=0;
$().ready(function(){
//Dialog ayuda
$("#ayuda").dialog({
	autoOpen: false,
	height: 500,
	width: 800,
	draggable:true,
	modal:false,
	open: function(evt, ui){
	$('#ayuda').html('');
	$.get(URL +'help/eventos especiales/ManualayudabonosCp.html',function(data){
	$('#ayuda').html(data);
	});
	}
	});
//Dialog Colaboracion en linea
$("#dialog-form2").dialog({
	autoOpen: false,
	height: 400,
	width: 500,
	modal: true,
	buttons: {
	'Enviar': function() {
	var bValid = true;
	var campo=$('#notas').val();
	var campo0=$.trim(campo);
	if (campo0==""){
	$(this).dialog('close');
	return false;
	}
	var campo1=$('#usuario').val();
	var campo2="bonoCp.php";
	$.post(URL+'phpComunes/colaboracion.php',{v0:campo0,v1:campo1,v2:campo2},
	function(datos){
	if(datos=='1'){
	alert("Su comentario fue enviado correctamente!. Gracias por participar en el mejoramiento de SIGAS");
	}else{
	alert(datos);
	}
	});
	$(this).dialog('close');
	},
	Cancelar: function() {
	$(this).dialog('close');
	$("#dialog-form2").dialog("destroy");
	}
	}
	});


//Dialog Ver mas
$("#verMas").dialog({
	autoOpen: false,
	modal: true,
	width:600,
	resizable:false

   });//end dialog

	//Campos solo numericos
	$("#valor,#cedula").numeric();

});//fin ready
//-------------Nueva pignoracion


//Dialog Ayuda
function mostrarAyuda(){
	$("#ayuda").dialog('open' );
	}

function notas(){
	$("#dialog-form2").dialog('open');
	}

function limpiarCampos(){
    $("table.tablero input:text").val('');
    $("table.tablero  select").val('0');
	$("#txtfecha").val(fechaHoy());
	$("#tdnumero").html("");
    $("#txtBono").focus();
}

function buscarBono(){
    var bono=$("#txtBono").val();
    if(bono=="") return false;
    $.getJSON('buscarBono.php', {v0:bono}, function(data){
        if(data==0){
            alert("Lo lamento el bono no Existe o ya esta Anulado!");
			limpiarCampos();
            return false;
        }
        $.each(data, function(i,fila){
        var nom = fila.pnombre+" "+fila.snombre+" "+fila.papellido+" "+fila.sapellido;
        $("#tdnumero").html(nom);
		$("#tipoI").val(1);
		$("#numero").val(fila.identificacion);
		$("#valor").val(fila.valorbono);
        return;
        })
    })
}

function guardarBono(){
	if(confirm("Esta seguro de Anular el presente bono?")== true){
		var nota=$("#txtMotivo").val();
		if(nota==""){
			alert("Faltan el motivo de la anulación");
			return false;
			}
		var idb=$("#txtBono").val();
		var valor=$("#valor").val();
		$.getJSON('anular.php',{v0:idb,v1:nota,v2:valor},function(data){
			if(data==0){
				alert("No se pudo anular el bono!");
				return false;
				}
			if(data==2){
				alert("No se pudo actualizar el presupuesto, por favor llame al administrador del sistema!");
				return false;
				}
			alert("El bono fue Anulado!");
			limpiarCampos();	
			})
		
		}
	}