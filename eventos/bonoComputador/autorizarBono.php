<?php
/* autor:       orlando puentes
 * fecha:       08/10/2010
 * objetivo:    
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$url=$_SERVER['PHP_SELF'];
include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'auditoria.php';
auditar($url);
include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'p.definiciones.class.php';
$objClase=new Definiciones;

?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head><TITLE>Pignoracion</TITLE>
<META http-equiv=Content-Type content="text/html; charset=iso-8859-1">
<meta http-equiv="Content-Script-Type" content="text/javascript; charset=iso-8859-1" />
<META content="MSHTML 6.00.2900.2180" name=GENERATOR>
<link href="../../css/Estilos.css" rel="stylesheet" type="text/css" />
<link type="text/css" href="../../css/estilo_tablas.css" rel="stylesheet"/>
<link type="text/css" href="../../css/formularios/base/ui.all.css" rel="stylesheet" />
<link href="../../css/marco.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../../js/jquery-1.3.2.min.js"></script>
<!--<script type="text/javascript" src="../../../../js/jquery-1.4.2.js"></script> -->
<script type="text/javascript" src="../../js/formularios/ui/ui.core.js"></script>
<script type="text/javascript" src="../../js/formularios/ui/ui.tabs.js"></script>
<script type="text/javascript" src="../../../../js/formularios/ui/ui.draggable.js"></script>
<script type="text/javascript" src="../../js/formularios/ui/ui.resizable.js"></script>
<script type="text/javascript" src="../../js/formularios/ui/ui.dialog.js"></script>
<script type="text/javascript" src="../../js/formularios/ui/ui.datepicker.js"></script>
<script type="text/javascript" src="../../js/jquery.alphanumeric.js"></script>
<script type="text/javascript" src="../../js/comunes.js"></script>
<script type="text/javascript" src="js/autorizarBono.js"></script>

<script type="text/javascript">
shortcut.add("Shift+F",function() {
		                                var URL=src();
		var url=URL+"aportes/trabajadores/consultaTrabajador.php";
    	window.open(url,"_blank");
    },{
	'propagate' : true,
	'target' : document 
        });        
</script>

</head>
<body>
<center>
<br /><br />
<table width="90%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="13" height="29" class="arriba_iz">&nbsp;</td>
    <td class="arriba_ce"><span class="letrablanca">::&nbsp;Autorizar Bono Cumputador &nbsp;::</span></td>
    <td width="13" class="arriba_de" align="right">&nbsp;</td>
  </tr>
  <tr>
<td class="cuerpo_iz">&nbsp;</td>
<td class="cuerpo_ce">
<img src="../../imagenes/spacer.gif" width="1" height="1"/>
<img src="../../imagenes/menu/nuevo.png" title="Nuevo" width="16" height="16" style="cursor:pointer" onClick="nuevaPignoracion();" />
<img src="../../imagenes/spacer.gif" width="1" height="1"/>
<img src="../../imagenes/menu/grabar.png" width="16"  height=16 style="cursor:pointer" title="Guardar" onClick="guardarBono();" id= "bGuardar" /> 
<img src="../../imagenes/spacer.gif" width="1" height="1"/>
<img src="../../imagenes/menu/modificar.png" title="Actualizar" width="16" height="16" onClick="validarCampos(2)" style="cursor:pointer" />
<img src="../../imagenes/spacer.gif" width="1" height="1"/>
<img src="../../imagenes/menu/imprimir.png" width="16" height="16" style="cursor:pointer" title="Imprimir" onClick="window.open(URL+'centroReportes/aportes/radicacion/reporte01.php','_blank')" />
<img src="../../imagenes/spacer.gif" width="1" height="1"/><img src="../../imagenes/spacer.gif" width="1" height="1"/>
<img src="../../imagenes/menu/informacion.png" width="16" height="16" style="border:none; cursor:pointer" title="Manual" onClick="mostrarAyuda();" />
<img src="../../imagenes/menu/notas.png" width="16" height="16" style="cursor: pointer" title="Colaborac&oacute;on en l&oacute;nea" onClick="notas();" />
</td>
    <td class="cuerpo_de">&nbsp;</td>
  </tr>
  <tr>
   <td class="cuerpo_iz">&nbsp;</td>
    <td class="cuerpo_ce"><div id="resultado" style="font-weight: bold;font-size: 14px;color:#FF0000"></div></td>
    <td class="cuerpo_de">&nbsp;</td>
  </tr>
  <tr>
    <td class="cuerpo_iz">&nbsp;</td>
    <td class="cuerpo_ce">
	<table width="95%" border="0" cellspacing="0" class="tablero">
      <tr>
        <td>Fecha</td>
        <td colspan="2"><input name="txtfecha" id="txtfecha" class="box1"/></td>
        </tr>
      <tr>
        <td>Tipo identificaci&oacute;n</td>
        <td colspan="2"><select name="tipoI" id="tipoI" class="box1">
          <?php
		$consulta = $objClase->mostrar_datos(1,1);
		while($row=mssql_fetch_array($consulta)){
		echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
			}
        ?>
          </select>
          <img src="../../imagenes/menu/obligado.png" width="12" height="12" /></td>
        </tr>
      <tr>
        <td width="25%">N&uacute;mero</td>
        <td width="20%" ><input name="numero" id="numero" class="box1" onBlur="buscarTrabajador();"/>
          <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12"/></td>
        <td width="55%" id="tdnumero" >&nbsp;</td>
        </tr>
      <tr>
      <td>Fecha Afiliaci&oacute;n</td>
      <td colspan="2"><input name="txtFecha" type="text" class="box1" id="txtFecha" /></td>
        </tr>
      <tr>
        <td>Salario</td>
        <td colspan="2"><input name="txtSalario" type="text" class="box1" id="txtSalario"  /></td>
      </tr>
      <tr>
        <td>Valor</td>
        <td colspan="2"><input name="valor" type="text" class="box1" id="valor" readonly /></td>
      </tr>
      <tr>
        <td>Motivo Autorizaci&oacute;n</td>
        <td colspan="2"><input name="txtNotas" type="text" class="boxlargo" id="txtNotas"  />
          <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12"/></td>
      </tr>
             
       </table>
   
      </td>
    <td class="cuerpo_de">&nbsp;</td>
  </tr>
  <tr>
    <td class="abajo_iz" >&nbsp;</td>
    <td class="abajo_ce" ></td>
    <td class="abajo_de" >&nbsp;</td>
  </tr>
</table>

</center>
    <input type="hidden" id="hvalor" name="hvalor"/>
 <!-- VENTANA DIALOGVER MAS-->

<!-- colaboracion en linea -->

<div id="dialog-form2" title="Colaboraci&oacute;n en l&iacute;nea">
<p>Por favor diligencie este formulario para enviar comentarios, errores o falencias encontradas en el proceso. M&oacute;ximo 250 caracteres </p>
<label>Tus comentarios:</label><br />
<textarea name="notas" id="notas" cols="60" rows="10"></textarea>
</div>
<!-- Manual Ayuda -->
<div id="ayuda" title="Manual Pignoraci&oacute;n" 
style="background-image:url('../../imagenes/FondoGeneral0.png')">
</div>
</body>
</html>
