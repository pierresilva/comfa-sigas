$(function(){
		
	$.ajax({
		url  : "regalos_navidad.log.php",
		type : "POST",
		data : {v0:"TIPO_IDENTIFICACION"},
		dataType:"json",
		async:false,
		success:function(datos){
			if(datos==0){
				$("#txtNumero").attr("disabled","disabled");
				return false;
			}
			var option = "";
			$.each(datos,function(i,row){
				option += "<option value='"+row['iddetalledef']+"'>"+row['detalledefinicion']+"</option>";
			});
			$("#cmbTipoDocumento").append(option);
		}
	});
	
	$("#txtNumero").blur(function(){
		var tipoIdentificacion = $("#cmbTipoDocumento").val();
		var numero = $("#txtNumero").val().trim();
		$("#lblMensajeNumero").html("");
		$("#tdMensajeError").html("");
		$("#tdNombres").html("");
		$("#tabRegalos").html("");
		if(numero=="" || tipoIdentificacion==""){
			$("#hdnIdTrabajador").val('');
			$("#hdnIdBeneficiarios").val('');
			return false;
		}
			
		$.ajax({
			url:"regalos_navidad.log.php",
			type:"POST",
			data:{v0:"CONSULTAR",v1:numero,v2:tipoIdentificacion},
			dataType:"json",
			async:false,
			success:function(datos){
					var registros = "";
					if(datos['error']==1){
						$("#lblMensajeNumero").html(datos['mensaje']);
						return false;
					}
					$("#tdNombres").html(datos['nombres']);
					$("#hdnIdTrabajador").val(datos['idpersona']);
					if(datos['error']==2){
						$("#tdMensajeError").html(datos['mensaje']);
						return false;
					}
					var idBeneficiarios = "";
					$.each(datos['beneficiarios'],function(i,row){//idbeneficiario
						registros += "<tr><td>"+row['idpersona']+"</td><td>"+row['beneficiarios']+"</td><td>"+row['fechanacimiento']+"</td><td>"+row['edad']+"</td><td>"+row['detalledefinicion']+"</td></tr>";	
						idBeneficiarios += row['idpersona']+",";
					});	
					$("#tabRegalos").append("<tbody><tr><th >Id</th><th>Nombres</th><th>Fecha Nacimiento</th><th>Edad</th><th>Tipo</th></tr>"+registros+"</tbody>");
					$("#hdnIdBeneficiarios").val(idBeneficiarios);
			}
		});
	});	
	
	$("#btnMensaje").click(function(){
		$("#divMensaje").dialog("close");
		$("#divMensaje").css('display','none');
	});

});

function guardarR(){
	//var checkRegalo = $("#tabRegalos input[type=checkbox]");
	var idBeneficiarios = $("#hdnIdBeneficiarios").val();
	if(idBeneficiarios==""){
		return false;
	}
	/*$.each(checkRegalo,function(i,row){
		if($(row).is(':checked')){
			idPersona += $(row).val()+",";
		}
	});*/		
	idBeneficiarios += $("#hdnIdTrabajador").val();
	
	$.ajax({
		url:"regalos_navidad.log.php",	
		type:"POST",
		data:{v0:'GUARDAR',v1:idBeneficiarios},
		dataType:"json",
		async:true,
		success:function(datos){
			if(datos==0)
				alert("Los datos No se actualizaron");
			else{
				$("#divMensaje").dialog({
					modal:true,
					width:500,
					height:330,
					maxHeight:330,
					maxWidth:500,
					minHeight:330,
					position: 'center',
					title:'Feliz Navidad'			
				});
				$("#hdnIdBeneficiarios").val('');
				$("#tabRegalos").html('');
			}
		}
	});
}




