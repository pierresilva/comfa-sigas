<?php
date_default_timezone_set('America/Bogota');
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}
$case = $_POST['v0'];
switch($case){
	case 'TIPO_IDENTIFICACION':
		$data = array();
		$sql = "SELECT iddetalledef,iddefinicion,codigo,detalledefinicion FROM aportes091 WHERE iddefinicion=1 order by 1";
		$rs = $db->querySimple($sql);
		while($row = $rs->fetch()){
			$data[] = $row;
		}
		if(!$data){
			echo 0;
			break;
		}
		echo json_encode($data);
	break;
	case 'CONSULTAR':
		$numero = $_POST['v1'];
		$tipoIdentificacion = $_POST['v2'];
		$data = array('nombres'=>'','idpersona'=>0,'beneficiarios'=>array(),'error'=>0,'mensaje'=>'');
		$sql = "SELECT  idpersona,pnombre+' '+isnull(snombre,'')+' '+papellido+' '+isnull(sapellido,'') persona,flag FROM aportes015 WHERE idtipodocumento=$tipoIdentificacion AND identificacion='$numero'
";
		$rs = $db->querySimple($sql);
		if($row = $rs->fetch()){
			if(count($row)>3){
				$data['error']=1;
				$data['mensaje']="Hay mas de dos trabajadores con la mismo identificacion";
			}else if($row['flag']=='S'){
				$data['nombres']=$row['persona'];
				$data['idpersona']=$row['idpersona'];
				$data['error']=2;
				$data['mensaje']="El trabajador ya recibio regalo";
			}else{
				$data['nombres']=$row['persona'];
				$data['idpersona']=$row['idpersona'];
				$sql = "SELECT count(*)estado FROM aportes016 WHERE idempresa=27342 AND estado='A' AND idpersona=".$data['idpersona'];			
				$rs = $db->querySimple($sql);
				$row = $rs->fetch();
				
				if($row['estado']==0){
					$data['error']=2;
					$data['mensaje']="El trabajador no esta activo en COMFAMILIAR";
				}else{
					$sql = " DECLARE @anno CHAR(4) = YEAR(GETDATE())
					SELECT a15.idpersona,a15.pnombre+' '+isnull(a15.snombre,'')+' '+a15.papellido+' '+isnull(a15.sapellido,'') beneficiarios,convert(char(10),a15.fechanacimiento,103) fechanacimiento,datediff(dd,a15.fechanacimiento,getdate())/365.25 as edad,a91.detalledefinicion FROM aportes021 a21 INNER JOIN aportes015 a15 ON a15.idpersona=a21.idbeneficiario INNER JOIN aportes091 a91 ON a91.iddetalledef=a21.idparentesco WHERE a21.idparentesco IN (35,38) AND a15.flag is null AND a21.estado='A' and datediff(day,a15.fechanacimiento,@anno+'-12-31')<4392 AND a21.idtrabajador=".$data['idpersona'];			
					$rs = $db->querySimple($sql);
					while($row = $rs->fetch()){
						$data['beneficiarios'][] = $row;
					}
					if(count($data['beneficiarios'])==0){
						$data['error']=2;
						$data['mensaje']="El trabajador no tiene beneficiarios para regalo de navidad";
					}
				}
			}
		}else{
			$data['error']=1;
			$data['mensaje']="No existe el trabajador";
		}
		echo json_encode($data);
	break;
	case 'GUARDAR':
		$idPersonas = $_POST['v1'];
		$sql="update aportes015 set flag='S' where idpersona in (".$idPersonas.")";
		if($rs = $db->querySimple($sql))
			echo 1;
		else
			echo 0;
	break;
}

?>