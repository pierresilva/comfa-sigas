<?php 

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';

include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'utilitario.class.php';


class DefinicionNovedad {
    /** #### ATRIBUTOS #### **/
	    /**
     * Contiene el objeto Conexion
     * @var Conexion 
     */
    public static $con = null;
    /** #### METODOS MAGICOS #### **/
	function __construct(){
		try{
			self::$con = IFXDbManejador::conectarDB();
			if( self::$con->conexionID==null ){
				throw new Exception(self::$con->error);
			}
		}catch(Exception $e){
			echo $e->getMessage();
			exit();
		}
	}
	    public function insert_definicion_novedad($datos, $usuario){
        $queryDML = " INSERT INTO aportes440 (definicion_novedad, descripcion, estado, usuario_creacion)
			VALUES('{$datos["definicion_novedad"]}',:descripcion,'{$datos["estado"]}','$usuario')";
        
        $statement = self::$con->conexionID->prepare($queryDML);
        $statement->bindValue(":descripcion", $datos["descripcion"],  PDO::PARAM_STR);
        $guardada = $statement->execute();
        return $guardada == false ? 0 : 1;
    }
    public function update_definicion_novedad($datos, $usuario){
        $queryDML = " UPDATE aportes440
					 SET definicion_novedad = '{$datos["definicion_novedad"]}'
					     , descripcion = :descripcion
					     , estado = '{$datos["estado"]}'
					     , usuario_modificacion = '$usuario'
					     , fecha_modificacion = GETDATE()
				      WHERE id_definicion_novedad = '{$datos["id_definicion_novedad"]}'";
        
        $statement = self::$con->conexionID->prepare($queryDML);
        $statement->bindValue(":descripcion", $datos["descripcion"],  PDO::PARAM_STR);
        $guardada = $statement->execute();
        return $guardada == false ? 0 : 1;
    }

    public function select_definicion_novedad($arrAtributoValor){
    	$attrEntidad = array(
    			array("nombre"=>"definicion_novedad","tipo"=>"TEXT","entidad"=>"a440")
    			,array("nombre"=>"estado","tipo"=>"TEXT","entidad"=>"a440"));
    
    	$filtroSql = Utilitario::obtenerFiltro($attrEntidad,$arrAtributoValor);
    	$sql = " SELECT
				    a440.id_definicion_novedad
				    , a440.definicion_novedad
				    , a440.descripcion
				    , a440.estado
				    , a440.usuario_creacion
				    , a440.usuario_modificacion
				    , a440.fecha_creacion
				    , a440.fecha_modificacion
				FROM aportes440 a440
    	$filtroSql";
    	return Utilitario::fetchConsulta($sql,self::$con);
    }
}
?>