var uri = "",
	uriLogUpdatValorPaqueEscol = "",
	objAccion = {UPDATE:"U"};

$(function(){
	uri = src();
	uriLogUpdatValorPaqueEscol = uri + "resolucion1056/logica/updat_valor_paque_escol.log.php";
	
	datepickerC("ANIO","#txtAnio");
});

function nuevo(){
	limpiarCampos2("#txtAnio,#txtValor");
}

function guardar(){
	
	//Verificar campos obligatorios
	if(validateElementsRequired()) return false;
	
	var objDatos = {
			anio: $("#txtAnio").val().trim()
			, valor: $("#txtValor").val().trim()
	};
	
	var accion = objAccion.UPDATE;
	
	$.ajax({
		url:uriLogUpdatValorPaqueEscol,
		type:"POST",
		data:{accion:accion,datos:objDatos},
		dataType: "json",
		async:false,
		success:function(data){
			if(data.error==0){
				alert("El valor del paquete se actualizo correctamente!!");
				nuevo();
				return true;
				
			}
			alert("ERROR al actualizar el valor del paquete!!");
		}
	});
}