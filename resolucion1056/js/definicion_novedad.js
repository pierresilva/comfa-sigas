var uri = "",
	uriLogDefinicionNovedad = "",
	objAccion = {SELECT:"S", INSERT:"I", UPDATE:"U"};

$(function(){
	uri = src();
	uriLogDefinicionNovedad = uri + "resolucion1056/logica/definicion_novedad.log.php";
	
	$("#txtDefinicionNovedad").blur(contrDefinNoved);
});

function nuevo(){
	limpiarCampos2("#hidIdDefinicionNovedad,#txtDefinicionNovedad,#txaDescripcion,#cmbEstado");
}

function contrDefinNoved(){
	var definicionNovedad = $("#txtDefinicionNovedad").val().trim();
	
	if(!definicionNovedad) return false;
	
	var objFiltro = {definicion_novedad:definicionNovedad};
	var arrDatos = fetchDefinNoved(objAccion.SELECT, objFiltro);
	
	if(arrDatos && typeof arrDatos == "object" && arrDatos.data.length>0){
		
		if(!confirm("La definicion ya existe, desea modificarla!!")){
			nuevo();
			return false;
		}
		
		arrDatos = arrDatos.data[0];
		
		$("#hidIdDefinicionNovedad").val(arrDatos.id_definicion_novedad);
		$("#txaDescripcion").val(arrDatos.descripcion);
		$("#cmbEstado").val(arrDatos.estado);
	}
}

function guardar(){
	
	//Verificar campos obligatorios
	if(validateElementsRequired()) return false;
	
	var objDatos = {
			id_definicion_novedad: $("#hidIdDefinicionNovedad").val().trim()
			, definicion_novedad: $("#txtDefinicionNovedad").val().trim()
			, estado: $("#cmbEstado").val()
			, descripcion: $("#txaDescripcion").val().trim()
	};
	
	var accion = objDatos.id_definicion_novedad?objAccion.UPDATE:objAccion.INSERT;
	
	$.ajax({
		url:uriLogDefinicionNovedad,
		type:"POST",
		data:{accion:accion,datos:objDatos},
		dataType: "json",
		async:false,
		success:function(data){
			if(data.error==0){
				alert("La definicion de la novedad se guardo correctamente!!");
				nuevo();
				return true;
				
			}
			
			alert("ERROR al guardar la definicion de la novedad!!");
		}
	});
}

function fetchDefinNoved(accion,objFiltro){
	var resultado = null;
	
	$.ajax({
		url:uriLogDefinicionNovedad,
		type:"POST",
		data:{accion:accion,objDatosFiltro:objFiltro},
		dataType: "json",
		async:false,
		success:function(data){
			resultado = data;
		}
	});
	
	return resultado;
}
