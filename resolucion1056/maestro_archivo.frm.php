<?php	
	$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
	include_once  $root;
	
	$url=$_SERVER['PHP_SELF'];
	include_once $raiz.DIRECTORY_SEPARATOR.'phpComunes'.DIRECTORY_SEPARATOR.'auditoria.php';
	auditar($url);
?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<title>::Maestro Archivos::</title>
		
		<link href="../css/Estilos.css" rel="stylesheet" type="text/css" />
		<link href="../css/marco.css" rel="stylesheet" type="text/css" />
		<link href="../newcss/css.1.8/jquery-ui-1.8.17.custom.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="../newjs/1.6/jquery-1.6.2.min.js"></script>
		<script type="text/javascript" src="../newjs/1.6/jquery-ui-1.8.16.custom.min.js"></script>
		<script type="text/javascript" src="../js/comunes.js"></script>
		<script type="text/javascript" src="js/maestro_archivo.js"></script>
	</head>
	<body>
		<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0">
			<tbody>
		  	<!-- ESTILOS SUPERIOR TABLA-->
		  	<tr>
			    <td width="13" height="29" class="arriba_iz">&nbsp;</td>
			    <td class="arriba_ce"><span class="letrablanca">::&nbsp;Maestro Archivo::</span></td>
			    <td width="13" class="arriba_de" align="right">&nbsp;</td>
		  	</tr> 
		  	<!-- ESTILOS ICONOS TABLA-->
		  	<tr>
		    	<td class="cuerpo_iz">&nbsp;</td>
		    	<td class="cuerpo_ce">
					<img width="3" height="1" src="../imagenes/spacer.gif" />   
		    	</td>
		    	<td class="cuerpo_de">&nbsp;</td>
		  	</tr>
		  	<!-- ESTILOS MEDIO TABLA-->
		  	<tr>
		    	<td class="cuerpo_iz">&nbsp;</td>
		    	<td class="cuerpo_ce"></td>
		    	<td class="cuerpo_de">&nbsp;</td>
		  	</tr>  
		  	<!-- CONTENIDO TABLA-->
		  	<tr>
		   		<td class="cuerpo_iz">&nbsp;</td>
		   		<td class="cuerpo_ce">
					<table cellspacing="0" width="90%" border="0" class="tablero" align="center">
		      			<tbody>
		      				<tr>
			      				<td >Tipo Archivo:</td>
								<td >
									<select name="cmbTipoArchivo" id="cmbTipoArchivo" class="boxlargo element-required">
									</select>
									<span class="clsObligatorio">&nbsp;&nbsp;&nbsp;</span>
								</td>
			        		</tr>
			    			<tr>
			      				<td >Fecha Informaci&oacute;n:</td>
								<td >
									<input type="text" name="txtFechaInicio" id="txtFechaInicio" class="boxfecha element-required" readonly="readonly"/>
									-
									<input type="text" name="txtFechaFin" id="txtFechaFin" class="boxfecha element-required" readonly="readonly"/>
									<span class="clsObligatorio">&nbsp;&nbsp;&nbsp;</span>
								</td>
			        		</tr>
			        		<tr id="trPaqueteEscolar" style="display: none;">
			      				<td >Paquete Escolar:</td>
								<td >
									<input type="checkbox" name="chkPaqueteEscolar" id="chkPaqueteEscolar" value="S"/>
								</td>
			        		</tr>
			        		<tr>
			      				<td >Enviado:</td>
								<td >
									<input type="checkbox" name="chkEnviado" id="chkEnviado" value="S"/>
								</td>
			        		</tr>
			        		<tr>
								<td colspan="2" style="text-align: center;">
									<input type="button" name="btnGenerar" id="btnGenerar" value="Generar"/>
								</td>
			        		</tr>
		      			</tbody>
		      		</table>
					</td>
		    		<td class="cuerpo_de">&nbsp;</td>
		  		</tr>		  
		  		<!-- ESTILOS PIE TABLA-->
		  		<tr>
		    		<td class="abajo_iz" >&nbsp;</td>
		    		<td class="abajo_ce" ></td>
		    		<td class="abajo_de" >&nbsp;</td>
		  		</tr>		  
			</tbody>
		</table>
	</body>
</html>