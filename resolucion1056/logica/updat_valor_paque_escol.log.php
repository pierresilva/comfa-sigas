<?php 

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once '../clases' . DIRECTORY_SEPARATOR . 'paquete_escolar.class.php';

/**
 * Contiene los datos de retorno al formulario
 * @var array
 */
$arrMensaje = array('error'=>0,'data'=>null);

/*Verificar si accion existe*/
$accion = isset($_POST['accion'])?$_POST['accion']:'';

$usuario = $_SESSION["USUARIO"];

switch($accion){
    case 'U':

    	$datos = $_POST["datos"];
    	 
    	$objPaqueteEscolar = new PaqueteEscolar();
    	$resultado = $objPaqueteEscolar->update_valor($datos,$usuario);
    	
    	$arrMensaje["error"] = 0;
    	
    	if($resultado == 0){
    		$arrMensaje["error"] = 1;
    	}
    	
        break;
}

echo ($accion != '') ? json_encode($arrMensaje) : "";
?>