<?php 
date_default_timezone_set('America/Bogota');
set_time_limit(0);
ini_set('memory_limit','2000M');

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
include_once $raiz.'/config.php';

include_once '../clases' . DIRECTORY_SEPARATOR . 'maestro_afiliado.class.php';
include_once '../clases' . DIRECTORY_SEPARATOR . 'maestro_subsidio.class.php';
include_once '../clases' . DIRECTORY_SEPARATOR . 'maestro_novedad_ays.class.php';
include_once '../clases' . DIRECTORY_SEPARATOR . 'maestro_novedad_eya.class.php';

/**
 * Contiene los datos de retorno al formulario
 * @var array
 */
$arrMensaje = array('error'=>0,'data'=>null);

/*Verificar si accion existe*/
$accion = isset($_POST['accion'])?$_POST['accion']:'';

$usuario = $_SESSION["USUARIO"];

switch($accion){
    case 'CMCP':
    	
    	$datos = $_POST["datos"];
    	
    	$arrDatos = array("fecha_inici_perio_infor"=>$datos["fecha_inici_perio_infor"]
    			,"fecha_final_perio_infor"=>$datos["fecha_final_perio_infor"]
    			,"id_tipo_archivo"=>$datos["id_tipo_archivo"]
    			,"enviado"=>$datos["enviado"]
    			,"ruta_generados"=>$ruta_generados);
    	
    	$objMaestroAfiliado = new MaestroAfiliado();
    	$reultado = $objMaestroAfiliado->ejecutar($arrDatos,$usuario);
    	
        $arrMensaje["error"] = 0;
            
        if($reultado == false){
        	$arrMensaje["error"] = 1;
       	}
        break;
	case 'CMSP':
        	 
    	$datos = $_POST["datos"];
        
        $arrDatos = array("fecha_inici_perio_infor"=>$datos["fecha_inici_perio_infor"]
        		,"fecha_final_perio_infor"=>$datos["fecha_final_perio_infor"]
        		,"id_tipo_archivo"=>$datos["id_tipo_archivo"]
        		,"enviado"=>$datos["enviado"]
        		,"paquete_escolar"=>$datos["paquete_escolar"]
        		,"ruta_generados"=>$ruta_generados);
        	 
        $objMaestroSubsidio = new MaestroSubsidio();
        $reultado = $objMaestroSubsidio->ejecutar($arrDatos,$usuario);
        	        	 
        $arrMensaje["error"] = 0;
        
        if($reultado == false){
        	$arrMensaje["error"] = 1;
        }
        break;
	case 'CNCA':
        
        	$datos = $_POST["datos"];
        
        	$arrDatos = array("fecha_inici_perio_infor"=>$datos["fecha_inici_perio_infor"]
        			,"fecha_final_perio_infor"=>$datos["fecha_final_perio_infor"]
        			,"id_tipo_archivo"=>$datos["id_tipo_archivo"]
        			,"enviado"=>$datos["enviado"]
        			,"ruta_generados"=>$ruta_generados);
        
        	$objMaestroNovedadAyS = new MaestroNovedadAyS();
        	$reultado = $objMaestroNovedadAyS->ejecutar($arrDatos,$usuario);
        	 
        	$arrMensaje["error"] = 0;
        
        	if($reultado == false){
        		$arrMensaje["error"] = 1;
        	}
        	break;
        	
	case 'CNCE':
        	
    	$datos = $_POST["datos"];
        	
    	$arrDatos = array("fecha_inici_perio_infor"=>$datos["fecha_inici_perio_infor"]
    			,"fecha_final_perio_infor"=>$datos["fecha_final_perio_infor"]
    			,"id_tipo_archivo"=>$datos["id_tipo_archivo"]
        		,"enviado"=>$datos["enviado"]
        		,"ruta_generados"=>$ruta_generados);
        	
        $objMaestroNovedadEyA = new MaestroNovedadEyA();
        $reultado = $objMaestroNovedadEyA->ejecutar($arrDatos,$usuario);
        	
        $arrMensaje["error"] = 0;
        	
        if($reultado == false){
        	$arrMensaje["error"] = 1;
        }
        break;
}

echo ($accion != '') ? json_encode($arrMensaje) : "";
?>