/*
* @autor:      Ing. felipe rodriguez
* @fecha:      28/12/2010
* objetivo:
*/
/* var URL=src();*/
function mostrarAyuda(){
	$("#ayuda").dialog('open' );
}

function notas(){
	$("#dialog-form2").dialog('open');
}	

function procesar(){
$("#ajax").html('<img src=../../imagenes/ajax-loader.gif />');
$("#boton").html(' ');
$.ajax({
	url: 'procesarPlano.php',
	type: "POST",
	async:false,
	success: function(datos){
	$("#ajax").html('&nbsp;');
    $("#ajax").html(datos);	
	}
});	
}

function verificar(){
$("#ajax").html('<img src=../../imagenes/ajax-loader.gif />');
$("#boton").html(' ');
$.ajax({
	url: 'verificarInfoPer.php',
	type: "POST",
	async:false,
	success: function(datos){
	$("#ajax").html('&nbsp;');
    $("#ajax").html(datos);	
	}
});	
}


function procesarper(){
$("#ajax").html('<img src=../../imagenes/ajax-loader.gif />');
$("#boton").html(' ');
$.ajax({
	url: 'procesarPlanoPer.php',
	type: "POST",
	async:false,
	success: function(datos){
	$("#ajax").html('&nbsp;');
    $("#ajax").html(datos);	
	}
});	
}

function procesarPlano(op){
    var archivo='';
    var v0='';
	if($("#Mes").val()=='Seleccione..'){
	var v1='0';
	}
	else {
	var v1=$("#Mes").val();	
	}
    switch (op) {
        case 1: archivo='procesarPlano_1.php'; v0=1; break;
        case 2: archivo='procesarPlano_SubDes.php'; v0=2; break;
    }
	$("#boton").html("Procesando el archivo... espere por favor<br><img src=../../imagenes/ajax-loader.gif />" );
	$.get(URL+'RUAF/subsidios/procesarPlano_SubDes.php',{v1:v1}, function(datos){
            if (datos==0){
                $("#boton").html("No se pudo procesar el plano!");
                return false;
            }
            if(datos==1){
                $("#boton").html("No se pudo insertar control archivo movimientos");
                return false;
            }
            if(datos==2){
                $("#boton").html("No hay movimientos para cargar!");
                return false;
            }
            $("#boton").html("");
            $("#mensaje").html("<small>Informacion procesada satisfactoriamente.</b></small>");
            $("#ajax").html("<br><label style='cursor:pointer' onClick='descargar();'><img src=../../imagenes/descargarArchivo.png width=143 height=30 /></label>");
            
        });
	}
	
function procesarPlanoSub(op){
    var archivo='';
    var v0='';
	if($("#Mes").val()=='Seleccione..'){
	var v1='0';
	}
	else {
	var v1=$("#Mes").val();	
	}	
    switch (op) {
        case 1: archivo='procesarPlano_1.php'; v0=1; break;
        case 2: archivo='procesarPlano_Sub.php'; v0=2; break;
    }
	$("#boton").html("Procesando el archivo... espere por favor<br><img src=../../imagenes/ajax-loader.gif />" );
	$.get(URL+'RUAF/subsidios/procesarPlano_Sub.php',{v1:v1}, function(datos){
            if (datos==0){
                $("#boton").html("No se pudo procesar el plano!");
                return false;
            }
            if(datos==1){
                $("#boton").html("No se pudo insertar control archivo movimientos");
                return false;
            }
            if(datos==2){
                $("#boton").html("No hay movimientos para cargar!");
                return false;
            }
            $("#boton").html("");
            $("#mensaje").html("<small>Informacion procesada satisfactoriamente.</b></small>");
            $("#ajax").html("<br><label style='cursor:pointer' onClick='descargar();'><img src=../../imagenes/descargarArchivo.png width=143 height=30 /></label>");
            
        });
	}
	
function procesarPlanoSubCm(op){
    var archivo='';
    var v0='';
	if($("#Mes").val()=='Seleccione..'){
	var v1='0';
	}
	else {
	var v1=$("#Mes").val();	
	}
    switch (op) {
        case 1: archivo='procesarPlano_1.php'; v0=1; break;
        case 2: archivo='procesarPlano_SubCm.php'; v0=2; break;
    }
	$("#boton").html("Procesando el archivo... espere por favor<br><img src=../../imagenes/ajax-loader.gif />" );
	$.get(URL+'RUAF/subsidios/procesarPlano_SubCm.php',{v1:v1}, function(datos){
            if (datos==0){
                $("#boton").html("No se pudo procesar el plano!");
                return false;
            }
            if(datos==1){
                $("#boton").html("No se pudo insertar control archivo movimientos");
                return false;
            }
            if(datos==2){
                $("#boton").html("No hay movimientos para cargar!");
                return false;
            }
            $("#boton").html("");
            $("#mensaje").html("<small>Informacion procesada satisfactoriamente.</b></small><br><label style='cursor:pointer' onClick='descargar();'><img src=../../imagenes/descargarArchivo.png width=143 height=30 /></label>");      
        });
	}
	
	function procesarPlanoSubViv(op){
    var archivo='';
    var v0='';
	if($("#Mes").val()=='Seleccione..'){
	var v1='0';
	}
	else {
	var v1=$("#Mes").val();	
	}
    switch (op) {
        case 1: archivo='procesarPlano_1.php'; v0=1; break;
        case 2: archivo='procesarPlano_SubViv.php'; v0=2; break;
    }
	$("#boton").html("Procesando el archivo... espere por favor<br><img src=../../imagenes/ajax-loader.gif />" );
	$.get(URL+'RUAF/subsidios/procesarPlano_SubViv.php',{v1:v1}, function(datos){
            if (datos==0){
                $("#boton").html("No se pudo procesar el plano!");
                return false;
            }
            if(datos==1){
                $("#boton").html("No se pudo insertar control archivo movimientos");
                return false;
            }
            if(datos==2){
                $("#boton").html("No hay movimientos para cargar!");
                return false;
            }
            $("#boton").html("");
            $("#mensaje").html("<small>Informacion procesada satisfactoriamente.</b></small>");
            $("#ajax").html("<br><label style='cursor:pointer' onClick='descargar();'><img src=../../imagenes/descargarArchivo.png width=143 height=30 /></label>");      
        });
	}
	
	function procesarPlanoSubEdu(op){
    var archivo='';
    var v0='';
	if($("#Mes").val()=='Seleccione..'){
	var v1='0';
	}
	else {
	var v1=$("#Mes").val();	
	}
    switch (op) {
        case 1: archivo='procesarPlano_1.php'; v0=1; break;
        case 2: archivo='procesarPlano_SubEdu.php'; v0=2; break;
    }
	$("#boton").html("Procesando el archivo... espere por favor<br><img src=../../imagenes/ajax-loader.gif />" );
	$.get(URL+'RUAF/subsidios/procesarPlano_SubEdu.php',{v1:v1}, function(datos){
            if (datos==0){
                $("#boton").html("No se pudo procesar el plano!");
                return false;
            }
            if(datos==1){
                $("#boton").html("No se pudo insertar control archivo movimientos");
                return false;
            }
            if(datos==2){
                $("#boton").html("No hay movimientos para cargar!");
                return false;
            }
            $("#boton").html("");
            $("#mensaje").html("<small>Informacion procesada satisfactoriamente.</b></small>");
            $("#ajax").html("<br><label style='cursor:pointer' onClick='descargar();'><img src=../../imagenes/descargarArchivo.png width=143 height=30 /></label>");      
        });
	}
	
function procesarPlanoSubEsp(op){
    var archivo='';
    var v0='';
	
	if($("#valor").val()=='0'){
	var v1='0';
	}
	else {
	var v1=$("#valor").val();	
	}
    switch (op) {
        case 1: archivo='procesarPlano_1.php'; v0=1; break;
        case 2: archivo='procesarPlano_SubEsp.php'; v0=2; break;
    }
	$("#boton").html("Procesando el archivo... espere por favor<br><img src=../../imagenes/ajax-loader.gif />" );
	$.get(URL+'RUAF/subsidios/procesarPlano_SubEsp.php',{v1:v1}, function(datos){
            if (datos==0){
                $("#boton").html("No se pudo procesar el plano!");
                return false;
            }
            if(datos==1){
                $("#boton").html("No se pudo insertar control archivo movimientos");
                return false;
            }
            if(datos==2){
                $("#boton").html("No hay movimientos para cargar!");
                return false;
            }
            $("#boton").html("");
            $("#mensaje").html("<small>Informacion procesada satisfactoriamente.</b></small>");
            $("#ajax").html("<br><label style='cursor:pointer' onClick='descargar();'><img src=../../imagenes/descargarArchivo.png width=143 height=30 /></label>");    
        });
	}
	
function procesarCierrePlanoSub(op){
    var archivo='';
    var v0='';
    switch (op) {
        case 1: archivo='procesarPlano_1.php'; v0=1; break;
        case 2: archivo='procesarCierrePlano_Sub.php'; v0=2; break;
    }
	$("#boton").html("Procesando el archivo... espere por favor<br><img src=../../imagenes/ajax-loader.gif />" );
	$.get(URL+'RUAF/subsidios/procesarCierrePlano_Sub.php',{v0:v0}, function(datos){     
            $("#boton").html("");
            $("#mensaje").html("<small>Archivo cerrado satisfactoriamente.</b></small>");       
        });
	}

function descargar(){
	var url=URL+'phpComunes/descargaPlanos.php';
	window.open(url);
}
