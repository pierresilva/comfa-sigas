<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$url=$_SERVER['PHP_SELF'];
include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'auditoria.php';
auditar($url);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::Supervivencia::</title>
<link href="../../css/Estilos.css" rel="stylesheet" type="text/css" /> 
<link href="../../css/marco.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../../js/1.6/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="../../js/1.6/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="../../js/comunes.js"></script>
<script type="text/javascript" src="js/generarArchivo.js"></script>

<script type="text/javascript">
shortcut.add("Shift+F",function() {
	var URL=src();
	var url=URL+"aportes/trabajadores/consultaTrabajador.php";
    window.open(url,"_blank");
    },{
	'propagate' : true,
	'target' : document 
});
</script>
</head>
<body>
<br /><br />
<table border="0" align="center" cellpadding="0" cellspacing="0" width="80%">
  <tbody>
  <!-- ESTILOS SUPERIOR TABLA-->
  <tr>
    <td width="13" height="29" class="arriba_iz">&nbsp;</td>
    <td class="arriba_ce"><span class="letrablanca">::&nbsp;Preparar archivo validar supervivencia&nbsp;::</span></td>
    <td width="13" class="arriba_de" align="right">&nbsp;</td>
  </tr>
  <!-- ESTILOS ICONOS TABLA-->
  <tr>
   <td class="cuerpo_iz">&nbsp;</td>
	<td class="cuerpo_ce">&nbsp;</td>
    <td class="cuerpo_de">&nbsp;</td>
  </tr>
  
  <!-- ESTILOS MEDIO TABLA-->
  <tr>
    <td class="cuerpo_iz">&nbsp;</td>
    <td class="cuerpo_ce">
		<table border="0" cellpadding="5" align="center" class="tablero" width="90%">
			<tr>
				<td width="31%">Afiliados</td>
				<td width="69%"><input type="radio" name="radioSuperv" value="1" />
			    </td>
			</tr>
			<tr>
				<td>Beneficiarios (todos)</td>
				<td><input type="radio" name="radioSuperv" value="2" /></td>
			</tr>
			<tr>
				<td>Solo Padres</td>
				<td><input type="radio" name="radioSuperv" value="3" /></td>
			</tr>
			<tr>
			  <td>Solo hijos,hijastroa,hermanos discapacitados</td>
			  <td><input type="radio" name="radioSuperv" value="4" /></td>
		  </tr>
		</table>
	<BR>
    <div id="boton" align="center"><input type="submit" value="Procesar" id="btnProcesar" class="ui-state-default" style="margin-first:20px"/></div>
	<br/>
    </td>
    <td class="cuerpo_de">&nbsp;</td>
  </tr>
  <!-- CONTENIDO TABLA-->
 
  
  <!-- ESTILOS PIE TABLA-->
  <tr>
    <td height="34" class="abajo_iz" >&nbsp;</td>
    <td class="abajo_ce" ></td>
    <td class="abajo_de" >&nbsp;</td>
  </tr>
  
</tbody>
</table>

<!-- colaboracion en linea -->

<div id="dialogo-archivo" title="Archivo banco">
<div id="progreso" style="display: none; font-size: 15pt; font-weight: bold;">Procesado(s) <span id="pg">0</span> de <span id="tt"></span> archivo(s)</div>
<div id="log"></div>
</div>
<div id="dialog-form2" title="Colaboraci&oacute;n en l&iacute;nea" style="display:none">
<p>Por favor diligencie este formulario para enviar comentarios, errores o falencias encontradas en el proceso. M&aacute;ximo 250 caracteres </p>
<label>Tus comentarios:</label><br />
<textarea name="notas" id="notas" cols="60", rows="10"></textarea>
</div>
<!--fin colaboracion en linea-->

<!-- Manual Ayuda -->
<div id="ayuda" title="::Manual Discapacitados::"></div>
<input id="idPersona" type="hidden"  />
</body>
</html>