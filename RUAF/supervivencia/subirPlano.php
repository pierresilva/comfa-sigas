<?php

// @autor:		Orlando Puentes A
// @fecha:		mayo 31/2010
// @formulario:	procesar plano convivencia

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'config.php';
include_once $raiz.DIRECTORY_SEPARATOR.'funcionesComunes'.DIRECTORY_SEPARATOR.'funcionesComunes.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head> 
 <title>Procesar archivos</title>
 <link href="../../css/Estilos.css" rel="stylesheet" type="text/css" /> 
 <script type="text/javascript" src="../../js/1.6/jquery-1.6.2.min.js"></script>
 <script language="javascript" src="js/subirPlanoSuper.js"></script>
 </head>
 <body>
 <br />
 <p align="center" class="Rojo">Resultado de la copia de archivos</p>
 <br />
<?php 
$tipoarchivo=$_REQUEST["tipoarchivo"];
$anno=date("Y");
$mes=date("m");
$dia=date("d"); 
$directorio = $ruta_cargados. DIRECTORY_SEPARATOR. 'RUAF' .DIRECTORY_SEPARATOR.'supervivencia'.DIRECTORY_SEPARATOR.$anno.DIRECTORY_SEPARATOR.$mes.DIRECTORY_SEPARATOR.$dia.DIRECTORY_SEPARATOR; 
if(!verificaDirectorio($directorio)){
	exit(2);
	}

echo "<br><br><br><br>";

if(isset($_FILES['archivo'])){
	foreach ($_FILES['archivo']['error'] as $key => $error) {
		$file=$_FILES["archivo"]["name"][$key];
	    $archivos[]= $_FILES["archivo"]["name"][$key];
	    $cad=substr($file,0,9);
	    if($cad != "SMC0CCF32"){
	    	echo "<p align=center>El archivo: <strong>".$file."</strong> NO es de tipo respuesta</p>";
	    	exit();
	    }
	    
		if(move_uploaded_file($_FILES["archivo"]["tmp_name"][$key],$directorio.$_FILES["archivo"]["name"][$key])){
		
		}
		else{ echo "Ocurrio un problema al intentar subir el archivo."; }
	   }
	   }
	
 else{
 	echo "<p class=Rojo>No se pudo cargar el archivo!</p>";
 	exit();
 }
 
for($i=0; $i<count($archivos); $i++){
	echo "<p align=center>Archivo copiado en el servidor: <strong>".$archivos[$i]."</strong></p>"; 
}
if($i>0){
?>
<center>
    <div id="boton">    
    <p style="text-decoration:none; font-size:12px; color:#F00; font-weight:bold; cursor:pointer" onclick="procesar('<?php echo $tipoarchivo; ?>');">Procesar estos archivos</p>
</div>
</center>
<?php
	}
 ?> 
 <div id="ajax" align="center"> </div>
<div id="mensajes" align="center">..:..</div>
 </body>
 </html>
 
