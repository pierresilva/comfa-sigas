<?php
/* autor:       orlando puentes
 * fecha:       14/10/2010
 * objetivo:    
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'rsc' . DIRECTORY_SEPARATOR . 'conexion.class.php';

class Ruaf{
	//constructor d
	var $con;
	var $fechaSistema;
	function Ruaf(){
		$this->con=new DBManager;
	}

function procesar_afi(){
	if($this->con->conectar()==true){
 		$sql="SELECT tipdoc, documento, p_apellido, s_apellido, p_nombre, s_nombre, codnovedad, val1, val2, val3, val4, val5, val6, val7, val8, procesado, fechanovedad, aprobado 
    FROM aportes410 WHERE procesado is null";
		//echo $sql;
		return mssql_query($sql,$this->con->conect);
	}
}

function contar_afi(){
	if($this->con->conectar()==true){
 		$sql="SELECT count(*) FROM aportes410 WHERE procesado is null";
		//echo $sql;
		return mssql_query($sql,$this->con->conect);
	}
}

function contar_aportes413_afi(){
	if($this->con->conectar()==true){
		$sql="SELECT count(*) as cuenta FROM aportes413";
		return mssql_query($sql,$this->con->conect);
	}
}

function actualizar_afi_410($documento){
	if($this->con->conectar()==true){
 		$sql="UPDATE aportes410 SET procesado='S' WHERE documento='$documento' and procesado is null";
		//echo $sql;
		return mssql_query($sql,$this->con->conect);
	}
}

function llenar413($fechCorte){
	if($this->con->conectar()==true){
		$this->limpiar413();
		
		$separador=explode("/",$fechCorte);
		$perido=$separador[0].''.$separador[1];
		
		$sql="INSERT INTO aportes413 ( tipoafil,identafili,tipobenef,identificacion,sexo,fechanacimiento,papellido,sapellido,pnombre,snombre,iddepresidencia,munirecide,fechaingreso,codigoentidad,nombreentidad,codtipoafi,dptolabora,munilabora,estadoafil,codtipoblacion,tipoaportante,nit,digito,fechaafiliacion,datoscertificado,aportealdia )
				SELECT * FROM (
					SELECT DISTINCT 
						'' AS tipoafil,'' AS identafili,a91.codigo AS tipobenef,a15.identificacion,a15.sexo,a15.fechanacimiento,a15.papellido,a15.sapellido,a15.pnombre,a15.snombre,a15.iddepresidencia,substring(a15.idciuresidencia,3,5) AS munirecide,a16.fechaingreso,'CCF32' AS codigoentidad,'CAJA DE COMPENSACION FAMILIAR DEL HUILA  COMFAMILIAR' as nombreentidad,'' as codtipoafi,a48.iddepartamento AS dptolabora,substring(a48.idciudad,3,5) AS munilabora,'1' AS estadoafil,'1' AS codtipoblacion,e91.codigo AS tipoaportante,a48.nit,a48.digito,a48.fechaafiliacion,'0' AS datoscertificado,
						CASE WHEN (SELECT count(*) FROM aportes011 a11 WHERE a11.idempresa=a16.idempresa and a11.periodo='".$perido."') >0 THEN '1' ELSE '0' END AS aportealdia 
					FROM aportes016 a16 
					INNER JOIN aportes015 a15 ON a16.idpersona=a15.idpersona AND a16.estado='A' 
					INNER JOIN aportes048 a48 ON a16.idempresa=a48.idempresa 
					LEFT JOIN aportes091 a91 ON a15.idtipodocumento=a91.iddetalledef
					LEFT JOIN aportes091 e91 ON a48.idtipodocumento=e91.iddetalledef
					WHERE a16.fechaingreso=(SELECT min(b16.fechaingreso) FROM aportes016 b16 WHERE b16.idpersona=a16.idpersona AND b16.estado='A' AND b16.fechaingreso <='".$fechCorte."')

					UNION
					
				---------------- AFILIADOS ACTIVO PROTECCION ----------------
					SELECT DISTINCT 
						'' AS tipoafil,'' AS identafili,a91.codigo AS tipobenef,a15.identificacion,a15.sexo,a15.fechanacimiento,a15.papellido,a15.sapellido,a15.pnombre,a15.snombre,a15.iddepresidencia,substring(a15.idciuresidencia,3,5) AS munirecide,a16.fechaingreso,'CCF32' AS codigoentidad,'CAJA DE COMPENSACION FAMILIAR DEL HUILA  COMFAMILIAR' nombreentidad,'' as codtipoafi,a48.iddepartamento AS dptolabora,substring(a48.idciudad,3,5) AS munilabora,'0' AS estadoafil,'1' AS codtipoblacion,e91.codigo AS tipoaportante,a48.nit,a48.digito,a48.fechaafiliacion,'0' AS datoscertificado,
						CASE WHEN (SELECT count(*) FROM aportes011 a11 WHERE a11.idempresa=a16.idempresa and a11.periodo='".$perido."') >0 THEN '1' ELSE '0' END AS aportealdia 
					FROM aportes017 a16
					INNER JOIN aportes015 a15 ON a16.idpersona=a15.idpersona --AND a16.estado='A'
					INNER JOIN aportes048 a48 ON a16.idempresa=a48.idempresa
					LEFT JOIN aportes091 a91 ON a15.idtipodocumento=a91.iddetalledef
					LEFT JOIN aportes091 e91 ON a48.idtipodocumento=e91.iddetalledef
					WHERE 0=(SELECT count(*) FROM aportes016 b16 WHERE b16.idpersona=a16.idpersona) AND a16.estadofidelidad='A'

					UNION

				------------------- BENEFICIARIOS ACTIVOS -------------------
					SELECT DISTINCT 
						e91.codigo AS tipoafil,t15.identificacion AS afiliado,a91.codigo AS tipobenef,a15.identificacion,a15.sexo,a15.fechanacimiento,a15.papellido,a15.sapellido,a15.pnombre,a15.snombre,a15.iddepresidencia,substring(a15.idciuresidencia,3,5) AS munirecide,a21.fechaafiliacion,'CCF32' AS codigoentidad,'CAJA DE COMPENSACION FAMILIAR DEL HUILA  COMFAMILIAR' nombreentidad,'' as codtipoafi,'' AS dptolabora,'' AS munilabora,'1' AS estadoafil,CASE a21.idparentesco WHEN '34' THEN '3' ELSE '2' END AS codtipoblacion,'','','','','0' AS datoscertificado,
						CASE WHEN (SELECT count(*) FROM aportes011 a11 WHERE a11.idempresa=a16.idempresa and a11.periodo='".$perido."') >0 THEN '1' ELSE '0' END AS aportealdia 
					FROM aportes016 a16
					INNER JOIN aportes021 a21 ON a16.idpersona=a21.idtrabajador AND a21.estado='A'
					INNER JOIN aportes015 a15 ON a21.idbeneficiario=a15.idpersona --and ((a21.idparentesco=34 AND a21.conviven='S') OR (a21.idparentesco IN (35,36,37,38)))
					INNER JOIN aportes015 t15 ON a21.idtrabajador=t15.idpersona --and ((a21.idparentesco=34 AND a21.conviven='S') OR (a21.idparentesco IN (35,36,37,38)))
					--INNER JOIN aportes048 a48 ON a16.idempresa=a48.idempresa
					LEFT JOIN aportes091 a91 ON a15.idtipodocumento=a91.iddetalledef
					LEFT JOIN aportes091 e91 ON t15.idtipodocumento=e91.iddetalledef
					WHERE a16.fechaingreso=(SELECT min(b16.fechaingreso) FROM aportes016 b16 WHERE b16.idpersona=a16.idpersona AND b16.estado='A' AND b16.fechaingreso <='".$fechCorte."')

					UNION

				------------------ BENEFICIARIOS PROTECCION ------------------
					SELECT DISTINCT 
						e91.codigo AS tipoafil,t15.identificacion AS afiliado,a91.codigo AS tipobenef,a15.identificacion,a15.sexo,a15.fechanacimiento,a15.papellido,a15.sapellido,a15.pnombre,a15.snombre,a15.iddepresidencia,substring(a15.idciuresidencia,3,5) AS munirecide,a21.fechaafiliacion,'CCF32' AS codigoentidad,'CAJA DE COMPENSACION FAMILIAR DEL HUILA  COMFAMILIAR' nombreentidad,'' as codtipoafi,'' AS dptolabora,'' AS munilabora,'0' AS estadoafil,CASE a21.idparentesco WHEN '34' THEN '3' ELSE '2' END AS codtipoblacion,'','','','', '0' AS datoscertificado,
						CASE WHEN (SELECT count(*) FROM aportes011 a11 WHERE a11.idempresa=a16.idempresa and a11.periodo='".$perido."') >0 THEN '1' ELSE '0' END AS aportealdia 
					FROM aportes017 a16
					INNER JOIN aportes021 a21 ON a16.idpersona=a21.idtrabajador AND a21.estado='A'
					INNER JOIN aportes015 a15 ON a21.idbeneficiario=a15.idpersona --and ((a21.idparentesco=34 AND a21.conviven='S') OR (a21.idparentesco IN (35,36,37,38)))
					INNER JOIN aportes015 t15 ON a21.idtrabajador=t15.idpersona --and ((a21.idparentesco=34 AND a21.conviven='S') OR (a21.idparentesco IN (35,36,37,38)))
					--INNER JOIN aportes048 a48 ON a16.idempresa=a48.idempresa
					LEFT JOIN aportes091 a91 ON a15.idtipodocumento=a91.iddetalledef
					LEFT JOIN aportes091 e91 ON t15.idtipodocumento=e91.iddetalledef
					WHERE 0=(SELECT count(*) FROM aportes016 b16 WHERE b16.idpersona=a16.idpersona) AND a16.estadofidelidad='A'
				) a	";
		return mssql_query($sql,$this->con->conect);
	}
}

function consultar_aportes413_afi($rowini, $rowfin){
	if($this->con->conectar()==true){
		$sql="SELECT * FROM (
				SELECT * , ROW_NUMBER() OVER (ORDER BY idconsecutivo DESC) as row 
				FROM aportes413
			  ) a WHERE row > $rowini and row <= $rowfin";
		//echo $sql;
		return mssql_query($sql,$this->con->conect);
	}
}

function procesar_afi_ma($rowini, $rowfin){
	if($this->con->conectar()==true){		
 		$sql="SELECT * FROM(
				SELECT * , ROW_NUMBER() OVER (ORDER BY identificacion desc) as row 
				FROM (
				-------------------- AFILIADOS ACTIVOS --------------------
					SELECT DISTINCT 
						'' AS tipoafil,'' AS identafili,a91.codigo AS tipobenef,a15.identificacion,a15.sexo,a15.fechanacimiento,a15.papellido,a15.sapellido,a15.pnombre,a15.snombre,a15.iddepresidencia,substring(a15.idciuresidencia,3,5) AS munirecide,a16.fechaingreso,'CCF32' AS codigoentidad,'CAJA DE COMPENSACION FAMILIAR DEL HUILA  COMFAMILIAR' nombreentidad,a48.iddepartamento AS dptolabora,substring(a48.idciudad,3,5) AS munilabora,'1' AS estadoafil,'1' AS codtipoblacion,e91.codigo AS tipoaportante,a48.nit,a48.digito,a48.fechaafiliacion,'0' AS datoscertificado,
						CASE WHEN (SELECT count(*) FROM aportes011 a11 WHERE a11.idempresa=a16.idempresa and a11.periodo='".$perido."') >0 THEN '1' ELSE '0' END AS aportealdia 
					FROM aportes016 a16 
					INNER JOIN aportes015 a15 ON a16.idpersona=a15.idpersona AND a16.estado='A' 
					INNER JOIN aportes048 a48 ON a16.idempresa=a48.idempresa 
					LEFT JOIN aportes091 a91 ON a15.idtipodocumento=a91.iddetalledef
					LEFT JOIN aportes091 e91 ON a48.idtipodocumento=e91.iddetalledef
					WHERE a16.fechaingreso=(SELECT min(b16.fechaingreso) FROM aportes016 b16 WHERE b16.idpersona=a16.idpersona AND b16.estado='A' AND b16.fechaingreso <='".$fechCorte."')

					UNION
					
				---------------- AFILIADOS ACTIVO PROTECCION ----------------
					SELECT DISTINCT 
						'' AS tipoafil,'' AS identafili,a91.codigo AS tipobenef,a15.identificacion,a15.sexo,a15.fechanacimiento,a15.papellido,a15.sapellido,a15.pnombre,a15.snombre,a15.iddepresidencia,substring(a15.idciuresidencia,3,5) AS munirecide,a16.fechaingreso,'CCF32' AS codigoentidad,'CAJA DE COMPENSACION FAMILIAR DEL HUILA  COMFAMILIAR' nombreentidad,a48.iddepartamento AS dptolabora,substring(a48.idciudad,3,5) AS munilabora,'0' AS estadoafil,'1' AS codtipoblacion,e91.codigo AS tipoaportante,a48.nit,a48.digito,a48.fechaafiliacion,'0' AS datoscertificado,
						CASE WHEN (SELECT count(*) FROM aportes011 a11 WHERE a11.idempresa=a16.idempresa and a11.periodo='".$perido."') >0 THEN '1' ELSE '0' END AS aportealdia 
					FROM aportes017 a16
					INNER JOIN aportes015 a15 ON a16.idpersona=a15.idpersona --AND a16.estado='A'
					INNER JOIN aportes048 a48 ON a16.idempresa=a48.idempresa
					LEFT JOIN aportes091 a91 ON a15.idtipodocumento=a91.iddetalledef
					LEFT JOIN aportes091 e91 ON a48.idtipodocumento=e91.iddetalledef
					WHERE 0=(SELECT count(*) FROM aportes016 b16 WHERE b16.idpersona=a16.idpersona) AND a16.estadofidelidad='A'

					UNION

				------------------- BENEFICIARIOS ACTIVOS -------------------
					SELECT DISTINCT 
						e91.codigo AS tipoafil,t15.identificacion AS afiliado,a91.codigo AS tipobenef,a15.identificacion,a15.sexo,a15.fechanacimiento,a15.papellido,a15.sapellido,a15.pnombre,a15.snombre,a15.iddepresidencia,substring(a15.idciuresidencia,3,5) AS munirecide,a21.fechaafiliacion,'CCF32' AS codigoentidad,'CAJA DE COMPENSACION FAMILIAR DEL HUILA  COMFAMILIAR' nombreentidad,'' AS dptolabora,'' AS munilabora,'1' AS estadoafil,CASE a21.idparentesco WHEN '34' THEN '3' ELSE '2' END AS codtipoblacion,'','','','','0' AS datoscertificado,
						CASE WHEN (SELECT count(*) FROM aportes011 a11 WHERE a11.idempresa=a16.idempresa and a11.periodo='".$perido."') >0 THEN '1' ELSE '0' END AS aportealdia 
					FROM aportes016 a16
					INNER JOIN aportes021 a21 ON a16.idpersona=a21.idtrabajador AND a21.estado='A'
					INNER JOIN aportes015 a15 ON a21.idbeneficiario=a15.idpersona --and ((a21.idparentesco=34 AND a21.conviven='S') OR (a21.idparentesco IN (35,36,37,38)))
					INNER JOIN aportes015 t15 ON a21.idtrabajador=t15.idpersona --and ((a21.idparentesco=34 AND a21.conviven='S') OR (a21.idparentesco IN (35,36,37,38)))
					--INNER JOIN aportes048 a48 ON a16.idempresa=a48.idempresa
					LEFT JOIN aportes091 a91 ON a15.idtipodocumento=a91.iddetalledef
					LEFT JOIN aportes091 e91 ON t15.idtipodocumento=e91.iddetalledef
					WHERE a16.fechaingreso=(SELECT min(b16.fechaingreso) FROM aportes016 b16 WHERE b16.idpersona=a16.idpersona AND b16.estado='A' AND b16.fechaingreso <='".$fechCorte."')

					UNION

				------------------ BENEFICIARIOS PROTECCION ------------------
					SELECT DISTINCT 
						e91.codigo AS tipoafil,t15.identificacion AS afiliado,a91.codigo AS tipobenef,a15.identificacion,a15.sexo,a15.fechanacimiento,a15.papellido,a15.sapellido,a15.pnombre,a15.snombre,a15.iddepresidencia,substring(a15.idciuresidencia,3,5) AS munirecide,a21.fechaafiliacion,'CCF32' AS codigoentidad,'CAJA DE COMPENSACION FAMILIAR DEL HUILA  COMFAMILIAR' nombreentidad,'' AS dptolabora,'' AS munilabora,'0' AS estadoafil,CASE a21.idparentesco WHEN '34' THEN '3' ELSE '2' END AS codtipoblacion,'','','','', '0' AS datoscertificado,
						CASE WHEN (SELECT count(*) FROM aportes011 a11 WHERE a11.idempresa=a16.idempresa and a11.periodo='".$perido."') >0 THEN '1' ELSE '0' END AS aportealdia 
					FROM aportes017 a16
					INNER JOIN aportes021 a21 ON a16.idpersona=a21.idtrabajador AND a21.estado='A'
					INNER JOIN aportes015 a15 ON a21.idbeneficiario=a15.idpersona --and ((a21.idparentesco=34 AND a21.conviven='S') OR (a21.idparentesco IN (35,36,37,38)))
					INNER JOIN aportes015 t15 ON a21.idtrabajador=t15.idpersona --and ((a21.idparentesco=34 AND a21.conviven='S') OR (a21.idparentesco IN (35,36,37,38)))
					--INNER JOIN aportes048 a48 ON a16.idempresa=a48.idempresa
					LEFT JOIN aportes091 a91 ON a15.idtipodocumento=a91.iddetalledef
					LEFT JOIN aportes091 e91 ON t15.idtipodocumento=e91.iddetalledef
					WHERE 0=(SELECT count(*) FROM aportes016 b16 WHERE b16.idpersona=a16.idpersona) AND a16.estadofidelidad='A'
				) A ) A WHERE row > $rowini and row <= $rowfin";
		//echo $sql;
		return mssql_query($sql,$this->con->conect);
	}
}

function contar_afi_ma(){
	if($this->con->conectar()==true){
 		$sql="SELECT top 780 idpersona, case when
			idtipodocumento = 1 then 'CC'
			when
			idtipodocumento = 2 then 'TI'
			when
			idtipodocumento = 3 then 'PA'
			when
			idtipodocumento = 4 then 'CE'
			when
			idtipodocumento = 5 then 'NI'
			when
			idtipodocumento = 6 then 'RC'
			when
			idtipodocumento = 7 then 'MS'
			when
			idtipodocumento = 8 then 'AS'
			end as tipdoc, identificacion, papellido, sapellido, pnombre, snombre, sexo, direccion, idbarrio, telefono, celular, email, idpropiedadvivienda, idtipovivienda, idciuresidencia, iddepresidencia, idzona, idestadocivil, fechanacimiento, idciunace, iddepnace, capacidadtrabajo, desplazado, discapacitado, reinsertado, idescolaridad, idprofesion, conyuges, hijos, hijosmenores, hermanos, padres, tarjeta, ruaf, biometria, rutadocumentos, fechaafiliacion, estado, validado, flag, tempo1, tempo2, tempo3, usuario, fechasistema, nombrecorto 
    FROM aportes015 WHERE fechaafiliacion>=01/02/2011";
		//echo $sql;
		return mssql_query($sql,$this->con->conect);
	}
}

function limpiar413(){
	if($this->con->conectar()==true){
 		$sql="TRUNCATE TABLE aportes413";
		return mssql_query($sql,$this->con->conect);
	}
}

}
?>
