<?php
/* autor:       orlando puentes
 * fecha:       30/09/2010
 * objetivo:    
 */
date_default_timezone_set('America/Bogota');
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$usuario=$_SESSION['USUARIO'];
$op=2;
global $archivoplano;
include_once 'clases' . DIRECTORY_SEPARATOR . 'ruaf1.class.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';
$objRuaf = new Ruaf;
//armar plano
if(@chdir('./RUAF/modificaciones/')){ 
   //echo 'esta!'; 
}else{ 
   //echo 'no esta!';
   mkdir('./RUAF/modificaciones/'); 
}  
$fechaHoy=date("m/d/Y");
$f=date("Y");
$fecha=date("m");
$fec=date("d");
chdir ($raiz2."planos_generados/RUAF/modificaciones/");
//$archivo=getcwd () . DIRECTORY_SEPARATOR .'CNCCF32'.$f.$fecha.$fec."";
$archivo='CNCCCF32'.$f.$fecha.$fec;
$handle=fopen($archivo, "w");
fclose($handle);

$acumula=0;
$con=0;


$consulta=$objRuaf->procesar_archivo_2();


if ($consulta==false){
   echo 0;
   exit ();
}


//cabecera
$year=date("Y");
$mes=date("m");
$dia='01';

if (((fmod($year,4)==0) and (fmod($year,100)!=0)) or (fmod($year,400)==0)) {
    $dias_febrero = 29;
} else {
    $dias_febrero = 28;
}
switch($mes) {
    case 01: $dias= '31'; break;
    case 02: $dias= $dias_febrero; break;
    case 03: $dias= '31'; break;
    case 04: $dias= '30'; break;
    case 05: $dias= '31'; break;
    case 06: $dias= '30'; break;
    case 07: $dias= '31'; break;
    case 08: $dias= '31'; break;
    case 09: $dias= '30'; break;
    case 10: $dias= '31'; break;
    case 11: $dias= '30'; break;
    case 12: $dias= '31'; break;
}

$cuantos=$objRuaf->contar_archivos_per();
$z=0;
while($row=mssql_fetch_array($cuantos)){
	if(strlen($row['ciudad'])<=0){
	}
		else{
	$z++;
		}
}
$cadena="1,CCF32,".$year."-".$mes."-".$dia.",".$year."-".$mes."-".$dias.",".intval($z).",".$archivo."\r\n";
$handle=fopen($archivo, "a");
fwrite($handle, $cadena);
fclose($handle);


//detalle


while ($row=mssql_fetch_array($consulta)){
	
	if(strlen($row['ciudad'])<=0){
	}
		else{
	$ciudad=substr($row['ciudad'],-3);
	 
	switch($row['error']) {
		
		//C01
		case '211': $ape1=explode(" ",$row['ape11']);
					$ape2=explode(" ",$row['ape21']);
					$nom1=explode(" ",$row['nom11']);
					$nom2=explode(" ",$row['nom21']);
					
					$cadena="2,CCF32,".$row['id0'].",".$row['numid0'].",".$row['ape10'].",".$row['ape20'].",".$row['nom10'].",".$row['nom20'].",".'C01'.",".$row['sexo'].",".$row['fecnaci'].",".$ape1[0].",".$ape2[0].",".$nom1[0].",".$nom2[0].",".$row['depto'].",".$ciudad.",".",".",".",".",".",".","."\r\n";
					$handle=fopen($archivo, "a");
    				fwrite($handle, $cadena);
				    fclose($handle);
				    $con++;
					break;

		case '212': $ape1=explode(" ",$row['ape11']);
					$ape2=explode(" ",$row['ape21']);
					$nom1=explode(" ",$row['nom11']);
					$nom2=explode(" ",$row['nom21']);
					
					$cadena="2,CCF32,".$row['id0'].",".$row['numid0'].",".$row['ape10'].",".$row['ape20'].",".$row['nom10'].",".$row['nom20'].",".'C01'.",".$row['sexo'].",".$row['fecnaci'].",".$ape1[0].",".$ape2[0].",".$nom1[0].",".$nom2[0].",".$row['depto'].",".$ciudad.",".",".",".",".",".",".","."\r\n";
					$handle=fopen($archivo, "a");
    				fwrite($handle, $cadena);
				    fclose($handle);
				    $con++;
					break;
		
		case '213': $ape1=explode(" ",$row['ape11']);
					$ape2=explode(" ",$row['ape21']);
					$nom1=explode(" ",$row['nom11']);
					$nom2=explode(" ",$row['nom21']);
					
					$cadena="2,CCF32,".$row['id0'].",".$row['numid0'].",".$row['ape10'].",".$row['ape20'].",".$row['nom10'].",".$row['nom20'].",".'C01'.",".$row['sexo'].",".$row['fecnaci'].",".$ape1[0].",".$ape2[0].",".$nom1[0].",".$nom2[0].",".$row['depto'].",".$ciudad.",".",".",".",".",".",".","."\r\n";
					$handle=fopen($archivo, "a");
    				fwrite($handle, $cadena);
				    fclose($handle);
				    $con++;
					break;

		case '214': $ape1=explode(" ",$row['ape11']);
					$ape2=explode(" ",$row['ape21']);
					$nom1=explode(" ",$row['nom11']);
					$nom2=explode(" ",$row['nom21']);
					
					$cadena="2,CCF32,".$row['id0'].",".$row['numid0'].",".$row['ape10'].",".$row['ape20'].",".$row['nom10'].",".$row['nom20'].",".'C01'.",".$row['sexo'].",".$row['fecnaci'].",".$ape1[0].",".$ape2[0].",".$nom1[0].",".$nom2[0].",".$row['depto'].",".$ciudad.",".",".",".",".",".",".","."\r\n";
					$handle=fopen($archivo, "a");
    				fwrite($handle, $cadena);
				    fclose($handle);
				    $con++;
					break;

		case '221': $ape1=explode(" ",$row['ape11']);
					$ape2=explode(" ",$row['ape21']);
					$nom1=explode(" ",$row['nom11']);
					$nom2=explode(" ",$row['nom21']);
					
					$cadena="2,CCF32,".$row['id0'].",".$row['numid0'].",".$row['ape10'].",".$row['ape20'].",".$row['nom10'].",".$row['nom20'].",".'C01'.",".$row['sexo'].",".$row['fecnaci'].",".$ape1[0].",".$ape2[0].",".$nom1[0].",".$nom2[0].",".$row['depto'].",".$ciudad.",".",".",".",".",".",".","."\r\n";
					$handle=fopen($archivo, "a");
    				fwrite($handle, $cadena);
				    fclose($handle);
				    $con++;
					break;

		case '222': $ape1=explode(" ",$row['ape11']);
					$ape2=explode(" ",$row['ape21']);
					$nom1=explode(" ",$row['nom11']);
					$nom2=explode(" ",$row['nom21']);
					
					$cadena="2,CCF32,".$row['id0'].",".$row['numid0'].",".$row['ape10'].",".$row['ape20'].",".$row['nom10'].",".$row['nom20'].",".'C01'.",".$row['sexo'].",".$row['fecnaci'].",".$ape1[0].",".$ape2[0].",".$nom1[0].",".$nom2[0].",".$row['depto'].",".$ciudad.",".",".",".",".",".",".","."\r\n";
					$handle=fopen($archivo, "a");
    				fwrite($handle, $cadena);
				    fclose($handle);
				    $con++;
					break;

		case '223': $ape1=explode(" ",$row['ape11']);
					$ape2=explode(" ",$row['ape21']);
					$nom1=explode(" ",$row['nom11']);
					$nom2=explode(" ",$row['nom21']);
					
					$cadena="2,CCF32,".$row['id0'].",".$row['numid0'].",".$row['ape10'].",".$row['ape20'].",".$row['nom10'].",".$row['nom20'].",".'C01'.",".$row['sexo'].",".$row['fecnaci'].",".$ape1[0].",".$ape2[0].",".$nom1[0].",".$nom2[0].",".$row['depto'].",".$ciudad.",".",".",".",".",".",".","."\r\n";
					$handle=fopen($archivo, "a");
    				fwrite($handle, $cadena);
				    fclose($handle);
				    $con++;
					break;

		case '224': $ape1=explode(" ",$row['ape11']);
					$ape2=explode(" ",$row['ape21']);
					$nom1=explode(" ",$row['nom11']);
					$nom2=explode(" ",$row['nom21']);
					
					$cadena="2,CCF32,".$row['id0'].",".$row['numid0'].",".$row['ape10'].",".$row['ape20'].",".$row['nom10'].",".$row['nom20'].",".'C01'.",".$row['sexo'].",".$row['fecnaci'].",".$ape1[0].",".$ape2[0].",".$nom1[0].",".$nom2[0].",".$row['depto'].",".$ciudad.",".",".",".",".",".",".","."\r\n";
					$handle=fopen($archivo, "a");
    				fwrite($handle, $cadena);
				    fclose($handle);
				    $con++;
					break;

		case '225': $ape1=explode(" ",$row['ape11']);
					$ape2=explode(" ",$row['ape21']);
					$nom1=explode(" ",$row['nom11']);
					$nom2=explode(" ",$row['nom21']);
					
					$cadena="2,CCF32,".$row['id0'].",".$row['numid0'].",".$row['ape10'].",".$row['ape20'].",".$row['nom10'].",".$row['nom20'].",".'C01'.",".$row['sexo'].",".$row['fecnaci'].",".$ape1[0].",".$ape2[0].",".$nom1[0].",".$nom2[0].",".$row['depto'].",".$ciudad.",".",".",".",".",".",".","."\r\n";
					$handle=fopen($archivo, "a");
    				fwrite($handle, $cadena);
				    fclose($handle);
				    $con++;
					break;

		case '226': $ape1=explode(" ",$row['ape11']);
					$ape2=explode(" ",$row['ape21']);
					$nom1=explode(" ",$row['nom11']);
					$nom2=explode(" ",$row['nom21']);
					
					$cadena="2,CCF32,".$row['id0'].",".$row['numid0'].",".$row['ape10'].",".$row['ape20'].",".$row['nom10'].",".$row['nom20'].",".'C01'.",".$row['sexo'].",".$row['fecnaci'].",".$ape1[0].",".$ape2[0].",".$nom1[0].",".$nom2[0].",".$row['depto'].",".$ciudad.",".",".",".",".",".",".","."\r\n";
					$handle=fopen($archivo, "a");
    				fwrite($handle, $cadena);
				    fclose($handle);
				    $con++;
					break;
		
	}
			
    //
	
	}
}

mssql_free_result($consulta);
//$objRsc=new Tarjeta;
//$transaccion=$objRsc->iniciar_transaccion();
$campos = array();

$campos[0] = $archivo;	//archivo
$campos[1] = 21;		//tipoarchivo
$campos[2]="";			//fechasistema
$campos[3]=$_SESSION['USUARIO'];	//usuario
$campos[4]=$con;		//registros
$campos[5]=0;			//totalregistros
$campos[6]=0;			//registros1
$campos[7]=0;			//totalregistros1
$campos[8]=0;			//registros2
$campos[9]=0;			//totalregistros2
$campos[10]=0;			//registros3
$campos[11]=0;			//totalregistros3
$campos[12]=0;			//registros4
$campos[13]=0;			//registros5
$campos[14]=$fechaHoy;		//fechamovimiento

$objeRuaf = new Ruaf;

$control=$objeRuaf->insert_control($campos);


$enlace=$raiz2."planos_generados/RUAF/modificaciones/".$archivo;
$_SESSION['ENLACE']=$enlace;
$_SESSION['ARCHIVO']=$archivo;
echo 3;
?>

