<?php
/* autor:       orlando puentes
 * fecha:       05/10/2010
 * objetivo:    
 */
 
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'funcionesComunes' . DIRECTORY_SEPARATOR . 'funcionesComunes.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'p.definiciones.class.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'ciudades.class.php';
include_once 'clases' . DIRECTORY_SEPARATOR . 'subsieducacion.class.php';
$objClase=new Definiciones();
$objCiudad=new Ciudades();
$objTipoId=new SubsiEducacion();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Documento sin título</title>
<link href="../../css/Estilos.css" rel="stylesheet" type="text/css" />
<link type="text/css" href="../../css/formularios/base/ui.all.css" rel="stylesheet" />
<link href="../../css/marco.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../../js/jquery-1.4.2.js"></script>
<script type="text/javascript" src="../../js/formularios/ui/ui.core.js"></script>
<script type="text/javascript" src="../../js/formularios/ui/ui.resizable.js"></script>
<script type="text/javascript" src="../../js/formularios/ui/ui.dialog.js"></script>
<script type="text/javascript" src="../../js/formularios/ui/ui.datepicker.js"></script>
<script type="text/javascript" src="../../js/comunes.js"></script>
<script language="javascript" src="js/subsiEducacion.js"></script>

<script type="text/javascript">
shortcut.add("Shift+F",function() {
		                                var URL=src();
		var url=URL+"aportes/trabajadores/consultaTrabajador.php";
    	window.open(url,"_blank");
    },{
	'propagate' : true,
	'target' : document 
        });        
</script>

<script type="text/javascript">
	$(function() {
		$('#FechaAsignacion').datepicker({
			changeMonth: true,
			changeYear: true
		});
		
		$('#FechaEntrega').datepicker({
			changeMonth: true,
			changeYear: true
		});
	});
</script>

<script language="javascript">
function Ciudad(){
				var elegido=$('#Departamento').val();
				$.post("../../aportes/phpComunes/ciudades.php", { elegido: elegido }, function(data){
				$("#Municipio").html(data);								
        });
}
</script>

<script language="javascript">
function beneficiario(){
				var elegido=$('#QuienRecibe').val();
				
				$("#idbeneficiario").val(elegido);							
      
}
</script>

<script language="javascript">
function Quien(){
	var idp=$('#Obtiene').val();
	if(idp==1){
			var num=$('#NumIdAfiliado').val();
			var campo1=parseInt(num);
			var nom="";
			if(isNaN(campo1))return false;
			var idtd=$("#TipoDocAfi").val();
			$.getJSON(URL+'phpComunes/buscarPersonaAfiliadaActiva.php',{v0:num,v1:idtd},function(data){
			if(data==0){
			alert("El trabajador no existe o esta inactivo, verifique la identificación!");
			return false;
		}
		$("#QuienRecibe option").remove();
		$('#QuienRecibe').append("<option value=0>Seleccione..</option>");
		$.each(data,function(i,fila){
			var nom = fila.pnombre+" "+fila.snombre+" "+fila.papellido+" "+fila.sapellido;
			$('#QuienRecibe').append("<option value="+fila.idpersona+" >"+nom+"</option>");
			return;
		
		});
    });
	}
	else{
		var num=$('#NumIdAfiliado').val();
		$.getJSON(URL+'phpComunes/buscarBeneficiario.php',{v0:num},function(data){
			if(data==0){
			//alert("El trabajador no existe o esta inactivo, verifique la identificación!");
			return false;
		}
			$("#QuienRecibe option").remove();
			$('#QuienRecibe').append("<option value=0>Seleccione..</option>");
			$.each(data,function(i,fila){
			$("#idbeneficiario").val(fila.idbeneficiario);	
			var nom = fila.pnombre+" "+fila.snombre+" "+fila.papellido+" "+fila.sapellido;
			$('#QuienRecibe').append("<option value="+fila.idbeneficiario+" >"+nom+"</option>");
			return;
		
			});
			});
	}
}
</script>
</head>
<body>
<br />
<center>
<table width="90%" border="0" cellspacing="0" cellpadding="0">
	<tr> 
		<td width="13" height="29" class="arriba_iz">&nbsp;</td>
		<td class="arriba_ce"><span class="letrablanca">::Grabar Subsidio Educaci&oacute;n::::</span></td>
		<td width="13" class="arriba_de" align="right">&nbsp;</td>
	</tr>
	<tr> 
		<td class="cuerpo_iz">&nbsp;</td>
	<td class="cuerpo_ce">
        <img src="../../imagenes/menu/nuevo.png" alt="Nuevo" width="16" height="16" style="cursor:pointer" title="Nuevo" onclick="limpiarCampos();" /> <img src="../../imagenes/menu/grabar.png" alt="Guardar" height="16" style="cursor:pointer" title="Guardar" onclick="GuardarInfoEdu(2);" width:16="width:16" /> <img src="../../imagenes/spacer.gif" alt="" width="1" height="1"/><img src="../../imagenes/spacer.gif" alt="" width="1" height="1"/>
			<img height="1" width="1" src="../../imagenes/spacer.gif"> 
			<img src="../../imagenes/menu/informacion.png" width="16" style="border:none; cursor:pointer" title="Manual" onClick="mostrarAyuda();" /> 
			<img src="../../imagenes/menu/notas.png" width="16" height="16" style="cursor: pointer" title="Colaborac&oacute;on en l&oacute;nea" onClick="notas();" /> 
		</td>
		<td class="cuerpo_de">&nbsp;</td>
	</tr>
	<tr> 
		<td class="cuerpo_iz">&nbsp;</td>
		<td> 
			<table  width="100%"border="0"> 
				<tr> 
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
<tr>
				  <td>&nbsp;</td>
				  <td align="center" class="normal">
                  <table width="100%" border="0" class="tablero">
				    <tr>
				      <td colspan="4" align="center" valign="middle">
					<div id="mensaje" align="center"> </div></td>
			        </tr>
				    <tr>
				      <td width="25%">Tipo Id. Afiliado:</td>
				      <td width="25%"><select name="TipoDocAfi" class="box" id="TipoDocAfi" style="border: !important">
				        <option selected="selected" >Seleccione..</option>
				        <?php
						$consultaid=$objTipoId->consultar091();
						while($row=mssql_fetch_array($consultaid)){
						?>          
						<option value="<?php
						echo $row["iddetalledef"];
						?>"><?php
						echo $row["detalledefinicion"]?></option>
						<?php
						}
						?>  
			          </select></td>
				      <td width="25%">No. Id. Afiliado:</td>
				      <td width="25%"><input type="text" name="NumIdAfiliado" id="NumIdAfiliado" onblur="buscarTrabajador(this.value);"/></td>
			        </tr>
				    <tr>
				      <td>Afiliado:</td>
				      <td colspan="3"><input type="text" name="Afiliado" id="Afiliado" readonly="readonly" size="90%"/></td>
			        </tr>
				    <tr>
				      <td>Obtiene el subsidio: </td>
				      <td colspan="3"><select name="Obtiene" class="box" id="Obtiene" style="border: !important" onchange="javascript:Quien(this.options[this.selectedIndex].value);">
				        <option selected="selected" >Seleccione..</option>
				        <option value="1">Afiliado</option>
				        <option value="2">Beneficiario</option>
			          </select></td>
			        </tr>
				    <tr>
				      <td>Quien recibe el subsidio:</td>
				      <td colspan="3"><select name="QuienRecibe" class="box" id="QuienRecibe" style="border: !important" onchange="javascript:beneficiario(this.options[this.selectedIndex].value);">
				        <option selected="selected" >Seleccione..</option>
				        
			          </select>
			          <input name="idbeneficiario" type="text" disabled="disabled" font color='white' style="background-color:transparent;border:0px solid white;visibility:hidden;" id="idbeneficiario" size="1%"/></td>
			        </tr>
				    <tr>
				      <td>Valor Subsidio:</td>
				      <td><input type="text" name="ValorSubsidio" id="ValorSubsidio" /></td>
				      <td>Fecha Asignaci&oacute;n:</td>
				      <td><input name="FechaAsignacion" type="text" class="box1" id="FechaAsignacion" readonly="readonly" value="" style="border: !important"/></td>
			        </tr>
				    <tr>
				      <td>Departamento Recibe:</td>
				      <td><select name="Departamento" class="box" id="Departamento" style="border: !important" onchange="javascript:Ciudad(this.options[this.selectedIndex].value);"  >
				        <option selected="selected" >Seleccione..</option>
				        <?php
						$consulta=$objCiudad->departamentos();
						while($row=mssql_fetch_array($consulta)){
						?>          
						<option value="<?php
						echo $row["coddepartamento"];
						?>"><?php
						echo $row["departmento"]?></option>
						<?php
						}
						?>  
			          </select></td>
				      <td>Municipio Recibe:</td>
				      <td><select name="Municipio" class="box" id="Municipio" style="border: !important">
				        <option selected="selected" >Seleccione..</option>
				        
			          </select></td>
			        </tr>
				    <tr>
				      <td>Fecha Entrega:</td>
				      <td><input name="FechaEntrega" type="text" class="box1" id="FechaEntrega" readonly="readonly" value="" style="border: !important"/></td>
				      <td>&nbsp;</td>
				      <td>&nbsp;</td>
			        </tr>
</table></td>
				  <td>&nbsp;</td>
			  </tr>
				<tr> 
					<td>&nbsp;</td>
				</tr>
				<tr>
				  <td height="16">&nbsp;</td>
			  </tr>
			</table>
</td>
		<td class="cuerpo_de">&nbsp;</td>
	</tr>
	<tr>
    <td class="abajo_iz" >&nbsp;</td>
    <td class="abajo_ce" ></td>
    <td class="abajo_de" >&nbsp;</td>
  </tr>
</table>

 
<div id="dialog-form2" title="Colaboraci&oacute;n en l&iacute;nea">
	<div title="::Detalles Trabajador::" id="detalles"> 
		<p align="left" id="info"></p>  <p>&nbsp;</p>
		<p>Por favor diligencie este formulario para enviar comentarios, errores o falencias 
		encontradas en el proceso. M&aacute;ximo 250 caracteres </p>
		<label>Tus comentarios:</label>
		<br />
		<textarea name="notas" id="notas" cols="60", rows="10"></textarea>
     </div> 
</div>

<!-- Manual Ayuda -->
	<div id="ayuda" title="Manual Cargar Valores Subsidio" style="background-image:url(../../imagenes/FondoGeneral0.png)"> </div>
	  
</body>

</html>