/*
* @autor:      Ing. Orlando Puentes
* @fecha:      24/09/2010
* objetivo:
*/
/* var URL=src();*/

function mostrarAyuda(){
	$("#ayuda").dialog('open' );
}

function notas(){
	$("#dialog-form2").dialog('open');
}	

$(document).ready(function(){
	$("#ayuda").dialog({
		autoOpen: false,
		height: 450,
		width: 700,
		draggable:true,
		modal:false,
		open: function(evt, ui){
				$('#ayuda').html('');
				$.get('../../help/tarjetas/Ayudacarguevaloresdelsubsidio.html',function(data){
						$('#ayuda').html(data);
				})
		 }
	});
	
	$("#dialog-form2").dialog({
		autoOpen: false,
		height: 400,
		width: 500,
		modal: true,
		buttons: {
			'Enviar': function() {
			var bValid = true;
			var campo=$('#notas').val();
			var campo0=$.trim(campo);
			if (campo0==""){
				$(this).dialog('close');
					return false;
				}
			var campo1=$('#usuario').val();
			var campo2="cargarValoresSubsidio.php";
			$.post(URL+'phpComunes/colaboracion.php',{v0:campo0,v1:campo1,v2:campo2},
			function(datos){
				if(datos=='1'){
					alert("Su comentario fue enviado correctamente!. Gracias por participar en el mejoramiento de SIGAS");
				}else{
					alert(datos);
				}
		});
		$(this).dialog('close');
		},
		Cancelar: function() {
			$(this).dialog('close');
			$("#dialog-form2").dialog("destroy");
		}
	}
	});
	
});

function procesar(){
$("#ajax").html('<img src=../../imagenes/ajax-loader.gif />');
$("#boton").html(' ');
$.ajax({
	url: 'procesarPlano.php',
	type: "POST",
	async:false,
	success: function(datos){
	$("#ajax").html('&nbsp;');
    $("#ajax").html(datos);	
	}
});	
}

function limpiarCampos(){
	$("table.tablero input:text").val('');
	$("table.tablero select").val(0);
	$("table.tablero td input:text.ui-state-error").removeClass("ui-state-error");
	$("#Afiliado").val('');
	$("#QuienRecibe option").remove();
	$('#QuienRecibe').append("<option value=0>Seleccione..</option>");
	$("#Municipio option").remove();
	$('#Municipio').append("<option value=0>Seleccione..</option>");
	//$("#tipoI").focus();
	//$("#tipoI,tipoIT option:first-child").attr("selected","true");
}



/*function procesarPlano(op){
$("#ajax").html('<img src=../../imagenes/ajax-loader.gif />');
$("#boton").html(' ');
$.ajax({
	url: 'procesarPlano_1.php',
	type: "POST",
	async:false,
	success: function(datos){
	//$("#ajax").html('&nbsp;');
    //$("#ajax").html(datos);	
	$("#boton").html(".");
            $("#descargar").show("drop").find("a").attr("href",datos);
            $("#mensaje").html("<small>Dé click derecho y a continuación eliga la opcion <b>Guardar enlace como..</b></small>");
	}
});	

}*/
function GuardarInfoEdu(op){
 var archivo='';
 var v0='';
 switch (op) {
        case 1: archivo='procesarPlano_1.php'; v0=1; break;
        case 2: archivo='guardarcargarIndividual.php'; v0=2; break;
 }
var Obtiene=$("#Obtiene").val();	
var TipoDocAfi=$("#TipoDocAfi").val();	
var NumIdAfiliado=$("#NumIdAfiliado").val();	
var idben=$("#idbeneficiario").val();	
var ValorSubsidio=$("#ValorSubsidio").val();	
var FechaAsignacion=$("#FechaAsignacion").val();	
var Departamento=$("#Departamento").val();	
var Municipio=$("#Municipio").val();	
var FechaEntrega=$("#FechaEntrega").val();	
  
$("#boton").html("Procesando el archivo... espere por favor<br><img src=../../imagenes/ajax-loader.gif />" );
$.get(URL+'RUAF/subsieducacion/guardarcargarIndividual.php',{Obtiene:Obtiene,TipoDocAfi:TipoDocAfi,NumIdAfiliado:NumIdAfiliado,idben:idben,ValorSubsidio:ValorSubsidio,FechaAsignacion:FechaAsignacion,Departamento:Departamento,Municipio:Municipio,FechaEntrega:FechaEntrega}, function(datos){
           if (datos==0){
               $("#boton").html("No se pudo guardar la información!");
               return false;
           }
           if(datos==1){
               $("#boton").html("No se pudo insertar control archivo movimientos");
               return false;
           }
           if(datos==2){
               $("#boton").html("No hay movimientos para cargar!");
               return false;
           }
           $("#boton").html("");
           //$("#mensaje").html("<small>Información guardada satisfactoriamente.</b></small>");
           alert("Información guardada satisfactoriamente.");  
		   limpiarCampos();
       });
}

function buscarMunicipio(obj){
	var idf=parseInt(obj.value);
	var idp=$('#Departamento').val();
	
		$.getJSON(URL+'phpComunes/buscarHijos.php',{v0:idp},function(data){
			$('#Municipio option').remove();
			$.each(data,function(i, fila){
			var nom = fila.pnombre+" "+fila.snombre+" "+fila.papellido+" "+fila.sapellido;
			$('#Municipio').append("<option value="+fila.idbeneficiario+">"+nom+"</option>");
			});
			});
	
	
}


function buscarTrabajador(num){
	if($("#TipoDocAfi").val()==0){
		alert("Falta tipo documento");
		$("#TipoDocAfi").focus();
		return false;
	}
	var campo1=parseInt(num);
	var nom="";
	if(isNaN(campo1))return false;
		var idtd=$("#TipoDocAfi").val();
	$.getJSON(URL+'phpComunes/buscarPersonaAfiliadaActiva.php',{v0:num,v1:idtd},function(data){
		if(data==0){
			alert("El trabajador no existe o esta inactivo, verifique la identificación!");
			return false;
		}
		$.each(data,function(i,fila){
			
			$("#Afiliado").val(fila.pnombre.replace(/^(\s|\&nbsp;)*|(\s|\&nbsp;)*$/g,"")+" "+fila.snombre.replace(/^(\s|\&nbsp;)*|(\s|\&nbsp;)*$/g,"")+" "+fila.papellido.replace(/^(\s|\&nbsp;)*|(\s|\&nbsp;)*$/g,"")+" "+fila.sapellido.replace(/^(\s|\&nbsp;)*|(\s|\&nbsp;)*$/g,""));
			
			return;
		
		});
    });
}


function buscarPersona(objeto){
	if($("#TipoDocAfi").val()==0){
		alert("Falta tipo documento");
		$("#TipoDocAfi").focus();
		return false;
	}
	var numero=parseInt(objeto);
	if(numero.length==0){
		return;}
	var idtd=$("#TipoDocAfi").val();
	$.getJSON(URL+'phpComunes/buscarPersona2.php',{v1:numero,v0:idtd},function(datos){
		if(datos==0){
				alert("No existe en nuestra base!");
				return false;
			}
			$.each(datos,function(i,fila){
				$("#Nombre1Afi").val(fila.pnombre);
				$("#Nombre2Afi").val(fila.snombre);
				$("#Apellido1Afi").val(fila.papellido);
				$("#Apellido2Afi").val(fila.sapellido);
				
				
				return;
				});
	});
}
