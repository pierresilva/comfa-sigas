<?php

/** 
 * @author Jose Luis Rojas
 * 
 * 
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
 
 class ManejoArchivosServidor{
	//TODO - Insert your code here
	
	/**
	 * @var $dS string Separador de directorios Unix (/) Windows(\)
	 */
	private static $dS = DIRECTORY_SEPARATOR;
	
	/**
	 *  
	 * 
	 * @param string $ruta Ruta donde se va a crear la carpeta
	 * @param string $nombre Nombre de la carpeta
	 * @param boolean $crearAut Bandera de creacion automatica de ruta, valor por defecto false 
	 */
	static function crearCarpeta($ruta, $nombre, $crearAut = false){
		if(is_dir($ruta)){
			
		}else if($crearAut == true){
			
		}
	}
	
	
	private static function verficaNombreRuta(){
	//TODO - Verificar la estructura de la ruta, debe cumplir con los estandares del S.O	
	}
	
	/**
	 * Crea un archivo plano e inserta un registro que contiene las columnas
	 *
	 * @param string $filePath ruta f�sica hacia el archivo
	 * @param array $arregloColumnas arreglo con los nombres de las columnas
	 * @param string $separador default: ;
	 */
	static function prepararArchivoPlano($filePath, $arregloColumnas = array(),$separador = ";"){
		$archivo = fopen($filePath,"w");
		$strColumnas = implode($separador,$arregloColumnas);
		fwrite($archivo,$strColumnas."\r\n");
		fclose($archivo);		
	}
	
	/**
	 * Graba una l�nea al final de un archivo plano,
	 * agregando un salto de l�nea y retorno de carro
	 *
	 * @param string $filePath ruta hacia el archivo
	 * @param array $arregloDatos arreglo de datos a grabar
	 * @param string $separador caracter utilizado como separadaro, por defecto ;
	 */
	static function grabarRegistroEnArchivoGenerico($filePath, $arregloDatos = array(), $separador = ";"){
		$archivo = fopen($filePath,"a+");
		$arregloDatos = array_map('trim',$arregloDatos);
		$cadenaGrabar = implode($separador,$arregloDatos) ."\r\n";
		fwrite($archivo,$cadenaGrabar);
		fclose($archivo);
	}
	
	/**
	 * Descarga un archivo
	 *
	 * @param string $nombreArchivo nombre del archivo
	 * @param string $enlace ruta completa hacia el archivo
	 */
	static function descargarArchivo($nombreArchivo,$enlace){
		//$enlace += $nombreArchivo;
		header("Pragma: private");
		header("Expires: 0"); //
		header ("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header ("Content-Type: application/octet-stream");
		header ("Content-Disposition: attachment; filename=". $nombreArchivo ." "); 
		header ("Content-Length: ".filesize($enlace));
		readfile($enlace);
	}
	
}

?>