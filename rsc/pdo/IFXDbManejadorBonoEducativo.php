<?php

include_once 'IFXDbConfiguracionBonoEducativo.php';
include_once 'configDbBonoEducativo.php';
/**
 * Description of IFXDBManejador
 *
 */
class IFXDbManejadorBonoEducativo {
//put your code here

    public $conexionID;
    public $error;
    private $logger;

    static $_instancia;

    /**
     * Constructor privado de la clase, crea la conexion a informix
     */
    public function __construct() {
        try {
			$this->conexionID = new PDO( "sqlsrv:Server=10.10.1.55 ; Database = PosHiper ", "poshiper", "Merhiper-2011", array(PDO::SQLSRV_ATTR_DIRECT_QUERY => true)); 
            //$this->conexionID->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
            //$this->conexionID->setAttribute(PDO::ATTR_CASE, PDO::CASE_LOWER);
        }catch(PDOException $e) {
            $this->error=$e->getMessage();
            self::$_instancia = null;
        }
    }

    /**
     * Previene la clonacion de la conexion
     */
    private function __clone() {

    }

    /**
     *Conecta con la base de datos, si ya se tiene conexion a la base de datos
     * devuelve la conexion que ya se encuentra establecida mediante el manejador
     *
     * @return IFXDbManager
     */
    public static function conectarDB() {
        if( ! (self::$_instancia instanceof self) ) {
            self::$_instancia = new self();
        }
    	
        return self::$_instancia;
    }


    public function setNivelError($nivel = 0) {
        switch ($nivel) {
            case 1:
                $this->conexionID->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
                break;
            case 2:
                $this->conexionID->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                break;
            default:
                $this->conexionID->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
                break;
        }
    }

    private function getNivelAsociacion($nivelAsoc = 0) {
        switch ($nivelAsoc) {
            case 1:
                $asoc = PDO::FETCH_NUM;
                break;
            case 2:
                $asoc = PDO::FETCH_BOTH;
                break;
            default;
                $asoc = PDO::FETCH_ASSOC;
                break;
        }
        return $asoc;
    }


    /**
     * Devuelve el recurso para manejar de manera manual las
     * funciones PDO de PHP
     *
     * @return <resource>
     */
    public function controlManualDb() {
        return $this->conexionID;
    }



    /**
     * Ejecuta sentencias sencillas SQL SELECT
     *
     * @param String $sql
     * @param Integer $tipoAsoc
     * @return PDOStatement
     */
    public function querySimple($sql, $tipoAsoc = 0) {
        $asoc = $this->getNivelAsociacion($tipoAsoc);
        try {
            $row = $this->conexionID->query($sql, $asoc);
        }catch(PDOException $e) {
			$this->error=$e->getMessage();
        }
        return $row;
    }


    public function queryComplejo() {
	
    }

    /**
     * Ejecuta sentencias SQL INSERT, UPDATE, DELETE
     * y devuelve el numero de filas afectadas
     *
     * ej. Al insertar un registro devuelve el numero 1, indicando
     * que una fila fue insertada
     *
     * @param <String> $sql
     * @return <Int>
     */

public function queryActualiza($sql){
	$numRows = $this->conexionID->exec($sql);
	if(intval($numRows)>=0){
		return $numRows;
	}
	else 
	{
		$e=$this->conexionID->errorInfo();
		$this->error=$e;
	}
}
	
public function queryInsert($sql,$tabla) {
	$numRows = $this->conexionID->exec($sql);
	if($numRows==0){
		$e=$this->conexionID->errorInfo();
		$this->error=$e;
	}
	else{
		return $this->conexionID->lastInsertId($tabla);
	} 
		
}	

public function inicioTransaccion(){
	$this->conexionID->beginTransaction();
} 	

public function cancelarTransaccion(){
	$this->conexionID->rollBack();
}

public function confirmarTransaccion(){
	$this->conexionID->commit();
}
}
?>
