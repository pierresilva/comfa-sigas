<?php
/*if ( !isset( $_SESSION ['USUARIO'] ) ) {session_start();}
if(empty($_SESSION['LOGUIADO'])){
	echo "no hay session!!";
	exit();
}
$raiz=$_SESSION['RAIZ'];
include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'session.php';
session();
include_once $raiz . DIRECTORY_SEPARATOR . 'IFXDbConfigInforma.php';*/

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
include 'IFXDbConfigInforma.php';

/**
 * Description of IFXDBManejador
 *
 */
class IFXDbManejaInforma {
//put your code here

    public $conexionID;
    public $error;
    private $logger;

    static $_instancia;

    /**
     * Constructor privado de la clase, crea la conexion a informix
     */
    private function __construct() {
        try {
            $this->conexionID = new PDO(IFXDbConfigInforma::_ifxDSN(), IFXDbConfigInforma::_ifxUsuario(), IFXDbConfigInforma::_ifxContrasena());
            $this->conexionID->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
        }catch(PDOException $e) {
            $this->error=$e->getMessage();
            self::$_instancia = null;
        }
    }

    /**
     * Previene la clonacion de la conexion
     */
    private function __clone() {

    }

    /**
     *Conecta con la base de datos, si ya se tiene conexion a la base de datos
     * devuelve la conexion que ya se encuentra establecida mediante el manejador
     *
     * @return IFXDbManager
     */
    public static function conectarDB() {
        if( ! (self::$_instancia instanceof self) ) {
            self::$_instancia = new self();
        }
    	
        return self::$_instancia;
    }


    public function setNivelError($nivel = 0) {
        switch ($nivel) {
            case 1:
                $this->conexionID->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
                break;
            case 2:
                $this->conexionID->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                break;
            default:
                $this->conexionID->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
                break;
        }
    }

    private function getNivelAsociacion($nivelAsoc = 0) {
        switch ($nivelAsoc) {
            case 1:
                $asoc = PDO::FETCH_NUM;
                break;
            case 2:
                $asoc = PDO::FETCH_BOTH;
                break;
            default;
                $asoc = PDO::FETCH_ASSOC;
                break;
        }
        return $asoc;
    }


    /**
     * Devuelve el recurso para manejar de manera manual las
     * funciones PDO de PHP
     *
     * @return <resource>
     */
    public function controlManualDb() {
        return $this->conexionID;
    }



    /**
     * Ejecuta sentencias sencillas SQL SELECT
     *
     * @param String $sql
     * @param Integer $tipoAsoc
     * @return PDOStatement
     */
    public function querySimple($sql, $tipoAsoc = 0) {
        $asoc = $this->getNivelAsociacion($tipoAsoc);
        try {
            $row = $this->conexionID->query($sql, $asoc);
        }catch(PDOException $e) {
			$this->error=$e->getMessage();
        }
        return $row;
    }


    public function queryComplejo() {
	
    }

    /**
     * Ejecuta sentencias SQL INSERT, UPDATE, DELETE
     * y devuelve el numero de filas afectadas
     *
     * ej. Al insertar un registro devuelve el numero 1, indicando
     * que una fila fue insertada
     *
     * @param <String> $sql
     * @return <Int>
     */
public function queryActualiza($sql) {
	$numRows = $this->conexionID->exec($sql);
	if($numRows==0){
		$e=$this->conexionID->errorInfo();
		$this->error=$e;
	}
	else{
		return $numRows;
	} 
		
	}

}
/*
if(strstr($e->getMessage(), 'SQLSTATE[')) {
preg_match('/SQLSTATE\[(\w+)\] \[(\w+)\] (.*)/', $e->getMessage(), $matches);
$this->code = ($matches[1] == 'HT000' ? $matches[2] : $matches[1]);
$this->message = $matches[3];
} 
*/ 

?>
