<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of IFXDbConfiguracion
 *
 */
class PDO_DSN {
	
	private static $ifxAportesDevDSN = array("dsn"=>"informix:host=10.10.1.190; service=9088;database=aportes; server=sigas_dev; protocol=onsoctcp; EnableScrollableCursors=1", "user"=>"informix", "password"=>"informix");
	private static $ifxAportesDSN = array("dsn"=>"informix:host=10.10.1.70; service=10048;database=aportes; server=produ_tcp; protocol=onsoctcp; EnableScrollableCursors=1", "user"=>"informix", "password"=>"informix");
	private static $ifxHuilaDSN = array("dsn"=>"informix:host=10.10.1.194; service=9088;database=huila; server=produ_tcp; protocol=onsoctcp; EnableScrollableCursors=1", "user"=>"informix", "password"=>"informix");
	private static $ifxPruebaDSN = array("dsn"=>"informix:host=10.10.1.97; service=9088;database=prueba; server=prueba_tcp; protocol=onsoctcp; EnableScrollableCursors=1", "user"=>"informix", "password"=>"informix");
	
    public static function _getDSN($dsn){
     	if($dsn == 'aportes_dev'){
        	return self::$ifxAportesDevDSN;
    	}elseif($dsn == 'aportes'){
    		return self::$ifxAportesDSN;
    	}elseif($dsn == 'huila'){
    		return self::$ifxHuilaDSN;
    	}elseif($dsn == 'prueba'){
    		return self::$ifxPruebaDSN;
    	}
     } 
}
?>
