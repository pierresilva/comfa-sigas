<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
include_once $raiz.DIRECTORY_SEPARATOR. 'config.php';

include_once $raiz . DIRECTORY_SEPARATOR . 'asopagos' . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'maestro.proceso.class.php';
include_once 'clases' . DIRECTORY_SEPARATOR . 'maestroSaldo.class.php';

//Obtener parametros
$idProceso = $_REQUEST["id_proceso"];
$fechaPago = $_REQUEST["fecha_pago"];
$tipoCarge = $_REQUEST["tipo_carge"];
$arrFiles = isset($_FILES)?$_FILES:null;
$accion = $_REQUEST["accion"];
$usuario = $_SESSION["USUARIO"];

$arrResultado = array(); 

switch ($accion){
	case "PROCESAR":
		
		$objMaestroSaldo = new MaestroSaldo();
		$arrResultado = $objMaestroSaldo->ejecutar($idProceso,$usuario,$fechaPago,false,$ruta_cargados,$tipoCarge,$arrFiles);
		
		break;
	case "REPROCESAR":
		$objMaestroSaldo = new MaestroSaldo();
		$arrResultado = $objMaestroSaldo->ejecutar($idProceso,$usuario,null,true,$ruta_cargados,$tipoCarge,$arrFiles);
		break;
}

echo json_encode($arrResultado);
?>