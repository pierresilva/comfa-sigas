<?php
set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$usuario=$_SESSION['USUARIO'];
$url=$_SERVER['PHP_SELF'];
include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'auditoria.php';
//include_once 'phpComunes/auditoria.php';
auditar($url);

include_once $raiz.DIRECTORY_SEPARATOR.'config.php';
$fecha=date('Ymd');
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

$ruta= $ruta_generados."asopagos".DIRECTORY_SEPARATOR."novedades_no".DIRECTORY_SEPARATOR;
$c1='A';	// Car�cter Fijo 
$c2='01';	// C�digo del bolsillo (d�bito).
$c3='032';	// Codigo de la caja seg�n Supersubsidio (Ej: 032 Huila).
$c4=date('ymd');	// fecha envio.
$cant="SELECT COUNT(*) as cuenta FROM aportes149 WHERE fechasistema=cast(getdate() as date) AND tipoarchivo=2876";
$resultado = $db->querySimple($cant);// consecutivo diario
$temp=$resultado->fetch();
$c5=str_pad($temp['cuenta']+1,3,"0",STR_PAD_LEFT);
$temp=$c5;
$resultado->closeCursor();
$archivo=$c1.$c2.$c3.$c4.$c5.".txt";
$path =$ruta.$archivo;
$handle=fopen($path, "w");
fclose($handle);

$sql = "SELECT iddesde,ab.bono AS anterior,aportes101.bono,aportes101.idtarjeta, aportes015.idtipodocumento, aportes015.idpersona, aportes015.identificacion, papellido, sapellido, pnombre, snombre, nombrecorto, fechanacimiento, sexo, idestadocivil, direccion, iddepresidencia, idciuresidencia,telefono 
FROM aportes101 INNER JOIN aportes110 ON aportes101.idtarjeta = aportes110.idpara INNER JOIN aportes015 ON aportes101.idpersona = aportes015.idpersona 
INNER JOIN aportes101 ab ON ab.idtarjeta=aportes110.iddesde where aportes110.procesado='N'";
$rs = $db->querySimple($sql);
$con=0;
$espacio=" ";
$dir="";
$tel="";
$iddepto="";
$idciud="";
while ($row = $rs->fetch()){
	$con++;
	$row=array_map("utf8_decode", $row);
	$idpersona=$row['idpersona'];
	$idtd=$row['idtipodocumento'];
	$numero=trim($row['identificacion']);
	$bono=trim($row['bono']);
	$pnombre=trim($row['pnombre']);
	$snombre=trim($row['snombre']);
	$sapellido=trim($row['sapellido']);
	$papellido=trim($row['papellido']);
	$nombrerealce=trim($row['nombrecorto']);
	$idt=$row['idtarjeta'];
	$dir=trim($row['direccion']);
	$tarjetaAnterior=trim($row['anterior']);
	
	if(strstr($pnombre,"�"))
	{
		$pnombre=str_replace("�","N",$pnombre);	
	}
	if(strstr($snombre,"�"))
	{
		$snombre=str_replace("�","N",$snombre);	
	}
	if(strstr($papellido,"�"))
	{
		$papellido=str_replace("�","N",$papellido);	
	}
	if(strstr($sapellido,"�"))
	{
		$sapellido=str_replace("�","N",$sapellido);	
	}
	if(strstr($nombrerealce,"�"))
	{
		$nombrerealce=str_replace("�","N",$nombrerealce);	
	}
	if(strstr($nombrerealce,"&"))
	{
		$nombrerealce=str_replace("&","N",$nombrerealce);	
	}
	if(strlen($nombrerealce)>26  ){
		$nombrerealce= substr($nombrerealce, 0,26);
	}
	
	if(strlen($dir)==0){
		$dir="CALLE 11 5 13";
	}
		
	if(strlen(trim($row['telefono']))==0){
		$tel='8759544';
	}
	else{
		$tel=$row['telefono'];
	}
	
	if(strlen(trim($row['iddepresidencia']))==0){
		$iddepto='41';
	}
	else{
		$iddepto=$row['iddepresidencia'];
	}
	
	if(strlen(trim($row['idciuresidencia']))==0){
		$idciud='41001';
	}
	else{
		$idciud=$row['idciuresidencia'];
	}
	
	$nombrecorto=$pnombre." ".$papellido;
	if(strlen($nombrecorto)>20  ){
		$nombrecorto= substr($nombrecorto, 0,20);
	}
	
	if(strlen(trim($row['fechanacimiento']))==0){
		echo 2;
		exit();
	}
	$fechanace=explode("-",$row['fechanacimiento']);
	$fechan=$fechanace[0].$fechanace[1].$fechanace[2];
	
	switch ($idtd){
		case 1 : $td=2; break;
		case 2 : $td=4; break;
		case 4 : $td=3; break;
		default:$td=9;
	}
	switch ($row['idestadocivil']){
		case 50 : $estado = 1; break;
		case 51 : $estado = 5; break;
		case 52 : $estado = 4; break;
		case 53 : $estado = 2; break;
		case 54 : $estado = 3; break;
		default:$estado = 9;
	}
	switch($row['sexo']){
		case 'M' : $s=1; break;
		case 'F' : $s=2; break;
		default:$s = 1;
	}
$c1='06';//tipo novedad
$c2=str_pad("0",14,"0");//num producto ref
$c3=str_pad($bono,19,$espacio,STR_PAD_RIGHT);
$c4="N";//tipo persona
$c5=$td;//tipo identifiacion
$c6=str_pad($numero,15,"0",STR_PAD_LEFT);//num identificacion
$c7=str_pad($papellido,15);//papellido
$c8=str_pad($sapellido,15);//sapellido
$c9=str_pad($pnombre,15);//pnombre
$c10=str_pad($snombre,15);//snombre
$c11=str_pad($nombrecorto,20);
$c12=str_pad($nombrerealce,26);
$c13=$fechan;//fecha nacimiento
$c14=$s;//sexo
$c15=$estado;//estadocivil
$c16=str_pad($dir,40); // str_pad($row['direccion'],40);
$c17=str_pad($dir,40); //str_pad($row['direccion'],40);
$c18=str_pad($iddepto,2);
$c19=str_pad($idciud,5);
$c20='999999'; //str_pad("9",6);
$c21=str_pad($dir,40); //str_pad($row['direccion'],40);
$c22=str_pad($dir,40); //str_pad($row['direccion'],40);
$c23=str_pad($iddepto,2);
$c24=str_pad($idciud,5);
$c25='999999'; //pad("9",6);
$c26=str_pad("0",5,"0");//oficina radicacion
$c27=str_pad($tel,14);
$c28=str_pad($tel,14);
$c29=str_pad("0",5,"0");
$c30=str_pad("0",15,'0');
$c31='00000000'; //$fecha;
$c32='000'; //str_pad($espacio,3);
$c33='002';//grupo manejo cutas
$c34='001';//tipo tarjeta
$c35='000000'; //str_pad($espacio,6);
$c36=$espacio;//estado registro recibido
$c37="    ";//cod error
$c38=str_pad('0',10,'0');//numero solicitud
$c39=str_pad($tarjetaAnterior,19);//tarjeta anterior
$c40='000';//cod punto de distribucion
$c41=209;//cod barras convenio mercadeo
$c42=str_pad($espacio,2);//cod bloqueo tarjeta
$c43=str_pad($espacio,2);//motivo del bloqueo
$c44='01';//tipo de cuenta
$c45=str_pad($espacio,36);
	$cadena=$c1.$c2.$c3.$c4.$c5.$c6.$c7.$c8.$c9.$c10.$c11.$c12.$c13.$c14.$c15.$c16.$c17.$c18.$c19.$c20.$c21.$c22.$c23.$c24.$c25.$c26.$c27.$c28.$c29.$c30.$c31.$c32.$c33.$c34.$c35.$c36.$c37.$c38.$c39.$c40.$c41.$c42.$c43.$c44.$c45."\r\n";
	$handle=fopen($path, "a");
	fwrite($handle, $cadena);
	fclose($handle);
	$sql="Update aportes110 set procesado='S', fechaproceso=cast(getdate() as date),usuarioprocesa='$usuario' where idtarjeta=$idt";
	$rsup=$db->queryActualiza($sql);
}
$rs->closeCursor();

$c1="000000900319291";  //nit
$c2=$fecha;
$c3=str_pad($temp,5,"0",STR_PAD_LEFT);	//consecutivo
$c4=str_pad($con, 6, "0",STR_PAD_LEFT);			//num de registros
$c5="00"; //str_pad('0',2);		//cod area
$c6=str_pad($espacio, 427);		//filler
$c7="636480";					//prefijo convenio
$c8="001";						//subtipo
$c9='T';	//str_pad('T',3,$espacio);	//trailer
$c10=str_pad($espacio, 3);

$cadena=$c1.$c2.$c3.$c4.$c5.$c6.$c7.$c8.$c9.$c10;
$handle=fopen($path, "a");
if($handle)
{
	if($con==0){
		echo 3; // no hay registros
		exit();
	}
	else{
	fwrite($handle, $cadena);
	fclose($handle);
	$hora=date("H:i:s");
	$sent="INSERT INTO aportes149(archivo,tipoarchivo,fechasistema,usuario,registros,hora) VALUES('$archivo',2876,cast(getdate() as date),'.$_SESSION[USUARIO].','$con','$hora')";
	$db->queryActualiza($sent);
	echo 1;
	}
}
$enlace=$ruta_generados."asopagos".DIRECTORY_SEPARATOR."novedades_no".DIRECTORY_SEPARATOR.$archivo;
$_SESSION['ENLACE']=$enlace;
$_SESSION['ARCHIVO']=$archivo;
?>