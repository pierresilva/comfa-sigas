<?php
set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");
$cantidad=intval($_REQUEST['v0']);
if($cantidad<=0){
	exit();
}
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz . DIRECTORY_SEPARATOR .'config.php'; 
include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR .'ean13.php';
session();

$url=$_SERVER['PHP_SELF'];
include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'auditoria.php';
//include_once 'phpComunes/auditoria.php';
//auditar($url);

$fecha=date('Ymd');
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

$ruta= $ruta_generados."asopagos".DIRECTORY_SEPARATOR."novedades_no".DIRECTORY_SEPARATOR;
$c1='A';	// Car�cter Fijo 
$c2='01';	// C�digo del bolsillo (d�bito).
$c3='032';	// Codigo de la caja seg�n Supersubsidio (Ej: 032 Huila).
$c4=date('ymd');// fecha envio.
$sql="SELECT COUNT(*) as cuenta FROM aportes149 WHERE fechasistema=cast(getdate() as date) AND tipoarchivo=2876";
$resultado = $db->querySimple($sql);// consecutivo diario
$temp=$resultado->fetch();
$c5=str_pad($temp['cuenta']+1,3,"0",STR_PAD_LEFT);
$temp=$c5;
$resultado->closeCursor();
$archivo=$c1.$c2.$c3.$c4.$c5.".txt";
$path =$ruta.$archivo;
$handle=fopen($path, "w");
fclose($handle);

$sql0="SELECT max(consecutivo) as consecutivo FROM aportes100 ";
$rs0 = $db->querySimple($sql0);
$row0 = $rs0->fetch();
$secuencia=$row0['consecutivo'];

$sql2="SELECT min(idbono) as consec FROM aportes100 WHERE asignada='N' AND plastico='N' AND impresion='N' AND validado='N' AND estado='I' ";
$rs2 = $db->querySimple($sql2);
$row2 = $rs2->fetch();
$consec=$row2['consec'];

$desde=$consec + ($cantidad - 1);

$sql = "SELECT count(*) as cuenta from aportes100 where idbono BETWEEN $consec AND $desde ";
//$sql = "SELECT count(*) as cuenta from aportes100 where idbono BETWEEN 95615 AND 198933 ";

$rs = $db->querySimple($sql);
    $row = $rs->fetch();
	$cuenta=$row['cuenta'];
	
     if($cuenta < $cantidad){
	     echo "No hay Numeros de Bonos suficientes";
	     exit();
     }
     
$sql="Select TOP $cantidad idbono,bono from aportes100 where idbono BETWEEN $consec AND $desde ";
//  $sql="Select TOP $cantidad idbono,bono from aportes100 where idbono BETWEEN 95615 AND 198933 ";
$rs = $db->querySimple($sql);
$con=$secuencia;
$espacio=" ";
$dir="";
$tel="";
$iddepto="";
$idciud="";

while ($row = $rs->fetch()){
	$con++;
	
	if($con == 45000 || $con == 45687 || $con == 47201 || $con == 49218) {$con=$con+1;}
	
	$idbono=$row['idbono'];
	$td="9";
	$numero=$con;
	$bono=$row['bono'];
	$codigo=intval(substr($bono, 5,10));
	$codigo=900000000000+$codigo;
	$dig=ean13($codigo);
	$codigo=$codigo.$dig;
	$pnombre="ASOPAGOS";
	$snombre="ASOPAGOS";
	$sapellido="ASOPAGOS";
	$papellido="ASOPAGOS";
	$nombrerealce="ASOPAGOS";	
	$dir="ASOPAGOS";	
	$tel=$con;	
	$iddepto=41;	
	$idciud=41001;
	$nombrecorto="ASOPAGOS";
	$fechan='20120101';
	$estado = 9;
	$s = 9;
	
	$c1='01';//tipo novedad
	$c2=str_pad("0",14,"0");//num producto ref
	//$c3=str_pad($bono,16,"0",STR_PAD_LEFT);//num tarjeta
	$c3=str_pad($bono,19,$espacio,STR_PAD_RIGHT);
	$c4="N";//tipo persona
	$c5=$td;//tipo identifiacion
	$c6=str_pad($numero,15,"0",STR_PAD_LEFT);//num identificacion
	$c7=str_pad($papellido,15);//papellido
	$c8=str_pad($sapellido,15);//sapellido
	$c9=str_pad($pnombre,15);//pnombre
	$c10=str_pad($snombre,15);//snombre
	$nc=substr($nombrecorto,0,20);//nombre corto
	$c11=str_pad($nc,20);
	$nc1=substr($nombrerealce,0,26);
	$c12=str_pad($nc1,26);
	$c13=$fechan;//fecha nacimiento
	$c14=$s;//sexo
	$c15=$estado;//estadocivil
	$c16=str_pad($dir,40); // str_pad($row['direccion'],40);
	$c17=str_pad($dir,40); //str_pad($row['direccion'],40);
	$c18=str_pad($iddepto,2);
	$c19=str_pad($idciud,5);
	$c20='999999'; //str_pad("9",6);
	$c21=str_pad($dir,40); //str_pad($row['direccion'],40);
	$c22=str_pad($codigo,40,$espacio); //Codigo de barras
	$c23=str_pad($iddepto,2);
	$c24=str_pad($idciud,5);
	$c25='999999'; //pad("9",6);
	$c26=str_pad("0",5,'0');//oficina radicacion
	$c27=str_pad($tel,14);
	$c28=str_pad($tel,14);
	$c29=str_pad("0",5,'0');
	$c30=str_pad("0",15,'0');
	$c31=$fecha; //$fecha;
	$c32='000'; //str_pad($espacio,3);
	$c33='002';//grupo manejo cutas
	$c34='001';//tipo tarjeta
	$c35='000000'; //str_pad($espacio,6);
	$c36=$espacio;//estado registro recibido
	$c37="    ";//cod error
	$c38=str_pad('0',10,'0');//numero solicitud
	$c39=str_pad($espacio,19);//tarjeta anterior
	$c40='000';//cod punto de distribucion
	$c41=209;//cod barras convenio mercadeo
	$c42=str_pad($espacio,2);//cod bloqueo tarjeta
	$c43=str_pad($espacio,2);//motivo del bloqueo
	$c44='01';//tipo de cuenta
	$c45=str_pad($espacio,36);
	$cadena=$c1.$c2.$c3.$c4.$c5.$c6.$c7.$c8.$c9.$c10.$c11.$c12.$c13.$c14.$c15.$c16.$c17.$c18.$c19.$c20.$c21.$c22.$c23.$c24.$c25.$c26.$c27.$c28.$c29.$c30.$c31.$c32.$c33.$c34.$c35.$c36.$c37.$c38.$c39.$c40.$c41.$c42.$c43.$c44.$c45."\r\n";
	$handle=fopen($path, "a");
	fwrite($handle, $cadena);
	fclose($handle);
	
	$sql3="update aportes100 set consecutivo=$con WHERE idbono =$idbono ";
	$rs3=$db->queryActualiza($sql3);
	
	$rs2=$db->queryActualiza($sql);
	if($rs2 != 1){
		//archivo log
	}
}
$rs->closeCursor();

$c1="000000900319291";  //nit
$c2=$fecha;
$c3=str_pad($temp,5,"0",STR_PAD_LEFT);	//consecutivo
$c4=str_pad($con, 6, "0",STR_PAD_LEFT);			//num de registros
$c5="00"; //str_pad('0',2);		//cod area
$c6=str_pad($espacio, 427);		//filler
$c7="636480";					//prefijo convenio
$c8="001";						//subtipo
$c9='T';	//str_pad('T',3,$espacio);	//trailer
$c10=str_pad($espacio, 3);

$cadena=$c1.$c2.$c3.$c4.$c5.$c6.$c7.$c8.$c9.$c10;
$handle=fopen($path, "a");
$con=$con-$secuencia;
if($handle)
{
	if($con==0){
		echo 3; // no hay registros
		exit();
	}
else{
	fwrite($handle, $cadena);
	fclose($handle);
	$hora=date("H:i:s");
	$sent="INSERT INTO aportes149(archivo,tipoarchivo,fechasistema,usuario,registros,hora) VALUES('$archivo',2876,cast(getdate() as date),'$_SESSION[USUARIO]','$con','$hora')";
	$db->queryActualiza($sent);
	
	$sql2="update aportes100 set plastico='S' WHERE idbono BETWEEN $consec AND $desde ";
	$rs2=$db->queryActualiza($sql2);
	
	echo 1;
	}
}
$ruta_generados."asopagos".DIRECTORY_SEPARATOR."novedades_no".DIRECTORY_SEPARATOR;
$enlace=$ruta_generados."asopagos".DIRECTORY_SEPARATOR."novedades_no".DIRECTORY_SEPARATOR.$archivo;
$_SESSION['ENLACE']=$enlace;
$_SESSION['ARCHIVO']=$archivo;
?>