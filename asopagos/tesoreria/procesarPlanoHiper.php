<?php
/* autor:       orlando puentes
 * fecha:       octubre 4 de2010
 * objetivo:    Procesar los movimientos de los Bonos almacenados en la base de datos. 
 */
set_time_limit(0);
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
$raiz=$_SESSION['RAIZ'];
$ip=$_SERVER['SERVER_NAME'];
$url0="http://".$ip."/planos_cargados/";

$usuario= $_SESSION["USUARIO"];
include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';
require_once $raiz . DIRECTORY_SEPARATOR . 'funcionesComunes' . DIRECTORY_SEPARATOR . 'funcionesComunes.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
 $db = IFXDbManejador::conectarDB();
 if($db->conexionID==null){
 	$cadena = $db->error;
 	echo $cadena;
	exit();
 }
 
$directorio = $ruta_cargados.'tesoreria'. DIRECTORY_SEPARATOR.'cruces'.DIRECTORY_SEPARATOR.'TEMPO.txt';
$fp = fopen ($directorio,"r");
$con=0;
while (!feof($fp)) {
	$linea = fgets($fp, 1024);
	if($con==1){
		break;
	}
	$con++;
}
fclose($fp);
$fecha = substr($linea, 0,10);
$a = substr($linea,0,4);
$m = substr($linea,5,2);
$d = substr($linea, 8,2);

$directorio2 = $ruta_cargados.'tesoreria'.DIRECTORY_SEPARATOR.'cruces'.DIRECTORY_SEPARATOR.'procesados'.DIRECTORY_SEPARATOR."$a".DIRECTORY_SEPARATOR."$m".DIRECTORY_SEPARATOR."$d".DIRECTORY_SEPARATOR;
$ok=verificaDirectorio($directorio2);
if($ok==0){
	echo "No se pudo crear el directorio destino<br>";
	exit();
}
$file1='resultadoCruce'.$a.$m.$d.'.csv';
$salida1 = $directorio2.$file1;
$file2='NOExiten'.$a.$m.$d.'.csv';
$salida2 = $directorio2.$file2;
$fp = fopen($salida2, "w");
fclose($fp);


$cadena="NUMERO,NOMBRES,VALOR,FECHA,HORA,DATAFONO,RRN,AUTORIZACION,CODIGO,ESTABLECIMIENTO,CRUCE"."\r\n";
$fp = fopen($salida1, "w");
fwrite($fp,$cadena);
fclose($fp);
	$fp = fopen ($directorio,"r");
	$con=0;
    while (!feof($fp)) {
    	$linea = fgets($fp, 1024);
    	if($con==0){
    		$fp2 = fopen($salida2, "a");
    		fwrite($fp2, $linea);
    		fclose($fp2);
    	}
    	if(!$linea) breack;
    	if($con > 0){
    	$campos = explode(",", $linea);
    	//Fecha,Cajero,Cajero,Caja,Factura,Hora,RRN,Cedula,Cliente,valor,Almacen,Negocio
        $num = $campos[6];
        $valor = $campos[9];
       
        $sql = "UPDATE aportes152 SET cruce ='S' where referencia=$num AND fechatransaccion='$fecha' and valor=$valor";
        $rs = $db->queryActualiza($sql);
        	
        if($rs == 0){ // no exite
        		$fp2 = fopen($salida2, "a");
        		fwrite($fp2, $linea);
        		fclose($fp2);
        	}	
    }
		$con++;
	}    
	
fclose($fp);
if(file_exists($directorio))
	{
		unlink($directorio);
	}
$fp = fopen($salida1,"a");	
$sql="SELECT  identificacion, nombrecorto, valor, fechatransaccion, horatransaccion, numdispositivo, referencia, autorizacion, establecimiento,detalledefinicion,cruce 
FROM dbo.aportes152
LEFT JOIN aportes100 ON aportes152.tarjeta=aportes100.bono LEFT JOIN aportes015 ON aportes100.idpersona=aportes015.idpersona
LEFT JOIN aportes091 ON APORTES152.establecimiento=APORTES091.codigo WHERE fechatransaccion = '$fecha' AND referencia > 0";
$rs=$db->querySimple($sql);
while( $row = $rs->fetch() ){
	$row=array_map("trim", $row);
	$cadena=implode(",",$row);
	$cadena=$cadena."\r\n";
	fwrite($fp,$cadena);
	}
fclose($fp);

$url1 = $url0.'tesoreria/cruces/procesados/'.$a.'/'.$m.'/'.$d.'/'.$file2;
$url2 = $url0.'tesoreria/cruces/procesados/'.$a.'/'.$m.'/'.$d.'/'.$file1;
echo"<br> Proceso Terminado<br>Procesados: $con";
echo "<br><br><a href='$url1' style='text-decoration:none'>Archivo: Registros que no estan en SIGAS</a><br> <br><a href='$url2' style='text-decoration:none'>Archivo: Informe del CRUCE</a>";

 //echo 1;
 ?> 