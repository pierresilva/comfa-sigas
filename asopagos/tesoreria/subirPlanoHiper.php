<?php
// @autor:		Orlando Puentes A
// @fecha:		mayo 31/2010
// @formulario:	guardar datos persona - modulo empresa
?>
<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es">
<head> 
 <title>Procesar archivos</title>
 <link href="../../css/Estilos.css" rel="stylesheet" type="text/css" /> 
 <script type="text/javascript" src="../../newjs/1.6/jquery-1.6.2.min.js"></script>
 <script language="javascript" src="js/subirPlanoHiper.js"></script>
 
 </head>
 <body>
 <br />
 <p align="center" class="Rojo">Resultado de la copia de archivos</p>
 <br />
 <?php 
include_once("../../config.php"); 
include_once '../../funcionesComunes/funcionesComunes.php';
$directorio= $ruta_cargados.DIRECTORY_SEPARATOR.'tesoreria'.DIRECTORY_SEPARATOR.'cruces'.DIRECTORY_SEPARATOR;
$ok=verificaDirectorio($directorio);
if($ok==0){
	echo 2;		//no se pudo crear el directorio destino
	exit();
} 
$dir = $directorio.'TEMPO.txt';
if(file_exists($directorio))
	if(unlink($dir))
echo "<br><br><br><br>";
if(isset($_FILES['archivo'])){
	foreach ($_FILES['archivo']['error'] as $key => $error) {
	   if ($error == UPLOAD_ERR_OK) {
			$destino =  $directorio.'TEMPO.txt';
		   $archivos[]= $_FILES["archivo"]["name"][$key];
		   move_uploaded_file($_FILES["archivo"]["tmp_name"][$key],$destino) 
		   or die("Ocurrio un problema al intentar subir el archivo.");
	   }
	}
 }
 
for($i=0; $i<count($archivos); $i++){
	echo "<p align=center>Archivo copiado en el servidor: <strong>".$archivos[$i]."</strong></p>"; 
}
if($i>0){
?>
<center>
    <div id="boton1">
    <p style="text-decoration:none; font-size:12px; color:#F00; font-weight:bold; cursor:pointer" onclick="procesar();">Procesar estos archivos</p>
    </div>
</center>
<?php
	}
 ?> 
 <div id="ajax" align="center"> </div>
<div id="mensajes" align="center">..:..</div>
 </body>
 </html>
 
