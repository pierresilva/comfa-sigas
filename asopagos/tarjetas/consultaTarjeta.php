<?php
/* autor:       Orlando Puentes
 * fecha:       Julio 23 de 2010 
 */
date_default_timezone_set('America/Bogota');
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'p.definiciones.class.php';
$objClase=new Definiciones();
$fecha=date("m/d/Y");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="es">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>::Consulta Tarjeta::</title>

<link type="text/css" href="../../css/Estilos.css" rel="stylesheet"/>
<link type="text/css" href="../../css/estilo_tablas.css" rel="stylesheet"/>
<link href="../../css/marco.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="../../css/css.1.8/jquery-ui-1.8.17.custom.css" />
<link href="../../css/sorterStyle.css" rel="stylesheet" type="text/css" media="screen" />
<script type="text/javascript" src="../../js/1.6/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="../../js/1.6/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="../../js/jquery.tablesorter.js"></script>
<script type="text/javascript" src="../../js/jquery.tablesorter.pager.js"></script>

<script type="text/javascript" src="../../js/comunes.js"></script>
<script type="text/javascript" src="js/consultaTarjeta.js"></script>

<script type="text/javascript">
	$(function() {
		$( "#accordion" ).accordion({
			autoHeight: false,
			navigation: true
			});
		
		$( "#tMovimientos" ).tablesorter();
		$( "#tMovimientosAnt" ).tablesorter();
		$( "#cargues" ).tablesorter();
	});

shortcut.add("Shift+F",function() {
	var URL=src();
	var url=URL+"aportes/trabajadores/consultaTrabajador.php";
    window.open(url,"_blank");
    },{
	'propagate' : true,
	'target' : document 
        });        
</script>

</head>

<body>
<form name="forma">

<!-- TABLA VISIBLE CON BOTONES -->
<br />

<div class="demo">

<div id="accordion">
	<h3><a href="#">Consulta Tarjeta</a></h3>
	<div>
    <center>
		<table width="80%" border="0" cellspacing="0" cellpadding="0" align="center">
<tr>
<td width="13" height="29" class="arriba_iz">&nbsp;</td>
<td class="arriba_ce"><span class="letrablanca">::Datos de la Tarjeta&nbsp;::</span></td>
<td width="13" class="arriba_de" align="right">&nbsp;</td>
</tr>      
<tr>
 <td class="cuerpo_iz">&nbsp;</td>
<td class="cuerpo_ce">
<img src="../../imagenes/tabla/spacer.gif" width="1" height="1">
<img src="../../imagenes/spacer.gif" width="1" height="1"><img src="../../imagenes/spacer.gif" width="1" height="1"><img src="../../imagenes/spacer.gif" width="1" height="1"><img src="../../imagenes/spacer.gif" width="1" height="1"><img src="../../imagenes/spacer.gif" width="1" height="1"><img src="../../imagenes/spacer.gif" width="1" height="1"><img src="../../imagenes/spacer.gif" width="1" height="1"><img src="../../imagenes/menu/informacion.png" width="16" height="16" style="border:none; cursor:pointer" title="Manual" onClick="mostrarAyuda();" />
<img src="../../imagenes/menu/notas.png" width="16" height="16" style="cursor: pointer" title="Colaboración en línea" onclick="notas();" />
 <td class="cuerpo_de">&nbsp;</td>
</tr> 
<tr>     
<td class="cuerpo_iz">&nbsp;</td>
<td class="cuerpo_ce"><div id="error" style="color:#FF0000"></div></td>
<td class="cuerpo_de">&nbsp;</td>
</tr>
<tr>
<td class="cuerpo_iz">&nbsp;</td>

<td class="cuerpo_ce" align="center">
<table width="90%" border="0" cellspacing="0" class="tablero">
<tr>
<td width="25%">Buscar Por</td>
<td width="25%">
<select name="buscarPor" id="buscarPor" class="box1" onchange="mostrar();">
<option value="0" selected="selected">Seleccione</option>
<option value="1">Identificación</option>
<option value="2">Bono</option>
</select>
</td>
<td width="25%">&nbsp;</td>
<td width="25%">&nbsp;</td>
</tr>
</table>

<div id="tNumero" style="display:none">
<table width="90%" border="0" cellspacing="0" class="tablero">
<tr>
    <td width="25%">Tipo Documento</td>
    <td width="25%">
    <select name="tipoI" id="tipoI" class="box1" >
    <?php
	$consulta=$objClase->mostrar_datos(1, 1);
	while($row=mssql_fetch_array($consulta)){
		echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
	}
	?>
    </select>
    </td>
    <td width="25%">N&uacute;mero</td>
    <td width="25%">
    <input name="numero" type="text" class="box1" id="numero" onblur="buscarPersona();" onkeypress="tabular(this,event);" /></td>
</tr>
</table>
</div>

<div id="tBono" style="display:none">
<table width="90%" border="0" cellspacing="0" class="tablero">
<tr>
  <td width="25%">Bono N&uacute;mero</td>
  <td width="25%"><input name="txtBono" type="text" class="box1" id="txtBono" onblur="rellenarTarjeta(this,$.trim(this.value));buscarPersona2();" onkeypress="tabular(this,event);" /></td>
  <td width="25%">Identificaci&oacute;n</td>
  <td width="25%" id="lidentificacion">&nbsp;</td>
</tr>
</table>
</div>
<table width="90%" border="0" cellspacing="0" class="tablero" id="tTarjeta">

<tr>
  <td width="25%">Nombres</td>
  <td colspan="7" id="lnombre" width="75%">&nbsp;</td>
  </tr>
  <tr>
  <td width="25%">Bono</td>
  <td width="25%" colspan="5" id="lbono">&nbsp;</td>
  <td width="25%">Estado</td>
  <td id="lestado" width="25%">&nbsp;</td>
  </tr>

<tr>
  <td width="25%">Fecha Solicitud</td>
  <td width="25%" colspan="5" id="lfechasol"></td>
  <td width="25%">Saldo</td>
  <td width="25%" id="lsaldo"></td>
</tr>
<tr>
  <td width="25%">Entregada</td>
  <td width="25%" colspan="5" id="tEntregada"></td>
  <td width="25%">Fecha Entrega</td>
  <td width="25%" id="tFechaEntrega"></td>
</tr>
<tr>
  <td width="25%">Ubicacion</td>
  <td width="25%" colspan="5" id="lubicacion">
  <select id="ubicacion" name="ubicacion" class="box1" disabled="disabled">
  <option value="01">01 Neiva</option>
  <option value="02">02 Garzon</option>
  <option value="03">03 Pitalito</option>
  <option value="04">04 La Plata</option>
  </select>
  </td>
  <td width="25%">Motivo estado</td>
  <td width="25%">
  <select name="motivo" id="motivo" class="box1" disabled="disabled" style="width: 230px" >
    <?php
	$consulta=$objClase->mostrar_datos(12, 1);
	while($row=mssql_fetch_array($consulta)){
		echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
	}
	?>
    </select>
  </td>
  </tr>
 <tr>
  <td width="25%">Notas</td>
  <td width="75%" colspan="7" id="lnotas"></td>
</tr>
<tr>
  <td width="25%" class="RojoGrande">INACTIVAS</td>
  <td colspan="7" width="75%">&nbsp;</td>
  </tr>
<tr>
  <td>Tarjeta</td>
  <td>Estado</td>
  <td>Saldo</td>
  <td>Etapa</td>
  <td>Fecha </td>
  <td>Motivo Bloqueo</td>
  <td>Descripcion</td>
  <td>Usuario</td>
</tr>
<tfoot>
</tfoot>
</table>

<div id="errores" align="left"></div>
<td class="cuerpo_de">&nbsp;</td><!-- FONDO DERECHA -->
<tr>
<td class="abajo_iz" >&nbsp;</td>
    <td class="abajo_ce" ></td>
    <td class="abajo_de" >&nbsp;</td>
</tr>
</table>  
</center>
	</div>
   
   <!-- fin primer acordeon-->
	<h3><a href="#">Movimientos de la Tarjeta Activa</a></h3>
	<div style="display:none">
	<table width="100%" border="0" cellspacing="0" class="sortable hover" id="tMovimientos">
<caption style="text-align:left; padding:5px; font-size:11px;"><span></span></caption>
  <thead>
  <tr>
<th class="head"><h3><strong>Item</strong></h3></th>
<th class="head"><h3><strong>Fecha</strong></h3></th>
<th class="head"><h3><strong>Tipo</strong></h3></th>
<th class="head"><h3><strong>Valor</strong></h3></th>
<th class="head"><h3><strong>Establecimiento</strong></h3></th>
<th class="head"><h3><strong>Referencia</strong></h3></th>
<th class="head"><h3><strong>Fuente</strong></h3></th>
</tr>
</thead>
<tbody>

</tbody>
</table>
<div id="div-registros">N&uacute;mero de Registros: 
<select name="nRegistrosMovimientos" id="nRegistrosMovimientos" onchange="refrescar();">
	<option selected="selected"  value="10" >10</option>
	<option value="20">20</option>
	<option value="30">30</option>
	<option  value="40">40</option>
    <option  value="50">50</option>
</select>
</div> 
<br />
</div>
  
<!-- fin segundo acordeon --> 

<h3><a href="#">Movimientos de la Tarjeta Inactiva</a></h3>
<div style="display:none">
<table width="100%" border="0" cellspacing="0" class="sortable hover" id="tMovimientosAnt">
	<caption style="text-align:left; padding:5px; font-size:11px;"><span></span></caption>
	  <thead>
	  	<tr>
			<th class="head"><h3><strong>Item</strong></h3></th>
			<th class="head"><h3><strong>Fecha</strong></h3></th>
			<th class="head"><h3><strong>Tipo</strong></h3></th>
			<th class="head"><h3><strong>Valor</strong></h3></th>
			<th class="head"><h3><strong>Fuente</strong></h3></th>
		</tr>
	</thead>
	<tbody>
	
	</tbody>
</table>
<div id="div-registrosAnt">N&uacute;mero de Registros: 
<select name="nRegistrosMovimientosAnt" id="nRegistrosMovimientosAnt" onchange="refrescar3();">
	<option selected="selected"  value="10" >10</option>
	<option value="20">20</option>
	<option value="30">30</option>
	<option  value="40">40</option>
    <option  value="50">50</option>
</select>
</div>
<br />
</div>

<!-- fin segundo acordeon -->
<h3><a href="#">Traslados de dinero a la Tarjeta</a></h3>
<div style="display:none">
 <table width="100%" border="0" cellspacing="0" class="sortable hover" id="cargues">
<caption style="text-align:left; padding:5px; font-size:11px;"><span></span></caption>
  <thead>
  <tr>
<th class="head"><h3><strong>Periodo</strong></h3></th>
<th class="head"><h3><strong>Tipo</strong></h3></th>
<th class="head"><h3><strong>Autorizado</strong></h3></th>
<th class="head"><h3><strong>Conyuge</strong></h3></th>
<th class="head"><h3><strong>Valor</strong></h3></th>
<th class="head"><h3><strong>Proceso</strong></h3></th>
<th class="head"><h3><strong>Fecha</strong></h3></th>
</tr>
</thead>
<tbody>

</tbody>
</table>
<div id="div-registros-car">N&uacute;mero de Registros: 
<select name="nRegistrosTarjetaCar" id="nRegistrosTarjetaCar" onchange="refrescar2();">
	<option selected="selected"  value="10" >10</option>
	<option value="20">20</option>
	<option value="30">30</option>
	<option  value="40">40</option>
    <option  value="50">50</option>
</select>
</div> 		
		
	</div>
	
</div><!-- End acodion -->

</div><!-- End demo -->

<div style="display: none;" class="demo-description">
<p>
Click headers to expand/collapse content that is broken into logical sections, much like tabs.
Optionally, toggle sections open/closed on mouseover.
</p>
<p>
The underlying HTML markup is a series of headers (H3 tags) and content divs so the content is
usable without JavaScript.
</p>
</div><!-- End demo-description -->



       
<!--colaboracion en linea-->
<div id="dialogo-archivo" title="Archivo banco">
<div id="progreso" style="display: none; font-size: 15pt; font-weight: bold;">Procesado(s) <span id="pg">0</span> de <span id="tt"></span> archivo(s)</div>
<div id="log"></div>
</div>
<div id="dialog-form2" title="Colaboraci&oacute;n en l&iacute;nea" style="display:none">
<p>Por favor diligencie este formulario para enviar comentarios, errores o falencias encontradas en el proceso. M&aacute;ximo 250 caracteres </p>
<label>Tus comentarios:</label><br />
<textarea name="notas" id="notas" cols="60", rows="10"></textarea>
</div>

<!-- fin colaboracion -->

<!-- ayuda en linea -->
<div id="ayuda" title="Manual .:. Bloqueo Tarjeta" style="background-image:url(../../imagenes/FondoGeneral0.png)"></div>

</form>

</body>
<script language="javascript">
$("#buscarPor").focus();

function mostrarAyuda(){
	$("#ayuda").dialog('open' );
	}

function notas(){
	$("#dialog-form2").dialog('open');
	}	
</script>
</html>
