<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

if ( !isset( $_SESSION ['USUARIO'] ) ) {session_start();}
if(empty($_SESSION['LOGUIADO'])){
	echo "no hay session!!";
	exit();
}
$raiz=$_SESSION['RAIZ'];
include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'session.php';
session();
$url=$_SERVER['PHP_SELF'];
include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'auditoria.php';
auditar($url);
include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'p.definiciones.class.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'ciudades.class.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';

$objClase=new Definiciones();
$objCiudad=new Ciudades();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::Asignar Tarjeta::</title>
<link type="text/css" href="../../css/css.1.8/jquery-ui-1.8.17.custom.css" rel="stylesheet">
<link type="text/css" href="../../css/marco.css" rel="stylesheet">
<link type="text/css" href="../../css/Estilos.css" rel="stylesheet">
<script type="text/javascript" src="../../js/1.6/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="../../js/1.6/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/comunes.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/jquery.combos.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/direccion.js"></script>
<script type="text/javascript" src="js/asignarTarjeta.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	shortcut.add("Shift+F",function() {
		var URL=src();
		var url=URL+"aportes/trabajadores/consultaTrabajador.php";
    	window.open(url,"_blank");
    },{
	'propagate' : true,
	'target' : document 
        }); 

	$("#fecNac").datepicker({
			altField: "#alternate",
			altFormat: "yy, MM, d",
			changeMonth: true,
			changeYear: true,
			minDate: new Date(1940, 1, 1),
       		maxDate: '-10Y'  
		});       
})
</script>

</head>

<body>
<table border="0" align="center" cellpadding="0" cellspacing="0" width="80%">
  <tbody>
 
  <!-- ESTILOS SUPERIOR TABLA-->
  <tr>
    <td width="13" height="29" class="arriba_iz">&nbsp;</td>
    <td class="arriba_ce"><span class="letrablanca">::&nbsp;Asignar Tarjeta::</span></td>
    <td width="13" class="arriba_de" align="right">&nbsp;</td>
  </tr>
  
  <!-- ESTILOS ICONOS TABLA-->
  <tr>
   <td class="cuerpo_iz">&nbsp;</td>
	<td class="cuerpo_ce">
   <img src="../../imagenes/menu/nuevo.png" alt="Nuevo" width="16" height="16" style="cursor:pointer" title="Nuevo" onclick="nuevoT()" />
   <img src="<?php echo URL_PORTAL; ?>imagenes/spacer.gif" alt="" width="1" height="1" />
   <img src="../../imagenes/menu/grabar.png" alt="Guardar" width="16" height="16" style="cursor:pointer" title="Guardar" onclick="guardarT();" id= "bGuardar" />  
   <img src="<?php echo URL_PORTAL; ?>imagenes/menu/informacion.png" alt="Ayuda" width="16" height="16" style="border:none; cursor:pointer" title="Manual" onclick="mostrarAyuda();" />
   <img src="<?php echo URL_PORTAL; ?>imagenes/menu/notas.png" alt="Soporte" width="16" height="16" style="cursor: pointer" title="Colaboraci&oacute;n en l&iacute;nea" onclick="notas();" />
      </td>
    <td class="cuerpo_de">&nbsp;</td>
  </tr>
  
  <!-- ESTILOS MEDIO TABLA-->
  <tr>
    <td class="cuerpo_iz">&nbsp;</td>
    <td class="cuerpo_ce">
    <br />
   <table border="0" cellpadding="3" align="center" class="tablero" width="90%">
	<tr>
	<td width="25%">Tipo Documento</td>
	<td width="25%">
	<select id="tipoDoc" class="box1">
    <?php
	$consulta= $objClase->mostrar_datos(1,1);
	while($row=mssql_fetch_array($consulta)){
	echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
	}
?>
    </select>
	<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
<td width="25%">N&uacute;mero</td>
<td width="25%"><input id="identificacion" type="text" class="box1" onkeypress="return validarNumero(event)" onblur="buscarPersona(document.getElementById('tipoDoc').value,this.value)" /></td>
</tr>
    <tr>
      <td>Primer apellido</td>
      <td><input type="text" name="pa" id="pa" class="box1" onkeypress="return vpnombre(event)" />
        <img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
      <td>Segundo Apellido</td>
      <td><input type="text" name="sa" id="sa" class="box1" onkeypress="return vsnombre(event)" /></td>
    </tr>
    <tr>
      <td>Primer nombre</td>
      <td><input type="text" name="pn" id="pn" class="box1" onkeypress="return vpnombre(event)" />
        <img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
      <td>Segundo nombre</td>
      <td><input type="text" name="sn" id="sn" class="box1" onkeypress="return vsnombre(event)" /></td>
    </tr>
    <tr>
      <td>Nombre Realce</td>
      <td colspan="3"><input type="text" name="realce" id="realce" class="boxlargo" readonly="readonly" />
        <img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
      </tr>
    <tr>
      <td>Fecha Nacimiento</td>
      <td colspan="3"><input type="text" name="fecNac" id="fecNac" class="box1" readonly="readonly" />&nbsp;<input type="text" id="alternate" class="box1"/>
        <img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
      </tr>
    <tr>
      <td>Género</td>
      <td><select name="sexo" id="sexo" class="box1">
        <option value="0">Seleccione...</option>
          <option value="M">Masculino</option>
	      <option value="F">Femenino</option>
      </select>        
        <img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
      <td>País</td>
      <td><select name="pais" id="pais" class="box1" disabled="disabled">
          <option value="57">COLOMBIA</option>
	     </select>        
     </td>
    </tr>
    <tr>
      <td>Departamento Res.</td>
      <td><select name="cboDepto" id="cboDepto" class="box1">
        <option value="0">Seleccione..</option>
          <?php
		$consulta = $objCiudad->departamentos();
		while($r_defi=mssql_fetch_array($consulta)){
		echo "<option value=".$r_defi['coddepartamento'].">".utf8_encode($r_defi['departmento'])."</option>";
			}
        ?>
      </select>
        <img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
      <td>Ciudad Res.</td>
      <td><select name="cboCiudad" id="cboCiudad" class="box1">
        <option value="0">Seleccione..</option>
      </select>
        <img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
    </tr>
    <tr>
      <td>Dirección</td>
      <td colspan="3"><input type="text" name="txtDireccion" id="txtDireccion" class="boxmediano" onfocus="direccion(this,document.getElementById('telefono'));"/></td>
      </tr>
    <tr>
      <td>Teléfono</td>
      <td><input type="text" name="telefono" id="telefono" class="box1"/>
        <img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
      <td>Celular</td>
      <td><input type="text" name="celular" id="celular" class="box1"/></td>
    </tr>
    <tr>
      <td>E-mail</td>
      <td colspan="3"><input type="text" name="email" id="email" class="boxlargo" /></td>
      </tr>
     <tr>
      <td>Profesión</td>
      <td><select name="profesion" id="profesion" class="box1">
        <option value="0" selected="selected">Sin Profesi&oacute;n</option>
        <?php
	$consulta=$objClase->mostrar_datos(20, 4);
	while($row=mssql_fetch_array($consulta)){
		echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
	}
	?>
      </select></td>
      <td>Saldo REDEBAN</td>
      <td><input type="text" name="saldo" id="saldo" class="box1" readonly="readonly" /></td>
    </tr>
    <tr>
      <td>Negocio</td>
      <td><select name="negocio" id="negocio" class="box1" onchange="validarTipo(this.value)">
        <option value="0">Seleccione..</option>
        <option value="S">Subsidio</option>
   <!-- <option value="H">Hipermercado</option>
        <option value="F">Fonede</option>  -->
      
      </select>  
            <img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
      <td>Tipo Tarjeta</td>
      <td><select name="tipoT" id="tipoT" class="box1">
        <option value="0">Seleccione..</option>
         <?php
	$consulta=$objClase->mostrar_datos(9, 3);
	while($row=mssql_fetch_array($consulta)){
		echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
	}
	?>
      </select>
        <img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
    </tr>
   
	</table>
   </td>
    <td class="cuerpo_de">&nbsp;</td>
  </tr>
  
  <!-- CONTENIDO TABLA-->
  <tr>
    <td class="cuerpo_iz"></td>
    <td class="cuerpo_ce">    
    </td>
    <td class="cuerpo_de"></td>
  </tr>
  
  <!-- ESTILOS PIE TABLA-->
  <tr>
    <td class="abajo_iz" ></td>
    <td class="abajo_ce" ></td>
    <td class="abajo_de" ></td>
  </tr>
  
  
</tbody>
  
</table>
<!-- FORMULARIO OCULTO HIPERMERCADO -->
 
  <table width="70%" border="0" cellspacing="0" class="tablero" id="div-hiper" align="center" style="display:none">
  	<tr>
  	   <th colspan="4">Información Adicional Terceros</th>
     </tr>
     <tr>
       <td width="16%">Estado</td>
       <td width="31%"><select name="estado" id="estado" disabled="disabled" class="box1">
         <option value="A" selected="selected">ACTIVO</option>
         <option value="I">INACTIVO</option>
       </select></td>
       <td width="17%">Nombre Comercial</td>
       <td width="36%"><input type="text" name="ncomercial" id="ncomercial" class="boxmediano" disabled="disabled" /></td>
     </tr>
     
     <tr>
       <td>Acumula Puntos</td>
       <td><select name="puntos" id="puntos" class="box1">
         <option value="S" selected="selected">SI</option>
         <option value="N">NO</option>
       </select></td>
       <td>Tipo Cliente</td>
       <td><select name="tipoCliente" id="tipoCliente" class="boxmediano" disabled="disabled">
         <option value="1">AFILIADO COMFAMILIAR</option>
       </select></td>
     </tr>
     
     <tr>
       <td>Tipo Impuesto</td>
       <td><select name="tipoImpuesto" id="tipoImpuesto" class="box1" disabled="disabled">
         <option value="S" selected="selected">SIMPLIFICADO</option>
         <option value="R" selected="selected">REGIMEN COMUN</option>
         <option value="C" selected="selected">GRAN CONTRIBUYENTE</option>
         <option value="G" selected="selected">GOBIERNO</option>
       </select></td>
       <td>% Retención Iva</td>
       <td><input name="iva" type="text" class="boxcorto" id="iva" value="0" /></td>
     </tr>
     
     <tr>
       <td>Autoretiene Renta</td>
       <td><select name="retieneRenta" id="retieneRenta" class="box1">
         <option value="S">Si</option>
         <option value="N" selected="selected">No</option>
       </select></td>
       <td>Responsable Iva</td>
       <td><select name="responsableIva" id="responsableIva" class="box1">
         <option value="S" selected="selected">Si</option>
         <option value="N">No</option>
       </select></td>
     </tr>
     
     <tr>
       <td>Declaración Renta</td>
       <td><select name="declaracionRenta" id="declaracionRenta" class="box1">
         <option value="S" selected="selected">Si</option>
         <option value="N">No</option>
       </select></td>
       <td>&nbsp;</td>
       <td>&nbsp;</td>
     </tr>
    
  </table>

	<!-- FIN OCULTO HIPERMERCADO -->


<!-- colaboracion en linea -->
<div id="dialog-form2" title="Colaboraci&oacute;n en l&iacute;nea">
<p>Por favor diligencie este formulario para enviar comentarios, errores o falencias encontradas en el proceso. M&aacute;ximo 250 caracteres </p>
<label>Tus comentarios:</label><br />
<textarea name="notas" id="notas" cols="60" rows="10"></textarea>
</div>

<!-- Manual Ayuda -->
<div id="ayuda" title="::Manual Asignaci&oacute;n Tarjeta::"></div>
<!-- formulario direcciones  --> 
<div id="dialog-form" title="Formulario de direcciones"></div>
</body>
</html>