/*
* @autor:      Ing. Orlando Puentes
* @fecha:      septiembre 6 de 2010
*/
var URL=src();
var estado=0;
var idp=0;
var idb=0;
var bono=0;
var bonoAnt=0;

//Dialog Ayuda
function mostrarAyuda(){
	$("#ayuda").dialog('open' );
}

function notas(){
	$("#dialog-form2").dialog('open');
}

$(document).ready(function(){
	//Dialog ayuda
	$("#ayuda").dialog({
		autoOpen: false,
		height: 500,
		width: 750,
		draggable:true,
		modal:false,
		open: function(evt, ui){
			$('#ayuda').html('');
			$.get(URL +'help/tarjetas/manualayudaBloqueotarjeta.html',function(data){
				$('#ayuda').html(data);
			});
		}
	});
	//Dialog Colaboracion en linea
	$("#dialog-form2").dialog({
		autoOpen: false,
		height: 400,
		width: 500,
		modal: true,
		buttons: {
		'Enviar': function() {
			var bValid = true;
			var campo=$('#notas').val();
			var campo0=$.trim(campo);
			if (campo0==""){
				$(this).dialog('close');
				return false;
			}
			var campo1=$('#usuario').val();
			var campo2="radicacion.php";
			$.post(URL+'phpComunes/colaboracion.php',{v0:campo0,v1:campo1,v2:campo2},
			function(datos){
				if(datos=='1'){
					alert("Su comentario fue enviado correctamente!. Gracias por participar en el mejoramiento de SIGAS");
				}else{
					alert(datos);
				}
			});
			$(this).dialog('close');
		},
		Cancelar: function() {
			$(this).dialog('close');
			$("#dialog-form2").dialog("destroy");
		}
		}
	});
	$("#buscarPor").bind('change',function(){
 		$("#lnombre,#lbono,#lfechasol,#lsaldo").html(" ");
		$("#lestado").html("");
//		$("#buscarPor").focus();
		});
});//fin ready

function mostrar(){
	var opt=$("#buscarPor").val();
	if(opt==0){
		$("#tNumero").css("display", "none"); 
		$("#tBono").css("display", "none"); 
		$("#numero").val("");
		}
	if(opt==1){
		$("#tNumero").css("display", "block"); 
		$("#tBono").css("display", "none"); 
		$("#tipoI").focus();
		}
	if(opt==2){
		$("#tBono").css("display", "block"); 
		$("#tNumero").css("display", "none"); 
		$("#txtBono").focus();
		}
	}
	
function buscarPersona(){
	var tipoI=$("#tipoI").val();
	var numero=$.trim($("#numero").val());
	if(numero.length==0){
		return false;
	}
	$("#tTarjeta tfoot").empty();
	$("#tMovimientosAnt tbody tr").remove();
	$.getJSON(URL+'phpComunes/buscarPersona2.php',{v0:tipoI,v1:numero},function(data){
		if(data==0){
			alert("Lo lamento, el n\u00FAmero no existe!");
			$("#numero").val("");
			$("#numero").focus();
			return false;
		}
		$.each(data,function(i,fila){
		var nom=fila.pnombre+" "+fila.snombre+" "+fila.papellido+" "+fila.sapellido;
		idp=fila.idpersona;
		$("#lnombre").html(nom);
		//return;
		});
	$.getJSON('pdo.buscar.tarjeta.idp.php',{v0:idp},function(datos){
			if(datos==0){
				alert("Lo lamento, no hay bono asociado a ese n�mero!");
				return false;
			}
		$.each(datos,function(i,fila){
			inactivas(idp);
			if(fila.saldo==null){
				fila.saldo='0';	
			}
			idb=fila.idtarjeta;
			$("#lbono").html(fila.bono);
			$("#lfechasol").html(fila.fechasolicitud);
			$("#lestado").html(fila.estado);
			$("#lsaldo").html(formatCurrency(fila.saldo));
			$("#ubicacion").val(fila.ubicacion);
			$("#tEntregada").html(fila.entregada);
			$("#tFechaEntrega").html(fila.fechaentrega)
			$("#motivo").val(fila.idetapa);
			$("#lnotas").html(fila.nota);
			estado=fila.estado;
			bono=fila.bono;
			refrescar();
			refrescar2();
			return false;
		});
		
		});
	})
	
	}	

function inactivas(idp){
	$.getJSON('pdo.buscar.tarjeta.inac.php',{v0:idp},function(dato){
			if(dato==0){
				return false;
				}		
			$.each(dato,function(i,f){
				$("#tTarjeta tfoot").append("<tr><td><label style=cursor:pointer onclick='movAnterior(" + f.bono + ")'>" + f.bono+"</label></td><td>"+f.estado+"</td><td>"+f.saldo+"</td><td>"+f.detalledefinicion+"</td><td>"+f.fechaetapa+"</td><td>"+f.bloqueo+"</td><td>"+f.nota+"</td><td>"+f.usuario+"</td></tr>");
				return;
				});
			})
	}
	
function movAnterior(bono){
	bonoAnt=bono
	$("#tMovimientosAnt tbody tr").remove();
	var num=$("#nRegistrosMovimientosAnt").val();
		
	$.getJSON(URL+'phpComunes/pdo.buscar.movimientos.php',{v0:bono,v1:100},function(data){
		if(data==0){
			//alert("Lo lamento, no hay movimientos asociados a este bono!");
			return false;
		}
		$.each(data,function(i,n){
	
			var no=data.length;
			$("#tMovimientosAnt caption span").html(no+" registros encontrados."); 
			$("#tMovimientosAnt tbody").append("<tr><td>"+n.idconsumo+"</td><td>"+n.fechatransaccion+' '+n.horatransaccion+"</td><td>"+n.descripcion+"</td><td style=text-align:right>"+formatCurrency(n.valor)+"</td><td>"+n.numdispositivo+"</td></tr>");  
		});
		 
		$("#tMovimientosAnt").trigger("update");
		$("#tMovimientosAnt tbody tr:odd").addClass("evenrow");
		return;
	});
	}
		
function buscarPersona2(){
	bono=$.trim($("#txtBono").val());
	var idp=0;
	if(bono.length==0){
		return false;
	}
	$("#tTarjeta tfoot").empty();
	$("#tMovimientosAnt tbody tr").remove();
	$.ajax({
		url: 'pdo.buscar.tarjeta.tar.php',
		type: "POST",
		async: false,
		data: {v0:bono},
		dataType: "json",
		success: function(datos){
		if(datos==0){
			//Buscar Inactivas
			$.ajax({
				url: 'pdo.buscar.tarjeta.tar.inac.php',
				type: "POST",
				async: false,
				data: {v0:bono},
				dataType: "json",
				success: function(datosI){
					if(datosI==0){
						alert("Lo lamento, el bono no existe!");
						$("txtBono").val("");
						$("txtBono").focus();
						limpiarCamposConsulta();
						return false;
					} else { 
						datos=datosI;
						bono=datos[0].bono;
					}
				}
			});
			if(datos==0){ return false; }
		}
		idb=datos[0].idtarjeta;
		idp=datos[0].idpersona;
		inactivas(idp);
		$("#lbono").html(datos[0].bono);
		$("#lfechasol").html(datos[0].fechasolicitud);
		$("#lsaldo").html(formatCurrency(datos[0].saldo));
		$("#lestado").html(datos[0].estado);
		$("#lubicacion").html(datos[0].ubicacion)
		estado=datos[0].estado;
		$("#ubicacion").val(datos[0].ubicacion);
		$("#tEntregada").html(datos[0].entregada);
		$("#tFechaEntrega").html(datos[0].fechaentrega)
		refrescar();
		refrescar2();
		$.getJSON(URL+'phpComunes/pdo.buscar.persona.id.php',{v0:idp},function(data){
		if(data==0){
			alert("Lo lamento, el n\u00FAmero no existe!");
			$("#numero").val("");
			$("#numero").focus();
			return false;
		}
		$.each(data,function(i,fila){
			var nom=fila.pnombre+" "+fila.snombre+" "+fila.papellido+" "+fila.sapellido;
			var identificacion=fila.identificacion;
			$("#lnombre").html(nom);
			$("#lidentificacion").html(identificacion);			
			return;
			});
		});
		}
		});
	}	

function limpiarCampos(){
	$("table.tablero input:text").val('');
	$("table.tablero select").val(0);
	$("#lnombre,#lbono,#lfechasol,#lsaldo").html(" ");
	$("#lestado").html("");
	$("#tNumero").css("display", "none"); 
	$("#tBono").css("display", "none"); 
	$("#numero").val("");
	$("#buscarPor").focus();
	$("#tTarjeta tfoot").empty();
}

function limpiarCamposConsulta(){
	$("table.tablero input:text").val('');
	$("#lnombre,#lbono,#lfechasol,#lsaldo").html(" ");
	$("#lestado").html("");
	$("#tNumero").css("display", "none"); 
	$("#tBono").css("display", "none"); 
	$("#numero").val("");
	$("#buscarPor").focus();
	mostrar();
}

function refrescar(){
	$("#tMovimientos tbody tr").remove();
	var num=$("#nRegistrosMovimientos").val();
		
	$.getJSON(URL+'phpComunes/pdo.buscar.movimientos.php',{v0:bono,v1:100},function(data){
		if(data==0){
			//alert("Lo lamento, no hay movimientos asociados a este bono!");
			return false;
		}
		$.each(data,function(i,n){
	
			var no=data.length;
			$("#tMovimientos caption span").html(no+" registros encontrados."); 
			$("#tMovimientos tbody").append("<tr><td>"+n.idconsumo+"</td><td>"+n.fechatransaccion+' '+n.horatransaccion+"</td><td>"+n.descripcion+"</td><td style=text-align:right>"+formatCurrency(n.valor)+"</td><td>"+n.detalledefinicion+"</td><td>"+n.referencia+"</td><td>"+n.numdispositivo+"</td></tr>");  
		});
		 
		$("#tMovimientos").trigger("update");
		$("#tMovimientos tbody tr:odd").addClass("evenrow");
		return;
	});
}

function refrescar2(){
	$("#cargues tbody tr").remove();
	var num=$("#nRegistrosTarjetaCar").val();
		
	$.getJSON(URL+'phpComunes/pdo.buscar.cargues.php',{v0:idp,v1:100},function(data){
		if(data==0){
			//alert("Lo lamento, no hay movimientos asociados a este bono!");
			return false;
		}
		$.each(data, function(i,n){ 
				var row=(data==false ? "-" : data.ultimo );
				var no=data.length;
				$("#cargues caption span").html(no+" registros encontrados."); 
				$("#cargues tbody").append("<tr><td>"+n.periodogiro+"</td><td>"+n.tipopago+"</td><td>"+n.idpersona+"</td><td>"+n.idconyuge+"</td><td style=text-align:right>"+formatCurrency(n.valor)+"</td><td>"+n.procesado+"</td><td style=text-align:center>"+n.fechasistema+"</td></tr>");  
       	          });//end each emrpesa Id	<td>"+lic_maternidad+"</td>
			 //Actualizar Tabla de sorter 
				  $("#cargues").trigger("update");
				  $("#cargues tbody tr:odd").addClass("evenrow");
	});
}