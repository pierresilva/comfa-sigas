var URL=src();
var nuevo=0;
var existe=0;
var idpersona=0;

$(document).ready(function(){
//$("#fecha").datepicker();
	
	$('#numRecibo').hide();
	$("#tipoReexpedicion").bind("change",function(){
		if($('#tipoReexpedicion').val()==1){
			$('#numRecibo').show();
		}else{
			$('#numRecibo').hide();
		}
		
	});
	
//Dialog Colaboracion en linea
  $("#dialog-form2").dialog({
		autoOpen: false,
		height: 400,
		width: 500,
		draggable:false,
		modal: true,
		buttons: {
		    'Enviar': function() {
			var bValid = true;
			var campo=$('#notas').val();
			var campo0=$.trim(campo);
			if (campo0==""){
				$(this).dialog('close');
					return false;
				}
			var campo1=$('#usuario').val();
			var campo2="empresaSinConv.php";
			$.post(URL+'phpComunes/colaboracion.php',{v0:campo0,v1:campo1,v2:campo2},
			function(datos){
				if(datos=='1'){
					alert("Su comentario fue enviado correctamente!. Gracias por participar en el mejoramiento de SIGAS");
				}else{
					alert(datos);
				}
		});
		$(this).dialog('close');
		},
		Cancelar: function() {
			$(this).dialog('close');
			$("#dialog-form2").dialog("close");
		}
	}
  });
   //Dialog ayuda
  $("#ayuda").dialog({
		 	      autoOpen: false,
			      height: 450,
			      width: 700,
			      modal:true,
			      open: function(evt, ui){
					$('#ayuda').html('');
					/*$.get(URL +'help/promotoria/xxx.html',function(data){
							$('#ayuda').html(data);
					});*/
			      }
});
});//ready
function nuevoT(){
	nuevo=1;
	limpiarCampos();
	$("#identificacion").focus();
}
//---------------------------------------GUARDAR TARJETA
function guardarT(){
   $("table.tablero :input.ui-state-error,select.ui-state-error").removeClass("ui-state-error");//Limpiar la clase de error
   var idper=0;
   var estadoTercero;
   if(nuevo==0){
   MENSAJE("Si es un registro nuevo haga click en NUEVO.");
   return false;
   }
   var error=0;
   var email=/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test($("#email").val());
   
   if($("#identificacion").val()=='')	{$("#identificacion").addClass("ui-state-error");error++;}
   if($("#pa").val()=='')				{$("#pa").addClass("ui-state-error");error++;}
   if($("#pn").val()=='')				{$("#pn").addClass("ui-state-error");error++;}
   if($("#sexo").val()=='0')			{$("#sexo").addClass("ui-state-error");error++;}
   if($("#cboDepto").val()=='0')		{$("#cboDepto").addClass("ui-state-error");error++;}
   if($("#cboCiudad").val()=='0')		{$("#cboCiudad").addClass("ui-state-error");error++;}
   if($("#txttxtDireccion").val()=='')	{$("#txttxtDireccion").addClass("ui-state-error");error++;}
   if($("#telefono").val()=='')			{$("#telefono").addClass("ui-state-error");error++;}
   if($("#fecNac").val()=='')			{$("#fecNac").addClass("ui-state-error");error++;}
   if($("#negocio").val()=='0')			{$("#negocio").addClass("ui-state-error");error++;}
   if($("#tipoT").val()=='0')			{$("#tipoT").addClass("ui-state-error");error++;}
   if($("#tipoReexpedicion").val()=='0'){$("#tipoReexpedicion").addClass("ui-state-error");error++;}
   if($("#tipoReexpedicion").val()=='1'){
	    if($("#numRecibo").val()==''){
			$("#numRecibo").addClass("ui-state-error");
			error++;
			}
	   }
   var e=$.trim($("#email").val());
   if(e.length>0){if(!email) {$("#email").addClass("ui-state-error");error++;}}
   if(error>0){
   		MENSAJE("Llene los campos obligados!")
   return false; 
   }
   var nomcorto=nombre_corto($("#pn").val(),$("#sn").val(),$("#pa").val(),$("#sa").val());
   $("#realce").val(nomcorto);
   
    //Insertar ó Actualizar persona
	datos={
			v0:idpersona,
			v1:$("#tipoDoc").val(),
			v2:$("#identificacion").val(),
			v3:$("#pa").val(),
			v4:$("#sa").val(),
			v5:$("#pn").val(),
			v6:$("#sn").val(),
			v7:$("#sexo").val(),
			v8:$("#txtDireccion").val(),
			v9:$("#telefono").val(),
			v10:$("#celular").val(),
			v11:$("#email").val(),
			v12:$("#cboDepto").val(),
			v13:$("#cboCiudad").val(),
			v14:$("#fecNac").val(),
			v15:$("#profesion").val(),
			v16:$("#realce").val()
			};   
  idper=actPersona(datos); 
   
  guardarTarjeta(idper);
}

function actPersona(datos){
	$.ajax({
		url:"pdo.actualizarPersona.php",
		type: "POST",
		data: datos,
		async: false,
		success: function(data){
			if(data==0){
				alert("No se pudo actualizar la persona!");
				return false
			}
			alert("Persona actualizada!");
			return true;
		}
		});//ajax*/
	}
	
//---------------------------------------GUARDAR/ACTUALIZAR PERSONA
function persona(datos){
	var idPersonaIA;
	var ruta=URL+'aportes/trabajadores/insertPersona.php';
	$.ajax({
		url:ruta ,
		type: "POST",
		data: datos,
		async: false,
		success: function(data){
			if(data==0){
				alert("No se pudo grabar la persona!");
				return false
			}
			if(data>0){
				alert("Se grabo la persona ID "+data);
				$("#idPersona").val(data);
				idpersona=data;
				return true;
			}	
			alert("No se pudo grabar la persona. Error: "+data);
			return false;
		}
		});//ajax*/
}

//---------------------------------------GUARDAR TARJETA
function guardarTarjeta(idp){
	var tipo=$("#tipoT").val();
	var negocio=$("#negocio").val();
	var ced=$("#identificacion").val();
	$.ajax({
		url: "pdo.reexpedirTarjeta.php",
		type: "POST",
		async: false,
		data: {v1:idpersona,v2:negocio,v3:tipo,v4:ced},
		success: function(datos){
			var x=parseInt(datos);
			switch (x){
				case 0 : msg='Tarjeta asignada!'; break;
				case 1 : msg='Ya existe una tarjeta activa NO fue asignada una nueva tarjeta'; break;
				case 2 : msg='No hay plasticos disponibles!'; break;
				case 3 : msg='No se pudo marcar A el maestro de tarjetas'; break;
				case 4 : msg='No se pudo asignar la tarjeta!'; break;
				case 5 : msg='No se pudo actualizar el maestro ASOPAGOS'; break;
				case 6 : msg='No hay bono bloqueado'; break;
				case 7 : msg='No se pudo realizar el traslado del saldo'; break;
				case 8 : msg='No se pudo realizar el traslado del saldo'; break;
				case 9 : msg='No se pudo borrar la tarjeta bloqueada!'; break;
			}
			alert(msg);
			limpiarCampos();
		}
		
	});//ajax
}

//---------------------------------------GUARDAR TERCEROS
function guardarTerceros(){}

//---------------------------------------BUSCAR AFILIADO-PERSONA
function buscarPersona(td,numero){
	if(numero.length==0) return false;
	$("table.tablero input:text").not("#identificacion,#pais,#iva").val('');
	$("#guardar").show();
	  $.ajax({
		  url:URL+'phpComunes/buscarPersona2.php',
		  type:"POST",
		  dataType:"json",
		  async:false,
		  data:{v1:numero,v0:td},
		  success:function(datos){
			if(datos==0){
			alert  ("La persona no existe en nuestra base de datos!");
			$("#identificacion").val('');
			$("#identificacion").focus();
			existe=0;
			return false;
			}else{
			existe=1;
			}
			
			$.each(datos,function(i,fila){
				idpersona   =fila.idpersona;
				pn			=$.trim(fila.pnombre);
				sn			=$.trim(fila.snombre);
				pa			=$.trim(fila.papellido);
				sa			=$.trim(fila.sapellido);
				sexo		=(fila.sexo);
				idp			=(fila.idpersona);
				idciures	=(fila.idciuresidencia);
				iddepres	=(fila.iddepresidencia);
				txtDireccion=$.trim(fila.direccion);
				telefono	=$.trim(fila.telefono);
				celular		=$.trim(fila.celular);
				email		=$.trim(fila.email);
				profesion	=(fila.idprofesion);
				fecnac		=(fila.fechanacimiento);
				nr			=$.trim(fila.nombrecorto);
			});//each
			$("#pn").val(pn);
			$("#sn").val(sn);
			$("#pa").val(pa);
			$("#sa").val(sa);
			$("#realce").val(nr);
			$("#sexo").val(sexo);
			$("#idPersona").val(idp);
			
			$("#cboDepto").val(iddepres).trigger("change");
			setTimeout((function(){
			$("#cboCiudad").val(idciures);
		    $("#cboCiudad").trigger("change");
			 }),1000);
			
			$("#txtDireccion").val(txtDireccion);
			$("#telefono").val(telefono);
			$("#celular").val(celular);
			$("#email").val(email);
			
			$("#profesion").val(profesion);
			$("#fecNac").val(fecnac);
		  },
		 complete:function(){
			if(existe==1){
			//Buscar tarjetas activas con ese idpersona
			$.ajax({
			url:'buscarTarjetasBloqueadas.php',
			type:"POST",
			dataType:"json",
			data:{v0:idp},
			success:function(data){
				if(data==0){
					MENSAJE("La persona no posee <b>Tarjetas Bloqueadas</b>");
					limpiarCampos();
					return false;
				}
				if(data[0].diaspasados>0){
					if(data[0].diassaldo>=0){
						existe=2;
						$("#tdSaldo").html(data[0].saldo);
						$("#tdTarjeta").html(data[0].bono);
					} else {
						MENSAJE("Favor comunicarse con la auxiliar de tarjetas y solicitar la actualizacion de saldos de la fecha " + data[0].fechaetapa);
						limpiarCampos();
						return false;
					}
				} else {
					MENSAJE("Debe esperar <b>24</b> horas para la reexpedicion de la tarjeta, Por Actualizacion de saldos de REDEBAN");
					limpiarCampos();
					return false;
				}
				
			},
			complete:function(){
				if(existe==2){
					//Buscar tarjetas activas con ese idpersona
					$.ajax({
					url:'buscarTarjetasActivas2.php',
					type:"POST",
					dataType:"json",
					data:{v0:idp},
					success:function(datas){
						if(datas!=0){
							idb=datas[0].idtarjeta;
							MENSAJE("La persona posee una tarjeta <b>ACTIVA</b> con No de bono: <b>"+datas[0].bono+"</b>");
							limpiarCampos();
						}else{
							MENSAJE("La persona <b>NO</b> posee tarjetas activas.");
							existe=2;
						}
			}
			});//complete
		}
			}
			});
			}//complete
		} 		 
	  });//ajax
}
//----------------------------------------VENTANA DE DIALOGO DE AYUDA
function mostrarAyuda(){
	$("#ayuda").dialog('open');
}
//----------------------------------------VENTANA DE DIALOGO DE MANUAL
function notas(){
	$("#dialog-form2").dialog('open');
}

function limpiarCampos(){
	$("table.tablero input:text").not("#pais,#iva").val('');
	$("table.tablero select").val(0);
	$("#tipoDoc").focus();
	$("#idPersona").val('');
	$("#mensaje small").empty();
	$("#mensaje").hide();
	$("table.tablero :input.ui-state-error,select.ui-state-error").removeClass("ui-state-error");//Limpiar la clase de error
	$("#tdSaldo").html('');
	$("#tdTarjeta").html('');
	$("#guardar").show();
	
}
