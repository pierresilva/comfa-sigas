/*
* @autor:      Ing. Orlando Puentes
* @fecha:      septiembre 6 de 2010
* objetivo:
*/
var URL=src();
var estado=0;
var idp=0;
var idb=0;

function buscarBono(){
	var bono=$.trim($("#bono").val());
	if(bono.length==0) return false;
	$.ajax({
		url: URL+"phpComunes/pdo.b.tarjeta.nom.php",
		type: "POST",
		data: {v0:bono},
		dataType: "json",
		async: false,
		success: function(datos){
			if(datos==0){
				alert("Lo lamento no encontre el registro!")
				return false;
			}
			$("#tdNombre").html(datos[0].NOMBRE);
			$("#tdEstado").html(datos[0].ESTADO);
			$("#tdSaldo").html(datos[0].SALDO);
		},
		complete: function(){
			$.ajax({
				url:"pdo.buscar.consumos.php",
				type: "POST",
				data: {v0:bono},
				dataType: "json",
				success: function(dato){
					if(dato==0){
						alert("No hay transacciones para esta tarjeta");
						return false;
					}
					$.each(dato,function(i,fila){
						$("#tConsumos tbody").append("<tr><td>"+fila.FECHATRANSACCION+"</td><td>"+fila.HORATRANSACCION+"</td><td>"+fila.VALOR+"</td><td>"+fila.NUMDISPOSITIVO+"</td><td>"+fila.DESCRIPCION+"</td></tr>");
					});
				}
			})
		}
	})
	}


function limpiarCampos(){
	$("table.tablero input:text").val('');
	$("table.tablero select").val(0);
	$("#lnombre,#lbono,#lfechasol,#lsaldo").html(" ");
	$("#lestado").html("");
	$("#tNumero").css("display", "none"); 
	$("#tBono").css("display", "none"); 
	$("#numero").val("");
	$("#buscarPor").focus();
}

