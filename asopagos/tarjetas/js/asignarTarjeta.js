var URL=src();
var nuevo=0;
var existe=0;
var idpersona=0;

$(document).ready(function(){
//$("#fecha").datepicker();

//Dialog Colaboracion en linea
  $("#dialog-form2").dialog({
		autoOpen: false,
		height: 400,
		width: 500,
		draggable:false,
		modal: true,
		buttons: {
		    'Enviar': function() {
			var bValid = true;
			var campo=$('#notas').val();
			var campo0=$.trim(campo);
			if (campo0==""){
				$(this).dialog('close');
					return false;
				}
			var campo1=$('#usuario').val();
			var campo2="empresaSinConv.php";
			$.post(URL+'phpComunes/colaboracion.php',{v0:campo0,v1:campo1,v2:campo2},
			function(datos){
				if(datos=='1'){
					alert("Su comentario fue enviado correctamente!. Gracias por participar en el mejoramiento de SIGAS");
				}else{
					alert(datos);
				}
		});
		$(this).dialog('close');
		},
		Cancelar: function() {
			$(this).dialog('close');
			$("#dialog-form2").dialog("close");
		}
	}
  });
   //Dialog ayuda
  $("#ayuda").dialog({
		 	      autoOpen: false,
			      height: 450,
			      width: 700,
			      modal:true,
			      open: function(evt, ui){
					$('#ayuda').html('');
					/*$.get(URL +'help/promotoria/xxx.html',function(data){
							$('#ayuda').html(data);
					});*/
			      }
});
});//ready
function nuevoT(){
	nuevo=1;
	limpiarCampos();
}
//---------------------------------------GUARDAR TARJETA
function guardarT(){
   $("table.tablero :input.ui-state-error,select.ui-state-error").removeClass("ui-state-error");//Limpiar la clase de error
   var idper=0;
   var estadoTercero;
   if(nuevo==0){
   MENSAJE("Si es un registro nuevo haga click en NUEVO.");
   return false;
   }
   var error=0;
   var email=/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test($("#email").val());
   
   if($("#identificacion").val()=='')	{$("#identificacion").addClass("ui-state-error");error++;}
   if($("#pa").val()=='')				{$("#pa").addClass("ui-state-error");error++;}
   if($("#pn").val()=='')				{$("#pn").addClass("ui-state-error");error++;}
   if($("#sexo").val()=='0')			{$("#sexo").addClass("ui-state-error");error++;}
   if($("#cboDepto").val()=='0')		{$("#cboDepto").addClass("ui-state-error");error++;}
   if($("#cboCiudad").val()=='0')		{$("#cboCiudad").addClass("ui-state-error");error++;}
   if($("#txttxtDireccion").val()=='')	{$("#txttxtDireccion").addClass("ui-state-error");error++;}
   if($("#telefono").val()=='')			{$("#telefono").addClass("ui-state-error");error++;}
   if($("#fecNac").val()=='')			{$("#fecNac").addClass("ui-state-error");error++;}
   if($("#negocio").val()=='0')			{$("#negocio").addClass("ui-state-error");error++;}
   if($("#tipoT").val()=='0')			{$("#tipoT").addClass("ui-state-error");error++;}
   var e=$.trim($("#email").val());
   if(e.length>0){if(!email) {$("#email").addClass("ui-state-error");error++;}}
   if(error>0){
   		MENSAJE("Llene los campos obligados!")
   return false; 
   }
   var nomcorto=nombre_corto($("#pn").val(),$("#sn").val(),$("#pa").val(),$("#sa").val());
   $("#realce").val(nomcorto);
   
    //Insertar ó Actualizar persona
   if(existe==0){
	 datos= {
			v1:$("#tipoDoc").val(),
			v2:$("#identificacion").val(),
			v3:$("#pa").val(),
			v4:$("#sa").val(),
			v5:$("#pn").val(),
			v6:$("#sn").val(),
			v7:$("#sexo").val(),
			v8:$("#txtDireccion").val(),
			v9:$("#telefono").val(),
			v10:$("#celular").val(),
			v11:$("#email").val(),
			v12:$("#cboDepto").val(),
			v13:$("#cboCiudad").val(),
			v14:$("#fecNac").val(),
			v15:$("#profesion").val(),
			v16:$("#realce").val()
			};  
	   
   idper=persona('I',datos);
   }else{
	datos={
			v0:idpersona,
			v1:$("#tipoDoc").val(),
			v2:$("#identificacion").val(),
			v3:$("#pa").val(),
			v4:$("#sa").val(),
			v5:$("#pn").val(),
			v6:$("#sn").val(),
			v7:$("#sexo").val(),
			v8:$("#txtDireccion").val(),
			v9:$("#telefono").val(),
			v10:$("#celular").val(),
			v11:$("#email").val(),
			v12:$("#cboDepto").val(),
			v13:$("#cboCiudad").val(),
			v14:$("#fecNac").val(),
			v15:$("#profesion").val(),
			v16:$("#realce").val()
			};   
   idper=actPersona(datos); 
   }
  
  
  if($("#negocio").val()=='H'){
  estadoTercero=guardarTerceros();
  	if(estadoTercero==1){
   	alert("Se guarda tercero correctamente en Informa Web !");
	}else{
	alert("Error al guardar terceros en Informa Web.");
	return false;
	}	  
  } 
  guardarTarjeta(idper);
}

function actPersona(datos){
	$.ajax({
		url:"pdo.actualizarPersona.php",
		type: "POST",
		data: datos,
		async: false,
		success: function(data){
			if(data==0){
				alert("No se pudo actualizar la persona!");
				return false
			}
			alert("Persona actualizada!");
			return true;
		}
		});//ajax*/
	}
	
//---------------------------------------GUARDAR/ACTUALIZAR PERSONA
function persona(datos){
	var idPersonaIA;
	var ruta=URL+'aportes/trabajadores/insertPersona.php';
	$.ajax({
		url:ruta ,
		type: "POST",
		data: datos,
		async: false,
		success: function(data){
			if(data==0){
				alert("No se pudo grabar la persona!");
				return false
			}
			if(data>0){
				alert("Se grabo la persona ID "+data);
				$("#idPersona").val(data);
				idpersona=data;
				return true;
			}	
			alert("No se pudo grabar la persona. Error: "+data);
			return false;
		}
		});//ajax*/
}
//---------------------------------------GUARDAR TARJETA
function guardarTarjeta(idp){
	var tipo=$("#tipoT").val();
	var negocio=$("#negocio").val();
	var ced=$("#identificacion").val();
	$.ajax({
		url: "pdo.asignarTarjeta.php",
		type: "POST",
		async: false,
		data: {v1:idpersona,v2:negocio,v3:tipo,v4:ced},
		success: function(datos){
		if(datos==0){
			alert("No se pudo ASIGNAR la tarjeta!");
			return false;
		}
		if(datos == 1)
		{
			alert("No hay tarjetas asignadas a esta Agencia!");
			return false;
		}	
		if(datos == 2)
		{
			alert("No hay existencia de Plasticos!");
			return false;
		}	
		if(datos == 3){
			alert("Este formulario es solo para asignar tarjeta por primera vez, utilice la REEXPEDICION de tarjetas!");
			nuevo=0;
			limpiarCampos();	
			return false;			
		}
		if(datos > 3){
			alert("La tarjeta fue Asignada!");
			nuevo=0;
			limpiarCampos();				
		}
		else
		{
		  alert(datos);	
		}
		}
	});//ajax
}
//---------------------------------------GUARDAR TERCEROS
function guardarTerceros(){
		var td;	
		var nombreTercero=$("#pn").val()+" "+$("#sn").val()+" "+$("#pa").val()+" "+$("#sa").val();
		var ok;	 
		//DATOS PERSONALES
		switch($("#tipoDoc").val()){
		case "2": td='T' ;break;//t. identidad
		case "4": td='E' ;break;//extranjeria
		case "5": td='N' ;break;//nit
		default:  td='C' ;break;//cedula
		}	
			
		datosTerceros ={	
			 v1:td,
			 v2:$("#identificacion").val(),
			 v3:nombreTercero,
			 v4:$("#sexo").val(),
			 v5:$("#txtDireccion").val(),
			 v6:$("#telefono").val(),
			v7:$("#celular").val(),
			v8:$("#email").val(),
			v9:$("#cboDepto").val(),
			v10:$("#cboCiudad").val(),
			v11:$("#fecNacHide").val(),
			v12:$("#profesion").val(),
			
			//DATOS TERCEROS
			v13:$("#pais").val(),	
			v14:$("#estado").val(),
			v15:$("#ncomercial").val(),	
			v16:$("#puntos").val(),		
			v17:$("#tipoCliente").val(),	
			v18:$("#tipoImpuesto").val(),
			v19:$("#iva").val(),	
			v20:$("#retieneRenta").val(),	
			v21:$("#responsableIva").val(),		
			v22:$("#declaracionRenta").val(),	
		};
			
			$.ajax({
				url:URL+"asopagos/tarjetas/guardarTercerosInforma.php",
				dataType:"POST",
				data:datosTerceros,
				async:false,
				success:function(data){
					if(data==0){
					ok=0;
					
					}else{
					ok=1;
					}
				}
			});	
			
			return ok;

}
//---------------------------------------BUSCAR AFILIADO-PERSONA
function buscarPersona(td,numero){
	if(numero.length==0) return false;
	$("table.tablero input:text").not("#identificacion,#pais,#iva").val('');
	$("#guardar").show();
	  $.ajax({
		  url:URL+'phpComunes/buscarPersona2.php',
		  type:"POST",
		  dataType:"json",
		  async:false,
		  data:{v1:numero,v0:td},
		  success:function(datos){
			if(datos==0){
			alert  ("La persona no existe en nuestra base de datos!");
			$("#identificacion").val('');
			$("#identificacion").focus();
			existe=0;
			return false;
			}else{
			existe=1;
			}
			
			$.each(datos,function(i,fila){
				idpersona   =fila.idpersona;
				pn			=$.trim(fila.pnombre);
				sn			=$.trim(fila.snombre);
				pa			=$.trim(fila.papellido);
				sa			=$.trim(fila.sapellido);
				sexo		=(fila.sexo);
				idp			=(fila.idpersona);
				idciures	=(fila.idciuresidencia);
				iddepres	=(fila.iddepresidencia);
				txtDireccion=$.trim(fila.direccion);
				telefono	=$.trim(fila.telefono);
				celular		=$.trim(fila.celular);
				email		=$.trim(fila.email);
				profesion	=(fila.idprofesion);
				fecnac		=(fila.fechanacimiento);
				nr			=$.trim(fila.nombrecorto);
			});//each
			$("#pn").val(pn);
			$("#sn").val(sn);
			$("#pa").val(pa);
			$("#sa").val(sa);
			$("#realce").val(nr);
			$("#sexo").val(sexo);
			$("#idPersona").val(idp);
			
			$("#cboDepto").val(iddepres).trigger("change");
			setTimeout((function(){
			$("#cboCiudad").val(idciures);
		    $("#cboCiudad").trigger("change");
			 }),1000);
			
			$("#txtDireccion").val(txtDireccion);
			$("#telefono").val(telefono);
			$("#celular").val(celular);
			$("#email").val(email);
			
			$("#profesion").val(profesion);
			$("#fecNac").val(fecnac);
		  },
		 complete:function(){
			if(existe==1){
			//Buscar tarjetas activas con ese idpersona
			$.ajax({
			url:'buscarTarjetasActivas.php',
			type:"POST",
			dataType:"json",
			data:{v0:idp},
			success:function(data){
				if(data!=0){
				MENSAJE("Este formulario es solo para asignar tarjeta por primera vez, utilice la REEXPEDICION de tarjetas! La persona posee una tarjeta <b>ACTIVA O BLOQUEADA</b> con No de bono: <b>"+data[0].bono+"</b>");
				limpiarCampos();
				}else{
					MENSAJE("La persona <b>NO</b> posee tarjetas activas.");
					existe=2;
				}
			},
			complete: function(){
				if(existe==2){
					var num=$("#identificacion").val();
					$.ajax({
						url:"pdo.buscarSaldo.php",
						type:"POST",
						dataType:"json",
						data:{v0:num},
						success: function(datos){
							if(datos==0){
								$("#saldo").val(0);
							}
							else{
								$("#saldo").val(datos);
							}
						}
					})
				}
			}
			});
			}//complete
		} 		 
	  });//ajax
}
//----------------------------------------VENTANA DE DIALOGO DE AYUDA
function mostrarAyuda(){
	$("#ayuda").dialog('open');
}
//----------------------------------------VENTANA DE DIALOGO DE MANUAL
function notas(){
	$("#dialog-form2").dialog('open');
}

function limpiarCampos(){
	$("table.tablero input:text").not("#pais,#iva").val('');
	$("table.tablero select").val(0);
	$("#tipoDoc").focus();
	$("#idPersona").val('');
	$("#mensaje small").empty();
	$("#mensaje").hide();
	$("table.tablero :input.ui-state-error,select.ui-state-error").removeClass("ui-state-error");//Limpiar la clase de error
	$("#guardar").show();
	$("#div-hiper").hide();
}

function validarTipo(valor){
$("#tipoT").val(0).attr("disabled",false);	
$("#div-hiper").hide();
if(valor=='H'){
$("#tipoT").val('49').attr("disabled",true);
$("#div-hiper").show();
}
}

function validarNumero(e){
	//cc 3,4,5,6,8,7,10
	//rg alfa 
	var x=$("#identificacion").val().length;
	tecla = (document.all) ? e.keyCode : e.which; 
    if ((tecla==8) || (tecla==0)) return true; 
	if(x>9) return false;
    patron =/[0-9]/; 
    te = String.fromCharCode(tecla); 
    if(patron.test(te)) {
		return te;} 
	else {
		return false;
		}
	
	}