<?php
// @autor:		Orlando Puentes A
// @fecha:		mayo 31/2010
// @formulario:	guardar datos persona - modulo empresa
ini_set("display_errors",'1');
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'ean13.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

$campo1 = $_REQUEST['v1'];		//idpersona
$campo2 = $_REQUEST['v2'];		//negocio
$campo3 = $_REQUEST['v3'];		//tipo
$campo4 = $_REQUEST['v4'];		//cedtra

$campo6 = $_SESSION['USUARIO'];
$agencia=$_SESSION['AGENCIA'];
$secc=$_SESSION['AGENCIA'];
//bono, idpersona, precodigo, codigobarra, programa, tipo, usuario

$sql="SELECT COUNT(*) AS cuenta FROM aportes101 WHERE idpersona=$campo1 AND estado in ('A','B')";
$rs=$db->querySimple($sql);
$row=$rs->fetch();
$cuenta=$row['cuenta'];
if($cuenta > 0){
    exit ('3');
}

/*
if( $agencia=='04' ){
	$agencia='01';
}
*/

$sql="Select count(*) as cuenta from aportes100 where asignada='N' AND plastico='S' and agencia='$agencia' and estado='A'";
$rs=$db->querySimple($sql);
$row=$rs->fetch();
$cuenta=$row['cuenta'];

if($cuenta==0){
    exit ('1');
}
$sql="SELECT top 1 idbono,bono,codigobarra FROM aportes100 WHERE asignada='N' AND plastico='S' and agencia='$agencia' and estado='A'";
$rs=$db->querySimple($sql);
$row=$rs->fetch();
$bono=$row['bono'];
$idbono=$row['idbono'];
$barra=$row['codigobarra'];

$sql="Select valor from aportes121 where procesado='N' and cedtra='$campo4'";
$rs=$db->querySimple($sql);
$saldo=0;
while ($row=$rs->fetch()){
	$saldo=$row['valor'];
}

$sql="SELECT TOP 1 periodo FROM aportes012 WHERE procesado='S' ORDER BY periodo desc";
$rs=$db->querySimple($sql);
$row=$rs->fetch();
$periodo=$row['periodo'];

$sql="Select idtarjeta from aportes101 where estado <> 'I' and idpersona=$campo1";
$rs=$db->querySimple($sql);
	
$db->inicioTransaccion();

while($row=$rs->fetch()){
	$idbono2=$row['idtarjeta'];
	$sql="Update aportes101 set estado='I' where idtarjeta=$idbono2";
	$db->queryActualiza($sql);
	}

$sql="Update aportes100 set asignada='S',idpersona=$campo1 where idbono=$idbono";
$rs=$db->queryActualiza($sql);
if($rs<1){
	$db->cancelarTransaccion();
	exit(3);
}

$sql="INSERT INTO aportes101(bono, idpersona, precodigo, codigobarra, programa, tipo, fechaexpedicion, entregada, estado, ubicacion, idetapa, saldo, usuario, fechasistema,fechavence,fechaetapa,fechasolicitud) VALUES('$bono',$campo1,'0','$barra','$campo2','$campo3',cast(getdate() as date),'N','A','$secc',61, 0,'$campo6',cast(getdate() as date),'2999/12/31',cast(getdate() as date),cast(getdate() as date))";
$rs=$db->queryInsert($sql,'aportes101');
$idtarjeta=$rs;
if(is_array($rs) || (is_null($rs))){
	$db->cancelarTransaccion();
	$cadena=$db->error;
	echo msg_error($cadena);
	exit;
	}
if($saldo > 100){
	$sql="INSERT INTO aportes104 (periodogiro, tipopago, idtrabajador, idpersona, idconyuge, valor, procesado, fechasistema, usuario,flag) VALUES ('$periodo', 'T', $campo1, 0, 0, $saldo, 'N', cast(getdate() as date), '$campo6','1')";
	$rs=$db->queryInsert($sql,'aportes104');
	$x=intval($rs);
	if($x < 1){
		$db->cancelarTransaccion();
		$cadena=$db->error;
		echo msg_error($cadena);
		exit;
	}
	$sql="update aportes121 set procesado='S',usuario='$campo6', fechasistema= CAST(GETDATE() AS DATE) WHERE cedtra='$campo4'";
	$rs=$db->queryActualiza($sql);
	if($rs==0){
		echo "No se actualizo aportes121";
		$db->cancelarTransaccion();
		exit();
		}
		
		$sql="update aportes998 set procesado='S' WHERE cedtra='$campo4'";
		$rs98=$db->queryActualiza($sql);
		if($rs98==0){
			echo "No se actualizo aportes121";
			$db->cancelarTransaccion();
			exit();
		}
}

$sql="INSERT INTO aportes108 (idtarjeta, procesado, fechasistema, usuario) VALUES ($idtarjeta, 'N', CAST(GETDATE() AS DATE), '$campo6')";
$rs=$db->queryInsert($sql,'aportes108');

$db->confirmarTransaccion();	

echo 10;	
?>