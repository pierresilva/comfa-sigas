<?php
/* autor:       orlando puentes
 * fecha:       14/10/2010
 * objetivo:    
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'rsc' . DIRECTORY_SEPARATOR . 'conexion.class.php';
//include_once("rsc/conexion.class.php");

class Ruaf{
	//constructor
	var $con;
	var $fechaSistema;
	function Ruaf(){
		$this->con=new DBManager;
	}

	
function insertar_movimiento($campos){
	if($this->con->conectar()==true){
 		$sql="INSERT INTO aportes400 (codigo,tipoidentificacion,numero,papellido,sapellido, pnombre, snombre, fechanacimiento, sexo,codigoerror,descripcion) VALUES('".$campos[0]."','".$campos[1]."','".$campos[2]."','".$campos[3]."','".$campos[4]."','".$campos[5]."','".$campos[6]."','".$campos[7]."','".$campos[8]."','".$campos[9]."','".$campos[10]."')";
		//echo $sql;
		return mssql_query($sql,$this->con->conect);
	}
}

function insertar_movimiento_Per($campos){
	if($this->con->conectar()==true){
 		$sql="INSERT INTO aportes402 (codigo,tipoidentificacion,numero,papellido,sapellido, pnombre, snombre, fechanacimiento, sexo,codigoerror,descripcion) VALUES('".$campos[0]."','".$campos[1]."','".$campos[2]."','".$campos[3]."','".$campos[4]."','".$campos[5]."','".$campos[6]."','".$campos[7]."','".$campos[8]."','".$campos[9]."','".$campos[10]."')";
		//echo $sql;
		return mssql_query($sql,$this->con->conect);
	}
}

function insertar_info_Per($campos){
	if($this->con->conectar()==true){
 		$sql="INSERT INTO aportes403 (idtipodocumento, identificacion, papellido, sapellido, pnombre, snombre, sexo, fechanace) VALUES('".$campos[0]."','".$campos[1]."','".$campos[2]."','".$campos[3]."','".$campos[4]."','".$campos[5]."','".$campos[6]."','".$campos[7]."')";
		//echo $sql;
		return mssql_query($sql,$this->con->conect);
	}
}


function seleccionar_verificar_Per(){
	if($this->con->conectar()==true){
 		$sql="SELECT a.tipoidentificacion as tipoida, a.numero as numida, 
			case when
			b.idtipodocumento = 1 then 'CC'
			when
			b.idtipodocumento = 2 then 'TI'
			when
			b.idtipodocumento = 3 then 'PA'
			when
			b.idtipodocumento = 4 then 'CE'
			when
			b.idtipodocumento = 5 then 'NI'
			when
			b.idtipodocumento = 6 then 'RC'
			when
			b.idtipodocumento = 7 then 'MS'
			when
			b.idtipodocumento = 8 then 'AS'
			end as tipoidb, b.identificacion as numidb, b.papellido as ape1b, b.sapellido as ape2b, 
			b.pnombre as nom1b, b.snombre as nom2b, b.sexo as sexob, b.fechanacimiento as fecnacib 
			FROM aportes402 as a inner join aportes015 as b on a.numero=b.identificacion";
		//echo $sql;
		return mssql_query($sql,$this->con->conect);
	}
}


function limpiar_movimiento_Per(){
		if($this->con->conectar()==true){
			$sql="DELETE FROM aportes402";		
			return mssql_query($sql,$this->con->conect);
		}
}

function limpiar_movimiento(){
		if($this->con->conectar()==true){
			$sql="DELETE FROM aportes400";		
			return mssql_query($sql,$this->con->conect);
		}
}

function limpiar_403(){
		if($this->con->conectar()==true){
			$sql="DELETE FROM aportes403";		
			return mssql_query($sql,$this->con->conect);
		}
}



function insert_control($campos){
		date_default_timezone_set('America/Bogota');
		$hora=date("h:i:s A");
		//echo $campos[0];
		//echo $campos[1];
		if($this->con->conectar()==true){
			$sql="INSERT INTO aportes149 (archivo, tipoarchivo, fechasistema, usuario, registros, totalregistros, registros1, totalregistros1, registros2, totalregistros2, registros3, totalregistros3, registros4, registros5, fechamovimiento,hora) VALUES ('".$campos[0]."','".$campos[1]."',cast(getdate() as date),'".$campos[3]."','".$campos[4]."','".$campos[5]."','".$campos[6]."','".$campos[7]."','".$campos[8]."','".$campos[9]."','".$campos[10]."','".$campos[11]."','".$campos[12]."','".$campos[13]."','".$campos[14]."','$hora')";
			//echo $sql;
			return mssql_query($sql,$this->con->conect);
		}
}


/*function procesar_archivo_1(){
	if($this->con->conectar()==true){
        $sql="SELECT aportes015.idciuresidencia as ciudad, aportes015.iddepresidencia as depto, aportes400.sexo as sexo, aportes400.fechanacimiento as fecnaci, aportes400.codigoerror as error, aportes400.tipoidentificacion as id0, aportes400.numero as numid0, aportes400.papellido as ape10, aportes400.sapellido as ape20, aportes400.pnombre as nom10, aportes400.snombre as nom20, aportes401.idtipodocumento as id1, aportes401.identificacion as numid1, aportes401.papellido as ape11, aportes401.sapellido as ape21, aportes401.pnombre as nom11, aportes401.snombre as nom21 FROM aportes400 right join aportes401 on aportes400.numero=aportes401.identificacion inner join aportes015 on aportes400.numero=aportes015.identificacion where aportes015.idciuresidencia is not null or aportes015.iddepresidencia is not null group by aportes015.idciuresidencia, aportes015.iddepresidencia, aportes400.sexo, aportes400.fechanacimiento, aportes400.codigoerror, aportes400.tipoidentificacion, aportes400.numero, aportes400.papellido, aportes400.sapellido, aportes400.pnombre, aportes400.snombre, aportes401.idtipodocumento, aportes401.identificacion, aportes401.papellido, aportes401.sapellido, aportes401.pnombre, aportes401.snombre";
        return mssql_query($sql,$this->con->conect);
        }
	}*/
	
	
function procesar_archivo_1(){
	if($this->con->conectar()==true){
        $sql="SELECT aportes015.idciuresidencia as ciudad, aportes015.iddepresidencia as depto, aportes400.sexo as sexo, 
aportes400.fechanacimiento as fecnaci, aportes400.codigoerror as error, aportes400.tipoidentificacion as id0, 
aportes400.numero as numid0, aportes400.papellido as ape10, aportes400.sapellido as ape20, aportes400.pnombre as nom10, 
aportes400.snombre as nom20, aportes401.idtipodocumento as id1, aportes401.identificacion as numid1, aportes401.papellido as ape11, 
aportes401.sapellido as ape21, aportes401.pnombre as nom11, aportes401.snombre as nom21 
FROM aportes401 
inner join aportes400 on aportes401.identificacion= aportes400.numero and aportes401.idtipodocumento=aportes400.tipoidentificacion
inner join aportes015 on aportes401.identificacion= aportes015.identificacion
where aportes401.idtipodocumento='TI' OR aportes401.idtipodocumento='CC' OR aportes401.idtipodocumento='CE' OR aportes401.idtipodocumento='PA'";
        return mssql_query($sql,$this->con->conect);
        }
	}	


function procesar_archivo_2(){
	if($this->con->conectar()==true){
        $sql="SELECT aportes015.idciuresidencia as ciudad, aportes015.iddepresidencia as depto, aportes402.sexo as sexo, 
aportes402.fechanacimiento as fecnaci, aportes402.codigoerror as error, aportes402.tipoidentificacion as id0, 
aportes402.numero as numid0, aportes402.papellido as ape10, aportes402.sapellido as ape20, aportes402.pnombre as nom10, 
aportes402.snombre as nom20, aportes403.idtipodocumento as id1, aportes403.identificacion as numid1, aportes403.papellido as ape11, 
aportes403.sapellido as ape21, aportes403.pnombre as nom11, aportes403.snombre as nom21 
FROM aportes403
inner join aportes402 on aportes403.identificacion= aportes402.numero and aportes403.idtipodocumento=aportes402.tipoidentificacion
inner join aportes015 on aportes403.identificacion= aportes015.identificacion
where aportes403.idtipodocumento='TI' OR aportes403.idtipodocumento='CC' OR aportes403.idtipodocumento='CE' OR aportes403.idtipodocumento='PA'";
        return mssql_query($sql,$this->con->conect);
        }
	}	
	

/*function contar_archivos(){
    if($this->con->conectar()==true){
        $sql="SELECT aportes015.idciuresidencia as ciudad, aportes015.iddepresidencia as depto, aportes400.sexo as sexo, aportes400.fechanacimiento as fecnaci, aportes400.codigoerror as error, aportes400.tipoidentificacion as id0, aportes400.numero as numid0, aportes400.papellido as ape10, aportes400.sapellido as ape20, aportes400.pnombre as nom10, aportes400.snombre as nom20, aportes401.idtipodocumento as id1, aportes401.identificacion as numid1, aportes401.papellido as ape11, aportes401.sapellido as ape21, aportes401.pnombre as nom11, aportes401.snombre as nom21 FROM aportes400 right join aportes401 on aportes400.numero=aportes401.identificacion inner join aportes015 on aportes400.numero=aportes015.identificacion where aportes015.idciuresidencia is not null or aportes015.iddepresidencia is not null group by aportes015.idciuresidencia, aportes015.iddepresidencia, aportes400.sexo, aportes400.fechanacimiento, aportes400.codigoerror, aportes400.tipoidentificacion, aportes400.numero, aportes400.papellido, aportes400.sapellido, aportes400.pnombre, aportes400.snombre, aportes401.idtipodocumento, aportes401.identificacion, aportes401.papellido, aportes401.sapellido, aportes401.pnombre, aportes401.snombre";
        return mssql_query($sql,$this->con->conect);
    }
}*/

function contar_archivos(){
    if($this->con->conectar()==true){
        $sql="SELECT aportes015.idciuresidencia as ciudad, aportes015.iddepresidencia as depto, aportes400.sexo as sexo, 
aportes400.fechanacimiento as fecnaci, aportes400.codigoerror as error, aportes400.tipoidentificacion as id0, 
aportes400.numero as numid0, aportes400.papellido as ape10, aportes400.sapellido as ape20, aportes400.pnombre as nom10, 
aportes400.snombre as nom20, aportes401.idtipodocumento as id1, aportes401.identificacion as numid1, aportes401.papellido as ape11, 
aportes401.sapellido as ape21, aportes401.pnombre as nom11, aportes401.snombre as nom21 
FROM aportes401 
inner join aportes400 on aportes401.identificacion= aportes400.numero and aportes401.idtipodocumento=aportes400.tipoidentificacion
inner join aportes015 on aportes401.identificacion= aportes015.identificacion
where aportes401.idtipodocumento='TI' OR aportes401.idtipodocumento='CC' OR aportes401.idtipodocumento='CE' OR aportes401.idtipodocumento='PA'";
        return mssql_query($sql,$this->con->conect);
    }
}

function contar_archivos_per(){
    if($this->con->conectar()==true){
        $sql="SELECT aportes015.idciuresidencia as ciudad, aportes015.iddepresidencia as depto, aportes402.sexo as sexo, 
aportes402.fechanacimiento as fecnaci, aportes402.codigoerror as error, aportes402.tipoidentificacion as id0, 
aportes402.numero as numid0, aportes402.papellido as ape10, aportes402.sapellido as ape20, aportes402.pnombre as nom10, 
aportes402.snombre as nom20, aportes403.idtipodocumento as id1, aportes403.identificacion as numid1, aportes403.papellido as ape11, 
aportes403.sapellido as ape21, aportes403.pnombre as nom11, aportes403.snombre as nom21 
FROM aportes403
inner join aportes402 on aportes403.identificacion= aportes402.numero and aportes403.idtipodocumento=aportes402.tipoidentificacion
inner join aportes015 on aportes403.identificacion= aportes015.identificacion
where aportes403.idtipodocumento='TI' OR aportes403.idtipodocumento='CC' OR aportes403.idtipodocumento='CE' OR aportes403.idtipodocumento='PA'";
        return mssql_query($sql,$this->con->conect);
    }
}

/*function actualizar_registro($numiden){
	if($this->con->conectar()==true){
 		$sql="UPDATE aportes401 SET enviado='S' WHERE identificacion='$numiden'";
		//echo $sql;
		return mssql_query($sql,$this->con->conect);
	}
}*/


}
?>
