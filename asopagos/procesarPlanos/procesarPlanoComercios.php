<?php
set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
$usuario=$_SESSION['USUARIO'];

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}
include_once $raiz . DIRECTORY_SEPARATOR .'config.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'session.php';
session();
include_once $raiz . DIRECTORY_SEPARATOR . 'funcionesComunes' . DIRECTORY_SEPARATOR . 'funcionesComunes.php';

$c0=$_REQUEST["v0"]; //Archivo .csv � .txt
$bolsillo=$_REQUEST["v1"];
//$fechaHoy=date("Y/m/d");
?>
<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es">
<head>
<title>Procesar archivos</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link type="text/css" rel="stylesheet" href="../../css/css.1.8/jquery-ui-1.8.17.custom.css" />
		<link type="text/css" href="../../css/Estilos.css" rel="stylesheet"/>
		<link href="../../css/marco.css" rel="stylesheet" type="text/css" />

		<script type="text/javascript" src="../../js/1.6/jquery-1.6.2.min.js"></script>
		<script type="text/javascript" src="../../js/1.6/jquery-ui-1.8.16.custom.min.js"></script>
		<script type="text/javascript" src="../../js/comunes.js"></script>
		<script language="javascript" src="js/subirPlanosComercios.js"></script>
</head>
<body>
<?php
//$dirPrint = $ruta_cargados ."asopagos".DIRECTORY_SEPARATOR."comercios".DIRECTORY_SEPARATOR."logs".DIRECTORY_SEPARATOR;
$directorio=$ruta_cargados."asopagos".DIRECTORY_SEPARATOR."comercios".DIRECTORY_SEPARATOR."cargados".DIRECTORY_SEPARATOR.$c0;
$directorio2=$ruta_cargados."asopagos".DIRECTORY_SEPARATOR."comercios".DIRECTORY_SEPARATOR."procesados".DIRECTORY_SEPARATOR;

$registros = array();

	if (($fichero = fopen($directorio, "r")) !== FALSE) {
		// Lee los nombres de los campos
		$nombres_campos = fgetcsv($fichero, 0, ";");
		$num_campos = count($nombres_campos);
		// Lee los registros
		while (($datos = fgetcsv($fichero, 0, ";")) !== FALSE) {
			// Crea un array asociativo con los nombres y valores de los campos
			for ($icampo = 0; $icampo < $num_campos; $icampo++) {
				$registro[$nombres_campos[$icampo]] = $datos[$icampo];
			}
			// A�ade el registro leido al array de registros
			$registros[] = $registro;
		}
		fclose($fichero);
		echo "Leidos " . count($registros) . " registros\n";
		echo "</br>";
		
	for($i=0; $i<count($c0); $i++){
		
		echo "<br>Procesando archivo: ".$c0;
		
		/////////////
		$consulta=$db->buscar_archivo($c0);
		$row = $consulta->fetch();
		$conr=$row['cuenta'];
		if($conr>0){
			echo "<br>El archivo: ".$c0." ya fue procesado!";
			unlink($directorio);
			continue;
		}
		////////////
		for ($i = 0; $i < count($registros); $i++) {
			$identificacion = $registros[$i]["identificacion"];
			//$tarjeta = $registros[$i]["tarjeta"];
			$valor = $registros[$i]["valor"];
			$fechatransaccion = $registros[$i]["fechatransaccion"];
			$referencia = $registros[$i]["referencia"];
			$numerodispositivo = $registros[$i]["numerodispositivo"];
			$establecimiento = $registros[$i]["establecimiento"];
			$autorizacion = $registros[$i]["autorizacion"];
			//$bolsillo = $registros[$i]["bolsillo"];
			
			
			$sql1="insert into aportes153 (identificacion,valor,fechatransaccion,referencia,numdispositivo,establecimiento,usuario,fechasistema,archivo,autorizacion, bolsillo)
			values ('$identificacion',$valor,'$fechatransaccion',$referencia,'$numerodispositivo','$establecimiento','$usuario',cast(getdate() as date),'$c0','$autorizacion','$bolsillo')";
			$rs1=$db->queryInsert($sql1,"aportes153");
			if($rs1 == 0) {
				echo 'No se pudo grabar el registro!';
			}
			
		} // fin for 
		
		$hora=date('h:i:s');
		$sql="insert into aportes149 (archivo,tipoarchivo,fechasistema,usuario,hora,registros) values('".$c0."',4448,cast(getdate() as date),'$usuario','".$hora."',".count($registros).")";
		$rs=$db->queryInsert($sql,'aportes149');
		if($rs == 0){
			echo "<br>No se pudo guardar el encabezado del Archivo procesado: ".$c0." pero los movimientos fueron cargados!";
		}
		if($numeroDuplicado>=1){
			echo "<script>alert('Existen registros ya conciliados')</script>";
			echo "<br>Archivo procesado: ".$c0;
			/* Manda a procesar los cruces */
			echo "<table><tr><td><input type='button' value='Procesar Cruces' onClick=procesar01('$c0'); ></td></tr></table>";
		}else{
			echo "<br>Archivo procesado: ".$c0;
			/* Manda a procesar los cruces */
			echo "<table><tr><td><input type='button' value='Procesar Cruces' onClick=procesar01('$c0'); ></td></tr></table>";
		}
		//$viejo="/var/www/html/planos_cargados/asopagos/consumos/cargados/".$c0;
		$viejo=$ruta_cargados."asopagos".DIRECTORY_SEPARATOR."comercios".DIRECTORY_SEPARATOR."cargados".DIRECTORY_SEPARATOR.$c0;
		$nuevo=$ruta_cargados."asopagos".DIRECTORY_SEPARATOR."comercios".DIRECTORY_SEPARATOR."procesados".DIRECTORY_SEPARATOR.$c0;
		//$nuevo="/var/www/html/planos_cargados/asopagos/consumos/procesados/".$c0;
		$nuevo=$directorio2.$c0;
		if(moverArchivo($viejo,$nuevo)==false){
			echo "<br>No se pudo mover el archivo: $viejo";
		}
		
	} //fin for 
	}// Fin if (($fichero = fopen
?>
	<input type="hidden" id="txtusuario" name="txtusuario" value="<?php echo $usuario; ?>" />
 </body>
 </html>