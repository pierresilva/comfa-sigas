<?php
/* autor:       orlando puentes
 * fecha:       octubre 4 de2010
 * objetivo:    Almacenar los movimientos diarios de las tarjetas.
 */
set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$usuario=$_SESSION['USUARIO'];
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}
include_once $raiz . DIRECTORY_SEPARATOR .'config.php'; 
include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'session.php';
session();
include_once $raiz . DIRECTORY_SEPARATOR . 'funcionesComunes' . DIRECTORY_SEPARATOR . 'funcionesComunes.php';
//require_once('funcionesComunes/funcionesComunes.php');
//$directorio = "/var/www/html/planos_cargados/asopagos/consumos/cargados/";
$directorio=$ruta_cargados."asopagos".DIRECTORY_SEPARATOR."consumos".DIRECTORY_SEPARATOR."cargados".DIRECTORY_SEPARATOR;
$directorio2=$ruta_cargados."asopagos".DIRECTORY_SEPARATOR."consumos".DIRECTORY_SEPARATOR."procesados".DIRECTORY_SEPARATOR;
$dir = opendir($directorio);
$flag=false;
while ($elemento = readdir($dir))
{
		if(strlen($elemento)>2){
			$cad=substr($elemento, 0,1);
			if($cad != 'T'){
				continue;
			}
			$archivos[]=$elemento;
			$flag=true;
		}
} 
closedir($dir);
//T636480128.TXT
$arcProc=$archivos[0];

if(!$flag){
	echo "NO hay archivos tipo consumo!!";
	exit();
}
$mesP=substr($arcProc,6,2);
$diaP=substr($arcProc,8,2);

for($i=0; $i<count($archivos); $i++){
	echo "<br>Procesando archivo: ".$archivos[$i];
	/////////////
	$consulta=$db->buscar_archivo($archivos[$i]);
	$row = $consulta->fetch();
	$conr=$row['cuenta'];
	if($conr>0){
		echo "<br>El archivo: ".$archivos[$i]." ya fue procesado!";
		unlink($directorio.$archivos[$i]);
		continue;
	}
	////////////
	$arcProc=$archivos[$i];
	$directorio.=$archivos[$i];

	$anoP=date ("Y");  
	$fechaSaldo=$anoP."/".$mesP."/".$diaP;
	$archivoS = file($directorio); 
	$lineas = count($archivoS);
	for($c=0; $c < $lineas; $c++){ 
		$tarjeta=trim(substr($archivoS[$c],9,19));
		$dispositivo=substr($archivoS[$c],62,2);
		$descripcion=substr($archivoS[$c],94,30);
		$valor=substr($archivoS[$c],124,17);	
		$valor=intval($valor)/100;	
		$anio=substr($archivoS[$c],158,4);
		$mes=substr($archivoS[$c],162,2);				
		$dia=substr($archivoS[$c],164,2);
		$codaut=substr($archivoS[$c],267,6);		
		$fechatransaccion=$mes."/".$dia."/".$anio;
		$h=substr($archivoS[$c],284,2);	
		$m=substr($archivoS[$c],286,2);
		$hora=$h.":".$m;				
		$referencia=substr($archivoS[$c],299,12);	
		$referencia=intval($referencia);	
		$numdispositivo=substr($archivoS[$c],315,16);		
		$establecimiento=substr($archivoS[$c],331,10);		
		$sql1="insert into aportes152 (tarjeta, dispositivo ,descripcion, valor, fechatransaccion, horatransaccion, referencia, numdispositivo, establecimiento, usuario, fechasistema, archivo,autorizacion) values ('$tarjeta', '$dispositivo','$descripcion', $valor, '$fechatransaccion', '$hora', $referencia, '$numdispositivo', '$establecimiento','$usuario',cast(getdate() as date),'$arcProc','$codaut')";
		$rs1=$db->queryInsert($sql1,"aportes152"); 
 	    if($rs1 == 0) {
			echo 'No se pudo grabar el registro: $referencia\r\n!';
			//exit();
		   }
		} 
	$hora=date('h:i:s');
	$sql="insert into aportes149 (archivo,tipoarchivo,fechasistema,usuario,registros,hora) values('".$arcProc."',2928,'".$fechaSaldo."','$usuario',".$lineas.",'".$hora."')";
	$rs=$db->queryInsert($sql,'aportes149');
	if($rs == 0){
		echo "<br>No se pudo guardar el control del Archivo procesado: ".$archivos[$i]." pero los movimientos fueron cargados!";
		
	}
	echo "<br>Archivo procesado: ".$archivos[$i];
	//$viejo="/var/www/html/planos_cargados/asopagos/consumos/cargados/".$archivos[$i];
	$viejo=$ruta_cargados."asopagos".DIRECTORY_SEPARATOR."consumos".DIRECTORY_SEPARATOR."cargados".DIRECTORY_SEPARATOR.$archivos[$i];	
	$nuevo=$ruta_cargados."asopagos".DIRECTORY_SEPARATOR."consumos".DIRECTORY_SEPARATOR."procesados".DIRECTORY_SEPARATOR.$archivos[$i];	
	//$nuevo="/var/www/html/planos_cargados/asopagos/consumos/procesados/".$archivos[$i];
	$nuevo=$directorio2.$archivos[$i];	
	if(moverArchivo($viejo,$nuevo)==false){
		echo "<br>No se pudo mover el archivo: $viejo";
	}
}
 ?>
