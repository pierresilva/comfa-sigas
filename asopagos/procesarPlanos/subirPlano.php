<?php

// @autor:		Orlando Puentes A
// @fecha:		mayo 31/2010
// @formulario:	guardar datos persona - modulo empresa
set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'config.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head> 
 <title>Procesar archivos</title>
 <link href="../../css/Estilos.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../js/jquery-1.4.2.js"></script>
<script language="javascript" src="js/subirPlanos.js"></script>
</head>
 <body>
 <p>&nbsp;</p>
 <fieldset>
 <br />
 <p align="center"><strong>Resultado de la Copia de Archivos</strong></p>
 <br />
<?php 
 
$directorio = $ruta_cargados."/asopagos/saldos/cargados/"; 
if(!is_dir($directorio)){
	$dirs = array();
	$path = preg_replace('/(\/){2,}|(\\\){1,}/','/',$directorio); //only forward-slash
	$dirs = explode("/", $path);
	$i=0;
	foreach ($dirs AS $element) {
		$path2 .= $element . "/";
		if(!is_dir($path2) && $i != 0) {
			if(!mkdir($path2, '0777',true)){
				//error 
				break;
			}
			chmod($path2, 0777);
		}  
		$i++;
		}  
	}
echo "<br><br><br><br>";
if(isset($_FILES['archivo'])){
	foreach ($_FILES['archivo']['error'] as $key => $error) {
	    $archivos[]= $_FILES["archivo"]["name"][$key];
		if(move_uploaded_file($_FILES["archivo"]["tmp_name"][$key],$directorio.$_FILES["archivo"]["name"][$key])){
		
		}
		else{ echo "Ocurrio un problema al intentar subir el archivo."; }
	   }
	   }
	
 else{
 	echo "<p class=Rojo>No se pudo cargar el archivo!</p>";
 	exit();
 }
 
for($i=0; $i<count($archivos); $i++){
	echo "<p align=center>Archivo copiado en el servidor: <strong>".$archivos[$i]."</strong></p>"; 
}

for($i=0; $i<count($archivos); $i++){

	$total = count(glob($directorio."{*}",GLOB_BRACE));
	if ($total>1){
		echo "<script language='JavaScript'>alert('Existe mas de un archivo para procesar por favor comunicarse con T.I');</script>"; exit();
	}

	echo "<p align=center id='msjCopia' style='display:none'>Archivo copiado en el servidor: <strong>".$archivos[$i]."</strong></p>";
}


if($i>0){
?>
<center>
    <div id="boton1">
    <p style="text-decoration:none; font-size:12px; color:#F00; font-weight:bold; cursor:pointer" onclick="procesar();">Procesar estos archivos</p>
    </div>
</center>
<center>
    <div id="procesar" style="display:none">
    <p>Procesando...</p>
    </div>
</center>
<?php
	}
 ?> 
 <div id="ajax" align="center"> </div>
<div align="center">..:..</div>
</fieldset>
</body>
</html>
 
