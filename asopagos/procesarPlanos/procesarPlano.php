<?php
/* autor:       orlando puentes
 * fecha:       octubre 4 de2010
 * objetivo:    Almacenar los movimientos diarios de las tarjetas.
 */
set_time_limit(0);
date_default_timezone_set("America/Bogota");
ini_set("display_error", "1");
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'config.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}
$usuario=$_SESSION['USUARIO'];
include_once $raiz . DIRECTORY_SEPARATOR . 'funcionesComunes' . DIRECTORY_SEPARATOR . 'funcionesComunes.php';
$directorio = $ruta_cargados."asopagos/saldos/cargados/";
$dir = opendir($directorio);
while ($elemento = readdir($dir))
{
		if(strlen($elemento)>2)
		$archivos[]=$elemento;
} 
closedir($dir);
$arcProc=$archivos[0];
$mesP=substr($arcProc,7,2);
$diaP=substr($arcProc,9,2);
$anoP=date ("Y");
$fechaISSO = $anoP.$mesP.$diaP;
for($i=0; $i<count($archivos); $i++){
	echo "<br>Procesando archivo: ".$archivos[$i];
	/////////////
	$consulta=$db->buscar_archivo($archivos[$i]);
	$row = $consulta->fetch();
	$conr=$row['cuenta'];
	if($conr>0){
		echo "<br>El archivo: ".$archivos[$i]." ya fue procesado!";
		unlink($archivos[$i]);
		continue;
	}
	////////////
	$directorio.=$archivos[$i];
	$fechaSaldo=$mesP."/".$diaP."/".$anoP;
	$archivoS = file($directorio); 
	$lineas = count($archivoS);
	for($c=0; $c < $lineas; $c++){ 
		$tarjeta=substr($archivoS[$c],24,19);
		$saldo=substr($archivoS[$c],73,17);
		$saldo=substr($saldo,0,-2);
		$saldo=(int)$saldo;
	    //$titular=substr($archivo[$i],46,27);
		$sql="update aportes101 set saldo=".$saldo.",fechasaldo='".$fechaSaldo."' where bono='".$tarjeta."'";
		$rs=$db->queryActualiza($sql);
		
		//LLEVAR EL CONTROL DE TRAZABILIDAD PARA LOS SALDOS QUE LLEGAN DEL ARCHIVO DE ASOPAGOS
		if($rs>=1){
			$sql="insert into aportes101_aux (bono,saldo,fechasaldo,usuario) values('".$tarjeta."',".$saldo.",'".$fechaSaldo."','$usuario')";
			$rs2=$db->queryInsert($sql,'aportes101_aux');
		}
		
		if($rs==0){
			$sql="update aportes102 set saldo=".$saldo.",fechasaldo='".$fechaSaldo."' where bono='".$tarjeta."'";
			$rs=$db->queryActualiza($sql);
			
			$sql="insert into aportes101_aux (bono,saldo,fechasaldo,usuario) values('".$tarjeta."',".$saldo.",'".$fechaSaldo."','$usuario')";
			$rs2=$db->queryInsert($sql,'aportes101_aux');
			
			}
	}

	//GUARDAR DATOS DE NO PROCESADOS APORTES104 PARA LLEVAR TRAZABILIDAD
	$sql="select idtrabajador,sum(valor) as valor from aportes104 where procesado='N' GROUP BY idtrabajador";
	$rs=$db->querySimple($sql);
	while ($row = $rs->fetch()){
		
		$sql="insert into aportes104_aux (idtrabajador,valor,fechasaldo,usuario) values(".$row['idtrabajador'].",".$row['valor'].",'".$fechaSaldo."','$usuario')";
		$rs3=$db->queryInsert($sql,'aportes104_aux');
	}
	
	
	$hora=date('h:i:s');
	$sql="insert into aportes149 (archivo,tipoarchivo,fechasistema,usuario,registros,hora) values('".$arcProc."',2927,'".$fechaSaldo."','$usuario',".$lineas.",'".$hora."')";
	$rs=$db->queryInsert($sql,'aportes149');
	echo "<br>Archivo procesado: ".$archivos[$i];
	$viejo=$ruta_cargados."asopagos/saldos/cargados/".$archivos[$i];
	$nuevo=$ruta_cargados."asopagos/saldos/procesados/".$archivos[$i];
	if(moverArchivo($viejo,$nuevo)==false){
		echo "<br>No se pudo mover el archivo: $viejo";
	}
}

$sql="SELECT COUNT(*) as cuenta FROM aportes041 WHERE fechasaldo='$fechaISSO'";
$rs=$db->querySimple($sql)->fetch(PDO::FETCH_OBJ)->cuenta;
if($rs == 0){
	$sql = "SELECT sum(cast(saldo as numeric(12,0))) as saldo FROM aportes101";
	$st = $db->querySimple($sql)->fetch(PDO::FETCH_OBJ)->saldo;
	$sql = "SELECT SUM(VALOR) as saldo FROM APORTES104 WHERE procesado='N'";
	$snt = $db->querySimple($sql)->fetch(PDO::FETCH_OBJ)->saldo;
	$sql = "SELECT SUM(VALOR) as saldo FROM APORTES121 WHERE procesado='N'";
	$sr = $db->querySimple($sql)->fetch(PDO::FETCH_OBJ)->saldo;
	$sql="SELECT SUM(CAST(saldo AS DECIMAL(10,0))) AS saldo FROM aportes101_aux where fechasaldo='".$fechaSaldo."' ";
	$sub=$db->querySimple($sql)->fetch(pdo::FETCH_OBJ)->saldo;
	$sql="SELECT SUM(CAST(saldo AS DECIMAL(10,0))) AS saldo FROM aportes101 where programa='F'";
	$fon=$db->querySimple($sql)->fetch(pdo::FETCH_OBJ)->saldo;
	$sql="SELECT sum(valor) AS saldo FROM aportes104 WHERE procesado='N' and tipopago='T'";
	$pt=$db->querySimple($sql)->fetch(pdo::FETCH_OBJ)->saldo;
	$sql="SELECT sum(valor) AS saldo FROM aportes104 WHERE procesado='N' and tipopago='F'";
	$pf=$db->querySimple($sql)->fetch(pdo::FETCH_OBJ)->saldo;
	$sql="SELECT sum(valor) AS saldo FROM aportes104 WHERE procesado='N' and tipopago='E'";
	$pe=$db->querySimple($sql)->fetch(pdo::FETCH_OBJ)->saldo;
	$sql = "INSERT INTO aportes041 (fechasaldo, entarjetas, sintarjetas, redeban, subsidio, fonede, saldo_e, saldo_f, saldo_t) VALUES (:fecha,:saldo1,:saldo2,:saldo3,:sub,:fon,:pe,:pf,:pt)";
	$rs = $db->conexionID->prepare($sql);
	try {
		if( $rs->execute(array('fecha' => $fechaISSO,'saldo1' => $st,'saldo2' => $snt,'saldo3' => $sr,'sub' => $sub,'fon' => $fon, 'pe' => $pe, 'pf' => 0, 'pt' => $pt)) ){
			echo "<br>";
			echo  $db->conexionID->lastInsertId('aportes041');
		}
	
	}
	catch (PDOException $e) {
  		echo $e->getMessage();
	}
	
}
 ?>
