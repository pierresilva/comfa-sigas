<?php
set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'config.php';
$fecha=date('Ymd');
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
include_once $raiz.DIRECTORY_SEPARATOR.'asopagos'.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'planos.class.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

$objPlanos = new Planos();

$nomArchivo = $_REQUEST['v0'];
$consecutivo = 0;
$sqlconsecutivo="SELECT max(consecutivo) AS consecutivo FROM aportes149";
$rsconsecutivo=$db->querySimple($sqlconsecutivo);

$rowconsecutivo=$rsconsecutivo->fetch();
$consecutivo = $rowconsecutivo['consecutivo'];

$consecutivo ++;
$nuevo_consecutivo = $consecutivo;

		$comercios=$objPlanos->consecutivoProcesarCruces($nomArchivo,$nuevo_consecutivo);
		echo (count($comercios)>0) ? json_encode($comercios) : 0;
?>		

