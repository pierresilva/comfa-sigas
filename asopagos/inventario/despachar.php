<?php
/**
 * Autor: Oswaldo Gonzalez
 * Fecha: 10-jul-2012
 */
set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}
include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';
global $arregloAgencias;
$usuario=$_SESSION['USUARIO'];

$agencia = $_REQUEST["agencia"];
$serieInicio = $_REQUEST["serieInicio"];
$serieFin = $_REQUEST["serieFin"];

$arregloDespachadas = array();
$arregloNoDespachadas = array();

$desde = str_replace("63648", "", $serieInicio);
$hasta = str_replace("63648", "", $serieFin);
$desde = intval(substr($desde, 0,-1));
$hasta = intval(substr($hasta,0,-1));
$total =$hasta-$desde;
$ini = substr($serieInicio,0,15);
$sql="Select idbono from aportes100 where bono like '$serieInicio%'";
$rs=$db->querySimple($sql);
if(!$rs){
	exit(9);
}
$w=$rs->fetch();
$id = intval($w['idbono']);
$con=0;
for($c = $id; $c<=($id+$total); $c++){
	$sql="Select count(*) as cuenta from aportes100 where asignada='N' and plastico='S' and estado='I' and idbono='$c'";
	$rs2=$db->querySimple($sql);
	$w2=$rs2->fetch();
	if($w2['cuenta']==1){
		$sql = "UPDATE aportes100 set estado='T', agencia='$agencia', fechaenvio=cast(getdate() as date) WHERE idbono=$c";
		$rs3=$db->queryActualiza($sql);
		if($rs3==1)
			$con++;
	}
}
$fecchaHoy = date("aaaammdd");
$sql="Select bono, codigobarra from aportes100 where estado='T' and agencia='$agencia' and fechaenvio=getdate()";


echo json_encode($con);
?>