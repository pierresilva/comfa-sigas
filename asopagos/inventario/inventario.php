<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$url=$_SERVER['PHP_SELF'];
include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'auditoria.php';
auditar($url);

include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Inventario</title>
<link type="text/css" href="<?php echo URL_PORTAL; ?>css/formularios/base/ui.all.css" rel="stylesheet" />
<link type="text/css" href="<?php echo URL_PORTAL; ?>css/Estilos.css" rel="stylesheet"/>
<link type="text/css" href="<?php echo URL_PORTAL; ?>css/estilo_tablas.css" rel="stylesheet"/>
<link type="text/css" href="<?php echo URL_PORTAL; ?>css/formularios/demos.css" />
<link href="<?php echo URL_PORTAL; ?>css/marco.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/jquery-1.3.2.min.js"></script>
<!-- <script language="javascript" src="<?php echo URL_PORTAL; ?>js/jquery-1.4.2.js"></script> -->

<script language="javascript" src="<?php echo URL_PORTAL; ?>js/jquery.MultiFile.js"></script>

<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/formularios/ui/ui.core.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/formularios/ui/ui.tabs.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/formularios/ui/ui.draggable.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/formularios/ui/ui.resizable.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/formularios/ui/ui.dialog.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/comunes.js"></script>

<script type="text/javascript">
shortcut.add("Shift+F",function() {
		                                var URL=src();
		var url=URL+"aportes/trabajadores/consultaTrabajador.php";
    	window.open(url,"_blank");
    },{
	'propagate' : true,
	'target' : document 
        });        
</script>

<script>

$(document).ready(function(){
var URL = src();
$("#ayuda").dialog({
		autoOpen: false,
		height: 450,
		width: 700,
		draggable:true,
		modal:false,
		open: function(evt, ui){
                    $('#ayuda').html('');
                    $.get(URL+'help/tarjetas/procesamiento.html',function(data){
                    $('#ayuda').html(data);
                    })
		 }
});
	
$("#dialog-form2").dialog({
		autoOpen: false,
		height: 400,
		width: 500,
		modal: true,
		buttons: {
			'Enviar': function() {
			var bValid = true;
			var campo=$('#notas').val();
			var campo0=$.trim(campo);
			if (campo0==""){
				$(this).dialog('close');
					return false;
				}
			var campo1=$('#usuario').val();
			var campo2="procesarMovimientos.php";
			$.post(URL+'phpComunes/colaboracion.php',{v0:campo0,v1:campo1,v2:campo2},
			function(datos){
				if(datos=='1'){
					alert("Su comentario fue enviado correctamente!. Gracias por participar en el mejoramiento de SIGAS");
				}else{
					alert(datos);
				}
		});
		$(this).dialog('close');
		},
		Cancelar: function() {
			$(this).dialog('close');
			//$("#dialog-form2").dialog("destroy");
		}
	}
});
	
});
function mostrarAyuda(){
	$("#ayuda").dialog('open' );
}

function notas(){
	$("#dialog-form2").dialog('open');
}	
</script>
</head>
<body>
<form action="procesarArchivo.php" id="forma" enctype="multipart/form-data" method="post">
<table border="0" align="center" cellpadding="0" cellspacing="0" width="80%">
  <tbody>
 
  <!-- ESTILOS SUPERIOR TABLA-->
  <tr>
    <td width="13" height="29" class="arriba_iz">&nbsp;</td>
    <td class="arriba_ce"><span class="letrablanca">::&nbsp;Inventario::</span></td>
    <td width="13" class="arriba_de" align="right">&nbsp;</td>
  </tr>
  
  <!-- ESTILOS ICONOS TABLA-->
  <tr>
   <td class="cuerpo_iz">&nbsp;</td>
	<td class="cuerpo_ce"><img src="<?php echo URL_PORTAL; ?>imagenes/spacer.gif" alt="" width="1" height="1" /><img src="<?php echo URL_PORTAL; ?>imagenes/menu/informacion.png" alt="Ayuda" width="16" height="16" style="border:none; cursor:pointer" title="Manual" onclick="mostrarAyuda();" />
   <img src="<?php echo URL_PORTAL; ?>imagenes/menu/notas.png" alt="Soporte" width="16" height="16" style="cursor: pointer" title="Colaboraci&oacute;n en l&iacute;nea" onclick="notas();" />
       </td>
    <td class="cuerpo_de">&nbsp;</td>
  </tr>
  
  <!-- ESTILOS MEDIO TABLA-->
  <tr>
    <td class="cuerpo_iz">&nbsp;</td>
    <td class="cuerpo_ce">
    <div align="center" class="muy_grande">PROCESAR TARJETAS NUEVAS</div><br />
   <p align="center"> Archivo a procesar:&nbsp;<input name="archivo[]" accept="txt|TXT|csv" type="file"/> <p/>
   <div id="boton" align="center"><input type="submit" value="Procesar Archivo" class="ui-state-default" /></div>
    </td>
    <td class="cuerpo_de">&nbsp;</td>
  </tr>
  
  <!-- CONTENIDO TABLA-->
  <tr>
    <td class="cuerpo_iz">&nbsp;</td>
    <td class="cuerpo_ce">    
    </td>
    <td class="cuerpo_de">&nbsp;</td>
  </tr>
  
  <!-- ESTILOS PIE TABLA-->
  <tr>
    <td class="abajo_iz" >&nbsp;</td>
    <td class="abajo_ce" ></td>
    <td class="abajo_de" >&nbsp;</td>
  </tr>
  
</tbody>
</table>
 </form> 
 
<!--Dialogo en linea--> 
<div id="div-observaciones-tab"></div> 
 <div id="dialog-form2" title="Colaboraci&oacute;n en l&iacute;nea">
  <p>&nbsp;</p>
  <p>Por favor diligencie este formulario para enviar comentarios, errores o falencias 
    encontradas en el proceso. M�ximo 250 caracteres </p>
<label>Tus comentarios:</label><br />
<textarea name="notas" id="notas" cols="60" rows="10"></textarea>
</div>

<!-- formulario direcciones  -->
<div id="dialog-form" title="Formulario de direcciones"></div>

<!-- Manual Ayuda -->
<div id="ayuda" title="::Manual Ayuda de Movimiento Diario de la Tarjeta::" style="background-image:url(<?php echo URL_PORTAL; ?>imagenes/FondoGeneral0.png)">
</div>
 
</body>
<script language="javascript">
function mensaje(){
document.getElementById('boton').innerHTML="";
document.getElementById('mensaje').innerHTML="Procesando por favor espere!";
}
</script>
</html>
