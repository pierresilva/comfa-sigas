<?php
// @autor:		Orlando Puentes A
// @fecha:		mayo 31/2010
// @formulario:	guardar datos persona - modulo empresa
?>
<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es">
<head> 
 <title>Procesar archivo</title>
 <link href="../../css/Estilos.css" rel="stylesheet" type="text/css">
 <script language="javascript" src="../../js/jquery-1.4.2.js"></script>
 </head>
 <body>
 <?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

  include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
 include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
 $db = IFXDbManejador::conectarDB();
 if($db->conexionID==null){
 	$cadena = $db->error;
 	echo msg_error($cadena);
	exit();
 }
 
 ini_set("display_errors",1);

 $temp=$_FILES['archivo']['tmp_name'];
 $temporal=file($temp[0]);
  for ($cont=0;$cont<count($temporal);$cont++)
  {
	  $colum1=substr($temporal[$cont],0,2);
	  $colum2=substr($temporal[$cont],7,20);
      $sql="update aportes101 set idetapa=63 where idtarjeta=$colum1 and codigobarra=$colum2";
      $db->queryActualiza($sql);	  
  }
?>
 <br />
 <p align="center" class="RojoGrande" style="margin-first:30px">Archivo Procesado Correctamente</p>
 <br />
</body>
 </html>