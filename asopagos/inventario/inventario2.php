<?php
/* autor:       Orlando Puentes
 * fecha:       Septiembre 14 de 2010
 * objetivo:    Llevar el inventario de todas las Tarjetas Plata de los Afiliados. 
 */
?>
<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

 if ( !isset( $_SESSION ['USUARIO'] ) ) {session_start();}
if(empty($_SESSION['LOGUIADO'])){
	echo "no hay session!!";
	exit();
}
$raiz=$_SESSION['RAIZ'];

include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'session.php';
session();

$url=$_SERVER['PHP_SELF'];
include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'auditoria.php';
auditar($url);

$usuario=$_SESSION['USUARIO'];
$agencia=$_SESSION['AGENCIA'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Inventario Tarjetas</title>
<link type="text/css" href="../../css/formularios/base/ui.all.css" rel="stylesheet" />
<link type="text/css" href="../../css/estilo_tablas.css" rel="stylesheet"/>
<link type="text/css" href="../../css/formularios/demos.css" />
<link href="../../css/Estilos.css" rel="stylesheet" type="text/css" />
<link href="../../css/marco.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../../js/jquery-1.3.2.min.js"></script> 
<script type="text/javascript" src="../../js/formularios/ui/ui.core.js"></script>
<script type="text/javascript" src="../../js/formularios/ui/ui.tabs.js"></script>
<script type="text/javascript" src="../../js/formularios/ui/ui.draggable.js"></script>
<script type="text/javascript" src="../../js/formularios/ui/ui.resizable.js"></script>
<script type="text/javascript" src="../../js/formularios/ui/ui.dialog.js"></script>
<script type="text/javascript" src="js/inventario.js"></script>
<script>
$(document).ready(function(){
	$("#ayuda").dialog({
		autoOpen: false,
		height: 450,
		width: 700,
		draggable:true,
		modal:false,
		open: function(evt, ui){
				$('#ayuda').html('');
				$.get('../../help/tarjetas/tarjeta-inventario.html',function(data){
						$('#ayuda').html(data);
				})
		 }
	});
	
	$("#dialog-form2").dialog({
		autoOpen: false,
		height: 400,
		width: 500,
		modal: true,
		buttons: {
			'Enviar': function() {
			var bValid = true;
			var campo=$('#notas').val();
			var campo0=$.trim(campo);
			if (campo0==""){
				$(this).dialog('close');
					return false;
				}
			var campo1=$('#usuario').val();
			var campo2="radicacion.php";
			$.post(URL+'phpComunes/colaboracion.php',{v0:campo0,v1:campo1,v2:campo2},
			function(datos){
				if(datos=='1'){
					alert("Su comentario fue enviado correctamente!. Gracias por participar en el mejoramiento de SIGAS");
				}else{
					alert(datos);
				}
		});
		$(this).dialog('close');
		},
		Cancelar: function() {
			$(this).dialog('close');
			$("#dialog-form2").dialog("destroy");
		}
	}
	});
	
});
function mostrarAyuda(){
	$("#ayuda").dialog('open' );
}

function notas(){
	$("#dialog-form2").dialog('open');
}	
</script>
</head>
<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
	<td width="13" height="29" class="arriba_iz">&nbsp;</td>
	<td class="arriba_ce"><span class="letrablanca">::Inventario de Tarjeta::::</span></td>
	<td width="13" class="arriba_de" align="right">&nbsp;</td>
	</tr>
	<tr>
		<td class="cuerpo_iz">&nbsp;</td>
		<td class="cuerpo_ce"><img height="1" width="1" src="../../imagenes/spacer.gif"> 
      <img height="1" width="1" src="../../imagenes/spacer.gif"/> <img src="../../imagenes/menu/informacion.png" width="16" style="border:none; cursor:pointer" title="Manual" onClick="mostrarAyuda();" /> 
      <img src="../../imagenes/menu/notas.png" width="16" height="16" style="cursor: pointer" title="Colaborac&oacute;on en l&oacute;nea" onClick="notas();" /> 
    </td>
		<td class="cuerpo_de">&nbsp;</td>
	</tr>
<tr>
	<td class="cuerpo_iz">&nbsp;</td>
	<td>
		<form name="forma">
			<blockquote>&nbsp;</blockquote>
			<table width="100%" border="0" cellspacing="0">
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td><div align="center" class="big">PROCESO DE INVENTARIO DE TARJETA PLATA</div></td>
				</tr>
				<tr>
					<td class="Rojo"><div align="center">Por favor escanear una a una las Tarjetas Plata teniendo en cuenta de no repetir la tarjeta escaneada</div></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td align="center"><input type="text" style="font-size:18px"  name="numero" id="numero" onkeypress="return continuar(event);"/>  </td>
				</tr>
				<tr>
					<td align="center"><div id="ultimo" style="font-size:18px; color:#F00; font-weight:bold" ></div></td>
				</tr>
				<tr>
					<td align="center">&nbsp;</td>
				</tr>
				<tr>
					<td align="center">Usuario: <?php echo $usuario." .:. Agencia: $agencia"; ?><br/><br/><br/><br/></td>
				</tr>
			</table>
			<input type="hidden" name="tAgencia" id="tAgencia" value="<?php echo $agencia; ?>" />
		</form>
		</td>
		<td class="cuerpo_de">&nbsp;</td>
	</tr>
	<tr>
		<td class="abajo_iz" >&nbsp;</td>
    <td class="abajo_ce" ></td>
    <td class="abajo_de" >&nbsp;</td>
	</tr>
</table>
<div id="dialog-form2" title="Colaboraci&oacute;n en l&iacute;nea">
  <p>Por favor diligencie este formulario para enviar comentarios, errores o falencias 
    encontradas en el proceso. M�ximo 250 caracteres </p>
  <label>Tus comentarios:</label>
  <br />
  <textarea name="notas" id="notas" cols="60", rows="10"></textarea>
</div>
<!-- Manual Ayuda -->
<div id="ayuda" title="::Manual Inventario de Tarjetas::" style="background-image:url(../../imagenes/FondoGeneral0.png)"> </div>
<div title="::Detalles Trabajador::" id="detalles"> 
  <p align="left" id="info"></p>
</div>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
</body>
<script>
document.getElementById('numero').focus();
</script>
</html>