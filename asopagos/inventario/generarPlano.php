<?php
set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$v0=$_REQUEST['v0'];
$v1=$_REQUEST['v1'];
$v2=$_REQUEST['v2'];

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
include_once $raiz.DIRECTORY_SEPARATOR. 'funcionesComunes'. DIRECTORY_SEPARATOR. 'funcionesComunes.php';
include_once $raiz.DIRECTORY_SEPARATOR.'config.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}
$sql="Select idbono,bono,codigobarra from aportes100 WHERE agencia='$v2' AND fechaenvio=CAST(getdate() AS DATE)";
$rs=$db->querySimple($sql);

$directorio=$ruta_generados. DIRECTORY_SEPARATOR. 'asopagos'. DIRECTORY_SEPARATOR. 'enviosagencias'. DIRECTORY_SEPARATOR;
if( !verificaDirectorio($directorio) ){
	exit(9);
} 
$fecha=date("Ymd");
$file='despacho_agencia_03_'.$fecha.'.csv';
$ruta=$directorio.$file;
$f=fopen($ruta, "w");
$cadena="RELACION DE TARJETAS ENVIADAS A PITALITO,,\r\n";
fwrite($f, $cadena);
$cadena="IDBONO,BONO,CODIGOBARRA\r\n";
fwrite($f, $cadena);
fclose($f);

while ($row=$rs->fetch()){
	$id=$row['idbono'];
	$bono=$row['bono'];
	$barra=$row['codigobarra'];
	$cadena="$id,$bono,$barra\r\n";
	$f=fopen($ruta, "a");
	fwrite($f, $cadena);
	fclose($f);
}

$_SESSION['ENLACE']=$ruta;
$_SESSION['ARCHIVO']=$file;
echo 1;
?>