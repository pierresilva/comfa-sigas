<?php
// @autor:		Orlando Puentes A
// @fecha:		mayo 31/2010
// @formulario:	guardar datos persona - modulo empresa
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
include_once $raiz.DIRECTORY_SEPARATOR.'config.php';
?>
<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es">
<head> 
 <title>Procesar archivos</title>
 <script language="javascript" src="../../js/ui/jquery-1.4.2.js"></script>
 <script language="javascript" src="js/bonosNuevos.js"></script>
 </head>
 <body>
 <fieldset>
 <br />
 <p align="center" class="Rojo"><strong>Resultado de la Copia de Archivos</strong></p>
 <br />
 <?php 
$directorio= $ruta_cargados.DIRECTORY_SEPARATOR.'asopagos'.DIRECTORY_SEPARATOR.'tarjetas_nuevas'.DIRECTORY_SEPARATOR;
//$directorio = "/var/www/html/planos_cargados/asopagos/tarjetas_nuevas/";
echo "<br><br><br><br>";
if(isset($_FILES['archivo'])){
	foreach ($_FILES['archivo']['error'] as $key => $error) {
	   if ($error == UPLOAD_ERR_OK) {
		   $archivos[]= $_FILES["archivo"]["name"][$key];
		   move_uploaded_file($_FILES["archivo"]["tmp_name"][$key],$directorio.$_FILES["archivo"]["name"][$key]) 
		   or die("Ocurrio un problema al intentar subir el archivo.");
	   }
	}
 }
 
for($i=0; $i<count($archivos); $i++){
	echo "<p align=center>Archivo copiado en el servidor: <strong>".$archivos[$i]."</strong></p>"; 
}
if($i>0){
?>

<center>
    <div id="boton1">
    <p style="text-decoration:none; font-size:12px; color:#F00; font-weight:bold; cursor:pointer" onclick="procesar();">Procesar estos archivos</p>
    </div>
</center>
<center>
    <div id="procesar" style="display:none">
    <p>Procesando...</p>
    </div>
</center>
<?php
	}
 ?> 
 <div id="ajax" align="center"> </div>
<div id="mensajes" align="center">..:..</div>

</fieldset>
 <p>&nbsp;</p>
 </body>
 </html>
 
