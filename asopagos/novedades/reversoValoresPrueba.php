<?php 
/* autor:       Orlando Puentes
 * fecha:       Julio 23 de 2010
 * objetivo:    
 */
date_default_timezone_set('America/Bogota'); 
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$url=$_SERVER['PHP_SELF'];
include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'auditoria.php';
auditar($url);

include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR .'p.definiciones.class.php';
$objClase=new Definiciones();
$fecha=date("m/d/Y");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="es">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>::Reversar Valores::</title>
<link type="text/css" rel="stylesheet" href="../../css/css.1.8/jquery-ui-1.8.17.custom.css" />
<link type="text/css" href="../../css/Estilos.css" rel="stylesheet"/>
<script type="text/javascript" src="../../js/1.6/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="../../js/1.6/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="../../js/comunes.js"></script>
<script type="text/javascript" src="js/reversoPrueba.js"></script>

<script type="text/javascript">
shortcut.add("Shift+F",function() {
		                                var URL=src();
		var url=URL+"aportes/trabajadores/consultaTrabajador.php";
    	window.open(url,"_blank");
    },{
	'propagate' : true,
	'target' : document 
        });        
</script>

</head>

<body>
<form name="forma">
<!-- TABLA VISIBLE CON BOTONES -->
<table width="97%" border="0" cellspacing="0" cellpadding="0" align="center">
<tr>
<td width="13" height="29" background="../../imagenes/tabla/arriba_izq.gif">&nbsp;</td>
<td background="../../imagenes/tabla/arriba_central2.gif"><span class="letrablanca">::Reverso de Valores&nbsp;::</span></td>
<td width="13" background="../../imagenes/tabla/arriba_der.gif" align="right">&nbsp;</td>
</tr>      
<tr>
<td background="../../imagenes/tabla/izquierda2.jpg">&nbsp;</td>
<td background="../../imagenes/tabla/centro.gif">
<img src="../../imagenes/tabla/spacer.gif" width="1" height="1">
<img src="../../imagenes/spacer.gif" width="1" height="1">
<img src="../../imagenes/spacer.gif" width="1" height="1"><img src="../../imagenes/spacer.gif" width="1" height="1">
<img src="../../imagenes/menu/modificar.png" title="Actualizar" width="16" height="16" onClick="guardar();" style="cursor:pointer">
<img src="../../imagenes/spacer.gif" width="1" height="1">
<img src="../../imagenes/menu/refrescar.png" width="16" height="16" style="cursor:pointer" title="Limpiar campos" onClick="limpiarCampos();">
<img src="../../imagenes/spacer.gif" width="1" height="1">
<img src="../../imagenes/menu/informacion.png" width="16" height="16" style="border:none; cursor:pointer" title="Manual" onClick="mostrarAyuda();" />
<img src="../../imagenes/menu/notas.png" width="16" height="16" style="cursor: pointer" title="Colaborac�on en l�nea" onclick="notas();" />
<td background="../../imagenes/tabla/derecha.gif">&nbsp;</td>
</tr> 
<tr>     
<td background="../../imagenes/tabla/izquierda2.jpg">&nbsp;</td>
<td align="left" background="../../imagenes/tabla/centro.gif"><div id="error" style="color:#FF0000"></div></td>
<td background="../../imagenes/tabla/derecha.gif">&nbsp;</td>
</tr>
<tr>
<td background="../../imagenes/tabla/izquierda2.jpg">&nbsp;</td>

<td align="center" background="../../imagenes/tabla/centro.gif">
<table width="90%" border="0" cellspacing="0" class="tablero">
<tr>
<td width="25%">Buscar Por</td>
<td width="25%">
<select name="buscarPor" id="buscarPor" class="box1">
<option value="2" selected="selected">Bono</option>
<option value="1">Identificaci�n</option>
<option value="2">Bono</option>
</select>
</td>
<td width="25%">Fecha</td>
<td width="25%"><?php echo $fecha;?>&nbsp;</td>
</tr>
<tr id="tNumero" style="display:none">
  <td width="25%">Tipo Documento</td>
  <td width="25%"><select name="tipoI" id="tipoI" class="box1" >
    <?php
	$consulta=$objClase->mostrar_datos(1, 1);
	while($row=mssql_fetch_array($consulta)){
		echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
	}
	?>
  </select></td>
  <td width="25%">N&uacute;mero</td>
  <td width="25%"><input name="numero" type="text" class="box1" id="numero" onblur="buscarPersona();" /></td>
</tr>
<tr id="tBono" style="display:none">
  <td>Bono N&uacute;mero</td>
  <td><input name="txtBono" type="text" class="box1" id="txtBono" onblur="rellenarTarjeta(this,this.value);buscarPersona2();" /></td>
  <td>&nbsp;</td>
  <td>&nbsp;</td>
</tr>
<tr>
  <td width="25%">Nombres</td>
  <td colspan="25" id="lnombre">&nbsp;</td>
  </tr>
  <tr>
  <td width="12%">Bono</td>
  <td id="lbono" width="28%">&nbsp;</td>
  <td width="15%">Estado</td>
  <td id="lestado" width="45%">&nbsp;</td>
  </tr>
<tr>
  <td width="12%">Fecha Solicitud</td>
  <td width="28%" id="lfechasol"></td>
  <td width="15%">Saldo</td>
  <td width="45%" id="lsaldo"></td>
</tr>
<tr>
  <td width="12%">Motivo</td>
  <td width="30%"><select id="mReverso" name="mReverso" class="boxmediano" onchange="mostrarTr();">
    <option value="3664" selected="selected">REVERSO POR TRASLADO</option>
    <?php
	$consulta=$objClase->mostrar_datos(53, 2);
	while($row=mssql_fetch_array($consulta)){
		echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
	}
	?>
  </select>&nbsp;&nbsp;<img src="../../imagenes/menu/obligado.png" width="12" height="12" /></td>
  <td width="15%">Valor Reverso</td>
  <td width="45%"><input type="text" name="txtReverso" id="txtReverso" class="box1" onkeypress="return soloNumeros(event);" /><img src="../../imagenes/menu/obligado.png" width="12" height="12" /></td>
</tr>
<tr id="nuevaTarjeta" style="display:none">
  <td>Tarjeta Nueva</td>
  <td colspan="2" >
  <input type="text" class="box1" name="txtBonoNuevo" id="txtBonoNuevo" onkeypress="return soloNumeros(event);" onblur="buscarNuevaT();" />
  </td>
  <td id="trNombre"></td>
</tr>
<tr>
	<td width="12%">Periodo Giro</td>
  	<td colspan="3"><input type="text" name="txtPeriodoGiro" id="txtPeriodoGiro" value="201410" class="box1" />
   		<img src="../../imagenes/menu/obligado.png" width="12" height="12" />
   	</td>
</tr>
<tr>
	<td width="12%">Notas</td>
  	<td colspan="3"><textarea class="boxlargo" name="notas" id="notas" maxlength="250">YA SE TRASLADO A LA TARJETA ACTIVA</textarea>
    	<img src="../../imagenes/menu/obligado.png" width="12" height="12" />
    </td>
</tr>
</table>
</center>
<div id="errores" align="left"></div>
<td background="../../imagenes/tabla/derecha.gif"></td><!-- FONDO DERECHA -->
<tr>
<td height="41" background="../../imagenes/tabla/abajo_izq2.gif">&nbsp;</td>
<td background="../../imagenes/tabla/abajo_central.gif" ></td>
<td background="../../imagenes/tabla/abajo_der.gif">&nbsp;</td>
</tr>
</table>  
       
<!--colaboracion en linea-->
<div id="dialogo-archivo" title="Archivo banco">
<div id="progreso" style="display: none; font-size: 15pt; font-weight: bold;">Procesado(s) <span id="pg">0</span> de <span id="tt"></span> archivo(s)</div>
<div id="log"></div>
</div>
<div id="dialog-form2" title="Colaboraci&oacute;n en l&iacute;nea" style="display:none">
<p>Por favor diligencie este formulario para enviar comentarios, errores o falencias encontradas en el proceso. M&aacute;ximo 250 caracteres </p>
<label>Tus comentarios:</label><br />
<textarea name="notas" id="notas" cols="60", rows="10"></textarea>
</div>

<!-- fin colaboracion -->

<!-- ayuda en linea -->
<div id="ayuda" title="Manual .:. Reverso de Valores" style="background-image:url(../../imagenes/FondoGeneral0.png)"></div>

</form>

</body>
<script language="javascript">
$("#buscarPor").focus();

function mostrarAyuda(){
	$("#ayuda").dialog('open' );
	}

function notas(){
	$("#dialog-form2").dialog('open');
	}	
</script>
</html>