<?php
/**
 * @date 21-Mayo-2015
 * @objetivo marcar prescripciones y automaticamente realizaar los reversos
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';
$fechaHoy=date("Y/m/d");
$usuario=$_SESSION['USUARIO'];
include_once $_SESSION['RAIZ'] . DIRECTORY_SEPARATOR . 'rsc/pdo/IFXDbManejador.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

?>
<HTML xmlns="http://www.w3.org/1999/xhtml"><HEAD>
<TITLE>::Prescripciones::</TITLE>
<META http-equiv=Content-Type content="text/html; charset=iso-8859-1">
<meta http-equiv="Content-Script-Type" content="text/javascript; charset=iso-8859-1">
<META content="MSHTML 6.00.2900.2180" name=GENERATOR>

<link type="text/css" href="../../css/css.1.8/jquery-ui-1.8.17.custom.css" rel="stylesheet">
<link type="text/css" href="../../css/marco.css" rel="stylesheet">
<link type="text/css" href="../../css/Estilos.css" rel="stylesheet">


<script type="text/javascript" src="../../js/1.6/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="../../js/1.6/jquery-ui-1.8.16.custom.min.js"></script>

<script type="text/javascript" src="../../js/comunes.js"></script>
<script type="text/javascript" src="js/marcarPrescripciones.js"></script>

<script type="text/javascript">
	shortcut.add("Shift+F",function() {
		var URL=src();
		var url=URL+"aportes/trabajadores/consultaTrabajador.php";
	    window.open(url,"_blank");
	},{
		'propagate' : true,
		'target' : document 
	});
</script>
<style>
.pagina_actual{
	font-size: 12pt;
	font-weight: bold;
}
</style>
</head>
<body>
<center>
<br><br>
<table width="90%" border="0" cellspacing="0" cellpadding="0" class="ui-corner-all">
  <tr>
    <td width="13" height="29" background="../../imagenes/arriba_izq.gif">&nbsp;</td>
    <td background="../../imagenes/arriba_central2.gif"><span class="letrablanca">::&nbsp;Prescripciones ::</span></td>
    <td width="13" background="../../imagenes/arriba_der.gif" align="right">&nbsp;</td>
  </tr>
  <tr>
    <td background="../../imagenes/tabla/izquierda2.jpg">&nbsp;</td>
    <td background="../../imagenes/centro.gif">
	 <img src="../../imagenes/spacer.gif" width="1" height="1"/><img src="../../imagenes/spacer.gif" width="1" height="1"/>
<img src="../../imagenes/menu/grabar.png" width:16 height=16 style="cursor:pointer" title="Guardar" onClick="guardarMarcadas();" id="bGuardar"/> 
<img src="../../imagenes/spacer.gif" width="1" height="1"/><img src="../../imagenes/spacer.gif" width="1" height="1"/><img src="../../imagenes/spacer.gif" width="1" height="1"/>
	  </td>
    <td background="../../imagenes/tabla/derecha.gif">&nbsp;</td>
  </tr>
  <tr>
    <td background="../../imagenes/tabla/izquierda2.jpg">&nbsp;</td>
    <td align="center" background="../../imagenes/centro.gif"><div id="resultado" style="font-weight: bold;font-size: 14px;color:#FF0000"></div></td>
    <td background="../../imagenes/tabla/derecha.gif">&nbsp;</td>
  </tr>
  <tr>
    <td background="../../imagenes/tabla/izquierda2.jpg">&nbsp;</td>
    <td  background="../../imagenes/centro.gif" >
    <h5>PRESCRIPCIONES DE TARJETAS PENDIENTES</h5>
    <p>Tiene <span id="span_num_empresas" class="font-weigth:bold;"></span> tarjetas por marcar.</p>
    <br/>
    	<table width="100%" border="0" cellspacing="0" class="tablero">
          <tr>
		  	<td width="12%">Fecha Corte</td>
		  	<td><input type="text" name="fechaCorte" id="fechaCorte" class="box1" /></td>
		  	 <td colspan="2" width="641"><input name="buscarListado" type="button" class="ui-state-default" id="buscarListado" value="Generar Lista"/>
             <label style="cursor:pointer" onClick="procesar01();"><img src="../../imagenes/icono_excel.png" width="18" height="18"></label>
       	  </tr> 
       	  <tr>
		  	<td width="12%">Periodo Giro</td>
		  	<td colspan="3"><input type="text" name="periodoGiro" id="periodoGiro" class="box1" /></td>
		  </tr>
		  <tr>
		  	<td width="12%">Notas</td>
		  	<td colspan="3"><textarea class="boxlargo" name="notas" id="notas" maxlength="250"></textarea></td>
          </tr>
          <tr>
             <td width="186">Buscar Por:</td>
             <td width="153"><select name="buscarPor" class="box1" id="buscarPor">
               <option selected="selected">Seleccione..</option>
               <option value="documento" selected="selected">Identificación</option>
               <option value="bono">Bono</option>
             </select></td>
             <td width="127"><input name="idTarjeta" type="text" class="box1" id="idTarjeta"/></td>
             <td width="641"><input name="buscarTarjeta" type="button" class="ui-state-default" id="buscarTarjeta" value="Buscar" />
              <span class="Rojo"></span>
             </td>
          </tr>
        </table>
	<p></p>
	<table width="100%" border="0" cellpadding="0" cellspacing="0" class="tablaR hover" id="tDatos"  >
		<thead>
			<tr>
		        <th width="15%" align="center">Identificacion.</th>
		        <th width="15%" align="center">Tarjeta</th>
		        <th width="10%">Saldo Corte</th>
		        <th width="10%">Reverso</th>
		        <th width="10%">Saldo Actual</th>
		        <th width="20%">Direccion</th>
		        <th width="7%">Telefono</th>
		        <th> Marcar Todos <input class='ck' type='checkbox' name='checktodos' id='checktodos'/> <th>
	        </tr>
		</thead>
		<tbody>
		</tbody>
		<tfoot>
        </tfoot>
	</table>
     
    </td>
    <td background="../../imagenes/tabla/derecha.gif">&nbsp;</td>
  </tr>
  <tr>
    <td height="38" background="../../imagenes/abajo_izq2.gif">&nbsp;</td>
    <td background="../../imagenes/abajo_central.gif"></td>
    <td background="../../imagenes/abajo_der.gif">&nbsp;</td>
  </tr>
</table>

</center>
<input type="hidden" name="pageNum" value="0">

<input type="hidden" name="valorp" id="valorp" >
<input type="hidden" name="txtIdCliente" value="">
	
<!-- DIALOGO REVERSO MAYOR AL VALOR ACTUAL -->
<div id="dialog-reverso-mayor" title="::REVERSOS MAYOR AL SALDO ACTUAL::" style="display:none">	
	<table width="100%" class="tablero" id="DivTable">
		<tr>
	  		<th width="15%" align="center">Identificacion.</th>
	        <th width="15%" align="center">Bono</th>
	        <th width="10%">Saldo</th>
		</tr>
	</table>
</div>

</body>
<input type="hidden" id="txtusuario" name="txtusuario" value="<?php echo $usuario; ?>" /> 
<input type="hidden" id="txtFecha" name="txtFecha" value="<?php echo $fechaHoy; ?>" /> 
</html>