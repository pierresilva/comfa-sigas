<?php
set_time_limit(0);
ini_set("display_errors",'0');
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$usuario= $_SESSION["USUARIO"];

include_once $raiz.DIRECTORY_SEPARATOR.'funcionesComunes'.DIRECTORY_SEPARATOR.'funcionesComunes.php';

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

$pagina = $_REQUEST["pagina"];
$numRegistros = $_REQUEST["numRegistros"];
$bono = (isset($_REQUEST["bono"]) && $_REQUEST["bono"] != "")?$_REQUEST["bono"]:null;
$documento = (isset($_REQUEST["documento"]) && $_REQUEST["documento"] != "")?$_REQUEST["documento"]:null;
$strCondicionBusqueda = "";

if($bono != null)
	$strCondicionBusqueda = " AND a101.bono like '%{$bono}%' ";
else if($documento != null)
	$strCondicionBusqueda = " AND a15.identificacion like '%{$documento}%' ";

$sql="SELECT * FROM (
		SELECT row_number() OVER(ORDER BY a117.idtarjeta ASC) AS contador, a117.idtarjeta, 
			a15.identificacion,
			a101.bono,
			a15.pnombre+' '+isnull(a15.snombre,'')+' '+a15.papellido+' '+isnull(a15.sapellido,'') as nombres,
			a117.fechasistema , a117.usuario
		FROM 
			aportes117 a117 
			INNER JOIN aportes101 a101 ON a101.idtarjeta=a117.idtarjeta
			INNER JOIN aportes015 a15 ON a15.idpersona=a101.idpersona 
		WHERE
			a117.estado='I' {$strCondicionBusqueda}) AS c WHERE c.contador BETWEEN ($pagina-1) * $numRegistros + 1 AND $pagina * $numRegistros";
$rs = $db->querySimple($sql);
$filas=array();
while($w = $rs->fetch())
	$filas[] = $w;

if(count($filas) == 0)
	echo 0;
else
	echo json_encode($filas);
?>