<?php 
date_default_timezone_set('America/Bogota'); 
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$url=$_SERVER['PHP_SELF'];
include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'auditoria.php';
auditar($url);

include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR .'p.definiciones.class.php';
$objClase=new Definiciones();
$fecha=date("m/d/Y");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="es">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>::Reversar No Procesados::</title>
<link type="text/css" rel="stylesheet" href="../../css/css.1.8/jquery-ui-1.8.17.custom.css" />
<link type="text/css" href="../../css/Estilos.css" rel="stylesheet"/>
<script type="text/javascript" src="../../js/1.6/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="../../js/1.6/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="../../js/comunes.js"></script>
<script type="text/javascript" src="js/reversoNoProcesa.js"></script>

<script type="text/javascript">
shortcut.add("Shift+F",function() {
		                                var URL=src();
		var url=URL+"aportes/trabajadores/consultaTrabajador.php";
    	window.open(url,"_blank");
    },{
	'propagate' : true,
	'target' : document 
        });        
</script>

</head>

<body>
<form name="forma">
<!-- TABLA VISIBLE CON BOTONES -->
<table width="97%" border="0" cellspacing="0" cellpadding="0" align="center">
<tr>
<td width="13" height="29" background="../../imagenes/tabla/arriba_izq.gif">&nbsp;</td>
<td background="../../imagenes/tabla/arriba_central2.gif"><span class="letrablanca">::Reverso de Valores No Procesados&nbsp;::</span></td>
<td width="13" background="../../imagenes/tabla/arriba_der.gif" align="right">&nbsp;</td>
</tr>      
<tr>
<td background="../../imagenes/tabla/izquierda2.jpg">&nbsp;</td>
<td background="../../imagenes/tabla/centro.gif">
<img src="../../imagenes/tabla/spacer.gif" width="1" height="1">
<img src="../../imagenes/spacer.gif" width="1" height="1">
<img src="../../imagenes/spacer.gif" width="1" height="1"><img src="../../imagenes/spacer.gif" width="1" height="1">
<img src="../../imagenes/menu/grabar.png" title="Guardar" width="16" height="16" onClick="guardar();" style="cursor:pointer">
<img src="../../imagenes/spacer.gif" width="1" height="1">
<img src="../../imagenes/menu/refrescar.png" width="16" height="16" style="cursor:pointer" title="Limpiar campos" onClick="limpiarCampos();">
<img src="../../imagenes/spacer.gif" width="1" height="1">
<td background="../../imagenes/tabla/derecha.gif">&nbsp;</td>
</tr> 
<tr>     
<td background="../../imagenes/tabla/izquierda2.jpg">&nbsp;</td>
<td align="left" background="../../imagenes/tabla/centro.gif"><div id="error" style="color:#FF0000"></div></td>
<td background="../../imagenes/tabla/derecha.gif">&nbsp;</td>
</tr>
<tr>
<td background="../../imagenes/tabla/izquierda2.jpg">&nbsp;</td>

<td align="center" background="../../imagenes/tabla/centro.gif">
<table width="90%" border="0" cellspacing="0" class="tablero">
<tr>
<td width="25%">Fecha</td>
<td colspan="3" width="25%"><?php echo $fecha;?>&nbsp;</td>
</tr>
<tr>
  <td width="25%">Tipo Documento</td>
  <td width="25%"><select name="tipoI" id="tipoI" class="box1" >
    <?php
	$consulta=$objClase->mostrar_datos(1, 1);
	while($row=mssql_fetch_array($consulta)){
		echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
	}
	?>
  </select></td>
  <td width="25%">N&uacute;mero</td>
  <td width="25%"><input name="numero" type="text" class="box1" id="numero" onblur="buscarPersona();" /></td>
</tr>
<tr>
  <td>Nombres</td>
  <td id="lnombre">&nbsp;</td>
  <td>Saldo</td>
  <td id="lsaldo"></td>
</tr>
<tr>
  <td width="12%">Motivo</td>
  <td colspan="3" width="30%"><select id="mReverso" name="mReverso" class="boxmediano"">
    <option value="0" selected="selected">Seleccione</option>
    <?php
	$consulta=$objClase->mostrar_datos(53, 2);
	while($row=mssql_fetch_array($consulta)){
		echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
	}
	?>
  </select>&nbsp;&nbsp;<img src="../../imagenes/menu/obligado.png" width="12" height="12" /></td>
</tr>
<!-- GENERAR CHECKBOX  -->
<tr>
<td class="cuerpo_iz">Periodos a Reversar</td>
<td colspan="2" class="cuerpo_ce">
	<table width="50%" border="0" cellspacing="0" cellpadding="0" align="center" class="tablero">
			<tr>
				<th>Periodo</th>
				<th>Valor</th>
				<th>Valor Reverso</th>
				<th>&nbsp;</th>							
			</tr>
			<tbody id="tbCargue"></tbody>
		</table>
</td>
	<td class="cuerpo_de">&nbsp;</td>
</tr> 
<tr>
	<td width="12%">Notas</td>
  	<td colspan="3"><textarea class="boxlargo" name="notas" id="notas" maxlength="250"></textarea>
    	<img src="../../imagenes/menu/obligado.png" width="12" height="12" />
    </td>
</tr>
</table>
</center>
<div id="errores" align="left"></div>
<td background="../../imagenes/tabla/derecha.gif"></td><!-- FONDO DERECHA -->
<tr>
<td height="41" background="../../imagenes/tabla/abajo_izq2.gif">&nbsp;</td>
<td background="../../imagenes/tabla/abajo_central.gif" ></td>
<td background="../../imagenes/tabla/abajo_der.gif">&nbsp;</td>
</tr>
</table>  
</form>

</body>
<script language="javascript">
$("#tipoI").focus();
</script>
</html>