var URL=src();
var estado=0;
var idp=0;
var idb=0;


$(document).ready(function(){
	$("#buscarPor").bind('change',function(){
 		$("#lnombre,#lbono,#lsaldo").html(" ");
		$("#lestado").html("");
//		$("#buscarPor").focus();
		});
	
});//fin ready

function mostrar(){
	var opt=$("#buscarPor").val();
	if(opt==0){
		$("#tNumero").css("display", "none"); 
		$("#tBono").css("display", "none"); 
		$("#numero").val("");
		}
	if(opt==1){
		$("#tNumero").css("display", "block"); 
		$("#tBono").css("display", "none"); 
		$("#tipoI").focus();
		}
	if(opt==2){
		$("#tBono").css("display", "block"); 
		$("#tNumero").css("display", "none"); 
		$("#txtBono").focus();
		}
	}
	
function buscarPersona(){
	var tipoI=$("#tipoI").val();
	var numero=$("#numero").val();
	if(numero.length==0){
		return false;
		}
	$.getJSON(URL+'phpComunes/buscarPersona2.php',{v0:tipoI,v1:numero},function(data){
		if(data==0){
			alert("Lo lamento, el n\u00FAmero no existe!");
			$("#numero").val("");
			$("#numero").focus();
			return false;
		}
		$.each(data,function(i,fila){
		var nom=fila.pnombre+" "+fila.snombre+" "+fila.papellido+" "+fila.sapellido;
		idp=fila.idpersona;
		$("#lnombre").html(nom);
		//return;
		});
	$.getJSON(URL+'phpComunes/pdo.buscar.tarjeta.act.php',{v0:idp},function(datos){
			if(datos==0){
				alert("Lo lamento, no hay bono asociado a ese n�mero!");
				return false;
			}
		$.each(datos,function(i,fila){
			if(fila.SALDO==null){
				fila.SALDO='0';	
			}
			idb=fila.idtarjeta;
			$("#lbono").html(fila.bono);
			$("#lestado").html(fila.estado);
			$("#lsaldo").html(formatCurrency(fila.saldo));
			estado=fila.estado;
			return;
		});
		});
	})
	}	

function buscarPersona2(){
	var bono=$.trim($("#txtBono").val());
	var idp=0;
	if(bono.length==0){
		alert("Digite el n\u00FAmero de Bono!");
		$("#txtBono").focus();
		return false;
		}
	$.ajax({
		url: URL+'phpComunes/pdo.buscar.tarjeta.act2.php',
		type: "POST",
		async: false,
		data: {v0:bono},
		dataType: "json",
		success: function(datos){
		if(datos==0){
			alert("Lo lamento, el bono no existe!");
             limpiarCamposReseteo();
			return false;
		}
		idb=datos[0].idtarjeta;
		idp=datos[0].idpersona;
		$("#lbono").html(datos[0].bono);
		$("#lsaldo").html(formatCurrency(datos[0].saldo));
		$("#lestado").html(datos[0].estado);
		estado=datos[0].estado;
		
		$.getJSON(URL+'phpComunes/pdo.buscar.persona.id.php',{v0:idp},function(data){
		if(data==0){
			alert("Lo lamento, el n\u00FAmero no existe!");
			$("#numero").val("");
			$("#numero").focus();
			return false;
		}
		$.each(data,function(i,fila){
			var nom=fila.pnombre+" "+fila.snombre+" "+fila.papellido+" "+fila.sapellido;
			$("#lnombre").html(nom);
			return;
			});
		});
		}
		});
	
	}	

function limpiarCampos(){
	$("table.tablero input:text").val('');
	$("table.tablero select").val(0);
	$("#lnombre,#lbono,#lsaldo").html(" ");
	$("#lestado").html("");
	$("#tNumero").css("display", "none"); 
	$("#tBono").css("display", "none"); 
	$("#numero").val("");
	$("#buscarPor").focus();
}


function limpiarCamposReseteo(){
	$("table.tablero input:text").val('');
	$("#lnombre,#lbono,#lsaldo").html(" ");
	$("#lestado").html("");
	$("#tNumero").css("display", "none"); 
	$("#tBono").css("display", "none"); 
	$("#numero").val("");
	$("#txtBono").focus();
	mostrar();
}
function guardar(){
	if(estado=='B'){
		alert("La tarjeta esta BLOQUEADA!");
		return false;
	}
	var opt=$("#buscarPor").val();
	var notas=$("#notas").val();
	if(opt==0){
		alert("No hay informacion para guardar!");
		return false;
	}
	if(notas.length==0){
		alert("Escriba las notas!");
		return false;
	}
	if(notas.length<10){
		alert("Las notas son muy cortas...!");
		return false;
	}
	$.ajax({
	url: 'guardarSolicitudResetClave.php',
	type: "POST",
	async: false,
	data: {v0:idb},
	success: function(datos){
		if(datos==0){
			alert("Lo lamento, no se pudo Mandar la Solicitud de Reseteo!")
			return false;
		}
		if(datos==2){
			alert("La solicitud de Reseteo ya se hizo!")
			return false;
		}
		if(datos==1){
			alert("La solicitud se mando con exito!");
			limpiarCampos();
		if(notas.length>0){
			$.getJSON(URL+'asopagos/guardarNota.php',{v0:idb,v1:notas},function(data){
				if(data==0){
					alert("No se pudo guardar las notas, por favor comunicar al Administrador del Sistema!");
					}
			});
		}
		}
	}
	});
}