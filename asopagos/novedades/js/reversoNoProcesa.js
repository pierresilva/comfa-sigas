var URL=src();
var estado=0;
var idp=0;
var idt=0;
var valor=0;
var idnt=0;
tiempoFuera=30000;


function buscarPersona(){	
	var tipoI=$("#tipoI").val();
	var numero=$("#numero").val();
	var idp=0;
	limpiarCampos();
			
	$.ajax({
		url: URL+'phpComunes/pdo.buscar.persona.php',
		type: "POST",
		async: false,
		data: {v0:tipoI,v1:numero,v2:2},
		dataType: "json",
		success: function(data){
			if(data==0){
				alert("Lo lamento, el n\u00FAmero no existe!");
				$("#numero").val("");
				$("#numero").focus();
				return false;
			}
			var nom=data[0].pnombre+" "+data[0].snombre+" "+data[0].papellido+" "+data[0].sapellido;
			idp=data[0].idpersona;
			$("#lnombre").html(nom);
		},
		complete: function(){
			$.getJSON('buscarSaldoCargue.php',{v0:idp},function(datos){
			$("#lsaldo").html(formatCurrency(datos[0].valor));
			valor=datos[0].valor;	
			var contar=0;
			idt=datos.idtraslado;
			$.ajax({
		        url: "buscarCarguesTabla.php",
		        data:{v0:idp},
		        dataType: "json",
		        async:false,
		        success: function(datos){
		        	if(datos==0){
		        		alert("Lo lamento, no existe cargue asociado a ese n�mero!");
		        		limpiarCampos();
		    		}	
		    		$.each(datos,function(i,f){
		    			idt=f.idtraslado;
		    			contar++;	
						$("#tbCargue").append("<tr id='trReverso"+f.idtraslado+"'><td id='tdPeriodo"+f.idtraslado+"'>"+f.periodogiro+"</td>" +
						"<td id='tdValor"+f.idtraslado+"'>"+f.valor+"</td>" +
						"<td><input type='text' name='txtReverso"+f.idtraslado+"' id='txtReverso"+f.idtraslado+"' value='"+f.valor+"'></td>" +
						"<td><input type='checkbox' name='checkReverso"+f.idtraslado+"' id='checkReverso"+f.idtraslado+"' value='"+f.idtraslado+"'></td></tr>")
		    			return;
		    			})
		        },
		        timeout: tiempoFuera,
		        type: "GET"
		 });
	});	//fin buscar persona
			} // fin complete: function(){
	})
	}

function limpiarCampos(){
	$("table.tablero input:text").val('');
	$("table.tablero select").val(0);
	$("#lnombre,#lsaldo").html(" ");
	$("#notas").val("");
	$("#tbCargue tr input:checkbox:checked").attr("checked",false);
	$("#tbCargue tr").html(" ");
	$("#tipoI").focus();
	
}

function guardar(){
	var numero=$("#numero").val();
	var notas=$("#notas").val();
	if(notas.length==0){
		alert("Digite las notas!");
		return false;
	}
	if(notas.length<10){
		alert("Las notas son muy cortas...!");
		return false;
	}
	if($("#mReverso").val()==0){
		alert("Falta el motivo del reverso!");
		return false;
		}
	var idmotivo=$("#mReverso").val();
	var continuar=1;
	
	var idnota=0;
	var notas=$("#notas").val();
	/**--Recorre la tabla de reversos por periodo--**/
	var objDatoReverso = [];
	var banderaError = 0;
	
	  if ($("#tbCargue tr input:checkbox:checked").length == 0){
		  alert("Seleccione una opcion de reverso");
		  return false;	  
	  }
      
	$("#tbCargue tr input:checkbox:checked").each(function(){
	  var idTraslado = $(this).val();
	  
	  if ($("#txtReverso"+idTraslado).val().trim()!=""){
	      objDatoReverso[objDatoReverso.length] = {
	        id_reverso:idTraslado,
	        periodo:$("#tdPeriodo"+idTraslado).text().trim(),
	        valor_reverso:$("#txtReverso"+idTraslado).val().trim(),
	        valor:$("#tdValor"+idTraslado).text().trim()
	     }
	      
	      if (parseInt($("#txtReverso"+idTraslado).val().trim()) > parseInt($("#tdValor"+idTraslado).text().trim())){
	          alert("El valor a reversar es mayor al valor");
	          banderaError++;
	          $("#txtReverso"+idTraslado).addClass("ui-state-error");
	        }
	  }else{
		    banderaError++;
		    $("#txtReverso"+idTraslado).addClass("ui-state-error");
	  }
	});
		if(banderaError>0)
			return false
	/**-----------------------------------------------**/
	$.ajax({
		url: URL+'asopagos/guardarNota.php',
		type: "POST",
		async: false,
		data: {v0:idt,v1:notas},
		dataType: "json",
		success: function(data){
			if(data>0){
				idnota=data;
				$.ajax({
					url:'reversarNoProcesados.php',
					type:'POST',
					async:false,
					data:{v0:idt,v1:objDatoReverso,v2:idnota,v3:idmotivo},
					dataType:'json',
					success:function(datos){
						if(datos>0){
							alert("Valor reversado!");
							limpiarCampos();
						}
						else{
							alert("No se pudo realizar el Reverso, comuniquese con el administrador del sistema, error: "+datos)
							continuar=0;
							return false
						}
					},
				})
				
			}			
		}
	});
	
}
