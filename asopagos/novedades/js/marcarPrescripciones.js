// funcion para obtener cookie en javascript
	function getCookie(c_name)
	{
		var i,x,y,ARRcookies=document.cookie.split(";");
		for (i=0;i<ARRcookies.length;i++)
		{
  			x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
  			y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
  			x=x.replace(/^\s+|\s+$/g,"");
  			if (x==c_name)
    			{
    				return unescape(y);
    			}// fin if
		}// fin for
  	}// fin getCookie
//funcion para obtener cookie en javascript

var paginaActual=null;
$(document).ready(function(){
	var URL = src();	
	
	$("#buscarTarjeta").click(function(){
		$("#tDatos tbody,tfoot").empty();
		//VALIDAR QUE EL NIT SEA NUMEROS
		if( $("#buscarPor").val()=="1"&& isNaN($("#idTarjeta").val()) ){
			$(this).next("span.Rojo").html("La Identificacion debe ser num\u00E9rico.").hide().fadeIn("slow");
				return false;
		}
		
		if($("#buscarPor").val()=='Seleccione..'){
			$(this).next("span.Rojo").html("Seleccione el criterio de b\u00FAsqueda.").hide().fadeIn("slow");
			return false;
		}

		if($("#idTarjeta").val()==''){
			$(this).next("span.Rojo").html("Ingrese la Identificacion \u00F3 Bono").hide().fadeIn("slow");
			$("#idEmpresa").focus();
			return false;
		}

		tarjetasPrescripciones(1,[$("#idTarjeta").val().toUpperCase(),$("#buscarPor").val()]);
	});//fin click
	
	/* Genera el listado automaticamente */
	//tarjetasPrescripciones(1,[$("#idTarjeta").val().toUpperCase(),$("#buscarPor").val()]);
	
	$("#buscarListado").click(function(){
		$("#tDatos tbody,tfoot").empty();
		var fecha=$("#fechaCorte").val();
		if(fecha.length==0){
			alert("Debe colocar la Fecha de Corte!");
			return false;
		}
		
		tarjetasPrescripciones(1,[$("#idTarjeta").val().toUpperCase(),$("#buscarPor").val()]);

	});//fin click
	
	
}); // dom ready


//Generar Lista de las prescripciones
function procesar01(){
//Genera reporte en excel
var fecha=$("#txtFecha").val(); //Fecha del sistema	
var fechcorte = $("#fechaCorte").val();
var usuario=$("#txtusuario").val();

url="http://"+getCookie("URL_Reportes")+"/asopagos/rptExcel038.jsp?v0="+ usuario + "&v1=" + fecha + "&v2=" + fechcorte;

window.open(url,"_NEW");
}

function tarjetasPrescripciones(pagina,parametros){
	var fechaCorte=$("#fechaCorte").val();
	paginaActual = pagina;
	$("#tDatos tbody").empty();
	var documento = "";
	var bono = "";
	if(parametros.length == 2){
		// buscar por
		if(parametros[1] == "documento")
			documento = parametros[0];
		else if(parametros[1] == "bono"){
			bono = parametros[0];
		}
	}
	dialogLoading('show');
	$.getJSON('contarPrescripciones.php?fechaCorte='+ fechaCorte +"&documento="+ documento +"&bono="+ bono,function(totalRegistros){
		var regsPorPagina = 30;
		var numPaginas = Math.ceil(totalRegistros/regsPorPagina);
		var strEnlacesPag = "";
		$("#span_num_empresas").html(totalRegistros);
		c=0;
		for(var i = pagina; i<=numPaginas; i++){
			if(c <= 10){
				strEnlacesPag += "<a href='#' id='pagina_"+ i +"' class='link_paginacion' >"+ i +"</a> ";
			}
			c++;	
		}
		
		var strEnlaceAnterior = "";
		if(pagina > 1)
			strEnlaceAnterior = "<a href='#' id='pagina_anterior'> << </a>";
		
		var strEnlaceSiguiente = "";
		if(pagina < numPaginas)
			strEnlaceSiguiente = "<a href='#' id='pagina_siguiente'> >> </a>";
		
		strEnlacesPag = strEnlaceAnterior + strEnlacesPag + strEnlaceSiguiente; 
		//carga las empresas de la BD
		$.getJSON('tarjetasPrescripciones.php?fechaCorte='+ fechaCorte + "&pagina="+ pagina +'&numRegistros='+regsPorPagina +'&bono='+ bono +"&documento="+ documento,function(data){
		  	if(data == 0){
		  		alert("no hay datos");
		  		dialogLoading("hide");
		  		return false;
		  		//$("#idEmpresa").val("");
		  		//$("#cmbIdCausal").val("0");
		  		//tarjetasPrescripciones(paginaActual,[$("#idEmpresa").val().toUpperCase(),$("#buscarPor").val()]);
		  	}
		  	dialogLoading("hide");
		  	$.each(data,function(i,fila){
				$("#tDatos").append("<tr><td style=text-align:center>"+fila.identificacion+"</td><td style=text-align:center>"+fila.tarjeta+"</td><td style=text-align:left>"+fila.SaldoCorte+"</td><td style=text-align:center>"+fila.Reverso+"</td><td style=text-align:center>"+fila.saldoActual+"</td><td style=text-align:center>"+fila.direccion+"</td><td style=text-align:center>"+fila.telefono+"</td><td><input type='checkbox' value='"+ fila.idtarjeta +"*"+ fila.SaldoCorte +"*"+ fila.saldoActual +"*"+ fila.identificacion +"*"+ fila.idpersona +"'/></td></tr>");
		  	});
		  	
		  	$("table.tablaR tr:even").addClass("zebra");
			//Dar un ID dinamico a los check box de la tabla de empresas
			$("table.tablaR td input:checkbox").each(function(index){
				//$(this).attr({"id":"chk"+index,"value": $(this).parents("tr").children("td:first").find("a").html()}).hide();//id de los check
			});
			
			/*$("table.tablaR td img").each(function(index,imagen){
				$(imagen).attr({"id":"img"+index,"value": $(imagen).parents("tr").children("td:first").find("a").html()});//id de los check
				$(imagen).bind('click',function(){
					if($(imagen).next("input:checkbox").is(":checked")){
						$(imagen).attr("src",URL+"imagenes/chk0.png");
						$(imagen).next("input:checkbox").attr("checked",false);
					}else{
						$(imagen).attr("src",URL+"imagenes/chk1.png");
						$(imagen).next("input:checkbox").attr("checked",true);
					}
				});
			});*/
		});//end post
		
		$("#tDatos tfoot").html("<tr><td colspan='6'>"+ strEnlacesPag +"</td></tr>");
		$(".link_paginacion").each(function(ind,enlace){
			$(enlace).bind('click',function(){
				var pag = $(this).attr("id").replace("pagina_","");
				tarjetasPrescripciones(pag,[]);
			});
		});
		$("#pagina_anterior").bind("click",function(){
			if(pagina > 1)
				tarjetasPrescripciones(parseInt(pagina)-1,[$("#idTarjeta").val().toUpperCase(),$("#buscarPor").val()]);
		});
		
		$("#pagina_siguiente").bind("click",function(){
			if(pagina < numPaginas)
				tarjetasPrescripciones(parseInt(pagina)+1,[$("#idTarjeta").val().toUpperCase(),$("#buscarPor").val()]);
		});
		
		$("#pagina_"+pagina).addClass("pagina_actual");
	});	
}

$(document).ready(function(){
	//Checkbox
	$("input[name=checktodos]").change(function(){
		$('input[type=checkbox]').each( function() {			
			if($("input[name=checktodos]:checked").length == 1){
				this.checked = true;
			} else {
				this.checked = false;
			}
		});
	});

	$("#periodoGiro").datepicker({
	    dateFormat: 'yymm',
	    changeMonth: true,
	    changeYear: true,
	    showButtonPanel: true,
	    onClose: function(dateText, inst) {
	    	var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
	        var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
	        $(this).datepicker('setDate', new Date(year, month, 1));
	   	}
	});
		
	$("#periodoGiro").focus(function () {
		$(".ui-datepicker-calendar").hide();
		$("#ui-datepicker-div").position({
			my: "center top",
			at: "center bottom",
			of: $(this)
		});
	});
	
	$("#fechaCorte").datepicker({
		maxDate:"+0D",
		changeMonth: true,
		changeYear: true
	});
	
});

$(function(){
	$("#dialog-reverso-mayor").dialog({
		autoOpen:false,
		width:640,
		hide: "clip",	
		open:function(event,ui){
		},//fin funcion open
		
		close:function(event,ui){
			$("#lidentificacion").html("");
			$("#lbono").html("");	
			$("#lsaldo").html("");	
		}
	});//fin dialog
});

function guardarMarcadas(){
	if(confirm('Realmente desea continuar con el proceso?'))
	{   
	var seleccionadas = [];
	var marcadastarjetas = [];
	var contador = 0;
	var arrayjson = [];
	
	$("table.tablaR td input:checked").each(function(i){
		var ide = $(this).val();//extraigo el valor del value del check cargado dinamicamente
		seleccionadas[i] = ide;

		arreglo = ide.split('*');
		
		var idt=arreglo[0];
		var saldocorte=arreglo[1];
		var saldoAct=arreglo[2];
		var documento=arreglo[3];
		var idpersona=arreglo[4];
		
		cont=saldoAct-saldocorte;
		if(cont < 0){
			//alert("No puede reversar un valor mayor al saldo!");
			marcadastarjetas[contador] = idt;
			contador++;
		}else{
			
			arrayjson[arrayjson.length] = {
					id_tarjeta : idt,
					id_persona : idpersona,
					reverso : saldocorte,
					documento : documento
				};
		}
	});//Fin valores del check

	if($("#tDatos tbody :checkbox:checked").length > 0){
		
 		var periodo=$("#periodoGiro").val();
 		var nota=$("#notas").val();
 		
 		if(nota.length==0){
 			alert("Digite las notas!");
 			return false;
 		}
 		if(nota.length<10){
 			alert("Las notas son muy cortas...!");
 			return false;
 		}
 		if(periodo.length==0){
 			alert("Seleccion el periodo giro!");
 			return false;
 		}

 		$.getJSON('guardarNotaPrescripcion.php',{v0:arrayjson,v1:nota},function(data){
			if(data == 0)
				alert("Hubo problemas al guardar la nota");
			else{
					idnota=data;
					/*alert("Registros guardados.");*/
				}
			//Reversos
	 		$.getJSON('marcarGuardarReversos.php',{v0:arrayjson, v1:idnota,v2:periodo},function(data){
				if(data == 0)
					alert("Hubo problemas al momento de reversar los valores");
				else{
						alert("Registros reversados.");
					}
	 		}); //Fin getJSON 2
 		});//Fin getJSON 1
 		
 		$.getJSON('marcarGuardarPrescripciones.php',{v0:arrayjson},function(data){
			if(data == 0){
				alert("Prescripciones guardadas.");
				$("#periodoGiro").val("");
				$("#notas").val("");
			}else{
					alert("Hubo problemas al momento de ingresar la prescripcion.");
				}
			
			tarjetasPrescripciones(paginaActual,[$("#idTarjeta").val().toUpperCase(),$("#buscarPor").val()]);
 		}); //Fin getJSON

 		//Buscar tarjetas con reverso mayor al saldo actual
		$.getJSON('buscarTarjetasPrescripcion.php',{v0:marcadastarjetas.join()},function(datos){
			if(datos!=0){
				$.each(datos,function(i,fila){
				$("#DivTable").append("<tr><td style=text-align:center>"+fila.identificacion+"</td><td style=text-align:center>"+fila.bono+"</td><td style=text-align:left>"+fila.saldo+"</td></tr>");
				});
				$("#dialog-reverso-mayor").dialog('open');
			}
		});	//fin buscar tarjeta
 		
 	}else{
 		alert("Debe marcar al menos una Tarjeta.");
 		}
	} // Fin confirm
}