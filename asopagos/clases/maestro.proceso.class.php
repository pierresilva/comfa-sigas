<?php

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';

include_once $raiz.DIRECTORY_SEPARATOR. 'phpComunes' . DIRECTORY_SEPARATOR . 'cargar_archivo.class.php';

include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'utilitario.class.php';

class MaestroProceso{
	
	private $idProceso = 0;
	private $tipo = null;
	private $usuario = null;
	private $rutaCargados;
	
	private static $con = null;
	
	private $arrError = array("error"=>0,"data"=>null,"descripcion"=>"");
	
	function __construct(){
		
		//Conexion
		try{
			self::$con = IFXDbManejador::conectarDB();
			if( self::$con->conexionID==null ){
				throw new Exception(self::$con->error);
			}
		}catch(Exception $e){
			echo $e->getMessage();
			exit();
		}
	}
	
	public function setIdProceso($idProceso){
		$this->idProceso = $idProceso;
	}
	public function setTipo($tipo){
		$this->tipo = $tipo;
	}
	public function setUsuario($usuario){
		$this->usuario = $usuario;
	}
	public function setRutaCargados($rutaCargados){
		$this->rutaCargados = $rutaCargados;
	}
	
	/**
	 * Metodo encargado de inicializar el proceso
	 * 
	 * @param unknown_type $tipo [SALDOS]
	 * @param unknown_type $usuario
	 * @return multitype:number NULL string [error[0:no existen error, 1: existe error], descripcion: Descripcion del proceso, data[id_proceso]]
	 */
	public function iniciar_proceso(){
		
		if($this->estado_inicio_proceso()==true){
			//Iniciar proceso
			$query = "INSERT INTO dbo.aportes160 (tipo,estado,usuario) VALUES ('$this->tipo','INICIO','$this->usuario')";
			$this->idProceso = self::$con->queryInsert($query,"aportes160");
				
			if($this->idProceso === null){
				$this->arrError["error"] = 1;
				$this->arrError["descripcion"] = "Error: No fue posible inicializar el procesos";
			}else{
				$this->arrError["error"] = 0;
				$this->arrError["descripcion"] = "El proceso se inicializo correctamente";
				$this->arrError["data"]["id_proceso"] = $this->idProceso;
			}
		}
		
		return $this->arrError;
	}
	
	public function iniciar_reproceso(){
		if($this->estado_inicio_proceso()==true){
			//Remover estado de error para las etapas con error
			
			if($this->tipo == "SALDOS"){
				$resultado = 0;
				$sentencia = self::$con->conexionID->prepare ( "EXEC [asopagos].[sp_Reprocesar_Saldo]
						@id_proceso = $this->idProceso
						, @usuario = '$this->usuario'
						, @resultado = :resultado" );
				$sentencia->bindParam ( ":resultado", $resultado, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 4000 );
				$sentencia->execute ();
				
				if($resultado==0){
					$this->arrError["error"] = 0;
					$this->arrError["descripcion"] = "El reproceso se inicializo correctamente";
				}else{
					$this->arrError["error"] = 1;
					$this->arrError["descripcion"] = "Error el reproceso no fue posible actualizarlo";
				}
				
			}
		}
		return $this->arrError;
	}
	
	/**
	 * Metodo encargado de inicializar la etapa del proceso
	 *
	 * @param unknown_type $tipo [SALDOS]
	 * @param unknown_type $usuario
	 * @return multitype:number NULL string [error[0:no existen error, 1: existe error], descripcion: Descripcion del proceso
	 * 										, data[id_etapa_proceso,etapa_proceso,id_traza_proceso]]
	 */
	public function iniciar_etapa_proceso(){
		//Obtener etapa
		$arrResultado = $this->fetch_etapa_procesar();
		$arrDataTrazaProce = array();
		
		if(count($arrResultado)>0){
			
			$arrResultado = $arrResultado[0];
			
			$idEtapaProceso = $arrResultado["id_etapa_proceso"];
			$idTrazaProceso = intval($arrResultado["id_traza_proceso"]);

			//Verificar si ya existe una traza proceso inicializada
			if($idTrazaProceso==0){
				//Guardar la traza proceso
				$query = "INSERT INTO dbo.aportes162 (id_proceso,id_etapa_proceso,usuario)VALUES ($this->idProceso, $idEtapaProceso,'$this->usuario')";
				$idTrazaProceso = self::$con->queryInsert($query,"aportes162");
				
				if($idTrazaProceso===null){
					$this->arrError["error"] = 1;
					$this->arrError["descripcion"] = "Error: No fue posible inicializar la traza";
				}else{
					$this->arrError["error"] = 0;
					$this->arrError["descripcion"] = "La traza se inicializo correctamente";
				}
			}
			
			$arrDataTrazaProce["id_etapa_proceso"] = $idEtapaProceso;
			$arrDataTrazaProce["etapa_proceso"] = $arrResultado["etapa_proceso"];
			$arrDataTrazaProce["id_traza_proceso"] = $idTrazaProceso;			
			
			$this->arrError["data"] = $arrDataTrazaProce;
			
		}else{
			$arrError["error"] = 1;
			$arrError["descripcion"] = "Error: No se puedo obtener la etapa a procesar";
		}
		
		return $this->arrError;
	}
	
	/**
	 * Metodo encargado de terminar el proceso
	 *
	 * @param unknown_type $estado Estado del proceso [FIN_EXITOSO, FIN_ERROR] 
	 * @return multitype:number NULL string [error[0:no existen error, 1: existe error], descripcion: Descripcion del proceso]
	 */
	public function terminar_proceso(){
		
		$estado = "";
		$query = "SELECT COUNT(*) AS contador FROM aportes162 WHERE id_proceso=$this->idProceso AND procesado='E'";
		$resultado = Utilitario::fetchConsulta($query,self::$con);
		
		if(intval($resultado[0]["contador"])>0)
			$estado="FIN_ERROR";
		else 
			$estado="FIN_EXITOSO";
				
		$query = "UPDATE dbo.aportes160
					SET estado = '$estado',fecha_fin = getdate()
				WHERE id_proceso=$this->idProceso";
		
		if(self::$con->queryActualiza($query)>0){
			//El proceso se cerro correctamente
			$this->arrError["error"] = 0;
			$this->arrError["descripcion"] = "El proceso se termino con: ".$estado;
		}else{
			$this->arrError["error"] = 1;
			$this->arrError["descripcion"] = "Error: No fue posible terminar el procesos";
		}
			
		return $this->arrError;
	}
	
	/**
	 * Metodo encargado de terminar la etapa proceso
	 *
	 * @param unknown_type $idTrazaProceso El id de la etapa proceso ha terminar
	 * @param unknown_type $procesado Estado de la etapa proceso [E, S]
	 * @return multitype:number NULL string [error[0:no existen error, 1: existe error], descripcion: Descripcion del proceso]
	 */
	public function terminar_etapa_proceso( $idTrazaProceso, $procesado, $etapaProceso){
		
		//Iniciar transaccion
		self::$con->inicioTransaccion();
		
		$banderaError = 0;
		
		$query = "UPDATE dbo.aportes162
					SET procesado = '$procesado',	fecha_fin = getdate()
				WHERE id_traza_proceso=$idTrazaProceso";
		
		if(self::$con->queryActualiza($query)>0){
			
			//Verificar si el proceso se debe terminar
			if($etapaProceso=="fin"){
				$arrResultado = $this->terminar_proceso();
				
				if($arrResultado["error"]==1){
					//Error al terminar el proceso
					$banderaError = 1;
					$this->arrError["error"] = 1;
					$this->arrError["descripcion"] = $arrResultado["descripcion"];
				}
			}
		}else{
			//Error al terminar la etapa proceso
			$banderaError = 1;
			$this->arrError["error"] = 0;
			$this->arrError["descripcion"] = "Error: La etapa proceso no se termino";
		}
		
		if($banderaError==1){
			//Rolback transaccion
			self::$con->cancelarTransaccion();
		}else{
			//Commit transaccion
			self::$con->confirmarTransaccion();
		}
		return $this->arrError;
	}
	
	/**
	 * Metodo encargado de guardar los datos del archivo

	 */
	public function guardar_archivo($nombreArchivo, $tipoArchivo,$fechaSaldo,$cantidadRegistros, $hora){

		$query = "INSERT INTO aportes149
					(archivo,tipoarchivo,fechasistema,usuario,registros,hora)
				VALUES('$nombreArchivo','$tipoArchivo', '$fechaSaldo', '$this->usuario',$cantidadRegistros, '$hora')";
		
		$resultado = self::$con->queryInsert($query,"aportes149");
		
		$idArchivo = 0;
		if($resultado!==null){
			$idArchivo = self::$con->conexionID->lastInsertId("aportes149");
		}
		return $idArchivo;
	}
	
	/**
	 * Metodo encargado de guardar la relacion del archivo con el proceso
	 */
	public function guardar_archivo_proceso($idArchivo, $totalLineas, $lineaActual = 0){
	
		$query = "INSERT INTO dbo.aportes159 (id_proceso, id_archivo, total_lineas, linea_actual,	usuario)
				VALUES($this->idProceso,$idArchivo, $totalLineas, $lineaActual, '$this->usuario')";
	
		$resultado = self::$con->queryInsert($query,"aportes159");
	
		$idArchivoProceso = 0;
		if($resultado!==null){
			$idArchivoProceso = self::$con->conexionID->lastInsertId("aportes159");
		}
		return $idArchivoProceso;
	}
	
	/**
	 * Metodo encargado de actualizar el atributo linea_actual del archivo proceso
	 */
	public function update_linea_actual_archivo_proceso($idArchivoProceso, $lineaActual){
		$query = "UPDATE dbo.aportes159 SET linea_actual = $lineaActual WHERE id_archivo_proceso=$idArchivoProceso";	
		return self::$con->queryActualiza($query);
	}

	public function fetch_archivo_proceso($arrAtributoValor){
	
		$attrEntidad = array(
				array("nombre"=>"id_proceso","tipo"=>"NUMBER","entidad"=>"a159")
				,array("nombre"=>"archivo","tipo"=>"TEXT","entidad"=>"a149")
				,array("nombre"=>"tipoarchivo","tipo"=>"TEXT","entidad"=>"a149"));
	
		$filtroSql = Utilitario::obtenerFiltro($attrEntidad,$arrAtributoValor);
	
		$query = "SELECT a159.id_archivo_proceso,a159.id_proceso , a159.id_proceso,	a159.id_archivo, a159.total_lineas, a159.linea_actual
					, a149.archivo, a149.fechasistema AS fecha_sistema_archivo, a149.usuario AS usuario_archivo
				FROM aportes159 a159
					INNER JOIN aportes149 a149 ON a149.idcontrol=a159.id_archivo
				$filtroSql";
	
		return Utilitario::fetchConsulta($query,self::$con);
	}
	
	/**
	 * Metodo encargado de verificar el estado para el inicio del nuevo proceso o reproceso
	 * @return boolean [true: Procesar, false:no procesar]
	 */
	public function estado_inicio_proceso(){
		$query = "
					SELECT
						count(*) AS cantidad, 'inicializado' AS bandera 
					FROM aportes160
					WHERE estado NOT IN ('FIN_ERROR','FIN_EXITOSO')
					UNION
					SELECT 
						count(*) AS cantidad, 'minuto' AS bandera 
					FROM aportes160 
					WHERE dateadd(mi,5,fecha_fin)>(getdate())";
		
		$arrResultado = Utilitario::fetchConsulta($query,self::$con);
		
		$banderaCont = 0;
		foreach($arrResultado as $row){
			if($row["bandera"] == "inicializado" && $row["cantidad"]>0){
				//Existen procesos inicializados
				$this->arrError["error"] = 1;
				$this->arrError["descripcion"] = "Error: Existen procesos inicializados";
				$banderaCont++;
				break;
			}else if($row["bandera"] == "minuto" && $row["cantidad"]>0){
				//Aun no han pasado 5 minutos despues del ultimo proceso
				$this->arrError["error"] = 1;
				$this->arrError["descripcion"] = "Error: Debes dejar pasar 5 minutos despues del ultimo proceso";
				$banderaCont++;
				break;
			}
		}
		
		return $banderaCont==0?true:false;
	}
	
	public function fetch_proceso_completo($arrAtributoValor){
		
		$attrEntidad = array(
				array("nombre"=>"id_proceso","tipo"=>"NUMBER","entidad"=>"a160")
				,array("nombre"=>"tipo","tipo"=>"TEXT","entidad"=>"a160")
				,array("nombre"=>"estado","tipo"=>"TEXT","entidad"=>"a160"));
		
		$filtroSql = Utilitario::obtenerFiltro($attrEntidad,$arrAtributoValor);
	
		$query = "SELECT
					a160.id_proceso, a160.tipo, a160.fecha_inicio AS fecha_inici_proce, a160.fecha_fin AS fecha_fin_proce, a160.estado AS estado_proceso, a160.usuario, a160.fecha_creacion
					, a161.id_etapa_proceso, a161.etapa_proceso, a161.descripcion, a161.orden
					, a162.id_traza_proceso, a162.procesado, a162.fecha_inicio AS fecha_inici_traza, a162.fecha_fin AS fecha_fin_traza	
				FROM aportes160 a160
					INNER JOIN aportes161 a161 ON a161.tipo='{$arrAtributoValor["tipo"]}'
					LEFT JOIN aportes162 a162 ON a162.id_proceso=a160.id_proceso AND a162.id_etapa_proceso=a161.id_etapa_proceso
				$filtroSql
				ORDER BY a161.orden ASC";
	
		return Utilitario::fetchConsulta($query,self::$con);
	}
	
	public function fetch_ultimo_proceso($arrAtributoValor){
		$query = "SELECT
					a160.id_proceso, a160.tipo, a160.fecha_inicio AS fecha_inici_proce, a160.fecha_fin AS fecha_fin_proce, a160.estado AS estado_proceso, a160.usuario, a160.fecha_creacion
					, a161.id_etapa_proceso, a161.etapa_proceso, a161.descripcion, a161.orden
					, a162.id_traza_proceso, isnull(a162.procesado,'') AS procesado, a162.fecha_inicio AS fecha_inici_traza, a162.fecha_fin AS fecha_fin_traza
				FROM aportes160 a160
					INNER JOIN aportes161 a161 ON a161.tipo = '{$arrAtributoValor["tipo"]}'
					LEFT JOIN aportes162 a162 ON a162.id_proceso=a160.id_proceso AND a162.id_etapa_proceso=a161.id_etapa_proceso
				WHERE a160.tipo = '{$arrAtributoValor["tipo"]}'
					AND a160.id_proceso = (SELECT TOP 1 id_proceso FROM aportes160 ORDER BY fecha_inicio DESC)
				ORDER BY a161.orden ASC";
	
		return Utilitario::fetchConsulta($query,self::$con);
	}
	
	/**
	 * Metodo encargado de obtener la etapa a seguir
	 * @param unknown_type $tipo [SALDOS]
	 */
	public function fetch_etapa_procesar(){
		$resultado = array();
		
		$query = "SELECT TOP 1 
					a161.id_etapa_proceso, a161.etapa_proceso, a161.descripcion, a161.tipo, a161.orden, a161.estado AS estad_etapa_proce
					, a162.id_traza_proceso, a162.procesado, a162.fecha_inicio
				FROM aportes161 a161
					LEFT JOIN aportes162 a162 ON a162.id_etapa_proceso=a161.id_etapa_proceso  AND a162.id_proceso=$this->idProceso
				WHERE a161.tipo='$this->tipo'
					AND a161.id_etapa_proceso NOT IN (
						SELECT id_etapa_proceso 
						FROM aportes162  
						WHERE id_proceso=$this->idProceso AND procesado IN ('S','E') 
					)
				ORDER BY a161.orden ASC";
	
		$resultado = Utilitario::fetchConsulta($query,self::$con);
		return $resultado;
	}
	
	public function verifica_directorio($ruta){
		if (is_dir($ruta)){
			return true;
		}
		if(!mkdir($ruta, 0, true)){
			return false;
		}
		return true;
	}
	
	
	public function fetch_archivo_cargado($uriPlanoCargado){
		$archivos = array();
		
		//Obtener los archivos del directorio
		$openDirCargados = opendir($uriPlanoCargado);
		while ($archivo = readdir($openDirCargados))
		{
			if(strlen($archivo)>2){
				$archivos[]=$archivo;
			}
		}
		closedir($openDirCargados);
		
		return $archivos;
	}
	
	/**
	 * Metodo de trasladar de directorio el archivo
	 * 
	 * @param unknown_type $uriActual
	 * @param unknown_type $uriDestino
	 */
	public function trasladar_archivo($uriActual,$uriDestino){
		if(file_exists($uriActual)){
			copy ( $uriActual, $uriDestino );
			unlink ( $uriActual );
		}
	}
	
	/**
	 * Metodo encargado de guardar el logs en archivo plano
	 * 
	 * @return number [0:Error, 1:ok]
	 */
	public function guardar_logs_archivo(){
		//$uriPlanoProcesado = $ruta_cargados . 'planillas' . DIRECTORY_SEPARATOR . 'procesados' . DIRECTORY_SEPARATOR;
		return 1;
	}
	
	/**
	 * Metodo encargado de cargar de forma manual los archivos al servidor
	 * 
	 * @param unknown_type $uriPlanoCargado
	 * @param unknown_type $extencionArchivo
	 * @param unknown_type $arrTipoArchivo
	 * @param unknown_type $arrFiles
	 * 
	 * @return boolean [true: Ejecucion exitosa, false: Error]
	 */
	public function carga_archivo_manual($uriPlanoCargado,$extencionArchivo, $arrTipoArchivo,$arrFiles){
		
		$resultado = true;
		
		$cargarArchivo = new CargarArchivo();
		$cargarArchivo->setDirectorio($uriPlanoCargado);
		$cargarArchivo->setExtencionArchivo($extencionArchivo);
		$cargarArchivo->setTipoArchivo($arrTipoArchivo);
		
		foreach( $arrFiles as $rowFile){
			try{
				$cargarArchivo->setNombreArchivo($rowFile['name']);
				$cargarArchivo->setFile($rowFile);
				
				if(!$cargarArchivo->cargar()){
					$resultado = false;
					break;
				}
				
			}catch(Exception $e){
				$resultado = false;
				break;
			}
		}
		
		return $resultado;
	}
	
	/**
	 * Retorna los valores obtenidos en la consulta
	 * @return multitype:array
	 */
	/*private function fetchConsulta($querySql){
		$resultado = array();
		$rs = self::$con->querySimple($querySql);
		while($row = $rs->fetch())
			$resultado[] = array_map("trim",$row);
		//$resultado[] = array_map("utf8_encode",$row);
		return $resultado;
	}*/
}