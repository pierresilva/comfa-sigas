/*
* @autor:      Ing. Orlando Puentes
* @fecha:      8/10/2010
* objetivo:
*/

var URL=src();
var idt=0;
var continuar=1;
var max=0;
var nuevoR=0;
var aprobado=0;

$().ready(function(){
//Dialog ayuda
$("#ayuda").dialog({
		 	autoOpen: false,
			height: 500,
			width: 800,
			draggable:true,
			modal:false,
			open: function(evt, ui){
					$('#ayuda').html('');
					$.get(URL +'help/pignoracion/manualayudaAnularpignoracion.html',function(data){
							$('#ayuda').html(data);
					});
			 }
		});
//Dialog Colaboracion en linea
$("#dialog-form2").dialog({
		autoOpen: false,
		height: 400,
		width: 500,
		modal: true,
		buttons: {
			'Enviar': function() {
			var bValid = true;
			var campo=$('#notas').val();
			var campo0=$.trim(campo);
			if (campo0==""){
				$(this).dialog('close');
					return false;
				}
			var campo1=$('#usuario').val();
			var campo2="pignoracion.php";
			$.post(URL+'phpComunes/colaboracion.php',{v0:campo0,v1:campo1,v2:campo2},
			function(datos){
				if(datos=='1'){
					alert("Su comentario fue enviado correctamente!. Gracias por participar en el mejoramiento de SIGAS");
				}else{
					alert(datos);
				}
		});
		$(this).dialog('close');
		},
		Cancelar: function() {
			$(this).dialog('close');
			$("#dialog-form2").dialog("destroy");
		}
	}
	});
});//fin ready

//Dialog Ayuda
function mostrarAyuda(){
	$("#ayuda").dialog('open' );
	}

function notas(){
	$("#dialog-form2").dialog('open');
	}

function buscarPagare(){
	var idp=$("#txtId").val();
	idp=espacios(idp);
	if(idp.length==0)
	 {return 0;}
	$.getJSON("buscarPagare.php",{v0:idp},function(data){
		if(data==0){
			MENSAJE("Lo lamento ese pagare no existe!");
			return false;
			}
			$.each(data,function(i,fila){
				var nom = $.trim(fila.pnombre)+" "+$.trim(fila.snombre)+" "+$.trim(fila.papellido)+" "+$.trim(fila.sapellido);
				$("#valor").html(formatCurrency(fila.valor));
				$("#afiliado").html(nom);
				})
		})
	}

function actualizar(){
	var idp=$("#txtId").val();
	idp=espacios(idp);
	if(idp.length==0)
	 {return 0;}
	 var notas=$("#notas").val();
	 if(notas.length==0){
		 MENSAJE("Digite las notas!");
		 $("#notas").focus();
		 return false;
		 }
	if(confirm("Esta seguro de ANULAR el Pagare Nro. "+idp+"?")==true){
		$.getJSON("actualizarPagare.php",{v0:idp,v1:notas},function(data){
			if(data==0){
				MENSAJE("El pagare no se pudo anular!");
				return false;
				}
			MENSAJE("El pagare fue anulado!")
			limpiarCampos();
			})
		}
	}
	
function limpiarCampos(){
$("table.tablero input:text").val('');
//$("table.tablero  select").val('0');
$("#beneficiarios tr:not(':first')").remove();
//$("#valor2").css({fontSize:"18px",color:"red"}).html(formatCurrency(0));
$("#valor").html("&nbsp;");
$("#afiliado").html("&nbsp;");
$("#txtId").focus();
}

function MENSAJE(str){
	var dialog="<div title='MENSAJE SIGAS' id='info'></div>";
	$("#info").dialog("destroy").remove();
	$(dialog).appendTo("body");
    $("#info").append("<p>"+str+"</p>");
    $("#info").dialog({
		  modal:true,
		  resizable:false
		  });      
    $("#convenio").focus();
}
