<?php

/* autor:       orlando puentes
 * fecha:       08/10/2010
 * objetivo:    
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$url=$_SERVER['PHP_SELF'];
include_once $raiz.DIRECTORY_SEPARATOR.'phpComunes'.DIRECTORY_SEPARATOR.'auditoria.php';

include_once $raiz.DIRECTORY_SEPARATOR.'config.php';

auditar($url);
include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'p.definiciones.class.php';

$objClase=new Definiciones;
?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head><TITLE>Pignoracion</TITLE>
<META http-equiv=Content-Type content="text/html; charset=iso-8859-1">
<meta http-equiv="Content-Script-Type" content="text/javascript; charset=iso-8859-1" />
<META content="MSHTML 6.00.2900.2180" name=GENERATOR>
<link type="text/css" href="../css/Estilos.css" rel="stylesheet" />
<link type="text/css" href="../css/estilo_tablas.css" rel="stylesheet"/>
<link type="text/css" href="../css/formularios/base/ui.all.css" rel="stylesheet" />
<link type="text/css" href="../css/marco.css" rel="stylesheet" />
<script type="text/javascript" src="../js/jquery-1.3.2.min.js"></script>
<!--<script type="text/javascript" src="../../js/jquery-1.4.2.js"></script> -->
<script type="text/javascript" src="../js/formularios/ui/ui.core.js"></script>
<script type="text/javascript" src="../js/formularios/ui/ui.tabs.js"></script>
<script type="text/javascript" src="../js/formularios/ui/ui.draggable.js"></script>
<script type="text/javascript" src="../js/formularios/ui/ui.resizable.js"></script>
<script type="text/javascript" src="../js/formularios/ui/ui.dialog.js"></script>
<script type="text/javascript" src="../js/formularios/ui/ui.datepicker.js"></script>
<script type="text/javascript" src="../js/comunes.js"></script>
<script type="text/javascript" src="pignoracion.js"></script>
<script type="text/javascript" src="../js/numerosLetras.js"></script>
<script type="text/javascript">
shortcut.add("Shift+F",function() {
		                                var URL=src();
		var url=URL+"aportes/trabajadores/consultaTrabajador.php";
    	window.open(url,"_blank");
    },{
	'propagate' : true,
	'target' : document 
        });        
</script>

</head>
<body>
<center>
<br /><br />
<table width="90%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="13" height="29" class="arriba_iz">&nbsp;</td>
    <td class="arriba_ce"><span class="letrablanca">::&nbsp;Pignoraci&oacute;n &nbsp;::</span></td>
    <td width="13" class="arriba_de" align="right">&nbsp;</td>
  </tr>
  <tr>
<td class="cuerpo_iz">&nbsp;</td>
<td class="cuerpo_ce">
<img src="../imagenes/spacer.gif" width="1" height="1"/>
<img src="../imagenes/menu/nuevo.png" title="Nuevo" width="16" height="16" style="cursor:pointer" onClick="nuevaPignoracion();" />
<img src="../imagenes/spacer.gif" width="1" height="1"/>
<img src="../imagenes/menu/grabar.png" width="16"  height=16 style="cursor:pointer" title="Guardar" onClick="guardarPignoracion('<?php echo URL_REPORTES_JSP; ?>');" id= "bGuardar" /> 
<img src="../imagenes/spacer.gif" width="1" height="1"/>
<!--<img src="../imagenes/menu/modificar.png" title="Actualizar" width="16" height="16" onClick="validarCampos(2)" style="cursor:pointer" />  -->
<img src="../imagenes/spacer.gif" width="1" height="1"/>
<!--<img src="../imagenes/menu/imprimir.png" width="16" height="16" style="cursor:pointer" title="Imprimir" onClick="window.open(URL+'centroReportes/aportes/radicacion/reporte01.php','_blank')" />-->
<img src="../imagenes/spacer.gif" width="1" height="1"/><img src="../imagenes/spacer.gif" width="1" height="1"/>
<img src="../imagenes/menu/informacion.png" width="16" height="16" style="border:none; cursor:pointer" title="Manual" onClick="mostrarAyuda();" />
<img src="../imagenes/menu/notas.png" width="16" height="16" style="cursor: pointer" title="Colaborac&oacute;on en l&oacute;nea" onClick="notas();" />
</td>
     <td class="cuerpo_de">&nbsp;</td>
  </tr>
  <tr>
    <td class="cuerpo_iz">&nbsp;</td>
    <td class="cuerpo_ce">
    <div id="resultado" style="font-weight: bold;font-size: 14px;color:#FF0000"></div></td>
    <td class="cuerpo_de">&nbsp;</td>
  </tr>
  <tr>
    <td class="cuerpo_iz">&nbsp;</td>
    <td class="cuerpo_ce">
	<table width="95%" border="0" cellspacing="0" class="tablero">
      <tr>
        <td>Fecha</td>
        <td><input name="txtfecha" id="txtfecha" class="box1"/></td>
        <td>Pagar&eacute; Nro</td>
        <td><input name=txtId class=box1 id="txtId" readonly /></td>
      </tr>
      <tr>
        <td>Tipo documento</td>
        <td><select name="select" id="select" class="box1">
          <option value="0" selected="selected">Seleccione...</option>
          <?php
		$consulta = $objClase->mostrar_datos(1,2);
		while($row=mssql_fetch_array($consulta)){
		echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
			}
        ?>
        </select>
              <img src="../imagenes/menu/obligado.png" width="12" height="12" /></td>
        <td>C&eacute;dula Trabajador</td>
        <td><input name="cedula" type="text" class="box1" id="cedula" onBlur="buscarTrabajador(this.value);"  />
              <img src="../imagenes/menu/obligado.png" width="12" height="12"> 
            </td>
      </tr>
      <tr>
        <td width="25%">Convenio</td>
        <td colspan="3"><select name="convenio" class="boxlargo" id="convenio">
          <option value="0" selected="selected">Seleccione...</option>
          <?php
		$consulta = $objClase->mostrar_datos(25);
		while($row=mssql_fetch_array($consulta)){
		echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
			}
        ?>
        </select>
              <img src="../imagenes/menu/obligado.png" width="12" height="12"> 
            </td>
        </tr>
      <tr>
      <td>Valor a Pignorar</td>
      <td colspan="3"><input name="valor" type="text" class="box1" id="valor" readonly="readonly" style="font-size: 12;color: red" />
              <img src="../imagenes/menu/obligado.png" width="12" height="12"> 
            </td>
        </tr>
             
       </table>
      <div  id="datost" style="width:94%; border:1px dashed #CCC; margin:10px ; padding:10px; display:none; " >
      <table width="100%" border="0" cellpadding="5" cellspacing="0" class="tablero" style="border-collapse:collapse; border:1px solid #CCC">
        <tr>
          <td bgcolor="#ECFFB0"><strong>Trabajador</strong></td>
          <td bgcolor="#ECFFB0"><strong>Salario</strong></td>
          <td bgcolor="#ECFFB0"><strong>Fecha Afiliaci&oacute;n</strong></td>
          <td bgcolor="#ECFFB0"><strong>Empresa</strong></td>
        </tr>
        <tr>
          <td id="tdNombre"></td>
          <td id="tdSalario"></td>
          <td id="tdFechaAfilia"></td>
          <td id="tdEmpresa"></td>
        </tr>
      </table>
          <h3>HIST&Oacute;RICO DE PIGNORACIONES</h3>
      <table width="100%" border="0" cellpadding="5" cellspacing="0" class="tablero" style="border-collapse:collapse; border:1px solid #CCC" id="historico">
        <tr>
          <td bgcolor="#ECFFB0"><strong>Id pignoraci&oacute;n</strong></td>
          <td bgcolor="#ECFFB0"><strong>Convenio</strong></td>
          <td bgcolor="#ECFFB0"><strong>Valor</strong></td>
          <td bgcolor="#ECFFB0"><strong>Fecha</strong></td>
          <td bgcolor="#ECFFB0"><strong>Estado</strong></td>
          <td bgcolor="#ECFFB0"><strong>Saldo</strong></td>
          <td bgcolor="#ECFFB0"><strong>Fec cancelaci�n</strong></td>
        </tr>
      </table>
      <div id="capaBeneficiarios" style=" display:none;">
          <h3>BENEFICIARIOS CON SUBSIDIO &Uacute;LTIMOS TRES PERIODOS</h3>
        <table WIDTH="100%" border="0" cellpadding="5" class="tablero" style="border-collapse:collapse; border:1px solid #CCC" id="beneficiarios">
          <tbody>
            <tr>
                <td bgcolor="#ECFFB0"><strong>Periodo</strong></td>
                <td bgcolor="#ECFFB0"><strong>ID Benef</strong></td>
                <td bgcolor="#ECFFB0"><strong>Nombres</strong></td>
                <td bgcolor="#ECFFB0"><strong>Valor</strong></td>
                <td bgcolor="#ECFFB0"><strong>F Nace</strong></td>
                <td bgcolor="#ECFFB0"><strong>Edad</strong></td>
                <td bgcolor="#ECFFB0"><strong>Cap T</strong></td>
                <td bgcolor="#ECFFB0"><strong>T Benef</strong></td>
                <td bgcolor="#ECFFB0"><strong>Ind Pago</strong></td>
            </tr>
          </tbody>
        </table>

          
          <p id="datos2" class="big">&nbsp;Tiene derecho a PIGNORAR hasta: <span id="valor2"></span></p>
      <img src="../imagenes/verMas.png" width="143" height="30" title="Ver mas"  onClick="verMas();" style="cursor:pointer; display:none;"  />
      </div><!-- fin capa beneficiarios -->
      </div>
      
      </td>
    <td class="cuerpo_de">&nbsp;</td>
  </tr>
   <tr>
    <td class="abajo_iz" >&nbsp;</td>
    <td class="abajo_ce" ></td>
    <td class="abajo_de" >&nbsp;</td>
  </tr>
</table>

</center>
<input type="hidden" name="pageNum" value="0" />

<input type="hidden" name="valorp" id="valorp" />
<input type="hidden" name="txtIdCliente" value="" />
 
 <!-- VENTANA DIALOGVER MAS-->
<div id="verMas" title="INFORMACI�N ADICIONAL" style="display:none;">
  <h4>INFORMACI&Oacute;N DE TRAYECTORIAS</h4>
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="box-table-a" style="margin:0" id="trayectoria">
  <tr>
    <th>Nit</th>
    <th>Empresa</th>
    <th>Fecha de Ingreso</th>
    <th>Fecha de retiro</th>
  </tr>
</table>

  <h4>INFORMACI&Oacute;N PLANILLA &Uacute;NICA</h4>
    
    <table width="100%" border="0" cellspacing="0" cellpadding="0"  id="planilla" class="box-table-a" style="margin:0"  >
  <tr>
    <th>Nit</th>
    <th>Periodo</th>
    <th>Fecha de pago</th>
    <th>Ingreso</th>
    <th>Retiro</th>
  </tr>
</table>
    
  <h4>INFORMACI&Oacute;N BENEFICIARIOS</h4>
  <table width="100%" border="0" cellspacing="0" cellpadding="0"  class="box-table-a" style="margin:0" id="beneficiarios">
  <tr>
    <th>Documento</th>
    <th>Nombres</th>
    <th>Parentesco</th>
    <th>Estado;</th>
    <th>Giro</th>
  </tr>
</table>

</div>
<!-- colaboracion en linea -->
<div id="dialogo-archivo" title="Archivo banco">
<div id="progreso" style="display: none; font-size: 15pt; font-weight: bold;">Procesado(s) <span id="pg">0</span> de <span id="tt"></span> archivo(s)</div>
<div id="log"></div>
</div>
<div id="dialog-form2" title="Colaboraci&oacute;n en l&iacute;nea" style="display:none">
<p>Por favor diligencie este formulario para enviar comentarios, errores o falencias encontradas en el proceso. M&aacute;ximo 250 caracteres </p>
<label>Tus comentarios:</label><br />
<textarea name="notas" id="notas" cols="60", rows="10"></textarea>
</div>
<!--fin de colaboracion en linea-->

<!-- Manual Ayuda -->
<div id="ayuda" title="Manual Pignoraci&oacute;n" 
style="background-image:url('../imagenes/FondoGeneral0.png')">
</div>
</body>
</html>
