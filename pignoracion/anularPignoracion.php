<?php
/* autor:       orlando puentes
 * fecha:       08/10/2010
 * objetivo:    
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$url=$_SERVER['PHP_SELF'];
include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'auditoria.php';

auditar($url);

?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head><TITLE>Pignoracion</TITLE>
<META http-equiv=Content-Type content="text/html; charset=iso-8859-1">
<meta http-equiv="Content-Script-Type" content="text/javascript; charset=iso-8859-1" />
<META content="MSHTML 6.00.2900.2180" name=GENERATOR>
<link href="../css/Estilos.css" rel="stylesheet" type="text/css" />
<link type="text/css" href="../css/formularios/base/ui.all.css" rel="stylesheet" />
<link href="../css/marco.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="../js/formularios/ui/ui.core.js"></script>
<script type="text/javascript" src="../js/formularios/ui/ui.resizable.js"></script>
<script type="text/javascript" src="../js/formularios/ui/ui.dialog.js"></script>
<script type="text/javascript" src="../js/comunes.js"></script>
<script type="text/javascript" src="js/anularPignoracion.js"></script>
<script type="text/javascript">
shortcut.add("Shift+F",function() {
		                                var URL=src();
		var url=URL+"aportes/trabajadores/consultaTrabajador.php";
    	window.open(url,"_blank");
    },{
	'propagate' : true,
	'target' : document 
        });        
</script>
</head>
<body>
<center>
<br /><br />
<table width="90%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="13" height="29" class="arriba_iz">&nbsp;</td>
    <td class="arriba_ce">
    <span class="letrablanca">::&nbsp;Anular Pignoraci&oacute;n &nbsp;::</span></td>
    <td width="13" class="arriba_de" align="right">&nbsp;</td>
  </tr>
  <tr>
 <td class="cuerpo_iz">&nbsp;</td>
	<td class="cuerpo_ce">
<img src="../imagenes/spacer.gif" width="1" height="1"/><img src="../imagenes/spacer.gif" width="1" height="1"/><img src="../imagenes/spacer.gif" width="1" height="1"/>
<img src="../imagenes/menu/modificar.png" title="Actualizar" width="16" height="16" onClick="actualizar()" style="cursor:pointer" />
<img src="../imagenes/spacer.gif" width="1" height="1"/>
<img src="../imagenes/menu/imprimir.png" width="16" height="16" style="cursor:pointer" title="Imprimir" /> 
<img src="../imagenes/spacer.gif" width="1" height="1"/><img src="../imagenes/spacer.gif" width="1" height="1"/>
<img src="../imagenes/menu/informacion.png" width="16" height="16" style="border:none; cursor:pointer" title="Manual" onClick="mostrarAyuda();" />
<img src="../imagenes/menu/notas.png" width="16" height="16" style="cursor: pointer" title="Colaborac&oacute;on en l&oacute;nea" onClick="notas();" />
</td>
     <td class="cuerpo_de">&nbsp;</td>
  </tr>
  <tr>
    <td class="cuerpo_iz">&nbsp;</td>
    <td class="cuerpo_ce">
    <div id="resultado" style="font-weight: bold;font-size: 14px;color:#FF0000"></div></td>
    <td class="cuerpo_de">&nbsp;</td>
  </tr>
  <tr>
    <td class="cuerpo_iz">&nbsp;</td>
    <td class="cuerpo_ce">
	<table width="95%" border="0" cellspacing="0" class="tablero">
      <tr>
        <td width="25%">Pagar&eacute; Nro</td>
        <td width="25%"><input name="txtId" id="txtId" class="box1" onBlur="buscarPagare();"/>
          <img src="../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
        <td width="25%">Valor</td>
        <td width="25%" id="valor">&nbsp;</td>
      </tr>
      <tr>
        <td>Afiliado</td>
        <td colspan="3" id="afiliado">&nbsp;</td>
        </tr>
      <tr>
        <td width="25%">Notas</td>
        <td colspan="3"><input name="notas" type="text" class="boxlargo" id="notas" />
        <img src="../imagenes/menu/obligado.png" width="12" height="12"> 
            </td>
        </tr>
      <tr>
      <td>&nbsp;</td>
      <td colspan="3">&nbsp;</td>
        </tr>
             
       </table>
      </td>
    <td class="cuerpo_de">&nbsp;</td>
  </tr>
  <tr>
    <td class="abajo_iz" >&nbsp;</td>
    <td class="abajo_ce" ></td>
    <td class="abajo_de" >&nbsp;</td>
  </tr>
</table>

</center>
 
<!-- colaboracion en linea -->

<div id="dialogo-archivo" title="Archivo banco">
<div id="progreso" style="display: none; font-size: 15pt; font-weight: bold;">Procesado(s) <span id="pg">0</span> de <span id="tt"></span> archivo(s)</div>
<div id="log"></div>
</div>
<div id="dialog-form2" title="Colaboraci&oacute;n en l&iacute;nea" style="display:none">
<p>Por favor diligencie este formulario para enviar comentarios, errores o falencias encontradas en el proceso. M&aacute;ximo 250 caracteres </p>
<label>Tus comentarios:</label><br />
<textarea name="notas" id="notas" cols="60", rows="10"></textarea>
</div>
<!-- fin colaboracion en linea--->

<!-- Manual Ayuda -->
<div id="ayuda" title="Manual Pignoraci&oacute;n" 
style="background-image:url('../imagenes/FondoGeneral0.png')">
</div>
</body>
<script language="javascript">
$("#txtId").focus();
</script>
</html>
