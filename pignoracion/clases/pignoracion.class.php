<?php
/* autor:       orlando puentes
 * fecha:       26/08/2010
 * objetivo:    
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'rsc' . DIRECTORY_SEPARATOR . 'conexion.class.php';
//include_once ("rsc/conexion.class.php");

class Pignoracion {
	//constructor
var $con;
function Pignoracion() {
    $this->con = new DBManager ();
}

function buscar_ultimos_3_p() {
    if ($this->con->conectar () == true) {
	$sql = "SELECT top 3 periodo FROM aportes012 WHERE procesado='S' ORDER BY periodo DESC";
	return mssql_query ( $sql, $this->con->conect );
	}
}
function buscar_ultimo_p() {
    if ($this->con->conectar () == true) {
	$sql = "SELECT top 1 periodo FROM aportes012 WHERE procesado='S' ORDER BY periodo DESC";
	return mssql_query ( $sql, $this->con->conect );
	}
}

function buscar_ultimos_3_c($idt,$per){
    if($this->con->conectar()==true){
        $sql="SELECT DISTINCT periodo,idbeneficiario,valor,pnombre,snombre,papellido,sapellido,fechanacimiento,cast(datediff(day,fechanacimiento,getdate())/365.25 AS INT) AS edad,
tipogiro, aportes015.capacidadtrabajo,aportes091.detalledefinicion
FROM aportes014
INNER JOIN aportes015 ON aportes014.idbeneficiario=aportes015.idpersona
INNER JOIN aportes091 ON aportes014.idtipobeneficiario=aportes091.iddetalledef
WHERE idtrabajador=$idt AND isnull(embargo, '') <> 'S' AND periodo IN($per)

UNION

SELECT DISTINCT periodo,idbeneficiario,valor,pnombre,snombre,papellido,sapellido,fechanacimiento,cast(datediff(day,fechanacimiento,getdate())/365.25 AS INT) AS edad,
tipogiro, aportes015.capacidadtrabajo,aportes091.detalledefinicion
FROM aportes009
INNER JOIN aportes015 ON aportes009.idbeneficiario=aportes015.idpersona
INNER JOIN aportes091 ON aportes009.idtipobeneficiario=aportes091.iddetalledef
WHERE idtrabajador=$idt AND isnull(embargo, '') <> 'S' AND periodo IN($per)";
        //echo $sql;
        return mssql_query($sql,$this->con->conect);
    }
}

function buscar_ultima_c($idt,$per){
    if($this->con->conectar()==true){
        $sql="SELECT COUNT(DISTINCT idbeneficiario) AS cuenta, sum(valor) AS valor FROM aportes014 WHERE idtrabajador=$idt and periodo='$per' AND isnull(embargo, '') <> 'S'
        UNION
        SELECT COUNT(DISTINCT idbeneficiario) AS cuenta, sum(valor) AS valor FROM aportes009 WHERE idtrabajador=$idt and periodo='$per' AND isnull(embargo, '') <> 'S' ";
        return mssql_query($sql,$this->con->conect);
    }
}

function contar_ultimos_3_c($idt,$per){
    if($this->con->conectar()==TRUE){
        $sql="SELECT COUNT(DISTINCT idbeneficiario) AS cuenta, periodo FROM aportes014 WHERE idtrabajador=$idt AND isnull(embargo, '') <> 'S' AND periodo IN($per) GROUP BY periodo
              UNION
              SELECT COUNT(DISTINCT idbeneficiario) AS cuenta, periodo FROM aportes009 WHERE idtrabajador=$idt AND isnull(embargo, '') <> 'S' AND periodo IN($per) GROUP BY periodo";
        return mssql_query($sql,$this->con->conect);
    }
}


function insert_pagare($campos){
    if($this->con->conectar()==true){
        $sql="Insert into aportes093 (idpersona,anno,valor,idconvenio,agencia,usuario,fechasistema) values (".$campos[0].",".$campos[1].",".$campos[2].",'".$campos[3]."','".$campos[4]."','".$campos[5]."',cast(getdate() as date))";
        //echo $sql;
        return mssql_query($sql,$this->con->conect);
    }
}

function buscar_pagare($idp){
	 if($this->con->conectar()==true){
	 	$sql="SELECT valor,pnombre,snombre,papellido,sapellido FROM aportes093 INNER JOIN aportes015 ON aportes093.idpersona=aportes015.idpersona WHERE idpagare=$idp";
		return mssql_query($sql,$this->con->conect);
	 }
	}

function anular_pagare($idp,$notas){
	 if($this->con->conectar()==true){
	 	$sql="UPDATE aportes093 SET valoranulado=valor, anulado='S', valor=0,motivo='$notas',estado='I' WHERE idpagare=$idp";
		return mssql_query($sql,$this->con->conect);
	 }
	}

}

?>
