/**
 * Funcion-clase que contiene todos los campos existentes en la base de datos de la tabla de certificado(aportes026) 
 */
function Certificado(){
	var idcertificado;
	var idbeneficiario;
	var idparentesco;
	var idtipocertificado;
	var periodoinicio;
	var periodofinal;
	var fechapresentacion;
	var formapresentacion;
	var estado;
	var usuario;
	var fechasistema;
}

/**
 * Funcion que inicializa todos los campos del certificado 
 */
function camposCertificado(objeto){ var obj = new Certificado();
	obj.idcertificado = esUndefined(objeto.idcertificado);
	obj.idbeneficiario = esUndefined(objeto.idbeneficiario);
	obj.idparentesco = esUndefined(objeto.idparentesco);
	obj.idtipocertificado = esUndefined(objeto.idtipocertificado);
	obj.periodoinicio = esUndefined(objeto.periodoinicio);
	obj.periodofinal = esUndefined(objeto.periodofinal);
	obj.fechapresentacion = esUndefined(objeto.fechapresentacion);
	obj.formapresentacion = esUndefined(objeto.formapresentacion);
	obj.estado = esUndefined(objeto.estado);
	obj.usuario = esUndefined(objeto.usuario);
	obj.fechasistema = esUndefined(objeto.fechasistema);
	
	return obj;
}