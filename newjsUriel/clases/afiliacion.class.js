/**
 * Funcion-clase que contiene todos los campos existentes en la base de datos de la tabla de Afiliacion(aportes016) 
 */
function Afiliacion(){
	var idformulario;
	var tipoformulario; 
	var tipoafiliacion; 
	var idempresa; 
	var idpersona; 
	var fechaingreso; 
	var horasdia; 
	var horasmes; 
	var salario; 
	var agricola; 
	var cargo; 
	var primaria; 
	var estado; 
	var fecharetiro; 
	var motivoretiro; 
	var fechanovedad; 
	var semanas; 
	var fechafidelidad; 
	var estadofidelidad; 
	var traslado; 
	var codigocaja; 
	var flag; 
	var tempo1; 
	var tempo2; 
	var fechasistema; 
	var usuario; 
	var tipopago; 
	var categoria; 
	var auditado; 
	var idagencia; 
	var idradicacion; 
	var vendedor; 
	var codigosalario;
	var flag2;
	var porplanilla;
	var madrecomunitaria;
	var claseafiliacion;
}

/**
 * Funcion que inicializa todos los campos de la afiliacion 
 */
function camposAfiliacion(objeto){ var obj = new Afiliacion();
	obj.idformulario = esUndefined(objeto.idformulario);
	obj.tipoformulario = esUndefined(objeto.tipoformulario); 
	obj.tipoafiliacion = esUndefined(objeto.tipoafiliacion); 
	obj.idempresa = esUndefined(objeto.idempresa); 
	obj.idpersona = esUndefined(objeto.idpersona); 
	obj.fechaingreso = esUndefined(objeto.fechaingreso); 
	obj.horasdia = esUndefined(objeto.horasdia); 
	obj.horasmes = esUndefined(objeto.horasmes); 
	obj.salario = esUndefined(objeto.salario); 
	obj.agricola = esUndefined(objeto.agricola); 
	obj.cargo = esUndefined(objeto.cargo); 
	obj.primaria = esUndefined(objeto.primaria); 
	obj.estado = esUndefined(objeto.estado); 
	obj.fecharetiro = esUndefined(objeto.fecharetiro); 
	obj.motivoretiro = esUndefined(objeto.motivoretiro); 
	obj.fechanovedad = esUndefined(objeto.fechanovedad); 
	obj.semanas = esUndefined(objeto.semanas); 
	obj.fechafidelidad = esUndefined(objeto.fechafidelidad); 
	obj.estadofidelidad = esUndefined(objeto.estadofidelidad); 
	obj.traslado = esUndefined(objeto.traslado); 
	obj.codigocaja = esUndefined(objeto.codigocaja); 
	obj.flag = esUndefined(objeto.flag); 
	obj.tempo1 = esUndefined(objeto.tempo1); 
	obj.tempo2 = esUndefined(objeto.tempo2); 
	obj.fechasistema = esUndefined(objeto.fechasistema); 
	obj.usuario = esUndefined(objeto.usuario); 
	obj.tipopago = esUndefined(objeto.tipopago); 
	obj.categoria = esUndefined(objeto.categoria); 
	obj.auditado = esUndefined(objeto.auditado); 
	obj.idagencia = esUndefined(objeto.idagencia); 
	obj.idradicacion = esUndefined(objeto.idradicacion); 
	obj.vendedor = esUndefined(objeto.vendedor); 
	obj.codigosalario = esUndefined(objeto.codigosalario);
	obj.flag2 = esUndefined(objeto.flag2);
	obj.porplanilla = esUndefined(objeto.porplanilla);
	obj.madrecomunitaria = esUndefined(objeto.madrecomunitaria);
	obj.claseafiliacion = esUndefined(objeto.claseafiliacion);
	
	return obj;
}