/**
 * Funcion-clase que contiene todos los campos existentes en la base de datos de la tabla de GrupoFamiliar(aportes021) 
 */
function GrupoFamiliar(){
	var idrelacion;
	var idtrabajador;
	var idbeneficiario;
	var idparentesco;
	var idconyuge;
	var conviven;
	var fechaafiliacion;
	var fechaasignacion;
	var giro;
	var estado;
	var tempo1;
	var usuario;
	var fechasistema;
	var idtiporelacion;
	var postulacionfonede;
	var embarga;
	var idmotivo;
	var fechaestado;
	var idbiologico;
	var fechagiro;
}

/**
 * Funcion que inicializa todos los campos de la Grupo Familiar 
 */
function camposGrupoFamiliar(objeto){ var obj = new GrupoFamiliar();
	obj.idrelacion = esUndefined(objeto.idrelacion);
	obj.idtrabajador = esUndefined(objeto.idtrabajador);
	obj.idbeneficiario = esUndefined(objeto.idbeneficiario);
	obj.idparentesco = esUndefined(objeto.idparentesco);
	obj.idconyuge = esUndefined(objeto.idconyuge);
	obj.conviven = esUndefined(objeto.conviven);
	obj.fechaafiliacion = esUndefined(objeto.fechaafiliacion);
	obj.fechaasignacion = esUndefined(objeto.fechaasignacion);
	obj.giro = esUndefined(objeto.giro);
	obj.estado = esUndefined(objeto.estado);
	obj.tempo1 = esUndefined(objeto.tempo1);
	obj.usuario = esUndefined(objeto.usuario);
	obj.fechasistema = esUndefined(objeto.fechasistema);
	obj.idtiporelacion = esUndefined(objeto.idtiporelacion);
	obj.postulacionfonede = esUndefined(objeto.postulacionfonede);
	obj.embarga = esUndefined(objeto.embarga);
	obj.idmotivo = esUndefined(objeto.idmotivo);
	obj.fechaestado = esUndefined(objeto.fechaestado);
	obj.idbiologico = esUndefined(objeto.idbiologico);
	obj.fechagiro = esUndefined(objeto.fechagiro);
	
	return obj;
}