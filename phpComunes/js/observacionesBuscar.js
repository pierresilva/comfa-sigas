/*
* @autor:      Ing. Orlando Puentes
* @fecha:      septiembre 6 de 2010
* objetivo:
*/
var URL=src();
var idp="";
$(document).ready(function(){
	$("#buscarPor").bind('change',function(){
 		$("#lnombre").html(" ");
 		$("#lnombreE").html(" ");
		$("#numero").val("");
		});
});//fin ready

function mostrar(){
	var opt=$("#buscarPor").val();
	if(opt==0){
		$("#tNumero").css("display", "none"); 
		$("#tNit").css("display", "none"); 
		limpiarCampos();
		}
	if(opt==1){
		$("#tNumero").css("display", "block"); 
		$("#tNit").css("display", "none"); 
		$("#tipoI").focus();
		$("#txtNit").val("");
		$("#lnombreE").html("");
		$("#observaciones").val("");
		$("#observacionesAnt").html("");
		}
	if(opt==2){
		$("#tNit").css("display", "block"); 
		$("#tNumero").css("display", "none"); 
		$("#txtNit").focus();
		$("#numero").val("");
		$("#lnombre").html("");
		$("#observaciones").val("");
		$("#observacionesAnt").html("");
		}
	}
	
function buscarPersona(){
	var tipoI=$("#tipoI").val();
	var numero=$("#numero").val();
	if(numero.length==0){
		alert("Digite el n\u00FAmero de identificaci\u00F3n!");
		$("#numero").focus();
		return false;
		}
	$.getJSON(URL+'phpComunes/buscarPersona2.php',{v0:tipoI,v1:numero},function(data){
		if(data==0){
			alert("Lo lamento, el n\u00FAmero no existe!");
			$("#numero").val("");
			$("#numero").focus();
			return false;
		}
		$.each(data,function(i,fila){
		var nom=fila.pnombre+" "+fila.snombre+" "+fila.papellido+" "+fila.sapellido;
		idp=fila.idpersona;
		$("#lnombre").html(nom);
		});
		
		$.getJSON(URL+'phpComunes/pdo.buscar.notas.php',{v0:idp,v1:'1'},function(datos){
				$('#observacionesAnt').html(datos);
		});
	});
}	

function buscarNIT(){
	var nit=$.trim($("#txtNit").val());
	var nombreEmp=0;
	if(nit.length==0){
		alert("Digite el n\u00FAmero de NIT!");
		$("#txtNit").focus();
		return false;
		}
	$.getJSON(URL+'phpComunes/buscarEmpresa.php',{v0:nit},function(datos){
		if(datos==0){
				alert("Lo lamento, el NIT no existe!");
				$("#txtNit").val("");
				$("#txtNit").focus();
				return false;
			}
			$.each(datos,function(i,fila){
			nombreEmp=fila.razonsocial;
			idp=fila.idempresa;
			$("#lnombreE").html(nombreEmp);
		})
		
		$.getJSON(URL+'phpComunes/pdo.buscar.notas.php',{v0:idp,v1:'2'},function(datos){
				$('#observacionesAnt').html(datos);
		});
	});
}	

function limpiarCampos(){
	$("table.tablero input:text").val('');
	$("table.tablero select").val(0);
	$("#lnombre,#lnombreE,#lbono").html(" ");
	$("#tNumero").css("display", "none"); 
	$("#tNit").css("display", "none"); 
	$("#numero").val("");
	$("#txtNit").val("");
	$("#observaciones").val("");
	$("#observacionesAnt").html("");
	$("#buscarPor").focus();
}

function guardar(){
	var opt=$("#buscarPor").val();
	var notas=$("#observaciones").val();
	if(notas.length==0){
		alert("Escriba las observaciones!");
		return false;
	}
	if(notas.length<10){
		alert("Las observaciones son muy cortas...!");
		return false;
	}
	$.getJSON(URL+'phpComunes/pdo.insert.notas.php',{v0:idp,v1:notas,v2:opt},function(datos){
		if(datos==0){
			alert("Lo lamento, no se pudo guardar las observaciones!");
			return false;
		}
		else
		{
			alert("Observaciones guardadas!");
			limpiarCampos();
		}
	})
}
$(function() {
	$("#dialog-form2").dialog("destroy");
	$("#dialog-form2").dialog({
		autoOpen: false,
		height: 400,
		width: 500,
		modal: true,
		buttons: {
			'Enviar': function() {
			var bValid = true;
			var campo=$('#notas').val();
			var campo0=$.trim(campo);
			if (campo0==""){
				$(this).dialog('close');
					return false;
				}
			var campo1=$('#usuario').val();
			var campo2="observaciones.php";
			$.post(URL+'phpComunes/colaboracion.php',{v0:campo0,v1:campo1,v2:campo2},
			function(datos){
				if(datos=='1'){
					alert("Su comentario fue enviado correctamente!. Gracias por participar en el mejoramiento de SIGAS");
				}else{
					alert(datos);
				}
		});
		$(this).dialog('close');
		},
		Cancelar: function() {
			$(this).dialog('close');
			$("#dialog-form2").dialog("destroy");
		}
	}
	});
	/*
	$('#enviar-notas')
		.button()
		.click(function() {
		$('#dialog-form2').dialog('open');
	});
	*/
});

$(function() {
		$("#ayuda").dialog({
		 	autoOpen: false,
			height: 450,
			width: 600,
			draggable:true,
			modal:false,
			open: function(evt, ui){
					$('#ayuda').html('');
					$.get('../../help/aportes/ayudaObservaciones.html',function(data){
							$('#ayuda').html(data);
					});
			 }
		});
	});