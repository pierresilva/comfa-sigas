<?php
/*@autor:       Ing Orlando Puentes A.
* @fecha:       septiembre 6 de 2010
* @objetivo:
*/
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'grupo.familiar.class.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}
$campo0 = (empty($_REQUEST['v0'])) ? 'NULL' : $_REQUEST['v0'];	//id trabajador
$campo1 = (empty($_REQUEST['v1'])) ? 'NULL' : $_REQUEST['v1'];	//id beneficiario	
$campo2 = (empty($_REQUEST['v2'])) ? 'NULL' : $_REQUEST['v2'];	//idpearentesco
$campo3 = (empty($_REQUEST['v3'])) ? 'NULL' : $_REQUEST['v3'];	//idconyuge
$campo4 = (empty($_REQUEST['v4'])) ? 'NULL' : "'".$_REQUEST['v4']."'";	//conviven
$campo5 = (empty($_REQUEST['v5'])) ? 'NULL' : $_REQUEST['v5'];	//idtiporelacion
$campo6 = "'".$_SESSION['USUARIO']."'";	//usuario


/**
 * Buscar si ya existe una relaci�n de convivencia inactiva con la misma persona, y si ya existe,
 * se debe actualizar el estado.
 * 
 * o Tambien, se buscar si ya existe una relaci�n de convivencia con la misma persona, y si ya existe,
 * se actualiza
 */
$objGrupoFamiliar = new GrupoFamiliar();
$result = $objGrupoFamiliar->buscarConvivencias($campo0);
$contRelaciones = 0;
$registrosActualizados = 0;
while($registro = mssql_fetch_assoc($result)){
	if(($registro["estado_relacion"] == 'I' && $registro["idtrabajador"] == $campo0 && $registro["idbeneficiario"] == $campo1) || ($registro["idparentesco"] == 34 && $registro["idtrabajador"] == $campo0 && $registro["idbeneficiario"] == $campo1))
		$contRelaciones++;
}

/**
 * si existen relaciones inactivas por actualizar, entonces se cambia el estado a Activo
 * @author Oswaldo G.
 * @date 12-04-2012
 */
if($contRelaciones>0){
	$sql = "UPDATE aportes021 SET estado='A', conviven={$campo4}, idtiporelacion={$campo5}, usuario={$campo6}, fechaafiliacion=cast(getdate() as date), fechasistema=cast(getdate() as date) WHERE idtrabajador={$campo0} AND idbeneficiario={$campo1} AND idparentesco={$campo2}";
	$registrosActualizados = $db->queryActualiza($sql);
	if($registrosActualizados>0)
		echo trim($registrosActualizados);
	else 
		echo 0;
}else{
	/**
	 * si no hay relaciones inactivas por actualizar, entonces se inserta la nueva relaci�n
	 * @author Oswaldo G.
	 * @date 12-04-2012
	 */
	//idtrabajador, idbeneficiario, idparentesco, idconyuge, conviven
	/* require('clases/grupo.familiar.class.php'); 24-02-2012*/
	$sql="Insert into aportes021 (idtrabajador, idbeneficiario, idparentesco,idconyuge,conviven,idtiporelacion,	usuario,fechaafiliacion,estado,fechasistema) values ($campo0,$campo1,$campo2,$campo3,$campo4,$campo5,$campo6,cast(getdate() as date),'A',cast(getdate() as date))";
	$rs=$db->queryInsert($sql,'aportes021');
	if($rs==null)
		echo 0;
	else
		echo $rs;	
}
?>