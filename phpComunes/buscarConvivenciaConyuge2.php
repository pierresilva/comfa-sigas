<?php
/* autor:       orlando puentes
 * fecha:       10/08/2010
 * objetivo:    
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'grupo.familiar.class.php';

$idp=$_REQUEST['v0'];
$sql = "SELECT a21.idtrabajador,a21.idbeneficiario,a21.estado,a21.idparentesco,a21.conviven,a91.detalledefinicion,a15.identificacion,a15.pnombre,a15.snombre,a15.papellido,a15.sapellido,a15.sexo,td91.codigo
		FROM aportes021 a21 INNER JOIN aportes015 a15 ON a15.idpersona=a21.idtrabajador INNER JOIN aportes091 a91 ON a91.iddetalledef=a21.idparentesco INNER JOIN aportes091 td91 ON td91.iddetalledef=a15.idtipodocumento
	  	WHERE a21.idbeneficiario=$idp AND a21.idparentesco=34 AND a21.conviven='S'
		UNION
		SELECT a21.idtrabajador,a21.idbeneficiario,a21.estado,a21.idparentesco,a21.conviven,a91.detalledefinicion,a15.identificacion,a15.pnombre,a15.snombre,a15.papellido,a15.sapellido,a15.sexo,td91.codigo 
		FROM aportes021 a21 INNER JOIN aportes015 a15 ON a15.idpersona=a21.idbeneficiario INNER JOIN aportes091 a91 ON a91.iddetalledef=a21.idparentesco INNER JOIN aportes091 td91 ON td91.iddetalledef=a15.idtipodocumento
		WHERE a21.idtrabajador=$idp AND a21.idparentesco=34 AND a21.conviven='S' AND 0=isnull((SELECT count(b21.idrelacion) FROM aportes021 b21 WHERE b21.idbeneficiario=a21.idtrabajador AND b21.idtrabajador=a21.idbeneficiario AND b21.idparentesco=34),0)"; 
$rs=$db->querySimple($sql);

$conr=0;
$data = array();
while($row=$rs->fetch()){
	$data[]=array_map('utf8_encode',$row);
	$conr++;
}
if($conr>0){
	echo json_encode($data);
} else {
	echo 0;
}
?>