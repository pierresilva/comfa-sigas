<?php
/* autor:       orlando puentes
 * fecha:       17/08/2010
 * objetivo:    
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';

?>
<html>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/jquery.direccion.js"></script>

<script type="text/javascript" >
$(document).ready(function(){

				//Direcciones
	$("#uno").change(function(){
		$("#dos,#dos2").val('');
		if($(this).val()=='AVD'){
			$("#dos").hide();
			$("#dos2").show();
			}else{
			$("#dos").show();
			$("#dos2").hide();
			}
		});
			
			
});//fin ready
</script>
<style>
.requerido{ background:#E0FFC1; border:1px  solid  #55D92F}
</style>
<table width="578" height="364" border="0" align="center" cellpadding="0"
	cellspacing="12" bgcolor="#ffffff">
	<tbody>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>
            <span	 style="color:#390"><strong>(*)</strong></span>Son Campos requeridos para ingresar la dirección</td>
		</tr>
		<tr>
			<td>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tbody>
					<tr>
						<td bgcolor="#EBEBEB">
						<table width="100%" border="0" cellpadding="2" cellspacing="0">
							<tbody>
								<tr>
									<td width="11%" class="ui-corner-tl">Dirección</td>
									<td width="23%"><select name="uno" class="requerido" id="uno" tabindex="1">
										<option selected="selected" value="">Seleccione</option>
										<option value="CALLE ">CALLE</option>
										<option value="CARRERA ">CARRERA</option>
										<option value="AVD">AVENIDA</option>
										<option value="AUT">AUTOPISTA</option>
										<option value="TRASVERSAL ">TRASVERSAL</option>
										<option value="DIAGONAL ">DIAGONAL</option>
									</select>									
                                   </td>
                                    <td width="26%">  
                                    <input maxlength="3" size="19" name="dos" id="dos"		type="text" tabindex="2" class="requerido">	
                                    <input  size="19" name="dos2" id="dos2"		type="text" tabindex="2" class="requerido"  style="display:none"/>
                                    	
                                  </td>
                                    <td width="11%">
                                    <select name="tres" id="tres" tabindex="3">
										<option selected="selected" value=""></option>
										<option value="A ">A</option>
										<option value="B ">B</option>
										<option value="C ">C</option>
										<option value="D ">D</option>
										<option value="E ">E</option>
										<option value="F ">F</option>
										<option value="G ">G</option>
										<option value="H ">H</option>
										<option value="I ">I</option>
										<option value="J ">J</option>
										<option value="K ">K</option>
										<option value="L ">L</option>
										<option value="M ">M</option>
										<option value="N ">N</option>
										<option value="O ">O</option>
										<option value="P ">P</option>
										<option value="Q ">Q</option>
										<option value="R ">R</option>
										<option value="S ">S</option>
										<option value="T ">T</option>
										<option value="U ">U</option>
										<option value="V ">V</option>
										<option value="W ">W</option>
										<option value="X ">X</option>
										<option value="Y ">Y</option>
										<option value="Z ">Z</option>
									</select>
                                    </td>
                                    <td width="13%">
                                    <select name="cuatro" id="cuatro" tabindex="4">
										<option selected="selected" value=""></option>
										<option value="BIS ">BIS</option>
									</select>
                                    </td>
                                    <td width="16%" class="ui-corner-tr">
                                    <select name="cinco" id="cinco" tabindex="5">
										<option selected="selected" value=""></option>
										<option value="W ">WEST</option>
										<option value="SUR ">SUR</option>
									</select>
                                    </td>
								</tr>
							</tbody>
						</table>
					  </td>
					</tr>
					<tr>
						<td bgcolor="#EBEBEB">
						<table width="100%" border="0" cellpadding="2" cellspacing="0">
							<tbody>
								<tr>
									<td width="11%" class="ui-corner-bl">Numero&nbsp;&nbsp;</td>
									<td width="23%"><input name="seis"
										type="text" class="requerido" id="seis" tabindex="6" size="18" maxlength="3"></td>
									<td width="11%"><select name="siete" id="siete" tabindex="7">
										<option selected="selected" value=""></option>
										<option value="A ">A</option>
										<option value="B ">B</option>
										<option value="C ">C</option>
										<option value="D ">D</option>
										<option value="E ">E</option>
										<option value="F ">F</option>
										<option value="G ">G</option>
										<option value="H ">H</option>
										<option value="I ">I</option>
										<option value="J ">J</option>
										<option value="K ">K</option>
										<option value="L ">L</option>
										<option value="M ">M</option>
										<option value="N ">N</option>
										<option value="O ">O</option>
										<option value="P ">P</option>
										<option value="Q ">Q</option>
										<option value="R ">R</option>
										<option value="S ">S</option>
										<option value="T ">T</option>
										<option value="U ">U</option>
										<option value="V ">V</option>
										<option value="W ">W</option>
										<option value="X ">X</option>
										<option value="Y ">Y</option>
										<option value="Z ">Z</option>
									</select></td>
									<td width="15%"><select name="ocho" id="ocho" tabindex="8">
										<option selected="selected" value=""></option>
										<option value="BIS ">BIS</option>
									</select>
								    -</td>
									<td width="24%"><select name="nueve" id="nueve" tabindex="9">
										<option selected="selected" value=""></option>
										<option value="W ">WEST</option>
										<option value="SUR ">SUR</option>
									</select></td>
									<td width="16%"><input name="diez"
										type="text" class="requerido" id="diez" tabindex="10" size="19" maxlength="3"></td>
								</tr>
							</tbody>
						</table>
					  </td>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
		<tr>
			<td >Los siguientes campos no son obligatorios.
			Compl&eacute;telos solamente si es necesario.</td>
		</tr>
		<tr>
			<td>
			<table width="100%" border="0" cellpadding="3" cellspacing="0">
				<tbody>
					<tr>
					  <td width="25%" bgcolor="#EBEBEB" class="ui-corner-tl">Datos Adicionales</td>
						<td width="24%" bgcolor="#EBEBEB"><select name="once" id="once" tabindex="11">
							<option selected="selected" value=""></option>
							<option value="OFICINA ">OFICINA</option>
							<option value="CONDOMINIO ">CONDOMINIO</option>
							<option value="EDIFICIO ">EDIFICIO</option>
							<option value="TORRE ">TORRE</option>
							<option value="MANZANA ">MANZANA</option>
							<option value="LOTE ">LOTE</option>
							<option value="CASA ">CASA</option>
							<option value="APTO ">APARTAMENTO</option>
							<option value="VEREDA ">VEREDA</option>
							<option value="FINCA ">FINCA</option>
							<option value="KM ">KILOMETRO</option>
                            <option value="LOCAL ">LOCAL</option>
						</select></td>
						<td width="51%" bgcolor="#EBEBEB" class="ui-corner-tr"><input maxlength="20" size="30" name="doce"
							id="doce" type="text" tabindex="12"></td>
					</tr>
					<tr>
					  <td bgcolor="#EBEBEB">Datos Adicionales</td>
						<td bgcolor="#EBEBEB"><select name="trece" id="trece" tabindex="13">
							<option selected="selected" value=""></option>
							<option value="OFICINA ">OFICINA</option>
							<option value="CONDOMINIO ">CONDOMINIO</option>
							<option value="EDIFICIO ">EDIFICIO</option>
							<option value="TORRE ">TORRE</option>
							<option value="MANZANA ">MANZANA</option>
							<option value="LOTE ">LOTE</option>
							<option value="CASA ">CASA</option>
							<option value="APTO ">APARTAMENTO</option>
							<option value="VEREDA ">VEREDA</option>
							<option value="FINCA ">FINCA</option>
							<option value="KM ">KILOMETRO</option>
                            <option value="LOCAL ">LOCAL</option>
						</select></td>
						<td bgcolor="#EBEBEB"><input maxlength="20" size="30" name="catorce" id="catorce"
							type="text" tabindex="14"></td>
					</tr>
					<tr>
					  <td bgcolor="#EBEBEB" class="ui-corner-bl">Datos Adicionales</td>
						<td bgcolor="#EBEBEB"><select name="quince" id="quince" tabindex="15">
							<option selected="selected" value=""></option>
							<option value="OFICINA ">OFICINA</option>
							<option value="CONDOMINIO ">CONDOMINIO</option>
							<option value="EDIFICIO ">EDIFICIO</option>
							<option value="TORRE ">TORRE</option>
							<option value="MANZANA ">MANZANA</option>
							<option value="LOTE ">LOTE</option>
							<option value="CASA ">CASA</option>
							<option value="APTO ">APARTAMENTO</option>
							<option value="VEREDA ">VEREDA</option>
							<option value="FINCA ">FINCA</option>
							<option value="KM ">KILOMETRO</option>
                            <option value="LOCAL ">LOCAL</option>
						</select></td>
						<td bgcolor="#EBEBEB" class="ui-corner-br"><input maxlength="20" size="30" name="dieciseis"
							id="dieciseis" type="text" tabindex="16"></td>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
		<tr>
			<td>
<table width="100%" border="0" cellpadding="0" cellspacing="2">
<tbody>
<tr>
<td colspan="2" align="center"><input disabled="disabled" maxlength="256" size="70" name="tDirCompleta" id="tDirCompleta" type="text"></td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td align="center">&nbsp;</td>
</tr>
</tbody>
</table>
<script>
$("#uno").focus();
</script>
</html>