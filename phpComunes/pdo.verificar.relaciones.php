<?php 
/*
* @autor:      Ing. Orlando Puentes
* @fecha:      04/09/2010
* objetivo:		verifica las relaciones que una persona pueda tener activas en comfamiliar
*/
ini_set("display_errors",'1');
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}
$c0=$_REQUEST['v0'];  //id persona a verificar

$sql="Select * from aportes021 where idbeneficiario=$c0 and estado='A'";
$rs=$db->querySimple($sql);
$con=0;
$msg='';
while ($w=$rs->fetch()){
	$con++;
	$par = $w['idparentesco'];
	switch ($par) {
		case 34: $paren='Conyuge'; break;
		case 35: $paren='Hijo';	break;
		case 36: $paren='Padre'; break;
		case 37: $paren='Hermano'; break;
		case 38: $paren='Hijastro';	break;
	}
	$idp=$w['idtrabajador'];
	$sql="Select identificacion,pnombre,snombre,papellido,sapellido from aportes015 where idpersona=$idp";
	$rs2=$db->querySimple($sql);
	$w2=$rs2->fetch();
	$msg=$paren." con ".$w2['identificacion']." ".$w2['pnombre']." ".$w2['snombre']." ".$w2['papellido']." ".$w2['sapellido'];
}
if ($con==0)
	echo 0;
else
	echo json_encode($msg);
?>
