<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
require_once 'dompdf-master/dompdf_config.inc.php';
require_once 'numero_texto.class.php';

class Util{
    private static $dato;
    private static $datoTrasformado;
    
    public static function reempDatosPlant($plantilla,$arrIndices,$arrDatos){
        
        foreach($arrIndices AS $key => $valor){
            $banFormato = substr($key,strlen($key)-5,5);
            
            if($banFormato!="urldi" && !isset($arrDatos[$valor]))continue;
            
            $banDato = "";
            //Obtener el datos en el formato requerido
            if($banFormato=="urldi"){$banDato = $valor;}
            else $banDato = $arrDatos[$valor];
            
            switch($banFormato){
                case 'nform'://Formatear numero
                    $banDato = self::trasformaNumero("NORMAL",$banDato);
                    break;
                case 'ntext'://Numero en texto
                    $banDato = self::trasformaNumero("TEXTO",$banDato);
                    break;
                case 'ftext'://Numero en texto
                    $banDato = self::trasformaFecha("TEXTO",$banDato);
                    break;
                case 'upper'://Mayuscula
                    $banDato = self::trasformaTexto("UPPER",$banDato);
                    break;
                case 'lower'://Minuscula
                    $banDato = self::trasformaTexto("LOWER",$banDato);
                    break;
                case 'urldi': //Direccion url
                    if($banDato=="dir_url_css_urldi"){
                        $banDato = DIR_URL_CSS;
                    }
                    //var_dump($banDato);exit();
                    break;
            }
            $plantilla = str_replace('$'.$key, $banDato, $plantilla);
        }
        return $plantilla;
    }
    
    /**
     * Metodo para dar formato a un numero
     * @param $tipo [NORMAL-TELEFONO-CELULAR-CUENTA-TEXTO]
     * @param $numero Numero a formatear
     */
    public static function trasformaNumero($tipo, $numero){
        self::$dato = $numero;
        switch($tipo){
            case 'NORMAL':
                self::numeroNormal();
                break;
            case 'DECIMAL':
                self::numerNormaDecim();
                break;
            case 'TEXTO':
                self::$datoTrasformado = NumeroTexto::getNumeroTexto($numero);
                break;
            default:
                self::$datoTrasformado = $numero;
        }
        return self::$datoTrasformado;
    }
    
    private static function numeroNormal(){
        self::$datoTrasformado = number_format(intval(self::$dato),0, ',', '.');
    }
    private static function numerNormaDecim(){
        self::$datoTrasformado = number_format(floatval(self::$dato),2, ',', '.');
    }
    
    /**
     * Metodo para dar formato al texto
     * @param $tipo [UPPER-LOWER]
     * @param $texto Texto a formatear
     */
    public static function trasformaTexto($tipo,$texto){
        switch($tipo){
            case 'UPPER':
                self::$datoTrasformado = strtoupper($texto);
                break;
            case 'LOWER':
                self::$datoTrasformado = strtolower($texto);
                break;
            default:
                self::$datoTrasformado = $texto;
        }
        return self::$datoTrasformado;
    }
    
    /**
     * Metodo para dar formato a una fecha
     * @param $tipo [TEXTO]
     * @param $fecha Fecha a formatear (AAAA-MM-DD)
     */
    public static function trasformaFecha($tipo, $fecha){
        switch($tipo){
            case 'TEXTO':  
                $arrMesTexto = array(1=>"ENERO",2=>"FEBRERO",3=>"MARZO",4=>"ABRIL",5=>"MAYO",6=>"JUNIO",7=>"JULIO",8=>"AGOSTO",9=>"SEPTIEMBRE",10=>"OCTUBRE",11=>"NOVIEMBRE",12=>"DICIEMBRE");
                $anno = "";
                $mes = "";
                $dia = "";
                list($anno,$mes,$dia) = explode("-",$fecha);
                $dia = (intval($dia)==1?"UNO":NumeroTexto::getNumeroTexto($dia))." ($dia)";
                $mes = " de ".$arrMesTexto[intval($mes)]; 
                $anno = " del ".NumeroTexto::getNumeroTexto($anno)." ($anno)";
                
                self::$datoTrasformado = self::$datoTrasformado = $dia.$mes.$anno;
                break;
            default:
                self::$datoTrasformado = $fecha;
        }
        return self::$datoTrasformado;
    }
    
    /**
     *
     * @param type $contenido
     */
    public static function archivoPDF($contenido, $orientation = "portrait"){
        
        if($contenido==""){
            $contenido = "<b>No existe resultado</b>";
        }
        
        # Instanciamos un objeto de la clase DOMPDF.
        $mipdf = new DOMPDF();
        
        # Definimos el tamaño y orientacion del papel que queremos.
        # O por defecto cogere el que esta en el fichero de configuracion.
        $mipdf->set_paper("A4", $orientation);
        //$mipdf->set_paper("letter", "portrait");

        # Cargamos el contenido HTML.
        $mipdf->load_html(utf8_decode($contenido));

        # Renderizamos el documento PDF.
        $mipdf->render();

        # Enviamos el fichero PDF al navegador.
        $mipdf->stream("Fichero.pdf", array("Attachment"=>0));
    }
}
?>
