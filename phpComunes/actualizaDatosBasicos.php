<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
include_once $raiz.DIRECTORY_SEPARATOR.'config.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}
$idp=$_REQUEST['v0'];

$sql="SELECT idtipodocumento, identificacion, papellido, sapellido, pnombre, snombre, sexo, idestadocivil, fechanacimiento, nombrecorto,
aportes091.codigo, e.detalledefinicion FROM aportes015 INNER JOIN aportes091 ON aportes015.idtipodocumento=aportes091.iddetalledef
LEFT JOIN aportes091 e ON aportes015.idestadocivil=e.iddetalledef WHERE idpersona=$idp";
$rs=$db->querySimple($sql);
$per=$rs->fetch();
$fec=$per['fechanacimiento'];
$fecha='';
if(strlen($fec)>0){
	$fec=explode("-",$per['fechanacimiento']);
	$fecha=$fec[1]."/".$fec[2]."/".$fec[0];
}
?>
<table border="0" class="tablero" id="persona" width="100%" align="center">
<tr>
  <td>Id registro</td>
  <td><input type="text" name="txtidr" id="txtidr" class="box1" disabled="disabled" value="<?php echo $idp; ?>" /></td>
</tr>
<tr>
<td width="40%">Tipo documento</td>
<td width="60%"><select name="tipoDocB" id="tipoDocB" class="box1">
<?php
$def=$db->Definiciones(1,1);
while($row=$def->fetch()){
	if($per['idtipodocumento']==$row['iddetalledef'])
		echo "<option value='".$row['iddetalledef']."' selected='selected'>".$row['detalledefinicion']."</option>";
	else
		echo "<option value='".$row['iddetalledef']."' >".$row['detalledefinicion']."</option>";
	}
?>
</select>
</td>
</tr>
<tr>
  <td>Número</td>
  <td><input type="text" name="txtnumeroB" id="txtnumeroB" class="box1" value="<?php echo $per['identificacion']; ?>" /></td>
</tr>
<tr>
  <td>P. Nombre</td>
  <td><input type="text" name="txtpnombreB" id="txtpnombreB" class="box1" value="<?php echo $per['pnombre']; ?>" /></td>
</tr>
<tr>
  <td>S. Nombre</td>
  <td><input type="text" name="txtsnombreB" id="txtsnombreB" class="box1"  value="<?php echo $per['snombre']; ?>"/></td>
</tr>
<tr>
  <td>P. Apellido</td>
  <td><input type="text" name="txtpapellidoB" id="txtpapellidoB" class="box1" value="<?php echo $per['papellido']; ?>" /></td>
</tr>
<tr>
  <td>S. Apellido</td>
  <td><input type="text" name="txtsapellidoB" id="txtsapellidoB" class="box1"  value="<?php echo $per['sapellido']; ?>"/></td>
</tr>
<tr>
  <td>Fecha Nace</td>
  <td><input type="text" name="txtfechanaceB" id="txtfechanaceB" class="box1"  value="<?php echo $fecha; ?>" onblur="validarFecha(this)" onfocus="this.value=''"/></td>
</tr>
<tr>
  <td>Estado Civil</td>
  <td><select name="civilB" id="civilB" class="box1">
<?php
$def=$db->Definiciones(10,1);
while($row=$def->fetch()){
	if($row["iddetalledef"]!=54){
	if($per['idestadocivil']== $row['iddetalledef'])
		echo "<option value=".$row['iddetalledef']." selected=selected>".$row['detalledefinicion']."</option>";
	else
		echo "<option value=".$row['iddetalledef']." >".$row['detalledefinicion']."</option>";
	}
}
?>    
  </select></td>
</tr>
<tr>
  <td>Sexo</td>
  <td><select name="sexoB" id="sexoB" class="box1">
  <?php
	$attrSelectedM=($per['sexo']=="M")? "selected='selected'" : "";
	$attrSelectedF=($per['sexo']=="F")? "selected='selected'" : "";
?>
  <option value='M' <?php echo $attrSelectedM; ?>>Masculino</option>
  <option value='F' <?php echo $attrSelectedF; ?>>Femenino</option>
    
  </select></td>
</tr>
<tr>
<td>Nombre corto</td>
<td><input type="text" name="txtnombrecortoB" id="txtnombrecortoB" class="boxmediano" value="<?php echo $per['nombrecorto']; ?>" readonly="readonly" /></td>
</tr>
</table>