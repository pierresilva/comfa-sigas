<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Formulario de actulizacion</title>
<link type="text/css" href="<?php echo URL_PORTAL; ?>css/marco.css" rel="stylesheet" />
<link type="text/css" href="<?php echo URL_PORTAL; ?>css/Estilos.css" rel="stylesheet" />
<!--<style type="text/css">
.zIndex{ z-index:9999}
</style>
<script language="javascript" type="text/javascript" src="<?php echo URL_PORTAL; ?>js/formularios/ui/ui.datepicker.js" ></script>-->
</head>

<body>
<center>
<table width="95%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td class="arriba_iz" >&nbsp;</td>
<td class="arriba_ce" ><span class="letrablanca">::Grabar datos básicos de la persona&nbsp;::</span></td>
<td class="arriba_de" >&nbsp;</td>
</tr>
<tr>
<td class="cuerpo_iz">&nbsp;</td>
<td class="cuerpo_ce">
<center>
<table border="0" class="tablero" id="persona" width="90%">
<tr>
<td width="40%">Tipo documento</td>
<td width="60%"><select name="tipodoc" id="tipDoc" class="box1">
<?php
$def=$db->Definiciones(1,1);
while($row=$def->fetch()){
	if($per['idtipodocumento']==$row['iddetalledef'])
		echo "<option value=".$row['iddetalledef']." selected=selected>".$row['detalledefinicion']."</option>";
	else
		echo "<option value=".$row['iddetalledef']." >".$row['detalledefinicion']."</option>";
	}
?>
</select>
</td>
</tr>
<tr>
  <td>Número</td>
  <td><input type="text" name="txtnumero1" id="txtnumero1" class="box1" value="<?php echo $per['identificacion']; ?>" /></td>
</tr>
<tr>
  <td>P. Nombre</td>
  <td><input type="text" name="txtpnombre1" id="txtpnombre1" class="box1" value="<?php echo $per['pnombre']; ?>" /></td>
</tr>
<tr>
  <td>S. Nombre</td>
  <td><input type="text" name="txtsnombre1" id="txtsnombre1" class="box1"  value="<?php echo $per['snombre']; ?>"/></td>
</tr>
<tr>
  <td>P. Apellido</td>
  <td><input type="text" name="txtpapellido1" id="txtpapellido1" class="box1" value="<?php echo $per['papellido']; ?>" /></td>
</tr>
<tr>
  <td>S. Apellido</td>
  <td><input type="text" name="txtsapellido1" id="txtsapellido1" class="box1"  value="<?php echo $per['sapellido']; ?>"/></td>
</tr>
</table>
</center>
&nbsp;</td>
<td class="cuerpo_de">&nbsp;</td>
</tr>

<tr>
<td class="cuerpo_iz" >&nbsp;</td>
<td class="cuerpo_ce" >&nbsp;</td>
<td class="cuerpo_de" >&nbsp;</td>
<tr>
<td class="abajo_iz" >&nbsp;</td>
<td class="abajo_ce" ></td>
<td class="abajo_de" >&nbsp;</td>
</tr>
</table>
</center>
</body>
</html>