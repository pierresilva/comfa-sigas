<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
function ean13 ($barcode) {
	$barcode=strval($barcode);
	$even_sum = $barcode[1] + $barcode[3] + $barcode[5] + $barcode[7] + $barcode[9] + $barcode[11];
	$even_sum_three = $even_sum * 3;
	$odd_sum = $barcode[0] + $barcode[2] + $barcode[4] + $barcode[6] + $barcode[8] + $barcode[10];
	$total_sum = $even_sum_three + $odd_sum;
	$next_ten = (ceil($total_sum/10))*10;
	$check_digit = $next_ten - $total_sum; 
	return $check_digit;
} 
?>