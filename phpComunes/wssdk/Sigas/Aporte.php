<?php

include_once 'Fecha.php';

/**
 * Encapsula el de una empresa
 * @author Juan Fernando Tamayo Puertas
 * @version 1.0
 */
class Aporte {
	
	public $Idaporte;
	public $Comprobante;
	public $Documento;
	public $CodigoPun;
	public $NumeroRecibo;
	public $Planilla;
	public $Idempresa;
	public $Periodo;
	public $ValorNomina;
	public $ValorTrabajador;
	public $ValorAporte;
	public $ValorPagado;
	public $Trabajadores;
	public $Ajuste;
	public $FechaPago;
	public $FechaSistema;
	public $Creador;
	
	function __construct() {
		try{
			$numArgs = count(func_get_args());
			$args = func_get_args();
			if($numArgs==0){
				$this->Idaporte = 0;
				$this->Comprobante = '';
				$this->Documento = '';
				$this->CodigoPun = '';
				$this->NumeroRecibo = '';
				$this->Planilla = '';
				$this->Idempresa = 0;
				$this->Periodo = '';
				$this->ValorNomina = 0;
				$this->ValorTrabajador = 0;
				$this->ValorAporte = 0;
				$this->ValorPagado = 0;
				$this->Trabajadores = 0;
				$this->Ajuste = '';
				$this->FechaPago = new Fecha();
				$this->FechaSistema = new Fecha();
				$this->Creador = '';
			}elseif($numArgs==1){
				if(gettype($args[0])=='array'){
					if(count($args[0])==17){						
						$this->Idaporte = $args[0]['Id'];
						$this->Comprobante = strtoupper(trim($args[0]['Comprobante']));
						$this->Documento = trim($args[0]['Documento']);
						$this->CodigoPun = trim($args[0]['CodigoPun']);
						$this->NumeroRecibo = trim($args[0]['NumeroRecibo']);
						$this->Planilla = trim($args[0]['Planilla']);
						$this->Idempresa = $args[0]['Idempresa'];
						$this->Periodo = trim($args[0]['Periodo']);
						$this->ValorNomina = $args[0]['ValorNomina'];
						$this->ValorTrabajador = $args[0]['ValorTrabajador'];
						$this->ValorAporte = $args[0]['ValorAporte'];
						$this->ValorPagado = $args[0]['ValorPagado'];
						$this->Trabajadores = $args[0]['Trabajadores'];
						$this->Ajuste = $args[0]['Ajuste'];
						$this->FechaPago = $args[0]['FechaPago']=='0001-01-01T00:00:00'?'':new Fecha($args[0]['FechaPago']);
						$this->FechaSistema = $args[0]['FechaSistema']=='0001-01-01T00:00:00'?'':new Fecha($args[0]['FechaSistema']);
						$this->Creador = trim($args[0]['Creador']);
					}else{
						echo "El numero de elementos en el array debe ser de 17, como lo exige la segunda implementacion del constructor";
						exit();
					}
				}else{
					echo "La segunda implementacion del constructor Aporte exige un argumento de tipo array";
					exit();
				}
			}elseif($numArgs==17){
				if((gettype($args[0])=='integer')&&(gettype($args[1])=='string')&&(gettype($args[2])=='string')&&(gettype($args[3])=='string')&&(gettype($args[4])=='string')&&(gettype($args[5])=='string')&&(gettype($args[6])=='integer')&&(gettype($args[7])=='string')&&(gettype($args[8])=='double')&&(gettype($args[9])=='double')&&(gettype($args[10])=='double')&&(gettype($args[11])=='double')&&(gettype($args[12])=='double')&&(gettype($args[13])=='boolean')&&(gettype($args[14])=='object')&&(gettype($args[15])=='object')&&(gettype($args[16])=='string')){
					if((get_class($args[14])!='Fecha')||(get_class($args[15])!='Fecha')){
						$this->Idaporte = $args[0];
						$this->Comprobante = strtoupper(iconv('', 'UTF-8', trim($args[1])));
						$this->Documento = iconv('', 'UTF-8', trim($args[2]));
						$this->CodigoPun = iconv('', 'UTF-8', trim($args[3]));
						$this->NumeroRecibo = iconv('', 'UTF-8', trim($args[4]));
						$this->Planilla = iconv('', 'UTF-8', trim($args[5]));
						$this->Idempresa = $args[6];
						$this->Periodo = iconv('', 'UTF-8', trim($args[7]));
						$this->ValorNomina = $args[8];
						$this->ValorTrabajador = $args[9];
						$this->ValorAporte = $args[10];
						$this->ValorPagado = $args[11];
						$this->Trabajadores = $args[12];
						$this->Ajuste = $args[13];
						$this->FechaPago = $args[14];
						$this->FechaSistema = $args[15];
						$this->Creador = iconv('', 'UTF-8', trim($args[16]));
					}else{
						echo "Los argumentos 14,15 deben ser de tipo Fecha, exigida en la segunda implementacion del constructor";
						exit();
					}
				}else{
					echo "El tipo de argumentos no corresponde a la exigida en la segunda implementacion del constructor";
					exit();
				}
			}else{
				echo "El numero de argumentos no corresponde a ninguna implementacion del constructor";
				exit();
			}
		}catch (\Exception $ex){
			echo $ex->getMessage();
			exit();
		}
	}
}

?>