<?php

include_once 'Fecha.php';

/**
 * Encapsula el periodo actual en proceso
 * @author Juan Fernando Tamayo Puertas
 * @version 1.0
 * @tutorial El constructor tiene 3 sobre cargas:<br>
 * 1) $obj = new Periodo(); //Implementacion por defecto<br>
 * 2) $ar = array();<br>$ar['Id'] = 1;<br>$ar['NombrePeriodo'] = '201206';<br>$ar['FechaInicio'] = new Fecha();<br>$ar['FechaFin'] = new Fecha();<br>$ar['FechaLimite'] = new Fecha();<br>$ar['Smlv'] = 515600;<br>$ar['Procesado'] = false; $obj = new Periodo($ar); //Implementacion por arreglo<br>
 * 3) $obj = new Periodo(1,'201206',new Fecha(),new Fecha(),new Fecha(),515600,false); //Implementacion por parametros<br>
 */
class Periodo {
	
	public $IdPeriodo;
	public $Periodo;
	public $FechaInicio;
	public $FechaFin;
	public $FechaLimite;
	public $Smlv;
	public $Procesado;
	
	function __construct() {
		try{
			$numArgs = count(func_get_args());
			$args = func_get_args();
			if($numArgs==0){
				$this->IdPeriodo = 0;
				$this->Periodo = '';
				$this->FechaInicio = new Fecha();
				$this->FechaFin = new Fecha();
				$this->FechaLimite = new Fecha();
				$this->Smlv = 0;
				$this->Procesado = false;
			}elseif($numArgs==1){
				if(gettype($args[0])=='array'){
					if(count($args[0])==7){
						$this->IdPeriodo = $args[0]['Id'];
						$this->Periodo = $args[0]['NombrePeriodo'];
						$this->FechaInicio = $args[0]['FechaInicio']=='0001-01-01T00:00:00'?'':new Fecha($args[0]['FechaInicio']);
						$this->FechaFin = $args[0]['FechaFin']=='0001-01-01T00:00:00'?'':new Fecha($args[0]['FechaFin']);
						$this->FechaLimite = $args[0]['FechaLimite']=='0001-01-01T00:00:00'?'':new Fecha($args[0]['FechaLimite']);
						$this->Smlv = $args[0]['Smlv'];
						$this->Procesado = $args[0]['Procesado'];
					}else{
						echo "El numero de elementos en el array debe ser de 7, como lo exige la segunda implementacion del constructor";
						exit();
					}
				}else{
					echo "La segunda implementacion del constructor Periodo exige un argumento de tipo array";
					exit();
				}
			}elseif($numArgs==7){
				if((gettype($args[0])=='integer')&&(gettype($args[1])=='string')&&(gettype($args[2])=='object')&&(gettype($args[3])=='object')&&(gettype($args[4])=='object')&&(gettype($args[5])=='double')&&(gettype($args[6])=='boolean')){
					if((get_class($args[2])!='Fecha')||(get_class($args[3])!='Fecha')||(get_class($args[4])!='Fecha')){
						$this->IdPeriodo = $args[0];
						$this->Periodo = iconv('', 'UTF-8', trim($args[1]));
						$this->FechaInicio = $args[2];
						$this->FechaFin = $args[3];
						$this->FechaLimite = $args[4];
						$this->Smlv = $args[5];
						$this->Procesado = $args[6];
					}else{
						echo "Los argumentos 3,4 y 5 deben ser de tipo Fecha, exigida en la segunda implementacion del constructor";
						exit();
					}
				}else{
					echo "El tipo de argumentos no corresponde a la exigida en la segunda implementacion del constructor";
					exit();
				}
			}else{
				echo "El numero de argumentos no corresponde a ninguna implementacion del constructor";
				exit();
			}
		}catch (\Exception $ex){
			echo $ex->getMessage();
			exit();
		}
	}
}

?>