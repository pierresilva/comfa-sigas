<?php

/**
 * Encapsula la definicion de un tipo en sigas
 * @author Juan Fernando Tamayo Puertas
 * @version 1.0
 * @tutorial El constructor tiene 3 sobre cargas:<br>
 * 1) $obj = new DetalleDefinicion(); //Implementacion por defecto<br>
 * 2) $ar = array();<br>$ar['Id'] = 1;<br>$ar['Iddefinicion'] = 2;<br>$ar['Codigo'] = 'CC';<br>$ar['Detalle'] = 'CEDULA DE CIUDADANIA'; $obj = new DetalleDefinicion($ar); //Implementacion por arreglo<br>
 * 3) $obj = new DetalleDefinicion(1,2,'CC','CEDULA DE CIUDADANIA'); //Implementacion por parametros<br>
 */
class DetalleDefinicion {
	
	public $Iddetalledef;
	public $Iddefinicion;
	public $Codigo;
	public $Detalle;
	
	function __construct() {
		try{
			$numArgs = count(func_get_args());
			$args = func_get_args();
			if($numArgs==0){
				$this->Iddetalledef = 0;
				$this->Iddefinicion = 0;
				$this->Codigo = '';
				$this->Detalle = '';
			}elseif($numArgs==1){
				if(gettype($args[0])=='array'){
					if(count($args[0])==4){
						$this->Iddetalledef = $args[0]['Id'];
						$this->Iddefinicion = $args[0]['Iddefinicion'];
						$this->Codigo = $args[0]['Codigo'];
						$this->Detalle = $args[0]['Detalle'];
					}else{
						echo "El numero de elementos en el array debe ser de 4, como lo exige la segunda implementacion del constructor";
						exit();
					}
				}else{
					echo "La segunda implementacion del constructor DetalleDefinicion exige un argumento de tipo array";
					exit();
				}
			}elseif($numArgs==4){
				if((gettype($args[0])=='integer')&&(gettype($args[1])=='integer')&&(gettype($args[2])=='string')&&(gettype($args[3])=='string')){
					$this->Iddetalledef = $args[0];
					$this->Iddefinicion = $args[1];
					$this->Codigo = iconv('', 'UTF-8', trim($args[2]));
					$this->Detalle = iconv('', 'UTF-8', trim($args[3]));
				}else{
					echo "El tipo de argumentos no corresponde a la exigida en la segunda implementacion del constructor";
					exit();
				}
			}else{
				echo "El numero de argumentos no corresponde a ninguna implementacion del constructor";
				exit();
			}
		}catch (\Exception $ex){
			echo $ex->getMessage();
			exit();
		}
	}
}

?>