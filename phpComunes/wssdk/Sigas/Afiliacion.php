<?php

include_once 'DetalleDefinicion.php';
include_once 'Empresa.php';
include_once 'Fecha.php';

/**
 * Encapsula la afiliacion de un trabajador
 * @author Juan Fernando Tamayo Puertas
 * @version 1.0
 */
class Afiliacion {
	
	public $IdFormulario;
	public $TipoFormulario;
	public $TipoAfiliacion;
	public $Empresa;
	public $FechaIngreso;
	public $FechaRetiro;
	public $FechaProteccion;
	public $Salario;
	public $Categoria;
	public $Primaria;
	public $Estado;
	
	function __construct() {
		try{
			$numArgs = count(func_get_args());
			$args = func_get_args();
			if($numArgs==0){
				$this->IdFormulario = 0;
				$this->TipoFormulario = new DetalleDefinicion();
				$this->TipoAfiliacion = new DetalleDefinicion();
				$this->Empresa = new Empresa();
				$this->FechaIngreso = new Fecha();
				$this->FechaRetiro = new Fecha();
				$this->FechaProteccion = new Fecha();
				$this->Salario = 0;
				$this->Categoria = '';
				$this->Primaria = false;
				$this->Estado = '';
			}elseif($numArgs==1){
				if(gettype($args[0])=='array'){
					if(count($args[0])==11){						
						$this->IdFormulario = $args[0]['Id'];
						$this->TipoFormulario = new DetalleDefinicion($args[0]['TipoFormulario']);
						$this->TipoAfiliacion = new DetalleDefinicion($args[0]['TipoAfiliacion']);
						$this->Empresa = new Empresa($args[0]['Empresa']);
						$this->FechaIngreso = $args[0]['FechaIngreso']=='0001-01-01T00:00:00'?'':new Fecha($args[0]['FechaIngreso']);
						$this->FechaRetiro = $args[0]['FechaRetiro']=='0001-01-01T00:00:00'?'':new Fecha($args[0]['FechaRetiro']);
						$this->FechaProteccion = $args[0]['FechaProteccion']=='0001-01-01T00:00:00'?'':new Fecha($args[0]['FechaProteccion']);
						$this->Salario = $args[0]['Salario'];
						$this->Categoria = $args[0]['Categoria'];
						$this->Primaria = $args[0]['Primaria'];
						$this->Estado = $args[0]['Estado'];
					}else{
						echo "El numero de elementos en el array debe ser de 11, como lo exige la segunda implementacion del constructor";
						exit();
					}
				}else{
					echo "La segunda implementacion del constructor Afiliacion exige un argumento de tipo array";
					exit();
				}
			}elseif($numArgs==11){
				if((gettype($args[0])=='integer')&&(gettype($args[1])=='object')&&(gettype($args[2])=='object')&&(gettype($args[3])=='object')&&(gettype($args[4])=='object')&&(gettype($args[5])=='object')&&(gettype($args[6])=='object')&&(gettype($args[7])=='double')&&(gettype($args[8])=='string')&&(gettype($args[9])=='boolean')&&(gettype($args[10])=='string')){
					if((get_class($args[1])!='DetalleDefinicion')||(get_class($args[2])!='DetalleDefinicion')||(get_class($args[3])!='Empresa')||(get_class($args[4])!='Fecha')||(get_class($args[5])!='Fecha')||(get_class($args[6])!='Fecha')){						
						$this->IdFormulario = $args[0];
						$this->TipoFormulario = $args[1];
						$this->TipoAfiliacion = $args[2];
						$this->Empresa = $args[3];
						$this->FechaIngreso = $args[4];
						$this->FechaRetiro = $args[5];
						$this->FechaProteccion = $args[6];
						$this->Salario = $args[7];
						$this->Categoria = strtoupper(iconv('', 'UTF-8', trim($args[8])));
						$this->Primaria = $args[9];
						$this->Estado = strtoupper(iconv('', 'UTF-8', trim($args[10])));
					}else{
						echo "Los argumentos 1,2 deben ser de tipo DetalleDefinicion, 3 debe ser de tipo Empresa, 4,5,6 deben ser de tipo Fecha, exigida en la segunda implementacion del constructor";
						exit();
					}
				}else{
					echo "El tipo de argumentos no corresponde a la exigida en la segunda implementacion del constructor";
					exit();
				}
			}else{
				echo "El numero de argumentos no corresponde a ninguna implementacion del constructor";
				exit();
			}
		}catch (\Exception $ex){
			echo $ex->getMessage();
			exit();
		}
	}
}

?>