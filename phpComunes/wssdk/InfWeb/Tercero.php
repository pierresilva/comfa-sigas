<?php

/**
 * Encapsula la información relacionada con un tercero
 * @author Juan Fernando Tamayo Puertas
 * @version 1.1
 * @tutorial Se agrego validacion de tipos en el constructor y los comentarios del mismo
 */
class Tercero {
	
	public $Aaereo = '';
	public $Autoret = '';
	public $Cargues = 0;
	public $Celular = '';
	public $Ciudad = '';
	public $Clase = '';
	public $Codigo = '';
	public $Contacto = '';
	public $CupoG = 0;
	public $CupoUsado = 0;
	public $DChequeo = 0;
	public $Depto = '';
	public $Direccion = '';
	public $Estado = 'A';
	public $Fax = '';
	public $FechaCrea = '';
	public $FechaMod = '';
	public $Grabador = '';
	public $IdAlterno = '';
	public $Intereses = '';
	public $Lista = 0;
	public $Nit = 0;
	public $Nombre = '';
	public $Pais = '';
	public $PlazoAut = 0;
	public $Prefijo = '';
	public $RespIva = '';
	public $Sexo = '';
	public $TContrib = '';
	public $Telefono = '';
	public $TesDepto = 0;
	public $TesMunicipio = 0;
	public $ZonaPostal = '';
	public $Tipoclie = '';
	public $Zona = '';
	
	/**
	 * Constructor de la clase
	 * @param int $Nit Numero de identificacion de la persona natural o Nit de la empresa
	 * @param string $Nombre Nombre del tercero, persona natural o empresa
	 * @param string $TContrib Tipo de contribuyente. N-Regimen comun, S-Regimen simplificado, E-Gran contribuyente, G-Gobierno
	 * @param string $Grabador Usuario logueado en el cliente quien consume el servicio
	 * @param string $Aaereo
	 * @param string $Autoret
	 * @param int $Cargues
	 * @param string $Celular
	 * @param string $Ciudad
	 * @param string $Clase
	 * @param string $Codigo
	 * @param string $Contacto
	 * @param int $CupoG
	 * @param int $CupoUsado
	 * @param int $DChequeo
	 * @param string $Depto
	 * @param string $Direccion
	 * @param string $Estado
	 * @param string $Fax
	 * @param string $IdAlterno
	 * @param string $Intereses
	 * @param int $Lista
	 * @param string $Pais
	 * @param int $PlazoAut
	 * @param string $Prefijo
	 * @param string $RespIva
	 * @param string $Sexo
	 * @param string $Telefono
	 * @param int $TesDepto
	 * @param int $TesMunicipio
	 * @param string $ZonaPostal
	 * @param string $Tipoclie
	 * @param string $Zona
	 */
	function __construct($Nit, $Nombre, $TContrib, $Grabador, $Aaereo = '', $Autoret = '', $Cargues = 0, $Celular = '', $Ciudad = '', $Clase = 'N', $Codigo = '', $Contacto = '', $CupoG = 0, $CupoUsado = 0, $DChequeo = 0, $Depto = '', $Direccion = '', $Estado = 'A', $Fax = '', $IdAlterno = '', $Intereses = '', $Lista = 0, $Pais = '', $PlazoAut = 0, $Prefijo = '', $RespIva = '', $Sexo = '', $Telefono = '', $TesDepto = 0, $TesMunicipio = 0, $ZonaPostal = '', $Tipoclie = '', $Zona = '') {
		$this->Nit = $Nit;
		$this->Clase = strtoupper(iconv('', 'UTF-8', trim($Clase)));
		$this->Nombre = base64_encode(strtoupper(iconv('', 'ISO-8859-1', trim($Nombre))));
		$this->Direccion = strtoupper(iconv('', 'UTF-8', trim($Direccion)));
		$this->Telefono = iconv('', 'UTF-8', trim($Telefono));
		$this->Ciudad = iconv('', 'UTF-8', trim($Ciudad));
		$this->Autoret = strtoupper(iconv('', 'UTF-8', trim($Autoret)));
		$this->Contacto = strtoupper(iconv('', 'UTF-8', trim($Contacto)));
		$this->CupoG = $CupoG;
		$this->Tipoclie = strtoupper(iconv('', 'UTF-8', trim($Tipoclie)));
		$this->Aaereo = strtolower(iconv('', 'UTF-8', trim($Aaereo)));
		$this->Fax = iconv('', 'UTF-8', trim($Fax));
		$this->Depto = iconv('', 'UTF-8', trim($Depto));
		$this->Pais = iconv('', 'UTF-8', trim($Pais));
		$this->Zona = iconv('', 'UTF-8', trim($Zona));
		$this->PlazoAut = $PlazoAut;
		$this->Estado = strtoupper(iconv('', 'UTF-8', trim($Estado)));
		$this->Intereses = iconv('', 'UTF-8', trim($Intereses));
		$this->Codigo = iconv('', 'UTF-8', trim($Codigo));
		$this->Lista = $Lista;
		$this->RespIva = strtoupper(iconv('', 'UTF-8', trim($RespIva)));
		$this->CupoUsado = $CupoUsado;
		$this->Cargues = $Cargues;
		$this->TContrib = strtoupper(iconv('', 'UTF-8', trim($TContrib)));
		$this->DChequeo = $DChequeo;
		$this->IdAlterno = iconv('', 'UTF-8', trim($IdAlterno));
		$this->Prefijo = strtoupper(iconv('', 'UTF-8', trim($Prefijo)));
		$this->Sexo = strtoupper(iconv('', 'UTF-8', trim($Sexo)));
		$this->ZonaPostal = iconv('', 'UTF-8', trim($ZonaPostal));
		$this->Celular = iconv('', 'UTF-8', trim($Celular));
		$this->TesMunicipio = $TesMunicipio;
		$this->TesDepto = $TesDepto;
		$this->Grabador = iconv('', 'UTF-8', trim($Grabador));
		$this->FechaCrea = '1900-01-01T00:00:00';
		$this->FechaMod = '1900-01-01T00:00:00';
	}
	
	function constructByResult($result) {
		$this->Nit = $result['Nit'];
		$this->Clase = $result['Clase'];
		$this->Nombre = $result['Nombre'];
		$this->Direccion = $result['Direccion'];
		$this->Telefono = $result['Telefono'];
		$this->Ciudad = $result['Ciudad'];
		$this->Autoret = $result['Autoret'];
		$this->Contacto = $result['Contacto'];
		$this->CupoG = $result['CupoG'];
		$this->Tipoclie = $result['Tipoclie'];
		$this->Aaereo = $result['Aaereo'];
		$this->Fax = $result['Fax'];
		$this->Depto = $result['Depto'];
		$this->Pais = $result['Pais'];
		$this->Zona = $result['Zona'];
		$this->PlazoAut = $result['PlazoAut'];
		$this->Estado = $result['Estado'];
		$this->Intereses = $result['Intereses'];
		$this->Codigo = $result['Codigo'];
		$this->Lista = $result['Lista'];
		$this->RespIva = $result['RespIva'];
		$this->CupoUsado = $result['CupoUsado'];
		$this->Cargues = $result['Cargues'];
		$this->TContrib = $result['TContrib'];
		$this->DChequeo = $result['DChequeo'];
		$this->IdAlterno = $result['IdAlterno'];
		$this->Prefijo = $result['Prefijo'];
		$this->Sexo = $result['Sexo'];
		$this->ZonaPostal = $result['ZonaPostal'];
		$this->Celular = $result['Celular'];
		$this->TesMunicipio = $result['TesMunicipio'];
		$this->TesDepto = $result['TesDepto'];
		$this->Grabador = $result['Grabador'];
		$this->FechaCrea = "";
		$this->FechaMod = "";
	}
}

?>