<?php

/**
 * Encapsula la informaci�n relacionada con un error
 * ocurrido en la ejecuci�n de un m�todo
 * @author Juan Fernando Tamayo Puertas
 * @version 1.0
 */
class WSError {
	
	public $ErrorDescription = '';
	
	function __construct($result) {
		$this->ErrorDescription = $result['ErrorDescription'];
	}
}

?>