<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'p.definiciones.class.php';
$objClase=new Definiciones();
$fecha=date("m/d/Y");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="es">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>::Obsevaciones::</title>
<link type="text/css" href="../css/Estilos.css" rel="stylesheet"/>
<link href="../css/marco.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/jquery-1.4.2.js"></script>
<script type="text/javascript" src="../js/comunes.js"></script>
<script type="text/javascript" src="js/observacionesBuscar.js"></script>

<script type="text/javascript">
shortcut.add("Shift+F",function() {
		                                var URL=src();
		var url=URL+"aportes/trabajadores/consultaTrabajador.php";
    	window.open(url,"_blank");
    },{
	'propagate' : true,
	'target' : document 
        });        
</script>

</head>

<body>
<form name="forma">
<center>
<!-- TABLA VISIBLE CON BOTONES -->
<br />
<table width="80%" border="0" cellspacing="0" cellpadding="0" align="center">
	<tr>
		<td width="13" height="29" class="arriba_iz">&nbsp;</td>
		<td class="arriba_ce"><span class="letrablanca">::Observaciones&nbsp;::</span></td>
		<td width="13" class="arriba_de" align="right">&nbsp;</td>
	</tr>      
	<tr>
 		<td class="cuerpo_iz">&nbsp;</td>
		<td class="cuerpo_ce">
        <img src="../imagenes/tabla/spacer.gif" width="1" height="1">
        <img src="../imagenes/spacer.gif" width="1" height="1"><img src="../imagenes/spacer.gif" width="1" height="1"><img src="../imagenes/spacer.gif" width="1" height="1"><img src="../imagenes/menu/modificar.png" alt="" width="16" height="16" style="cursor:pointer" title="Guardar Observación" onclick="guardar();" />
        <img src="../imagenes/spacer.gif" width="1" height="1">
        <img src="../imagenes/menu/refrescar.png" width="16" height="16" style="cursor:pointer" title="Limpiar campos" onClick="limpiarCampos();"> 
        <a href="../help/aportes/ayudaObservaciones.html" target="_blank" onClick="window.open(this.href, this.target, 'width=800,height=550,titlebar=0, resizable=no'); return false;" > 
        <img src="../imagenes/menu/informacion.png" width="16" height="16" style="border:none" title="Manual" onclick="" /></a>
		<img src="../imagenes/menu/notas.png" width="16" height="16" style="cursor: pointer" title="Colaboración en Línea" onclick="notas();/>
		<td class="cuerpo_de">&nbsp;</td>
	</tr> 
	<tr>     
        <td class="cuerpo_iz">&nbsp;</td>
        <td class="cuerpo_ce"><div id="error" style="color:#FF0000"></div></td>
        <td class="cuerpo_de">&nbsp;</td>
    </tr>
    <td class="cuerpo_iz">&nbsp;</td>
    <td class="cuerpo_ce">
        <table width="90%" border="0" cellspacing="0" class="tablero">
        <tr>
            <td width="25%">Buscar Por:</td>
            <td>
              <select name="buscarPor" id="buscarPor" class="box1" onchange="mostrar();">
                <option value="0" selected="selected">Seleccione</option>
                <option value="1">Afiliado</option>
                <option value="2">Empresa</option>
              </select></td>
        </tr>
        </table>
        <div id="tNumero" style="display:none">
            <table width="90%" border="0" cellspacing="0" class="tablero">
            <tr>
                <td width="25%">Tipo Documento:</td>
                <td width="25%">
                    <select name="tipoI" id="tipoI" class="box1" >
                    <?php
                    $consulta=$objClase->mostrar_datos(1, 2);
                    while($row=mssql_fetch_array($consulta)){
                        echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
                    }
                    ?>
                    </select>
                </td>
                <td width="25%">N&uacute;mero:</td>
                <td width="25%">
                <input name="numero" type="text" class="box1" id="numero" onblur="buscarPersona();" onkeypress="tabular(this,event);" /></td>
            </tr>
            <tr>
                  <td width="25%">Nombres:</td>
                  <td colspan="3" id="lnombre" width="75%">&nbsp;</td>
            </tr>
            </table>
		</div>

        <div id="tNit" style="display:none">
            <table width="90%" border="0" cellspacing="0" class="tablero">
            <tr>
              <td width="25%">NIT:</td>
              <td width="25%"><input name="txtNit" type="text" class="box1" id="txtNit" onblur="buscarNIT();" onkeypress="tabular(this,event);" /></td>
            </tr>
            <tr>
                  <td width="25%">Nombre:</td>
                  <td colspan="3" id="lnombreE" width="75%">&nbsp;</td>
            </tr>
            </table>
        </div>
        
        <table width="90%" border="0" cellspacing="0" class="tablero">
         	<tr>
				<td>Escriba las observaciones:</td>
            	<td>Observaciones antiguas:<br></td>
            </tr>
            <tr>
                  <td width="50%">
                  <textarea name="observaciones" id="observaciones" cols="67", rows="10"></textarea>
                  </td>

              	  <td width="50%" id="observacionesAnt" valign="first">
              	  </td>
            </tr>
        </table>
            <td class="cuerpo_de">&nbsp;</td><!-- FONDO DERECHA -->
            <tr>
                <td class="abajo_iz" >&nbsp;</td>
                <td class="abajo_ce" ></td>
                <td class="abajo_de" >&nbsp;</td>
            </tr>
</center>
</table>
</form>

</body>
<script language="javascript">
$("#buscarPor").focus();

function mostrarAyuda(){
	$("#ayuda").dialog('open' );
	}

function notas(){
	$("#dialog-form2").dialog('open');
	}	
</script>
</html>
