<?php 
/*
* @autor: 	
* @fecha:      16/11/2012
* objetivo:    Actualizar empresa existente, solo campos estado, clase aporte, y contratista
*/

$idEmpresa = $_REQUEST["idEmpresa"];
$estado = $_REQUEST["estado"];
$contratista = $_REQUEST["contratista"];
$claseaportante = $_REQUEST["claseaportante"];
$tipoaportante = $_REQUEST["tipoaportante"];

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
include_once $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'config.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
include_once $raiz.DIRECTORY_SEPARATOR.'aportes'.DIRECTORY_SEPARATOR.'empresas'.DIRECTORY_SEPARATOR .'clases'.DIRECTORY_SEPARATOR .'empresas.class.php';

$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

// si cambia el estado, debe cambiar la fecha de estado
$sql="UPDATE aportes048 SET contratista='$contratista', estado='$estado', claseaportante='$claseaportante', tipoaportante='$tipoaportante', legalizada='S', idtipoafiliacion=3317 WHERE idempresa=$idEmpresa";
$rs=$db->queryActualiza($sql,'aportes048');
$error=$db->error;
if(is_array($error)){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}
echo $rs;
?>