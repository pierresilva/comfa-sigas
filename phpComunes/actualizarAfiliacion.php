<?php
/* autor:       orlando puentes
 * fecha:       14/09/2010
 * objetivo:     
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

$campo0 = (empty($_REQUEST['v0'])) ? 'NULL' : $_REQUEST['v0'];  //tipo form
$campo1 = (empty($_REQUEST['v1'])) ? 'NULL' : $_REQUEST['v1'];  //tipoafiliacion
$campo2 = (empty($_REQUEST['v2'])) ? 'NULL' : $_REQUEST['v2'];  //idempresa
$campo3 = (empty($_REQUEST['v3'])) ? 'NULL' : $_REQUEST['v3'];  //idpersona
$campo4 = (empty($_REQUEST['v4'])) ? 'NULL' : "'".$_REQUEST['v4']."'";  //fechaingre
$campo5 = (empty($_REQUEST['v5'])) ? 'NULL' : $_REQUEST['v5'];  //horas dia
$campo6 = (empty($_REQUEST['v6'])) ? 'NULL' : $_REQUEST['v6'];  //horas mes
$campo7 = (empty($_REQUEST['v7'])) ? 'NULL' : $_REQUEST['v7'];  //salario
$campo8 = (empty($_REQUEST['v8'])) ? 'NULL' : "'".$_REQUEST['v8']."'"; //agricola
$campo9 = (empty($_REQUEST['v9'])) ? 'NULL' : "'".$_REQUEST['v9']."'";	//cargo
$campo10 = (empty($_REQUEST['v10'])) ? 'NULL' : "'".$_REQUEST['v10']."'";  //primaria
$campo11 = (empty($_REQUEST['v11'])) ? 'NULL' : "'".$_REQUEST['v11']."'";  //estado
$campo12 = (empty($_REQUEST['v12'])) ? 'NULL' : "'".$_REQUEST['v12']."'";  //traslado
$campo13 = (empty($_REQUEST['v13'])) ? 'NULL' : "'".$_REQUEST['v13']."'";  //cod caja
$campo15 = "'".$_SESSION['USUARIO']."'";
$campo16 = (empty($_REQUEST['v16'])) ? 'NULL' : "'".$_REQUEST['v16']."'"; 	//tipo pago
$campo17 = (empty($_REQUEST['v17'])) ? 'NULL' : "'".$_REQUEST['v17']."'"; //categoria
$campo18 = (empty($_REQUEST['v18'])) ? 'NULL' : "'".$_REQUEST['v18']."'"; 	//auditado
$campo19 = "'".$_SESSION['AGENCIA']."'";	//agencia
$campo20 = (empty($_REQUEST['v20'])) ? 'NULL' : $_REQUEST['v20'];	//id radicacion
$campo21 = (empty($_REQUEST['v21'])) ? 'NULL' : "'".$_REQUEST['v21']."'";	//vendedor

$sql="UPDATE aportes016 SET tipoformulario=$campo0, tipoafiliacion=$campo1, fechaingreso=$campo4, horasdia=$campo5,
	  horasmes=$campo6, salario=$campo7, agricola=$campo8, cargo=$campo9, primaria=$campo10, estado=$campo11,
	  traslado=$campo12, codigocaja=$campo13, fechasistema=cast(getdate() as date), usuario=$campo15, tipopago=$campo16,
	  categoria=$campo17, auditado=$campo18, idagencia=$campo19, idradicacion=$campo20, vendedor=$campo21
	  WHERE idpersona=$campo3 AND idempresa=$campo2";

$rs=$db->queryActualiza($sql,'aportes016');

$error=$db->error;
if(is_array($error)){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}
echo $rs;

/*
if(is_null($rs))
	echo 0;
else
	echo $rs;
*/
die();
?>