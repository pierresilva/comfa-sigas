<?php 
/*
* @autor:      Ing. Orlando Puentes
* @fecha:      04/09/2010
* objetivo:
*/
ini_set("display_errors",'1');
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}
$c0=$_REQUEST['v0'];
$c1=$_REQUEST['v1'];
$sql="select top 50 nit,aportes011.idaporte,periodo,valornomina,valoraporte,aportes011.trabajadores,ajuste,
fechapago,comprobante,documento,numerorecibo,aportes011.fechasistema,aportes011.usuario 
from aportes011 
INNER JOIN aportes048 ON aportes011.idempresa=aportes048.idempresa 
where aportes011.idempresa in (Select idempresa from aportes016 where idpersona=$c0)"; 
$result=$db->querySimple($sql);
$filas=array();
$con=0;
while ($row=$result->fetch()){
	$filas[]=$row;
	$con++;
}
if ($con==0){
	echo 0;
}
else {
	echo json_encode($filas);
}
?>
