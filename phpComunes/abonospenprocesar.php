<?php
/* autor:       orlando puentes
 * fecha:       29/06/2010
 * objetivo:    
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
include_once $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'config.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

$solicituajax=$_REQUEST['solajax'];

if($solicituajax=='1'){  // decision utilizar para consultar la tabla aportes 104 y genera report N016
    $tipo = $_REQUEST['tipo'];  //siempre va a venir
    $tipoiden= $_REQUEST['tipoiden'];
    $identificacion=$_REQUEST['identificacion'];
    $tipoinforme=$_REQUEST['tipoinforme'];   //siempre va a venir
    /*
    $tipo = 0;  //siempre va a venir
    $tipoiden= "1";
    $identificacion="36265110";
    $tipoinforme=2;   //siempre va a venir
    */

    // se evalua si los datos tipoidentificacion y tipo identificacion esta diligenciados
    $complemento=""; //lo utilizamos para concatenar las decisiones
  

    //se evalua el tipo afiliación que se esta requiriendo
    switch ($tipo){
	    case "1":
	       $consu_sql="and tipopago IN ('T','E')";	
	       break;
	    case "2":
	       $consu_sql="and tipopago IN ('F')";
	       break;
	    default:
	       $consu_sql="and tipopago IN ('T','E','F')";
	       break;
     }

     if($tipoiden!=0 and $identificacion!=""){
	    $componen="(idtipodocumento='".$tipoiden."' and identificacion='".$identificacion."')";
	    $consu_sql.= " and ".$componen;
     }

     $data = array();
     $sql="select idtrabajador,rtrim(papellido)+' '+rtrim(pnombre) as nombre,identificacion, aportes104.periodogiro, tipopago
     ,isnull((aportes104.valor - aportes115.valor),aportes104.valor) AS valor,bono,direccion,telefono
     from aportes104
     inner join aportes015 on aportes015.idpersona = aportes104.idtrabajador
     left  join aportes101 on aportes101.idpersona = aportes104.idtrabajador and aportes101.estado = 'A'
     left  join aportes115 on aportes115.idtraslado= aportes104.idtraslado and aportes104.reverso = 'S'
     where procesado = 'N' ".$consu_sql." order by idtrabajador";
     $abonadoaux=$db->querySimple($sql);
     //$canreg= count($abonadoaux->fetchall()); // se indentifica el numero de registros
     $valtotal=0;
     $canperiodos=0;


     if($tipoinforme==1){
	    while ($row=$abonadoaux->fetch()){
	    	//verificar si tiene registrado seguimientos a bonos no procesados
	    	$observaciones=$db->consu_observaciones('1',$row['idtrabajador']);
	    	$res=count($observaciones->fetchAll());
	    	$row['num_seguimiento']=$res;
		    $data[]=$row;
	    }
     }else{
          $row=$abonadoaux->fetchAll();
          $canfilas=count($row);
          for($i=0;$i<$canfilas;$i++){
	          for($c=$i;$c<$canfilas;$c++){
		          if($row[$i]['identificacion']==$row[$c]['identificacion']){
		             $canperiodos++;
	                 $valtotal=$valtotal+$row[$c]['valor'];
	                 $nombre=$row[$c]['nombre'];
	                 $identificacion=$row[$c]['identificacion'];
	                 $idtrabajador=$row[$c]['idtrabajador'];
	                 $periodogiro=$canperiodos;
	                 $tipopago=$row[$c]['ntipopago'];
	                 $bono=$row[$c]['bono'];
	                 $direccion=$row[$c]['direccion'];
	                 $telefono=$row[$c]['telefono'];
		           }
		           else{
			          // se monta los datos al objeto data_2 con información consolidada
		           	  //verificar si tiene registrado seguimientos a bonos no procesados
		           	  $observaciones=$db->consu_observaciones('1',$idtrabajador);
		           	  $res=count($observaciones->fetchAll());
		           	  $data_2= new stdClass();
			          $data_2->nombre=$nombre;
			          $data_2->identificacion=$identificacion;
			          $data_2->idtrabajador=$idtrabajador;
			          $data_2->periodogiro=$periodogiro;
			          $data_2->tipopago=$tipopago;
			          $data_2->valor=$valtotal;
			          $data_2->direccion=$direccion;
			          $data_2->telefono=$telefono;
			          $data_2->num_seguimiento=$res;
	                  $data[]=$data_2;
	                  $valtotal=0;
	                  $canperiodos=0;
	                  $i=$c-1;
	                  break;
		           }
		
	          }
              if($c==$canfilas){
              	 $observaciones=$db->consu_observaciones('1',$idtrabajador);
              	 $res=count($observaciones->fetchAll());
              	 $row['num_seguimiento']=$res;
  	             $data_2= new stdClass();
	             $data_2->nombre=$nombre;
	             $data_2->identificacion=$identificacion;
	             $data_2->idtrabajador=$idtrabajador;
	             $data_2->periodogiro=$periodogiro;
	             $data_2->tipopago=$tipopago;
	             $data_2->valor=$valtotal;
	             $data_2->direccion=$direccion;
	             $data_2->telefono=$telefono;
	             $data_2->num_seguimiento=$res;
	             $data[]=$data_2;
  	             break;
              }
          }
     }
   unset($row);
}// fin solicitu ajax == 1 consulta abonos pendientes de proocesar reporte N016
elseif($solicituajax==2){
		//$data['nombre']="Nombre".$_POST['nombre']." identificacion".$_POST['identificacion']." id trabajador ".$_POST['idtrabajador']." fecha:".$_POST['fecha']." hora".$_POST['hora'];
	    $sql="insert into aportes122(tipoobservacion,nombrepersona,identificacion,idpersona,fecha,hora,observacion,efectiva) values(1,'".$_POST['nombre']."','".$_POST['identificacion']."','".$_POST['idtrabajador']."','".$_POST['fecha']."','".$_POST['hora']."','".$_POST['observacion']."','".$_POST['efectiva']."')";
	    $insertse=$db->queryInsert($sql,'aportes122');
	    if($insertse!=null)
	        $data['validacion']=$insertse;
	    else{ 
	    	$data['validacion']=0;
	        $data['error']=$db->error.' sql :'.$sql;
	    }    
	}
$db=null;
echo json_encode($data);

?>