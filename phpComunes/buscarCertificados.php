<?php
/* autor: Oswaldo Gonz�lez
 * fecha: 05/07/2012
 * objetivo: Buscar certificados de beneficiarios    
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'aportes' . DIRECTORY_SEPARATOR . 'otrosProcesos' . DIRECTORY_SEPARATOR . 'certificados' . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'certificados.class.php';
$objCertificados = new Certificados();
$idBeneficiario = $_REQUEST["idBeneficiario"];
$consulta = $objCertificados->buscar_certificados_beneficiario($idBeneficiario);
$data = array();
while($row = mssql_fetch_array($consulta))
	$data[]=$row;

if(count($data) > 0)
	echo json_encode($data);
else
	echo 0;
?>