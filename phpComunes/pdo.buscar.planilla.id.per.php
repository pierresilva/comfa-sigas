<?php 
/*
* @autor:      Ing. Orlando Puentes
* @fecha:      04/09/2010
* objetivo:
*/
ini_set("display_errors",'1');
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}
$c0=$_REQUEST['v0'];
$c1=$_REQUEST['v1'];
$sql="select TOP 50 idplanilla, planilla, aportes010.nit, aportes048.razonsocial as rsocial, salariobasico, ingresobase, diascotizados, isnull(ingreso,'')ingreso, isnull(retiro,'')retiro, isnull(var_tra_salario,'')var_tra_salario, isnull(var_per_salario,'')var_per_salario, isnull(sus_tem_contrato,'')sus_tem_contrato, isnull(inc_tem_emfermedad,'')inc_tem_emfermedad, isnull(lic_maternidad,'')lic_maternidad, isnull(vacaciones,'')vacaciones, isnull(inc_tem_acc_trabajo,'')inc_tem_acc_trabajo, isnull(tipo_cotizante,'')tipo_cotizante, isnull(periodo,'')periodo, correccion, aportes010.idempresa, idtrabajador, horascotizadas, procesado, control,  fechapago, aportes010.fechasistema, usuariomodifica, fechamodifica, valoraporte from aportes010 inner join aportes048 on aportes048.nit=aportes010.nit where idtrabajador=$c0 ORDER BY periodo desc";
$result=$db->querySimple($sql);
$filas=array();
$con=0;
while (($row=$result->fetch())==true){
	$filas[]=$row;
	$con++;
}
if ($con==0){
	echo 0;
}
else {
	echo json_encode($filas);
}
?>
