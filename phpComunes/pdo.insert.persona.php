<?php 
/*
* @autor:      Ing. Orlando Puentes
* @fecha:      04/09/2010
* objetivo:
*/
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$campo0=(empty($_REQUEST['v0'])) ? NULL : $_REQUEST['v0'];		//idtipodocumento
$campo1 = (empty($_REQUEST['v1'])) ? NULL : trim($_REQUEST['v1']);	//numero
$campo2=strtoupper(trim($_REQUEST['v2']));	//primer apellido
$campo3=strtoupper(trim($_REQUEST['v3']));	//segundo apellido
$campo4=strtoupper(trim($_REQUEST['v4']));	//primer nombre
$campo5=strtoupper(trim($_REQUEST['v5']));	//segundo nombre
$campo6=(empty($_REQUEST['v6'])) ? NULL : $_REQUEST['v6'];		//sexo
$campo7=(empty($_REQUEST['v7'])) ? NULL : $_REQUEST['v7'];		//estado civil
$campo8=(empty($_REQUEST['v8'])) ? NULL : $_REQUEST['v8'];		//fecha nace
$campo9=(empty($_REQUEST['v9'])) ? NULL : $_REQUEST['v9'];		//ciudad nace
$campo10=(empty($_REQUEST['v10'])) ? NULL : $_REQUEST['v10'];	//departamento nace
$campo11=(empty($_REQUEST['v11'])) ? NULL : $_REQUEST['v11'];	//capacidad trabajo
$campo12=(empty($_REQUEST['v12'])) ? NULL : $_REQUEST['v12'];   //
$campo13=(empty($_REQUEST['v13'])) ? NULL : $_REQUEST['v13'];
$campo14=(empty($_REQUEST['v14'])) ? NULL : $_REQUEST['v14'];
$campo15=(empty($_REQUEST['v15'])) ? NULL : $_REQUEST['v15'];
$campo16=(empty($_REQUEST['v16'])) ? NULL : $_REQUEST['v16'];
$campo17=(empty($_REQUEST['v17'])) ? NULL : $_REQUEST['v17'];
$campo18=(empty($_REQUEST['v18'])) ? NULL : $_REQUEST['v18'];
$campo19=(empty($_REQUEST['v19'])) ? NULL : $_REQUEST['v19'];

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
		exit();
}
// direccion, idbarrio, telefono, celular, email, idpropiedadvivienda, idtipovivienda, idciuresidencia, iddepresidencia, idzona, , , , , , desplazado, discapacitado, reinsertado, idescolaridad, idprofesion, conyuges, hijos, hijosmenores, hermanos, padres, tarjeta, documentacionfonede, ruaf, biometria, rutadocumentos, fechaafiliacion, estado, validado, flag, tempo1, tempo2, tempo3, usuario, fechasistema, nombrecorto)
$sql="INSERT INTO aportes015 (idtipodocumento, identificacion, papellido, sapellido, pnombre, snombre, sexo, idestadocivil, fechanacimiento, idciunace, iddepnace,capacidadtrabajo,estado,fechasistema,nombrecorto) values(:campo0, :campo1, :campo2, :campo3, :campo4, :campo5, :campo6, :campo7, :campo8, :campo9, :campo10, :campo11, 'N', cast(getdate() as date), :campo19)";
$statement = $db->conexionID->prepare($sql);
$guardada = false;

$statement->bindParam(":campo0", $campo0, PDO::PARAM_INT);
$statement->bindParam(":campo1", $campo1, PDO::PARAM_STR);
$statement->bindParam(":campo2", $campo2, PDO::PARAM_STR);
$statement->bindParam(":campo3", $campo3, PDO::PARAM_STR);
$statement->bindParam(":campo4", $campo4, PDO::PARAM_STR);
$statement->bindParam(":campo5", $campo5, PDO::PARAM_STR);
$statement->bindParam(":campo6", $campo6, PDO::PARAM_STR);
$statement->bindParam(":campo7", $campo7, PDO::PARAM_INT);
$statement->bindParam(":campo8", $campo8, PDO::PARAM_STR);
$statement->bindParam(":campo9", $campo9, PDO::PARAM_STR);
$statement->bindParam(":campo10", $campo10, PDO::PARAM_STR);
$statement->bindParam(":campo11", $campo11, PDO::PARAM_STR);
$statement->bindParam(":campo19", $campo19, PDO::PARAM_STR);
$guardada = $statement->execute();
if($guardada){
	// buscar id de la empresa creada
	$rs = $db->conexionID->lastInsertId();
	echo trim($rs);
}else{
	// errores
	$error = $statement->errorInfo();
	echo "Error codigo {$error[0]} con el mensaje >>> {$error[2]}";
}
die();
?>
