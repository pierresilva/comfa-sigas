<?php 
/*
* @autor:      Ing. Orlando Puentes
* @fecha:      04/09/2010
* objetivo:
*/
ini_set("display_errors",'1');
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}
$c0=$_REQUEST['v0'];	//tipo documento
$c1=$_REQUEST['v1'];	//numero
$c2=$_REQUEST['v2'];	//flag 
// 1 por identificacion
// 2 por documento y numero
// 3
// 4
// 5 por idpersona
//$idtd=0,$num=0,$flag=0
$result=$db->Persona($c0,$c1,$c2);
$filas=array();
$con=0;
while ($row=$result->fetch()){
	$filas[]=array_map("utf8_encode",$row);
	$con++;
}
if ($con==0){
	echo 0;
}
else {
	echo json_encode($filas);
}
?>
