<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

if ( !isset( $_SESSION ['USUARIO'] ) ) {
	session_start();
	$raiz=$_SESSION['RAIZ'];
	include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'session.php';
	include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'wssdk' . DIRECTORY_SEPARATOR . 'ClientWSSigas.php';
	include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';
	session();
}
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
include_once $raiz.DIRECTORY_SEPARATOR.'phpComunes'.DIRECTORY_SEPARATOR.'wssdk'.DIRECTORY_SEPARATOR.'ClientWSSigas.php';
include_once $raiz.DIRECTORY_SEPARATOR.'config.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}
$ws = new ClientWSSigas(USUARIO_WEB_SIGAS, CONTRASENA_WEB_SIGAS);
$rutaOrigen=$_REQUEST['rutaOrigen']; //numero identificacion Trabajador o Empresa
$rutaDestino=$_REQUEST['rutaDestino'];//Trabajador o Empresa

$exito=$ws->rutaDigitalizacionMigrar($rutaOrigen, $rutaDestino);

if($exito==true)
{
	echo 1;
}else {
	echo 0;
}

?>