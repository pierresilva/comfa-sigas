

<?php
/*
 * idrequerimiento
 * idtiporequerimiento
 * requerimiento
 * aprobado
 * fechaaprobacion
 * asignadoa
 * fechaasignacion
 * fechaentrega
 * notas
 * fechasistema
 * usuario
 * */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
$usuario=$_SESSION['USUARIO'];
include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'p.definiciones.class.php';
$objClase=new Definiciones();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Requerimiento</title>
<link type="text/css" href="../css/Estilos.css" rel="stylesheet"/>
<link type="text/css" href="../css/marco.css" rel="stylesheet"/>
<link type="text/css" rel="stylesheet" href="../css/css.1.8/jquery-ui-1.8.17.custom.css" />
<script language="javascript" src="../js/1.6/jquery-1.6.2.min.js"></script>
<script language="javascript" src="../js/1.6/jquery-ui-1.8.16.custom.min.js"></script>
<script language="javascript" src="js/seguimientoRequerimiento.js"></script>

</head>

<body>
<center>
<table width="80%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td class="arriba_iz" >&nbsp;</td>
<td class="arriba_ce" ><span class="letrablanca">::Seguimiento Requerimientos&nbsp;::</span></td>
<td class="arriba_de" >&nbsp;</td>
</tr>
<tr>
<td class="cuerpo_iz">&nbsp;</td>
<td class="cuerpo_ce">
<img src="../imagenes/spacer.gif" alt="" width="2" height="1" /> 
<img src="../imagenes/menu/buscar.png" alt="" width="16" height="16" id="bBuscar" style="cursor:pointer" title="Buscar" onclick="buscarR();" />
<img src="../imagenes/spacer.gif" alt="" width="2" height="1" /> 
<img src="../imagenes/menu/modificar.png" alt="" width="16" height="16" id="bGuardar" style="cursor:pointer" title="Guardar" onclick="guardarR();" />
</td>
<td class="cuerpo_de">&nbsp;</td>
</tr>
<tr>
<td class="cuerpo_iz" >&nbsp;<input type="hidden" name="hidIdRequerimiento" id="hidIdRequerimiento"/></td>
<td class="cuerpo_ce" >
<table width="100%" border="0" class="tablero" cellspacing="0">
  <tr>
    <td width="18%">Req N&uacute;mero</td>
    <td width="82%" id='tdNumero'><input type="text" name="txtNumeroRequerimiento" id="txtNumeroRequerimiento"/></td>
  </tr>
  <tr>
    <td width="18%">Fecha</td>
    <td width="82%"><label id="lblFecha"></label></td>
  </tr>
  <tr>
    <td width="18%">Tipo requerimiento</td>
    <td width="82%">
    <select id="cboTipoRequerimiento" class="box1">
    <option value="0">Seleccione...</option>
    <?php
		$consulta= $objClase->mostrar_datos(60,1);
		while($row=mssql_fetch_array($consulta)){
		echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
		}
	?>
    </select>
    </td>
  </tr>
  <tr>
    <td>Requerimiento</td>
    <td><label id="lblRequerimiento" ></label></td>
  </tr>
  <tr>
    <td width="18%">Aprobado</td>
    <td width="82%">Si: <input type="radio" name="chbAprobado" id="chbAprobadoA" value="A"/>No: <input type="radio" name="chbAprobado" id="chbAprobadoP" value="P"/></td>
  </tr>
  <tr>
    <td width="18%">Fecha Aprobaci&oacute;n</td>
    <td width="82%"><input type="text" id="txtFechaAprobacion" name="txtFechaAprobacion"/></td>
  </tr>
  <tr>
    <td width="18%">Asignado A</td>
    <td width="82%"><input type="text" name="txtUsuario" id="txtUsuario" /></td>
  </tr>
  <tr>
    <td width="18%">Fecha Asignaci&oacute;n </td>
    <td width="82%"><input type="text" name="txtFechaAsignacion" id="txtFechaAsignacion"/></td>
  </tr>
  <tr>
    <td width="18%">Fecha Entrega</td>
    <td width="82%"><input type="text" name="txtFechaEntrega" id="txtFechaEntrega" /></td>
  </tr>
  <tr>
    <td width="18%">Notas</td>
    <td width="82%"><textarea id="txtNotas" name="txtNotas" cols="100" rows="5"></textarea></td>
  </tr>
  <tr>
    <td width="18%">Usuario</td>
    <td width="82%"><label id="lblUsuario"></label></td>
  </tr>
</table></td>
<td class="cuerpo_de" >&nbsp;</td>
</tr>
<tr>
<td class="cuerpo_iz" >&nbsp;</td>
<td class="cuerpo_ce" >&nbsp;</td>
<td class="cuerpo_de" >&nbsp;</td>
</tr>
<tr>
<td class="abajo_iz" >&nbsp;</td>
<td class="abajo_ce" ></td>
<td class="abajo_de" >&nbsp;</td>
</tr>
</table>

</center>
</body>
</html>
