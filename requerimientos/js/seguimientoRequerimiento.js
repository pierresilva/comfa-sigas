$(document).ready(function(){
	$("#txtFechaAprobacion, #txtFechaAsignacion, #txtFechaEntrega").datepicker({
		changeMonth: true,
		changeYear: true,
		maxDate: "+0D"});
});

function buscarR(){
	var idRequerimiento = $.trim($("#txtNumeroRequerimiento").val());
	if(idRequerimiento == 0){
		alert("Debe digitar el N�mero Requerimiento");
		return false;
	}
	/*
	 * idrequerimiento
	* idtiporequerimiento
	* requerimiento
	* aprobado
	* fechaaprobacion
	* asignadoa
	* fechaasignacion
	* fechaentrega
	* notas
	* fechasistema
	* usuario
	* */
	$.getJSON('buscarRequerimiento.php',{v0:idRequerimiento},function(datos){
		$("#hidIdRequerimiento").val(datos.idrequerimiento);
		$("#txtNumeroRequerimiento").val(datos.idrequerimiento);
		$("#lblFecha").text(datos.fechasistema);
		$("#cboTipoRequerimiento").val(datos.idtiporequerimiento);
		$("#lblRequerimiento").text(datos.requerimiento);
		$("#lblUsuario").text(datos.usuario);
		if(datos.aprobado == "P")
			$("#chbAprobadoP").attr("checked","checked");
		else
			$("#chbAprobadoA").attr("checked","checked");
		$("#txtFechaAprobacion").val(datos.fechaaprobacion);
		$("#txtUsuario").val(datos.asignadoa);
		$("#txtFechaAsignacion").val(datos.fechaasignacion);
		$("#txtFechaEntrega").val(datos.fechaentrega);
		$("#txtNotas").val(datos.notas);
	});	
}

function guardarR(){
	var idRequerimiento = $.trim($("#hidIdRequerimiento").val());
	if(!idRequerimiento){
		alert("Primero debe gestionar la busqueda");
		return false;
	}
	//var idTipoRequerimiento = $("#cboTipoRequerimiento").val(); 
	//var requerimiento = $("#lblRequerimiento").text();
	//var  = $("input[name='chbAprobado']:checked").val(); 
	var aprobado = $("input:radio[name='chbAprobado']:checked'").val();
	var fechaAprobacion = $("#txtFechaAprobacion").val();
	var asignadoA = $("#txtUsuario").val();
	var fechaAsignacion = $("#txtFechaAsignacion").val();
	var fechaEntrega = $("#txtFechaEntrega").val();
	var notas = $("#txtNotas").val();
	//var fecha = $("#lblFecha").text();
	//var usuario = $("#lblUsuario").text();
	
	$.getJSON('actualizarRequerimiento.php', {
		v0:idRequerimiento,
		v1:aprobado, 
		v2:fechaAprobacion,
		v3:asignadoA,
		v4:fechaAsignacion,
		v5:fechaEntrega,
		v6:notas}, function(datos){
		if(datos == 0){
			alert("No fu� posible guardar la informaci�n.");
			return false;
		}
		alert("La informaci�n fu� actualizada");
	});
	

}