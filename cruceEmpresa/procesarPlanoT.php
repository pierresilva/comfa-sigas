<?php
set_time_limit ( 0 );
ini_set ( "display_errors", '1' );
date_default_timezone_set ( "America/Bogota" );

$c0=$_REQUEST["v0"];

$root = $_SERVER ['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once $root;
include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'funcionesComunes' . DIRECTORY_SEPARATOR . 'funcionesComunes.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'rsc' . DIRECTORY_SEPARATOR . 'pdo' . DIRECTORY_SEPARATOR . 'IFXDbManejador.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'rsc' . DIRECTORY_SEPARATOR . 'pdo' . DIRECTORY_SEPARATOR . 'IFXerror.php';
$db = IFXDbManejador::conectarDB ();
if ($db->conexionID == null) {
	$cadena = $db->error;
	echo -2;
	exit ();
}

$directorioProcesado = $ruta_cargados . "credito/procesados/";
if (!file_exists($directorioProcesado)) {
	mkdir($directorioProcesado, 0777, true);
}
$archivo='reporte.csv';
$rutadelplano=$directorioProcesado.DIRECTORY_SEPARATOR.$archivo;
$cadena = "NIT;RAZON SOCIAL;DIRECCION;TELEFONO;CORREO;No EMPLEADOS;IDENTIFICACION;NOMBRE REPRESENTANTE LEGAL;AGENCIA;OPERADOR;ESTADO\r\n";
$fp=fopen($rutadelplano,'w');
fwrite($fp, $cadena);
fclose($fp);

		$sql="SELECT a48.nit,a48.razonsocial,a48.direccion,a48.telefono,a48.email,
		(SELECT count(DISTINCT idpersona) FROM aportes016 a16 WHERE a16.estado='A' AND a16.idempresa=a48.idempresa) AS trabajadores,
		a15.identificacion,a15.pnombre+' '+isnull(a15.snombre,'')+' '+a15.papellido+' '+isnull(a15.sapellido,'') AS representante,
		a500.agencia,(SELECT TOP 1 codigooperador FROM aportes031 WHERE nit=a48.nit ORDER BY periodo DESC) AS operador,
		a48.estado
		FROM aportes048 a48
		LEFT JOIN aportes015 a15 ON a48.idrepresentante=a15.idpersona
		LEFT JOIN aportes500 a500 ON a48.seccional=a500.codigo
		WHERE a48.estado='A' AND a48.idtipoafiliacion=3316";
		$rs=$db->querySimple($sql);
		$row=$rs->fetch();
		
		while (($row=$rs->fetch())==true){
			
			$cadena .= "$row[nit];$row[razonsocial];$row[direccion];$row[telefono];$row[email];$row[trabajadores];$row[identificacion];$row[representante];$row[agencia];$row[operador];$row[estado]"."\r\n";
		
		}
		$fp=fopen($rutadelplano,'w');
		fwrite($fp, $cadena);
		fclose($fp);

$_SESSION['ENLACE']=$rutadelplano;
$_SESSION['ARCHIVO']=$archivo;
echo 1;
?>
