<?php
/* autor:       orlando puentes
 * fecha:       01/07/2010
 * objetivo:     
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;


$fechaHoy = date("m/d/Y h:i A");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Reporte 1</title>
<link type="text/css" href="../../../css/estiloReporte.css" rel="stylesheet"/>
<link href="../../css/style.css" rel="stylesheet" type="text/css" />
</head>

<body>
<table width="100%" border="0">
  <tr>
    <td ><div align="center"><img src="../imagenes/logo_reporte.png" width="707" height="86" /></div></td>
  </tr>
  <tr>
    <td align="center" ><strong class="titulo">TITULO</strong></td>
  </tr>
  <tr>
    <td class="fecha" >Fecha impresi&oacute;n: <?php echo $fechaHoy; ?></td>
  </tr>
</table>

<table  border="0" class="tablero" width="100%">
  <tr>
    <th>Titulo 1</th>
    <th>Titulo 2</th>
    <th>Titulo 3</th>
    <th>Titulo 4</th>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
</body>
</html>
