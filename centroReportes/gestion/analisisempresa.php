<?php
date_default_timezone_set('America/Bogota');
set_time_limit(0);
$raiz="";
//ini_set("display_errors",'1');
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}
include_once '../../config.php';
$path_plano =$ruta_generados.'gestion'.DIRECTORY_SEPARATOR;

$fecha=date('Ymd');
$archivo='empresa'.$fecha.'.csv';
$path_plano.=$archivo;

$fp=fopen($path_plano,'w');
$cadena="";
fwrite($fp, $cadena);
fclose($fp);

$fechaI=$_REQUEST['fechaI'];
$fechaF=$_REQUEST['fechaF'];

$sql = "DECLARE @fecha_pago_i DATE='$fechaI', @fecha_pago_f DATE ='$fechaF' 
		SELECT a48.idempresa,a48.nit,a48.razonsocial,a48.actieconomicadane,a79.descripcion,a48.estado,a91.detalledefinicion,b91.detalledefinicion as tipoAfiliacion,sum(a11.valorpagado) AS valorpagado,convert(CHAR(6),a11.fechapago,112) AS periodo
			,(
				SELECT 'Fecha Pago'+' '+ CONVERT(VARCHAR,b11.fechapago,110) +' '+ 'Periodo'+' '+ b11.periodo+' - '
				FROM aportes011 b11
				WHERE b11.idempresa=a48.idempresa 
					AND fechapago BETWEEN @fecha_pago_i AND @fecha_pago_f
				GROUP BY b11.fechapago, b11.periodo
				FOR xml path('')
			) AS fechas
		FROM aportes048 a48
			INNER JOIN aportes011 a11 ON a11.idempresa=a48.idempresa
			left join aportes079 a79 on a79.clase=a48.actieconomicadane and a79.clase!='' and len(a79.clase)>='4'
			left join aportes091 a91 on a48.tipoaportante=a91.iddetalledef
			left join aportes091 b91 on a48.idtipoafiliacion=b91.iddetalledef
		WHERE fechapago BETWEEN @fecha_pago_i AND @fecha_pago_f
		GROUP BY a48.nit,a48.razonsocial,a48.actieconomicadane,a79.descripcion,a48.estado,a91.detalledefinicion,b91.detalledefinicion, a48.idempresa,convert(CHAR(6),a11.fechapago,112)";

$rs=$db->querySimple($sql);

$arrDatosEmpresa = array();
$arrPeriodo = array();

while (($rowDatosEmpresa=$rs->fetch())==true){
	$arrDatosEmpresa[] = $rowDatosEmpresa;
	//Obtener las columnas de los periodos
	if(!in_array($rowDatosEmpresa["periodo"],$arrPeriodo)){	
		$arrPeriodo[] = $rowDatosEmpresa["periodo"];
	}
}

$banderaArrayFinal = array();
//Ordernar los datos del arreglo
asort($arrPeriodo);

foreach($arrDatosEmpresa as $datoEmpresa){
	$banderaArrayFinal[$datoEmpresa["idempresa"]]["nit"] = $datoEmpresa["nit"];
	$banderaArrayFinal[$datoEmpresa["idempresa"]]["razonsocial"] = $datoEmpresa["razonsocial"];
	$banderaArrayFinal[$datoEmpresa["idempresa"]]["actieconomicadane"] = $datoEmpresa["actieconomicadane"];
	$banderaArrayFinal[$datoEmpresa["idempresa"]]["descripcion"] = $datoEmpresa["descripcion"];
	$banderaArrayFinal[$datoEmpresa["idempresa"]]["estado"] = $datoEmpresa["estado"];
	$banderaArrayFinal[$datoEmpresa["idempresa"]]["detalledefinicion"] = $datoEmpresa["detalledefinicion"];
	$banderaArrayFinal[$datoEmpresa["idempresa"]]["tipoAfiliacion"] = $datoEmpresa["tipoAfiliacion"];
	$banderaArrayFinal[$datoEmpresa["idempresa"]]["fechas"] = $datoEmpresa["fechas"];
	
	foreach ($arrPeriodo as $periodo){
		if(!isset($banderaArrayFinal[$datoEmpresa["idempresa"]][$periodo])){
			$banderaArrayFinal[$datoEmpresa["idempresa"]][$periodo] = 0;
		}
		if($periodo==$datoEmpresa["periodo"]){
			 $banderaArrayFinal[$datoEmpresa["idempresa"]][$periodo] = $datoEmpresa["valorpagado"];
		}
	}
}

$banderaFilas = "";
foreach ($banderaArrayFinal as $datoEmpresa){
	$banderaFilas .= implode($datoEmpresa,";") . "\r\n";
}

$banderaFilas="NIT;RAZON SOCIAL;CODIGO CIUU;DESCRIPCION ACTIVIDAD;ESTADO;TIPO APORTANTE;TIPO AFILIACION;FECHAS DE PAGO;".implode($arrPeriodo,";")."\r\n".$banderaFilas;

$fp=fopen($path_plano,'a');
fwrite($fp, $banderaFilas);
fclose($fp);

$_SESSION['ENLACE']=$path_plano;
$_SESSION['ARCHIVO']=$archivo;
echo 1;
?>