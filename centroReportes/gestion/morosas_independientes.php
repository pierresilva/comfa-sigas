<?php
	date_default_timezone_set('America/Bogota');
	set_time_limit(0);
	$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
	include_once  $root;
	
	$usuario=$_SESSION['USUARIO'];
	include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
	include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
	include_once $raiz.'/config.php';
	$db = IFXDbManejador::conectarDB();
	if($db->conexionID==null){
		$cadena = $db->error;
		echo msg_error($cadena);
		exit();
	}
	$sql = "DECLARE @periodo_inicial CHAR(6) = '{$_POST['periodo_inicial']}', @periodo_final CHAR(6) = '{$_POST['periodo_final']}', @identificacion VARCHAR(20) = '{$_POST['identificacion']}',@pago CHAR(1) = '{$_POST['pago']}'
			DECLARE @fecha_pago_inicial DATE = '{$_POST['fecha_pago_inicial']}', @fecha_pago_final DATE ='{$_POST['fecha_pago_final']}'
			SELECT a91.detalledefinicion AS tipo_documento
				,a15.identificacion, a15.pnombre+' '+isnull(a15.snombre,'')+' '+a15.papellido + ' '+ isnull(a15.sapellido,'') AS nombre_afiliado
				, a15.direccion, a15.telefono,a15.celular, a15.email
				, (SELECT TOP 1 municipio FROM aportes089 WHERE codmunicipio=a15.idciuresidencia) AS ciudad
				, count(*) AS cantidad_peridos_mora
				, (SELECT periodo+',' 
					FROM aportes250
					WHERE id_trabajador=a250.id_trabajador AND pago='N'
					FOR XML PATH('')
				)AS periodos_activos
				, (SELECT periodo+',' 
					FROM aportes250
					WHERE id_trabajador=a250.id_trabajador AND pago='S'
					FOR XML PATH('')
				)AS periodos_inactivos
				, a251.id_desafiliacion,a251.fecha_sistema AS fecha_desafiliacion
				, a16.idformulario AS id_formulario_activo, b91.detalledefinicion AS tipo_afiliacion_activa, a16.fechaingreso AS fecha_ingreso_activa, a16.estado AS estado_afiliacion_activa
				, c91.detalledefinicion AS tipo_afiliacion_inact, a17.fechaingreso AS fecha_ingreso_inact, a17.estado AS estado_afiliacion_inact
				, a17.idformulario AS id_formulario_inactivo,a17.fecharetiro AS fecha_retiro_inact
				, a16.salario AS salario_activo, a17.salario AS salario_inactivo
				, max(a250.fecha_pago) AS ultima_fecha_pago
				, (
					SELECT sum(valoraporte)
					FROM aportes250 a
						INNER JOIN aportes011 a11 ON a11.idempresa=a.id_trabajador
							AND a11.tipo_aportante IN (4561,4562) AND a11.periodo=a.periodo
					WHERE a.id_trabajador=a250.id_trabajador AND pago='S'
				) AS valor_aporte_recuperado
				, a500.agencia AS agencia_activa
				, b500.agencia AS agencia_inactiva
				,(	SELECT max(fecha_sistema) 
					FROM aportes253 
					WHERE id_trabajador=a250.id_trabajador
						AND id_carta_notificacion=4567
						AND id_estado=4255 -- [4255]NOTIFICACION ENTREGADA A SATISFACCION
				) AS fecha_notif_moros
				,(	SELECT max(fecha_sistema) 
					FROM aportes253 
					WHERE id_trabajador=a250.id_trabajador
						AND id_carta_notificacion=4568
						AND id_estado=4255 -- [4255]NOTIFICACION ENTREGADA A SATISFACCION
				) AS fecha_notif_desaf
			FROM aportes250 a250
				LEFT JOIN aportes015 a15 ON a15.idpersona=a250.id_trabajador
				LEFT JOIN aportes091 a91 ON a91.iddetalledef=a15.idtipodocumento
				LEFT JOIN aportes016 a16 ON a16.idformulario=a250.id_afiliacion
				LEFT JOIN aportes091 b91 ON b91.iddetalledef=a16.tipoafiliacion
				LEFT JOIN aportes500 a500 ON a500.idagencia=a16.idagencia
				LEFT JOIN aportes252 a252 ON a252.id_mora=a250.id_mora
				LEFT JOIN aportes251 a251 ON a251.id_desafiliacion=a252.id_desafiliacion
				LEFT JOIN aportes017 a17 ON a17.idformulario=a251.id_afiliacion_inactiva
				LEFT JOIN aportes091 c91 ON c91.iddetalledef=a17.tipoafiliacion
				LEFT JOIN aportes500 b500 ON b500.idagencia=a17.idagencia
			WHERE (@periodo_inicial = '' OR a250.periodo BETWEEN @periodo_inicial AND @periodo_final)
				AND (@identificacion = '' OR a250.identificacion=@identificacion) 
				AND (@pago='' OR a250.pago=@pago)
				AND (@fecha_pago_inicial = '' OR a250.fecha_pago BETWEEN @fecha_pago_inicial AND @fecha_pago_final)
			GROUP BY a91.detalledefinicion
				, a15.identificacion, a15.pnombre,a15.snombre,a15.papellido,a15.sapellido, a15.direccion, a15.telefono,a15.celular, a15.email
				, a15.idciuresidencia
				, b91.detalledefinicion, a16.fechaingreso, a16.estado, a16.idformulario
				, a250.id_trabajador
				, a251.id_desafiliacion,a251.fecha_sistema
				, c91.detalledefinicion, a17.fechaingreso, a17.estado,a17.fecharetiro, a17.idformulario
				, a16.salario, a17.salario
				, a500.agencia
				, b500.agencia
				";
	$resultado = $db->querySimple($sql);
	
	$path_plano =$ruta_generados.'gestion'.DIRECTORY_SEPARATOR;
	if( !file_exists($path_plano) ){
		mkdir($path_plano, 0777, true);
	}
	$fecha=date('Ymd');
	$archivo='reporte_morosas_independiente_'.$fecha.'.csv';
	$path_plano.=DIRECTORY_SEPARATOR.$archivo;
	$fp=fopen($path_plano,'w');
	
	$encabezado = "NUM;TIPO DOC.;NUMERO IDENTIFICACION;NOMBRE AFILIADO;CIUDAD;DIRECCION;TELEFONO;CORREO ELECTRONICO;AGENCIA;TIPO AFILIACION;FECHA AFILIACION;ESTADO AFILIACION;FECHA 1ER OFICIO;FECHA 2DO OFICIO;FECHA INACTIVACION;No PERIODOS ADEUDADOS;PERIODO ADEUDADOS;PERIODO RECUPERADO;VALOR ADEUDA;VALOR RECUPERADO;SALDO POR RECUPERAR;FECHA PAGO REALIZADO"."\r\n";
	fwrite($fp, $encabezado);
	$cont = 1;
	$indices = 0;
	$cadena = null;
	$bandera = 'w';
	while ( ( $row = $resultado->fetch() ) == true ){	
		$row = array_map("utf8_decode", $row);
		
		if($row['id_formulario_activo']>0){
			$tipoafiliacion = $row['tipo_afiliacion_activa'];
			$fechaAfiliacion = $row['fecha_ingreso_activa'];
			$estadoAfiliacion = $row['estado_afiliacion_activa'];
			$fechaInactivacion = '';
			$agencia = $row['agencia_activa'];
			$valor_deuda = $row['salario_activo'];
		}else{
			$tipoafiliacion = $row['tipo_afiliacion_inact'];
			$fechaAfiliacion = $row['fecha_ingreso_inact'];
			$estadoAfiliacion = $row['estado_afiliacion_inact'];
			$fechaInactivacion = $row['fecha_retiro_inact'];
			$agencia = $row['agencia_inactiva'];
			$valor_deuda = $row['salario_inactivo'];
		}
		$saldo = $valor_deuda-$row['valor_aporte_recuperado'];

		$cadena = $cont.';'.$row['tipo_documento'].';'.$row['identificacion'].';'.$row['nombre_afiliado'].';'.$row['ciudad'].';'.$row['direccion'].';'.($row['telefono'].'/'.$row['celular']).';'.$row['email'].';'.$agencia.';'.$tipoafiliacion.';'.$fechaAfiliacion.';'.$estadoAfiliacion.';'.$row['fecha_notif_moros'].';'.$row['fecha_notif_desaf'].';'.$fechaInactivacion.';'.$row['cantidad_peridos_mora'].';'.$row['periodos_activos'].';'.$row['periodos_inactivos'].';'.$valor_deuda.';'.$row['valor_aporte_recuperado'].';'.$saldo.';'.$row['ultima_fecha_pago']."\r\n";
		fwrite($fp, $cadena);
		$cont++;
	}
	fclose($fp);
	
	$_SESSION['ENLACE']=$path_plano;
	$_SESSION['ARCHIVO']=$archivo;
	echo 1;
?>