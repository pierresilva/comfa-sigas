<?php
date_default_timezone_set('America/Bogota');
set_time_limit(0);
$raiz="";
//ini_set("display_errors",'1');
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}
include_once '../../config.php';
$path_plano =$ruta_generados.'gestion'.DIRECTORY_SEPARATOR;

$fecha=date('Ymd');
$archivo='empresa'.$fecha.'.csv';
$path_plano.=$archivo;

$fp=fopen($path_plano,'w');
$cadena="NIT;VALOR APORTE;DIFERENCIA;NIT;VALOR APORTE NOMINA;MORA\r\n";


$fechaI=$_REQUEST['fechaI'];

$sql = "DECLARE @fecha_pago_i DATE='$fechaI' 
		SELECT a48.nit,sum(a11.valoraporte) AS total FROM aportes011 a11 
		LEFT JOIN aportes048 a48 ON a11.idempresa=a48.idempresa
		WHERE a11.fechapago=@fecha_pago_i 
		GROUP BY a48.nit";

$rs=$db->querySimple($sql);

$total=0;
$valorAporteNomina=0;
$mora=0;

while (($row=$rs->fetch())==true){
	
	$nit = $row["nit"];
	$total = $row["total"];
	
	$sql2 = "DECLARE @fecha_pago_i DATE='$fechaI' 
			SELECT a10.nit,sum(a10.valoraporte) AS valorAporteNomina,(SELECT sum(distinct a31.mora) FROM aportes031 a31 WHERE a31.nit=a10.nit AND a31.fechapago=a10.fechapago GROUP BY a31.fechapago) AS mora
			FROM aportes010 a10 WHERE a10.fechapago=@fecha_pago_i AND a10.nit=$nit AND a10.fechasistema>=@fecha_pago_i GROUP BY a10.nit,a10.fechapago";
	$rs2=$db->querySimple($sql2);
	
	while (($row2=$rs2->fetch())==true){
		
		$nit2 = $row2["nit"];
		$valorAporteNomina = $row2["valorAporteNomina"];
		$mora = $row2["mora"];
		
	}
	
	$diferencia = ($total - $valorAporteNomina) - $mora;
	$cadena .= "$nit;$total;$diferencia;$nit2;$valorAporteNomina;$mora"."\r\n";
	
	$nit=0;
	$total=0;
	$nit2=0;
	$valorAporteNomina=0;
	$mora=0;
	
}

$fp=fopen($path_plano,'a');
fwrite($fp, $cadena);
fclose($fp);

$_SESSION['ENLACE']=$path_plano;
$_SESSION['ARCHIVO']=$archivo;
echo 1;
?>