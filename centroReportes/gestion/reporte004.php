<?php 
/* @autor:	Oscar.
 *
*
*/
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

date_default_timezone_set('America/Bogota');
ini_set("display_errors",'1');

include_once '../../config.php';
include_once '../../rsc/pdo/IFXDbManejador.php';
include_once '../../rsc/pdo/IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

$sql = "SELECT TOP 1 dateadd(day,1,fechalimite) fechaIni FROM aportes012 WHERE procesado='S' ORDER BY periodo DESC";
$rs = $db->querySimple($sql);
$row = $rs->fetch();
$fechaIni = $row["fechaIni"];
$sql="SELECT TOP 1 fechalimite fechaFin FROM aportes012 WHERE procesado='N' ORDER BY periodo ASC";
$rs = $db->querySimple($sql);
$row = $rs->fetch();
$fechaFin = $row["fechaFin"];
?>

<html>
	<head>
	    <meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
	    <title>Comprobaci&oacute;n Previa Cuota Monetaria</title>
	    <link type="text/css" href="../../css/Estilos.css" rel="stylesheet" />	    
	</head>
	<body>
		<form name="form1" id="form1" method="post">
		<table width="95%" border="0" cellspacing="0" cellpadding="0" >
			<tr>
				<td class="cuerpo_iz">&nbsp;</td>
				<td class="cuerpo_ce">
					<br/>
					<table align="center" border="0" width="900" style="border:1px solid #000000;" >
						<tr>
							<td colspan="2" align="center"><h1>Comprobaci&oacute;n Previa Cuota Monetaria</h1></td>
						</tr>
						<tr>
							<td colspan="2" align="center"><h3>COMFAMILIAR HUILA</h3></td>
						</tr>
						<tr>
							<td width="700">Fecha Impresi&oacute;n: <?php echo strftime("%m %d del a�o %Y"); ?></td>
							<td>Usuario: <?php echo $_SESSION['USUARIO'];?></td>
						</tr>                        
						<tr>
							<td colspan="2" >
								<br /><br />
								<table id="tblResultado" style="margin: 0 auto 0 auto;" width="40%">
									<tr>
										<td width="80%"><b>Concepto</b></td>										
										<td width="20%" align="right" ><b>Cantidad</b></td>
									</tr>
									<tr >
										<td>
											Empresas con Aporte y sin Planillas
										</td>
										<td align="right" >
											<?php
												$sql = "SELECT count(DISTINCT a11.idempresa) cant
														FROM aportes011 a11  
														WHERE a11.fechapago BETWEEN '$fechaIni' AND '$fechaFin' 
														  AND 0=isnull((SELECT count(a10.idplanilla) FROM aportes010 a10
														    			WHERE a11.idempresa=a10.idempresa AND a10.fechapago BETWEEN '$fechaIni' AND '$fechaFin'),0)";
												$rs = $db->querySimple($sql);
												$row = $rs->fetch();
												echo number_format($row["cant"]);												
											?>
										</td>
									</tr>									
									<tr >
										<td>
											Empresas Activas
										</td>
										<td align="right" >
											<?php
												$sql = "SELECT count(a48.idempresa) cant
														FROM aportes048 a48  
														WHERE a48.estado='A'";
												$rs = $db->querySimple($sql);
												$row = $rs->fetch();
												echo number_format($row["cant"]);												
											?>
										</td>
									</tr>
									<tr >
										<td>
											Empresas Afiliaci&oacute;n Pendiente
										</td>
										<td align="right" >
											<?php
												$sql = "SELECT count(a48.idempresa) cant
														FROM aportes048 a48  
														WHERE a48.estado='P'";
												$rs = $db->querySimple($sql);
												$row = $rs->fetch();
												echo number_format($row["cant"]);												
											?>
										</td>
									</tr>
									<tr >
										<td>
											Afiliados Activos
										</td>
										<td align="right" >
											<?php
												$sql = "SELECT count(DISTINCT a16.idpersona) cant
														FROM aportes016 a16  
														WHERE a16.estado='A'";
												$rs = $db->querySimple($sql);
												$row = $rs->fetch();
												echo number_format($row["cant"]);												
											?>
										</td>
									</tr>
									<tr >
										<td>
											Afiliados Afiliaci&oacute;n Pendiente
										</td>
										<td align="right" >
											<?php
												$sql = "SELECT count(DISTINCT a16.idpersona) cant
														FROM aportes016 a16  
														WHERE a16.estado='P'
														  AND 0=isnull((SELECT count(DISTINCT b16.idpersona) cant FROM aportes016 b16 WHERE b16.estado='A' AND a16.idpersona=b16.idpersona),0)";
												$rs = $db->querySimple($sql);
												$row = $rs->fetch();
												echo number_format($row["cant"]);												
											?>
										</td>
									</tr>
									<tr >
										<td>
											Afiliados con Pignoraciones Activas
										</td>
										<td align="right" >
											<?php
												$sql = "SELECT count(a43.idtrabajador) cant
														FROM aportes043 a43  
														WHERE a43.estado='A'
														  AND 0<isnull((SELECT count(DISTINCT b16.idpersona) cant FROM aportes016 b16 WHERE b16.idpersona=a43.idtrabajador),0)";
												$rs = $db->querySimple($sql);
												$row = $rs->fetch();
												echo number_format($row["cant"]);												
											?>
										</td>
									</tr>
									<tr >
										<td>
											Afiliados con Embargados
										</td>
										<td align="right" >
											<?php
												$sql = "SELECT count(distinct a18.idtrabajador) cant
														FROM aportes018 a18  
														WHERE a18.estado='A'
														  AND 0<isnull((SELECT count(DISTINCT b16.idpersona) cant FROM aportes016 b16 WHERE b16.idpersona=a18.idtrabajador),0)";
												$rs = $db->querySimple($sql);
												$row = $rs->fetch();
												echo number_format($row["cant"]);												
											?>
										</td>
									</tr>
									<tr >
										<td>
											Defunciones no canceladas
										</td>
										<td align="right" >
											<?php
												$sql = "SELECT count(distinct a19.idtrabajador) cant
														FROM aportes019 a19  
														WHERE a19.cancelado='N'
														  AND 0<isnull((SELECT count(DISTINCT b16.idpersona) cant FROM aportes016 b16 WHERE b16.idpersona=a19.idtrabajador),0)";
												$rs = $db->querySimple($sql);
												$row = $rs->fetch();
												echo number_format($row["cant"]);												
											?>
										</td>
									</tr>
									<tr >
										<td>
											Total personas con Planillas
										</td>
										<td align="right" >
											<?php
												$sql = "SELECT count(DISTINCT a10.identificacion) cant
														FROM aportes010 a10  
														WHERE a10.fechapago BETWEEN '$fechaIni' AND '$fechaFin'";
												$rs = $db->querySimple($sql);
												$row = $rs->fetch();
												echo number_format($row["cant"]);												
											?>
										</td>
									</tr>
									<tr >
										<td>
											Total Empresas con Aportes
										</td>
										<td align="right" >
											<?php
												$sql = "SELECT count(DISTINCT a11.idempresa) cant
														FROM aportes011 a11  
														WHERE a11.fechapago BETWEEN '$fechaIni' AND '$fechaFin'";
												$rs = $db->querySimple($sql);
												$row = $rs->fetch();
												echo number_format($row["cant"]);												
											?>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
					</td>
					<td class="cuerpo_de">&nbsp;</td>
				</tr>

			</table>
		</form>
	</body>
</html>
