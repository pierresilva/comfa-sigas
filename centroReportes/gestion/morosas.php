
<?php
date_default_timezone_set('America/Bogota');
set_time_limit(0);
$raiz="";
//ini_set("display_errors",'1');
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
include_once '../../config.php';

$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

$path_plano =$ruta_generados.'gestion'.DIRECTORY_SEPARATOR;

$fecha=date('Ymd');
$archivo='morosas'.$fecha.'.csv';
$path_plano.=$archivo;

//$fp=fopen($path_plano,'w');
//$cadena="IDEMP;NIT;RAZON SOCIAL;TIPO EMP;AGENCIA;DIRECCION;TEL;CIUDAD;REPRESENTANTE;EMPLE;ESTADO;CONTRATISTA;SALARIOS;MORA;NUMEROTRABAJADORES;PERIODOS;INICIO\r\n";
$cadena = "IDEMPRESA;NIT;RAZON SOCIAL;TIPO AFILIACION;CLASE APORTANTE;"
			."AGENCIA;DIRECCION;TELEFONO;CIUDAD;REPRESENTANTE;"
			."EMPLEADOS;ESTADO;CONTRATISTA;CANTIDAD PERIODOS;INDICADOR;"
			."SALARIOS;MORA;EDAD 30;EDAD 60;EDAD 90;"
			."EDAD 120;EDAD 150;EDAD 180;EDAD MAS DE 180;PERIODOS\r\n";
//fwrite($fp, $cadena);
//fclose($fp);

//$tipoFiltro=$_REQUEST['filtro'];
$periodoI=$_REQUEST['periodoI'];
$periodoF=$_REQUEST['periodoF'];
$nit=$_REQUEST['nit'];

$querySql = "DECLARE @periodo_i CHAR(6) = '$periodoI', @periodo_f CHAR(6) = '$periodoF', @nit VARCHAR(12) = '$nit'
			SELECT DISTINCT 
				a48.idempresa,a48.nit,razonsocial,a48.direccion,a48.telefono
				, a48.estado,a48.contratista
				, a15.pnombre + ' ' + isnull(a15.snombre,'')+ ' ' + a15.papellido + ' ' + isnull(a15.sapellido,'') AS representante
				, isnull((select top 1 a89.municipio from aportes089 a89 where a89.codmunicipio=a48.idciudad),'') as municipio
				, a500.agencia
				--, a97.periodo
				, a91.detalledefinicion AS tipo_afiliacion
				--, b91.codigo AS tipomorosidad
				, c91.detalledefinicion AS clase_aportante
				--Indicador
				, d91.codigo AS indicador
				--, ((CASE WHEN convert(DECIMAL(8,4),d91.codigo)>0 THEN convert(DECIMAL(8,4),d91.codigo) ELSE 4 END)/100) AS indicador
				, (SELECT count(DISTINCT idpersona) FROM aportes016 WHERE idempresa=a48.idempresa) AS numero_empleados
				, (SELECT sum(salario) FROM aportes016 WHERE idempresa=a48.idempresa) AS salario
				--Cantidad de periodos en mora
				,(	SELECT count(periodo)
					FROM aportes097
					WHERE idempresa=a48.idempresa 
						AND pago='N' 
					   	--AND (@periodo_i='' OR periodo BETWEEN @periodo_i AND @periodo_f)
				) AS canti_perio_mora
				--Periodos en mora
				, (	
					SELECT ';'+periodo+''+ (CASE WHEN a91.codigo='MAI' THEN '-X' ELSE '' END)
					FROM aportes097
						LEFT JOIN aportes091 a91 ON a91.iddetalledef=tipomorosidad
					WHERE idempresa=a48.idempresa
						AND pago='N'
						AND (@periodo_i='' OR periodo BETWEEN @periodo_i AND @periodo_f)
					FOR xml path('')
				) AS periodos
				--Bandera para calcular la edad
				, (	SELECT count(periodo)
					FROM aportes097
					WHERE idempresa=a48.idempresa 
						AND pago='N'
				) AS edad
			FROM aportes097 a97
				INNER JOIN aportes048 a48 ON a48.idempresa=a97.idempresa
				INNER JOIN aportes091 a91 ON a91.iddetalledef=a48.idtipoafiliacion
				LEFT JOIN aportes091 c91 ON c91.iddetalledef=a48.claseaportante
				LEFT JOIN aportes091 d91 ON d91.iddetalledef=a48.indicador
				LEFT JOIN aportes015 a15 ON a15.idpersona=a48.idrepresentante
				LEFT JOIN aportes500 a500 ON a500.codigo=a48.seccional
			WHERE a97.pago='N'
				AND (@periodo_i='' OR a97.periodo BETWEEN @periodo_i AND @periodo_f)
				AND (@nit='' OR a48.nit=@nit)
			ORDER BY a48.nit ASC";

$valorMora = 0;
$edad = "";
$rsQuery = $db->querySimple($querySql);
while(($row = $rsQuery->fetch())==true){
	
	$row = array_map("utf8_decode",$row);
	//Valor en mora
	$valorMora = $row["canti_perio_mora"] * $row["salario"] * ($row["indicador"]/100);
	
	//Edad de la cartera
	if($row["edad"]==1){$edad = "X;;;;;;";} 
	else if($row["edad"]==2){$edad = ";X;;;;;";}
	else if($row["edad"]==3){$edad = ";;X;;;;";}
	else if($row["edad"]==4){$edad = ";;;X;;;";}
	else if($row["edad"]==5){$edad = ";;;;X;;";}
	else if($row["edad"]==6){$edad = ";;;;;X;";}
	else if($row["edad"]>=7){$edad = ";;;;;;X";}
	else{$edad = ";;;;;;";}

	$cadena .= "{$row["idempresa"]};{$row["nit"]};{$row["razonsocial"]};{$row["tipo_afiliacion"]};{$row["clase_aportante"]};"
				."{$row["agencia"]};{$row["direccion"]};{$row["telefono"]};{$row["municipio"]};{$row["representante"]};"
				."{$row["numero_empleados"]};{$row["estado"]};{$row["contratista"]};{$row["canti_perio_mora"]};{$row["indicador"]};{$row["salario"]};"
				."$valorMora;$edad{$row["periodos"]}\r\n";
}

$fp=fopen($path_plano,'w');
fwrite($fp, $cadena);
fclose($fp);

$_SESSION['ENLACE']=$path_plano;
$_SESSION['ARCHIVO']=$archivo;
echo 1;
exit();
/**********|||||||O|S|C|A|R|||U|R|I|E|L|||R|O|D|R|I|G|U|E|Z||||||||*********/
/*
//$tipoFiltro=$_REQUEST['filtro'];
$periodoI=$_REQUEST['periodoI'];
$periodoF=$_REQUEST['periodoF'];
$nitEmpresa=$_REQUEST['nit'];

$filtroSql = "";
if($tipoFiltro=="Rango") {
	$filtroSql = "a97.periodo BETWEEN '$periodoI' AND '$periodoF'";
}else if($tipoFiltro=="Empresa") {
	$filtroSql = "a48.nit = '$nitEmpresa'";
}else if($tipoFiltro=="EmpresaRango") {
	$filtroSql = "a48.nit = '$nitEmpresa' AND a97.periodo BETWEEN '$periodoI' AND '$periodoF'";
}

$sql = "SELECT DISTINCT 
			a48.idempresa,a48.nit,razonsocial,a48.direccion,a48.telefono,
			pnombre,snombre,papellido,sapellido,
			CASE (a48.seccional)
				WHEN '01' THEN 'NEIVA'
				WHEN '02' THEN 'GARZON'
				WHEN '03' THEN 'PITALITO'
				WHEN '04' THEN 'LA PLATA'
			END as agencia
			, aportes091.detalledefinicion,a97.periodo
			-------------------------------
			--,(SELECT count(*) FROM aportes016 a16 WHERE a16.idempresa=a48.idempresa)AS trabajadores
			-------------------------------
			,isnull((select top 1 a89.municipio from aportes089 a89 where a89.codmunicipio=a48.idciudad),'') as municipio
			,a48.estado,a48.contratista
			,a91.codigo AS tipomorosidad
		FROM aportes048 a48
			LEFT JOIN aportes015 ON a48.idrepresentante=aportes015.idpersona 
			INNER JOIN aportes097 a97 ON a48.nit=a97.nit
			INNER JOIN aportes091 ON a48.idtipoafiliacion=aportes091.iddetalledef
			LEFT JOIN aportes091 a91 ON a91.iddetalledef=a97.tipomorosidad	
		WHERE $filtroSql
			AND a97.pago='N' AND interrucion='N' AND convenio='N' 
			AND ISNULL(a48.claseaportante,0) <> 2875 
		ORDER BY a48.nit,a97.periodo ASC";

$b=0;
$periodo = "";
$bandera = 0;
$con = 0;
$sal = 0;
$deu = 0;
//$trabajadores = 0;
$rs=$db->querySimple($sql);
while (($row=$rs->fetch())==true){
	
	$deu = 0;
	$ide = $row["idempresa"];
	$nit = $row["nit"];
	$rz = $row["razonsocial"];
	$tipoempresa = $row["detalledefinicion"];
	$dir = $row["direccion"];
	$tel = $row["telefono"];
	$ciu = $row["municipio"];
	$nom=$row['pnombre']." ".$row['snombre']." ".$row['papellido']." ".$row['sapellido'];
	$agencia = $row["agencia"];
	$estado = $row["estado"];
	$contratista = $row["contratista"];
	//------cumple la condicion si la empresa tiene dos periodos-----
	if($b==$row["nit"]){
		
		$periodo .= ";".$row["periodo"].(trim($row["tipomorosidad"])=="MAI"?"-X":"");
		//$trabajadores = $trabajadores+$row["trabajadores"];
		$con++;
	}else{	
	
		if ( $b>0){
			
			$deu = ($sal>0)?$sal * 0.04 * $con:0;
			//$cadena = $cadena.";$deu;$trabajadores;$con;$periodo"."\r\n";
			$cadena = $cadena.";$deu;$con;$periodo"."\r\n";
			$fp=fopen($path_plano,'a');
			fwrite($fp, $cadena);
			fclose($fp);
		}		
		//----------------S A L A R I O----------------
		$sql="select count(*) as cuenta, sum(salario) as total from aportes016 where idempresa=$ide";
		$rs3=$db->querySimple($sql);
		if($rs3){
			
			$row3=$rs3->fetch();
			$te=intval($row3['cuenta']);
			$sal = ($te>0)?$row3['total']:0;
		}else{
			$sal = 0;
		}
		//---------------------------------------------
		$cadena="$ide;$nit;$rz;$tipoempresa;$agencia;$dir;$tel;$ciu;$nom;$te;$estado;$contratista;$sal";
		$periodo = $row["periodo"].(trim($row["tipomorosidad"])=="MAI"?"-X":"");
		//$trabajadores = $row["trabajadores"];
		$con = 1;
	}
	$b=$row["nit"];	
}
$deu = ($sal>0)?$sal * 0.04 * $con:0;
//$cadena = $cadena.";$deu;$trabajadores;$con;$periodo"."\r\n";
$cadena = $cadena.";$deu;$con;$periodo"."\r\n";
$fp=fopen($path_plano,'a');
fwrite($fp, $cadena);
fclose($fp);
	
$_SESSION['ENLACE']=$path_plano;
$_SESSION['ARCHIVO']=$archivo;
echo 1;*/
?>

