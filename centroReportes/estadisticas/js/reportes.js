// JavaScript Document
// funcion para obtener cookie en javascript
	function getCookie(c_name)
	{
		var i,x,y,ARRcookies=document.cookie.split(";");
		for (i=0;i<ARRcookies.length;i++)
		{
  			x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
  			y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
  			x=x.replace(/^\s+|\s+$/g,"");
  			if (x==c_name)
    			{
    				return unescape(y);
    			}// fin if
		}// fin for
  	}// fin getCookie
//funcion para obtener cookie en javascript
	
$(function(){
	
	$("#txtPeriodo").datepicker({
		 dateFormat: 'yymm',
	     changeMonth: true,
	     changeYear: true,
	     showButtonPanel: true,
	     onClose: function(dateText, inst) {
	    	 var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
	         var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
	         $(this).datepicker('setDate', new Date(year, month, 1));
	     }
	});
		
	$("#txtPeriodo").focus(function () {
		$(".ui-datepicker-calendar").hide();
		$("#ui-datepicker-div").position({
			my: "center top",
			at: "center bottom",
			of: $(this)
		});
	}); 
})

function reporte001(){
	var periodo = $("#txtPeriodo").val();
	var seccional = $("#cmbSeccional").val();
	
	$(".ui-state-error").removeClass("ui-state-error").addClass("box1");
	if( !periodo ){
		$("#txtPeriodo").removeClass("box1").addClass("ui-state-error");
		return false;
	}
		
	url="http://"+getCookie("URL_Reportes")+"/estadisticas/rptExcel001.jsp?v0="+periodo+"&v1="+seccional;
	window.open(url,"_NEW");
}

function reporte002(){
	var periodo = $("#txtPeriodo").val();
	var seccional = $("#cmbSeccional").val();
	
	$(".ui-state-error").removeClass("ui-state-error").addClass("box1");
	if( !periodo ){
		$("#txtPeriodo").removeClass("box1").addClass("ui-state-error");
		return false;
	}
		
	url="http://"+getCookie("URL_Reportes")+"/estadisticas/rptExcel002.jsp?v0="+periodo+"&v1="+seccional;
	window.open(url,"_NEW");
}

function reporte003(){
	var periodo = $("#txtPeriodo").val();
	var seccional = $("#cmbSeccional").val();
	
	$(".ui-state-error").removeClass("ui-state-error").addClass("box1");
	if( !periodo ){
		$("#txtPeriodo").removeClass("box1").addClass("ui-state-error");
		return false;
	}
		
	url="http://"+getCookie("URL_Reportes")+"/estadisticas/rptExcel003.jsp?v0="+periodo+"&v1="+seccional;
	window.open(url,"_NEW");
}
	
function reporte004(){
	var periodo = $("#txtPeriodo").val();
	var seccional = $("#cmbSeccional").val();
	
	$(".ui-state-error").removeClass("ui-state-error").addClass("box1");
	if( !periodo ){
		$("#txtPeriodo").removeClass("box1").addClass("ui-state-error");
		return false;
	}
		
	url="http://"+getCookie("URL_Reportes")+"/estadisticas/rptExcel004.jsp?v0="+periodo+"&v1="+seccional;
	window.open(url,"_NEW");
}

function reporte005(){
	var periodo = $("#txtPeriodo").val();
	var seccional = $("#cmbSeccional").val();
	
	$(".ui-state-error").removeClass("ui-state-error").addClass("box1");
	if( !periodo ){
		$("#txtPeriodo").removeClass("box1").addClass("ui-state-error");
		return false;
	}
		
	url="http://"+getCookie("URL_Reportes")+"/estadisticas/rptExcel005.jsp?v0="+periodo+"&v1="+seccional;
	window.open(url,"_NEW");
}

function reporte006(){
	var periodo = $("#txtPeriodo").val();
	var seccional = $("#cmbSeccional").val();
	
	$(".ui-state-error").removeClass("ui-state-error").addClass("box1");
	if( !periodo ){
		$("#txtPeriodo").removeClass("box1").addClass("ui-state-error");
		return false;
	}
		
	url="http://"+getCookie("URL_Reportes")+"/estadisticas/rptExcel006.jsp?v0="+periodo+"&v1="+seccional;
	window.open(url,"_NEW");
}

function reporte007(){
	var periodo = $("#txtPeriodo").val();
	var seccional = $("#cmbSeccional").val();
	
	$(".ui-state-error").removeClass("ui-state-error").addClass("box1");
	if( !periodo ){
		$("#txtPeriodo").removeClass("box1").addClass("ui-state-error");
		return false;
	}
		
	url="http://"+getCookie("URL_Reportes")+"/estadisticas/rptExcel007.jsp?v0="+periodo+"&v1="+seccional;
	window.open(url,"_NEW");
}


