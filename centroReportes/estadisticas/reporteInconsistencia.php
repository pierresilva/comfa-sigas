<?php
	date_default_timezone_set('America/Bogota');
	set_time_limit(0);
	$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
	include_once  $root;
	
	$usuario=$_SESSION['USUARIO'];
	include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
	include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
	include_once $raiz.'/config.php';
	$db = IFXDbManejador::conectarDB();
	if($db->conexionID==null){
		$cadena = $db->error;
		echo msg_error($cadena);
		exit();
	}
	//--INFORMACION DEL PERIODO 
	$sqlPeriodo = "SELECT top 1 smlv FROM APORTES012 WHERE procesado='N' ORDER BY periodo ASC";
	$rsPeriodo = $db->querySimple($sqlPeriodo); 
	$smlv = null;
	if( ( $rowPeriodo = $rsPeriodo->fetchObject() ) == true ) {
		$smlv = $rowPeriodo->smlv;
	}
	$caso = $_REQUEST["caso"]; 	
	$resultado = null;
	switch( $caso ){
		case "TrabaConviMasUnConyu":
			$sql = "SELECT 
						a15.idpersona,a15.identificacion AS identificacion_trabajador
						, (a15.pnombre + ' ' + a15.papellido) AS trabajador
						, count(a21.idbeneficiario) AS cantidad_conyuges
					FROM aportes015 a15
						INNER JOIN aportes021 a21 ON a21.idtrabajador=a15.idpersona
					WHERE a21.conviven='S' AND a21.estado='A' 
					GROUP BY a15.idpersona,a15.identificacion, a15.pnombre, a15.papellido
					HAVING COUNT(*)>1 
			";
			$resultado = $db->querySimple($sql);			
		break;
		case "EmpreSinCiiu":
			$sql = "SELECT distinct a91.detalledefinicion AS tipoafiliacion,a48.nit,a48.razonsocial, a48.contratista AS la_empresa_es_contratista,a48.telefono,a48.direccion,a48.email,a89.municipio as ciudad
					FROM aportes048 a48
						LEFT JOIN aportes079 a79 ON a79.clase=a48.actieconomicadane 
							AND isnull(a79.clase,'')<>'' AND len(a79.clase)>3
						LEFT JOIN aportes091 a91 ON a91.iddetalledef=a48.idtipoafiliacion
						LEFT JOIN aportes089 a89 ON a48.iddepartamento=a89.coddepartamento AND a89.codmunicipio=a48.idciudad
					WHERE a79.idciiu IS NULL AND a48.estado='A'
			";
			$resultado = $db->querySimple($sql);
			break;
		case "FechaNacimNull":
			$sql = "SELECT DISTINCT a15.idpersona,a15.identificacion, a91.detalledefinicion AS tipo_documento,a17.estado AS estado_afiliacion_trabajador
					FROM aportes015 a15
						INNER JOIN aportes017 a17 ON a17.idpersona=a15.idpersona AND a17.estadofidelidad='A'
						LEFT JOIN aportes091 a91 ON a91.iddetalledef=a15.idtipodocumento
					WHERE a15.fechanacimiento IS NULL
					UNION
					SELECT DISTINCT a15.idpersona,a15.identificacion, a91.detalledefinicion AS tipo_documento,'A' AS estado_afiliacion_trabajador
					FROM aportes015 a15
						INNER JOIN aportes016 a16 ON a16.idpersona=a15.idpersona
						LEFT JOIN aportes091 a91 ON a91.iddetalledef=a15.idtipodocumento
					WHERE a15.fechanacimiento IS NULL
					UNION
					SELECT DISTINCT b15.idpersona,b15.identificacion, a91.detalledefinicion AS tipo_documento,'A' AS estado_afiliacion_trabajador
					FROM aportes015 a15
						INNER JOIN aportes016 a16 ON a16.idpersona=a15.idpersona
						INNER JOIN aportes021 a21 ON a21.idtrabajador=a15.idpersona AND a21.idparentesco IN (34,35,36,37,38) 
						INNER JOIN aportes015 b15 ON b15.idpersona=a21.idbeneficiario
						LEFT JOIN aportes091 a91 ON a91.iddetalledef=b15.idtipodocumento
					WHERE b15.fechanacimiento IS NULL
			";
			$resultado = $db->querySimple($sql);
			break;
		case "EmpreSinSecci":
			$sql = "SELECT a48.nit,a48.razonsocial
					FROM aportes048 a48
						LEFT JOIN aportes500 a500 ON a500.codigo=a48.seccional
					WHERE a500.idagencia IS NULL
			";
			$resultado = $db->querySimple($sql);
			break;
		case "RelacBenefHuerf":
			$sql = "SELECT a21.idrelacion,a15.identificacion AS identificacion_trabajador
					FROM aportes021 a21
						INNER JOIN aportes015 a15 ON a15.idpersona=a21.idtrabajador	
					WHERE idbeneficiario=0
					UNION
					SELECT 
						a21.idrelacion,b15.identificacion AS identificacion_trabajador
					FROM aportes017 a17
						INNER JOIN aportes048 a48 ON a48.idempresa=a17.idempresa
							AND a48.estado='A'
						INNER JOIN aportes021 a21 ON a21.idtrabajador=a17.idpersona
							AND a21.idparentesco IN (35,36,37,38)
							AND a21.estado='A'
						LEFT JOIN aportes015 a15 ON a15.idpersona=a21.idbeneficiario
						LEFT JOIN aportes015 b15 ON b15.idpersona=a21.idtrabajador
					WHERE a17.estadofidelidad='A' AND a15.idpersona IS NULL
					UNION
					SELECT a21.idrelacion,b15.identificacion AS identificacion_trabajador
					FROM aportes021 a21
						LEFT JOIN aportes015 a15 ON a15.idpersona=a21.idbeneficiario
						LEFT JOIN aportes015 b15 ON b15.idpersona=a21.idtrabajador
					WHERE a15.idpersona IS NULL
			";
			$resultado = $db->querySimple($sql);
			break;
		case "ConyuDobleConvi":
			$sql = "SELECT a15.identificacion AS identificacion_trabajador, b15.identificacion AS identificacion_conyuge
					FROM aportes021 a21
						INNER JOIN aportes015 a15 ON a15.idpersona=a21.idtrabajador
						INNER JOIN aportes015 b15 ON b15.idpersona=a21.idbeneficiario
						LEFT JOIN aportes016 a16 ON a16.idpersona=a21.idtrabajador
						LEFT JOIN aportes017 a17 ON a17.idpersona=a21.idtrabajador AND a17.estadofidelidad='A'
					WHERE (a17.idformulario IS NOT NULL OR a16.idformulario IS NOT NULL)
						AND a21.idbeneficiario IN (
							SELECT idbeneficiario
							FROM aportes021 
						 	WHERE idparentesco=34 AND conviven='S' AND idbeneficiario>0
							GROUP BY idbeneficiario
							HAVING count(*)>1
						)
						AND a21.idparentesco=34
						AND conviven='S'
					ORDER BY b15.identificacion
			";
			$resultado = $db->querySimple($sql);
			break;
		case "EmpreTipoAfiliIncoh":
			$sql = "
				-- 2653, 'APORTANTE CON 200 O MAS COTIZANTES'
			   	-- 2654, 'APORTANTE CON MENOS DE 200 COTIZANTES'
				-- 2874, 'APORTANTE MIPYME (LEY 590 DE 2000)'
				-- 2875, 'APORTANTE PRIMER EMPLEO (LEY 1429 DE 2010)'
				-- 3316, 'EMPLEADOR'
				SELECT a48.nit,a48.razonsocial,a91.detalledefinicion AS tipoafiliacion, b91.detalledefinicion AS claseaportante
				FROM aportes048 a48
					INNER JOIN aportes091 a91 ON a91.iddetalledef=a48.idtipoafiliacion
					INNER JOIN aportes091 b91 ON b91.iddetalledef=a48.claseaportante
				WHERE idtipoafiliacion<>3316 AND claseaportante IN (2653,2654,2874,2875)
			";
			$resultado = $db->querySimple($sql);
			break;
		case "EmpreDiferEmpleYConTraba":
			$sql = "SELECT a15.identificacion, (a15.pnombre + ' ' + a15.papellido) AS trabajador, a48.nit, a48.razonsocial
					FROM aportes016 a16
						INNER JOIN aportes048 a48 ON a48.idempresa=a16.idempresa
						INNER JOIN aportes015 a15 ON a15.idpersona=a16.idpersona
					WHERE a48.idempresa IN (
						SELECT a48.idempresa
						FROM aportes048 a48
							INNER JOIN aportes016 a16 ON a16.idempresa=a48.idempresa
						WHERE idtipoafiliacion<>3316
						GROUP BY a48.idempresa
						HAVING count(*)>1
					)
					ORDER BY a48.nit
			";
			$resultado = $db->querySimple($sql);
			break;
		case "PersoSinSexo":
			$sql = "SELECT a15.identificacion,a15.sexo, a91.detalledefinicion AS tipo_documento, 'A' AS estado_afiliacion_trabajador
					FROM aportes016 a16
						INNER JOIN aportes015 a15 ON a15.idpersona=a16.idpersona
						LEFT JOIN aportes091 a91 ON a91.iddetalledef=a15.idtipodocumento
					WHERE isnull(a15.sexo,'')=''
					UNION
					SELECT a15.identificacion,a15.sexo, a91.detalledefinicion AS tipo_documento, a17.estado AS estado_afiliacion_trabajador
					FROM aportes017 a17
						INNER JOIN aportes015 a15 ON a15.idpersona=a17.idpersona
						LEFT JOIN aportes091 a91 ON a91.iddetalledef=a15.idtipodocumento
					WHERE a17.estadofidelidad='A' AND isnull(a15.sexo,'')=''
			";
			$resultado = $db->querySimple($sql);
			break;
		case "AfiliacionesHuerfanas":
			$sql = "SELECT a15.identificacion,(a15.pnombre+' '+a15.papellido) AS trabajador, 'A' AS estado_afiliacion
						, a16.idformulario
						, a48.nit,a48.razonsocial
					FROM aportes016 a16 
						LEFT JOIN aportes015 a15 ON a15.idpersona=a16.idpersona
						INNER JOIN aportes048 a48 ON a48.idempresa=a16.idempresa
							AND a48.estado='A' 
							AND a48.idtipoafiliacion=3316
					WHERE a16.tipoafiliacion=18
						AND a16.primaria='S'
						AND a15.idpersona IS NULL
					UNION
					SELECT a15.identificacion,(a15.pnombre+' '+a15.papellido) AS trabajador, 'I' AS estado_afiliacion
						, a17.idformulario
						, a48.nit,a48.razonsocial
					FROM aportes017 a17
						LEFT JOIN aportes015 a15 ON a15.idpersona=a17.idpersona
						INNER JOIN aportes048 a48 ON a48.idempresa=a17.idempresa
					WHERE a17.estadofidelidad='A' AND a15.idpersona IS NULL
			";
			$resultado = $db->querySimple($sql);
			break;
		case "EmpreSinSecto":
			$sql = "SELECT nit,razonsocial, a91.detalledefinicion AS tipo_afiliacion
					FROM aportes048 a48
						LEFT JOIN aportes091 a91 ON a91.iddetalledef=a48.idtipoafiliacion
					WHERE isnull(idsector,0)=0 AND a48.estado='A'		
			";
			$resultado = $db->querySimple($sql);
			break;
		case "TrabaTipoAfiliIncoh":
			$sql = "SELECT DISTINCT a15.identificacion, a48.nit, a48.razonsocial
						, a91.detalledefinicion AS tipo_afiliacion_empresa
						, b91.detalledefinicion AS tipo_afiliacion_trabajador
					FROM aportes048 a48
						INNER JOIN aportes016 a16 ON a16.idempresa=a48.idempresa
						INNER JOIN aportes015 a15 ON a15.idpersona=a16.idpersona
						LEFT JOIN aportes091 a91 ON a91.iddetalledef=a48.idtipoafiliacion
						LEFT JOIN aportes091 b91 ON b91.iddetalledef=a16.tipoafiliacion
					WHERE 
						-- EMPLEADOR
						(a48.idtipoafiliacion=3316 AND a16.tipoafiliacion<>18)
						-- FACULTATIVO
						OR (a48.idtipoafiliacion=3317 AND a16.tipoafiliacion<>19)
						-- PENSIONADO
						OR (a48.idtipoafiliacion=3318 AND a16.tipoafiliacion NOT IN (20,21,4209))
						-- VOLUNTARIO
						OR (a48.idtipoafiliacion=3319 AND a16.tipoafiliacion<>19)
			";
			$resultado = $db->querySimple($sql);
			break;
		case "RelacBenefSinParen":
			$sql = "SELECT
						a21.idrelacion 
						, a15.identificacion AS identificacion_trabajador, (a15.pnombre+' '+a15.papellido) AS trabajador
						, b15.identificacion AS identificacion_beneficiario, (b15.pnombre+' '+b15.papellido) AS beneficiario
					FROM aportes021 a21
						LEFT JOIN aportes015 a15 ON a15.idpersona=a21.idtrabajador
						LEFT JOIN aportes015 b15 ON a15.idpersona=a21.idbeneficiario
					WHERE idparentesco=0
			";
			$resultado = $db->querySimple($sql);
			break;
	}
	
	$path_plano =$ruta_generados.'giro'.DIRECTORY_SEPARATOR.'auditoria'.DIRECTORY_SEPARATOR. 'gestion'.DIRECTORY_SEPARATOR;;
	if( !file_exists($path_plano) ){
		mkdir($path_plano, 0777, true);
	}
	$fecha=date('Ymd');
	$archivo='reporte_'.$caso."_".$fecha.'.csv';
	$path_plano.=DIRECTORY_SEPARATOR.$archivo;
	$cont = 0;
	$indices = 0;
	$cadena = null;
	$bandera = 'w';
	$fp=fopen($path_plano,'w');
	while ( ( $row = $resultado->fetch() ) == true ){	
		if($cont == 0){
			$indices = array_keys($row);
			$cadena = join(";", $indices)."\r\n";
			fwrite($fp, $cadena);
		}
		$row = array_map("utf8_decode", $row);
		$cadena = join(";", $row)."\r\n";
		fwrite($fp, $cadena);
		$cont++;
	}
	fclose($fp);
	
	$_SESSION['ENLACE']=$path_plano;
	$_SESSION['ARCHIVO']=$archivo;
	echo 1;
?>