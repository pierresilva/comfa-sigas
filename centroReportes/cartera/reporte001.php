<?php
//ini_set('display_errors',1);
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'session.php';
session();

include("../../config.php");
include(RUTA_FISICA_DIRECTORIO_CLASES_RSC."ManejoArchivos.php");
include(RUTA_FISICA_DIRECTORIO_CLASES."p.aportes.class.php");
include(RUTA_FISICA_DIRECTORIO_CLASES."p.afiliacion.class.php");
include(DIRECTORIO_RAIZ."aportes/empresas/clases/empresas.class.php");

$archivoDestino = "reporte_afiliados_antiguos_saldo.csv";
$pathDestino = RUTA_FISICA_CENTRO_REPORTES ."cartera/planos/". $archivoDestino;
$objAportes = new Aportes();
$objAfiliacion = new Afiliacion();
$objEmpresas = new Empresa();
ManejoArchivosServidor::prepararArchivoPlano($pathDestino,array('identifiacion',
												   'pri_nombre',
												   'seg_nombre',
												   'pri_apellido',
												   'seg_apellido',
												   'sexo',
												   'direccion_afi',
												   'ciudad_afi',
												   'zona_afi',
												   'telefono_afi',
												   'e_mail',
												   'estado',
												   'nit',
												   'razon_social',
												   'direccion_emp',
												   'tel_empresa'),",");

$resultadoBuscarAfi = $objAportes->buscar_todos_afiados(100);
$cont=0;
if($resultadoBuscarAfi){
	while ($trabajador = mssql_fetch_array($resultadoBuscarAfi)) {
		$registroGrabar = array(
								"identificacion" => $trabajador["identificacion"],
								"pnombre" => $trabajador["pnombre"],
								"snombre" => $trabajador["snombre"],
								"papellido" => $trabajador["papellido"],
								"sapellido" => $trabajador["sapellido"],
								"sexo" => $trabajador["sexo"],
								"direccion_afi" => $trabajador["direccion"],
								"ciudad" => $trabajador["municipio"],
								"zona" => $trabajador["zona"],
								"telefono_afi" => $trabajador["telefono"],
								"email_afi" => $trabajador["email"],
								"estado_afi" => $trabajador["estado"],
								"nit" => "",
								"razonsocial" => "",
								"direccion_empr" => "",
								"telefono_empr" => "");
	
		$resultadoafiliacion = $objAfiliacion->buscar_datos_afiliacion_por_idpersona($trabajador["idpersona"]);
		if($resultadoafiliacion){
			$afiliacion = mssql_fetch_array($resultadoafiliacion);
			if(is_array($afiliacion)){
				$resultadoempresa = $objEmpresas->buscar_empresa_por_idempresa($afiliacion["idempresa"]);
				if($resultadoempresa){
					$empresa = mssql_fetch_array($resultadoempresa);
					if(is_array($empresa)){
						$registroGrabar["nit"] = $empresa["nit"];
						$registroGrabar["razonsocial"] = $empresa["razonsocial"];
						$registroGrabar["direccion_empr"] = $empresa["direccion"];
						$registroGrabar["telefono_empr"] = $empresa["telefono"];
					}
					mssql_free_result($resultadoempresa);
				}
				mssql_free_result($resultadoafiliacion);	
			}else{
				$resultadotrayectoria = $objAfiliacion->obtener_ultima_trayectoria_por_idpersona($trabajador["idpersona"]);
				if($resultadotrayectoria){
					$trayectoria = mssql_fetch_array($resultadotrayectoria);
					if(is_array($trayectoria)){
						$registroGrabar["nit"] = $trayectoria["nit"];
						$registroGrabar["razonsocial"] = $trayectoria["razonsocial"];
						$registroGrabar["direccion"] = $trayectoria["direccion"];
						$registroGrabar["telefono"] = $trayectoria["telefono"];
					}
					mssql_free_result($resultadotrayectoria);
				}
			}
		}else{
			die("error");
		}
		ManejoArchivosServidor::grabarRegistroEnArchivoGenerico($pathDestino, $registroGrabar,",");
		$cont++;
	}
	mssql_free_result($resultadoBuscarAfi);	
}
echo "Fin del reporte.<br> El archivo del reporte puede ser descargado 
	<a href='". URL_PORTAL . DIRECTORIO_PHP_COMUNES ."descargarArchivo.php?archivo=". $archivoDestino ."&componente=cartera'>Aqui</a>.
	<br/>";
?>