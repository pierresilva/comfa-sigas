<?php 
set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$fecha=date('Ymd');
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

//Recibo los parametros del formulario
$ben=$_REQUEST['v1'];
$agencia=$_REQUEST['v2'];

include_once '../../config.php';
$archivo="trabajadores_por_agencia.csv";
$path_plano=$ruta_generados.$archivo;
$f=fopen($path_plano,"w");
fclose($f);

if($ben=='N'){
$cadena="TIPO DOC;IDENTIFICACION;NOMBRES;DIRECCION;TELEFONO;CIUDAD;BARRIO;CATEGORIA;NIT;EMPRESA;CIUDAD;SECCIONAL;DIRECCION;TELEFONO\r\n";
$f=fopen($path_plano,"a");//a = append
fwrite($f,$cadena);
fclose($f);
$sql="select aportes016.idpersona,aportes015.idtipodocumento,aportes091.detalledefinicion as tipodocumento,aportes015.identificacion,aportes015.pnombre,aportes015.snombre,
aportes015.papellido,aportes015.sapellido,aportes015.direccion,aportes015.telefono,aportes015.idciuresidencia,aportes089.municipio as ciudadres,aportes015.idzona,aportes015.idbarrio,aportes087.barrio,aportes016.idempresa,aportes016.primaria,aportes016.estado,aportes016.categoria,aportes048.nit,aportes048.razonsocial,
aportes048.principal,aportes048.estado,aportes048.idciudad,aportes048.idzona,aportes089.municipio as ciudademp,aportes048.seccional,aportes048.direccion as direccionemp,aportes048.telefono as telefonoemp
from aportes016 
inner join aportes015 on aportes016.idpersona=aportes015.idpersona
inner join aportes048 on aportes016.idempresa=aportes048.idempresa 
left join aportes091 on aportes015.idtipodocumento=aportes091.iddetalledef
left join aportes089 on aportes015.idciuresidencia=aportes089.codmunicipio and aportes048.idciudad=aportes089.codmunicipio and aportes015.idzona=aportes089.codzona
left join aportes087 on aportes015.idbarrio=aportes087.idbarrio 
where aportes016.primaria='S' and aportes016.estado='A' and aportes048.estado='A' and aportes048.principal='S' and aportes016.idagencia='$agencia'
order by aportes048.nit asc";
}else{
$cadena="TIPO DOC;IDENTIFICACION;NOMBRES;DIRECCION;TELEFONO;CIUDAD;BARRIO;CATEGORIA;NIT;EMPRESA;CIUDAD;SECCIONAL;DIRECCION;TELEFONO;PARENTESCO;NOMBRE BEN;FECHA NACIMIENTO\r\n";
$f=fopen($path_plano,"a");//a = append
fwrite($f,$cadena);
fclose($f);
$sql = "select aportes016.idpersona,aportes015.idtipodocumento,aportes091.detalledefinicion as tipodocumento,aportes015.identificacion,aportes015.pnombre,aportes015.snombre,
aportes015.papellido,aportes015.sapellido,aportes015.direccion,aportes015.telefono,aportes015.idciuresidencia,aportes089.municipio as ciudadres,aportes015.idzona,aportes015.idbarrio,aportes087.barrio,aportes016.idempresa,aportes016.primaria,aportes016.estado,aportes016.categoria,aportes048.nit,aportes048.razonsocial,
aportes048.principal,aportes048.estado,aportes048.idciudad,aportes048.idzona,aportes089.municipio as ciudademp,aportes048.seccional,aportes048.direccion as direccionemp,aportes048.telefono as telefonoemp,a91b.detalledefinicion as parentesco,aportes021.conviven,a15b.pnombre as pnombreb,a15b.snombre as snombreb ,a15b.papellido as papellidob,a15b.sapellido as sapellidob,a15b.fechanacimiento
from aportes016 
inner join aportes015 on aportes016.idpersona=aportes015.idpersona
inner join aportes021 on aportes021.idtrabajador=aportes015.idpersona AND (aportes021.conviven ='S' OR aportes021.conviven IS NULL)
inner join aportes015 a15b on aportes021.idbeneficiario=a15b.idpersona and aportes021.estado='A'
inner join aportes048 on aportes016.idempresa=aportes048.idempresa 
left join aportes091 on aportes015.idtipodocumento=aportes091.iddetalledef
inner join aportes091 a91b on aportes021.idparentesco=a91b.iddetalledef
left join aportes089 on aportes015.idciuresidencia=aportes089.codmunicipio and aportes048.idciudad=aportes089.codmunicipio and aportes015.idzona=aportes089.codzona
left join aportes087 on aportes015.idbarrio=aportes087.idbarrio 
where aportes016.primaria='S' and aportes016.estado='A' and aportes048.estado='A' and aportes048.principal='S' and aportes016.idagencia='$agencia'
order by aportes015.identificacion asc";
}

$cadena="";
$f=fopen($path_plano,"a");
$rs=$db->querySimple($sql);
$w=$rs->fetch();
while($row = $rs->fetch()){
	$tipoDoc=trim($row['tipodocumento']);
	$identificacion=trim($row['identificacion']);
	$nombre=trim($row['pnombre'])." ".trim($row['snombre'])." ".trim($row['papellido'])." ".trim($row['sapellido']); 
    $direccion=trim($row['direccion']);
	$telefono=trim($row['telefono']);
	$ciudad=trim($row['ciudadres']);
	$barrrio=trim($row['barrio']); 
	$categoria=trim($row['categoria']);
	$nit=trim($row['nit']);
	$rzocial=trim($row['razonsocial']);
	$ciudadEmp=trim($row['ciudademp']);
	switch($row['seccional']){
	  	case "01": $seccional="Neiva";  	break;
	  	case "02": $seccional="Garzon"; 	break;
	  	case "03": $seccional="Pitalito"; 	break;
	  	case "04": $seccional="La plata"; 	break;
	 	}
	$direccionEmp=trim($row['direccionemp']);
	$telefonoEmp=trim($row['telefonoemp']);
	
	if($ben == 'N'){
		$cadena="$tipoDoc;$identificacion;$nombre;$direccion;$telefono;$ciudad;$barrrio;$categoria;$nit;$rzocial;$ciudadEmp;$seccional;$direccionEmp;$telefonoEmp\r\n"; 
		}else{
		$parentesco=trim($row['parentesco']);
		$nombreb=trim($row['pnombreb'])." ".trim($row['snombreb'])." ".trim($row['papellidob'])." ".trim($row['sapellidob']); 
		$fecnace=trim($row['fechanacimiento']);
		$cadena="$tipoDoc;$identificacion;$nombre;$direccion;$telefono;$ciudad;$barrrio;$categoria;$nit;$rzocial;$ciudadEmp;$seccional;$direccionEmp;$telefonoEmp;$parentesco;$nombreb;$fecnace\r\n"; 
		
		}
	//Insertamos los datos en el archivo plano
	fwrite($f,$cadena);
	
	
}//while
fclose($f);	

$enlace=$path_plano;
$_SESSION['ENLACE']=$enlace;
$_SESSION['ARCHIVO']=$archivo;

echo 1;

?>
