
<?php
	date_default_timezone_set('America/Bogota');
	set_time_limit(0);
	$raiz="";
	$ruta_generados = "";
	$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
	include_once  $root;
	include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
	include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
	
	$db = IFXDbManejador::conectarDB();
	if($db->conexionID==null){
		$cadena = $db->error;
		echo msg_error($cadena);
		exit();
	}
	
	include_once $raiz.'/config.php';
	$path_plano =$ruta_generados.'aportes'.DIRECTORY_SEPARATOR.'empresas'.DIRECTORY_SEPARATOR;
	$fecha=date('Ymd');
	$fechaSistema=$fecha;			
	
	$sql="SELECT a48.nit,a48.razonsocial,isnull(a48.sigla,NULL) as sigla,
	(select count(idempresa) from aportes016 b16 where b16.idempresa=a16.idempresa and b16.estado='A') as numero_afiliados,
	isnull(a79.clase,' ') as clase,isnull(a79.descripcion,' ') as descripcion,isnull(c91.detalledefinicion,' ') as sector_empresarial,isnull(b91.detalledefinicion,' ') as tipo_afiliacion,
	isnull(a15.identificacion,' ') as cedula_afiliado,
	a15.pnombre+' '+isnull(a15.snombre,'')+' '+a15.papellido+' '+isnull(a15.sapellido,'') as afiliado,
	isnull(datediff(day,a15.fechanacimiento,getdate())/365.25,NULL) as edad_afiliado,isnull(a15.sexo,' ') as sexo_tra,isnull(a16.categoria,' ') AS categoria,(select isnull(max(b16.categoria),'') from aportes016 b16 where b16.idpersona=a16.idpersona and b16.estado='A') AS categoriaGeneral,isnull(a16.salario,0) AS salario,isnull(a500.agencia,' ') AS agencia,
	isnull(b15.identificacion,' ') as identificacion_beneficiario,
	b15.pnombre+' '+isnull(b15.snombre,'')+' '+b15.papellido+' '+isnull(b15.sapellido,'') as beneficiario,
	isnull(datediff(day,b15.fechanacimiento,getdate())/365.25,NULL) as edad_beneficiario,isnull(b15.sexo,' ') as sexo_ben,
	isnull(a91.detalledefinicion,' ') as parentesco,isnull(a21.giro,' ') AS giro
	FROM
	aportes016 a16
	INNER JOIN aportes015 a15 on a15.idpersona=a16.idpersona
	LEFT JOIN aportes021 a21 on a21.idtrabajador=a16.idpersona and ((a21.idparentesco=34 AND a21.conviven='S') OR (a21.idparentesco IN (35,36,37,38))) and a21.estado='A'
	LEFT JOIN aportes015 b15 on b15.idpersona=a21.idbeneficiario
	LEFT JOIN aportes091 a91 on a91.iddetalledef=a21.idparentesco
	INNER JOIN aportes048 a48 on a48.idempresa=a16.idempresa
	LEFT JOIN aportes091 b91 on b91.iddetalledef=a48.idtipoafiliacion
	LEFT JOIN aportes091 c91 on c91.iddetalledef=a48.idsector
	LEFT JOIN aportes079 a79 on a79.clase=a48.actieconomicadane and a79.clase!='' and len(a79.clase)>='4'
	LEFT JOIN aportes500 a500 on a16.idagencia=a500.codigo
	WHERE a16.estado='A' order by a48.nit,a15.identificacion";
	
	
	$archivo='reporte_'.$fecha.'.xls';
	$path_plano.='planos'.DIRECTORY_SEPARATOR.$archivo;
	$cadena = "nit|razon Social|razon_comercial|numero_afiliados|cod_actividad_economica|actividad_economica|sector_empresarial|tipo_afiliacion|cedula_afiliado|afiliado|edad_afiliado|sexo_afiliado|categoria_afiliacion|categoria_general|salario|agencia_afiliacion|identificacion_beneficiario|beneficiario|edad_beneficiario|sexo_beneficiario|parentesco|giro\r\n";
	
	$cont = 0;
	$rs=$db->querySimple($sql);
	while (($row=$rs->fetch())==true){		
		$cadena .= "$row[nit]|$row[razonsocial]|$row[sigla]|$row[numero_afiliados]|$row[clase]|$row[descripcion]|$row[sector_empresarial]|$row[tipo_afiliacion]|$row[cedula_afiliado]|$row[afiliado]|$row[edad_afiliado]|$row[sexo_tra]|$row[categoria]|$row[categoriaGeneral]|$row[salario]|$row[agencia]|$row[identificacion_beneficiario]|$row[beneficiario]|$row[edad_beneficiario]|$row[sexo_ben]|$row[parentesco]|$row[giro] "."\r\n";
		$cont++;
	}
	
	$fp=fopen($path_plano,'w');
	fwrite($fp, $cadena);
	fclose($fp);
	$_SESSION['ENLACE']=$path_plano;
	$_SESSION['ARCHIVO']=$archivo;
	echo 1;
?>