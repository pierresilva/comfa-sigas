<html>
	<head>
		<link type="text/css" rel="stylesheet" href="../css/css.1.8/jquery-ui-1.8.17.custom.css" />
		<link type="text/css" href="../css/Estilos.css" rel="stylesheet"/>
		<link href="../css/marco.css" rel="stylesheet" type="text/css" />

		<script type="text/javascript" src="../js/1.6/jquery-1.6.2.min.js"></script>
		<script type="text/javascript" src="../js/1.6/jquery-ui-1.8.16.custom.min.js"></script>
		<script type="text/javascript" src="../js/comunes.js"></script>

		<style type="text/css">
		<!--
			.ui-datepicker-year{
				border: 0px;
				background: visibility;
				visibility: 0;
			}
		-->
		</style>
		<script type="text/javascript" language="javascript">
			$(function(){
				$("#periodoI").datepicker({ 
					changeMonth: false,
					changeYear: true
				});

				$( "#periodoI" ).bind("change",function(){
					$("#periodoF").val('');
					$("#periodoF").datepicker({
						minDate: new Date($("#periodoI").val()),
						changeMonth: true,
						changeYear: true
					});
					$("#periodoF").datepicker("option","minDate",new Date($("#periodoI").val()));
			});
			});
		</script>
	</head>
	<body>
		<input type="text" name="periodoI" id="periodoI"/>...<input type="text" name="periodoF" id="periodoF"/>
	</body>
</html>