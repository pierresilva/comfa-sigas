<?php
/* autor:       orlando puentes
 * fecha:       28/09/2010
 * objetivo:    

if ( !isset( $_SESSION ['USUARIO'] ) ) {session_start();}
$raiz=$_SESSION['RAIZ'];
chdir($raiz);
*/
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

?>
<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Menú Reportes Secretaria</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="../../css/estiloReporte.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../js/comunes.js"></script>
</head>
<body>
<center>
  <table width="100%" border="0">
  <tr>
    <td align="center" ><img src="../imagenes/razonSocialRpt.png" width="518" height="91" /></td>
  </tr>
  <tr>
    <td align="center" >&nbsp;</td>
  </tr>
  <tr>
    <td class="fecha" ></td>
  </tr>
</table>

<table width="60%" border="0" cellspacing="1" class="tablero">
<tr><th>Reporte</th>
<th>Listado de Reportes Tarjeta Plata</th></tr>  
      <tr>
      <td scope="row" align="left" width="20%">Reporte001</td>
      <td align="left">&nbsp; 
      </td>
    </tr>
    <tr>
      <td scope="row">Reporte002</td>
      <td>
          <label style="cursor:pointer" onClick="window.open('http://10.10.1.121/sigas/centroReportes/tarjeta/reporte002.php')">Estado Disponibilidad de Bonos</label>
      </td>
    </tr>
	<tr>
      <td scope="row">Reporte003</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td scope="row">Reporte004</td>
      <td>
      <label style="cursor:pointer" onClick="window.open('http://10.10.1.121/sigas/centroReportes/tarjeta/reporte004.php','_blank')">Informe de los Valores Cargados a la Tarjeta Multiservicio</label>
    </td>
    </tr>
    <tr>
      <td scope="row">Reporte005</td>
      <td>Listado de Tarjetas con Destino a una Agencia</td>
    </tr>
    <tr>
      <td scope="row">Reporte006</td>
      <td>Listado de Tarjetas Pendientes de Solicitar</td>
    </tr>
    <tr>
      <td scope="row">Reporte007</td>
      <td><label style="cursor:pointer" onClick="configurar(2)">Listado&nbsp;de Tarjetas por edad sin reclamar - SUBSIDIO</label></td>
    </tr>
    <tr>
      <td scope="row">Reporte008</td>
      <td><label style="cursor:pointer" onClick="configurar(2)">Listado&nbsp;de Tarjetas por edad sin reclamar - SERVICIOS</label></td>
    </tr>
    <tr>
      <td scope="row">Reporte009</td>
      <td>Listado de Tarjetas por código de etapa </td>
    </tr>
    <tr>
      <td scope="row">Reporte010</td><td>
      <label style="cursor:pointer" onClick="window.open('http://10.10.1.121/sigas/centroReportes/tarjeta/reporte010.php','_blank')">
      Listado de Tarjetas Pendientes de Entregar por Empresa - Servicios
      </label></td>
    </tr>
    <tr>
      <td scope="row">Reporte011</td>
      <td>Listado de Tarjetas Enviadas por Correo -Rango fecha</td>
    </tr>
    <tr>
      <td scope="row">Reporte012</td>
      <td>
      <label style="cursor:pointer" onClick="window.open('http://10.10.1.121/sigas/centroReportes/tarjeta/reporte019.php','_blank')">
      Listado de Tarjetas Enviadas a los Asesores sin Devolver
      </label>
      </td>
    </tr>
    <tr>
      <td scope="row">Reporte013</td>
      <td>Listado de Tarjetas sin Reclamar - Subsidio Familiar</td>
    </tr>
    <tr>
      <td scope="row">Reporte014</td>
      <td>Listdo de Tarjetas sin Reclamar - FONEDE</td>
    </tr>
    <tr>
      <td scope="row">&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td scope="row">Reporte019</td>
      <td>
      <label style="cursor:pointer" onClick="window.open('http://10.10.1.121/sigas/centroReportes/tarjeta/reporte019.php','_blank')">
      Listado de Archivos Cargue Diario Tarjeta Procesados
      </label>
      </td>
    </tr>
    <tr>
      <td scope="row">Reporte022</td>
      <td>Relación de Envios de Tarjetas Tipo Servicio con el Asesor o por Correo</td>
    </tr>
    <tr>
      <td scope="row">&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td scope="row">Reporte030</td>
      <td><label style="cursor:pointer" onClick="configurar(3)">Listado Tarjetas Con Saldo</label></td>
    </tr>
  
  </table>
  </center>
  </body>
<script language="javascript">
var URL=src();
function configurar(nu){
	switch (nu){
		case 1 :url=URL+"centroReportes/configurarReporte.php?tipo=1&archivo=tarjeta/reporte001.php";
		break;
        case 2 :url=URL+"centroReportes/configurarReporte.php?tipo=1&archivo=tarjeta/reporte003.php";
		break;
        case 3 : url=URL+"centroReportes/configurarReporte.php?tipo=2&archivo=tarjeta/reporte030.php"
		}
	window.open(url,"mainFrame");
	}
</script>  
</html>
