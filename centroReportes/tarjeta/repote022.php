<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

if ( !isset( $_SESSION ['USUARIO'] ) ) {session_start();}
$usuario=$_SESSION['USUARIO'];$raiz=$_SESSION['RAIZ'];
include_once('clases/rpt.tarjeta.class.php');
chdir($raiz);
include_once("funcionesComunes/funcionesComunes.php");
$objTarjeta=new RptTarjeta();
$consulta = $objTarjeta->archivos_envio_asesor();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Documento sin título</title>
<link href="../../css/estiloReporte.css" rel="stylesheet" type="text/css" />
</head>

<body>
<img src="logo_reporte.png" width="707" height="86" />
<table width="100%" border="0" cellpadding="2">
  <tr>
    <td colspan="2"><div align="center">
      <p>RELACION  DE ENVIOS DE TARJETAS TIPO SERVICIOS  CON EL ASESOR Y POR CORREO A LAS EMPRESAS    </p>
    </div></td>
  </tr>
  <tr>
    <td>Fecha Reporte: <?php echo $fecha = fecha_hora_impresion(); ?></td>
    <td align="right">Usuario: <?php echo $usuario; ?></td>
  </tr>
</table>
<table width="100%" border="0" cellpadding="2" class="tablero">
  <tr>
    <th width="5%" scope="col">Nro</th>
    <th width="12%" scope="col">Fec envio</th>
    <th width="5%" scope="col">Cant</th>
    <th width="4%" scope="col">D&iacute;as</th>
    <th width="8%" scope="col">Tipo</th>
    <th width="8%" scope="col">Suc</th>
    <th width="58%" scope="col">Empresa</th>
  </tr>
  <?php
  $con=0;
  while ($row=mssql_fetch_array($consulta)){
	  $con++;
	  ?>
     
  <tr>
    <td><?php echo $con; ?>&nbsp;</td>
    <td><?php echo $row['fechasistema']; ?>&nbsp;</td>
    <td><?php echo $row['cantidad']; ?>&nbsp;</td>
    <td><?php echo $row['dias']; ?>&nbsp;</td>
    <td><?php echo $row['tipo']; ?>&nbsp;</td>
    <td><?php echo $row['codigosucursal']; ?>&nbsp;</td>
    <td><label style="cursor:pointer" onclick="buscarEnvio();"> <?php echo $row['razonsocial']; ?></label>&nbsp;</td>
  </tr>
  
  <?php
  }
  ?>
</table>
<p>&nbsp;</p>
</body>
</html>