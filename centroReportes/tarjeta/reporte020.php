<?php
/* autor:       orlando puentes
 * fecha:       21/09/2010
 * objetivo:     
 */
 $root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once("../../config.php");
$class = new Java("java.lang.Class");
$rgIfx = $class->forName("com.informix.jdbc.IfxDriver");
$rgIfx->newInstance();
$dMang = new Java("java.sql.DriverManager");
$conn = $dMang->getConnection($conexion_ifx);
$compileManager = new Java("net.sf.jasperreports.engine.JasperCompileManager");
$rp = realpath("reporte020.jrxml");

$report = $compileManager->compileReport($rp);
$fillManager = new Java("net.sf.jasperreports.engine.JasperFillManager");
$parametro = new Java("java.util.HashMap");

$jasperPrint = $fillManager->fillReport($report, $parametro, $conn);
$exportManager = new Java("net.sf.jasperreports.engine.JasperExportManager");
$outputPath = realpath(".")."/"."reporte020.pdf";
$exportManager->exportReportfirstdfFile($jasperPrint, $outputPath);

if (file_exists("reporte020.pdf")){
header("Content-disposition: attachment; filename=reporte020.pdf");
header("Content-Type: application/pdf");
header("Content-Transfer-Encoding: binary");
header("Content-Length: ". @filesize("reporte020.pdf"));
header("Pragma: no-cache");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Expires: 0");
set_time_limit(0);
@readfile("reporte020.pdf") or die("problem occurs.");
}

?>
