<?php
/* autor:       orlando puentes
 * fecha:       08/10/2010
 * objetivo:     
 */
 $root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

if ( !isset( $_SESSION ['USUARIO'] ) ) {session_start();}
$usuario=$_SESSION['USUARIO'];
$raiz=$_SESSION['RAIZ'];
include_once("config.php");
$fini= $_REQUEST['v0'];
$ffin=$_REQUEST['v2'];

$class = new Java("java.lang.Class");
$rgIfx = $class->forName("com.informix.jdbc.IfxDriver");
$rgIfx->newInstance();

// PARA LAS FECHAS!!! 
 $fec = mktime(0,0,0,$fini[0],$fini[1],$fini[2])*1000;
 $fechaI= new Java("java.util.Date", $fec);
 $fec = mktime(0,0,0,$ffin[0],$ffin[1],$ffin[2])*1000;
 $fechaF= new Java("java.util.Date", $fec);

//$idsolicitud = new Java("java.lang.Long", $ids);

$dMang = new Java("java.sql.DriverManager");
$conn = $dMang->getConnection($conexion_ifx);
$compileManager = new Java("net.sf.jasperreports.engine.JasperCompileManager");
$rp = realpath("reporte030.jrxml");

$report = $compileManager->compileReport($rp);
$fillManager = new Java("net.sf.jasperreports.engine.JasperFillManager");
$parametro = new Java("java.util.HashMap");

$parametro->put("fini",$fechaI);
$parametro->put("ffin",$fechaF);
//$emptyDataSource = new Java("net.sf.jasperreports.engine.JREmptyDataSource");
$jasperPrint = $fillManager->fillReport($report, $parametro, $conn);
$exportManager = new Java("net.sf.jasperreports.engine.JasperExportManager");
$outputPath = realpath(".")."/"."reporte030.pdf";
$exportManager->exportReportfirstdfFile($jasperPrint, $outputPath);

if (file_exists("reporte030.pdf")){
header("Content-disposition: attachment; filename=reporte030.pdf");
header("Content-Type: application/pdf");
header("Content-Transfer-Encoding: binary");
header("Content-Length: ". @filesize("reporte030.pdf"));
header("Pragma: no-cache");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Expires: 0");
set_time_limit(0);
@readfile("reporte030.pdf") or die("problem occurs.");
}

?>
