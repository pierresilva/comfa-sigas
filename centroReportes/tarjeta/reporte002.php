<?php
/* autor:       orlando puentes
 * fecha:       21/09/2010
 * objetivo:     
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
 
 if ( !isset( $_SESSION ['USUARIO'] ) ) {session_start();}
$raiz=$_SESSION['RAIZ'];
chdir($raiz);
include("centroReportes/tarjeta/clases/rpt.tarjeta.class.php");
$objClase=new RptTarjeta;
$consulta=$objClase->contar_bonos();
$row=mssql_fetch_array($consulta);
$totalB=$row['cuenta'];
$consulta=$objClase->contar_bonos_asignados();
$row=mssql_fetch_array($consulta);
$totalBA=$row['cuenta'];
$consulta=$objClase->contar_bonos_NO_asignados();
$row=mssql_fetch_array($consulta);
$totalBNA=$row['cuenta'];
$consulta=$objClase->contar_bonos_activos();
$row=mssql_fetch_array($consulta);
$totalBAct=$row['cuenta'];
$consulta=$objClase->contar_bonos_inactivos();
$row=mssql_fetch_array($consulta);
$totalBInac=$row['cuenta'];

$fechaHoy = date("m/d/Y h:i A");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Reportes</title>
<link href="../../css/estiloReporte.css" rel="stylesheet" type="text/css" />

</head>

<body>
<table width="100%" border="0">
  <tr>
    <td ><div align="left"><img src="logo_comfamiliar.jpg" width="190" height="79" />  <div align="center"><img src="logo_reporte.png" width="362" height="70" /></div></div></td>
  </tr>
  <tr>
    <td align="center" ><strong>ESTADO DISPONIBLIDAD DE BONOS</strong></td>
  </tr>
  <tr>
    <td class="fecha" >Fecha impresi&oacute;n: <?php echo $fechaHoy; ?></td>
  </tr>
</table>
<BR />
<center>
<table width="50%" border="0" class="tablero2">
  <tr>
      <th scope="row" width="80%">N&uacute;mero de bonos recibidos</th>
    <td style="text-align: right"><?php echo number_format($totalB); ?></td>
  </tr>
  <tr>
    <th scope="row">N&uacute;mero de bonos asignados</th>
    <td style="text-align: right"><?php echo number_format($totalBA); ?></td>
  </tr>
  <tr>
    <th scope="row">N&uacute;mero de bonos por asignar</th>
    <td style="text-align: right"><?php echo number_format($totalBNA); ?></td>
  </tr>
  <tr>
    <th scope="row">Bonos activos</th>
    <td style="text-align: right"><?php echo number_format($totalBAct); ?></td>
  </tr>
  <tr>
    <th scope="row">Bonos inactivos</th>
    <td style="text-align: right"><?php echo number_format($totalBInac); ?></td>
  </tr>
</table>
</center>
</body>
</html>