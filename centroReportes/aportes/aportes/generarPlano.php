<?php
date_default_timezone_set('America/Bogota');
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';

include_once $raiz.'/config.php';
$path_plano =$ruta_generados.'aportes'.DIRECTORY_SEPARATOR.'empresas'.DIRECTORY_SEPARATOR.'planos'.DIRECTORY_SEPARATOR;

$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

$fechaI = $_REQUEST["fechaI"];
$fechaF = $_REQUEST["fechaF"];
$instituto = $_REQUEST["instituto"];

$sql = "SELECT a48.nit,'000' AS codsuc,a48.idciudad,a79.clase,a11.fechapago,a11.periodo,a48.trabajadores,a11.$instituto AS valor 
		FROM aportes048 a48
			INNER JOIN aportes011 a11 ON a48.idempresa=a11.idempresa
			INNER JOIN aportes079 a79 ON a48.idcodigoactividad=a79.idciiu
		WHERE a11.fechapago BETWEEN '$fechaI' AND '$fechaF' AND $instituto>0";
$fecha=date('Ymd');
$archivo='reporte_planos_aportes_'.$fecha.'.txt';
$path_plano .= $archivo;
$fp=fopen($path_plano,'w');
$rs=$db->querySimple($sql);

$cadena= "nit		codsuc	ciudad	codact	fechapago	periodo	trab	$instituto\r\n";
$tab = "";
while ( ( $row=$rs->fetch() ) == true ){
	$tab = "	"; 
	if( strlen($row["nit"]) < 8 )
		$tab .= $tab;
	$cadena .= "$row[nit]".$tab."$row[codsuc]	$row[idciudad]	$row[clase]	$row[fechapago]	$row[periodo]	$row[trabajadores]	$row[valor]\r\n";
}

$fp=fopen($path_plano,'a');
fwrite($fp, $cadena);
fclose($fp);

$_SESSION['ENLACE']=$path_plano;
$_SESSION['ARCHIVO']=$archivo;
echo  1;
?>