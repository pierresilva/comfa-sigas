<?php
	$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
	include_once  $root;
	
	$usuario=$_SESSION['USUARIO'];
	include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
	include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
	include_once $raiz.'/config.php';
	$db = IFXDbManejador::conectarDB();
	if($db->conexionID==null){
		$cadena = $db->error;
		echo msg_error($cadena);
		exit();
	}
	$titulo = "Informes de inconsistencias en la cuota monetaria";
?>

<html>
	<head>
		<title>Men&uacute; Reportes</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link type="text/css" rel="stylesheet" href="<?php echo URL_PORTAL; ?>css/css.1.8/jquery-ui-1.8.17.custom.css" />
		<link type="text/css" href="<?php echo URL_PORTAL; ?>css/Estilos.css" rel="stylesheet"/>
		<link href="<?php echo URL_PORTAL; ?>css/marco.css" rel="stylesheet" type="text/css" />

		<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/1.6/jquery-1.6.2.min.js"></script>
		<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/1.6/jquery-ui-1.8.16.custom.min.js"></script>
		<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/comunes.js"></script>
		<script type="text/javascript" src="js/reporteInconsistencia.js"></script>
	</head>
	<body style="text-align: center;">
		<table width="100%" border="0">
			<tr>
				<td align="center" ><img src="<?php echo URL_PORTAL; ?>/imagenes/logo_reporte.png" width="362" height="70"></td>
			</tr>
			<tr>
				<td align="center" ><?php echo $titulo ?></td>
			</tr>
		</table>
		<div id="tipo1" style="display:block">
			<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
				<tr>
			    	<th colspan="4"><strong>Parametros de Configuracion Reporte</strong></th>
			    </tr>
    			<tr>
      				<td style="text-align: center;" >
      					Informe:
      					<select id="cmbInforme" name="cmbInforme" class="box1" onchange="controlChange();">
      						<option value="0">Seleccione...</option>
      						<optgroup label="Antes del proceso">
      							<option value="FechaAsignacion">Beneficiarios sin fecha asignaci&oacute;n.</option>
      							<option value="ConyugeDuplicado">Conyuge duplicado.</option>
      							<option value="TrabajadorConyugeEl">Trabajador esta como conyuge de &eacute;l.</option>
      							<option value="ConyugeErroneo">Conyuge errado.</option>
      						</optgroup> 
      						<optgroup label="Despues del proceso">
					        	<option value="TrabaTope4">Afiliado junto con la mujer hacen tope de 4 pero no de 6 smlv.</option>
					        	<option value="Tope4Convivencia">Tope de 4 por convivencia</option>
					        	<option value="Tope4Conyuge">Tope de 4 por conyuge</option>
					        	<option value="Tope6Convivencia">Tope de 6 por convivencia</option>	
					        	<option value="Hijo19Annos">Hijos mayores de 19 a&ntilde;os</option>
					        	<option value="Tope4ConviNull">Tope 4 del trabajador y convivencia null</option>
					        	<option value="CuotaYaAsignada">Cuota ya asignada</option>
					        	<option value="ValorAporte0">Valor aportes $0</option>	
					        	<option value="PUSinAfiliacion">Con planilla unica y sin afiliacion</option>
					        	<option value="Pignoracion">Informe Pignoracion</option>
					        	<option value="CuotaDoble">Cuotas Dobles</option>
					        	<option value="PagoPeriodoAntes">Empresas pagan el aportes anticipadamente.</option>
					        	<option value="BeneSinCertiEscolUnive">Beneficiarios sin certificado de escolaridad</option>
					        	<option value="EmpresasLey1429">Empresas ley 1429</option>
					        	<option value="Tope6Smlv">Tope de 6 smlv con pago de periodos anteriores a la fecha de afiliacion.</option>
					        	<option value="NovedadMaternidad">Personas que tiene novedad de maternidad.</option>
					        	<option value="EmpreNoPagaAportIndic">Empresas no pagan el aportes indicado para el trabajador.</option>
					        	<option value="PagosDosEmpresasMenos12Dias">Pagos con dos empresas con menos de 12 dias </option>
					        	<option value="RegistrosConGiroEinactivos">Afiliados con giro e inactivos </option>
					        	<option value="RegistrosDosPgosPuStc">Afiliados con 2 pagos PU y novedad STC </option>
					        	<option value="RegistrosValoresMayoresA">Valor girado mayor o igual a: </option>
					        	<option value="RegistrosValoresDiferentes"> Trabajadores que en subsidio el periodo anterior tenga valor diferente al que tiene en revision </option>
					        </optgroup>
      					</select>
     				</td>
    			</tr>
    			<tr class="clsCampos tr0">
			    	<td>&nbsp;</td>
			    </tr>
			    <!-- CAMPOS INFORME TrabaTope4 -->
    			<tr style="display: none;" class="clsCampos trTrabaTope4">
      				<td style="text-align:center; height: 30px;">
      					Periodo:
      					<input type="text" name="txtPeriodoA" id="txtPeriodoA" readonly="readonly" class="ui-state-default clsPeriodo"/>
      				</td>
    			</tr>
    			
    			<!-- CAMPOS INFORME trRegistrosValoresMayoresA -->
    			<tr style="display: none;" class="clsCampos trRegistrosValoresMayoresA">
      				<td style="text-align:center; height: 30px;">
      					Valor:
      					<input type="text" name="txtValor" id="txtValor" />
      				</td>
    			</tr>
    			
    			<!-- CAMPOS INFORME ConyugeErroneo -->
    			<tr style="display: none;" class="clsCampos trConyugeErroneo">
      				<td style="text-align:center; height: 30px;">
      					Nombres:
      					<select name="cmbNombres" id="cmbNombres" class="box1">
      						<option value="S">Si</option>
      						<option value="N">No</option>
      					</select>
      					Apellidos:
      					<select name="cmbApellidos" id="cmbApellidos" class="box1">
      						<option value="S">Si</option>
      						<option value="N">No</option>
      					</select>
      				</td>
    			</tr>
    			<!-- CAMPOS INFORME EmpresaSinGiro 
    			<tr style="display: none;" class="clsCampos trEmpresaSinGiro">
      				<td style="text-align:center; height: 30px;">
      					Periodo:
      					<input type="text" name="txtPeriodoB" id="txtPeriodoB" readonly="readonly" class="ui-state-default clsPeriodo"/>
      				</td>
    			</tr> -->
    			<!-- CAMPOS INFORME PagoPeriodoAntes -->
    			<tr style="display: none;" class="clsCampos trPagoPeriodoAntes">
      				<td style="text-align:center; height: 30px;">
      					Periodo:
      					<input type="text" name="txtPeriodoC" id="txtPeriodoC" readonly="readonly" class="ui-state-default clsPeriodo"/>
      				</td>
    			</tr>
    			<!-- Descripcion -->
    			<tr style="height: 30px;" >
      				<td style="text-align: center; ">
      					<p style="text-align:center; font-size: 12px; font-weight: bold;">Descripci&oacute;n</p>
      					<hr width="10%"/>
      					<p style="text-align:justify; font-size: 12px; width: 80%; margin: 0px auto;" align="center" id="pDescripcion"></p>
      					<br/>
      				</td>
    			</tr>
    			<!-- BOTTON -->
    			<tr >
    	  			<td colspan="4" style="text-align:center;height: 40px; cursor: pointer;" id="tdBoton"><img onclick="generarPlano();" src="<?php echo URL_PORTAL; ?>imagenes/procesar2.png"></td>
    			</tr>
    		</table>
    	</div>
	</body>
</html>