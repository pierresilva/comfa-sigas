<?php
	date_default_timezone_set('America/Bogota');
	set_time_limit(0);
	$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
	include_once  $root;
	
	$usuario=$_SESSION['USUARIO'];
	include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
	include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
	include_once $raiz.'/config.php';
	$db = IFXDbManejador::conectarDB();
	if($db->conexionID==null){
		$cadena = $db->error;
		echo msg_error($cadena);
		exit();
	}
	//--INFORMACION DEL PERIODO 
	$sqlPeriodo = "SELECT top 1 smlv FROM APORTES012 WHERE procesado='N' ORDER BY periodo ASC";
	$rsPeriodo = $db->querySimple($sqlPeriodo); 
	$smlv = null;
	if( ( $rowPeriodo = $rsPeriodo->fetchObject() ) == true ) {
		$smlv = $rowPeriodo->smlv;
	}
	$caso = $_REQUEST["caso"]; 	
	$resultado = null;
	switch( $caso ){
		case "TrabaTope4":
			$periodoA = $_REQUEST["periodoA"]; //Periodo anterior al actual
			$periodoB = $_REQUEST["periodoB"]; //Periodo actual
			$sql = "DECLARE @periodoA VARCHAR(6), @periodoB VARCHAR(6) = '$periodoB', @fechaI VARCHAR(10), @fechaF VARCHAR(10), @salario INT
					SET @periodoA = '$periodoA' 
					SELECT @fechaI = DATEADD(DAY,1, (min(fechalimite) ) ), @fechaF = max(fechalimite), @salario = max(topesalarial) FROM aportes012 WHERE periodo IN (@periodoA, @periodoB)
					SELECT
					    a15.identificacion,a15.pnombre,a15.snombre,a15.papellido,a15.sapellido
					    ,convert(numeric,sum(a10.salariobasico)) AS salariobasico,convert(numeric,sum(a10.ingresobase)) AS ingresobase
					    ,convert(numeric,sum(b10.salariobasico)) AS salariobasicoc,convert(numeric,sum(b10.ingresobase)) AS ingresobasec
					    ,convert(numeric,(sum(a10.salariobasico) + sum(b10.salariobasico)))AS total
					FROM aportes010 a10
					    INNER JOIN aportes015 a15 ON a15.idpersona=a10.idtrabajador AND a15.sexo='M'
					    INNER JOIN aportes021 a21 ON a21.idtrabajador=a15.idpersona AND a21.idparentesco=34 AND a21.conviven='S'
					    INNER JOIN aportes010 b10 ON b10.idtrabajador=a21.idbeneficiario 
					        AND b10.fechapago BETWEEN @fechaI AND @fechaF AND b10.periodo = @periodoB
					    --LEFT JOIN aportes016 a16 ON a16.idpersona=a21.idbeneficiario AND a16.fechaingreso<@fechaF
					    --LEFT JOIN aportes017 a17 ON a16.idformulario IS NULL AND a17.idpersona=a21.idbeneficiario 
					     --   AND a17.fecharetiro BETWEEN @fechaI AND @fechaF
					WHERE a10.fechapago BETWEEN @fechaI AND @fechaF AND a10.periodo = @periodoB
					   -- AND ( a16.idformulario IS NOT NULL OR a17.idformulario IS NOT NULL)
					    --AND 0 = (SELECT COUNT(*) FROM aportes010 b10 WHERE b10.idtrabajador=a21.idbeneficiario  AND b10.periodo = @periodoB AND b10.fechapago  BETWEEN @fechaI AND @fechaF )
					    AND 0 < (SELECT COUNT(*) FROM aportes021 b21 WHERE b21.idtrabajador=a10.idtrabajador AND b21.idparentesco IN (35,37,38) AND b21.estado='A')
					    AND 0 = (SELECT COUNT(DISTINCT idtrabajador ) FROM aportes009 a9 WHERE a9.periodo=@periodoB AND a9.idtrabajador=a21.idbeneficiario )
					    AND 0 = (SELECT COUNT(DISTINCT idtrabajador ) FROM aportes014 a14 WHERE a14.periodo=@periodoB AND a14.idtrabajador=a21.idbeneficiario )
    					AND 0 = (SELECT COUNT(DISTINCT idpersona ) FROM aportes099 a99 WHERE a99.periodo=@periodoB AND a99.idpersona=a21.idbeneficiario )
					GROUP BY a15.identificacion,a15.pnombre,a15.snombre,a15.papellido,a15.sapellido
					HAVING (sum(a10.salariobasico) + sum(b10.salariobasico))>@salario AND ( sum(a10.salariobasico) +sum(b10.salariobasico) ) < (@salario/4*6) 
			";
			$resultado = $db->querySimple($sql);			
		break;
		case "EmpresaSinGiro":
			$periodoA = $_REQUEST["periodoA"]; //Periodo anterior al actual
			$periodoB = $_REQUEST["periodoB"]; //Periodo actual
			$periodoC = $_REQUEST["periodoC"]; //Periodo enero anno actual 
			$sql = "DECLARE @periodoA VARCHAR(6) = '$periodoA', @periodoB VARCHAR(6) = '$periodoB', @fechaI VARCHAR(10), @fechaF VARCHAR(10)
					SELECT @fechaI = DATEADD(DAY,1, (min(fechalimite) ) ), @fechaF = max(fechalimite) FROM aportes012 WHERE periodo IN (@periodoA, @periodoB)
					
					SELECT distinct a11.idempresa,a48.nit,a48.razonsocial
					FROM aportes011 a11
					INNER JOIN aportes048 a48 ON a48.idempresa=a11.idempresa AND a48.claseaportante<>2655
					WHERE a11.periodo=@periodoB AND a11.fechapago BETWEEN @fechaI AND @fechaF
						AND 0=(SELECT COUNT(DISTINCT idempresa) FROM aportes009 a9 WHERE a9.periodo=a11.periodo AND a9.idempresa=a11.idempresa)
					    AND 0=(SELECT COUNT(DISTINCT idempresa) FROM aportes099 a99 WHERE a99.periodo=a11.periodo AND a99.idempresa=a11.idempresa)
     					AND 0=(SELECT COUNT(DISTINCT idempresa) FROM aportes014 a14 WHERE a14.periodo=a11.periodo AND a14.idempresa=a11.idempresa)
					    AND 
					        (0<(SELECT COUNT(DISTINCT idtrabajador) FROM aportes009 b9 WHERE b9.periodo BETWEEN '$periodoC' AND @periodoA AND b9.idempresa=a11.idempresa)
					         OR 0<(SELECT COUNT(DISTINCT idtrabajador) FROM aportes014 b14 WHERE b14.periodo BETWEEN '$periodoC' AND @periodoA AND b14.idempresa=a11.idempresa)
					         OR 0<(SELECT COUNT(DISTINCT idpersona) FROM aportes099 b99 WHERE b99.periodo BETWEEN '$periodoC' AND @periodoA AND b99.idempresa=a11.idempresa)        
					        )
					     AND 0<(SELECT COUNT(*) FROM aportes016 a16 WHERE a16.idempresa=a11.idempresa) 
					     AND a48.idtipoafiliacion=3316
					";
			$resultado = $db->querySimple($sql);
		break;
		case "Tope4Convivencia": //VALIDAR TOPE DE 4 POR CONVIVENCIA
			$sql = "--========================
					--TOPES POR CONVIVENCIA
					--========================
					SELECT idregistro,idpersona,NUMERO,idbeneficiario,periodo,idconyuge,genero,salario,salarioconyuge
					FROM aportes099 a99
					WHERE flag1='S' AND convivencia = 'S' AND genero='F' AND idparentesco IN (35,38) 
							aND (smlvpa + smlvpc)>4 
					AND(
						0<(SELECT COUNT(*) FROM aportes099 WHERE tipoformulario = 48 AND idpersona = a99.idconyuge AND idbeneficiario = a99.idbeneficiario AND periodo = a99.periodo)
						OR
						0<(SELECT COUNT(*) FROM aportes014 WHERE idtrabajador = a99.idconyuge AND idbeneficiario = a99.idbeneficiario AND periodo = a99.periodo AND anulado='N')
						OR 
						0<(SELECT COUNT(*) FROM aportes009 WHERE idtrabajador = a99.idconyuge AND idbeneficiario = a99.idbeneficiario AND periodo = a99.periodo AND anulado='N')
					)";
			$resultado = $db->querySimple($sql);
		break;
		case "Tope6Convivencia": //VALIDAR TOPE DE 6 POR CONVIVENCIA
			$sql = "SELECT * FROM aportes099  WHERE flag1='S' AND convivencia='S' and (salario+salarioconyuge) >".($smlv*6);
			$resultado = $db->querySimple($sql);
		break;
		case "Hijo19Annos":      //--VALIDAR HIJOS MAYOES DE 19 A�OS 
			$sql = "SELECT * FROM aportes099 WHERE edadbene>19 AND idparentesco=35 AND flag1 ='S' AND capacidadt='N'";
			$resultado = $db->querySimple($sql);
		break;
		case "Tope4ConviNull":   //--VALIDAR TOPE DE 4 DEL TRABAJADOR Y LA CONVIVENCIA SE ENCUENTRA NULL
			$sql = "SELECT * FROM aportes099 WHERE (salario+salarioconyuge) >".($smlv*4)." AND flag1='S' AND convivencia IS NULL";
			$resultado = $db->querySimple($sql);
			break;
		case "Tope4Conyuge":
			$sql = "SELECT 
						a15.identificacion AS 'identificacion afiliado', a15.pnombre+' '+isnull(a15.snombre,'')+' '+a15.papellido+' '+isnull(a15.sapellido,'') AS afiliado
						,b15.identificacion AS 'identificacion conyuge', b15.pnombre+' '+isnull(b15.snombre,'')+' '+b15.papellido+' '+isnull(b15.sapellido,'') AS conyuge
					FROM aportes099 a99
						
						INNER JOIN aportes099 b99 ON b99.idbeneficiario=a99.idbeneficiario AND b99.idpersona=a99.idconyuge 
							AND a99.periodo=b99.periodo
						INNER JOIN aportes015 a15 ON a15.idpersona=a99.idpersona
						INNER JOIN aportes015 b15 ON b15.idpersona=b99.idpersona
					WHERE (a99.smlvpa+a99.smlvpc)>4 AND a99.flag1='S' AND b99.flag1='S'";
			$resultado = $db->querySimple($sql);
			break;
		case "CuotaYaAsignada":  //--VALIDAR SI SE ENCUENTRA CUOTAS YA ASIGNADAS
			$sql = "SELECT aportes099.idregistro,aportes099.flag1 FROM aportes099 
					INNER JOIN aportes009 ON aportes099.idpersona=aportes009.idtrabajador AND aportes099.idbeneficiario=aportes009.idbeneficiario AND aportes099.periodo=aportes009.periodo
					WHERE aportes099.flag1='S'";
			$resultado = $db->querySimple($sql);
		break;
		case "ValorAporte0":  //--VALIDACION DE 0 APORTES EN PLANILLA
			$sql = "--============================
					--PU SIN APORTES Y SIN NOVEDAD
					--============================
					SELECT a99.idregistro,a99.nit,a99.idpersona,a99.numero,a99.periodo,a99.idbeneficiario,a99.planilla,a10.valoraporte, flag2
					FROM aportes099 a99
					INNER JOIN aportes010 a10 ON a10.idtrabajador=a99.idpersona AND a10.planilla=a99.planilla AND a10.periodo=a99.periodo
							AND a10.idempresa=a99.idempresa
					WHERE a99.flag1='S' AND a10.valoraporte=0
						AND LTRIM(RTRIM(a99.inc_tem_emfermedad)) IS NULL AND a99.nov_retiro IS NULL 
						AND a99.nov_maternidad IS NULL AND  a99.nov_vacaciones IS NULL AND a99.sus_tem_contrato IS NULL
					ORDER BY a99.idpersona";
			$resultado = $db->querySimple($sql);
		break;
		case "PUSinAfiliacion":  //--VALIDACION TIENE PAGO EN PLANILLA PERO NO TIENEN LEGALIZADA LA AFILIACION O NO TIENEN AFILIACION ACTIVA
			$sql = "SELECT A9.*
					FROM aportes010 a10 
					INNER JOIN aportes099 a9 ON a9.idpersona=a10.idtrabajador AND a9.periodo=a10.periodo AND a9.flag1='S'
					WHERE isnumeric(a10.periodo)=1 AND
						0 = isnull(( SELECT COUNT(b10.idplanilla) FROM aportes010 b10 WHERE b10.idtrabajador = a10.idtrabajador AND b10.periodo = a10.periodo AND b10.idempresa <> a10.idempresa ), 0)
						AND 
						0 = isnull(( SELECT COUNT(a16.idformulario) FROM aportes016 a16 WHERE a16.idpersona=a10.idtrabajador AND a16.idempresa=a10.idempresa AND a16.estado='A' AND convert(VARCHAR(6),a16.fechaingreso, 112) <= a10.periodo ), 0)
						AND 
						0 = isnull(( SELECT COUNT(a16.idformulario) FROM aportes017 a16 WHERE a16.idpersona=a10.idtrabajador AND a16.idempresa=a10.idempresa AND convert(VARCHAR(6),a16.fechaingreso, 112) <= a10.periodo AND convert(VARCHAR(6),a16.fecharetiro, 112) >= a10.periodo ), 0)	
					--GROUP BY a10.idtrabajador,a10.idempresa,a10.identificacion,a10.nit,a10.periodo,a9.flag1";
			$resultado = $db->querySimple($sql);
		break;
		case "Pignoracion":  //--INFORME PIGNORACION 
			$sql = "SELECT DISTINCT A15.idpersona,A15.identificacion,A43.idpignoracion,a43.valorpignorado,a43.saldo,a99.flag2 AS tipoGiro
					FROM aportes043 a43
					INNER JOIN aportes099 a99 ON a43.idtrabajador=a99.idpersona
					INNER JOIN aportes015 A15 ON A99.idpersona=A15.idpersona
					WHERE a43.estado='A' AND a43.saldo >0 AND a99.flag1='S'";
			$resultado = $db->querySimple($sql);
		break;
		case "CuotaDoble":  //--INFORME PIGNORACION
			$sql = "SELECT idpersona,idbeneficiario,periodo FROM aportes099 GROUP BY idpersona,idbeneficiario,periodo HAVING count(*) > 1";
			$resultado = $db->querySimple($sql);
		break;
		case "FechaAsignacion": //--INFORME DE BENEFICIARIOS SIN FECHA ASIGNACION
			$sql = "SELECT a15.identificacion AS identificacionAfiliado, a15.pnombre + ' ' + isnull(a15.snombre,'') + ' ' + a15.papellido + ' ' + isnull(a15.sapellido,'') AS afiliado 
						, b15.identificacion AS identificacionBeneficiario, b15.pnombre + ' ' + isnull(b15.snombre,'') + ' ' + b15.papellido + ' ' + isnull(b15.sapellido,'') AS beneficiario
					FROM aportes021 a21
						INNER JOIN aportes015 a15 ON a15.idpersona=a21.idtrabajador
						INNER JOIN aportes015 b15 ON b15.idpersona=a21.idbeneficiario
						INNER JOIN aportes016 a16 ON a16.idpersona=a21.idtrabajador
					WHERE fechaasignacion IS NULL AND a21.estado='A' AND giro='S' 
						AND 0<(SELECT COUNT(idtrabajador) FROM aportes009 WHERE idbeneficiario=a21.idbeneficiario)";
			$resultado = $db->querySimple($sql);
		break;
		case "PagoPeriodoAntes":
			$periodo = $_REQUEST["periodo"]; //Periodo actual
			$sql = "DECLARE @periodo VARCHAR(6) = '$periodo', @fechaI VARCHAR(10)
					SELECT TOP 1 @fechaI = fechalimite FROM aportes012 WHERE periodo < @periodo ORDER BY periodo desc
					SELECT distinct a11.idempresa,a48.nit,a48.razonsocial, a11.periodo
					FROM aportes011 a11
						INNER JOIN aportes048 a48 ON a48.idempresa=a11.idempresa AND a48.claseaportante<>2655
					WHERE a11.periodo=@periodo 
						 AND a11.fechapago <= @fechaI
					     AND 0=(SELECT COUNT(DISTINCT idempresa) FROM aportes009 a9 WHERE a9.periodo=a11.periodo AND a9.idempresa=a11.idempresa)
					     AND 0=(SELECT COUNT(DISTINCT idempresa) FROM aportes099 a99 WHERE a99.periodo=a11.periodo AND a99.idempresa=a11.idempresa)
					     AND 0=(SELECT COUNT(DISTINCT idempresa) FROM aportes014 a14 WHERE a14.periodo=a11.periodo AND a14.idempresa=a11.idempresa)
					     AND 
					        (0<(SELECT COUNT(DISTINCT idtrabajador) FROM aportes009 b9 WHERE b9.periodo < @periodo AND b9.idempresa=a11.idempresa)
					         OR 0<(SELECT COUNT(DISTINCT idtrabajador) FROM aportes014 b14 WHERE b14.periodo < @periodo AND b14.idempresa=a11.idempresa)
					         OR 0<(SELECT COUNT(DISTINCT idpersona) FROM aportes099 b99 WHERE b99.periodo < @periodo AND b99.idempresa=a11.idempresa)        
					        )
					     AND 0<(SELECT COUNT(*) FROM aportes016 a16 WHERE a16.idempresa=a11.idempresa) 
					     AND a48.idtipoafiliacion=3316 ";
			$resultado = $db->querySimple($sql);
		break;
		case "BeneSinCertiEscolUnive":
			$sql = "SELECT a99.idregistro,a99.periodo,a99.nit,a99.idpersona,a99.idbeneficiario,a99.capacidadt,a99.edadbene
					FROM aportes099 a99
					INNER JOIN aportes015 a15 ON a15.idpersona=a99.idbeneficiario
					INNER JOIN aportes012 a12 ON a12.periodo=a99.periodo
					WHERE 
						a99.flag1='S'
						AND a99.capacidadt='N'
						AND
							( ( DATEDIFF(DAY, a15.fechanacimiento,a12.fechafin)/365.25 ) BETWEEN 12 AND 19 )
						AND 
						0=(
							SELECT count(*) FROM aportes026 WHERE idbeneficiario=a99.idbeneficiario 
								AND periodoinicio <= a99.periodo AND periodofinal >= a99.periodo AND idtipocertificado IN (55,56)
						)";
			$resultado = $db->querySimple($sql);
		break;
		case "EmpresasLey1429":
			$sql = "SELECT idregistro,nit,periodo,idpersona,numero,idbeneficiario, flag1,causal, 'APORTANTE PRIMER EMPLEO (LEY 1429 DE 2010)' AS claseAportante
					FROM aportes099 WHERE claseaportante=2875
					UNION
					SELECT idregistro,a99.nit,a99.periodo,idpersona,numero,idbeneficiario,flag1,causal, a91.detalledefinicion AS claseAportante
					FROM aportes099 a99
					INNER JOIN aportes010 a10 ON a10.planilla=a99.planilla AND a10.periodo=a99.periodo AND a10.idtrabajador=a99.idpersona
					AND a10.idempresa=a99.idempresa
					LEFT JOIN aportes091 a91 ON a91.iddetalledef=a99.claseaportante
					WHERE (a10.ingresobase*0.02)>(a10.valoraporte-100) AND a99.flag1='S' AND claseaportante<>2875";
			$resultado = $db->querySimple($sql);
		break;
		case "Tope6Smlv":
			$sql = "SELECT a99.nit,a99.periodo,a99.idpersona,a99.numero,a99.idbeneficiario,a99.salario
					FROM aportes099 a99
					INNER JOIN aportes012 a12 ON a12.periodo=a99.periodo
					WHERE a99.flag1='S'
							AND a99.smlvpa<7
							AND
							6<( (SELECT sum(CASE 
											WHEN (ingreso = 'X' AND diascotizados < 30) THEN ingresobase
											ELSE salariobasico 
										END ) 
								FROM aportes010 a10
								WHERE idtrabajador=a99.idpersona AND periodo=a99.periodo 
							)/a12.smlv)";
			$resultado = $db->querySimple($sql);
		break;
		case "ConyugeDuplicado":
			$sql = "SELECT idtrabajador
						, a15.identificacion AS 'identificacion afiliado', a15.pnombre+' '+isnull(a15.snombre,'')+' '+a15.papellido+' '+isnull(a15.sapellido,'') AS afiliado
						, a21.idbeneficiario AS idconyuge
						, b15.identificacion AS 'identificacion conyuge', b15.pnombre+' '+isnull(b15.snombre,'')+' '+b15.papellido+' '+isnull(b15.sapellido,'') AS conyuge
					FROM aportes021 a21 
						INNER JOIN aportes015 a15 ON a15.idpersona=a21.idtrabajador
						INNER JOIN aportes015 b15 ON b15.idpersona=a21.idbeneficiario
					WHERE idparentesco=34 
						AND 0<(SELECT COUNT(*) FROM aportes016 WHERE idpersona=a21.idtrabajador AND estado='A')
						AND 0<(SELECT COUNT(*) FROM aportes021 WHERE idtrabajador=a21.idtrabajador AND estado='A' AND giro='S')
					GROUP BY idtrabajador, a21.idbeneficiario
						, a15.identificacion , a15.pnombre,a15.snombre,a15.papellido,a15.sapellido
						, b15.identificacion ,b15.pnombre,b15.snombre,b15.papellido,b15.sapellido
					HAVING count(*)>1";
			$resultado = $db->querySimple($sql);
			break;
		case "TrabajadorConyugeEl":
			$sql = "SELECT DISTINCT a15.identificacion,a15.pnombre+' '+isnull(a15.snombre,'')+' '+a15.papellido+' '+isnull(a15.sapellido,'') AS afiliado
					FROM aportes021 a21
						INNER JOIN aportes015 a15 ON a15.idpersona=a21.idtrabajador AND a21.idparentesco=34
							AND 0<(SELECT COUNT(*) FROM aportes016 WHERE idpersona=a15.idpersona AND estado='A')
							AND 0<(SELECT COUNT(*) FROM aportes021 WHERE idtrabajador=a15.idpersona AND estado='A' AND giro='S')
						INNER JOIN aportes015 c15 ON c15.idpersona=a21.idbeneficiario
						INNER JOIN aportes015 b15 ON b15.idpersona<>c15.idpersona AND 
							(
								(b15.pnombre=c15.pnombre 
									AND b15.snombre=c15.snombre
									AND b15.papellido=c15.papellido
									AND b15.sapellido=c15.sapellido))
						INNER JOIN aportes021 b21 ON b21.idbeneficiario=a21.idbeneficiario AND b21.idparentesco=34
						INNER JOIN aportes015 d15 ON d15.idpersona=b21.idconyuge
					WHERE d15.pnombre=a15.pnombre 
						AND d15.snombre=a15.snombre
						AND d15.papellido=a15.papellido
						AND d15.sapellido=a15.sapellido";
			$resultado = $db->querySimple($sql);
			break;
		case "ConyugeErroneo":
			$queryFiltro = ($_REQUEST["banNombres"]=="S")?" AND b15.pnombre=c15.pnombre AND b15.snombre=c15.snombre ":"";
			$queryFiltro .= ($_REQUEST["banApellidos"]=="S")?" AND b15.papellido=c15.papellido AND b15.sapellido=c15.sapellido ":"";
			$sql = "SELECT DISTINCT
						a15.identificacion AS 'identificacion afiliado', a15.pnombre+' '+isnull(a15.snombre,'')+' '+a15.papellido+' '+isnull(a15.sapellido,'') AS afiliado
						,c15.identificacion AS 'identificacion conyuge Errada', c15.pnombre+' '+isnull(c15.snombre,'')+' '+c15.papellido+' '+isnull(c15.sapellido,'') AS 'conyuge errada'
						,b15.identificacion AS 'identificacion conyuge Posible', b15.pnombre+' '+isnull(b15.snombre,'')+' '+b15.papellido+' '+isnull(b15.sapellido,'') AS 'conyuge posible'
					FROM aportes021 a21
						INNER JOIN aportes015 c15 ON c15.idpersona=a21.idbeneficiario
						INNER JOIN aportes015 a15 ON a15.idpersona=a21.idtrabajador AND a21.idparentesco=34
							AND 0<(SELECT COUNT(*) FROM aportes016 WHERE idpersona=a15.idpersona AND estado='A')
							AND 0<(SELECT COUNT(*) FROM aportes021 WHERE idtrabajador=a15.idpersona AND estado='A' AND giro='S')	
						INNER JOIN aportes015 b15 ON b15.idpersona<>c15.idpersona
							--AND b15.pnombre=c15.pnombre 
							--AND b15.snombre=c15.snombre
							--AND b15.papellido=c15.papellido
							--AND b15.sapellido=c15.sapellido
							$queryFiltro
							AND (0<(SELECT COUNT(*) FROM aportes016 WHERE idpersona=b15.idpersona AND estado='A')
								OR 
								0<(SELECT COUNT(*) FROM aportes017 WHERE idpersona=b15.idpersona AND estado='A')
							)
						INNER JOIN aportes021 c21 ON c21.idtrabajador=b15.idpersona AND c21.idparentesco=34
					WHERE c21.idbeneficiario=a15.idpersona AND a21.conviven='S'";
			$resultado = $db->querySimple($sql);
			break;
		case "NovedadMaternidad":
			$sql = "SELECT DISTINCT a15.identificacion,a15.pnombre+' '+a15.papellido AS trabajador
						,a99.periodo,a99.cuota,a99.flag1 AS giro,a91.detalledefinicion AS causal
					FROM aportes099 a99
						INNER JOIN aportes015 a15 ON a15.idpersona=a99.idpersona
						LEFT JOIN aportes091 a91 ON a91.iddetalledef=a99.causal
					WHERE causal=2895 AND nov_maternidad='X'";
			$resultado = $db->querySimple($sql);
			break;
		case "EmpreNoPagaAportIndic":
			$sql = "SELECT 
						a48.nit,a48.razonsocial,a91.detalledefinicion AS clase_aportante,
						count(DISTINCT a99.idpersona) AS cantidad_trabajadores
					FROM aportes099 a99
						INNER JOIN aportes048 a48 ON a48.idempresa=a99.idempresa
						INNER JOIN aportes091 a91 ON a91.iddetalledef=a48.claseaportante
					WHERE 
						(valoraporte) < ([giros].[fn_010_Calcular_Aporte](salario_pu, diascotizados, porcentaje_aporte))
						-- AND (valoraporte) < ([giros].[fn_010_Calcular_Aporte](salario_pu, 12, porcentaje_aporte))
					GROUP BY a48.nit,a48.razonsocial,a91.detalledefinicion";
			$resultado = $db->querySimple($sql);
			break;
			
			case "PagosDosEmpresasMenos12Dias":  //--VALIDA EL PAGO DE DOS EMPRESAS Y MENOS DE 12 DIAS
				$sql = "SELECT a99.* FROM aportes099 a99 
				INNER JOIN aportes010 a10 ON a99.idpersona=a10.idtrabajador AND a99.idempresa=a10.idempresa AND a99.periodo=a10.periodo
				WHERE a99.flag1='S' AND a10.ingresobase <=((((SELECT TOP 1 smlv FROM aportes012 WHERE procesado='S' ORDER BY periodo DESC)/2)/30)*12)
				AND a10.valoraporte=0 AND a10.inc_tem_emfermedad<>'X' AND 
				a99.inc_tem_acc_trabajo<1 AND a99.nov_vacaciones<>'X' AND a99.nov_maternidad<>'X' AND
				(SELECT count(DISTINCT b10.idempresa) FROM aportes010 b10 WHERE a99.idpersona=b10.idtrabajador AND a99.periodo=b10.periodo)>=2";
				$resultado = $db->querySimple($sql);
				break;
			
				case "RegistrosConGiroEinactivos":  //--VALIDA LOS GIROS CON AFILIADOS INACTIVOS
					$sql = "SELECT a99.*,(SELECT max(a17.fecharetiro) FROM aportes017 a17 WHERE a17.idpersona=a99.idpersona AND 0=(SELECT count(*) FROM aportes016 a16 WHERE a16.idpersona=a17.idpersona)) AS fechaRetiro
					FROM aportes099 a99 WHERE a99.flag1='S' AND a99.estado16='I' AND 
(SELECT max(a17.fecharetiro) FROM aportes017 a17 WHERE a17.idpersona=a99.idpersona AND 0=(SELECT count(*) FROM aportes016 a16 WHERE a16.idpersona=a17.idpersona)) <>''";
					$resultado = $db->querySimple($sql);
					break;
					
				
				case "RegistrosDosPgosPuStc":  //--VALIDA LOS GIROS CON DOS PAGOS DE PU Y UNA NOVEDAD DE STC
					$sql = "SELECT a99.*,(SELECT sum(b10.diascotizados) FROM aportes010 b10 WHERE b10.idtrabajador=a99.idpersona AND b10.periodo=a99.periodo) totalDias FROM aportes099 a99 WHERE a99.flag1='S'
					AND (SELECT count(*) FROM aportes010 a10 WHERE a10.idtrabajador=a99.idpersona AND a10.periodo=a99.periodo)>1 
					AND (SELECT count(*) FROM aportes010 a10 WHERE a10.idtrabajador=a99.idpersona AND a10.periodo=a99.periodo AND a10.sus_tem_contrato='X')>=1
				";
					$resultado = $db->querySimple($sql);
					break;
					
					case "RegistrosValoresMayoresA": //--VALIDAD REGISTROS CON VALORES MAYORES A
						$txtValor = $_REQUEST["txtValor"]; //Periodo anterior al actual
						
						$sql = "SELECT * FROM aportes099 WHERE flag1='S' AND numero IN (
							SELECT identificacion FROM aportes099
							INNER JOIN aportes048 ON aportes099.idempresa=aportes048.idempresa
							INNER JOIN aportes015 ON aportes099.idpersona= aportes015.idpersona
							where flag1='S'
							GROUP BY identificacion,pnombre,papellido,aportes099.nit,razonsocial
							HAVING sum(cuota)>=$txtValor)
						";
						$resultado = $db->querySimple($sql);
						break;
			
					case "RegistrosValoresDiferentes":  //--VALIDA LOS GIROS CON DOS PAGOS DE PU Y UNA NOVEDAD DE STC
							$sql = "SELECT a99.* FROM aportes099 a99 
						INNER JOIN aportes009 b9 ON a99.idpersona=b9.idtrabajador AND a99.idbeneficiario=b9.idbeneficiario AND a99.periodo=b9.periodo
						WHERE a99.flag1='S'
						AND 1=(SELECT count(*) FROM aportes009 a9 WHERE a9.periodo=(SELECT TOP 1 periodo FROM aportes012 WHERE procesado='S' ORDER BY periodo DESC) AND a9.idtrabajador=a99.idpersona AND a9.idbeneficiario=a99.idbeneficiario AND a9.valor<>a99.valorcuota)
						AND (SELECT count(*) FROM aportes009 a9 WHERE a9.periodo=(SELECT TOP 1 periodo FROM aportes012 WHERE procesado='S' ORDER BY periodo DESC) AND a9.idtrabajador=a99.idpersona AND a9.idbeneficiario=a99.idbeneficiario)>=1
							";
							$resultado = $db->querySimple($sql);
							break;
			
			
	}
	
	$path_plano =$ruta_generados.'giro'.DIRECTORY_SEPARATOR.'auditoria'.DIRECTORY_SEPARATOR. 'gestion'.DIRECTORY_SEPARATOR;;
	if( !file_exists($path_plano) ){
		mkdir($path_plano, 0777, true);
	}
	$fecha=date('Ymd');
	$archivo='reporte_'.$caso."_".$fecha.'.csv';
	$path_plano.=DIRECTORY_SEPARATOR.$archivo;
	$cont = 0;
	$indices = 0;
	$cadena = null;
	$bandera = 'w';
	$fp=fopen($path_plano,'w');
	while ( ( $row = $resultado->fetch() ) == true ){	
		if($cont == 0){
			$indices = array_keys($row);
			$cadena = join(";", $indices)."\r\n";
			fwrite($fp, $cadena);
		}
		$row = array_map("utf8_decode", $row);
		$cadena = join(";", $row)."\r\n";
		fwrite($fp, $cadena);
		$cont++;
	}
	fclose($fp);
	
	$_SESSION['ENLACE']=$path_plano;
	$_SESSION['ARCHIVO']=$archivo;
	echo 1;
?>