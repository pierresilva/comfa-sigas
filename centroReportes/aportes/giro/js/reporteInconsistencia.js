var objDescripcion = {
		FechaAsignacion: "Informe de beneficiarios sin fecha de asignacion y con derecho del pago de la cuota monetaria.",
		ConyugeDuplicado: "Informe de conyuges duplicados",
		TrabajadorConyugeEl: "Informe de trabajadores que estan como conyuge de \u00e9l.",
		ConyugeErroneo: "Informe de posibles conyuges errados.",
		TrabaTope4: "Trabajadores hombres que hagan tope por convivencia de 4 y no de 6, pero que la mujer no haya obtenido el subsidio monetario.",
		EmpresaSinGiro: "Empresas con pagos de aportes anteriores en la fecha de la cuota monetaria que corresponde al periodo seleccionado, pero no fue cancelado el subsidio familiar a los empleados.",
		Tope4Convivencia: "Tope de 4 smlv por convivencia y con beneficiarios hijos e hijastros",
		Tope6Convivencia: "Tope de 6 smlv por convivencia",
		Hijo19Annos: "Hijos mayores de 19 a&ntilde;os y sin discapacidad",
		Tope4ConviNull: "Tope 4 del trabajador y convivencia <span style='color:blue;'>null</span>",
		Tope4Conyuge: "Informe de posibles topes por convivencia, debido a la inconcistencia de la conyuge.",
		CuotaYaAsignada: "Validar Cuota ya asignada",
		ValorAporte0: "Valor aportes $0, sin novedades y con cuota monetaria",
		PUSinAfiliacion: "Tiene pago en planilla pero no tiene legalizada la afiliacion o no tiene afiliacion activa",
		Pignoracion: "Informe de afiliados con pago de la cuota monetaria y tiene pignoracion activa",
		CuotaDoble: "Cuotas dobles en la cuota monetaria de revision",
		PagoPeriodoAntes: "Empresas pagan anticipadamente los aportes parafiscales y no pago la cuota monetaria a sus afiliados.",
		BeneSinCertiEscolUnive: "Informe beneficiarios mayores de 12 a&ntilde;os con capacidad de trabajo N sin certificado de escolaridad y con giro.",
		EmpresasLey1429: "Informe de la empresas correspondientes a la ley 1429 con cuota monetaria en revision.",
		Tope6Smlv: "Tope de 6 smlv por pagos anteriores a la fecha de ingreso de la afiliacion.",
		NovedadMaternidad: "Este reporte se hace con el fin de verificar las maternidades especiales. Ejemplo el ni&ntilde;o nace antes del tiempo (9 meses) estipulado.",
		EmpreNoPagaAportIndic: "El reporte se hace con el fin de notificar las empresas para que cancelen el aportes indicado.",
		PagosDosEmpresasMenos12Dias: "El reporte es para determinar los giros por periodos de dos empresas y menos de 12 dias y sin novedad de vacaciones, maternidad, incapacidad temporal de emfermedad y incapacidad temporal acc de trabajo ",
		RegistrosConGiroEinactivos: "El reporte permite visualizar los registros con giro e inactivos",
		RegistrosDosPgosPuStc: "El reporte permite visualizar los registros con dos pagos en PU y con una novedad de STC",
		RegistrosValoresMayoresA: "El reporte permite visualizar los registros de los valores mayores a",
		RegistrosValoresDiferentes: "Trabajadores que en subsidio el periodo anterior tenga valor diferente al que tiene en revision"
};
$(document).ready(function(){
	$(".clsPeriodo").datepicker({
        dateFormat: 'yymm',
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        onClose: function(dateText, inst) {
        	var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
            $(this).datepicker('setDate', new Date(year, month, 1));
            }
	});
	$(".clsPeriodo").focus(function () {
		$(".ui-datepicker-calendar").hide();
		$("#ui-datepicker-div").position({
			my: "center top",
			at: "center bottom",
			of: $(this)
		});
	}); 
});

function controlChange(){
	var option = $("#cmbInforme").val();
	var idTr = ".tr" + option;
	$(".clsCampos").hide();
	$("input").val("");
	$(idTr).show();
	$("#pDescripcion").html("").html(objDescripcion[option]);
}

function generarPlano(){
	var caso = $("#cmbInforme").val(),
		datos = false;
	$(".ui-state-error").removeClass("ui-state-error");
	if( caso == 0 ){$("#cmbInforme").addClass("ui-state-error");return false;}
	switch( caso ){
		case "TrabaTope4":
			var periodoB = $("#txtPeriodoA").val(),
				periodoA = null;
			if( periodoB == 0 ){$("#txtPeriodoA").addClass("ui-state-error");return false;}
			periodoA = periodoAnterior(periodoB);
			var data = {
					caso:caso,
					periodoA:periodoA,
					periodoB:periodoB
				};
			datos = enviarPeticion(data);
			break;
		case "EmpresaSinGiro":
			var periodoB = $("#txtPeriodoB").val(),
				periodoA = null,
				periodoC = null;
			if( periodoB == 0 ){$("#txtPeriodoB").addClass("ui-state-error");return false;}
			periodoA = periodoAnterior(periodoB);
			periodoC = periodoB.substring(0,4) + "01";
			var data = {
					caso:caso,
					periodoA:periodoA,
					periodoB:periodoB,
					periodoC:periodoC
				};
			datos = enviarPeticion(data);
			break;
		case "PagoPeriodoAntes":
			var periodo = $("#txtPeriodoC").val();
			if( periodo == 0 ){$("#txtPeriodoC").addClass("ui-state-error");return false;}
			var data = {
					caso:caso,
					periodo:periodo
				};
			datos = enviarPeticion(data);
			break;
		case "ConyugeErroneo":
			var banNombres = $("#cmbNombres").val();
			var banApellidos = $("#cmbApellidos").val();
			var data = {
					caso:caso,
					banNombres:banNombres,
					banApellidos:banApellidos
				};
			datos = enviarPeticion(data);
			break;
			
		case "RegistrosValoresMayoresA":
			var txtValor = $("#txtValor").val();
		var data = {
				caso:caso,
				txtValor:txtValor
			};
		datos = enviarPeticion(data);
			break;
			
		case "Tope4Convivencia": case "Tope6Convivencia": case "Hijo19Annos": case "Tope4ConviNull": case "CuotaYaAsignada":
		case "ValorAporte0": case "PUSinAfiliacion": case "Pignoracion": case "CuotaDoble": case "FechaAsignacion": 
		case "BeneSinCertiEscolUnive": case "EmpresasLey1429": case "Tope6Smlv": case "ConyugeDuplicado":
		case "TrabajadorConyugeEl": case "ConyugeErroneo": case "Tope4Conyuge": case "NovedadMaternidad":
		case "EmpreNoPagaAportIndic": case "PagosDosEmpresasMenos12Dias":  case "RegistrosConGiroEinactivos":  case "RegistrosDosPgosPuStc": case "RegistrosValoresDiferentes":
			var data = {caso:caso};
			datos = enviarPeticion(data);
		break;
	}
	
	if( datos ){
		$("#tdBoton").html("<img src='" + URL + "imagenes/descargarArchivo.png' style='cursor:pinter' onclick='descargar()'/>");
	}else{
		$("#tdBoton").html( "Error al generar el archivo" );
	}
}

function periodoAnterior(periodo){
	var periodoA = null,
		mes =  parseInt( periodo.substring(4,6) ),
		anno = parseInt( periodo.substring(0,4) );
	if ( mes > 1 ){
		mes = ( mes-1 );
		mes = ( mes < 10 ? "0" + mes : mes );
		periodoA = anno + "" + mes;
	}else{		
		periodoA = ( anno - 1 ) + "12";
	}		
	return periodoA;
}
/*data:{caso:caso,parametros}*/
function enviarPeticion(data){
	var datosRetorno = false;
	$.ajax({
		url:"reporteInconsistencia.php",
		type:"POST",
		data:data,
		dataType:"json",
		async:false,
		beforeSend: function(objeto){
			$("#tdBoton").html("<img src='" + URL + "imagenes/ajax-loader-fon.gif'/>");
        },        
        complete: function(objeto, exito){
        	$("#tdBoton").html("");            
        },
		success:function(datos){
			if( parseInt( datos ) === 1 ){
				datosRetorno = true;
			}else{
				datosRetorno = false;
			}
		}
	});
	return datosRetorno;
}

function descargar(){
	var url=URL+'phpComunes/descargaPlanos.php';
	window.open(url);
}