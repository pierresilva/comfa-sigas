/*
* Autor: Orlando Puentes Andrade
* Objetivo: Archivo para informe mensual de aportes
* Fecha: 24-Mar-2011
* ----------- E M P R E S A -------------- 
*/

//var URL=src();
// funcion para obtener cookie en javascript
	function getCookie(c_name)
	{
		var i,x,y,ARRcookies=document.cookie.split(";");
		for (i=0;i<ARRcookies.length;i++)
		{
  			x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
  			y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
  			x=x.replace(/^\s+|\s+$/g,"");
  			if (x==c_name)
    			{
    				return unescape(y);
    			}// fin if
		}// fin for
  	}// fin getCookie
//funcion para obtener cookie en javascript
	
$(function(){
	$('#txtFechaI').datepicker({
		maxDate:"+0D",
		changeMonth: true,
		changeYear: true,
		onSelect: function(dateText, inst) {
			var lockDate = new Date($('#txtFechaI').datepicker('getDate'));
			$('input#txtFechaF').datepicker('option', 'minDate', lockDate);
		}
	});
	$("#txtFechaF").datepicker({
		maxDate:"+0D",
		changeMonth: true,
		changeYear: true
	});
});
function reporte001(){
	var fechaI  = $("#txtFechaI").val();	
	var fechaF  = $("#txtFechaF").val();	
	var usuario = $("#txtusuario").val();
	if(fechaI == "" || fechaF==""){
		alert("Debe digitar las dos fechas");
		return false;
	}
	url="http://"+getCookie("URL_Reportes")+"/aportes/empresaContrato/reporte001.jsp?v0="+fechaI+"&v1="+fechaF+"&v2="+usuario;
	window.open(url,"_NEW");
}






