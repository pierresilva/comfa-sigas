<?php
set_time_limit(0);
date_default_timezone_set("America/Bogota");
ini_set('memory_limit', '1000M');

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
include_once $raiz.DIRECTORY_SEPARATOR.'phpComunes'.DIRECTORY_SEPARATOR.'exportar_pdf'.DIRECTORY_SEPARATOR.'util.class.php';
include_once $raiz.DIRECTORY_SEPARATOR.'aportes'.DIRECTORY_SEPARATOR.'independientes'.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'control_notificacion.class.php';
include_once $raiz.DIRECTORY_SEPARATOR.'centroReportes'.DIRECTORY_SEPARATOR.'aportes'.DIRECTORY_SEPARATOR.'independientes'.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'carta_notif_mora.class.php';
include_once $raiz.DIRECTORY_SEPARATOR.'centroReportes'.DIRECTORY_SEPARATOR.'aportes'.DIRECTORY_SEPARATOR.'independientes'.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'carta_notif_desaf.class.php';
include_once $raiz.DIRECTORY_SEPARATOR.'centroReportes'.DIRECTORY_SEPARATOR.'aportes'.DIRECTORY_SEPARATOR.'independientes'.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'carta_notif_reafi.class.php';

$objControlNotificacion = new ControlNotificacion();

$arrDatosCarta = array();
$codigoCarta = $_REQUEST["codigo_carta"];
$arrFiltro = isset($_REQUEST["filtro"])?$_REQUEST["filtro"]:array();
$usuario = $_SESSION["USUARIO"];

$contenidoHtml = "";
switch ($codigoCarta){
	case "CARTA_NOTIF_MOROS_INDEP":
		
		$arrFiltro["comunicacion"]=isset($_REQUEST["comunicacion"])?$_REQUEST["comunicacion"]:"";

		$arrFiltro["identificacion"] = isset($_REQUEST["identificacion"])?$_REQUEST["identificacion"]:"";
		$arrFiltro["fecha_mora_inicial"] = isset($_REQUEST["fecha_inicial"])?$_REQUEST["fecha_inicial"]:"";
		$arrFiltro["fecha_mora_final"]= isset($_REQUEST["fecha_final"])?$_REQUEST["fecha_final"]:"";
		$arrFiltro["id_agencia"] = isset($_REQUEST["id_agencia"])?$_REQUEST["id_agencia"]:"";
		$arrFiltro["usuario"] = isset($_REQUEST["usuario"])?$_REQUEST["usuario"]:"";
		
		$objCartaNotifMora = new CartaNotifMora();
		
		$arrDatosCarta = $objCartaNotifMora->fetch_carta($arrFiltro);
		
		break;
	
	case "CARTA_NOTIF_DESAF_INDEP":
		
		$arrFiltro["comunicacion"]=isset($_REQUEST["comunicacion"])?$_REQUEST["comunicacion"]:"";

		$arrFiltro["identificacion"] = isset($_REQUEST["identificacion"])?$_REQUEST["identificacion"]:"";
		$arrFiltro["fecha_desaf_inicial"] = isset($_REQUEST["fecha_inicial"])?$_REQUEST["fecha_inicial"]:"";
		$arrFiltro["fecha_desaf_final"]= isset($_REQUEST["fecha_final"])?$_REQUEST["fecha_final"]:"";
		$arrFiltro["id_agencia"] = isset($_REQUEST["id_agencia"])?$_REQUEST["id_agencia"]:"";
		$arrFiltro["usuario"] = isset($_REQUEST["usuario"])?$_REQUEST["usuario"]:"";

		$objCartaNotifDesaf = new CartaNotifDesaf();
		
		$arrDatosCarta = $objCartaNotifDesaf->fetch_carta($arrFiltro);
	
		break;
	

	case "CARTA_NOTIF_REAFI_INDEP":

		$arrFiltro["comunicacion"]=isset($_REQUEST["comunicacion"])?$_REQUEST["comunicacion"]:"";

		$arrFiltro["identificacion"] = isset($_REQUEST["identificacion"])?$_REQUEST["identificacion"]:"";
		$arrFiltro["fecha_estado_desaf_inicial"] = isset($_REQUEST["fecha_inicial"])?$_REQUEST["fecha_inicial"]:"";
		$arrFiltro["fecha_estado_desaf_final"]= isset($_REQUEST["fecha_final"])?$_REQUEST["fecha_final"]:"";
		$arrFiltro["id_agencia"] = isset($_REQUEST["id_agencia"])?$_REQUEST["id_agencia"]:"";
		$arrFiltro["usuario"] = isset($_REQUEST["usuario"])?$_REQUEST["usuario"]:"";

		$objCartaNotifReafi = new CartaNotifReafi();
		
		$arrDatosCarta = $objCartaNotifReafi->fetch_carta($arrFiltro);
		
		break;
	
	case "GENERAR_NOTIFICACION_PDF":
		
		$idNotificacion = $_REQUEST["id_notificacion"];
		$datoNotificacion = $objControlNotificacion->buscar_notificacion(array("id_notificacion"=>$idNotificacion));
		
		if(count($datoNotificacion)>0){
			$contenidoHtml = $datoNotificacion[0]["contenido_carta"];
		}else{
			$contenidoHtml = "<b>ERROR AL GENERAR LA CARTA</b>";
		}
		
		break;
}

if($codigoCarta!="GENERAR_NOTIFICACION_PDF"){
	$objControlNotificacion->inicioTransaccion();
	$banderaError = 0;
	
	if(count($arrDatosCarta)>0){
		//Guardar control de notificaciones
		foreach($arrDatosCarta as $carta){
			$contenidoHtml .= $carta["contenido_carta"];
		
			if($objControlNotificacion->guardar_notificacion($carta,$usuario)==0){
				$banderaError++;
				break;
			}
		}
		
		//Control de error
		if($banderaError==0){
			//Ok
			$objControlNotificacion->confirmarTransaccion();
		}else{
			//Error
			$objControlNotificacion->cancelarTransaccion();
			$contenidoHtml = "<b>ERROR AL GENERAR LAS CARTAS</b>";
		}	
	}else{
		$contenidoHtml = "<b>NO SE ENCONTRARON DATOS</b>";
	}
}

//Generar las cartas en formato pdf
Util::archivoPDF($contenidoHtml);


?>