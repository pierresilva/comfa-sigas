
// funcion para obtener cookie en javascript
	function getCookie(c_name)
	{
		var i,x,y,ARRcookies=document.cookie.split(";");
		for (i=0;i<ARRcookies.length;i++)
		{
  			x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
  			y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
  			x=x.replace(/^\s+|\s+$/g,"");
  			if (x==c_name)
    			{
    				return unescape(y);
    			}// fin if
		}// fin for
  	}// fin getCookie
//funcion para obtener cookie en javascript
	
URL=src();
var uriBuscarControlNotificacion ="";
$(document).ready(function(){
	uriBuscarControlNotificacion = URL+"aportes/independientes/logica/control_notificacion.log.php";
	
	datepickerC("FECHA_BETWEEN_LIBRE","#txtFechaInicial,#txtFechaFinal");
	
	//Evento para buscar las notificaciones de las cartas
	$("#txtNumeroIdentificacion").blur(ctrNotificacion);
	
	$("#divControlNotificacion").dialog({
		autoOpen: false,
		modal: true,
		width: 900,
		buttons:{
			"CERRAR": function(){
				$(this).dialog("close");
			}
		}
	});
});

function fetchControlNotificacion(datos){
	var resultado = [];
	
	$.ajax({
		type:"POST",
		url:uriBuscarControlNotificacion, 
		data:{accion:'S',objDatosFiltro:datos}, 
		dataType:"json",
		async:false,
		success:function(datos){
			resultado = datos;
		}
	});
	
	return resultado;
}

function generarNotificacionPDf(idNotificacion){
	var data = "codigo_carta=GENERAR_NOTIFICACION_PDF&id_notificacion="+idNotificacion;
	
	var url=URL+"centroReportes/aportes/independientes/generar_carta.log.php?"+data;
	window.open(url,"_BLANK");
}

function ctrNotificacion(){
	var idCarta = $("#hidIdCarta").val();
	var identificacion = $("#txtNumeroIdentificacion").val();
	
	if(!identificacion || !idCarta) return false;
	
	var dataNotificacion = fetchControlNotificacion({id_carta_notificacion:idCarta,identificacion:identificacion});
	
	if(dataNotificacion.data){
		dataNotificacion = dataNotificacion.data;
		var contenidoHtml = "";
		
		$.each(dataNotificacion,function(i,row){
			contenidoHtml += "<tr><td>"+row.carta_notificacion+"</td>"
								+"<td>"+row.fecha_sistema+"</td>"
								+"<td>"+row.estado_notificacion+"</td>"
								+"<td>"+row.fecha_estado+"</td>"
								+"<td>"+row.informacion+"</td>"
								+"<td><label style='cursor:pointer' onClick='generarNotificacionPDf("+row.id_notificacion+");'><img src='../../../imagenes/icono_pdf.png' width='24' height='24'></label></td></tr>";
		});
		
		$("#tbControlNotificacion tbody").html(contenidoHtml);
		$("#divControlNotificacion").dialog("open");
	}else{
		alert("No se encontraron notificaciones para el afiliado!!");
	}
}

function reporte001(){
	//Verificar campos obligatorios
	if(validateElementsRequired()) return false;

	var comunicacion = $("#txtComunicacion").val().trim(),
		identificacion = $("#txtNumeroIdentificacion").val().trim(),
		tipoFiltro = $("#cmbFiltro").val(),
		fechaI = $("#txtFechaInicial").val(),
		fechaF = $("#txtFechaFinal").val(),
		idAgencia = $("#cmbIdAgencia").val(),
		usuario = $("#txtNombreUsuario").val();
	
	var data = "codigo_carta=CARTA_NOTIF_MOROS_INDEP&comunicacion="+comunicacion
			+"&identificacion="+identificacion
			+"&fecha_inicial="+fechaI
			+"&fecha_final="+fechaF
			+"&id_agencia="+idAgencia
			+"&usuario="+usuario;
	
	var url=URL+"centroReportes/aportes/independientes/generar_carta.log.php?"+data;
	window.open(url,"_BLANK");
}

function reporte002(){
	//Verificar campos obligatorios
	if(validateElementsRequired()) return false;

	var comunicacion = $("#txtComunicacion").val().trim(),
		identificacion = $("#txtNumeroIdentificacion").val().trim(),
		tipoFiltro = $("#cmbFiltro").val(),
		fechaI = $("#txtFechaInicial").val(),
		fechaF = $("#txtFechaFinal").val(),
		idAgencia = $("#cmbIdAgencia").val(),
		usuario = $("#txtNombreUsuario").val();
	
	var data = "codigo_carta=CARTA_NOTIF_DESAF_INDEP&comunicacion="+comunicacion
			+"&identificacion="+identificacion
			+"&fecha_inicial="+fechaI
			+"&fecha_final="+fechaF
			+"&id_agencia="+idAgencia
			+"&usuario="+usuario;
	
	var url=URL+"centroReportes/aportes/independientes/generar_carta.log.php?"+data;
	window.open(url,"_BLANK");
}

function reporte003(){
	//Verificar campos obligatorios
	if(validateElementsRequired()) return false;

	var comunicacion = $("#txtComunicacion").val().trim(),
		identificacion = $("#txtNumeroIdentificacion").val().trim(),
		tipoFiltro = $("#cmbFiltro").val(),
		fechaI = $("#txtFechaInicial").val(),
		fechaF = $("#txtFechaFinal").val(),
		idAgencia = $("#cmbIdAgencia").val(),
		usuario = $("#txtNombreUsuario").val();
	
	var data = "codigo_carta=CARTA_NOTIF_REAFI_INDEP&comunicacion="+comunicacion
			+"&identificacion="+identificacion
			+"&fecha_inicial="+fechaI
			+"&fecha_final="+fechaF
			+"&id_agencia="+idAgencia
			+"&usuario="+usuario;
	
	var url=URL+"centroReportes/aportes/independientes/generar_carta.log.php?"+data;
	window.open(url,"_BLANK");
}