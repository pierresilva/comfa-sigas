<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';

include_once $raiz . DIRECTORY_SEPARATOR . 'promotoria' . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'promotoria.class.php';

$usuario=$_SESSION['USUARIO'];
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
include_once $raiz.DIRECTORY_SEPARATOR.'seguridad'.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'agencia.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

//obtener los datos del usuario
$sql = "SELECT a15.sexo
		FROM aportes519 a519
			INNER JOIN aportes015 a15 ON a15.identificacion=a519.identificacion
		WHERE a519.usuario='$usuario'";
$rsDatoUsuario = $db->querySimple($sql);
$sexoUsuario = $rsDatoUsuario->fetchObject()->sexo;

$tipo=$_REQUEST['tipo'];
$titulo='...';

switch($tipo){
	case 1:$titulo='Carta Notificaci&oacute;n de Morosidad'; break;
	case 2:$titulo='Carta Notificaci&oacute;n de Desafiliaci&oacute;n'; break;
	case 3:$titulo='Carta Notificaci&oacute;n de Re Afiliaci&oacute;n'; break;
}

$comboAgencia = "";
//Obtener el combo de las agencias
$datosAgencias = Agencia::listarTodas();
foreach ($datosAgencias as $rowAgencia){
	$comboAgencia .= "<option value=".$rowAgencia->getIdAgencia().">".$rowAgencia->getAgencia()."</option>";
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<title>Men&uacute; Reportes Independientes</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link type="text/css" href="<?php echo URL_PORTAL; ?>newcss/Estilos.css" rel="stylesheet">
		<link type="text/css" href="<?php echo URL_PORTAL; ?>newcss/css.1.8/jquery-ui-1.8.17.custom.css" rel="stylesheet">
		<link type="text/css" href="<?php echo URL_PORTAL; ?>newcss/marco.css" rel="stylesheet">
		
		<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/1.6/jquery-1.6.2.min.js"></script>
		<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/1.6/jquery-ui-1.8.16.custom.min.js"></script>
		<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/comunes.js"></script>
		<script type="text/javascript" src="js/reporte.js"></script>
	</head>
	<body>
		<center>
			<table width="100%" border="0">
				<tr>
					<td align="center" ><img src="<?php echo URL_PORTAL; ?>imagenes/logo_reporte.png" width="362" height="70"></td>
				</tr>
				<tr>
					<td align="center" ><?php echo $titulo ?></td>
				</tr>
			</table>
		<br>
		<?php
			if($tipo==1){
		?>
		<div style="display:block">
			<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
				<tr>
			    	<th colspan="2"><strong>Parametros de Configuracion Reporte</strong></th>
			    </tr>
			    <tr>
				    <td width="50%" style="text-align: center;">
				    	Comunicaci&oacute;n:
				    	<input type="text" name="txtComunicacion" id="txtComunicacion" class="box1 element-required"/>
				    	<input type="hidden" name="hidIdCarta" id="hidIdCarta" value="4567" />
				    	<span class="clsObligatorio">&nbsp;&nbsp;&nbsp;</span>
				    </td>
			    </tr>
			    <tr>
				    <td style="text-align: center;">
				    	N&uacute;umero Identificaci&oacute;n:
				    	<input type="text" name="txtNumeroIdentificacion" id="txtNumeroIdentificacion" class="box1"/>
				    </td>
			    </tr>
			    <tr >
				    <td style="text-align: center;">
				    	Fecha Morosidad: 
				    	<input name="txtFechaInicial" type="text" class="box1 element-required" id="txtFechaInicial" readonly="readonly"/>
				    	- <input name="txtFechaFinal" type="text" class="box1 element-required" id="txtFechaFinal" readonly="readonly"/>
				    	<span class="clsObligatorio">&nbsp;&nbsp;&nbsp;</span>
				    </td>
			    </tr>
			    <tr>
			    	<td style="text-align: center;">
			    		Agencia
			    		<select name="cmbIdAgencia" id="cmbIdAgencia" class="box1">
			    			<option value="">Seleccione</option>
			    			<?php
			    				echo $comboAgencia;
			    			?>
			    		</select>
			    	</td>
			    </tr>
			    <tr>
		      		<td colspan="2">&nbsp;</td>
			  	</tr>
			    <tr>
		      		<td colspan="2" style="text-align:center">
		      			<label style="cursor:pointer" onClick="reporte001();"><img src="<?php echo URL_PORTAL; ?>imagenes/icono_pdf.png" width="32" height="32"></label>
		      		</td>
			    </tr>
	    	</table>
	    </div>
	 	<?php
			}
			if($tipo==2){
		?>
		<div style="display:block">
			<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
				<tr>
			    	<th colspan="2"><strong>Parametros de Configuracion Reporte</strong></th>
			    </tr>
			    <tr>
				    <td width="50%" style="text-align: center;">
				    	Comunicaci&oacute;n:
				    	<input type="text" name="txtComunicacion" id="txtComunicacion" class="box1 element-required"/>
				    	<input type="hidden" name="hidIdCarta" id="hidIdCarta" value="4568" />
				    	<span class="clsObligatorio">&nbsp;&nbsp;&nbsp;</span>
				    </td>
			    </tr>
			    <tr>
				    <td style="text-align: center;">
				    	N&uacute;umero Identificaci&oacute;n:
				    	<input type="text" name="txtNumeroIdentificacion" id="txtNumeroIdentificacion" class="box1"/>
				    </td>
			    </tr>
			    <tr >
				    <td style="text-align: center;">
				    	Fecha Desafiliaci&oacute;n: 
				    	<input name="txtFechaInicial" type="text" class="box1 element-required" id="txtFechaInicial" readonly="readonly"/>
				    	- <input name="txtFechaFinal" type="text" class="box1 element-required" id="txtFechaFinal" readonly="readonly"/>
				    	<span class="clsObligatorio">&nbsp;&nbsp;&nbsp;</span>
				    </td>
			    </tr>
			    <tr>
			    	<td style="text-align: center;">
			    		Agencia
			    		<select name="cmbIdAgencia" id="cmbIdAgencia" class="box1">
			    			<option value="">Seleccione</option>
			    			<?php
			    				echo $comboAgencia;
			    			?>
			    		</select>
			    	</td>
			    </tr>
			    <tr>
		      		<td colspan="2">&nbsp;</td>
			  	</tr>
			    <tr>
		      		<td colspan="2" style="text-align:center">
		      			<label style="cursor:pointer" onClick="reporte002();"><img src="<?php echo URL_PORTAL; ?>imagenes/icono_pdf.png" width="32" height="32"></label>
		      		</td>
			    </tr>
	    	</table>
	    </div>
	 	<?php
			}
			if($tipo==3){
		?>
		<div style="display:block">
			<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
				<tr>
			    	<th colspan="2"><strong>Parametros de Configuracion Reporte</strong></th>
			    </tr>
			    <tr>
				    <td width="50%" style="text-align: center;">
				    	Comunicaci&oacute;n:
				    	<input type="text" name="txtComunicacion" id="txtComunicacion" class="box1 element-required"/>
				    	<input type="hidden" name="hidIdCarta" id="hidIdCarta" value="4569" />
				    	<span class="clsObligatorio">&nbsp;&nbsp;&nbsp;</span>
				    </td>
			    </tr>
			    <tr>
				    <td style="text-align: center;">
				    	N&uacute;umero Identificaci&oacute;n:
				    	<input type="text" name="txtNumeroIdentificacion" id="txtNumeroIdentificacion" class="box1"/>
				    </td>
			    </tr>
			    <tr >
				    <td style="text-align: center;">
				    	Fecha Estado Desafiliaci&oacute;n: 
				    	<input name="txtFechaInicial" type="text" class="box1 element-required" id="txtFechaInicial" readonly="readonly"/>
				    	- <input name="txtFechaFinal" type="text" class="box1 element-required" id="txtFechaFinal" readonly="readonly"/>
				    	<span class="clsObligatorio">&nbsp;&nbsp;&nbsp;</span>
				    </td>
			    </tr>
			    <tr>
			    	<td style="text-align: center;">
			    		Agencia
			    		<select name="cmbIdAgencia" id="cmbIdAgencia" class="box1">
			    			<option value="">Seleccione</option>
			    			<?php
			    				echo $comboAgencia;
			    			?>
			    		</select>
			    	</td>
			    </tr>
			    <tr>
		      		<td colspan="2">&nbsp;</td>
			  	</tr>
			    <tr>
		      		<td colspan="2" style="text-align:center">
		      			<label style="cursor:pointer" onClick="reporte003();"><img src="<?php echo URL_PORTAL; ?>imagenes/icono_pdf.png" width="32" height="32"></label>
		      		</td>
			    </tr>
	    	</table>
	    </div>
	   	<?php }	?>
	
		<input type="hidden" id="txtUsuario" name="txtUsuario" value="<?php echo $usuario; ?>" /> 
		<input type="hidden" id="txtNombreUsuario" name="txtNombreUsuario" value="<?php echo $_SESSION["USSER"]; ?>" />
		<input type="hidden" id="txtSexoUsuario" name="txtSexoUsuario" value="<?php echo $sexoUsuario; ?>" 
		<input type="hidden" id="txtRept" name="txtRept" value="<?php echo $rept; ?>" />
		
		<!-- DIV PARA MOSTRAR LAS NOTIFICACIONES DE LA EMPRESA SEGUN LA CARTA A GENERAR -->
		<div id="divControlNotificacion" style="display: none;" title="CONTROL NOTIFICACI&Oacute;N"> 
			<table width="90%" border="0" align="center"  class="tablero" id="tbControlNotificacion">
				<thead>
					<tr>
						<th>CARTA</th>
						<th>FECHA NOTIFICACI&Oacute;N</th>
						<th>ESTADO</th>
						<th>FECHA ESTADO</th>
						<th>INFORMACI&Oacute;N</th>
						<th>...</th>
					</tr>
				</thead>
				<tbody>
					
				</tbody>
			</table>
		</div>
		
	</body>
</html>