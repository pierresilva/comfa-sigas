<?php
set_time_limit(0);
date_default_timezone_set("America/Bogota");
ini_set('memory_limit', '1000M');

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
include_once $raiz.DIRECTORY_SEPARATOR.'config.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
//include_once $raiz.DIRECTORY_SEPARATOR.'aportes'.DIRECTORY_SEPARATOR.'empresas'.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'empresas.class.php';
include_once $raiz.DIRECTORY_SEPARATOR.'parametrizacion'.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'modelo_documento_html.class.php';
include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'utilitario.class.php';

class CartaNotifMora{
	
	private static $conPDO = null;
	
	private $idCarta = 4567; //[4567][CARTA NOTIFICACION DE MOROSIDAD]
	private $modeloDocumento = "CARTA_NOTIF_MOROS_INDEP";
	private $arrResultado = null;
	private $arrFiltro = null;
	
	private $fechaSistema;
	
	private $objModeloDocumentoHtml = null;
	
	function __construct(){
		try{
			self::$conPDO = IFXDbManejador::conectarDB();
			if( self::$conPDO->conexionID==null ){
				throw new Exception(self::$conPDO->error);
			}
		}catch(Exception $e){
			echo $e->getMessage();
			exit();
		}
		
		$meses = array(
				"Enero"
				,"Febrero"
				,"Marzo"
				,"Abril"
				,"Mayo"
				,"Junio"
				,"Julio"
				,"Agosto"
				,"Septiembre"
				,"Octubre"
				,"Noviembre"
				,"Diciembre");
		
		$this->fechaSistema = date("d") . " " . $meses[date("n")-1] . " " . date("Y");
		$this->objModeloDocumentoHtml = new ModeloDocumentoHtml();
	}
	
	/**
	 * Metodo encargado de preparar el contenido de las cartas
	 *  
	 * @param unknown_type $arrFiltro
	 */
	public function fetch_carta($arrFiltro){
		$this->arrFiltro = $arrFiltro;		
		
		$this->preparar_datos();
		
		return $this->arrResultado;
	}
	
	private function fetch_datos(){
		$arrData = array();
		$query = "
				DECLARE @identificacion VARCHAR(20) = '{$this->arrFiltro["identificacion"]}', @fecha_mora_inicial DATE = '{$this->arrFiltro["fecha_mora_inicial"]}', @fecha_mora_final DATE ='{$this->arrFiltro["fecha_mora_final"]}', @id_agencia CHAR(2) = '{$this->arrFiltro["id_agencia"]}'
				SELECT
					a250.id_trabajador 
					, a15.pnombre+' '+isnull(a15.snombre,'')+' '+a15.papellido+' '+isnull(a15.sapellido,'') AS razonsocial
					, a15.identificacion, a15.direccion, a15.telefono
					, a89.municipio
					, a91.detalledefinicion AS tipo_afiliacion
					,(	SELECT periodo+' - ' 
						FROM aportes250 
						WHERE id_trabajador=a250.id_trabajador AND pago='N'
						FOR XML PATH('')
					) AS periodo_mora
				FROM aportes250 a250
					LEFT JOIN aportes015 a15 ON a15.idpersona=a250.id_trabajador
					LEFT JOIN aportes089 a89 ON a89.codmunicipio=a15.idciuresidencia
					LEFT JOIN aportes016 a16 ON a16.idformulario=a250.id_afiliacion
					LEFT JOIN aportes091 a91 ON a91.iddetalledef=a16.tipoafiliacion
				WHERE a250.pago='N'
					AND (@identificacion = '' OR a15.identificacion=@identificacion) 
					AND (@fecha_mora_inicial = '' OR a250.fecha_sistema BETWEEN @fecha_mora_inicial AND @fecha_mora_final)
					AND (@id_agencia = '' OR a16.idagencia=@id_agencia)
					AND 
					-- NO DEBEN EXISTIR DESAFILIACIONES ACTIVAS 
					0=(
						SELECT COUNT(*) 
						FROM aportes251 
						WHERE id_trabajador=a250.id_trabajador AND estado='A'
					)
				GROUP BY a250.id_trabajador
					, a15.pnombre,a15.snombre,a15.papellido,a15.sapellido
					, a15.identificacion, a15.direccion, a15.telefono
					, a89.municipio
					, a91.detalledefinicion";
		
		$arrData = Utilitario::fetchConsulta($query, self::$conPDO);
		return $arrData; 
	}
	
	/**
	 * Obtener la plantilla de la carta en formato Html
	 * @return string
	 */
	private function fetch_plantilla_carta(){
		
		$contenidoCarta = "";
		
		$arrDatos = $this->objModeloDocumentoHtml->fetch_modelo_documento(array("modelo_documento"=>$this->modeloDocumento));
		
		if(count($arrDatos)>0){
			$contenidoCarta = $arrDatos[0]["contenido_html"];
		}
		
		return $contenidoCarta;
	}
	
	private function preparar_datos(){
		
		//Obtener los datos de la visita
		$arrDatos = $this->fetch_datos();
		
		//Obtener los modelos de las cartas
		$plantCarta = $this->fetch_plantilla_carta();
		
		//Recorrer las visitas
		foreach($arrDatos as $datos){
			$plantillaCarta = $plantCarta;
			
			$datos["periodo_mora"] = trim($datos["periodo_mora"],"-");
			$datos["fecha_sistema"] = $this->fechaSistema;
			$datos["usuario"] = $this->arrFiltro["usuario"];
			$datos["comunicacion"] = $this->arrFiltro["comunicacion"];
			$datos["uri_img_firma"] = '../../../imagenes/firma_maria_isabel_diaz.png';
			
			$contenidoCarta = $this->preparar_contenido_html($plantillaCarta,$datos);
			
			$this->arrResultado[] = array(
					"id_carta_notificacion"=>$this->idCarta
					,"id_trabajador"=>$datos["id_trabajador"]
					,"informacion"=>''
					,"id_estado"=>0
					,"contenido_carta"=>$contenidoCarta);
		}
	}
	
	private function preparar_contenido_html($plantillaHtml, $datos){
		
		$arrIndiceModelo = array(
				"fecha_sistema"=>"fecha_sistema",
				"comunicacion"=>"comunicacion",
				"razonsocial_upper"=>"razonsocial",
				"identificacion"=>"identificacion",
				"direccion"=>"direccion",
				"telefono"=>"telefono",
				"municipio_upper"=>"municipio",
				
				"tipo_afiliacion_upper"=>"tipo_afiliacion",
				"periodo_mora"=>"periodo_mora",
				"usuario"=>"usuario",
				"uri_img_firma"=>"uri_img_firma"
		);
		
		//Datos referencia uno
		$plantillaHtml = Util::reempDatosPlant($plantillaHtml,$arrIndiceModelo,$datos);
		
		return $plantillaHtml;
	}
}

?>