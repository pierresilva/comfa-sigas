<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<title>Men&uacute; Reportes de Independientes</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link href="<?php echo URL_PORTAL; ?>css/estiloReporte.css" rel="stylesheet" type="text/css">
		<script src="<?php echo URL_PORTAL; ?>newjs/comunes.js" type="text/javascript"></script>
	</head>
	<body>
		<center>
	  		<table width="100%" border="0">
	  			<tr>
	    			<td align="center" ><img src="../../imagenes/razonSocial.png" width="615" height="55"></td>
	  			</tr>
	  			<tr>
	    			<td align="center" >&nbsp;</td>
	  			</tr>
	  			<tr>
	    			<td class="fecha" ></td>
	  			</tr>
			</table>
			<table width="60%" border="0" cellspacing="1" class="tablero">
				<tr><th>Reporte</th>
					<th>Listado de Reportes de los Independientes</th></tr>  
		  		<tr>
		    		<td scope="row" align="left">Reporte001</td>
		   	 		<td align="left"><a href="configReportePromotoria.php?tipo=1">Carta Notificaci&oacute;n de Morosidad.</a></td>
		  		</tr>
	 			<tr>
		  			<td scope="row" align="left" width="20%">Reporte002</td>
		  			<td align="left"><a href="configReportePromotoria.php?tipo=2">Carta Notificaci&oacute;n de Desafiliaci&oacute;n.</a></td>
				</tr>
		 		<tr>
		   			<td scope="row" align="left">Reporte003</td>
		   			<td align="left"><a href="configReportePromotoria.php?tipo=3">Carta Notificaci&oacute;n de Re Afiliaci&oacute;n.</a></td>
	    		</tr>
			</table>
		</center>
	</body>
</html>