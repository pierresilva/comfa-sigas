<?php
/* autor:       orlando puentes
 * fecha:       28/09/2010
 * objetivo:    

if ( !isset( $_SESSION ['USUARIO'] ) ) {session_start();}
$raiz=$_SESSION['RAIZ'];
chdir($raiz);
*/
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once $raiz.DIRECTORY_SEPARATOR ."config.php";
include_once  $root;
$usuario=$_SESSION['USUARIO'];

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Men&uacute; Reportes Radicaci&oacute;n</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link type="text/css" href="../../../css/Estilos.css" rel="stylesheet"/>
</head>
<body>
<center>
<table width="100%" border="0">
<tr>
<td align="center" ><img src="../../../imagenes/logo_reporte.png"></td>
</tr>
<tr>
<td align="center" >Listado de Reportes Nómina Manual</td>
</tr>
</table>
<br>
<table width="60%" border="0" cellspacing="1" class="tablero">
<tr>
<th>Reporte</th>
<th colspan="2">Descripci&oacute;n</th></tr>  
<tr>
<td scope="row" align="left">Reporte001</td>             
<td align="left">
<a style="text-decoration:none" target='_blank' href="<?php echo $ruta_reportes;?>aportes/nmanual/reporte001.jsp?v0=<?php echo $usuario; ?>" >Listado de Nóminas Manuales Realizadas </a>
</td>
</tr>
	<tr>
	   <td scope="row" align="left">Reporte002</td>             
	   <td align="left">Listado de Nóminas por NIT</td>
    </tr>
		   
		<tr>
			<td>Reporte003</td>
			<td><a href="<?php echo URL_PORTAL.DIRECTORIO_CENTRO_REPORTES; ?>aportes/radicacion/reporte002.php" target="_blank">Imprimir una Nómina Manual por ID</a></td>
		</tr>
		<tr>
			<td>Reporte004</td>
			<td><a href="<?php echo URL_PORTAL.DIRECTORIO_CENTRO_REPORTES; ?>aportes/radicacion/reporte003.php">Copia del Plano de una nómina manual por ID</a></td>
		</tr>
		<tr>
			<td>Reporte005</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Reporte006</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Reporte007</td>
			<td>&nbsp;</td>
		</tr>
        <tr>
			<td>Reporte008</td>
			<td>&nbsp;</td>
		</tr>
        <tr>
			<td>Reporte009</td>
			<td>&nbsp;</td>
		</tr>
		 
		<tr>
			<td>Reporte010</td>
			<td align="left">&nbsp;</td>
		</tr>
		<tr>
			<td>Reporte011</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Reporte012</td>
			<td>&nbsp;</td>
		</tr>
	</table>
	</center>
</body> 
</html>
