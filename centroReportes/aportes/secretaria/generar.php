<?php
$modulo = $_REQUEST["modulo"];
$idArchivo = $_REQUEST["archivo"];
$identificacion = $_REQUEST["identificacion"];

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;


include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'p.persona.class.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'rsc' . DIRECTORY_SEPARATOR . 'ControladorCdi.class.php';
$objControlador = new ControladorCdi();
$consecutivo = $objControlador->obtenerUltimoConsecutivo();
$coordinador = $objControlador->obtenerDatosCoordinadorSubsidioCdi();
$datosDestino = $objControlador->obtenerDatosPersonaNaturalOJuridica($identificacion);
$docEntidadDestino = '';
$consecEntidadDestino = null;
//@todo si no existe la entidad destino en la db del cdi, se debe crear...
if(is_array($datosDestino)){
	$docEntidadDestino = $datosDestino["njNoDoc"];
	$consecEntidadDestino = $datosDestino["njConsecutivo"];
}

switch($idArchivo){
    case 1: // certificado de afiliacion
        $query = "select aportes091.detalledefinicion, aportes015.pnombre, aportes015.identificacion,aportes015.snombre,aportes015.papellido, aportes015.sapellido,aportes048.nit,aportes048.razonsocial, aportes016.fechaingreso, aportes091.detalledefinicion as tipof from aportes016 inner join aportes091 on aportes016.tipoformulario = aportes091.iddetalledef inner join aportes048 on aportes048.idempresa = aportes016.idempresa inner join aportes015 on aportes016.idpersona = aportes015.idpersona where	aportes015.identificacion='$identificacion'";
        $result=$db->querySimple($query);
        $datosParaElCertificado = $result->fetch();
        $cuerpo = "<h2>EL COORDINADOR DE APORTES Y SUBISIO</h2> <h2>HACE CONSTAR</h2> <p>Que el se�or(a) {$datosParaElCertificado["pnombre"]} {$datosParaElCertificado["snombre"]} {$datosParaElCertificado["papellido"]} {$datosParaElCertificado["sapellido"]} identificado(a) con Cedula de Ciudadania No. {$datosParaElCertificado["identificacion"]}, se encuentra afiliado(a) a nuestra Entidad en la modalidad de {$datosParaElCertificado["detalledefinicion"]} por intermedio de la empresa {$datosParaElCertificado["razonsocial"]} Nit No. {$datosParaElCertificado["nit"]}, desde {$datosParaElCertificado["fechaingreso"]}.</p>";
        $generado = $objControlador->generarDocumentoCdi($consecutivo,$coordinador["corCedulaFun"],$datosParaElCertificado["identificacion"],"Certificado de afiliacion",$cuerpo,'',1,$consecEntidadDestino);

        $archivo = "aportes/secretaria/reporte001.jsp";
        $strParametros = "v0={$datosParaElCertificado["identificacion"]}&v1=$consecutivo&v2={$_SESSION ['USUARIO']}";
        break;
    case 2:
        $kardex = $_REQUEST["kardex"];
        $query = "SELECT nit,razonsocial, 4 as porcentajeapo, aportes048.fechasistema  as tiempopagado, 'cancela' as cadenapago FROM aportes048 where nit='$identificacion'";
        $result=$db->querySimple($query);
        $datosParaElCertificado = $result->fetch();
        $cuerpo = "<h2>EL COORDINADOR DE APORTES Y SUBISIO</h2> <h2>HACE CONSTAR</h2><p>Que la empresa {$datosParaElCertificado["razonsocial"]} identificada con Nit. No. {$datosParaElCertificado["nit"]}, se encuentra afiliada a nuestra Entidad y {$datosParaElCertificado["cadenapago"]} aportes parafiscales del {$datosParaElCertificado["porcentajeapo"]}% (CAJA DE COMPENSACION), {$datosParaElCertificado["tiempopagado"]}.</p><p>Anexo. Kardex a�o $kardex.</p><p>La expedici�n de este documento no impide que Comfamiliar Huila pueda realizar visitas de verificaci�n de la correcta y oportuna liquidaci�n de los aportes, bases de liquidaci�n y el pago oportuno de los mismos. (Decreto 562/90).</p><p>ESTE CERTIFICADO NO ES VALIDO PARA EFECTOS DE AFILIACION A OTRA CAJA DE COMPENSACION NI COMO SOPORTE DE PAGO DE PARAFISCALES POR CONTRATOS.</p>";
        $generado = $objControlador->generarDocumentoCdi($consecutivo,$coordinador["corCedulaFun"],$datosParaElCertificado["identificacion"],"Certificado de afiliacion empresa",$cuerpo,'',1,$consecEntidadDestino);

        $archivo = "aportes/secretaria/reporte002.jsp";
        $strParametros = "v0={$datosParaElCertificado["nit"]}&v1={$_SESSION ['USUARIO']}&v2=$kardex&v3=$consecutivo";
        break;
    case 3:
        $nombre = $_REQUEST["nombre"];
        $cuerpo = "<h2>EL COORDINADOR DE APORTES Y SUBISIO</h2> <h2>HACE CONSTAR</h2><p>Me permito certificar que una vez revisada nuestra base de datos se verific&oacute; que el Se&ntilde;or(a) $nombre identificado(a) con la c&eacute;dula de ciudadan&iacute;a No. $identificacion, no figura registrado en nuestra Entidad como afiliado, ni como Aportante de parafiscales.</p>";
        $generado = $objControlador->generarDocumentoCdi($consecutivo,$coordinador["corCedulaFun"],$identificacion,"Certificado de trabajador inactivo",$cuerpo,'',1,$consecEntidadDestino);
        $archivo = "aportes/secretaria/reporte004.jsp";
        $strParametros = "v0=$identificacion&v1={$_SESSION ['USUARIO']}&v2=$nombre&v3=$consecutivo";
        break;
    case 4:
        $nombre = $_REQUEST["nombre"];
        $tipoDoc = $_REQUEST["tipodocumento"];
        $objPersona = new Persona;
        $consulta = $objPersona->mostrar_registro_para($identificacion,1,$tipoDoc);
        $resultadoPeticion = array();
        $data = array();
        $activo = false;
        while($row=mssql_fetch_array($consulta))
                $data = array_map("utf8_encode",$row);
        mssql_free_result($consulta);
        if(count($data)>0){
            // Existe como afiliado activo
            $nombre = $data["papellido"] + $data["sapellido"] + $data["pnombre"] + $data["snombre"];
            $activo = true;
        }else{
            // se busca por afiliado inactivo
            $consulta = $objPersona->mostrar_registro_inactivos($identificacion,1,$tipoDoc);
            $data = array();
            while($row = mssql_fetch_array($consulta))
                    $data = $row;
            mssql_free_result($consulta);
            if(count($data)>0)
                    $nombre = $data["papellido"] + $data["sapellido"] + $data["pnombre"] + $data["snombre"];
            else{
                // buscar tercero, no tiene afiliaciones
                $registro = $objPersona->obtener_persona_tercero_por_identificacion($identificacion);
                if($registro !== false){
                    $data = array_map("utf8_encode",$registro);
                    $nombre = $data["papellido"] + $data["sapellido"] + $data["pnombre"] + $data["snombre"];
                }
            }
        }

        if(!$activo){
            $cuerpo = "<h2>EL COORDINADOR DE APORTES Y SUBISIO</h2> <h2>HACE CONSTAR</h2><p>Me permito certificar que una vez revisada nuestra base de datos se verific&oacute; que el(la) Se&ntilde;or(a) <strong>$nombre</strong> identificado(a) con  c&eacute;dula de ciudadan&iacute;a No. $identificacion, no figura registrado en nuestra Entidad como afiliado, ni como Cotizante o Beneficiario.</p>";
            $generado = $objControlador->generarDocumentoCdi($consecutivo,$coordinador["corCedulaFun"],$identificacion,"Certificado Trabajador no Afiliado",$cuerpo,'',1,$consecEntidadDestino);
            $archivo = "aportes/secretaria/reporte004.jsp";
            $strParametros = "v0=$identificacion&v1={$_SESSION ['USUARIO']}&v2=$nombre&v3=$consecutivo";
        }
        break;
    case 5:
        // certificado contratista
        $query = "SELECT nit, razonsocial,9 as porcentajeapo,aportes002.idliquidacion as liquidacion,aportes002.contrato as contrato FROM aportes048 inner join aportes002 on aportes048.idempresa = aportes002.idcontratista where aportes048.nit='$identificacion'";
        $result=$db->querySimple($query);
        $datosParaElCertificado = $result->fetch();
        if(is_array($datosParaElCertificado)){
            $cuerpo = "<h2>EL COORDINADOR DE APORTES Y SUBISIO</h2> <h2>HACE CONSTAR</h2><p>Me permito certificar que el Contratista {$datosParaElCertificado["razonsocial"]} identificado(a) con Nit No. {$datosParaElCertificado["nit"]}, cancel&oacute; aportes parafiscales del {$datosParaElCertificado["porcentajeapo"]}% (Caja, SENA e I.C.B.F), correspondiente al contrato No. {$datosParaElCertificado["contrato"]} de (a&ntilde;o) suscrito con el municipio.</p>";
            $generado = $objControlador->generarDocumentoCdi($consecutivo,$coordinador["corCedulaFun"],$datosParaElCertificado["nit"],"Certificado de contratista",$cuerpo,'',1,$consecEntidadDestino);
            $archivo = "aportes/secretaria/reporte003.jsp";
            $strParametros = "v0={$datosParaElCertificado["nit"]}&v1={$_SESSION ['USUARIO']}&v2=$consecutivo";
        }else{
            die("No se encontr&oacute; informaci&oacute;n para mostrar.");
        }		
        break;
    case 6:
        // Informe Aportes SENA e ICBF primeros diez dias
        $consecutivo = $objControlador->obtenerUltimoConsecutivoInterno();
        $anno = $_REQUEST["v1"];
        $mesNumero = $_REQUEST["v2"];
        $mes = $_REQUEST["v3"];
        $usuario = $_REQUEST["v4"];
        
        $query = "SELECT ae.seccional, CASE WHEN sum(aa.sena) IS NULL THEN 0 ELSE (sum(aa.sena)) END as sena, CASE WHEN sum(aa.icbf) IS NULL THEN 0 ELSE (sum(aa.icbf)) END as icbf FROM aportes011 aa INNER JOIN aportes048 ae ON aa.idempresa = ae.idempresa WHERE (ae.seccional = '01' OR ae.seccional = '02' OR ae.seccional = '03' OR ae.seccional = '04') AND (year(aa.fechapago) = $anno AND month(aa.fechapago) = $mesNumero AND day(aa.fechapago) BETWEEN 01 AND 10) GROUP BY ae.seccional ";

        if($result=$db->querySimple($query)){
            $array = array();
            $subtotal = array('sena'=>0,'icbf'=>0);
            while($datosParaElCertificado = $result->fetchObject()){
                $array[$datosParaElCertificado->seccional]['sena'] = $datosParaElCertificado->sena;
                $array[$datosParaElCertificado->seccional]['icbf'] = $datosParaElCertificado->icbf;
                $subtotal['sena'] += $datosParaElCertificado->sena;
                $subtotal['icbf'] += $datosParaElCertificado->icbf;
            }
            $table = "<table border='1'><tr><td></td><td>SENA</td><td>I.C.B.F</td></tr><tr><td>Neiva</td><td>".$array['01']['sena']."</td><td>".$array['01']['icbf']."</td></tr><tr><td>Pitalito</td><td>".$array['02']['sena']."</td><td>".$array['02']['icbf']."</td></tr><tr><td>La Plata</td><td>".$array['03']['sena']."</td><td>".$array['03']['icbf']."</td></tr><tr><td>Garzon</td><td>".$array['04']['sena']."</td><td>".$array['04']['icbf']."</td></tr><tr><td><b>SUBTOTAL</b></td><td><b>".$subtotal['sena']."</b></td><td><b>".$subtotal['icbf']."</b></td></tr><tr><td>Menos 0.5%</td><td>".($subtotal['sena']*0.005)."</td><td>".($subtotal['icbf']*0.005)."</td></tr><tr><td><b>TOTAL PAGO </b></td><td><b>".($subtotal['sena']-($subtotal['sena']*0.005))."</b></td><td><b>".($subtotal['sena']-($subtotal['sena']*0.005))."</b></td></tr></table>";
            $asunto = "Recaudos aportes 10 primeros dias I.C.B.F.-SENA";
            $saludo = "Cordialmente.";
            $cuerpo = " <p>Me permito informar, los aportes patronales para el I.C.B.F y SENA recaudados en los diez primeros días del mes de $mes DE $anno</p><p>$table</p>";
            $despedida = "Atentamente,";
            $generado = $objControlador->generarDocumentoCdiInterno('1', $consecutivo, '55167234', 'DF', '7', '36069213', 'DF',  '2', 'C', 0, '7', $asunto, $saludo, $cuerpo, $despedida, '1');
            $archivo = "aportes/empresa/reporte005.jsp";
            $strParametros = "v0=$consecutivo&v1=$anno&v2=$mesNumero&v3=$mes&v4=$usuario";        
        }
        break;

    default: break;
}

header("location:$ruta_reportes,$archivo?$strParametros");
?>
ciFechaEnvio