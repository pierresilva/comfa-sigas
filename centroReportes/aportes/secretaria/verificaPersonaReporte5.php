<?php
/* autor:       Oswaldo Gonzalez
 * fecha:       09/08/2012
 * objetivo:    Verifica si una persona est� afiliada como trabajador o como empresa aportante
 */
ini_set("display_errors","1");
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$tipoDoc=$_REQUEST['tipoDoc']; 
$numDoc=$_REQUEST['numeroDoc'];

include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'p.persona.class.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'consultas'. DIRECTORY_SEPARATOR .'empresa'. DIRECTORY_SEPARATOR .'clases' . DIRECTORY_SEPARATOR . 'empresas.class.php';

$objPersona = new Persona;
$consulta = $objPersona->mostrar_registro_para($numDoc,1,$tipoDoc);
$resultadoPeticion = array();
$conr=0;
$data = array();
while($row=mssql_fetch_array($consulta)){
	$data[]=array_map("utf8_encode",$row);
	$conr++;
}
mssql_free_result($consulta);
if($conr>0){
	// Existe como afiliado activo
	$resultadoPeticion["persona"] = array("nombre" => $data[0]["pnombre"] ." ". $data[0]["snombre"] ." ". $data[0]["papellido"] ." ". $data[0]["sapellido"], "identificacion" => $data[0]["identificacion"]);
	$resultadoPeticion["estado"] = "activo";
	$resultadoPeticion["tipo"] = "Afiliado";
	echo json_encode($resultadoPeticion);
}else {
	$objEmpresa = new Empresa();
	$empresa = $objEmpresa->cargarEmpresa($numDoc);
	
	if(is_array($empresa)){
		$resultadoPeticion["persona"] = array("nombre" => $empresa["razonsocial"], "identificacion" => $empresa["nit"]);
		$resultadoPeticion["estado"] = "activo";
		$resultadoPeticion["tipo"] = "Independiente";
		echo json_encode($resultadoPeticion);
	}else{
		$resultadoPeticion["persona"] = null;
		$resultadoPeticion["estado"] = null;
		$resultadoPeticion["tipo"] = null;
		echo json_encode($resultadoPeticion);
	}
}
die();
?>