<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$usuario=$_SESSION['USUARIO'];


include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}
$tipo=$_REQUEST['tipo'];
$rept=$_REQUEST['tit'];

/** TITULO **/

switch($rept){	
	case 11:$titulo='Listado afiliados inactivos a fecha corte'; break;
	case 12:$titulo='Listado Beneficiarios 12 a&ntilde;os'; break;
	case 13:$titulo='Listado Trabajadores Tipo Afiliacion'; break;
	case 23:$titulo='Listado de pagos de Embargos'; break;
	case 25:$titulo='Listado Trabajadores Novedad Ingreso y Retiro'; break;
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Men&uacute; Reportes trabajadores</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link type="text/css" rel="stylesheet" href="../../../css/css.1.8/jquery-ui-1.8.17.custom.css" />
<link type="text/css" href="../../../css/Estilos.css" rel="stylesheet"/>
<link href="../../../css/marco.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="../../../js/1.6/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="../../../js/1.6/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="../../../js/comunes.js"></script>
<script type="text/javascript" src="js/reportes.js"></script>
<script>
$(function(){

	$("#txtPeriodo,#txtPeriodo2").datepicker({
        dateFormat: 'yymm',
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        onClose: function(dateText, inst) {
        	var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
            $(this).datepicker('setDate', new Date(year, month, 1));
            }
        }); 

	$("#txtPeriodo,#txtPeriodo2").focus(function () {
		$(".ui-datepicker-calendar").hide();
		$("#ui-datepicker-div").position({
			my: "center top",
			at: "center bottom",
			of: $(this)
		});
	});
	
});

</script>
</head>
<body>
	<center>
	<table width="100%" border="0">
	<tr>
	<td align="center" ><img src="../../../imagenes/logo_reporte.png" width="362" height="70"></td>
	</tr>
	<tr>
	<td align="center" ><?php echo $titulo ?></td>
	</tr>
	</table>
	<br>
<?php
/** CONTENIDO **/

if($tipo==11){	
?>
<div id="tipo11" style="display:block">
  <table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
	<tr>
    	<th colspan="4"><strong>Par&aacute;metros de Configuracion Reporte</strong></th>
    </tr>
	<tr>
		<td style="text-align:center" colspan="4" >
			Agencia:	    
	  	    <select id="cmbAgencia" name="cmbAgencia">
                <option value="T">Todas</option>
                <option value="01">Neiva</option>
                <option value="02">Garzon</option>
                <option value="03">Pitalito</option>
                <option value="04">La Plata</option>                                	  		
            </select>	
	  </td>	  
	</tr>
	<tr>
		<td style="text-align:center" >
			Fecha Inicial: 
		</td>
		<td style="text-align:center" >
			<input type="text" id="txtFecha" name="txtFecha" readonly>
		</td>
		<td style="text-align:center" >
			Fecha Final: 
		</td>
		<td style="text-align:center" >
			<input type="text" id="txtFecha2" name="txtFecha2" readonly>
		</td>
	</tr>
	<tr>
	  <td colspan="4">&nbsp;</td>
	  </tr>
	<tr>
	  <td colspan="4" style="text-align:center">
	  	<label style="cursor:pointer" onClick="procesar11(0);" id="idProcesar004"><img src="../../../imagenes/icono_pdf.png" width="32" height="32"/></label>
      	&nbsp;&nbsp;
      	<label style="cursor:pointer" onClick="procesar11(1);" id="idProcesar004"><img src="../../../imagenes/icono_excel.png" width="32" height="32"/></label>
	  </td>
	</tr>
  </table>
</div>
<?php
}
if($tipo==12){	
?>
<div id="tipo12" style="display:block">
  <table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
	<tr>
    	<th colspan="4"><strong>Par&aacute;metros de Configuracion Reporte</strong></th>
    </tr>
	<tr>
		<td style="text-align:center" colspan="4" >
			Agencia:	    
	  	    <select name="cmbAgencia" id="cmbAgencia">
		  				<option value="T">Todas</option>
			     		<?php 
			     			$sql = "select * from aportes500";
			     			$rs = $db->querySimple($sql);
			     			while($row=$rs->fetchObject()){
			     				echo "<option value='$row->codigo'>$row->agencia</option>";
			     			}
			     		?>			     		
		   </select>
	  </td>	  
	</tr>
	<tr>
		<td style="text-align:center">
			Fecha Inicial: 
		</td>
		<td>
			<input type="text" id="txtFecha" name="txtFecha" readonly>
		</td>
		<td style="text-align:center">
			Fecha Final: 
		</td>
		<td>
			<input type="text" id="txtFecha3" name="txtFecha3" readonly>
		</td>
	</tr>
	<tr>
		<td style="text-align:center">
			Periodo Aporte Inicial: 
		</td>
		<td>
			<input type="text" id="txtPeriodo" name="txtPeriodo" readonly>
		</td>
		<td style="text-align:center">
			Periodo Aporte Final: 
		</td>
		<td>
			<input type="text" id="txtPeriodo2" name="txtPeriodo2" readonly>
		</td>
	</tr>
	<tr>
	  <td colspan="4">&nbsp;</td>
	  </tr>
	<tr>
	  <td colspan="4" style="text-align:center">
	  	<label style="cursor:pointer" onClick="procesar10(0);" id="idProcesar004"><img src="../../../imagenes/icono_pdf.png" width="32" height="32"/></label>
      	&nbsp;&nbsp;
      	<label style="cursor:pointer" onClick="procesar10(1);" id="idProcesar004"><img src="../../../imagenes/icono_excel.png" width="32" height="32"/></label>
	  </td>
	</tr>
  </table>
</div>
<?php
}
if($tipo==13){	
?>
<div id="tipo11" style="display:block">
  <table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
	<tr>
    	<th colspan="4"><strong>Par&aacute;metros de Configuracion Reporte</strong></th>
    </tr>
	<tr>
		<td style="text-align:center" colspan="4" >
			Tipo Afiliacion:	    
	  	    <select name="cmbAfiliacion" id="cmbAfiliacion">
		  				<option value="T">Todos</option>
			     		<?php 
			     			$sql = "SELECT * FROM aportes091 WHERE iddefinicion=2";
			     			$rs = $db->querySimple($sql);
			     			while($row=$rs->fetchObject()){
			     				echo "<option value='$row->iddetalledef'>$row->detalledefinicion</option>";
			     			}
			     		?>			     		
		   </select>
	  </td>	  
	</tr>
	<tr>
		<td style="text-align:center" >
			Fecha Inicial: 
		</td>
		<td style="text-align:center" >
			<input type="text" id="txtFechaI" name="txtFechaI" readonly>
		</td>
		<td style="text-align:center" >
			Fecha Final: 
		</td>
		<td style="text-align:center" >
			<input type="text" id="txtFechaF" name="txtFechaF" readonly>
		</td>
	</tr>
	<tr>
	  <td colspan="4">&nbsp;</td>
	  </tr>
	<tr>
	  <td colspan="4" style="text-align:center">
	  	<label style="cursor:pointer" onClick="procesar13(0);" id="idProcesar004"><img src="../../../imagenes/icono_pdf.png" width="32" height="32"/></label>
      	&nbsp;&nbsp;
      	<label style="cursor:pointer" onClick="procesar13(1);" id="idProcesar004"><img src="../../../imagenes/icono_excel.png" width="32" height="32"/></label>
	  </td>
	</tr>
  </table>
</div>
<?php
}
if($tipo==18){
?>
    <div id="tipo18" style="display:block">
    <table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
	<tr>
    <th colspan="4"><strong>Par&aacute;metros de Configuracion Reporte</strong></th>
    </tr>
	<tr>
		<td>
			Periodo Inicial:
		</td>
		<td>
			<input type="text" id="txtPeriodo" name="txtPeriodo" readonly>
		</td>
		<td>
			Periodo Final:
		</td>
		<td>
			<input type="text" id="txtPeriodo2" name="txtPeriodo2" readonly>
		</td>  
	</tr>
	<tr>
	  <td colspan="4" style="text-align:center">
	  <label style="cursor:pointer" onClick="procesar18(1);"><img src="../../../imagenes/icono_pdf.png" width="32" height="32"></label>
      <label style="cursor:pointer" onClick="procesar18(2);"><img src="../../../imagenes/icono_excel.png" width="32" height="32"></label>
      </td>
	  </tr>
    </table>
<?php
}
if($tipo==19){
?>
    <div id="tipo19" style="display:block">
    <table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
	<tr>
    <th colspan="4"><strong>Par&aacute;metros de Configuracion Reporte</strong></th>
    </tr>
	<tr>
		<td style="text-align:center" >
			Fecha Inicial: 
		</td>
		<td style="text-align:center" >
			<input type="text" id="txtFechaI" name="txtFechaI" readonly>
		</td>
		<td style="text-align:center" >
			Fecha Final: 
		</td>
		<td style="text-align:center" >
			<input type="text" id="txtFechaF" name="txtFechaF" readonly>
		</td>
	</tr>
	<tr>
	  <td colspan="4" style="text-align:center">
	  <label style="cursor:pointer" onClick="procesar19(1);"><img src="../../../imagenes/icono_pdf.png" width="32" height="32"></label>
      <label style="cursor:pointer" onClick="procesar19(2);"><img src="../../../imagenes/icono_excel.png" width="32" height="32"></label>
      </td>
	  </tr>
    </table>

	<?php
	}
if($tipo==20){
	?>
    <div id="tipo20" style="display:block">
    <table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
	<tr>
    <th colspan="4"><strong>Par&aacute;metros de Configuracion Reporte</strong></th>
    </tr>
	<tr>
		<td style="text-align:center">Periodo:<input type="text" id="txtPeriodo" name="txtPeriodo" readonly>
		</td>
	</tr>
	<tr>
	  <td colspan="4" style="text-align:center">
	  <label style="cursor:pointer" onClick="procesar20(1);"><img src="../../../imagenes/icono_pdf.png" width="32" height="32"></label>
      <label style="cursor:pointer" onClick="procesar20(2);"><img src="../../../imagenes/icono_excel.png" width="32" height="32"></label>
      </td>
	  </tr>
    </table>
	<?php
	}
if($tipo==23){
	?>
    <div id="tipo18" style="display:block">
    <table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
	<tr>
    <th colspan="4"><strong>Par&aacute;metros de Configuracion Reporte</strong></th>
    </tr>
    <tr>
	    <td colspan="4" style="text-align:center">Giro
	      		<select id="txtTipo" name="txtTipo" class="box1" onchange="mostrar()";>
	          		<option value="A">Actual</option>
	          		<option value="H" selected>Historico</option>
	      		</select>
	  	</td>
  	</tr>
	<tr id="TRperiodo">
		<td>
			Periodo Inicial:
		</td>
		<td>
			<input type="text" id="txtPeriodo" name="txtPeriodo" readonly>
		</td>
		<td>
			Periodo Final:
		</td>
		<td>
			<input type="text" id="txtPeriodo2" name="txtPeriodo2" readonly>
		</td>  
	</tr>
	<tr>
	  <td colspan="4" style="text-align:center">
	  <label style="cursor:pointer" onClick="procesar23(1);"><img src="../../../imagenes/icono_pdf.png" width="32" height="32"></label>
      <label style="cursor:pointer" onClick="procesar23(2);"><img src="../../../imagenes/icono_excel.png" width="32" height="32"></label>
      </td>
	  </tr>
    </table>
	<?php
	}
	if($tipo==25){
		?>
	    <div id="tipo18" style="display:block">
	    <table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
		<tr>
	    <th colspan="4"><strong>Par&aacute;metros de Configuracion Reporte</strong></th>
	    </tr>
		<tr >
			<td>
				Fecha Inicial:
			</td>
			<td>
				<input type="text" id="txtFechaI" name="txtFechaI" readonly>
			</td>
			<td>
				Fecha Final:
			</td>
			<td>
				<input type="text" id="txtFechaF" name="txtFechaF" readonly>
			</td>  
		</tr>
		<tr>
		  <td colspan="4" style="text-align:center">
		  <label style="cursor:pointer" onClick="procesar25(1);"><img src="../../../imagenes/icono_pdf.png" width="32" height="32"></label>
	      <label style="cursor:pointer" onClick="procesar25(2);"><img src="../../../imagenes/icono_excel.png" width="32" height="32"></label>
	      </td>
		  </tr>
	    </table>
		<?php
		}
		?>
	
<input type="hidden" id="txtusuario" name="txtusuario" value="<?php echo $usuario; ?>" /> 
<input type="hidden" id="txtRept" name="txtRept" value="<?php echo $rept; ?>" />
</body>

</html>