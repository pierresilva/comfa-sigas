<?php
	/**
	 * NOTA: Si modificas un sentecia DML, tambien debes modificarla en la carta,
	 * 		para no tener inconsistencias. 
	 * 		GRACIAS.
	 * */
	set_time_limit(0);
	ini_set("display_errors",'1');
	date_default_timezone_set("America/Bogota");
	$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
	include_once  $root;
	include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
	include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
	include_once $raiz.DIRECTORY_SEPARATOR.'aportes'.DIRECTORY_SEPARATOR.'empresas'.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'empresas.class.php';
	
	$db = IFXDbManejador::conectarDB();
	if($db->conexionID==null){
		$cadena = $db->error;
		echo msg_error($cadena);
		exit();
	}
	$usuario = $_SESSION["USUARIO"];
	$objEmpresa = new Empresa();
	
	/**
	 * El objeto debe estar compuesto por: object("reporte"=>"","datos"=>array("indice"=>valor))
	 *  	donde:
	 *  		1. reporte: MODULO-REPORTE
	 * @var $arrDatosPost
	 */
	$arrDatosPost = $_REQUEST["objDatos"];
	//$arrDatosNotificacion = array("id_empresa"=>array(),"id_carta_notificacion"=>0,"id_estado"=>0);
	$arrDatosNotificacion = array("data_notificacion"=>array(),"id_carta_notificacion"=>0,"id_estado"=>0);
	
	//Array con los errores
	$arrError = array("Error"=>0,"Mensaje"=>null);
	
	/**************************************
	 * Obtener los datos de la notificacion
	 **************************************/
	
	switch($arrDatosPost["reporte"]){
		case "EMPRESA-Reporte033": //Carta notificación por suspension.
			//[4249][CARTA AVISO DE INCUMPLIMIENTO]
			$arrDatosNotificacion["id_carta_notificacion"] = 4249;
			$arrDatosNotificacion["id_estado"] = 0;
			
			$sql = "DECLARE @nit VARCHAR(12) = '{$arrDatosPost["datos"]["nit"]}'
						, @clase_aportante INT = '{$arrDatosPost["datos"]["clase_aportante"]}'
						, @fecha_suspensio_i DATE = '{$arrDatosPost["datos"]["fecha_suspensio_i"]}'
						,@fecha_suspensio_f DATE = '{$arrDatosPost["datos"]["fecha_suspensio_f"]}'
						, @fecha_sistema DATE = getdate()
						
					SELECT DISTINCT a48.idempresa
					FROM aportes048 a48
						INNER JOIN aportes052 a52 ON a52.id_empresa=a48.idempresa AND a52.estado='A'
					    LEFT JOIN aportes015 b15 ON b15.idpersona=a48.idrepresentante
					    LEFT JOIN aportes089 a89 ON a89.codmunicipio=a48.idciudad
					WHERE dateadd(d,10,a52.fecha_sistema)>=@fecha_sistema
						AND (@fecha_suspensio_i='' OR a52.fecha_sistema BETWEEN @fecha_suspensio_i AND @fecha_suspensio_f)
						AND (@nit='' OR a48.nit=@nit)
						AND (@clase_aportante='' OR a48.claseaportante=@clase_aportante)";
			
			$rsDatos = $db->querySimple($sql);
			while ($row=$rsDatos->fetch()){
				$arrDatosNotificacion["data_notificacion"][] = 
						array("id_empresa"=>$row["idempresa"],"informacion"=>"");
			}
			break;
		case "EMPRESA-Reporte035":
			//[4253][CARTA NOTIFICACION EXPULSION CONSEJO DIRECTIVO]
			$arrDatosNotificacion["id_carta_notificacion"] = 4253;
			$arrDatosNotificacion["id_estado"] = 0;
				
			$sql = "DECLARE @fecha_i DATE = '{$arrDatosPost["datos"]["fecha_i"]}'
						, @fecha_f DATE = '{$arrDatosPost["datos"]["fecha_f"]}'
						, @nit VARCHAR(12)='{$arrDatosPost["datos"]["nit"]}'
					SELECT DISTINCT
						a48.idempresa
					FROM aportes067 a67
						INNER JOIN aportes048 a48 ON a48.idempresa=a67.id_empresa
						LEFT JOIN aportes015 a15 ON a15.idpersona=a48.idrepresentante
					WHERE a67.estado='A' AND a48.estado='A' AND a48.codigoestado=4118 --[4118][EXPULSION]
						AND (@nit='' OR a48.nit=@nit)
						AND (@fecha_i='' OR a67.fecha_sistema BETWEEN @fecha_i AND @fecha_f)
						AND 1<(
								SELECT count(*)
								FROM aportes052 a52
							  		INNER JOIN aportes068 a68 ON a68.id_suspension=a52.id_suspension
								WHERE a68.id_desafiliacion=a67.id_desafiliacion AND a52.estado='A'
							)";
				
			$rsDatos = $db->querySimple($sql);
			while ($row=$rsDatos->fetch()){
				//$arrDatosNotificacion["id_empresa"][]=$row["idempresa"];
				$arrDatosNotificacion["data_notificacion"][] =
						array("id_empresa"=>$row["idempresa"],"informacion"=>"");
			}
			break;
		case "EMPRESA-Reporte036":
				//[4254][CARTA NOTIFICACION DE EXPULSION]
				$arrDatosNotificacion["id_carta_notificacion"] = 4254;
				$arrDatosNotificacion["id_estado"] = 0;
			
				$sql = "DECLARE @fecha_i DATE = '{$arrDatosPost["datos"]["fecha_i"]}'
							, @fecha_f DATE = '{$arrDatosPost["datos"]["fecha_f"]}'
							, @nit VARCHAR(12) = '{$arrDatosPost["datos"]["nit"]}'
						SELECT DISTINCT
							a48.idempresa
						FROM aportes067 a67
							INNER JOIN aportes048 a48 ON a48.idempresa=a67.id_empresa
							LEFT JOIN aportes015 a15 ON a15.idpersona=a48.idrepresentante
							LEFT JOIN aportes089 a89 ON a89.codmunicipio=a48.idciudad
						WHERE a67.estado='A' AND a48.estado='I' AND a48.codigoestado=4118--[4118][EXPULSION]
							AND (@nit='' OR a48.nit=@nit)
							AND (@fecha_i='' OR a48.fechaestado BETWEEN @fecha_i AND @fecha_f)";
			
				$rsDatos = $db->querySimple($sql);
				while ($row=$rsDatos->fetch()){
					//$arrDatosNotificacion["id_empresa"][]=$row["idempresa"];
					$arrDatosNotificacion["data_notificacion"][] =
							array("id_empresa"=>$row["idempresa"],"informacion"=>"");
				}
			break;
		/*case "PROMOTORIA-Reporte005":
			//[4250][CARTA LIQUIDACION DE AFORO]
			$arrDatosNotificacion["id_carta_notificacion"] = 4250;
			$arrDatosNotificacion["id_estado"] = 0;
		
			$sql = "DECLARE @id_visita VARCHAR(15)='{$arrDatosPost["datos"]["id_visita"]}'
						, @fechaI DATE='{$arrDatosPost["datos"]["fechaI"]}'
						, @fechaF DATE='{$arrDatosPost["datos"]["fechaF"]}'
						, @nit VARCHAR(12) = '{$arrDatosPost["datos"]["nit"]}'
						, @id_agencia INT = '{$arrDatosPost["datos"]["id_agencia"]}'
				SELECT DISTINCT a48.idempresa, isnull(a302.consecutivo,'') AS consecutivo
				FROM aportes302 a302
				    INNER JOIN aportes301 a301 ON a301.idpromotor=a302.idpromotor
				    INNER JOIN aportes015 a15 ON a15.idpersona=a301.idpersona
				    INNER JOIN aportes048 a48 ON a48.idempresa=a302.idempresa
				    INNER JOIN aportes015 b15 ON b15.idpersona=a48.idrepresentante
				    INNER JOIN aportes305 a305 ON a305.idvisita=a302.idvisita
				    INNER JOIN aportes303 a303 ON a303.idliquidacion=a305.idliquidacion
				WHERE a302.estado='A' AND a303.cancelado='N'
					AND ( @id_visita='' OR a302.idvisita=@id_visita)
					AND ( @nit='' OR a48.nit=@nit)
					AND ( @fechaI='' OR fechavisita BETWEEN @fechaI AND @fechaF)
					AND ( @id_agencia='' OR a302.idagencia=@id_agencia)";
		
			$rsDatos = $db->querySimple($sql);
			while ($row=$rsDatos->fetch()){
				//$arrDatosNotificacion["id_empresa"][]=$row["idempresa"];
				$informacion = "Liquidacion de aforo #".$row["consecutivo"];
				$arrDatosNotificacion["data_notificacion"][] =
						array("id_empresa"=>$row["idempresa"],"informacion"=>$informacion);
			}
			break;
		case "PROMOTORIA-Reporte007":
			//[4252][CARTA CONVENIOS DE PAGO]
			$arrDatosNotificacion["id_carta_notificacion"] = 4252;
			$arrDatosNotificacion["id_estado"] = 0;
		
			$sql = "
				DECLARE @nit VARCHAR(12) = '{$arrDatosPost["datos"]["nit"]}'
						, @fecha_convenio_i DATE = '{$arrDatosPost["datos"]["fechaI"]}'
						, @fecha_convenio_f DATE = '{$arrDatosPost["datos"]["fechaI"]}'
						, @id_agencia INT = '{$arrDatosPost["datos"]["id_agencia"]}'
				SELECT DISTINCT
					a48.idempresa
				FROM aportes306 a306
					INNER JOIN aportes307 a307 ON a307.idconvenio=a306.idconvenio
					INNER JOIN aportes308 a308 ON a308.iddetalleconvenio=a307.iddetalleconvenio
					INNER JOIN aportes303 a303 ON a303.iddetalle=a308.idperiodoliquidacion
					INNER JOIN aportes305 a305 ON a305.idliquidacion=a303.idliquidacion
					INNER JOIN aportes302 a302 ON a302.idvisita=a305.idvisita
					INNER JOIN aportes048 a48 ON a48.idempresa=a302.idempresa
					INNER JOIN aportes015 a15 ON a15.idpersona=a48.idrepresentante
					INNER JOIN aportes519 a519 ON a519.usuario=a306.usuario
				WHERE (@nit = '' OR a48.nit=@nit)
					AND (@fecha_convenio_i='' OR a306.fechasistema BETWEEN @fecha_convenio_i AND @fecha_convenio_f)
					AND (@id_agencia='' OR a302.idagencia=@id_agencia)";
		
			$rsDatos = $db->querySimple($sql);
			while ($row=$rsDatos->fetch()){
				//$arrDatosNotificacion["id_empresa"][]=$row["idempresa"];
				$arrDatosNotificacion["data_notificacion"][] =
						array("id_empresa"=>$row["idempresa"],"informacion"=>"");
			}
			break;
		case "PROMOTORIA-Reporte013":
			//[4251][CARTA NOTIFICACION COBRO PERSUASIVO]
			$arrDatosNotificacion["id_carta_notificacion"] = 4251;
			$arrDatosNotificacion["id_estado"] = 0;
			
			$sql = "DECLARE @id_visita VARCHAR(15)='{$arrDatosPost["datos"]["id_visita"]}'
						, @fechaI DATE='{$arrDatosPost["datos"]["fechaI"]}'
						, @fechaF DATE='{$arrDatosPost["datos"]["fechaF"]}'
						, @nit VARCHAR(12)='{$arrDatosPost["datos"]["nit"]}'
						, @numero_reitero INT = '{$arrDatosPost["datos"]["numero_reitero"]}'
						, @id_agencia INT = '{$arrDatosPost["datos"]["id_agencia"]}'
						
					SELECT DISTINCT	a48.idempresa
					FROM aportes302 a302
					    INNER JOIN aportes305 a305 ON a305.idvisita=a302.idvisita
					    INNER JOIN aportes301 a301 ON a301.idpromotor=a302.idpromotor
					    INNER JOIN aportes015 a15 ON a15.idpersona=a301.idpersona
					    INNER JOIN aportes048 a48 ON a48.idempresa=a302.idempresa
					    INNER JOIN aportes015 b15 ON b15.idpersona=a48.idrepresentante
					WHERE a302.estado='A'
						--Verificar que no tenga convenio
						AND 0=(
							SELECT COUNT(*)
							FROM aportes303 a303
								INNER JOIN aportes308 a308 ON a308.idperiodoliquidacion=a303.iddetalle
								INNER JOIN aportes307 a307 ON a307.iddetalleconvenio=a308.iddetalleconvenio
								INNER JOIN aportes306 a306 ON a306.idconvenio=a307.idconvenio
							WHERE a303.idliquidacion=a305.idliquidacion AND a306.estado='A'
						)
						AND (@id_visita='' OR a302.idvisita=@id_visita)
						AND (@fechaI='' OR a302.fechavisita BETWEEN @fechaI AND @fechaF)
						AND (@nit='' OR a48.nit=@nit)
						AND (@numero_reitero='' OR a302.numeroreitero=@numero_reitero)
						AND (@id_agencia='' OR a302.idagencia=@id_agencia)";
			
			$rsDatos = $db->querySimple($sql);
			while ($row=$rsDatos->fetch()){
				//$arrDatosNotificacion["id_empresa"][]=$row["idempresa"];
				$arrDatosNotificacion["data_notificacion"][] =
						array("id_empresa"=>$row["idempresa"],"informacion"=>"");
			}
			break;*/
	}
	
	/**************************************************
	 * Almacenamiento del control de las notificaciones
	 **************************************************/
	
	if(count($arrDatosNotificacion["data_notificacion"])>0 
			&& $arrDatosNotificacion["id_carta_notificacion"]>0){

		$db->inicioTransaccion();
		$banderaError = 0;
		$banderaArray = array("id_empresa"=>0
				,"id_carta_notificacion"=>$arrDatosNotificacion["id_carta_notificacion"]
				,"id_estado"=>$arrDatosNotificacion["id_estado"]
				,"informacion"=>""
				,"contenido_carta"=>"");
		
		//Almacenar la notificacion
		foreach ($arrDatosNotificacion["data_notificacion"] as $dataNotificacion){
			$banderaArray["id_empresa"] = $dataNotificacion["id_empresa"];
			$banderaArray["informacion"] = $dataNotificacion["informacion"];
			
			if($objEmpresa->guardar_notificacion($banderaArray,$usuario)==0){
				$banderaError++;
				break;
			}
		}
		
		//Control de error
		if($banderaError==0){
			//Ok
			$db->confirmarTransaccion();
			$arrError["Error"] = 0;
		}else{
			//Error
			$db->cancelarTransaccion();
			$arrError["Error"] = 1;
			$arrError["Mensaje"] = "Error interno, favor comunicarse con T.I. CONTROL NOTIFICACION";
		}
	}else{
		//No hay empresas
		$arrError["Error"] = 1;
		$arrError["Mensaje"] = "No hay empresa que cumplan con la condicion. CONTROL NOTIFICACION";
	}
	
	echo json_encode($arrError);	
?>