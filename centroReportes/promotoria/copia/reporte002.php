<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

if ( !isset( $_SESSION ['USUARIO'] ) ) {session_start();}
$user=$_SESSION['USUARIO'];
//$raiz=$_SESSION['RAIZ'];
//chdir($raiz);
include_once("../../config.php");

$id=$_REQUEST['idempresa'];
$class = new Java("java.lang.Class");
$rgIfx = $class->forName("com.informix.jdbc.IfxDriver");
$rgIfx->newInstance();

//Tipo de dato de los parametros
$idempresa= new Java("java.lang.Long",$id);
$usuario= new Java("java.lang.String",$user);

$dMang = new Java("java.sql.DriverManager");
$conn = $dMang->getConnection($conexion_ifx);
$compileManager = new Java("net.sf.jasperreports.engine.JasperCompileManager");
$rp = realpath("reporte002.jrxml");

$report = $compileManager->compileReport($rp);
$fillManager = new Java("net.sf.jasperreports.engine.JasperFillManager");
$parametro = new Java("java.util.HashMap");

//Parametros del reporte
$parametro->put("idempresa",$idempresa);
$parametro->put("usuario",$usuario);
//$emptyDataSource = new Java("net.sf.jasperreports.engine.JREmptyDataSource");
$jasperPrint = $fillManager->fillReport($report, $parametro, $conn);
$exportManager = new Java("net.sf.jasperreports.engine.JasperExportManager");
$outputPath = realpath(".")."/"."reporte002.pdf";
$exportManager->exportReportfirstdfFile($jasperPrint, $outputPath);

if (file_exists("reporte002.pdf")){
header("Content-disposition: attachment; filename=reporte002.pdf");
header("Content-Type: application/pdf");
header("Content-Transfer-Encoding: binary");
header("Content-Length: ". @filesize("reporte002.pdf"));
header("Pragma: no-cache");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Expires: 0");
set_time_limit(0);
@readfile("reporte002.pdf") or die("problem occurs.");
}

?>