<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

if ( !isset( $_SESSION ['USUARIO'] ) ) {session_start();}
$user=$_SESSION['USUARIO'];
//$raiz=$_SESSION['RAIZ'];
//chdir($raiz);
include_once("../../config.php");

$id=$_REQUEST['idempresa'];
$class = new Java("java.lang.Class");
$rgIfx = $class->forName("com.informix.jdbc.IfxDriver");
$rgIfx->newInstance();

$usuario= new Java("java.lang.String",$user);

$dMang = new Java("java.sql.DriverManager");
$conn = $dMang->getConnection($conexion_ifx);
$compileManager = new Java("net.sf.jasperreports.engine.JasperCompileManager");
$rp = realpath("reporte004.jrxml");

$report = $compileManager->compileReport($rp);
$fillManager = new Java("net.sf.jasperreports.engine.JasperFillManager");
$parametro = new Java("java.util.HashMap");

//Parametros del reporte
$parametro->put("usuario",$usuario);
//$emptyDataSource = new Java("net.sf.jasperreports.engine.JREmptyDataSource");
$jasperPrint = $fillManager->fillReport($report, $parametro, $conn);
$exportManager = new Java("net.sf.jasperreports.engine.JasperExportManager");
$outputPath = realpath(".")."/"."reporte004.pdf";
$exportManager->exportReportfirstdfFile($jasperPrint, $outputPath);

if (file_exists("reporte004.pdf")){
header("Content-disposition: attachment; filename=reporte004.pdf");
header("Content-Type: application/pdf");
header("Content-Transfer-Encoding: binary");
header("Content-Length: ". @filesize("reporte004.pdf"));
header("Pragma: no-cache");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Expires: 0");
set_time_limit(0);
@readfile("reporte004.pdf") or die("problem occurs.");
}

?>