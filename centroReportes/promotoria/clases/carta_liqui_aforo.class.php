<?php
set_time_limit(0);
date_default_timezone_set("America/Bogota");
ini_set('memory_limit', '1000M');

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
include_once $raiz.DIRECTORY_SEPARATOR.'aportes'.DIRECTORY_SEPARATOR.'empresas'.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'empresas.class.php';
include_once $raiz.DIRECTORY_SEPARATOR.'parametrizacion'.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'modelo_documento_html.class.php';
include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'utilitario.class.php';

class CartaLiquiAforo{
	
	private static $conPDO = null;
	
	private $idCarta = 4250; //[4250][CARTA LIQUIDACION DE AFORO]
	private $modeloDocumento = "CARTA_LIQUI_AFORO";
	private $arrResultado = null;
	private $arrFiltro = null;
	
	private $fechaSistema;
	
	private $objModeloDocumentoHtml = null;
	
	function __construct(){
		try{
			self::$conPDO = IFXDbManejador::conectarDB();
			if( self::$conPDO->conexionID==null ){
				throw new Exception(self::$conPDO->error);
			}
		}catch(Exception $e){
			echo $e->getMessage();
			exit();
		}
		
		$meses = array(
				"Enero"
				,"Febrero"
				,"Marzo"
				,"Abril"
				,"Mayo"
				,"Junio"
				,"Julio"
				,"Agosto"
				,"Septiembre"
				,"Octubre"
				,"Noviembre"
				,"Diciembre");
		
		$this->fechaSistema = date("d") . " " . $meses[date("n")-1] . " " . date("Y");
		$this->objModeloDocumentoHtml = new ModeloDocumentoHtml();
	}
	
	/**
	 * Metodo encargado de preparar el contenido de las cartas
	 *  
	 * @param unknown_type $arrFiltro
	 */
	public function fetch_carta($arrFiltro){
		$this->arrFiltro = $arrFiltro;
		
		$this->preparar_datos();
		
		return $this->arrResultado;
	}
	
	private function fetch_datos_visita(){
		$arrData = array();
		$query = "DECLARE @id_visita VARCHAR(15)='{$this->arrFiltro["id_visita"]}'
					, @fechaI DATE='{$this->arrFiltro["fecha_inicial"]}'
					, @fechaF DATE='{$this->arrFiltro["fecha_final"]}'
					, @nit VARCHAR(12) = '{$this->arrFiltro["nit"]}'
					, @id_agencia INT = '{$this->arrFiltro["id_agencia"]}'
				
				SELECT
				    a302.idempresa,a302.idvisita
				    , CONVERT(varchar,a302.consecutivo) + '-' + SUBSTRING(a15.pnombre,1,1) + '' + SUBSTRING(a15.papellido,1,1) AS consecutivo
				    , a48.razonsocial,a48.nit,a48.direccion,a48.telefono
				    , (SELECT TOP 1 a89.municipio FROM aportes089 a89 WHERE a89.codmunicipio=a48.idciucorresp)  AS ciudad_corre_empre
				    , b15.pnombre + ' ' + isnull(b15.snombre,'') + ' ' + b15.papellido + ' ' + isnull(b15.sapellido,'') AS representante
					,(
				    	SELECT '-' + substring(a303.periodo,1,4)
						FROM aportes305 a305
						    INNER JOIN aportes303 a303 ON a303.idliquidacion=a305.idliquidacion
						WHERE a305.idvisita = a302.idvisita AND a303.cancelado='N'
						GROUP BY substring(a303.periodo,1,4)
						ORDER BY substring(a303.periodo,1,4) ASC
						FOR xml path('')
					) AS anno
				
					,(
				    	SELECT CONVERT(VARCHAR,a305.idliquidacion) + ', '
						FROM aportes305 a305
						    INNER JOIN aportes303 a303 ON a303.idliquidacion=a305.idliquidacion
						WHERE a305.idvisita = a302.idvisita AND a303.cancelado='N'
						GROUP BY a305.idliquidacion
						ORDER BY a305.idliquidacion ASC
						FOR xml path('')
					) AS liquidaciones
				
					,(
				    	SELECT a305.periodoletra + ', '
						FROM aportes305 a305
						    --INNER JOIN aportes303 a303 ON a303.idliquidacion=a305.idliquidacion
						WHERE a305.idvisita = a302.idvisita --AND a303.cancelado='N'
						--GROUP BY a305.periodoletra
						FOR xml path('')
					) AS periodos
				
					,(SELECT TOP 1 documentacion FROM aportes305 WHERE idvisita=a302.idvisita ORDER BY 1 DESC) AS documentacion
					,(SELECT TOP 1 indicador FROM aportes305 WHERE idvisita=a302.idvisita ORDER BY 1 DESC) AS indicador
				
					,(	SELECT TOP 1 CASE a91.codigo
								WHEN '1' THEN 'personalmente'
							    WHEN '2' THEN 'por email'
							    WHEN '3' THEN 'por correo certificado'
							END
						FROM aportes305 b305
							INNER JOIN aportes091 a91 ON a91.iddetalledef=b305.idtiponotificacion
						WHERE idvisita=a302.idvisita ORDER BY 1 DESC
					) AS notificacion
				
					,sum(a303.neto-isnull(a303.abono,0)) AS total
				FROM aportes302 a302
				    INNER JOIN aportes301 a301 ON a301.idpromotor=a302.idpromotor
				    INNER JOIN aportes015 a15 ON a15.idpersona=a301.idpersona
				    INNER JOIN aportes048 a48 ON a48.idempresa=a302.idempresa
				    INNER JOIN aportes015 b15 ON b15.idpersona=a48.idrepresentante
				    INNER JOIN aportes305 a305 ON a305.idvisita=a302.idvisita
				    INNER JOIN aportes303 a303 ON a303.idliquidacion=a305.idliquidacion

				--[2848]['EVASORA']
				--[2849]['ELUSORA']
				WHERE a302.estado='A' AND a303.cancelado='N' AND isnull(a302.idcausal,0) NOT IN(2848,2849) 
					AND ( @id_visita='' OR a302.idvisita=@id_visita)
					AND ( @fechaI='' OR fechavisita BETWEEN @fechaI AND @fechaF)
					AND ( @nit='' OR a48.nit=@nit)
					AND (@id_agencia='' OR a302.idagencia=@id_agencia)
				GROUP BY a302.idempresa,a302.idvisita
				    , CONVERT(varchar,a302.consecutivo) + '-' + SUBSTRING(a15.pnombre,1,1) + '' + SUBSTRING(a15.papellido,1,1)
				    , a48.razonsocial,a48.nit,a48.direccion,a48.telefono, a48.idciucorresp
				    , b15.pnombre + ' ' + isnull(b15.snombre,'') + ' ' + b15.papellido + ' ' + isnull(b15.sapellido,'')
				ORDER BY a302.idvisita";
		
		$arrData = Utilitario::fetchConsulta($query, self::$conPDO);
		return $arrData; 
	}
	
	/**
	 * Obtener la plantilla de la carta en formato Html
	 * @return string
	 */
	private function fetch_plantilla_carta(){
		
		$contenidoCarta = "";
		
		$arrDatos = $this->objModeloDocumentoHtml->fetch_modelo_documento(array("modelo_documento"=>$this->modeloDocumento));
		
		if(count($arrDatos)>0){
			$contenidoCarta = $arrDatos[0]["contenido_html"];
		}
		
		return $contenidoCarta;
	}
	
	private function preparar_datos(){
		
		$plantillaCarta = $this->fetch_plantilla_carta();
		
		//Obtener los datos de la visita
		$arrDatosVisita = $this->fetch_datos_visita();
		
		//Recorrer las visitas
		foreach($arrDatosVisita as $datosVisita){
			
			$datosVisita["fecha_actual"] = $this->fechaSistema;
			$datosVisita["salario_base"] = 0;
			$datosVisita["anio_gravable"] = "";
			$datosVisita["comunicacion"] = $this->arrFiltro["comunicacion"];
			$datosVisita["usuario"] = $this->arrFiltro["usuario"];
			
			$datosVisita["anno"] = trim($datosVisita["anno"],'-');
			$datosVisita["salario_base"] = $datosVisita["total"]/$datosVisita["indicador"]*100;
			$datosVisita["periodos"] = trim($datosVisita["periodos"],',');
			$datosVisita["liquidaciones"] = trim($datosVisita["liquidaciones"],',');
			
			$contenidoCarta = $this->preparar_contenido_html($plantillaCarta,$datosVisita);
			$informacion = "Liquidacion de aforo #".$datosVisita["consecutivo"];
			
			$this->arrResultado[] = array(
					"id_carta_notificacion"=>$this->idCarta
					,"id_empresa"=>$datosVisita["idempresa"]
					,"informacion"=>$informacion
					,"id_estado"=>0
					,"contenido_carta"=>$contenidoCarta);
		}
	}
	
	private function preparar_contenido_html($plantillaHtml, $datos){
		
		$arrIndiceModelo = array(
				"comunicacion"=>"comunicacion",
				"consecutivo"=>"consecutivo",
				"fecha_actual"=>"fecha_actual",
				"razonsocial"=>"razonsocial",
				"nit"=>"nit",
				"direccion"=>"direccion",
				"ciudad_corre_empre"=>"ciudad_corre_empre",
				"telefono"=>"telefono",
				"representante"=>"representante",
				"anio_gravable"=>"anno",
		
				"salario_base_nform"=>"salario_base",
				"indicador"=>"indicador",
				"periodos"=>"periodos",
				"total_nform"=>"total",
		
				"idvisita"=>"idvisita",
				"documentacion_lower"=>"documentacion",
				"liquidaciones"=>"liquidaciones",
				"notificacion"=>"notificacion",
				"usuario"=>"usuario",
		);
		
		//Datos referencia uno
		$plantillaHtml = Util::reempDatosPlant($plantillaHtml,$arrIndiceModelo,$datos);
		
		return $plantillaHtml;
	}
}

?>