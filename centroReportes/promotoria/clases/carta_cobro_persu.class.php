<?php
set_time_limit(0);
date_default_timezone_set("America/Bogota");
ini_set('memory_limit', '1000M');

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
include_once $raiz.DIRECTORY_SEPARATOR.'config.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
include_once $raiz.DIRECTORY_SEPARATOR.'aportes'.DIRECTORY_SEPARATOR.'empresas'.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'empresas.class.php';
include_once $raiz.DIRECTORY_SEPARATOR.'parametrizacion'.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'modelo_documento_html.class.php';
include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'utilitario.class.php';

class CartaCobroPersu{
	
	private static $conPDO = null;
	
	private $idCarta = 4251; //[4251][CARTA NOTIFICACION COBRO PERSUASIVO]
	private $modeloDocumento = "";
	private $arrResultado = null;
	private $arrFiltro = null;
	
	private $fechaSistema;
	
	private $objModeloDocumentoHtml = null;
	
	function __construct(){
		try{
			self::$conPDO = IFXDbManejador::conectarDB();
			if( self::$conPDO->conexionID==null ){
				throw new Exception(self::$conPDO->error);
			}
		}catch(Exception $e){
			echo $e->getMessage();
			exit();
		}
		
		$meses = array(
				"Enero"
				,"Febrero"
				,"Marzo"
				,"Abril"
				,"Mayo"
				,"Junio"
				,"Julio"
				,"Agosto"
				,"Septiembre"
				,"Octubre"
				,"Noviembre"
				,"Diciembre");
		
		$this->fechaSistema = date("d") . " " . $meses[date("n")-1] . " " . date("Y");
		$this->objModeloDocumentoHtml = new ModeloDocumentoHtml();
	}
	
	/**
	 * Metodo encargado de preparar el contenido de las cartas
	 *  
	 * @param unknown_type $arrFiltro
	 */
	public function fetch_carta($arrFiltro){
		$this->arrFiltro = $arrFiltro;		
		
		$this->preparar_datos();
		
		return $this->arrResultado;
	}
	
	private function fetch_datos_visita(){
		$arrData = array();
		$query = "DECLARE 
					@id_visita VARCHAR(15)='{$this->arrFiltro["id_visita"]}'
					, @fechaI DATE='{$this->arrFiltro["fechaI"]}'
					, @fechaF DATE='{$this->arrFiltro["fechaF"]}'
					, @nit VARCHAR(12)='{$this->arrFiltro["nit"]}'
					, @numero_reitero INT='{$this->arrFiltro["numero_reitero"]}'
					, @id_agencia INT='{$this->arrFiltro["id_agencia"]}'
				
				SELECT DISTINCT
					a302.idvisita
					, isnull(a302.numeroreitero,0) AS numeroreiteroint
					, CONVERT(varchar,a302.consecutivo) + '-' + SUBSTRING(a15.pnombre,1,1) + '' + SUBSTRING(a15.papellido,1,1) AS consecutivo
				    , a48.idempresa, a48.razonsocial,a48.nit,a48.direccion,a48.telefono
				    , b15.pnombre + ' ' + isnull(b15.snombre,'') + ' ' + b15.papellido + ' ' + isnull(b15.sapellido,'') AS representante
				    , c15.pnombre + ' ' + isnull(c15.snombre,'') + ' ' + c15.papellido + ' ' + isnull(c15.sapellido,'') AS asesor_juridico
				    , a89.municipio
				
				   	, (SELECT sum(a303.neto-isnull(a303.abono,0))
					   FROM aportes305 a305
					       INNER JOIN aportes303 a303 ON a303.idliquidacion=a305.idliquidacion
					   WHERE a305.idvisita=a302.idvisita
					) AS total
				
					,isnull((SELECT b305.periodonumero+', '
							FROM aportes305 b305
					   		WHERE b305.idvisita=a302.idvisita
					   		ORDER BY b305.idliquidacion
					   		FOR xml path('')
					),'')AS periodos
				FROM aportes302 a302
				    INNER JOIN aportes305 a305 ON a305.idvisita=a302.idvisita
				    INNER JOIN aportes301 a301 ON a301.idpromotor=a302.idpromotor
				    INNER JOIN aportes015 a15 ON a15.idpersona=a301.idpersona
				    INNER JOIN aportes048 a48 ON a48.idempresa=a302.idempresa
				    INNER JOIN aportes015 b15 ON b15.idpersona=a48.idrepresentante
				    LEFT JOIN aportes089 a89 ON a89.codmunicipio=a48.idciudad
					LEFT JOIN aportes300 a300 ON a300.idasesor=a302.idabogadointerno
					LEFT JOIN aportes015 c15 ON c15.idpersona=a300.idpersona
				WHERE a302.estado='A'
					/*Verificar que no tenga convenio*/
					AND 0=(
						SELECT COUNT(*)
						FROM aportes303 a303
							INNER JOIN aportes308 a308 ON a308.idperiodoliquidacion=a303.iddetalle
							INNER JOIN aportes307 a307 ON a307.iddetalleconvenio=a308.iddetalleconvenio
							INNER JOIN aportes306 a306 ON a306.idconvenio=a307.idconvenio
						WHERE a303.idliquidacion=a305.idliquidacion AND a306.estado='A'
					)
					/*Verificar que la causal sea diferente a EVASORA, ELUSORA O MORA PRESUNTA*/
					AND isnull(a302.idcausal,0) NOT IN (
						SELECT a91.iddetalledef
						FROM aportes090 a90
							INNER JOIN aportes091 a91 ON a91.iddefinicion=a90.iddefinicion
						WHERE a90.codigo='CAUSAL_VISITA' AND a91.codigo IN ('1','2','5')
					)
					
					AND (@id_visita='' OR a302.idvisita=@id_visita)
					AND (@fechaI='' OR a302.fechavisita BETWEEN @fechaI AND @fechaF)
					AND (@nit='' OR a48.nit=@nit)
					AND (@numero_reitero='' OR a302.numeroreitero=@numero_reitero)
					AND (@id_agencia='' OR a302.idagencia=@id_agencia)
				ORDER BY a48.nit";
		
		$arrData = Utilitario::fetchConsulta($query, self::$conPDO);
		return $arrData; 
	}
	
	/**
	 * Obtener la plantilla de la carta en formato Html
	 * @return string
	 */
	private function fetch_plantilla_carta(){
		
		$contenidoCarta = "";
		
		$arrDatos = $this->objModeloDocumentoHtml->fetch_modelo_documento(array("modelo_documento"=>$this->modeloDocumento));
		
		if(count($arrDatos)>0){
			$contenidoCarta = $arrDatos[0]["contenido_html"];
		}
		
		return $contenidoCarta;
	}
	
	private function preparar_datos(){
		
		//Obtener los datos de la visita
		$arrDatosVisita = $this->fetch_datos_visita();
		
		//Obtener los modelos de las cartas
		$this->modeloDocumento = "CARTA_COBRO_PERSU_PRIME_REITE";
		$plantCartaPrimeReite = $this->fetch_plantilla_carta();
	
		$this->modeloDocumento = "CARTA_COBRO_PERSU_SEGUN_REITE";
		$plantCartaSegunReite = $this->fetch_plantilla_carta();
		
		//Recorrer las visitas
		foreach($arrDatosVisita as $datos){
			
			if($datos["numeroreiteroint"]==2)
				$plantillaCarta = $plantCartaSegunReite;
			else 
				$plantillaCarta = $plantCartaPrimeReite;
			
			$datos["periodos"] = trim($datos["periodos"],",");
			$datos["fecha_sistema"] = $this->fechaSistema;
			$datos["usuario"] = $this->arrFiltro["usuario"];
			$datos["comunicacion"] = $this->arrFiltro["comunicacion"];
			
			$contenidoCarta = $this->preparar_contenido_html($plantillaCarta,$datos);
			$informacion = "No Reitero ".$datos["numeroreiteroint"];
			
			$this->arrResultado[] = array(
					"id_carta_notificacion"=>$this->idCarta
					,"id_empresa"=>$datos["idempresa"]
					,"informacion"=>$informacion
					,"id_estado"=>0
					,"contenido_carta"=>$contenidoCarta);
		}
	}
	
	private function preparar_contenido_html($plantillaHtml, $datos){
		
		$arrIndiceModelo = array(
				"fecha_sistema"=>"fecha_sistema",
				"comunicacion"=>"comunicacion",
				"representante"=>"representante",
				"razonsocial"=>"razonsocial",
				"nit"=>"nit",
				"direccion"=>"direccion",
				"telefono"=>"telefono",
				"municipio"=>"municipio",
				
				"consecutivo"=>"consecutivo",
				"total_nform"=>"total",
				"periodos"=>"periodos",
				"numeroreitero"=>"numeroreiteroint",
				"usuario"=>"usuario"
		);
		
		//Datos referencia uno
		$plantillaHtml = Util::reempDatosPlant($plantillaHtml,$arrIndiceModelo,$datos);
		
		return $plantillaHtml;
	}
}

?>