<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<title>Men&uacute; Reportes de promotoria</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link href="../../css/estiloReporte.css" rel="stylesheet" type="text/css">
		<script src="../../js/comunes.js" type="text/javascript"></script>
	</head>
	<body>
		<center>
	  		<table width="100%" border="0">
	  			<tr>
	    			<td align="center" ><img src="../../imagenes/razonSocial.png" width="615" height="55"></td>
	  			</tr>
	  			<tr>
	    			<td align="center" >&nbsp;</td>
	  			</tr>
	  			<tr>
	    			<td class="fecha" ></td>
	  			</tr>
			</table>
			<table width="60%" border="0" cellspacing="1" class="tablero">
				<tr><th>Reporte</th>
					<th>Listado de Reportes de Promotor&iacute;a</th></tr>  
		  		<tr>
		    		<td scope="row" align="left">Reporte001</td>
		   	 		<td align="left"><a href="configReportePromotoria.php?tipo=1&tit=1">Listado de visitas por empresas.</a></td>
		  		</tr>
	 			<tr>
		  			<td scope="row" align="left" width="20%">Reporte002</td>
		  			<td align="left"><a href="configReportePromotoria.php?tipo=2&tit=2">Listado de liquidaciones por empresa.</a></td>
				</tr>
		 		<tr>
		   			<td scope="row" align="left">Reporte003</td>
		   			<td align="left"><a href="configReportePromotoria.php?tipo=3&tit=3">Reporte mensual de producción por promotor.</a></td>
	    		</tr>
		 		<tr>
		   			<td scope="row" align="left">Reporte004</td>
		   			<td align="left"><a href="configReportePromotoria.php?tipo=4&tit=4">Reporte de la auditoria del periodo liquidado.</a></td>
	    		</tr>
	    		<tr>
		   			<td scope="row" align="left">Reporte005</td>
		   			<td align="left"><a href="configReportePromotoria.php?tipo=5&tit=5">Carta liquidaci&oacute;n de aforo.</a></td>
	    		</tr>
	    		<tr>
		   			<td scope="row" align="left">Reporte006</td>
		   			<td align="left">&nbsp;</td>
	    		</tr>
				<tr>
		   			<td scope="row" align="left">Reporte007</td>
		   			<td align="left"><a href="configReportePromotoria.php?tipo=7&tit=7">Carta de los convenios de pago de aportes parafiscales.</a></td>
	    		</tr>
	    		<tr>
		   			<td scope="row" align="left">Reporte008</td>
		   			<td align="left"><a href="configReportePromotoria.php?tipo=8&tit=8">Informe convenios de pago de aportes parafiscales.</a></td>
	    		</tr>
	    		<tr>
		   			<td scope="row" align="left">Reporte009</td>
		   			<td align="left"><a href="configReportePromotoria.php?tipo=9&tit=9">Informe general de promotoria.</a></td>
	    		</tr>
	    		<tr>
		   			<td scope="row" align="left">Reporte010</td>
		   			<td align="left"><a href="configReportePromotoria.php?tipo=10&tit=10">Informe discriminado de las liquidaciones.</a></td>
	    		</tr>
	    		<tr>
		   			<td scope="row" align="left">Reporte011</td>
		   			<td align="left"><a href="configReportePromotoria.php?tipo=11&tit=11">Informe del historial de los seguimientos de la visita.</a></td>
	    		</tr>
	    		<tr>
		   			<td scope="row" align="left">Reporte012</td>
		   			<td align="left"><a href="configReportePromotoria.php?tipo=12&tit=12">Reporte de los ajustes al periodo liquidado.</a></td>
	    		</tr>
	    		<tr>
		   			<td scope="row" align="left">Reporte013</td>
		   			<td align="left"><a href="configReportePromotoria.php?tipo=13&tit=13">Carta de cobro persuasivo.</a></td>
	    		</tr>
	    		<tr>
		   			<td scope="row" align="left">Reporte014</td>
		   			<td align="left"><a href="configReportePromotoria.php?tipo=14&tit=14">Informe notificaci&oacute;n de liquidaci&oacute;n de aforo que no han pagado.</a></td>
	    		</tr>
	    		<tr>
		   			<td scope="row" align="left">Reporte015</td>
		   			<td align="left"><a href="configReportePromotoria.php?tipo=15&tit=15">Reporte consolidado del historial de los abonos del periodo liquidado.</a></td>
	    		</tr>
	    		<tr>
		   			<td scope="row" align="left">Reporte016</td>
		   			<td align="left"><a href="configReportePromotoria.php?tipo=16&tit=16">Reporte discriminado del historial de los abonos del periodo liquidado.</a></td>
	    		</tr>
	    		<tr>
		   			<td scope="row" align="left">Reporte017</td>
		   			<td align="left"><a href="configReportePromotoria.php?tipo=17&tit=17">Reporte cierre promotoria.</a></td>
	    		</tr>
	    		<tr>
		   			<td scope="row" align="left">Reporte018</td>
		   			<td align="left"><a href="configReportePromotoria.php?tipo=18&tit=18">Reporte consolidado del cierre promotoria.</a></td>
	    		</tr>
			</table>
		</center>
	</body>
</html>