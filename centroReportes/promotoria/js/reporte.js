
// funcion para obtener cookie en javascript
	function getCookie(c_name)
	{
		var i,x,y,ARRcookies=document.cookie.split(";");
		for (i=0;i<ARRcookies.length;i++)
		{
  			x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
  			y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
  			x=x.replace(/^\s+|\s+$/g,"");
  			if (x==c_name)
    			{
    				return unescape(y);
    			}// fin if
		}// fin for
  	}// fin getCookie
//funcion para obtener cookie en javascript
	
URL=src();
var uriBuscarControlNotificacion ="";
$(document).ready(function(){
	uriBuscarControlNotificacion = URL+"aportes/empresas/buscarControlNotificacion.php";
	
	//datepicker reporte003	
	$('#txtFechaInicial').datepicker({
		 maxDate:"+0D",
		 changeMonth: true,
		 changeYear: true,
		 onSelect: function(dateText, inst) {
			 var lockDate = new Date($('#txtFechaInicial').datepicker('getDate'));
		 	$('input#txtFechaFinal').datepicker('option', 'minDate', lockDate);
		 }
	});
	 
	 $("#txtFechaFinal").datepicker({
		 maxDate:"+0D",
		 changeMonth: true,
		 changeYear: true
	 });
	 
	 $('#txtFechaInicial2').datepicker({
		 maxDate:"+0D",
		 changeMonth: true,
		 changeYear: true,
		 onSelect: function(dateText, inst) {
			 var lockDate = new Date($('#txtFechaInicial2').datepicker('getDate'));
		 	$('input#txtFechaFinal2').datepicker('option', 'minDate', lockDate);
		 }
	});
	 
	 $("#txtFechaFinal2").datepicker({
		 maxDate:"+0D",
		 changeMonth: true,
		 changeYear: true
	 }); 
	 
	 $("#txtPeriodo").datepicker({
		 dateFormat: 'yymm',
	     changeMonth: true,
	     changeYear: true,
	     showButtonPanel: true,
	     onClose: function(dateText, inst) {
	    	 var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
	         var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
	         $(this).datepicker('setDate', new Date(year, month, 1));
	     }
	});
		
	$("#txtPeriodo").focus(function () {
		$(".ui-datepicker-calendar").hide();
		$("#ui-datepicker-div").position({
			my: "center top",
			at: "center bottom",
			of: $(this)
		});
	}); 
	
	datepickerC("PERIODO","#txtPeriodoInicial,#txtPeriodoFinal");
	
	//Evento para buscar las notificaciones de las cartas
	$("#txtNit").blur(ctrNotificacionEmpresa);
	
	$("#divControlNotificacion").dialog({
		autoOpen: false,
		modal: true,
		width: 900,
		buttons:{
			"CERRAR": function(){
				$(this).dialog("close");
			}
		}
	});
});

function fetchControlNotificacion(datos){
	var resultado = [];
	
	$.ajax({
		type:"POST",
		url:uriBuscarControlNotificacion, 
		data:{datos:datos}, 
		dataType:"json",
		async:false,
		success:function(datos){
			resultado = datos;
		}
	});
	
	return resultado;
}

function generarNotificacionPDf(idNotificacion){
	var data = "codigo_carta=GENERAR_NOTIFICACION_PDF&id_notificacion="+idNotificacion;
	
	var url=URL+"centroReportes/generar_carta.log.php?"+data;
	window.open(url,"_BLANK");
}

function ctrNotificacionEmpresa(){
	var idCarta = $("#hidIdCarta").val();
	var nit = $("#txtNit").val();
	
	if(!nit || !idCarta) return false;
	
	var dataNotificacion = fetchControlNotificacion({id_carta_notificacion:idCarta,nit:nit});
	
	if(dataNotificacion && typeof dataNotificacion == "object" && dataNotificacion.length>0 ){
		
		var contenidoHtml = "";
		
		$.each(dataNotificacion,function(i,row){
			contenidoHtml += "<tr><td>"+row.carta_notificacion+"</td>"
								+"<td>"+row.fecha_sistema+"</td>"
								+"<td>"+row.estado_notificacion+"</td>"
								+"<td>"+row.fecha_estado+"</td>"
								+"<td>"+row.informacion+"</td>"
								+"<td><label style='cursor:pointer' onClick='generarNotificacionPDf("+row.id_notificacion+");'><img src='../../imagenes/icono_pdf.png' width='24' height='24'></label></td></tr>";
		});
		
		$("#tbControlNotificacion tbody").html(contenidoHtml);
		$("#divControlNotificacion").dialog("open");
	}else{
		alert("No se encontraron notificaciones para la empresa!!");
	}
}

//listado de visitas por empresa
function reporte001(){ 
	
	var nit = $("#txtNit").val().trim();
	//validar campos vacios
	if(nit==""){alert("Debe digitar el nit");return false;}
	
	$.ajax({
		url:URL+'phpComunes/buscarEmpresa.php', 
		type:"POST",
		data:"v0="+nit, 
		dataType:"json",
		async:false,
		success:function(datos){
       		if(datos==0){
				$("#rta").html("La empresa no existe en nuestra base de datos.").hide().fadeIn(1000);
			    return false;
			 }
			 $.each(datos,function(i,fila){ idempresa=fila.idempresa;empresa=fila.razonsocial;});//each
			
			 //Buscamos las visitas existentes  realizadas a la empresa seleccionada
			 $.ajax({
				  url:URL+'promotoria/buscarVisita.php', 
				  type:"POST",
				  data:{objDatosFiltro:{idempresa:idempresa}}, 
				  dataType:"json",
				  async:false,
				  success:function(visitas){
					  if(typeof visitas == "object" &&  visitas.length>0){
						//Si existen visitas generamos el reporte y pasamos el parametro
						  var url="http://"+getCookie("URL_Reportes")+"/promotoria/reporte001.jsp?v0="+idempresa+"&v1="+$("#txtUsuario").val();
						  window.open(url,"_BLANK");
					  }else{
						  $("#rta").html("NO existen visitas para esta empresa.").hide().fadeIn(1000);
					  }
				   }
			 });//ajax
		}//succes	 
	});//ajax
}

//listado de liquidaciones por empresa
function reporte002(){
	
	var nit = $("#txtNit").val().trim();
	//validar campos vacios
	if(nit==""){alert("Debe digitar el nit");return false;}
	
	$.ajax({
		url:URL+'phpComunes/buscarEmpresa.php', 
		type:"POST",
		data:"v0="+nit, 
		dataType:"json",
		async:false,
		success:function(datos){
       		if(datos==0){
				$("#rta").html("La empresa no existe en nuestra base de datos.").hide().fadeIn(1000);
			    return false;
			 }
			 $.each(datos,function(i,fila){ idempresa=fila.idempresa;empresa=fila.razonsocial;});//each
			
			 //Buscamos las visitas existentes  realizadas a la empresa seleccionada
			 $.ajax({
				  url:URL+'promotoria/buscarVisita.php', 
				  type:"POST",
				  data:{objDatosFiltro:{idempresa:idempresa}}, 
				  dataType:"json",
				  async:false,
				  success:function(visitas){
					  if(typeof visitas == "object" &&  visitas.length>0){
						//Si existen visitas generamos el reporte y pasamos el parametro
						  var url="http://"+getCookie("URL_Reportes")+"/promotoria/reporte002.jsp?v0="+idempresa+"&v1="+$("#txtUsuario").val();
						  window.open(url,"_BLANK");
					  }else{
						  $("#rta").html("NO existen visitas para esta empresa.").hide().fadeIn(1000);
					  }					  
				   }
			 });//ajax
		}//succes	 
	});//ajax
}

//listado promotorķa
function reporte003(opt){
	
	var idpromotor=$("#cmbPromotor").val(),
		fechaI=$("#txtFechaInicial").val(),
		fechaF=$("#txtFechaFinal").val(),
		idAgencia = $("#cmbIdAgencia").val(),
		idCausalVisita = $("#cmbCausalVisita").val();
	
	if( (fechaI && !fechaF) || (!fechaI && fechaF) ){
		if(!fechaI)
			$("#txtFechaInicial").removeClass("box1").addClass("ui-state-error").focus();
		else 
			$("#txtFechaFinal").removeClass("box1").addClass("ui-state-error").focus()
		return false;
	}
	
	//Parametros del reporte
	data = "v0="+fechaI+"&v1="+fechaF+"&v2="+idpromotor+"&v3="+idAgencia+"&v4="+idCausalVisita;
	if(opt=="PDF"){
		url="http://"+getCookie("URL_Reportes")+"/promotoria/reporte003.jsp?";
	}else{
		url="http://"+getCookie("URL_Reportes")+"/promotoria/rptExcel003.jsp?";
	}
	url += data;
	window.open(url,"_BLANK");
}


function reporte004(opt){
	var fechaI = $("#txtFechaInicial").val(),
		fechaF = $("#txtFechaFinal").val(),
		pazSalvo = $("#cmbPazYSalvo").val(),
		nit = $("#txtNit").val().trim(),
		idPromotor = $("#cmbPromotor").val(),
		idVisita = $("#txtNumeroVisita").val().trim(),
		idAgencia = $("#cmbIdAgencia").val(),
		idCausalVisita = $("#cmbCausalVisita").val(),
		data = null,
		url = null;
	
	//valida campos
	$(".ui-state-error").removeClass("ui-state-error").addClass("box1");
	if( (fechaI && !fechaF) || (!fechaI && fechaF) ){
		if(!fechaI)
			$("#txtFechaInicial").removeClass("box1").addClass("ui-state-error").focus();
		else 
			$("#txtFechaFinal").removeClass("box1").addClass("ui-state-error").focus()
		return false;
	}
	
	//Parametros del reporte
	data = "v0="+fechaI+"&v1="+fechaF+"&v2="+pazSalvo+"&v3="+nit+"&v4="+
			idPromotor+"&v5="+idVisita+"&v6="+idAgencia+"&v7="+idCausalVisita;
	if(opt=="PDF"){
		url="http://"+getCookie("URL_Reportes")+"/promotoria/reporte004.jsp?";
	}else{
		url="http://"+getCookie("URL_Reportes")+"/promotoria/rptExcel004.jsp?";
	}
	url += data;
	window.open(url,"_BLANK");
}
function reporte005(){
	var comunicacion = $("#txtComunicacion").val().trim(),
		tipoFiltro = $("#cmbFiltro").val(),
		fechaI = $("#txtFechaInicial").val(),
		fechaF = $("#txtFechaFinal").val(),
		idVisita = $("#txtVisita").val().trim(),
		nit = $("#txtNit").val().trim(),
		idAgencia = $("#cmbIdAgencia").val(),
		usuario = $("#txtNombreUsuario").val();
		
	//valida campos
	$(".ui-state-error").removeClass("ui-state-error");
	if( comunicacion == 0 ){$("#txtComunicacion").addClass("ui-state-error").focus();return false;}
	
	if( (fechaI && !fechaF) || (!fechaI && fechaF) ){
		if(!fechaI)
			$("#txtFechaInicial").removeClass("box1").addClass("ui-state-error").focus();
		else 
			$("#txtFechaFinal").removeClass("box1").addClass("ui-state-error").focus()
		return false;
	}
	
	//Actualizar el consecutivo y las observaciones de los periodos de la liquidacion
	/*var objDatos = {idVisita:idVisita
			,fechaI:fechaI
			,fechaF:fechaF
			,estado:"A"};*/
	
	var objDatos = {idvisita:idVisita
			,fechavisita:{I:fechaI,F:fechaF}
			,estado:"A"
			,nit:nit
			,idagencia:idAgencia};
	
	if(actuaConseObserVisita(objDatos)==0){
		alert("No fue posible almacenar el consecutivo u observacion de los periodos de las visitas");
		return false;
	}
	
	var data = "codigo_carta=CARTA_LIQUI_AFORO&v0="+comunicacion+"&v1="+fechaI+"&v2="+fechaF+"&v3="+idVisita+"&v4="+nit+"&v5="+usuario+"&v6="+idAgencia;
	
	var url=URL+"centroReportes/generar_carta.log.php?"+data;
	window.open(url,"_BLANK");
	
	/* **** Prepara para guardar el control de la notificacion **** */
	/*var objDatos = {
			reporte:"PROMOTORIA-Reporte005",
			datos:{
				id_visita:idVisita,
				nit:nit,
				fechaI:fechaI,
				fechaF:fechaF,
				id_agencia:idAgencia
			}
		};
	if(controlNotificacion(objDatos)==0)
		return false;*/
	
	/* **** Generar la carta **** */
	
	
	//Envio los parametros del reporte
	//var url="http://"+getCookie("URL_Reportes")+"/promotoria/reporte005.jsp?v0="+comunicacion+"&v1="+fechaI+"&v2="+fechaF+"&v3="+idVisita+"&v4="+nit+"&v5="+usuario+"&v6="+idAgencia;
	//window.open(url,"_BLANK");
}

/*function reporte006(){ 
	var fechaI = $("#txtFechaInicial").val(),
		fechaF = $("#txtFechaFinal").val(),
		estado = $("#cmbPazYSalvo").val();
	
	//valida campos
	$(".ui-state-error").removeClass("ui-state-error");
	if( fechaI == 0 ){$("#txtFechaInicial").addClass("ui-state-error").focus();return false;}
	if( fechaF == 0 ){$("#txtFechaFinal").addClass("ui-state-error").focus();return false;}
	if( estado == 0 ){$("#cmbPazYSalvo").addClass("ui-state-error").focus();return false;}
	
	//Envio los parametros del reporte
	var url="http://:8080/sigasReportes/promotoria/reporte006.jsp?v0="+fechaI+"&v1="+fechaF+"&v2="+estado;
	window.open(url,"_BLANK");
}*/

function reporte007(){ 
	var nit = $("#txtNit").val();
	var fechaI = $("#txtFechaInicial").val();
	var fechaF = $("#txtFechaFinal").val();
	var nombreUsuario = $("#txtNombreUsuario").val();
	var idAgencia = $("#cmbIdAgencia").val();
	
	$(".ui-state-error").removeClass("ui-state-error");
	if( (fechaI && !fechaF) || (!fechaI && fechaF) ){
		if(!fechaI)
			$("#txtFechaInicial").removeClass("box1").addClass("ui-state-error").focus();
		else 
			$("#txtFechaFinal").removeClass("box1").addClass("ui-state-error").focus()
		return false;
	}
	
	var data = "codigo_carta=CARTA_CONVE_PAGO&v0="+nit+"&v1="+nombreUsuario+"&v2="+fechaI+"&v3="+fechaF+"&v4="+idAgencia;
	
	var url=URL+"centroReportes/generar_carta.log.php?"+data;
	window.open(url,"_BLANK");
	
	/* **** Prepara para guardar el control de la notificacion **** */
	/*var objDatos = {
			reporte:"PROMOTORIA-Reporte007",
			datos:{
				nit:nit,
				fechaI:fechaI,
				fechaF:fechaF,
				id_agencia:idAgencia
			}
		};
	if(controlNotificacion(objDatos)==0)
		return false;
	*/
	/* **** Generar la carta **** */
	
	//Envio los parametros del reporte
	/*var url="http://"+getCookie("URL_Reportes")+"/promotoria/reporte007.jsp?v0="+nit+"&v1="+nombreUsuario+"&v2="+fechaI+"&v3="+fechaF+"&v4="+idAgencia;
	window.open(url,"_BLANK");*/
}

function reporte008(){ 
	var nit = $("#txtNit").val();
	var fechaI = $("#txtFechaInicial").val();
	var fechaF = $("#txtFechaFinal").val();
	var estado = $("#cmbEstadoConvenio").val();
	var idAgencia = $("#cmbIdAgencia").val();
	
	$(".ui-state-error").removeClass("ui-state-error");
	if( (fechaI && !fechaF) || (!fechaI && fechaF) ){
		if(!fechaI)
			$("#txtFechaInicial").removeClass("box1").addClass("ui-state-error").focus();
		else 
			$("#txtFechaFinal").removeClass("box1").addClass("ui-state-error").focus()
		return false;
	}
	
	//Envio los parametros del reporte
	var url="http://"+getCookie("URL_Reportes")+"/promotoria/rptExcel008.jsp?v0="+estado+"&v1="+nit+"&v2="+fechaI+"&v3="+fechaF+"&v4="+idAgencia;
	window.open(url,"_BLANK");
}

function reporte009(opt){ 
	var fechaI = $("#txtFechaInicial").val(),
		fechaF = $("#txtFechaFinal").val(),
		nit = $("#txtNit").val().trim(),
		idVisita = $("#txtIdVisita").val().trim(),
		idAgencia = $("#cmbIdAgencia").val(),
		idCausalVisita = $("#cmbCausalVisita").val(),
		data = null,
		url = null;
	
	//valida campos
	$(".ui-state-error").removeClass("ui-state-error");
	if( (fechaI && !fechaF) || (!fechaI && fechaF) ){
		if(!fechaI)
			$("#txtFechaInicial").removeClass("box1").addClass("ui-state-error").focus();
		else 
			$("#txtFechaFinal").removeClass("box1").addClass("ui-state-error").focus()
		return false;
	}
	
	//Parametros del reporte
	data = "v0="+fechaI+"&v1="+fechaF+"&v2="+nit+"&v3="+idVisita+"&v4="+idAgencia+"&v5="+idCausalVisita;
	if(opt=="PDF"){
		url="http://"+getCookie("URL_Reportes")+"/promotoria/reporte009.jsp?";
	}else{
		url="http://"+getCookie("URL_Reportes")+"/promotoria/rptExcel009.jsp?";
	}
	url += data;
	window.open(url,"_BLANK");
}

function reporte010(opt){
	var fechaI = $("#txtFechaInicial").val(),
		fechaF = $("#txtFechaFinal").val(),
		fechaAbonoI = $("#txtFechaInicial2").val(),
		fechaAbonoF = $("#txtFechaFinal2").val(),
		pazSalvo = $("#cmbPazYSalvo").val(),
		nit = $("#txtNit").val().trim(),
		idPromotor = $("#cmbPromotor").val(),
		idVisita = $("#txtNumeroVisita").val().trim(),
		idAgencia = $("#cmbIdAgencia").val(),
		idCausalVisita = $("#cmbCausalVisita").val(),
		data = null,
		url = null;
	
	//valida campos
	$(".ui-state-error").removeClass("ui-state-error").addClass("box1");
	if( (fechaI && !fechaF) || (!fechaI && fechaF) ){
		if(!fechaI)
			$("#txtFechaInicial").removeClass("box1").addClass("ui-state-error").focus();
		else 
			$("#txtFechaFinal").removeClass("box1").addClass("ui-state-error").focus()
		return false;
	}
	
	if( (fechaAbonoI && !fechaAbonoF) || (!fechaAbonoI && fechaAbonoF) ){
		if(!fechaAbonoI)
			$("#txtFechaInicial2").removeClass("box1").addClass("ui-state-error").focus();
		else 
			$("#txtFechaFinal2").removeClass("box1").addClass("ui-state-error").focus()
		return false;
	}
	
	//Parametros del reporte
	data = "v0="+fechaI+"&v1="+fechaF+"&v2="+pazSalvo+"&v3="+nit+"&v4="+
			idPromotor+"&v5="+idVisita+"&v6="+idAgencia+"&v7="+fechaAbonoI+"&v8="+fechaAbonoF
			+ "&v9="+idCausalVisita;
	if(opt=="PDF"){
		url="http://"+getCookie("URL_Reportes")+"/promotoria/reporte010.jsp?";
	}else{
		url="http://"+getCookie("URL_Reportes")+"/promotoria/rptExcel010.jsp?";
	}
	url += data;
	window.open(url,"_BLANK");
}

function reporte011(opt){
	var idVisita = $("#txtNumeroVisita").val().trim(),
		fechaI = $("#txtFechaInicial").val(),
		fechaF = $("#txtFechaFinal").val(),
		asesorJuridicoI = $("#cmbAsesorJuridicoI").val(),
		asesorJuridicoE = $("#cmbAsesorJuridicoE").val(),
		nit = $("#txtNit").val().trim(),
		idAgencia = $("#cmbIdAgencia").val(),
		idCausalVisita = $("#cmbCausalVisita").val(),
		data = null,
		url = null;
	
	//valida campos
	$(".ui-state-error").removeClass("ui-state-error").addClass("box1");
	if( (fechaI && !fechaF) || (!fechaI && fechaF) ){
		if(!fechaI)
			$("#txtFechaInicial").removeClass("box1").addClass("ui-state-error").focus();
		else 
			$("#txtFechaFinal").removeClass("box1").addClass("ui-state-error").focus()
		return false;
	}
	
	//Parametros del reporte
	data = "v0="+idVisita+"&v1="+fechaI+"&v2="+fechaF+"&v3="+asesorJuridicoI+"&v4="+asesorJuridicoE+"&v5="+nit+"&v6="+idAgencia
			+"&v7="+idCausalVisita;
	if(opt=="PDF"){
		url="http://"+getCookie("URL_Reportes")+"/promotoria/reporte011.jsp?";
	}else{
		url="http://"+getCookie("URL_Reportes")+"/promotoria/rptExcel011.jsp?";
	}
	url += data;
	window.open(url,"_BLANK");
}


function reporte012(opt){
	var fechaI = $("#txtFechaInicial").val(),
		fechaF = $("#txtFechaFinal").val(),
		pazSalvo = $("#cmbPazYSalvo").val(),
		nit = $("#txtNit").val().trim(),
		idPromotor = $("#cmbPromotor").val(),
		idVisita = $("#txtNumeroVisita").val().trim(),
		idAgencia = $("#cmbIdAgencia").val(),
		idCausalVisita = $("#cmbCausalVisita").val(),
		idCausalAjuste = $("#cmbCausalAjuste").val(),
		data = null,
		url = null;
	
	//valida campos
	$(".ui-state-error").removeClass("ui-state-error").addClass("box1");
	if( (fechaI && !fechaF) || (!fechaI && fechaF) ){
		if(!fechaI)
			$("#txtFechaInicial").removeClass("box1").addClass("ui-state-error").focus();
		else 
			$("#txtFechaFinal").removeClass("box1").addClass("ui-state-error").focus()
		return false;
	}
	
	//Parametros del reporte
	data = "v0="+fechaI+"&v1="+fechaF+"&v2="+pazSalvo+"&v3="+nit+"&v4="+
			idPromotor+"&v5="+idVisita+"&v6="+idAgencia+"&v7="+idCausalVisita
			+"&v8="+idCausalAjuste;
	if(opt=="PDF"){
		url="http://"+getCookie("URL_Reportes")+"/promotoria/reporte012.jsp?";
	}else{
		url="http://"+getCookie("URL_Reportes")+"/promotoria/rptExcel012.jsp?";
	}
	url += data;
	window.open(url,"_BLANK");
}

function reporte013(opt){
	/*Las cartas solo aplican para las visitas que aun no tiene paz y salvo*/
	var comunicacion = $("#txtComunicacion").val().trim(),
		idVisita = $("#txtNumeroVisita").val().trim(),
		fechaI = $("#txtFechaInicial").val(),
		fechaF = $("#txtFechaFinal").val(),
		numeroReitero = $('input:radio[name="radNumeroReitero"]:checked').val(),
		nit = $("#txtNit").val().trim(),
		idAgencia = $("#cmbIdAgencia").val(),
		usuario = $("#txtNombreUsuario").val(),
		data = null,
		url = null,
		bandera = 0;
	
	//Si no existen numero de reiteros
	numeroReitero = typeof numeroReitero == "undefined" ? "":numeroReitero;
	//valida campos
	$(".ui-state-error").removeClass("ui-state-error").addClass("box1");
	if(!comunicacion){ $("#txtComunicacion").removeClass("box1").addClass("ui-state-error").focus();return false;}
	if( (fechaI && !fechaF) || (!fechaI && fechaF) ){
		if(!fechaI)
			$("#txtFechaInicial").removeClass("box1").addClass("ui-state-error").focus();
		else 
			$("#txtFechaFinal").removeClass("box1").addClass("ui-state-error").focus()
		return false;
	}
	
	/* **** Prepara para guardar el control de la notificacion **** */
	/*var objDatos = {
			reporte:"PROMOTORIA-Reporte013",
			datos:{
				id_visita:idVisita,
				id_agencia:idAgencia,
				fechaI:fechaI,
				fechaF:fechaF,
				nit:nit,
				numero_reitero:numeroReitero
			}
		};
	if(controlNotificacion(objDatos)==0)
		return false;
	*/
	/* **** Actualizar el numero de reitero **** */
	
	if(!numeroReitero){
		
		//Actualizar el consecutivo y las observaciones de los periodos de la liquidacion
		var objDatos = {idvisita:idVisita
				,fechavisita:{I:fechaI,F:fechaF}
				,estado:"A"
				,nit:nit
				,idagencia:idAgencia};
		
		if(actuaConseObserVisita(objDatos)==0){
			alert("No fue posible almacenar el consecutivo u observacion de los periodos de las visitas");
			return false;
		}
		
		//Actualizar el numero de reitero
		$.ajax({
			url:URL+"promotoria/actualizarNumReiteroVista.php",
			type:"POST",
			data:{objDatosFiltro:objDatos},
			async:false,
			beforeSend: function(objeto){
				dialogLoading('show');
			},
			complete: function(objeto, exito){
				dialogLoading('close');
			},
			success:function(datos){
				if(datos!=1)
					bandera = 1;
			}
		});
	}
	
	/* **** Generar la carta **** */
	
	if(bandera==0){
		

		var data = "codigo_carta=CARTA_COBRO_PERSU&v0="+idVisita+"&v1="+fechaI+"&v2="+fechaF+"&v3="+comunicacion+"&v4="+numeroReitero+"&v5="+nit+"&v6="+usuario+"&v7="+idAgencia;
		
		var url=URL+"centroReportes/generar_carta.log.php?"+data;
		window.open(url,"_BLANK");
		
		//Parametros del reporte
		/*data = "v0="+idVisita+"&v1="+fechaI+"&v2="+fechaF+"&v3="+comunicacion+"&v4="+numeroReitero+"&v5="+nit+"&v6="+usuario+"&v7="+idAgencia;
		if(opt=="PDF"){
			url="http://"+getCookie("URL_Reportes")+"/promotoria/reporte013.jsp?";
		}
		url += data;
		window.open(url,"_BLANK");*/
	}else{
		alert("El n\xFAmero del reitero no se actualizo, por tanto no se puede genera la carta.");
	}
}

function reporte014(opt){
	var idVisita = $("#txtNumeroVisita").val().trim(),
		fechaI = $("#txtFechaInicial").val(),
		fechaF = $("#txtFechaFinal").val(),
		nit = $("#txtNit").val().trim(),
		idAgencia = $("#cmbIdAgencia").val(),
		idCausalVisita = $("#cmbCausalVisita").val(),
		data = null,
		url = null;
	
	//valida campos
	$(".ui-state-error").removeClass("ui-state-error").addClass("box1");
	if( (fechaI && !fechaF) || (!fechaI && fechaF) ){
		if(!fechaI)
			$("#txtFechaInicial").removeClass("box1").addClass("ui-state-error").focus();
		else 
			$("#txtFechaFinal").removeClass("box1").addClass("ui-state-error").focus()
		return false;
	}
	
	//Parametros del reporte
	data = "v0="+idVisita+"&v1="+fechaI+"&v2="+fechaF+"&v3="+nit+"&v4="+idAgencia+"&v5="+idCausalVisita;
	if(opt=="PDF"){
		url="http://"+getCookie("URL_Reportes")+"/promotoria/reporte014.jsp?";
	}else{
		url="http://"+getCookie("URL_Reportes")+"/promotoria/rptExcel014.jsp?";
	}
	url += data;
	window.open(url,"_BLANK");
}

function reporte015(opt){
	var fechaI = $("#txtFechaInicial").val(),
		fechaF = $("#txtFechaFinal").val(),
		pazSalvo = $("#cmbPazYSalvo").val(),
		nit = $("#txtNit").val().trim(),
		idPromotor = $("#cmbPromotor").val(),
		idVisita = $("#txtNumeroVisita").val().trim(),
		idAgencia = $("#cmbIdAgencia").val(),
		idCausalVisita = $("#cmbCausalVisita").val(),
		fechaAbonoI = $("#txtFechaInicial2").val(),
		fechaAbonoF = $("#txtFechaFinal2").val(),
		data = null,
		url = null;
	
	//valida campos
	$(".ui-state-error").removeClass("ui-state-error").addClass("box1");
	if( (fechaI && !fechaF) || (!fechaI && fechaF) ){
		if(!fechaI)
			$("#txtFechaInicial").removeClass("box1").addClass("ui-state-error").focus();
		else 
			$("#txtFechaFinal").removeClass("box1").addClass("ui-state-error").focus()
		return false;
	}
	
	//Parametros del reporte
	data = "v0="+fechaI+"&v1="+fechaF+"&v2="+pazSalvo+"&v3="+nit+"&v4="+
			idPromotor+"&v5="+idVisita+"&v6="+idAgencia+"&v7="+idCausalVisita
			+"&v8="+fechaAbonoI+"&v9="+fechaAbonoF;
	if(opt=="PDF"){
		url="http://"+getCookie("URL_Reportes")+"/promotoria/reporte015.jsp?";
	}else{
		url="http://"+getCookie("URL_Reportes")+"/promotoria/rptExcel015.jsp?";
	}
	url += data;
	window.open(url,"_BLANK");
}

function reporte016(opt){
	var fechaI = $("#txtFechaInicial").val(),
		fechaF = $("#txtFechaFinal").val(),
		fechaAbonoI = $("#txtFechaInicial2").val(),
		fechaAbonoF = $("#txtFechaFinal2").val(),
		pazSalvo = $("#cmbPazYSalvo").val(),
		nit = $("#txtNit").val().trim(),
		idPromotor = $("#cmbPromotor").val(),
		idVisita = $("#txtNumeroVisita").val().trim(),
		idAgencia = $("#cmbIdAgencia").val(),
		idCausalVisita = $("#cmbCausalVisita").val(),
		estadoAbono = "",//$("#cmbEstadoAbono").val(),
		data = null,
		url = null;
	
	//valida campos
	$(".ui-state-error").removeClass("ui-state-error").addClass("box1");
	if( (fechaI && !fechaF) || (!fechaI && fechaF) ){
		if(!fechaI)
			$("#txtFechaInicial").removeClass("box1").addClass("ui-state-error").focus();
		else 
			$("#txtFechaFinal").removeClass("box1").addClass("ui-state-error").focus()
		return false;
	}
	
	if( (fechaAbonoI && !fechaAbonoF) || (!fechaAbonoI && fechaAbonoF) ){
		if(!fechaAbonoI)
			$("#txtFechaInicial2").removeClass("box1").addClass("ui-state-error").focus();
		else 
			$("#txtFechaFinal2").removeClass("box1").addClass("ui-state-error").focus()
		return false;
	}
	
	//Parametros del reporte
	data = "v0="+fechaI+"&v1="+fechaF+"&v2="+pazSalvo+"&v3="+nit+"&v4="+
			idPromotor+"&v5="+idVisita+"&v6="+idAgencia+"&v7="+fechaAbonoI
			+"&v8="+fechaAbonoF+"&v9="+idCausalVisita+"&v10="+estadoAbono;
	if(opt=="PDF"){
		url="http://"+getCookie("URL_Reportes")+"/promotoria/reporte016.jsp?";
	}else{
		url="http://"+getCookie("URL_Reportes")+"/promotoria/rptExcel016.jsp?";
	}
	url += data;
	window.open(url,"_BLANK");
}


function reporte017(opt){
	var periodoI = $("#txtPeriodoInicial").val(),
		periodoF = $("#txtPeriodoFinal").val(),
		idAgencia = $("#cmbIdAgencia").val(),
		idCausalVisita = $("#cmbCausalVisita").val(),
		data = null,
		url = null;
	
	//valida campos
	$(".ui-state-error").removeClass("ui-state-error").addClass("box1");
	if( (periodoI && !periodoF) || (!periodoI && periodoF) ){
		if(!fechaI)
			$("#txtPeriodoInicial").removeClass("box1").addClass("ui-state-error").focus();
		else 
			$("#txtPeriodoFinal").removeClass("box1").addClass("ui-state-error").focus()
		return false;
	}
	
	//Parametros del reporte
	data = "v0="+periodoI+"&v1="+periodoF+"&v2="+idAgencia+"&v3="+idCausalVisita;
	if(opt=="PDF"){
		url="http://"+getCookie("URL_Reportes")+"/promotoria/reporte017.jsp?";
	}else{
		url="http://"+getCookie("URL_Reportes")+"/promotoria/rptExcel017.jsp?";
	}
	url += data;
	window.open(url,"_BLANK");
}


function reporte018(opt){
	var periodo = $("#txtPeriodo").val(),
		idCausalVisita = $("#cmbCausalVisita").val(),
		data = null,
		url = null;
	
	//valida campos
	$(".ui-state-error").removeClass("ui-state-error").addClass("boxfecha");
	if(!periodo){
		$("#txtPeriodo").addClass("ui-state-error").removeClass("boxfecha").focus();
		return false;
	}
	
	//Parametros del reporte
	data = "v0="+periodo+"&v1="+idCausalVisita;
	if(opt=="PDF"){
		url="http://"+getCookie("URL_Reportes")+"/promotoria/reporte018.jsp?";
	}else{
		url="http://"+getCookie("URL_Reportes")+"/promotoria/rptExcel018.jsp?";
	}
	url += data;
	window.open(url,"_BLANK");
}
/**
 * Metodo para guardar el control de notificaciones de la empresa
 * @param objDatos
 * @returns {Number}: 0:Error | 1:Ok
 */
function controlNotificacion(objDatos){
	var resultado = 0;
	$.ajax({
		url:URL+"centroReportes/controlNotifEmpresa.php",
		type:"POST",
		data:{objDatos:objDatos},
		dataType:"json",
		async:false,
		beforeSend: function(objeto){
			dialogLoading('show');
		},
		complete: function(objeto, exito){
			dialogLoading('close');	
		}, 
		success:function(datos){
			if(datos.Error==0)
				resultado = 1;
			else if(datos.Error>0)
				alert(datos.Mensaje);
		}
	});
	return resultado;
}

/**
 * Actualiza el consecutivo de la visita y las observaciones del periodo
 * Return 0:Error | 1:Ok
 */
function actuaConseObserVisita(objDatos){
	var resultado = 0;
	
	$.ajax({
		url:URL+"promotoria/actualizarConseObserVisita.php",
		type:"POST",
		data:{objDatosFiltro:objDatos},
		async:false,
		beforeSend: function(objeto){
			dialogLoading('show');
		},
		complete: function(objeto, exito){
			dialogLoading('close');
		}, 
		success:function(datos){
			if(datos==1)
				resultado = 1;
		}
	});
	return resultado;
}