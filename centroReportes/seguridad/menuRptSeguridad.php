<?php
/* autor:       orlando puentes
 * fecha:       28/09/2010
 * objetivo:    
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';

include_once  $root;
include_once $raiz.DIRECTORY_SEPARATOR ."config.php";
$usuario=$_SESSION['USUARIO'];

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Men&uacute; Reportes de subsidio cuota monetaria</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="../../css/estiloReporte.css" rel="stylesheet" type="text/css">
<script src="../../js/comunes.js" type="text/javascript"></script>
</head>
<body>
<center>
  <table width="100%" border="0">
  <tr>
    <td align="center" ><img src="../../imagenes/razonSocial.png" width="615" height="55"></td>
  </tr>
  <tr>
    <td align="center" >&nbsp;</td>
  </tr>
  <tr>
    <td class="fecha" ></td>
  </tr>
</table>
<table width="60%" border="0" cellspacing="1" class="tablero">
	<tr><th>Reporte</th>
	<th>Listado de Reportes M&oacute;dulo Seguridad</th></tr>  
	  <tr>
	    <td scope="row" align="left">Reporte001</td>
	    <td align="left"><label style="cursor:pointer" >
	    <a style="text-decoration:none" target='_blank' href="<?php echo $ruta_reportes;?>seguridad/rptExcel001.jsp?v0=<?php echo $usuario; ?>"> Listado de Agencias</a> </label></td>
	  </tr>
	 <!-- <tr>
	  <td scope="row" align="left" width="20%">Reporte002</td>
	  <td align="left"><label style="cursor:pointer" ><a style="text-decoration:none" target='_blank' href="http://:8080/sigasReportes/seguridad/reporte002.jsp?v0=<?php echo $usuario; ?>">Listado Procesos</a></label></td>
	</tr> -->
	 <tr>
	   <td scope="row" align="left">Reporte003</td>
	   <td align="left"><label style="cursor:pointer" >
	   <a style="text-decoration:none" target='_blank' href="<?php echo $ruta_reportes;?>seguridad/rptExcel003.jsp?v0=<?php echo $usuario; ?>">Listado de Procesos - Areas</a></label></td>
    </tr>
	<!--  <tr>
	   <td scope="row" align="left">Reporte004</td>
	   <td align="left"><label style="cursor:pointer" ><a style="text-decoration:none" target='_blank' href="http://:8080/sigasReportes/seguridad/reporte004.jsp?v0=<?php echo $usuario; ?>">Listado de Procesos - Areas</a></td>
    </tr>-->
	 <tr>
	   <td scope="row" align="left">Reporte005</td>
	   <td align="left"><label style="cursor:pointer" >
	   <a style="text-decoration:none" target='_blank' href="<?php echo $ruta_reportes;?>seguridad/reporte005.jsp?v0=<?php echo $usuario; ?>">Listado de Modulos</a></td>
    </tr>
	 <tr>
	   <td scope="row" align="left">Reporte006</td>
	   <td align="left"><label style="cursor:pointer" >
	   <a style="text-decoration:none" target='_blank' href="<?php echo $ruta_reportes;?>seguridad/rptExcel006.jsp?v0=<?php echo $usuario; ?>">Listado de Menus</a></td>
    </tr>
	 <tr>
	   <td scope="row" align="left">Reporte007</td>
	   <td align="left"><label style="cursor:pointer" >
	   <a style="text-decoration:none" target='_blank' href="<?php echo $ruta_reportes;?>seguridad/reporte007.jsp?v0=<?php echo $usuario; ?>">Listado de Formularios</a></td>
    </tr>
	 <tr>
	   <td scope="row" align="left">Reporte008</td>
	   <td align="left"><label style="cursor:pointer" >
	   <a style="text-decoration:none" target='_blank' href="<?php echo $ruta_reportes;?>seguridad/reporte008.jsp?v0=<?php echo $usuario; ?>">Listado de Festivos</a></td>
    </tr>
	 <tr>
	   <td scope="row" align="left">Reporte009</td>
	   <td align="left"><label style="cursor:pointer" >
	   <a style="text-decoration:none" target='_blank' href="<?php echo $ruta_reportes;?>seguridad/reporte009.jsp?v0=<?php echo $usuario; ?>">Listado de Versiones</a></td>
    </tr>
	 <tr>
	   <td scope="row" align="left">Reporte010</td>
	   <td align="left"><label style="cursor:pointer" >
	   <a style="text-decoration:none" target='_blank' href="<?php echo $ruta_reportes;?>seguridad/reporte010.jsp?v0=<?php echo $usuario; ?>">Listado de Asignacion de Módulos</a></td>
    </tr>
	 <tr>
	   <td scope="row" align="left">Reporte011</td>
	   <td align="left"><label style="cursor:pointer" >
	   <a style="text-decoration:none" target='_blank' href="<?php echo $ruta_reportes;?>seguridad/reporte011.jsp?v0=<?php echo $usuario; ?>">Listado de Roles</a></td>
    </tr>
	 <tr>
	   <td scope="row" align="left">Reporte012</td>
	   <td align="left"><label style="cursor:pointer" >
	   <a style="text-decoration:none" target='_blank' href="<?php echo $ruta_reportes;?>seguridad/reporte012.jsp?v0=<?php echo $usuario;?>">Listado de Formularios por Rol</a></td>
    </tr>
	 <tr>
	   <td scope="row" align="left">Reporte013</td>
	   <td align="left"><label style="cursor:pointer" >
	   <a style="text-decoration:none" href="../configurarReporte.php?tipo=<?php echo 5;?>&modulo=seguridad&v0=<?php echo '013';?>&menurpte=seguridad/menuRptSeguridad.php">Listado de Usuarios Activos por Agencia</a></td>
    </tr>
	<tr>
	   <td scope="row" align="left">Reporte014</td>
	   <td align="left"><label style="cursor:pointer" >
	   <a  style="text-decoration:none" href="../configurarReporte.php?tipo=<?php echo 5;?>&modulo=seguridad&v0=<?php echo '014';?>&menurpte=seguridad/menuRptSeguridad.php">Listado de Usuarios por Agencia</a></td>
    </tr>                                           
</table>
</center>
</body>
<script language="javascript">
var URL=src();

function configurar(nu){
	switch (nu){
		case 1 :url=URL+"centroReportes/pignoracion/configReporte002.php";
		break;
	}
	window.open(url,"myFrame" );
}

function imprimir(nu){
    switch(nu){
        case 1 : url=URL+"centroReportes/pignoracion/reporte003.php";
        break;
    }
    window.open(url, '_blank')
}

function noPagare(){
	var idp=prompt("Digite el número del Pagare");
	var url=URL+"centroReportes/pignoracion/reporte001.php?v0="+idp;
	window.open(url,"_blank");
	}
	
	
function Rol(url){
	newwindow=window.open(url,'Confamiliar','left=500,top=400,scrollbars=0,resizable=0,toolbar=0,location=0');
	if (window.focus) {newwindow.focus()}
	return false;
	
	
	
	//var url="http://:8080/sigasReportes/seguridad/reporte014.jsp?v0="+"$usuario"+"&v1="+rol1;
	//window.open(url,"_blank");
	}
	
	
	
</script>  
</html>