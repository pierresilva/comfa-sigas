<?php
/* @Autor	Ing. Orlando Puentes
 * @fecha	julio 10 2011
 * @objeto	Reporte de gesti�n de radicaciones - afiliaciones empresas
 */
if ( !isset( $_SESSION ['USUARIO'] ) ) {session_start();}
if(empty($_SESSION['LOGUIADO'])){
	header("Location:http://10.10.1.121/sigas/error.php"); 
}
ini_set("display_errors",'1');
$raiz=$_SESSION['RAIZ'];
chdir($raiz);
$usuario=$_SESSION['USUARIO'];
include("config.php");
include("centroReportes/ReporteController.php");

$objReporte = new ReporteController();
$parametros = array(
					array(
							"nombre" => "usuario",
						 	"tipo" => "String",
						 	"valor" => $usuario));
$objReporte->generarReporte("reporte001",$parametros,'/seguridad/roles/');
?>
