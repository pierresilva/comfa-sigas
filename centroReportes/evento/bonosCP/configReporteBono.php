<?php
/* autor:       orlando puentes
 * fecha:       28/08/2012
 * objetivo:    configuracion de los reportes de radicacion
 **********************************************************
 configuracion
 tipo 
 1. con tipo de radicacion
 2. solo fecha
 3. agencias
 
*/

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$usuario=$_SESSION['USUARIO'];
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}
$tipo=$_REQUEST['tipo'];
$op=$_REQUEST['op'];
$titulo='...';
$rept=$_REQUEST['tit'];
switch($rept){
	case 1:$titulo='Directorio de Empresas Activas'; break;
	case 2:$titulo='Saldo Fondo Bono Computadores Aprender'; break;
	case 3:$titulo='Listado de Bonos Asignados General'; break;
	case 4:$titulo='Listado de Bonos por Agencia'; break;
	case 5:$titulo='Listado de Bonos por Fecha'; break;
	case 6:$titulo='Listado de Bonos por Fecha y Agencia'; break;
	case 7:$titulo='Listado de Bonos Anulados'; break;
	case 8:$titulo='Reimpresion de Bonos'; break;

}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Men&uacute; Reportes Bono</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link type="text/css" rel="stylesheet" href="../../../css/css.1.8/jquery-ui-1.8.17.custom.css" />
<link type="text/css" href="../../../css/Estilos.css" rel="stylesheet"/>
<link href="../../../css/marco.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="../../../js/1.6/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="../../../js/1.6/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="../../../js/comunes.js"></script>
<script type="text/javascript" src="js/reportes.js"></script>
</head>
<body>
	<center>
	<table width="100%" border="0">
	<tr>
	<td align="center" ><img src="../../../imagenes/logo_reporte.png" width="362" height="70"></td>
	</tr>
	<tr>
	<td align="center" ><?php echo $titulo ?></td>
	</tr>
	</table>
	<br>
<?php
if($tipo==1){
	
}
if($tipo==2){

}
if($tipo==3){
	
}
if($tipo==4){
?>
    <table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
	<tr>
    <th><strong>Parámetros de Configuracion Reporte</strong></th>
    </tr>
	<tr>
            <td style="text-align:center">Agencia: 
            	<select name="cmbAgencia" id="cmbAgencia">
            		<?php 
			     			$sql = "select * from aportes500";
			     			$rs = $db->querySimple($sql);
			     			while($row=$rs->fetchObject()){
			     				echo "<option value='$row->codigo'>$row->agencia</option>";
			     			}
			     		?>		
            	</select> 
            </td>
	</tr>
	<tr>
	  <td>&nbsp;</td>
	  </tr>
	<tr>
	  <td style="text-align:center">
      <label style="cursor:pointer" onClick="reporte004(1);"><img src="../../../imagenes/icono_pdf.png" width="32" height="32"></label>
      <label style="cursor:pointer" onClick="reporte004(2);"><img src="../../../imagenes/icono_excel.png" width="32" height="32"></label>
      </td>
	  </tr>
    </table>
<?php
}
if($tipo == 5){
	?>
	     <table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
			<tr>
		    	<th><strong>Parámetros de Configuracion Reporte</strong></th>
		    </tr>
			<tr><td style="text-align:center">Fecha Inicial: <input type="text" name="txtFechaI" id="txtFechaI"/></td></tr>
	        <tr><td style="text-align:center">Fecha Final: <input type="text" name="txtFechaF" id="txtFechaF"/></td></tr>
			<tr>
			  <td>&nbsp;</td>
			  </tr>
			<tr>
			  <td style="text-align:center">
		      <label style="cursor:pointer" onClick="reporte005(1);"><img src="../../../imagenes/icono_pdf.png" width="32" height="32"></label>
		      <label style="cursor:pointer" onClick="reporte005(2);"><img src="../../../imagenes/icono_excel.png" width="32" height="32"></label>
		      </td>
			  </tr>
	    </table>
<?php 
}
if($tipo == 6){
	?>
	<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
			<tr>
		    	<th><strong>Parámetros de Configuracion Reporte</strong></th>
		    </tr>
		    <tr>
		    	<td style="text-align:center">Agencia: 
		    		<select name="cmbAgencia" id="cmbAgencia">
            		<?php 
			     			$sql = "select * from aportes500";
			     			$rs = $db->querySimple($sql);
			     			while($row=$rs->fetchObject()){
			     				echo "<option value='$row->codigo'>$row->agencia</option>";
			     			}
			     		?>		
            		</select> 
		    	</td>
		    </tr>
			<tr><td style="text-align:center">Fecha Inicial: <input type="text" name="txtFechaI" id="txtFechaI"/></td></tr>
	        <tr><td style="text-align:center">Fecha Final: <input type="text" name="txtFechaF" id="txtFechaF"/></td></tr>
			<tr>
			  <td>&nbsp;</td>
			  </tr>
			<tr>
			  <td style="text-align:center">
		      <label style="cursor:pointer" onClick="reporte006(1);"><img src="../../../imagenes/icono_pdf.png" width="32" height="32"></label>
		      <label style="cursor:pointer" onClick="reporte006(2);"><img src="../../../imagenes/icono_excel.png" width="32" height="32"></label>
		      </td>
			  </tr>
	    </table>
<?php 
}
if($tipo == 7){

}
if($tipo==8){
?>
	<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
			<tr>
		    	<th><strong>Parámetros de Configuracion Reporte</strong></th>
		    </tr>
		    <tr><td style="text-align:center">Bono No.: <input type="text" name="txtIdBono" id="txtIdBono" /></td></tr>
			<tr><td>&nbsp;</td></tr>
			<tr>
			  <td style="text-align:center">
		      <label style="cursor:pointer" onClick="reporte008();"><img src="../../../imagenes/icono_pdf.png" width="32" height="32"></label></td>
			  </tr>
	    </table>
<?php 
}
?>
<input type="hidden" id="txtusuario" name="txtusuario" value="<?php echo $usuario; ?>" /> 
<input type="hidden" id="txtRept" name="txtRept" value="<?php echo $rept; ?>" />
</body>

</html>