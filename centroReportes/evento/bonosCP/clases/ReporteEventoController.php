<?php
ini_set("display_errors",1);
class ReporteEventoController{
	var $strConexion	= "";
	var $raiz 			= "";
	var $dirReportes	= "evento/bonosCP/";
	var $classJava 		= null;
	var $rgIfx 			= null;
	var $dMang 			= null;
	var $conn 			= null;
	var $compileManager = null;
	var $fillManager	= null;
	var $exportManager	= null;
	
	function ReporteEventoController(){
		$this->strConexion = CADENA_CONEXION_INFORMIX;
		//echo $this->strConexion;exit();
		$this->raiz = DIRECTORIO_RAIZ;
		$this->dirReportes = DIRECTORIO_CENTRO_REPORTES . $this->dirReportes;
		$this->classJava = new Java("java.lang.Class");
		$this->rgIfx = $this->classJava->forName("com.informix.jdbc.IfxDriver");
		$this->rgIfx->newInstance();
		$this->dMang = new Java("java.sql.DriverManager");
		$this->conn = $this->dMang->getConnection($this->strConexion);
		$this->compileManager = new Java("net.sf.jasperreports.engine.JasperCompileManager");
		$this->fillManager = new Java("net.sf.jasperreports.engine.JasperFillManager");
		
	}
	
	function generarReporte($nombreReporte,$parametros = array()){
		chdir($this->raiz . $this->dirReportes);
		$objHashMap = new Java("java.util.HashMap");
		if(is_array($parametros) && count($parametros)>0){
			foreach($parametros as $key => $parametro){
				switch($parametro["tipo"]){
					case "Long":
						$param = new Java("java.lang.Long", $parametro["valor"]);
						break;
					case "Number":
						$param = new Java("java.lang.Number", $parametro["valor"]);
						break;
					case "Date":
						// Se debe recibir un arreglo de la forma array(mm,dd,aaaa)
						$fec = mktime(0,0,0,$parametro["valor"][0],$parametro["valor"][1],$parametro["valor"][2])*1000;
						$param = new Java("java.util.Date", $fec);
						break;
					default:
						$param = strval($parametro["valor"]);
						break;
				}
				$objHashMap->put($parametro["nombre"],$param);
			}
		}

		$rp = realpath($nombreReporte.".jrxml");
		$report = $this->compileManager->compileReport($rp);
		$jasperPrint = $this->fillManager->fillReport($report, $objHashMap, $this->conn);
		$this->exportManager = new Java("net.sf.jasperreports.engine.JasperExportManager");
		$outputPath = realpath(".")."/".$nombreReporte.".pdf";
		$this->exportManager->exportReportfirstdfFile($jasperPrint, $outputPath);
		if (file_exists($nombreReporte.".pdf")){
			header("Content-disposition: attachment; filename=$nombreReporte.pdf");
			header("Content-Type: application/pdf");
			header("Content-Transfer-Encoding: binary");
			header("Content-Length: ". @filesize("$nombreReporte.pdf"));
			header("Pragma: no-cache");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Expires: 0");
			set_time_limit(0);
			@readfile("$nombreReporte.pdf") or die("Hubo problemas al tratar de abrir el archivo.");
		}
	}
	
}

?>