
<?php 
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
include_once $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'config.php';
@$fechaHoy=date("Y/m/d");
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';

/**************************************************************************************************************/
/*************Incluimos el archibo reportInPen.class.php el cual contiene la clase*****************************/
include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'reportInPen.class.php';
ini_set("memory_limit", "-1");
set_time_limit(130);
require_once '../../lib/dompdf/dompdf_config.inc.php';
$objClase=new reportInPen();//Instanciamos la clase y creamos el Objeto.
$dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
$mes = array("","Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
date_default_timezone_set("America/Bogota");
date_default_timezone_get();

/*******************obtener parametros por la URL.***********************************/
@$comunicacion=$_REQUEST['comunicacion'];
@$numerodoc=$_REQUEST['numerodoc'];
@$agencia=$_REQUEST['agencia'];
@$tipoAfi = $_REQUEST['tipoafi'];
@$fechaInic = $_REQUEST['fechaini'];
@$fechaFins = $_REQUEST['fechafin'];
/************************************************************************************/


/**************************************************************************************************************/
/****************************Script para el encabezado y pie de pagina.****************************************/
$script= '<script type="text/php">
            if( isset($pdf) ) {
                    
                    //header y footer
                    $obj = $pdf->open_object();

                    //Agregamos texto Al Footer
                    $font = Font_Metrics::get_font("Helvetica");
                    $fontsize = 10;
                    $Tamañoletra = 20;
                    $fontcolor = array(0.4,0.4,0.4);
                    
                    

                    //Agregamos imagen, texto al Header.
                    $imageHEADER = "http://localhost/sigas/sigasReportinpen/img/confamiliar.png";
                    $imageFOOTER = "http://localhost/sigas/sigasReportinpen/img/membrete_inferior.png";
                    //Parametros Imagen: URL img,extencion(jpg o png), posicion x, posicion y, width, height.
                    $pdf->image($imageHEADER, "png", 40, 40, 180, 60);
                    $pdf->image($imageFOOTER, "png", -2, 670, 620, 125);
                    
                    

                    $pdf->close_object();
                    $pdf->add_object($obj, "all");
            }
        </script>';

/**************************************************************************************************************/
/****************************Estilos para configurar las margenes del pdf.*************************************/
$styles='   <style>
                @page { 
                    margin-top: 151px;
                    margin-bottom: 140px;
                    margin-left: 113px;
                    margin-right: 90px;

                }
                    
                    
                body{
                    text-align: justify;
                    font: 15px arial;
                    font-family: 15px arial;
                    font-size:15px;
                    align:justify;
                }
                div{
                    margin:auto;
                    text-align: justify;
                    font: 14px
                }
                p{ 
                    text-align:justify;
                    font: 14px
                }
                
                .img{
                    width: 190px;
                    height: 80px;
                }
            </style>';


/**************************************************************************************************************/
/************Variable la cual contiene HTML para que la libreria la renderize y cree el pdf.*******************/
if($numerodoc != ''){
    $datos = $objClase->mostrarCartaAceptacionDocumento($agencia, $numerodoc);
    $rows=mssql_fetch_array($objClase->mostrarCartaAceptacionDocumento($agencia, $numerodoc));
     if($rows != 0){
         $rows=mssql_fetch_array($datos);
         if ($rows['codTipoAfiliado']== 21){
             $html = '<html lang="es">
              <head>
              <meta >
              <title>Cartas de Aceptacion</title>
              <body>
              ';
            //Agregamos los estilos y los scrip al html.
            $html .=$script.$styles;



            //Contenido
            $html .="
            <div style='page-break-after: always;'>
            <p >
            DF-07-".$comunicacion."<br/>"
            .$rows['agencia'].",   ".date('d')." de ".$mes[date('n')]." de ".date('Y')." <br/><br/> <br/>


            Señor(a) <br/> 
            <b>".$rows['pnombre']." ".$rows['snombre']." ".$rows['papellido']." ".$rows['sapellido']."</b> <br/>
            ".$rows['direccion']." <br/>
            <b>".$rows['municipio']." (".$rows['departmento'].") </b><br/> <br/>

            Asunto: Carta de aceptación <br/><br/>

    Cordial Saludo: <br/><br/>

    Es un motivo de agrado comunicarle que ha sido aprobada su afiliación como trabajador que ha salido de la vida laboral por causa de una pensión de vejez, y que demostró que estuvo afiliado a una Caja de Compensación Familiar por 25 años o más, pudiendo gozar de la tarifa más baja en los programas de capacitación, recreación y turismo social. <br/> <br/>
    Por lo anterior lo invitamos a que se acerque a nuestras oficinas de Aportes y Subsidio ubicadas en el segundo piso del Hipermercado de Comfamiliar en el horario de lunes a viernes de 8:00 a 12.00 PM y de 2:00 a 6.00 PM y sábados en el horario de 9.00 a 1:00 PM a reclamar la tarjeta que lo acredita como afiliado en dicha modalidad y le permite a Ud. y a su grupo familiar afiliado incluyendo su cónyuge y/o compañera permanente gozar de la tarifa más baja en los programas  de capacitación, recreación y turismo social. Así: <br/> <br/>

        <b>1. Obtendrán tarifas especiales en Neiva, Garzón, La Plata y Pitalito para
               afiliados de acuerdo a la categoría en los siguientes servicios: </p><br/> <br/>
            <ul>
                <li><b>Centros vacacionales y Recreativos</b> ( Playa Juncal, Centro Turístico
                   Las Termales de Rivera, Sede Social y Deportiva Los Cámbulos, Centro
                   Recreacional los Lagos, Parque Extremo, con recreación dirigida).<br/></li>

                <li><b>Hotel Timanco</b> (Pitalito)<br/></li>
                <li><b>Educación</b> (Centro de Formación Empresarial)<br/></li>
                <li><b>Hacienda Campestre Años Dorados</b>(Programa adulto mayor)<br/></li>
                <li><b>Programas Especiales a Discapacitados</b><br/></li>
                <li><b>Jardín Infantil Panorama</b><br/></li>
                <li><b>CDI Bosques de San Luis</b><br/></li>
                <li><b>Escuelas Deportivas</b> (Baloncesto, natación, futbol, voleibol, ajedrez, patinaje).<br/></li>
                <li><b>Olimpiadas Empresariales</b><br/><li/>
                <li><b>Actividades Experienciales Empresariales</b><br/><br/></li>
            <ul/>
           <b> 2. Supermercados</b> (Neiva , Garzón)<br/>
           <b> 3. Droguerías </b>(Neiva, Garzón, La Plata y Pitalito)<br/>
           <b> 4. Famitienda </b>(Gigante)<br/>
           <b> 5. UIS </b>“Unidad Integral de Servicios” (Centro de Formación Empresarial, Salud<br/>
               1º Nivel, Droguería, Gimnasio, Biblioteca, Bebeteca) en el Norte y Sur de la<br/>
               Ciudad.<br/>
           <b> 6. Gimnasios </b>(Cámbulos), UIS del Sur Y Norte.<br/><br/>
           <b> 7. Agencia de Empleo</b><br/><br/>

            De igual forma le hacemos la invitación a que ingrese a nuestra página web www.comfamiliarhuila.com para que se entere de los programas y eventos que ofrece nuestra Caja a los afiliados.<br/><br/><br/><br/>


            Agradeciendo la confianza depositada en nuestra Caja.<br/><br/><br/><br/>


            Atentamente,<br/><br/><br/><br/>

            <img src='../../img/firmaCartas.png' class='img'/><br/>
            <b>HAROLD YESID SALAMANCA FALLA</b><br/>
            Director Administrativo<br/>
            Proyecto: Jazmín Ospina G/Y.M.L.
            </p></div>";

         }else If($rows['codTipoAfiliado']== 4209){
             $html = '<html lang="es">
              <head>
              <meta >
              <title>Cartas de Aceptacion</title>
              <body>
              ';
            //Agregamos los estilos y los scrip al html.
            $html .=$script.$styles;



            //Contenido
            $html .="
            <div style='page-break-after: always;'>
            <p >DF-07-".$comunicacion."<br/>"
            .$rows['agencia'].",   ".date('d')." de ".$mes[date('n')]." de ".date('Y')." <br/><br/> <br/>


            Señor(a) <br/> 
            <b>".$rows['pnombre']." ".$rows['snombre']." ".$rows['papellido']." ".$rows['sapellido']."</b> <br/>
            ".$rows['direccion']." <br/>
            <b>".$rows['municipio']." (".$rows['departmento'].")</b> <br/> <br/>

            Asunto: Carta de aceptación <br/><br/>

    Cordial Saludo: <br/><br/>

    Es un motivo de agrado comunicarle que ha sido aprobada su afiliación como pensionado de régimen especial, sin cotización alguna y cuya mesada es de hasta 1.5 salarios mínimos, para el acceso a los servicios a que tienen derecho los trabajadores activos en materia de recreación, deporte y cultura. <br/> <br/>
    Por lo anterior lo invitamos a que se acerque a nuestras oficinas de Aportes y Subsidio ubicadas en el segundo piso del Hipermercado de Comfamiliar en el horario de lunes a viernes de 8:00 a 12.00 PM y de 2:00 a 6.00 PM y sábados en el horario de 9.00 a 1:00 PM a reclamar la tarjeta que lo acredita como afiliado en dicha modalidad y le permite a Ud. y a su grupo familiar afiliado incluyendo su cónyuge y/o compañera permanente gozar de la tarifa más baja en los programas  de capacitación, recreación y turismo social. Así: <br/> <br/>


            <b>1. Obtendrán tarifas especiales en Neiva, Garzón, La Plata y Pitalito para
               afiliados de acuerdo a la categoría en los siguientes servicios: </p><br/> <br/>
            <ul>
                <li><b>Centros vacacionales y Recreativos</b> ( Playa Juncal, Centro Turístico
                   Las Termales de Rivera, Sede Social y Deportiva Los Cámbulos, Centro
                   Recreacional los Lagos, Parque Extremo, con recreación dirigida).<br/></li>

                <li><b>Hotel Timanco</b> (Pitalito)<br/></li>
                <li><b>Educación</b> (Centro de Formación Empresarial)<br/></li>
                <li><b>Hacienda Campestre Años Dorados</b>(Programa adulto mayor)<br/></li>
                <li><b>Programas Especiales a Discapacitados</b><br/></li>
                <li><b>Jardín Infantil Panorama</b><br/></li>
                <li><b>CDI Bosques de San Luis</b><br/></li>
                <li><b>Escuelas Deportivas</b> (Baloncesto, natación, futbol, voleibol, ajedrez, patinaje).<br/></li>
                <li><b>Olimpiadas Empresariales</b><br/><li/>
                <li><b>Actividades Experienciales Empresariales</b><br/><br/></li>
            <ul/>
           <b> 2. Supermercados</b> (Neiva , Garzón)<br/>
           <b> 3. Droguerías </b>(Neiva, Garzón, La Plata y Pitalito)<br/>
           <b> 4. Famitienda </b>(Gigante)<br/>
           <b> 5. UIS </b>“Unidad Integral de Servicios” (Centro de Formación Empresarial, Salud<br/>
               1º Nivel, Droguería, Gimnasio, Biblioteca, Bebeteca) en el Norte y Sur de la<br/>
               Ciudad.<br/>
           <b> 6. Gimnasios </b>(Cámbulos), UIS del Sur Y Norte.<br/><br/>
           <b> 7. Agencia de Empleo</b><br/><br/>

            De igual forma le hacemos la invitación a que ingrese a nuestra página web www.comfamiliarhuila.com para que se entere de los programas y eventos que ofrece nuestra Caja a los afiliados.<br/><br/><br/><br/>


            Agradeciendo la confianza depositada en nuestra Caja.<br/><br/><br/><br/>


            Atentamente,<br/><br/><br/><br/>

            <img src='../../img/firmaCartas.png' class='img'/><br/>
            <b>HAROLD YESID SALAMANCA FALLA</b><br/>
            Director Administrativo<br/>
            Proyecto: Jazmín Ospina G/Y.M.L.
            </p></div>";
         }
         //Intanciar la Clase DOMPDF
        $mipdf = new DOMPDF();

        /**********************************************************************/
        /****Definimos el tamaño y orientación del papel que queremos.*********/
        /****O por defecto cogerá el que está en el fichero de configuración.**/ 
        /****@params("tipo_papel,"orientacion") *******************************/ 
        $paper_size = array(0,0,612,792);
        $mipdf->set_paper($paper_size, "portrait");

        //Cargar Contenido HTML
        $mipdf->load_html(utf8_decode($html));



        ini_set("memory_limite", "-1");
        /*Posteriormente con las siguientes líneas renderizamos o 
         * convertimos el documento a pdf y luego enviamos el fichero 
         * al navegador para que sea descargado por el cliente.*/

        //Renderizamos el documento PDF.
        $mipdf->render();

        header('Content-type: application/pdf'); //Ésta es simplemente la cabecera para que el navegador interprete todo como un PDF

        echo $mipdf->output(); //Y con ésto se manda a imprimir el contenido del pdf

        //Enviamos el fichero PDF al navegador.
        $mipdf->stream('cartaPencionados.pdf');

     }else{
        echo "<html lang='es'>
                <head>
                    <meta >
                    <link rel='stylesheet' type='text/css' href='../../css/wow-alert.css'>
                    <link rel='stylesheet' type='text/css' href='../../css/example.css'>
              <title>Cartas de Aceptacion</title>
                    <body>
                    </body> 
                    <script type='text/javascript' src='../../js/jquery-2.1.3.js'></script>
                    <script type='text/javascript' src='../../js/wow-alert.js'></script>"
                 . "<script>"
                 . "      "
                         . "alert('Numero de documento no encontrado, orpima OK para volver al menu anterior.');"
                         . "$('#mensage').unbind('click');
                          $('#mensage').click(function(){
                              window.close();
                          });"
                  . "</script>
                    </html>";
     }
}else{
   $datos = $objClase->mostrarCartaAceptacionFechas($agencia, $tipoAfi, $fechaInic, $fechaFins);
   $rows=mssql_fetch_array($objClase->mostrarCartaAceptacionFechas($agencia, $tipoAfi, $fechaInic, $fechaFins));
     if($rows != 0){
         $html = '<html lang="es">
              <head>
              <meta >
              <title>Cartas de Aceptacion</title>
              <body>
              
              ';
            //Agregamos los estilos y los scrip al html.
            $html .=$script.$styles;
         while($rows=mssql_fetch_array($datos)){
             
         if ($rows['codTipoAfiliado']== 21){
            $html .="
            <div style='page-break-after: always;'>"
            . "<p >DF-07-".$comunicacion."<br/>"
            .$rows['agencia'].",   ".date('d')." de ".$mes[date('n')]." de ".date('Y')." <br/><br/> <br/>


            Señor(a) <br/> 
            <b>".$rows['pnombre']." ".$rows['snombre']." ".$rows['papellido']." ".$rows['sapellido']."</b> <br/>
            ".$rows['direccion']." <br/>
            <b>".$rows['municipio']." (".$rows['departmento'].") </b><br/> <br/>

            Asunto: Carta de aceptación <br/><br/>

    Cordial Saludo: <br/><br/>

    Es un motivo de agrado comunicarle que ha sido aprobada su afiliación como trabajador que ha salido de la vida laboral por causa de una pensión de vejez, y que demostró que estuvo afiliado a una Caja de Compensación Familiar por 25 años o más, pudiendo gozar de la tarifa más baja en los programas de capacitación, recreación y turismo social. <br/> <br/>
    Por lo anterior lo invitamos a que se acerque a nuestras oficinas de Aportes y Subsidio ubicadas en el segundo piso del Hipermercado de Comfamiliar en el horario de lunes a viernes de 8:00 a 12.00 PM y de 2:00 a 6.00 PM y sábados en el horario de 9.00 a 1:00 PM a reclamar la tarjeta que lo acredita como afiliado en dicha modalidad y le permite a Ud. y a su grupo familiar afiliado incluyendo su cónyuge y/o compañera permanente gozar de la tarifa más baja en los programas  de capacitación, recreación y turismo social. Así: <br/> <br/>

        <b>1. Obtendrán tarifas especiales en Neiva, Garzón, La Plata y Pitalito para
               afiliados de acuerdo a la categoría en los siguientes servicios: </p><br/> <br/>
            <ul>
                <li><b>Centros vacacionales y Recreativos</b> ( Playa Juncal, Centro Turístico
                   Las Termales de Rivera, Sede Social y Deportiva Los Cámbulos, Centro
                   Recreacional los Lagos, Parque Extremo, con recreación dirigida).<br/></li>

                <li><b>Hotel Timanco</b> (Pitalito)<br/></li>
                <li><b>Educación</b> (Centro de Formación Empresarial)<br/></li>
                <li><b>Hacienda Campestre Años Dorados</b>(Programa adulto mayor)<br/></li>
                <li><b>Programas Especiales a Discapacitados</b><br/></li>
                <li><b>Jardín Infantil Panorama</b><br/></li>
                <li><b>CDI Bosques de San Luis</b><br/></li>
                <li><b>Escuelas Deportivas</b> (Baloncesto, natación, futbol, voleibol, ajedrez, patinaje).<br/></li>
                <li><b>Olimpiadas Empresariales</b><br/><li/>
                <li><b>Actividades Experienciales Empresariales</b><br/><br/></li>
            <ul/>
           <b> 2. Supermercados</b> (Neiva , Garzón)<br/>
           <b> 3. Droguerías </b>(Neiva, Garzón, La Plata y Pitalito)<br/>
           <b> 4. Famitienda </b>(Gigante)<br/>
           <b> 5. UIS </b>“Unidad Integral de Servicios” (Centro de Formación Empresarial, Salud<br/>
               1º Nivel, Droguería, Gimnasio, Biblioteca, Bebeteca) en el Norte y Sur de la<br/>
               Ciudad.<br/>
           <b> 6. Gimnasios </b>(Cámbulos), UIS del Sur Y Norte.<br/><br/>
           <b> 7. Agencia de Empleo</b><br/><br/>

            De igual forma le hacemos la invitación a que ingrese a nuestra página web www.comfamiliarhuila.com para que se entere de los programas y eventos que ofrece nuestra Caja a los afiliados.<br/><br/><br/><br/>


            Agradeciendo la confianza depositada en nuestra Caja.<br/><br/><br/><br/>


            Atentamente,<br/><br/><br/><br/>

            <img src='../../img/firmaCartas.png' class='img'/><br/>
            <b>HAROLD YESID SALAMANCA FALLA</b><br/>
            Director Administrativo<br/>
            Proyecto: Jazmín Ospina G/Y.M.L.
            </p></div>";

         }else If($rows['codTipoAfiliado']== 4209){
             
            //Contenido
            $html .="
            <div style='page-break-after: always;'>"
            . "<p >DF-07-".$comunicacion."<br/>"
            .$rows['agencia'].",   ".date('d')." de ".$mes[date('n')]." de ".date('Y')." <br/><br/> <br/>


            Señor(a) <br/> 
            <b>".$rows['pnombre']." ".$rows['snombre']." ".$rows['papellido']." ".$rows['sapellido']."</b> <br/>
            ".$rows['direccion']." <br/>
            <b>".$rows['municipio']." (".$rows['departmento'].")</b> <br/> <br/>

            Asunto: Carta de aceptación <br/><br/>

    Cordial Saludo: <br/><br/>

    Es un motivo de agrado comunicarle que ha sido aprobada su afiliación como pensionado de régimen especial, sin cotización alguna y cuya mesada es de hasta 1.5 salarios mínimos, para el acceso a los servicios a que tienen derecho los trabajadores activos en materia de recreación, deporte y cultura. <br/> <br/>
    Por lo anterior lo invitamos a que se acerque a nuestras oficinas de Aportes y Subsidio ubicadas en el segundo piso del Hipermercado de Comfamiliar en el horario de lunes a viernes de 8:00 a 12.00 PM y de 2:00 a 6.00 PM y sábados en el horario de 9.00 a 1:00 PM a reclamar la tarjeta que lo acredita como afiliado en dicha modalidad y le permite a Ud. y a su grupo familiar afiliado incluyendo su cónyuge y/o compañera permanente gozar de la tarifa más baja en los programas  de capacitación, recreación y turismo social. Así: <br/> <br/>


            <b>1. Obtendrán tarifas especiales en Neiva, Garzón, La Plata y Pitalito para
               afiliados de acuerdo a la categoría en los siguientes servicios: </p><br/> <br/>
            <ul>
                <li><b>Centros vacacionales y Recreativos</b> ( Playa Juncal, Centro Turístico
                   Las Termales de Rivera, Sede Social y Deportiva Los Cámbulos, Centro
                   Recreacional los Lagos, Parque Extremo, con recreación dirigida).<br/></li>

                <li><b>Hotel Timanco</b> (Pitalito)<br/></li>
                <li><b>Educación</b> (Centro de Formación Empresarial)<br/></li>
                <li><b>Hacienda Campestre Años Dorados</b>(Programa adulto mayor)<br/></li>
                <li><b>Programas Especiales a Discapacitados</b><br/></li>
                <li><b>Jardín Infantil Panorama</b><br/></li>
                <li><b>CDI Bosques de San Luis</b><br/></li>
                <li><b>Escuelas Deportivas</b> (Baloncesto, natación, futbol, voleibol, ajedrez, patinaje).<br/></li>
                <li><b>Olimpiadas Empresariales</b><br/><li/>
                <li><b>Actividades Experienciales Empresariales</b><br/><br/></li>
            <ul/>
           <b> 2. Supermercados</b> (Neiva , Garzón)<br/>
           <b> 3. Droguerías </b>(Neiva, Garzón, La Plata y Pitalito)<br/>
           <b> 4. Famitienda </b>(Gigante)<br/>
           <b> 5. UIS </b>“Unidad Integral de Servicios” (Centro de Formación Empresarial, Salud<br/>
               1º Nivel, Droguería, Gimnasio, Biblioteca, Bebeteca) en el Norte y Sur de la<br/>
               Ciudad.<br/>
           <b> 6. Gimnasios </b>(Cámbulos), UIS del Sur Y Norte.<br/><br/>
           <b> 7. Agencia de Empleo</b><br/><br/>

            De igual forma le hacemos la invitación a que ingrese a nuestra página web www.comfamiliarhuila.com para que se entere de los programas y eventos que ofrece nuestra Caja a los afiliados.<br/><br/><br/><br/>


            Agradeciendo la confianza depositada en nuestra Caja.<br/><br/><br/><br/>


            Atentamente,<br/><br/><br/><br/>

            <img src='../../img/firmaCartas.png' class='img'/><br/>
            <b>HAROLD YESID SALAMANCA FALLA</b><br/>
            Director Administrativo<br/>
            Proyecto: Jazmín Ospina G/Y.M.L.
            </p></div>";
         }
        
    }
     //Intanciar la Clase DOMPDF
        $mipdf = new DOMPDF();

        /**********************************************************************/
        /****Definimos el tamaño y orientación del papel que queremos.*********/
        /****O por defecto cogerá el que está en el fichero de configuración.**/ 
        /****@params("tipo_papel,"orientacion") *******************************/ 
        $paper_size = array(0,0,612,792);
        $mipdf->set_paper($paper_size, "portrait");

        //Cargar Contenido HTML
        $mipdf->load_html(utf8_decode($html));



        ini_set("memory_limit", "-1");
        /*Posteriormente con las siguientes líneas renderizamos o 
         * convertimos el documento a pdf y luego enviamos el fichero 
         * al navegador para que sea descargado por el cliente.*/

        //Renderizamos el documento PDF.
        $mipdf->render();

        header('Content-type: application/pdf'); //Ésta es simplemente la cabecera para que el navegador interprete todo como un PDF

        echo $mipdf->output(); //Y con ésto se manda a imprimir el contenido del pdf

        //Enviamos el fichero PDF al navegador.
        $mipdf->stream('cartaPencionados.pdf');
     }else{
        echo "<html lang='es'>
                <head>
                    <meta >
                    <link rel='stylesheet' type='text/css' href='../../css/wow-alert.css'>
                    <link rel='stylesheet' type='text/css' href='../../css/example.css'>


              <title>Cartas de Aceptacion</title>
                    <body>
                    </body> 
                    <script type='text/javascript' src='../../js/jquery-2.1.3.js'></script>
                    <script type='text/javascript' src='../../js/wow-alert.js'></script>"
                 . "<script>"
                 . "      "
                         . "alert('En el rango de fecas especificado no se encontro ningun registro, orpima OK para volver al menu anterior.');"
                         . "$('#mensage').unbind('click');
                          $('#mensage').click(function(){
                              window.close();
                          });"
                  . "</script>
                    </html>";
     } 
}             
         
    


    ?>