
<?php 
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
include_once $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'config.php';
$fechaHoy=date("Y/m/d");
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';

/**************************************************************************************************************/
/*************Incluimos el archibo reportInPen.class.php el cual contiene la clase*****************************/
include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'reportInPen.class.php';
ini_set("memory_limit", "-1");
set_time_limit(130);
require_once '../../lib/dompdf/dompdf_config.inc.php';
$objClase=new reportInPen();//Instanciamos la clase y creamos el Objeto.
$dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
$mes = array("","Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
date_default_timezone_set("America/Bogota");
date_default_timezone_get();

/*******************obtener parametros por la URL.***********************************/
$usuasesion=$_REQUEST['usuasesion'];
$fecha_inicio=$_REQUEST['fechainicio'];
$fecha_fin=$_REQUEST['fechafin'];
$agenci=$_REQUEST['agencia'];
$tipo_radicacion=$_REQUEST['tiporadi'];
$users=$_REQUEST['user'];
$tipoAfiliadon=$_REQUEST['tipoAfiliado'];
/************************************************************************************/

/**************************************************************************************************************/
/*************Obtenemos datos atrabes del metodo para recorrerlos en el arreglo con un while y crear tabla*****/
$datos = $objClase->mostrar_datos_reporte00_01($fecha_inicio, $fecha_fin, $agenci, $users, $tipo_radicacion, $tipoAfiliadon);


/**************************************************************************************************************/
/******************************Obtenemos datos para traer tipo de radicación***********************************/
$datos2 = $objClase->mostrar_datos_reporte00_01($fecha_inicio, $fecha_fin, $agenci, $users, $tipo_radicacion, $tipoAfiliadon);


/**************************************************************************************************************/
/****************************Script para el encabezado y pie de pagina.****************************************/
$script= '<script type="text/php">
            if( isset($pdf) ) {
                    
                    //header y footer
                    $obj = $pdf->open_object();

                    //Agregamos texto Al Footer
                    $font = Font_Metrics::get_font("Helvetica");
                    $fontsize = 10;
                    $Tamañoletra = 20;
                    $fontcolor = array(0.4,0.4,0.4);
                    
                    //Parametros Text: mover horizontal, mover vertical, texto, fuente letra, tamaño letra, color letra.
                    $pdf->page_text(60, $pdf->get_height()-25, "Propiedad de Comfamiliar Huila", $font, $fontsize, $fontcolor);
                    $pdf->page_text($pdf->get_width()-100, $pdf->get_height()-25, "Pagina {PAGE_NUM} de {PAGE_COUNT}", $font, $fontsize, $fontcolor);

                    //Agregamos imagen, texto al Header.
                    $image = "http://localhost/sigas/sigasReportinpen/img/confamiliar.png";
                    //Parametros Imagen: URL img,extencion(jpg o png), posicion x, posicion y, width, height.
                    $pdf->image($image, "png", 25, 10, 255, 77);
                    
                    //Parametros Text: mover horizontal, mover vertical, texto, tipo letra, tamaño letra, color letra.
                    $pdf->page_text(560, $pdf->get_height()-545, "LISTADO DE RADICACIONES", $font, 13, $fontcolor);
                    $pdf->page_text(545, $pdf->get_height()-522, "DESDE: '.$fecha_inicio.' HASTA: '.$fecha_fin.'", $font, 13, $fontcolor);
                        $pdf->page_text(730, $pdf->get_height()-490, "Usuario: '.$usuasesion.'", $font, 13, $fontcolor);
                    $pdf->page_text(30, $pdf->get_height()-500, "Fecha del Reporte: '.$dias[date('w')].'  '.date("d").' de '.$mes[date("n")].' de '.date("Y").'   '.date("g:i a",  time()).'", $font, $fontsize, $fontcolor);

                    $pdf->close_object();
                    $pdf->add_object($obj, "all");
            }
        </script>';

/**************************************************************************************************************/
/****************************Estilos para configurar las margenes del pdf.*************************************/
$styles='   <style>
                @page { margin: 150px auto 19px ;
                margin-bottom: 20px; 
                }
                img {margin:100px;padding:100px}
                body{font: 87% arial; }
                big{font: 86% arial;}
                body{
                    text-align: justify;
                    font: 10.5px arial;
                    font-family: 10.5px arial;
                    font-size:10.5px;
                    align:center;
                }
            </style>';


/**************************************************************************************************************/
/************Variable la cual contiene HTML para que la libreria la renderize y cree el pdf.*******************/
$html = '<html lang="es">
      <head>
          <meta >
          <title>Listado de Radicacion</title>
          <body>
          ';
//Agregamos los estilos y los scrip al html.
$html .=$script.$styles;
 
$rows=mssql_fetch_array($datos2);        

//Contenido
$html .=  ' <div>
    
    <div class="panel panel-success">
        <big>AGENCIA:   '.$rows["agencia"].'<br/>
        '.utf8_decode("TIPO  DE  RADICACIÓN:   ".$rows["detalledefinicion"]).
        '</big>
        <div class="panel-body">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>No. </th>
                        <th>Rad. </th>
                        <th>Fecha </th>
                        <th>Hora </th>
                        <th>Tiempo </th>
                        <th>'.utf8_decode("Número ").'</th>
                        <th>Nombres </th>
                        <th>'.utf8_decode("notas").'</th>
                        <th>Tipo Afiliado </th>
                        <th>Estado </th>
                        <th>Folios </th>
                        <th>Usuario </th>
                    </tr>
                </thead>
                <tbody>';


 ?>
 <?php 
 
    /**************************************************************************************************************/
    /*********************Siclo que crea los campos a la tabla extraidos desde la base de datos.*******************/ 
     $contador = 1;
     while ($row = mssql_fetch_array($datos)){
        
        $html .= '<tr>
            <td >'.$contador.'</td>
            <td >'.$row['idradicacion'].'</td>
            <td >'.$row['fecharadicacion'].'</td>
            <td >'.$row['horainicio'].'</td>
            <td >'.$row['tiempo'].'</td>
            <td >'.$row['numero'].'</td>
            <td >'.$row['papellido'].' '.$row['pnombre'].'</td>
            <td >'.$row['notas'].'</td>
            <td >'.$row['tipo_afiliado'].'</td>
            <td >'.$row['estado'].'</td>
            <td >'.$row['folios'].'</td>
            <td >'.$row['usuario'].'</td>
        </tr>';
         //echo $row['edad'];           
         $contador = $contador + 1;           
     }
 ?>                   
  
 <?php 
        
 //Pie de Pagina
 $html .= '</tbody>
                 </table>
             </div>
         </div>
     </div> 
         
     </div>
     
     </body>
  </html>';                   
    // echo $html;       
    
    //Intanciar la Clase DOMPDF
    $mipdf = new DOMPDF();
  
    /**********************************************************************/
    /****Definimos el tamaño y orientación del papel que queremos.*********/
    /****O por defecto cogerá el que está en el fichero de configuración.**/ 
    /****@params("tipo_papel,"orientacion") *******************************/ 
    $mipdf->set_paper("A4", "landscape");
    
    //Cargar Contenido HTML
    $mipdf->load_html($html);
    
    
    
    ini_set("memory_limit", "1000M");
    /*Posteriormente con las siguientes líneas renderizamos o 
     * convertimos el documento a pdf y luego enviamos el fichero 
     * al navegador para que sea descargado por el cliente.*/
    
    //Renderizamos el documento PDF.
    $mipdf->render();
    
    header('Content-type: application/pdf'); //Ésta es simplemente la cabecera para que el navegador interprete todo como un PDF
    
    echo $mipdf->output(); //Y con ésto se manda a imprimir el contenido del pdf
    
    //Enviamos el fichero PDF al navegador.
    $mipdf->stream('reporte001_01.pdf');

    ?>