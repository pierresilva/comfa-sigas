﻿
<?php 
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
include_once $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'config.php';
$fechaHoy=date("Y/m/d");
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';

/**************************************************************************************************************/
/*************Incluimos el archibo reportInPen.class.php el cual contiene la clase*****************************/
include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'reportInPen.class.php';
ini_set("memory_limit", "128M");
$objClase=new reportInPen();//Instanciamos la clase y creamos el Objeto.
$dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
$mes = array("","Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
date_default_timezone_set("America/Bogota");
date_default_timezone_get();

/*obtener parametros por la URL. */
$usuasesion=$_REQUEST['usuasesion'];
$fecha_inicio=$_REQUEST['fechainicio'];
$fecha_fin=$_REQUEST['fechafin'];
$agenci=$_REQUEST['agencia'];
$tipo_radicacion=$_REQUEST['tiporadi'];
$users=$_REQUEST['user'];
$tipoAfiliadon=$_REQUEST['tipoAfiliado'];

//Crear excel.
    header("Content-Type: application/vnd.ms-excel");

    header("Expires: 0");
    
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    //Nombre del archivo con extencion xls.
    header("content-disposition: attachment;filename=reporte001_01.xls");

//La bariable $html almacena el codigo html para despues ser renderizado y exportado a EXCEL.
//Encabezado
$html = '<html lang="es">
      <head>
          <meta >
          <title>Listado de radicacion</title>
          <body>
          ';
 
        
//Contenido
$html .=  ' <div  >
        <div class="panel panel-success">
            <div class="panel-heading">
                <h3 class="panel-title"></h3>
            </div>
            <div class="panel-body">
                <table class="table table-striped table-hover" border="1" style="margin-left: auto; margin-right: auto">
                    <thead>
                        <tr>
                            <th  colspan="5" rowspan="4">
                               <img class="imagen-avatar"  src="http://10.10.30.102/sigas/sigasReportinpen/img/confamiliar2.png" style="margin-left: auto; height: 60px; Width: 110px;"> 
                            </th> 
                            <th  colspan="7" rowspan="4">
                                <h1>Informe de Independientes</h1>
                            </th>
                        </tr>
                        
                        <tr>
                        
                            
                        </tr>
                        <tr>
                            
                        </tr>
                        <tr>
                            
                        </tr>
                        
                        <tr>
                            <th>No. </th>
                            <th>Rad. </th>
                            <th>Fecha </th>
                            <th>Hora </th>
                            <th>Tiempo </th>
                            <th>'.utf8_decode("Número ").'</th>
                            <th>Nombres </th>
                            <th>'.utf8_decode("notas").'</th>
                            <th>Tipo Afiliado </th>
                            <th>Estado </th>
                            <th>Folios </th>
                            <th>Usuario </th>
                        </tr>
                        
                        
                    </thead>
                    <tbody>';


 ?>
 <?php
    /**************************************************************************************************************/
    /*************Obtenemos datos atrabes del metodo para recorrerlos en el arreglo con un while y crear tabla*****/
     $datos = $objClase->mostrar_datos_reporte00_01($fecha_inicio, $fecha_fin, $agenci, $users, $tipo_radicacion, $tipoAfiliadon);
     $contador = 1;
     
    /**************************************************************************************************************/
    /*********************Siclo que crea los campos a la tabla extraidos desde la base de datos.*******************/
     while ($row = mssql_fetch_array($datos)){
         
         if ($colorfila==0){ 
            $color= "#D4FFFB"; 
            $colorfila=1; 
         }else{ 
            $color="#F0F0F0"; 
            $colorfila=0; 
         }
          
        $html .= '<tr>
            <td bgcolor='."$color".'>'.$contador.'</td>
            <td bgcolor='."$color".'>'.$row['idradicacion'].'</td>
            <td bgcolor='."$color".'>'.$row['fecharadicacion'].'</td>
            <td bgcolor='."$color".'>'.$row['horainicio'].'</td>
            <td bgcolor='."$color".'>'.$row['tiempo'].'</td>
            <td bgcolor='."$color".'>'.$row['numero'].'</td>
            <td bgcolor='."$color".'>'.$row['papellido'].' '.$row['pnombre'].'</td>
            <td bgcolor='."$color".'>'.$row['notas'].'</td>
            <td bgcolor='."$color".'>'.$row['tipo_afiliado'].'</td>
            <td bgcolor='."$color".'>'.$row['estado'].'</td>
            <td bgcolor='."$color".'>'.$row['folios'].'</td>
            <td bgcolor='."$color".'>'.$row['usuario'].'</td>
        </tr>';
         //echo $row['edad'];           
         $contador = $contador + 1;           
     }
 ?>                   
  
 <?php 
        
 //Pie de Pagina
 $html .= '</tbody>
                 </table>
             </div>
         </div>
     </div> 
         
     </div>
     
     </body>
  </html>';                   
   echo $html;       
    
   
    ?>