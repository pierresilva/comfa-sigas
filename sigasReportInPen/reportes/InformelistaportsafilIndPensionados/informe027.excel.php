
<?php 
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
include_once $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'config.php';
@$fechaHoy=date("Y/m/d");
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
ini_set("memory_limit", "-1");
/**************************************************************************************************************/
/*************Incluimos el archibo reportInPen.class.php el cual contiene la clase*****************************/
include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'reportInPen.class.php';
$objClase=new reportInPen();//Instanciamos la clase y creamos el Objeto.
$dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
$mes = array("","Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
date_default_timezone_set("America/Bogota");
date_default_timezone_get();

/*******************************************/
/*****obtener parametros por la URL.********/
@$usuasesion=$_REQUEST['usuasesion'];
@$fecha_inicio=$_REQUEST['fechainicio'];
@$fecha_fin=$_REQUEST['fechafin'];
@$periodo=$_REQUEST['periodo'];
/*****obtener parametros por la URL.********/
/*******************************************/

/*******************************************/
/***variables para tipo de afiliacion*******/
$facultativo = 0;
$penvoluntario = 0;
$pensiscajas = 0;
$penreespecial = 0;
$agremiado = 0;
$extrangero = 0;
/*******************************************/



//La bariable $html almacena el codigo html para despues ser renderizado y exportado a EXCEL.
//Encabezado
$html = '<html lang="es">
      <head>
          <meta >
          <title>Informe listado aportes afiliaciones Independientes y Pensionados</title>
          <body>
          ';
 
        
//Contenido
$html .=  ' <div  >
        <div class="panel panel-success">
            <div class="panel-heading">
                <h3 class="panel-title"></h3>
            </div>
            <div class="panel-body">
                <table class="table table-striped table-hover" border="1" style="margin-left: auto; margin-right: auto">
                    <thead>
                        <tr border="0">
                            <th  colspan="1" rowspan="4">
                               <img class="imagen-avatar"  src="http://localhost/sigas/sigasReportinpen/img/confamiliar2.png" style="margin-left: auto; height: 60px; Width: 110px;"> 
                            </th>';
                            
                                if ($fecha_inicio != "" && $fecha_fin != ""){
                                      $html .= '<th  colspan="17" rowspan="4" border="0">'
                                              . '<h3 aling="center">Informe Listado Aportes de afiliaciones Independientes y Pensionados ACTIVOS<br>
                                                     '.$fecha_inicio.'  Hasta  '.$fecha_fin.'
                                                </h3><h5 aling="right">Usuario: '.$usuasesion.'</h5>
                                            </th>';
                                }
                                if($periodo != ""){
                                     $html .= '<th  colspan="17" rowspan="4" border="0">'
                                               . '<h3 aling="center">Informe Listado Aportes de afiliaciones Independientes y Pensionados ACTIVOS<br>
                                                     Del Periodo de pago '.$periodo.'
                                                 </h3><h5 aling="right">Usuario: '.$usuasesion.'</h5>
                                              </th>';
                                }
                           
                                
            $html .= '</tr>
                        
                        <tr>
                        
                            
                        </tr>
                        <tr>
                            
                        </tr>
                        <tr>
                            
                        </tr>
                        
                        <tr>
                            <th>No. </th>
                            <th>TIPO DOCUMENTO. </th>
                            <th>NUMERO DOCUMENTO </th>
                            <th>NOMBRE AFILIADO </th>
                            <th>'.utf8_decode('TIPO RADICACIÓN').'</th>
                            <th>'.utf8_decode('TIPO AFILIACIÓN').'</th>
                            <th>'.utf8_decode("FECHA AFILIACIÓN").'</th>    
                            <th>AGENCIA </th>
                            <th>'.utf8_decode("DIRECCIÓN ").'</th>
                            <th>TELEFONO </th>
                            <th>'.utf8_decode("CIUDAD").'</th>
                            <th>'.utf8_decode("DEPARTAMENTO").'</th>
                            <th>CORREO </th>
                            <th>COD.CIIU </th>
                            <th>ACTIVIDAD </th>
                            <th>VALOR APORTE </th>
                            <th>'.utf8_decode("FECHA PAGO").'</th>
                            <th>PERIODO PAGADO </th>
                        </tr>
                        
                        
                    </thead>
                    <tbody>';


 ?>
 <?php
    /**************************************************************************************************************/
    /*************Obtenemos datos atrabes del metodo para recorrerlos en el arreglo con un while y crear tabla*****/
        $datos = $objClase->mostrar_datos_reporte027(
            $fecha_inicio, 
            $fecha_fin, 
            $periodo
        );
        $rowss = mssql_fetch_array(
            $objClase->mostrar_datos_reporte027(
                $fecha_inicio, 
                $fecha_fin, 
                $periodo
            )
        );
        $contador = 1;
     
    /**************************************************************************************************************/
    /*********************Siclo que crea los campos a la tabla extraidos desde la base de datos.*******************/
     while ($row = mssql_fetch_array($datos)){
         if ($colorfila==0){ 
            $color= "#D4FFFB"; 
            $colorfila=1; 
         }else{ 
            $color="#F0F0F0"; 
            $colorfila=0; 
         }
          
        $html .= 
        '<tr>
            <td bgcolor='."$color".'>'.$contador.'</td>
            <td bgcolor='."$color".'>'.$row['Tipo_Documento'].'</td>
            <td bgcolor='."$color".'>'.$row['identificacion'].'</td>
            <td bgcolor='."$color".'>'.$row['pnombre'].' '.$row['snombre'].' '.$row['papellido'].' '.$row['sapellido'].'</td>
            <td bgcolor='."$color".'>'.$row['Tipo_Radicacion'].'</td>
            <td bgcolor='."$color".'>'.$row['tipo_afiliado'].'</td>
            <td bgcolor='."$color".'>'.$row['fechaingreso'].'</td>
            <td bgcolor='."$color".'>'.$row['agencia'].'</td>
            <td bgcolor='."$color".'>'.$row['direccion'].'</td>
            <td bgcolor='."$color".'>'.$row['telefono'].'</td>
            <td bgcolor='."$color".'>'.$row['municipio'].'</td>
            <td bgcolor='."$color".'>'.$row['departmento'].'</td>
            <td bgcolor='."$color".'>'.$row['email'].'</td>
            <td bgcolor='."$color".'>'.$row['ciiu'].'</td>
            <td bgcolor='."$color".'>'.$row['actividad'].'</td>
            <td bgcolor='."$color".'>'.intval($row['valoraporte']).'</td>
            <td bgcolor='."$color".'>'.$row['fechapago'].'</td>
            <td bgcolor='."$color".'>'.$row['periodo'].'</td>
        </tr>';
         //echo $row['edad'];           
         $contador = $contador + 1; 
         
         $row['tipoafiliacion'];
         switch ($row['tipoafiliacion']) {
            case 19:
                $facultativo++;
                break;
            case 20:
                $penvoluntario++;
                break;
            case 21:
                $pensiscajas++;
                break;
            case 4209:
                $penreespecial++;
                break;
            case 4520:
                $agremiado++;
                break;
            case 4521:
                $extrangero++;
                break;
        }
        $total = $penreespecial + $facultativo + $penvoluntario + $pensiscajas + $agremiado + $extrangero;
     }
 ?>                   
  
 <?php 

 $html .= '</tbody>
                 </table>
             </div>
         </div>
     </div> 
         
     </div> <br/><br/><br/>
     <table border="1px">
        <tr>
            <th>'.utf8_decode('Tipo Afiliación').'</th>
            <th>Cantidad</th>
        </tr>
        <tr>
            <td>INDEPENDIENTES FACULTATIVOS</td>
            <td>'.$facultativo.'</td>
        </tr>
        <tr>
            <td>PENSIONADOS VOLUNTARIOS</td>
            <td>'.$penvoluntario.'</td>
        </tr>
        <tr>
            <td>PENSIONADOS EN EL SISTEMA DE CAJAS</td>
            <td>'.$pensiscajas.'</td>
        </tr>
        <tr>
            <td>PENSIONADOS REGIMEN ESPECIAL</td>
            <td>'.$penreespecial.'</td>
        </tr>
        <tr>
            <td>INDEPENDIENTE AGREMIADO</td>
            <td>'.$agremiado.'</td>
        </tr>
        <tr>
            <td>INDEPENDIENTE EXTRANJERO</td>
            <td>'.$extrangero.'</td>
        </tr>
        <tr>
            <td>TOTAL</td>
            <td>'.@$total.'</td>
        </tr>
     </table> 
     </body>
  </html>';  
 
   if ($rowss != ""){
       //Crear excel.
            header("Content-Type: application/vnd.ms-excel");

            header("Expires: 0");

            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            //Nombre del archivo con extencion xls.
            header("content-disposition: attachment;filename=informe027.xls");
            echo $html;
   }else {
       echo "<html lang='es'>
      <head>
          <meta >
          <link rel='stylesheet' type='text/css' href='../../css/wow-alert.css'>
          <link rel='stylesheet' type='text/css' href='../../css/example.css'>
          
            
    <title>Informe listado aportes afiliaciones Independientes y Pensionados</title>
          <body>
          </body> 
          <script type='text/javascript' src='../../js/jquery-2.1.3.js'></script>
          <script type='text/javascript' src='../../js/wow-alert.js'></script>"
       . "<script>"
       . "      "
               . "alert('No hay registro de pagos en este periodo, oprima aceptar para retornar al menu anterior',"
               . "{
                    label: 'ACEPTAR',
                    success: function () {
                            console.log('User clicked YES');
                    }
                }"
               . ");"
               . "$('#mensage').unbind('click');
                $('#mensage').click(function(){
                    window.close();
                });"
        . "</script>
          </html>";
   }
           
    
   
    ?>

