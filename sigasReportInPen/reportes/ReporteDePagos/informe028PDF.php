
<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
include_once $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'config.php';
@$fechaHoy=date("Y/m/d");
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';

/**************************************************************************************************************/
/*************Incluimos el archibo reportInPen.class.php el cual contiene la clase*****************************/
include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'reportInPen.class.php';
ini_set("memory_limit", "-1");
set_time_limit(130);
require_once '../../lib/dompdf/dompdf_config.inc.php';
$objClase=new reportInPen();//Instanciamos la clase y creamos el Objeto.
$dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
$mes = array("","Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
date_default_timezone_set("America/Bogota");
date_default_timezone_get();

/************************************************************************************/
/*******************obtener parametros por la URL.***********************************/
@$usuasesion=$_REQUEST['usuasesion'];
@$agenci=$_REQUEST['agencia'];
@$annos=$_REQUEST['annos'];
@$annoIn=$_REQUEST['annoIn'];
@$annoFin=$_REQUEST['annoFin'];
@$periodoIn=$_REQUEST['periodoIn'];
@$periodoFin=$_REQUEST['periodoFin'];
/*******************obtener parametros por la URL.***********************************/
/************************************************************************************/

/**************************************************************************************************************/
/*************Obtenemos datos atrabes del metodo para recorrerlos en el arreglo con un while y crear tabla*****/
$datoss = $objClase->mostrar_datos_reporte028(
        $annos, 
        $annoIn, 
        $annoFin, 
        $periodoIn, 
        $periodoFin, 
        $agenci
);
$rowss = mssql_fetch_array(
    $objClase->mostrar_datos_reporte028(
        $annos, 
        $annoIn, 
        $annoFin, 
        $periodoIn, 
        $periodoFin, 
        $agenci
    )
);
/**************************************************************************************************************/
/**************************************************************************************************************/



/**************************************************************************************************************/
/****************************Script para el encabezado y pie de pagina.****************************************/
$script= '<script type="text/php">
            if( isset($pdf) ) {
            
                    //header y footer
                    $obj = $pdf->open_object();

                    //Agregamos texto Al Footer
                    $font = Font_Metrics::get_font("Helvetica");
                    $fontsize = 10;
                    $Tamañoletra = 20;
                    $fontcolor = array(0.4,0.4,0.4);
                    
                    //Parametros Text: mover horizontal, mover vertical, texto, fuente letra, tamaño letra, color letra.
                    $pdf->page_text(60, $pdf->get_height()-25, "Propiedad de Comfamiliar Huila", $font, $fontsize, $fontcolor);
                    $pdf->page_text($pdf->get_width()-100, $pdf->get_height()-25, "Pagina {PAGE_NUM} de {PAGE_COUNT}", $font, $fontsize, $fontcolor);

                    //Agregamos imagen, texto al Header.
                    $image = "http://localhost/sigas/sigasReportinpen/img/confamiliar.png";
                    //Parametros Imagen: URL img,extencion(jpg o png), posicion x, posicion y, width, height.
                    $pdf->image($image, "png", 25, 10, 255, 77);
                    
                    //Parametros Text: mover horizontal, mover vertical, texto, tipo letra, tamaño letra, color letra.
                    $pdf->page_text(630, $pdf->get_height()-715, "REPORTE DE PAGOS INDEPENDIENTES Y PENSIONADOS", $font, 13, $fontcolor);
                    $pdf->page_text(800, $pdf->get_height()-690, "Usuario: '.$usuasesion.'", $font, 13, $fontcolor);
                    $pdf->page_text(30, $pdf->get_height()-655, "Fecha del Reporte: '.$dias[date('w')].'  '.date("d").' de '.$mes[date("n")].' de '.date("Y").'   '.date("g:i a",  time()).'", $font, $fontsize, $fontcolor);

                    $pdf->close_object();
                    $pdf->add_object($obj, "all");
            }
        </script>';

/**************************************************************************************************************/
/****************************Estilos para configurar las margenes del pdf.*************************************/
$styles='   <style>
                @page { margin: 150px auto 19px ;
                margin-bottom: 20px; 
                }    

                body{
                    text-align: justify;
                    font: 10px arial;
                    font-family: 10px arial;
                    font-size:10px;
                    align:center;
                }
                table{
                    margin: 6px;
                    width: 100%;
                    font: 10px arial;
                    font-family: 10px arial;
                    font-size:10px;
                    align:center;
                }
            </style>';


/**************************************************************************************************************/
/************Variable la cual contiene HTML para que la libreria la renderize y cree el pdf.*******************/


if($rowss['identificacion'] != ""){

         $html = "<html>";
         $html .= $styles;
         $html .= "<body>";
         $html .= $script;
         $html .= '<table>
                        <tr>
                            <th>ORDEN No.</th>
                            <th>'.utf8_decode('NÚMERO IDENTIFICACIÓN').'</th>
                            <th>NOMBRE AFILIADO</th>
                            <th>CIUDAD</th>
                            <th>'.utf8_decode("DIRECIÓN").'</th>
                            <th>TELEFONO</th>
                            <th>CORREO ELECTRONICO</th>
                            <th>'.utf8_decode("TIPO AFILIACIÓN").'</th>
                            <th>'.utf8_decode("ESTADO AFILIACIÓN").'</th>
                            <th>'.utf8_decode("N° PLANILLA").'</th>
                            <th>'.utf8_decode("TIPO PLANILLA").'</th>
                            <th>PERIODO PAGADO</th>
                            <th>VALOR PAGADO</th>
                            <th>FECHA PAGO</th>
                            <th>APORTE</th>
                            <th>INTERES MORA</th>
                            <th>INDICE DE APORTE</th>
                        </tr>';
         
        /**************************************************************************************************************/
        /*********************Siclo que crea los campos a la tabla extraidos desde la base de datos.*******************/ 
         $contador = 1;
         $totalAportes = 0;
         while ($row = mssql_fetch_array($datoss)){

            $html .= 
                    '<tr>
                        <td >'.$contador.'</td>
                        <td >'.$row['identificacion'].'</td>
                        <td >'.$row['papellido'].' '.$row['pnombre'].'</td>
                        <td >'.$row['municipio'].'</td>
                        <td >'.$row['direccion'].'</td>
                        <td >'.$row['telefono'].'</td>
                        <td >'.$row['email'].'</td>
                        <td >'.$row['tipo_afiliado'].'</td>
                        <td >'.$row['estado'].'</td>
                        <td >'.$row['planilla'].'</td>
                        <td >'.$row['tipo_planilla'].'</td>
                        <td >'.$row['periodo'].'</td>
                        <td >$'.intval($row['valorpagado']).'</td>
                        <td >'.$row['fechapago'].'</td>
                        <td >$'.intval($row['valoraporte']).'</td>
                        <td >$'.intval($row['mora']).'</td>
                        <td >'.intval($row['tarifaaporte']).'</td>
                    </tr>';
                    $totalAportes = $totalAportes + intval($row['valorpagado']);
                    $contador = $contador + 1;            
         }
         $html .="<tr>
                        <th colspan='13'></th>
                        <td><h3><p><b>TOTAL APORTES: </b></p></h3></td>
                        <td><h3><p><b>$$totalAportes</b></p></h3></td>
                   </tr>
                </table>
            </body>
          </html>";  
        //echo $row['edad'];  
        $totalAportes = $totalAportes + intval($row['valorpagado']);
        $contador = $contador + 1; 
        //echo $html;
        //Intanciar la Clase DOMPDF
        $mipdf = new DOMPDF();

        /**********************************************************************/
        /****Definimos el tamaño y orientación del papel que queremos.*********/
        /****O por defecto cogerá el que está en el fichero de configuración.**/ 
        /****@params("tipo_papel,"orientacion") *******************************/ 
        $paper_size = array(0,0,760,1100);
        $mipdf->set_paper($paper_size, "landscape");

        //Cargar Contenido HTML
        $mipdf->load_html($html);

        ini_set("memory_limit", "-1");
        /*Posteriormente con las siguientes líneas renderizamos o 
         * convertimos el documento a pdf y luego enviamos el fichero 
         * al navegador para que sea descargado por el cliente.*/
        
        
        //Renderizamos el documento PDF.
        
        $mipdf->render();
        header('Content-type: application/pdf'); //Ésta es simplemente la cabecera para que el navegador interprete todo como un PDF
        header("Expires: 0");

        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        //echo $mipdf->output();
        header("content-disposition: attachment;filename=informe028.pdf");
        readfile('informe028.pdf');
        //echo $mipdf->output(); //Y con ésto se manda a imprimir el contenido del pdf
        echo $html;
        //Enviamos el fichero PDF al navegador.
        $mipdf->stream("informe028.pdf");
}else{
    $html .="<html>
          <link rel='stylesheet' type='text/css' href='../../css/wow-alert.css'>
          <script type='text/javascript' src='../../js/jquery-2.1.3.js'></script>
          <script type='text/javascript' src='../../js/wow-alert.js'></script>"
       . "<script>"
       . "      "
               . "alert('No hay registro de pagos en este periodo, oprima aceptar para retornar al menu anterior',"
               . "{
                    label: 'ACEPTAR',
                    success: function () {
                            console.log('User clicked YES');
                    }
                }"
               . ");"
               . "$('#mensage').unbind('click');
                $('#mensage').click(function(){
                    window.close();
                });"
               
        . "</script>
          </html>";
    echo $html;
}

?>

