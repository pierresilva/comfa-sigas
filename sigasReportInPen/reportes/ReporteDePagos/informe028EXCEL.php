<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
include_once $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'config.php';
@$fechaHoy=date("Y/m/d");
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';

/**************************************************************************************************************/
/*************Incluimos el archibo reportInPen.class.php el cual contiene la clase*****************************/
include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'reportInPen.class.php';
ini_set("memory_limit", "-1");
$objClase=new reportInPen();//Instanciamos la clase y creamos el Objeto.
$dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
$mes = array("","Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
date_default_timezone_set("America/Bogota");
date_default_timezone_get();

/************************************************************************************/
/*******************obtener parametros por la URL.***********************************/
@$usuasesion=$_REQUEST['usuasesion'];
@$agenci=$_REQUEST['agencia'];
@$annos=$_REQUEST['annos'];
@$annoIn=$_REQUEST['annoIn'];
@$annoFin=$_REQUEST['annoFin'];
@$periodoIn=$_REQUEST['periodoIn'];
@$periodoFin=$_REQUEST['periodoFin'];
/*******************obtener parametros por la URL.***********************************/
/************************************************************************************/

/**************************************************************************************************************/
/*************Obtenemos datos atrabes del metodo para recorrerlos en el arreglo con un while y crear tabla*****/
$datoss = $objClase->mostrar_datos_reporte028(
        @$annos, 
        @$annoIn, 
        @$annoFin, 
        @$periodoIn, 
        @$periodoFin, 
        @$agenci
);
$rowss = mssql_fetch_array(
    $objClase->mostrar_datos_reporte028(
        @$annos, 
        @$annoIn, 
        @$annoFin, 
        @$periodoIn, 
        @$periodoFin, 
        @$agenci
    )
);
/**************************************************************************************************************/
/**************************************************************************************************************/






/**************************************************************************************************************/
/****************************Estilos para configurar las margenes del EXCEL.*************************************/
$styles='   <style>
                @page { margin: 150px auto 19px ;
                margin-bottom: 20px; 
                }    
                    
                body{
                    text-align: justify;
                    font: 10px arial;
                    font-family: 10px arial;
                    font-size:10px;
                    align:center;
                }
                table{
                    font: 10px arial;
                    font-family: 10px arial;
                    font-size:10px;
                    align:center;
                    border: black 2px solid;
                }
                td{
                    align:center;
                    border: gray 1px groove;
                }
                tbody{
                    align:center;
                    border: red 1px solid;
                }
                .totalAportes{
                    mso-style-parent:style0;
                    font-size:8.0pt;
                    font-family:"10px arial";
                    mso-generic-font-family:auto;
                    mso-font-charset:0;
                }
                .aportes{
                    mso-style-parent:style0;
                    font-size:8.0pt;
                    font-weight:700;
                    font-family:"10px arial";
                    mso-generic-font-family:auto;
                    mso-font-charset:0;
                    text-align:center;
                    vertical-align:middle;
                    white-space:normal;
                }

            </style>';


/**************************************************************************************************************/
/************Variable la cual contiene HTML para que la libreria la renderize y cree el EXCEL.*******************/
$html = '<html lang="es">
      <head>
          <meta >
          <title>Reporte de pagos.</title>
          <body>
          ';
if($rowss['identificacion'] != ""){
    
    //Agregamos los estilos y los scrip al html.
    $html .=$styles;
    //Contenido
    $html .=
    '<div>

    <div class="panel-body">
        <table  class="table table-striped table-hover">
            <thead>
            <tr>
                <th  colspan="2" rowspan="4">
                   <img class="imagen-avatar"  src="http://localhost/sigas/sigasReportinpen/img/confamiliar2.png" style="margin-left: auto; height: 60px; Width: 110px;"> 
                </th> 
                <th  colspan="15" rowspan="4">
                    <h1>Reporte de Pagos Independientes y Pensionados.</h1>
                </th>
            </tr>

            <tr>


            </tr>
            <tr>

            </tr>
            <tr>

            </tr>
            <tr>
                <th>ORDEN No.</th>
                <th>'.utf8_decode('NÚMERO IDENTIFICACIÓN').'</th>
                <th>NOMBRE AFILIADO</th>
                <th>CIUDAD</th>
                <th>'.utf8_decode("DIRECIÓN").'</th>
                <th>TELEFONO</th>
                <th>CORREO ELECTRONICO</th>
                <th>'.utf8_decode("TIPO AFILIACIÓN").'</th>
                <th>'.utf8_decode("ESTADO AFILIACIÓN").'</th>
                <th>'.utf8_decode("N° PLANILLA").'</th>
                <th>'.utf8_decode("TIPO PLANILLA").'</th>
                <th>PERIODO PAGADO</th>
                <th>VALOR PAGADO</th>
                <th>FECHA PAGO</th>
                <th>APORTE</th>
                <th>INTERES MORA</th>
                <th>INDICE DE APORTE</th>
            </tr>
        </thead>
        <tbody>';


     ?>
     <?php 

        /**************************************************************************************************************/
        /*********************Siclo que crea los campos a la tabla extraidos desde la base de datos.*******************/ 
         $contador = 1;
         $totalAportes = 0;
         while ($row = mssql_fetch_array($datoss)){

            $html .= 
            '<tr>
                <td >'.$contador.'</td>
                <td >'.$row['identificacion'].'</td>
                <td >'.$row['papellido'].' '.$row['pnombre'].'</td>
                <td >'.$row['municipio'].'</td>
                <td >'.$row['direccion'].'</td>
                <td >'.$row['telefono'].'</td>
                <td >'.$row['email'].'</td>
                <td >'.$row['tipo_afiliado'].'</td>
                <td >'.$row['estado'].'</td>
                <td >'.$row['planilla'].'</td>
                <td >'.$row['tipo_planilla'].'</td>
                <td >'.$row['periodo'].'</td>
                <td >$'.intval($row['valorpagado']).'</td>
                <td >'.$row['fechapago'].'</td>
                <td >$'.intval($row['valoraporte']).'</td>
                <td >$'.$row['mora'].'</td>
                <td >'.intval($row['tarifaaporte']).'</td>
            </tr>';
             //echo $row['edad'];  
            $totalAportes = $totalAportes + intval($row['valorpagado']);
             $contador = $contador + 1;           
         }
         $html .= 
            '<tr>
            </tr>
            <tr> 
            </tr>
            <tbody>
                <tr class="totalAportes">
                    <th  class="aportes" colspan="9" rowspan="5"></th>
                    <th  class="aportes" rowspan="5">TOTAL VALOR APORTES:</th>
                    <th  class="aportes" rowspan="5">$'.$totalAportes.'</th>
                    <th  class="aportes" colspan="6" rowspan="5"></th>
                </tr>
            </tbody>';
     ?>                   

     <?php 

     //Pie de Pagina
     $html .= '</tbody>
                     </table>
                 </div>
             </div>
         </div> 

         </div>

         </body>
      </html>';  

        ini_set("memory_limit", "-1");
        //Crear excel.
            header("Content-Type: application/vnd.ms-excel");

            header("Expires: 0");

            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            //Nombre del archivo con extencion xls.
            header("content-disposition: attachment;filename=informe028.xls");
            echo $html;

}else{
    $html .="
          <link rel='stylesheet' type='text/css' href='../../css/wow-alert.css'>
          <script type='text/javascript' src='../../js/jquery-2.1.3.js'></script>
          <script type='text/javascript' src='../../js/wow-alert.js'></script>"
       . "<script>"
       . "      "
               . "alert('No hay registro de pagos en este periodo, oprima aceptar para retornar al menu anterior',"
               . "{
                    label: 'ACEPTAR',
                    success: function () {
                            console.log('User clicked YES');
                    }
                }"
               . ");"
               . "$('#mensage').unbind('click');
                $('#mensage').click(function(){
                    window.close();
                });"
               
        . "</script>
          </html>";
    echo $html;
}


?>
