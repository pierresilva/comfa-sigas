<?php
/* autor:       orlando puentes
 * fecha:       30/08/2010
 * objetivo:    Procesar los movimientos de los Bonos almacenados en la base de datos. 
 */
 
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;



include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'conexion.class.php';

include_once $raiz.DIRECTORY_SEPARATOR.'phpComunes'.DIRECTORY_SEPARATOR.'wssdk'.DIRECTORY_SEPARATOR.'ClientWSInfWeb.php';

$cliente = new ClientWSInfWeb('c523d9f153587fbe3c96fee41c26b471', '711b60b69a49aa33bfa888930b2ecf10');

class Empresa{
var $con;
var $con2;
var $fechaSistema;

function Empresa(){
 		$this->con=new DBManager;
		$this->con2=new DBManager;
 	}

function buscar_nit_informa($nit){
		if($this->con2->conectarInforma()==true){
			$sql="Select count(*) as cuenta from nits where nit='$nit'";
			//echo $sql;
			return mssql_query($sql,$this->con2->conect);
		}
	}
		
/*function insertar_informa($campos){
		if($this->con2->conectarInforma()==true){
			//$fechaSistema=date("m/d/Y");
			$sql="INSERT INTO nits (nit, clase, nombre, direccion, telefono, ciudad, autoret, contacto, tipoclie, aaereo, fax, depto, pais, estado, resp_iva, t_contrib, prefijo) VALUES ('".$campos[0]."','N','".$campos[1]."','".$campos[2]."','".$campos[3]."','".$campos[4]."','N','".$campos[5]."','N','".$campos[6]."','".$campos[7]."','".$campos[8]."','169','A','S','N','N')";
			//echo $sql;
			return mssql_query($sql,$this->con2->conect);
		}
	}
*/
	
function insertar_nueva($campos){
	if($this->con->conectar()==true){
		$a=substr($campos[24],6,4);
		$m=substr($campos[24],0,2);
		$d=substr($campos[24],3,2);	
		$ruta="digitalizacion\\empresa\\$a\\$m\\$d\\$campos[4]\\";	
		$sql="INSERT INTO aportes048 (idtipodocumento, nit, digito, nitrsn, codigosucursal, principal, razonsocial, sigla, direccion, iddepartamento, idciudad, idzona, telefono, fax, url, email, apartado, idrepresentante, idjefepersonal, contratista, colegio, exento, idcodigoactividad, actieconomicadane, indicador, idasesor, fechamatricula, idtipoentidad, idsector, seccional, tipopersona, claseaportante, tipoaportante, estado, fechaaportes, fechaafiliacion, usuario, fechasistema, idclasesociedad, rutadocumentos, legalizada) VALUES('".$campos[6]."','".$campos[4]."','".$campos[5]."','".$campos[4]."','000','S','".$campos[7]."','".$campos[8]."','".$campos[11]."','".$campos[9]."','".$campos[10]."',null,'".$campos[12]."','".$campos[13]."','".$campos[14]."','".$campos[15]."',null,'".$campos[16]."','".$campos[17]."','".$campos[18]."','N','N','".$campos[19]."',".$campos[27].",'".$campos[20]."','".$campos[21]."',null,null,'".$campos[2]."','".$campos[22]."','".$campos[3]."',null,null,'A','".$campos[25]."','".$campos[24]."','".$campos[28]."',cast(getdate() as date),'".$campos[3]."','$ruta','S')";
			//echo $sql;
			return mssql_query($sql,$this->con->conect);
		}
	}
	
/*function insertar_sucursal($campos){
		if($this->con->conectar()==true){
			$sql="INSERT INTO aportes048 (nit, digito, nitrsn, codigosucursal, principal, razonsocial, sigla, direccion, iddepartamento, idciudad, idzona, telefono, fax, url, email, apartado, idrepresentante, idjefepersonal, contratista, colegio, exento, idcodigoactividad, indicador, idasesor, idsector, seccional, estado, fechaaportes, fechaafiliacion, trabajadores, aportantes, conyuges, hijos, hermanos, padres, usuario, fechasistema, idtipodocumento, idclasesociedad) VALUES ('".$campos[4]."','".$campos[5]."','".$campos[4]."','".$campos[29]."','N','".$campos[7]."','".$campos[8]."','".$campos[11]."','".$campos[9]."','".$campos[10]."','".$campos[10]."','".$campos[12]."','".$campos[13]."','".$campos[14]."','".$campos[15]."','','". $campos[16] ."','". $campos[17] ."','". $campos[18] ."','". $campos[19] ."','". $campos[20] ."','". $campos[21] ."','". $campos[22] ."','". $campos[23] ."','". $campos[02] ."','". $campos[24] ."','". $campos[25] ."','". $campos[27] ."','". $campos[26] ."','0','0','0','0','0','0','". $campos[28]."',cast(getdate() as date),'". $campos[6] ."','". $campos[3] ."')";
			return mssql_query($sql,$this->con->conect);
		}
	}*/
		
function buscar_principal($nit){
    if($this->con->conectar()==true){
        $sql="select * from aportes048 where nit='$nit' and principal='S'";
        //echo $sql;
	return mssql_query($sql,$this->con->conect);
	}
 }
 
function contar_sucursales($nit){
	if($this->con->conectar()==true){
			$sql="select count(*) as cuenta from aportes048 where nit='$nit'";
			return mssql_query($sql,$this->con->conect);
		}
	} 
 
function buscar_grupo_empresa($nit){
	if($this->con->conectar()==true){
			$sql="select idempresa,nit,digito,principal,razonsocial,codigosucursal from aportes048 where nit='$nit'";
			return mssql_query($sql,$this->con->conect);
		}
	} 
	 
function actualizar_nit($nit,$nuevo,$digito){
	if($this->con->conectar()==true){
			$sql="update aportes048 set nit='$nuevo',digito='$digito' where nit='$nit'";
			return mssql_query($sql,$this->con->conect);
		}
	} 
		 
function buscar_empresa($ide){
	if($this->con->conectar()==true){
			$sql="SELECT aportes048.*, aportes015.papellido, aportes015.sapellido, aportes015.pnombre, aportes015.snombre,jp.papellido as pajp, jp.sapellido as sajp, jp.pnombre as pnjp, jp.snombre as snjp FROM aportes048 LEFT JOIN aportes015 ON aportes048.idrepresentante = aportes015.identificacion LEFT JOIN aportes015 jp ON aportes048.idjefepersonal = jp.identificacion WHERE idempresa=$ide";
			return mssql_query($sql,$this->con->conect);
		}
 }

function buscar_empresa1($ide){
	if($this->con->conectar()==true){
			$sql="SELECT aportes048.*, aportes015.identificacion as identificacionrep, aportes015.papellido, aportes015.sapellido, aportes015.pnombre, aportes015.snombre,jp.identificacion as identificacionjp, jp.papellido as pajp, jp.sapellido as sajp, jp.pnombre as pnjp, jp.snombre as snjp FROM aportes048 LEFT JOIN aportes015 ON aportes048.idrepresentante = aportes015.idpersona LEFT JOIN aportes015 jp ON aportes048.idjefepersonal = jp.idpersona WHERE idempresa=$ide";
			return mssql_query($sql,$this->con->conect);
		}
 }
 /**
  * Obtiene la informaci�n de una empresa seg�n el id recibido
  *
  * @param int $idEmpresa
  * @return ifx result or false
  */
function buscar_empresa_por_idempresa($idEmpresa){
	if($this->con->conectar()==true){
		$query = "select * from aportes048 where idempresa = ".$idEmpresa;
		return mssql_query($query,$this->con->conect);
	}
	return false;
}

function buscar_empresa_por_NIT($nit){
	if($this->con->conectar()==true){
		$query = "select * from aportes048 where nit = '{$nit}'";		
		return mssql_query($query,$this->con->conect);
	}
	return false;
}

function inactivar_empresa_por_NIT($nit){
	if($this->con->conectar()==true){
		$query = "update aportes048 set estado='I',fechasistema=cast(getdate() as date) where nit=".$nit;
		mssql_query($query,$this->con->conect);
	}
	return false;
}

function buscar_todas($parametro, $tipo=1){
	if($this->con->conectar()==true){
		if($tipo==1)
			$sql="SELECT idempresa,nit,razonsocial from aportes048 WHERE nit like '$parametro'";
		if($tipo==2)
			$sql="SELECT idempresa,nit,razonsocial from aportes048 WHERE razonsocial like '%$parametro%'";	
		return mssql_query($sql,$this->con->conect);
	}
 }		 	

function actualizar_empresa($campo){
		if($this->con->conectar()==true){
			$sql="update aportes048 set idsector=".$campo[2].", idclasesociedad=".$campo[3].", razonsocial='".$campo[7]."', sigla='".$campo[8]."', direccion='".$campo[9]."', iddepartamento='".$campo[10]."', idciudad='".$campo[11]."', telefono='".$campo[12]."', fax='".$campo[13]."', url='".$campo[14]."', email='".$campo[15]."', idrepresentante='".$campo[16]."', idjefepersonal='".$campo[21]."', contratista='".$campo[26]."', colegio='".$campo[27]."', exento='".$campo[28]."', idcodigoactividad='".$campo[29]."', indicador='".$campo[30]."', idasesor='".$campo[31]."', seccional='".$campo[32]."', estado='".$campo[33]."', usuario='".$campo[36]."', fechasistema=cast(getdate() as date), idtipodocumento=".$campo[6].", idclasesociedad=".$campo[3]." where idempresa=".$campo[38];
			//echo $sql;
			return mssql_query($sql,$this->con->conect);
		}
	}
		 
function trayectoria($ide){
	if($this->con->conectar()==true){
		$sql="SELECT nit,razonsocial,fechaafiliacion,fechaestado from aportes048 WHERE idempresa=$ide";	
		return mssql_query($sql,$this->con->conect);
	}
 }		 	
function otras_afiliaciones($ide){
	if($this->con->conectar()==true){
		$sql="SELECT nit,razonsocial,fechaafiliacion,fechaestado from aportes049 WHERE idempresa=$ide";	
		return mssql_query($sql,$this->con->conect);
	}
 }		 	
 function buscar_una_sucursal($nit,$suc){
	if($this->con->conectar()==true){
	$sql="SELECT * FROM aportes048 WHERE nit='$nit' and codigosucursal='$suc'";
        //echo $sql;
	return mssql_query($sql,$this->con->conect);
	}
 }
 
	 /**
	  * Obtiene una lista de empresas seg�n la(s) actividad(es) econ�mica(s) y dem�s par�metros
	  * @param array $idsActividadEco arreglo sigle con los ids de los tipos de actividad econ�mica
	  * @param array $parametros opcional, otros par�metros de b�squeda
	  * @return ifx_result $resultEmpresas
	  */
	 function buscar_x_actividad_eco($idsActividadEco = array(), $parametros = array()){
	 	if($this->con->conectar()){
	 		$strCondiciones = "";
		 	$arrCondiciones = array();
		 	if(is_array($parametros)){
		 		if(isset($parametros["razonsocial"]))
		 			$arrCondiciones[] = " razonsocial like '%". strtoupper($parametros["razonsocial"]) ."%'";
		 		
		 		// @TODO Agregar otros par�metros de criterios de b�squeda
		 	}
		 	if(is_array($arrCondiciones) && count($arrCondiciones)>0)
		 		$strCondiciones = " AND ". implode(" AND ", $arrCondiciones);
		 	
		 	$sql = "SELECT * FROM aportes048 WHERE idcodigoactividad in (". implode(",",$idsActividadEco) .") $strCondiciones";
		 	$resultEmpresas = mssql_query($sql, $this->con->conect);
		 	return $resultEmpresas;
	 	}
	 }
	 
	 function cargarEmpresa($nit = '',$id = null, $activa = true){
	 	if($this->con->conectar()){
	 		$strEstado = ($activa == true)?" AND estado ='A' ":"";
	 		if($nit != '')
	 			$sql = "SELECT * FROM aportes048 WHERE nit='$nit' ". $strEstado;
	 		elseif(intval($id)>0)
	 			$sql = "SELECT * FROM aportes048 WHERE idempresa=$id ". $strEstado;
	 		$resultado = mssql_query($sql);
	 		$empresa = mssql_fetch_array($resultado);
	 		if(is_array($empresa) && count($empresa)>0)
	 			return $empresa;
	 		return null;
	 	}else{
	 		die("No se pudo conectar con la base de datos.");
	 	}
	 }
 }	
?>