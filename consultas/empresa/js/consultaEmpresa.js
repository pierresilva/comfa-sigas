/*
* @autor:      Ing. Orlando Puentes
* @fecha:      26/08/ 2010
* objetivo:
*/
$(function() {
		$("#imagen").dialog({
		 	autoOpen: false,
			modal: true,
		    hide: 'slide',
		    show:'fade',
		    width:900,
		    height:700
		});
});

$(function() {
		
	$("#dialog-form2").dialog("destroy");
	
	$("#dialog-form2").dialog({
		autoOpen: false,
		height: 400,
		width: 500,
		modal: true,
		buttons: {
			'Enviar': function() {
			var bValid = true;
			var campo=$('#notas').val();
			var campo0=$.trim(campo);
			if (campo0==""){
				$(this).dialog('close');
					return false;
				}
			var campo1=$('#usuario').val();
			var campo2="consultaEmpresa.php";
			$.post(url+"phpComunes/colaboracion.php",{v0:campo0,v1:campo1,v2:campo2}, 								
			function(datos){
				if(datos=='1'){
					alert("Su comentario fue enviado correctamente!. Gracias por participar en el mejoramiento de SIGAS");
				}else{
					alert(datos);
				}
			});
				$(this).dialog('close');
			},
			Cancelar: function() {
				$(this).dialog('close');
				$("#dialog-form2").dialog("destroy");
			}
		}		
	});
	
	$('#enviar-notas').button().click(function() {
		$('#dialog-form2').dialog('open');
	});
});

$(function() {
		$("#ayuda").dialog({
		 	autoOpen: false,
			height: 550, 
			width: 750, 
			draggable:true,
			open: function(evt, ui){
					$('#ayuda').html('');
					$.get('../../help/aportes/ayudaConsultaempresas.html',function(data){
							$('#ayuda').html(data);
					})
			 }
		});
});

/*...BUSCAR ...//
	1 nit
	2 razon social
	*/
$(document).ready(function(){
//Cargar ruta

	$("#buscarEmpresa").click(function(){
		$("#accordion h3").unbind('click');
		$("#load").show();
		
		//... LIMPIAR ACORDEON ...//
		$("#accordion").accordion("destroy");
		$("#accordion").children().remove();
		$("#empresas").find("p").remove();
		
		//VALIDAR QUE EL NIT SEA NUMEROS
		if( $("#buscarPor").val()=="1"&& isNaN($("#idEmpresa").val()) ){
			$(this).next("span.Rojo").html("El NIT debe ser num\u00E9rico.").hide().fadeIn("slow");
				return false;
		}
		
		if($("#buscarPor").val()=='Seleccione..'){
			$(this).next("span.Rojo").html("Seleccione el criterio de b\u00FAsqueda.").hide().fadeIn("slow");
			return false;
		}
		if($("#idEmpresa").val()==''){
			$(this).next("span.Rojo").html("Ingrese el NIT \u00F3 RAZON SOCIAL").hide().fadeIn("slow");
			$("#idEmpresa").focus();
			return false;
		}	
				
		var v0=$("#idEmpresa").val();
		var v1=$("#buscarPor").val();
		
		$.getJSON(
				'buscarTodas.php',
				{v0:v0.toUpperCase(),v1:v1},
				function(data){
					if(data!=''){
						enviarNit(data[0].nit);
					}else{
						$("#buscarEmpresa").next("span.Rojo").html("No se encontraron resultados.").hide().fadeIn("slow");
					}
				});//fin JSON
		});//fin click
});//Fin ready



//... FUNCION PARA ENVIAR NIT PARA ACORDEON ...//
function enviarNit(nit){

	var	indexTab;

	$("#empresas").find("p").remove();
	$("#accordion").accordion("destroy");
	$("#accordion").children().remove();
	var tabs = "<div>" + "<ul>"
			+ "<li><a href='#tabs-' alt='a'>Datos Empresa</a></li>"
			+ "<li><a href='#tabs-' alt='b'>Aportes</a></li>"
			+ "<li><a href='#tabs-' alt='c'>Trabajadores</a></li>"
			/*
			+ "<li><a href='#tabs-' alt='d'>Documentos</a></li>"
			+ "<li><a href='#tabs-' alt='e'>Trayectoria</a></li>"
			+ "<li><a href='#tabs-' alt='f'>Planilla \u00DAnica</a></li>"
			+ "<li><a href='#tabs-' alt='g'>Reclamos</a></li>"
			+ "<li><a href='#tabs-' alt='h'>Radicaciones Pendientes</a></li>"
			*/
			+ "</ul>" + "<div></div>" + "<div></div>" + "<div></div>"
			+ "<div></div>" + "<div></div>" + "<div></div>" + "<div></div>" + "<div></div>" + "</div>";	
	
	// CUENTA ES EL NUMERO DE EMPRESAS EN EL ACORDEON
	$.getJSON('buscarGrupoEmpresa.php',{v0:nit},function(data){
	$.each(data, function(i, fila){
			$("#accordion").append(
			"<h3 class='ui-accordion-header' id='"+fila.idempresa+"'>Empresa: "+fila.idempresa+"-"+fila.razonsocial+"<span/></h3>"+
			"<div class='ui-accordion-content'><input type=\"hidden\" id=\"hdnIdempresa"+i+"\" value=\""+fila.idempresa+"\" />"+tabs+"</div>");
	});
	//... ITERACION Y CARGA DINAMICA DE TABS ...//
	$("div.ui-accordion-content ul li a").each(function(i){
		$(this).attr("href",function() {
		return $(this).attr("href")+i;
	   	}).click(function(){
			
			 		//Aqui Capturo el valor del ALT para el switch que carque los formularios	 
					indexTab=$(this).attr("alt");
						
			
			});//end attr
	//... ITERACION Y  NOMBRAMIENTO DE LOS DIV CONTENEDORES DE LOS TABS ...//
		$("div.ui-accordion-content ul~div").each(function(index){
			$(this).attr("id","tabs-"+index);
	       	});//end each
		
		
	/*----*/$(function() {
			$("#accordion").accordion({
		                               autoHeight: false,
		                        	   navigation:true,
									   collapsible:true,
									   active:false,
			                           icons:{ 'header': 'plus', 'headerSelected': 'minus' }
			});	
	       
		   //COLECCION DE RUTAS PARA EL TAB
		    var tabsArray=new Array("tabs/tablaEmpresa.php","tabs/tablaAportes.php","tabs/tablaTrabajadores.php","tabs/tablaImagenesTab.php","tabs/tablaTrayec.php","tabs/tablaReclamosEmpTab.php");
			//Dialog periodo	
	$(function(){	
	$("#periodo").dialog({
		                  autoOpen:false,
						  modal:true,
						  width:350,
						  height:20,
						  resizable:false
					   });
	});
			
	$("#accordion h3").click(function(){
				$("#buscarPeriodo").unbind();
				    //var ide =$(this).attr('id');
				    $("div.ui-accordion-content div:has(ul)").each(function(index){
			 		$(this).attr("id","tabs"+index);
					
					//limpia el div de la tabla trabajadores cuando sale para evitar colision de estilos
				    $(this).children("div:eq(2)").html('');
					$(this).children("div:eq(1)").html('');
					
					$("#tabs"+index).tabs({
									spinner:'<em>Loading&#8230;</em>',selected:-1,
									select:function(){
											var ide = $(this).siblings('input:hidden').val();
										     switch(indexTab){
										     case "a":  $(this).children("div:eq(0)").load(tabsArray[0],{v0:ide});
											 break;
									         case "b": $(this).children("div:eq(1)").load(tabsArray[1],{v0:ide,v1:20});
											 break; 
									         case "c": $(this).children("div:eq(2)").load(tabsArray[2],{v0:ide});
									         break; 
								             case "d": $(this).children("div:eq(3)").load("tabs/tablaImagenesTab.php",{v0:ide});
									         break; 
									         case "e": $(this).children("div:eq(4)").load("tabs/tablaTrayec.php",{v0:ide});
									         break; 
											 case "f":   $("#periodo").dialog('open');
											                    $("#buscarPeriodo").click(function(){
																	var periodo=$("#txtPeriodo").val();
																    if(periodo.length==0){
															            $("#periodo").dialog('close');
																	}else{
															                     if(periodo.length!=6){
																	                              alert("El periodo es de 6 Digitos.");
																	                              return false;
																							    }else
															                             {
																							 if(isNaN(periodo)){
																								 alert("El periodo debe ser num\u00E9rico.");
																	                              return false;
																							 }else{
																                           $("#periodo").dialog('close');
																                           $("#txtPeriodo").val('')	;   
															                               $('#tabs'+index).children("div:eq(5)").load("tabs/tablaPlanillaU.php",{v0:ide,v1:periodo});
																	                  }//end else
																					}//end else
															           }//end else
																	  }); //end click
											 break; 
											 case "g": $(this).children("div:eq(6)").load("tabs/tablaReclamosEmpTab.php",{v0:ide});
									         break;
											 case "h": $(this).children("div:eq(7)").load("tabs/TablaRadicacionesPendientesTab.php",{v0:nit});
									         break;
											 }	//end switch				 				 
									}});//end tab		
				 });//END EACH
				  
		});//END CLICK
	    
	/*----*/ });//end function acordeon
		   
	      });//--------------------------------------------------------END JSON
	});//end json buscarempresa				

}//END FUNCTION BUSCAR NIT



//...ABRIR DOCUMENTOS...
function imagen(imgAlt){
	
   //Capturo el objeto como un ojeto jquery	
	var img=jQuery(imgAlt);
	//Borror en el div #imagen alguna imagen
	$("#imagen").find("img").remove();
	$("#imagen").dialog('open');
	
	var srcImage= img.attr("alt");
	$("#imagen").append("<img src='"+srcImage+"'");
}//end function imagen		

/*
Ã� 	&Aacute; 	\u00C1
á 	&aacute; 	\u00E1
Ã‰ 	&Eacute; 	\u00C9
Ã© 	&eacute; 	\u00E9
Ã� 	&Iacute; 	\u00CD
Ã­ 	&iacute; 	\u00ED
Ó 	&Oacute; 	\u00D3
Ã³ 	&oacute; 	\u00F3
Ãš 	&Uacute; 	\u00DA
Ãº 	&uacute; 	\u00FA
Ãœ 	&Uuml;      \u00DC
Ã¼ 	&uuml; 	    \u00FC
á¹„ 	&Ntilde; 	\u00D1
Ã± 	&ntilde; 	\u00F1
*/ 