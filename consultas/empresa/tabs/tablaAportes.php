<?php
/* autor:       orlando puentes
 * fecha:       01/07/2010
 * objetivo:    
 */
 
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;


$idemp= $_POST['v0'];
$limite=$_POST['v1'];
include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'p.aportes.class.php';
$objAportes = new Aportes;
$consulta=$objAportes->buscar_aportes($idemp,$limite);
 ?>
 <head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 <title>APORTES EMPRESA</title>
 <script type="text/javascript" src="../../js/script.js"></script>
<script type="text/javascript">
  var sorter = new TINY.table.sorter("sorter");
	sorter.head = "head";
	sorter.asc = "asc";
	sorter.desc = "desc";
	sorter.even = "evenrow";
	sorter.odd = "oddrow";
	sorter.evensel = "evenselected";
	sorter.oddsel = "oddselected";
	sorter.paginate = true;
	sorter.pagesize = (5);
	sorter.currentid = "currentpage";
	sorter.limitid = "pagelimit";
	sorter.init("table",1);
  </script>
 
 </head>
 <body>
<h4>Consulta Aportes</h4>
<table  cellpadding="0" cellspacing="0"  id="table" border="0"  class="sortable" width="90%">
  <thead>
  <tr>
    <th><h3>Comprob</h3></th>
    <th><h3>Documen</h3></th>
    <th><h3>Número&nbsp;</h3></th>
    <th><h3>Periodo&nbsp;</h3></th>
    <th><h3>Nomina&nbsp;</h3></th>
    <th><h3>Aporte&nbsp;</h3></th>
    <th><h3>Trabajs&nbsp;</h3></th>
    <th><h3>Ajuste&nbsp;</h3></th>
    <th><h3>Fecha Pago&nbsp;</h3></th>
    <th><h3>Fecha Sistema&nbsp;</h3></th>
    <th><h3>Usuario&nbsp;</h3></th>
   </tr>
 </thead> 
 <tbody>
<?php 
while($row=mssql_fetch_array($consulta)){
?>  

  <tr>
    <td style="text-align:right"><?php echo $row['comprobante']; ?>&nbsp;</td>
    <td style="text-align:right"><?php echo $row['documento']; ?>&nbsp;</td>
    <td style="text-align:right"><?php echo $row['numerorecibo']; ?>&nbsp;</td>
    <td style="text-align:right"><?php echo $row['periodo']; ?>&nbsp;</td>
    <td style="text-align:right" ><?php echo number_format($row['valornomina']); ?>&nbsp;</td>
    <td style="text-align:right"><?php echo number_format($row['valoraporte']); ?>&nbsp;</td>
    <td style="text-align:right"><?php echo $row['trabajadores']; ?>&nbsp;</td>
    <td style="text-align:center"><?php echo $row['ajuste']; ?>&nbsp;</td>
    <td><?php echo $row['fechapago']; ?>&nbsp;</td>
    <td><?php echo $row['fechasistema']; ?>&nbsp;</td>
    <td><?php echo $row['usuario']; ?>&nbsp;</td>
  </tr>
<?php }?>  
</tbody>
</table>
    	<div id="controls">
		<div id="perpage">
			<select onChange="sorter.size(this.value)">
			<option value="5" selected="selected">5</option>
				<option value="10" >10</option>
				<option value="20">20</option>
				<option value="50">50</option>
				<option value="100">100</option>
			</select>
			<label>Registros Por P&aacute;gina</label>
		</div>
		<div id="navigation">
			<img src="../../imagenes/imagesSorter/first.gif" width="16" height="16" alt="first Page" onClick="sorter.move(-1,true)" />
			<img src="../../imagenes/imagesSorter/previous.gif" width="16" height="16" alt="first Page" onClick="sorter.move(-1)" />
			<img src="../../imagenes/imagesSorter/next.gif" width="16" height="16" alt="first Page" onClick="sorter.move(1)" />
			<img src="../../imagenes/imagesSorter/last.gif" width="16" height="16" alt="Last Page" onClick="sorter.move(1,true)" />
		</div>
		<div id="text">P&aacute;gina <label id="currentpage"></label> de <label id="pagelimit"></label></div>
	</div>
	    	</body>