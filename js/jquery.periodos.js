/*
 *Autor:Andrès Felipe Lara
 *Fecha:10-Mar-2011
 *Definiciòn:Plugin para el control de periodos V 1.0	
 *Requiere las siguientes reglas CSS:
 *Observaciones:Para mejoras o consejos anotar los cambios entre 
     //n 
	 codigo 
	 //n
  o  escribeme anslara89@gmail.com
  
  ___HTML___
  
  <img>-->Icono del calendario
  <div>--> destino de las etiquetas a crear
  <input>--> valores reales que se guardan hidden-text-textarea
  
  ____JS____
  
  anioInicial:'1930',       //AÑO INICIAL
  anioFinal:anio(),         //AÑO FINAL, POR DEFECTO ES EL AÑO ACTUAL
  idDestino:'destino',      //# ELEMENTO PARA PONER LOS PERIODOS(ETIQUETAS)
  claseCabecera:'cabecera', //ESTILO PARA LA CABECERA
  claseMes:'mes',			//ESTILO PARA LOS MESES
  idResultado:'array', 		//# ELEMENTO Q GUARDA EL ARRAY GENERADO !DEBE SER TIPO HIDDEN Ó TEXT Ó TEXTAREA!
  anioCompleto:false 		//ACTIVAR BOTON DE AÑO COMPLETO
  
  ____CSS____
  
  #destino{border:1px solid #eaeaea;height:35px;width:400px;padding:12px 0 0px 8px;}
  #calendario{border:1px solid red;font:10px Verdana, Geneva, sans-serif;width:150px;padding:5px;position:absolute;}
  #calendario label{ margin:20px;}
  #calendario .meses{ border:1px solid blue; margin:8px; padding:5px;overflow:hidden;}
  #calendario .meses span{float:left;padding:4px;border:1px solid orange;display:block; width:20px;margin:5px;cursor:pointer;}
  .cerrar{ color:red; cursor:pointer; margin:5px}
  .etiqueta{ color:#666;padding:4px; background:#fafafa; border:1px solid #ccc; font:10px verdana; margin:5px}
  .x{ color:#cc0000; cursor:pointer; margin:5px;font:9px arial;}
  .x:hover{color:#f00;}
  .resaltado{border: 1px solid #fcefa1; background: #fbf9ee}
  #spnAnioCompl{ font-size:10px; margin:5px 5px 0 22px}
  
*/
(function($){
    
  $.fn.periodos=function(options){
    
	
	meses=['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'];
	arrPeriodos=[];
	
	acum=0;
	//Opciones por defecto del plugin
	var defaultOptions={
	                   anioInicial:'1930',
                       anioFinal:anio(),
                       idDestino:'destino',//ELEMENTO PARA PONER LOS PERIODOS
					   claseCabecera:'cabecera',
					   claseMes:'mes',
					   idResultado:'array', //ELEMENTO Q GUARDA EL ARRAY GENERADO !DEBE SER TIPO HIDDEN Ó TEXT Ó TEXTAREA!
					   anioCompleto:false
                       };
					   
	option = $.extend(defaultOptions,options);
	
	
	//Calcular el año actual
	function anio(){
	  
	  var date=new Date();
	  var anioActual=date.getFullYear();
	  return anioActual; 
	}
	
	this.each(function(){
	
	elemento=$(this);	
	activo=0;
	//Evento asociado para mostrar el calendario
	elemento.click(mostrarCalendario);
	
    //Cerrar calendario X
	$("#calendario .cerrar").live("click",ocultarCalendario);
	$("#cboAnios").live("blur",function(){
	activo=0;
	});
	//Crear periodo
	$("#calendario ."+option.claseMes).live("click",crearPeriodo);
	//Eliminar periodo
	$("label.etiqueta .x").live("click",borrarPeriodo);	
	//Año completo
	$("#btnAnioCompl").live("click",mostrarAnioCompleto);
	function mostrarCalendario(){
	
	if(activo==0){
	ocultarCalendario();
	}
	
	$("#calendario").remove();
	
	var posicion=elemento.offset();
	cssCalendario={"top":""+parseInt(posicion.top)+"px","left":""+parseInt(posicion.left)+"px","position":"absolute","z-index":"1"};
	
	//DIV que contendrá el calendario ----------------------------------------+
	var calendario='<div id="calendario">'+
	               '<div class="'+option.claseCabecera+'"><label>A&Ntilde;O</label>'+
	               '<select id="cboAnios">';
	
    //Cargo los años en el combo	
    for(var i=parseInt(option.anioInicial);i<=parseInt(option.anioFinal);i=i+1){
	
	   calendario+="<option value='"+i+"'>"+i+"</option>";
	 
	}				   
	calendario+='</select><span class="cerrar"> X </span></div>';
	calendario+='<div class="meses">';
	
	//Cargo los meses
	$.each(meses,function(i,n){
		  var noMes=i+1;
	      calendario+="<span id='"+noMes+"' class='"+option.claseMes+"' >"+n+"</span>";
	});
	
	//Si es seleccionado la opcion ANIOCOMPLETO
	if(option.anioCompleto==true){
	calendario+="<button id='btnAnioCompl'>Todo el a&ntilde;o</button>";
	}	               
	calendario+="</div></div>";		
	
	$(document.body).append(calendario);
	$("#calendario").css(cssCalendario).hide().slideDown();
	//Mantener eleccionado el ultimo option
	$("#cboAnios option:last").attr("selected","selected");
    $("#cboAnios").focus();    	
	activo=1;
	// Fin DIV calendario -----------------------------------------------------+	
	}
    //Funcion para ocultar el calendario	
	function ocultarCalendario(){
	$("#calendario").hide("slow",function(){ $(this).remove();});
	}	     
	
	//Contar elementos del array
	function contar(arr){
	 elms=arr.length; 
	 return elms;
	}
	
    
	//Funcion para crear el periodo	
	function crearPeriodo(){
	
	//Reiniciar el plugin si han limpiado datos en formulario donde se llama
	if($("#"+option.idResultado).val()==''){
		 arrPeriodos=[];
		 $("#"+option.idResultado).val('');
	}
    /* //Si ya existe el label de anio completo
	if($("#"+option.idDestino+" label").length>0){
	 	
		arrayHidden=$("#array").val().split(",");
	
	   	//Buscar en los valores del campo oculto si existen repetidos
	 	for(i=0;i<arrayHidden.length;i++){
		  if(arrayHidden[i].slice(-4)==anio){
		  	 alert("Ya existe a\u00F1o "+anio+" completo agregado.");
	 		 ocultarCalendario();
			 return false;
		   }//if
	 	}//for
	 }//if*/
	var anio=$("#cboAnios option:selected").val();	
	var mes=(this.id > 9 ? this.id  : "0"+this.id );
	var periodo=anio+mes;
	$(".resaltado").removeClass("resaltado"); //Quitar clase resaltado 
	//Label a mostrar
	etiqueta=$("<label class='etiqueta' id='lbl"+periodo+"'><span>"+anio+"-"+mes+"</span><span class='x'>X</span></label>");
	var total=contar(arrPeriodos);//Saber cuantos elementos tiene el array actualmente	 
	
	if (total==0){
	 
	 arrPeriodos.push(periodo);
	 etiqueta.appendTo("#"+option.idDestino).hide().fadeIn(700);
	 ocultarCalendario();
	 
	 }else{
	    //Si es menor al primero  
		if(periodo<arrPeriodos[0]){
		     arrPeriodos.unshift(periodo);
			 antes(etiqueta);
		}else{
		       //Si es mayor al ultimo
			   if(periodo>arrPeriodos[total-1]){
		          arrPeriodos.push(periodo);
			      despues(etiqueta);
		       }else{
		    	   //control = 1;
					for(i=0;i<total;i=i+1){
					   
					   if(periodo==arrPeriodos[i]){
						acum++;
					   //Si el periodo ya está agreado advertir y salir
					   if(control==0){
							if(acum==12){
							acum=0;
							control=1;
							alert("Ya existen todos los periodos agregados del "+anio);
							ocultarCalendario();
					        break;
					       	}
							 return false;
					   } else{
						    control=1;
							acum=0;
						    alert("Ya existe el periodo "+periodo);
					   		ocultarCalendario();
					  	    break;
					  		return false;
						}
					   }//else
                    }//end for buscar
					
					for(i=0;i<total;i=i+1){
                       if(periodo>arrPeriodos[i] && periodo<arrPeriodos[i+1]){
							
							var arrAux=[];
							var arrTemp=[];
							
							for(var j=0;j<=total-1;j=j+1){
								if(j==i+1){
									arrAux[j]=periodo;
									medio(j,periodo,etiqueta);//j=posicion 
								
								}//if j=i
								arrAux.push(arrPeriodos[j]);
							}//end for j
							
							arrTemp=arrAux;
							arrAux=[];
							arrPeriodos=arrTemp;
					   	}					   
					}//end for i
			    }
		}//end if
	}//end if total	
 
 
 //elemento.next("span").html(" ["+arrPeriodos+"] TOTAL:"+total);
 $("#"+option.idResultado).val(arrPeriodos).trigger("change");
   
 }//funcion crear periodo
	
function mostrarAnioCompleto(){
	control=0;
	$("#calendario ."+option.claseMes).each(function(){
	$(this).trigger("click");
	});
 /*
	//Reiniciar el plugin si han limpiado datos en formulario donde se llama
	if($("#"+option.idResultado).val()==''){
		 arrPeriodos=[];
		 $("#"+option.idResultado).val('');
	}
	
	 //Agregar todos los meses a la vez
	 var etiqueta="";
	 var periodos="";
	 var anio=$("#cboAnios option:selected").val();	
	  var anioAux;
	  if(anio!=anioAux){
		 total=0;
		 }	 	 
	 //Cargar labels y periodos-----------------------------------
	 for(i=1;i<=meses.length;i++){
		 var total=contar(arrPeriodos);//Saber cuantos elementos tiene el array actualmente	
		 var mes=(i > 9 ? i  : "0"+i );
		 var periodo=anio+mes;
		
	  	 etiqueta="<label class='etiqueta' id='lbl"+periodo+"'><span>"+anio+"-"+mes+"</span><span class='x'>X</span></label>";
		//Validaciones
			if(total==0){
			 	arrPeriodos.push(periodo);
			 	$("#"+option.idDestino).append(etiqueta);
				anioAux=anio;
			 }else{
				//Si es menor al primero 
				if(periodo<$("#"+option.idDestino+" label:first").attr("id").substr(-6)){
			 	arrPeriodos.unshift(periodo);
			 	$("#"+option.idDestino).prepend(etiqueta);
			}else{
				  //Si es mayor al ultimo
				  if(periodo>$("#"+option.idDestino+" label:last").attr("id").substr(-6)){
					  arrPeriodos.push(periodo);
				     $("#"+option.idDestino).append(etiqueta);
				  }else{
				    	
					
					    if(periodo==arrPeriodos[i-1]){
					    
						alert("Ya existe el periodo del a\u00F1o "+anio+" agragado.");
					   // con++;
						ocultarCalendario();
					    break;
					    return false;
					    }
                      						
						//Si esta en la mitad
						for(i=0;i<total;i=i+12){
                        if(periodo>$("#"+option.idDestino+" label:eq("+i+")") &&$("#"+option.idDestino+" label:eq("+i+12+")")){
							
							var arrAux=[];
							var arrTemp=[];
							
							for(var j=0;j<=total-1;j=j+1){
								if(j==i+1){
									arrAux[j]=periodo;
									$("#"+option.idDestino+" .etiqueta").eq(i).prepend(etiqueta);
								}//if j=i
								arrAux.push(arrPeriodos[j]);
							}//end for j
							
							arrTemp=arrAux;
							arrAux=[];
							arrPeriodos=arrTemp;
					   	}					   
					}//end for i mitad
				  }//else
				}
			 }//end else total
		 }//for -------------------------------------	
			  	
	 //arrPeriodos.push(anio+"01",anio+"02",anio+"03",anio+"04",anio+"05",anio+"06",anio+"07",anio+"08",anio+"09",anio+"10",anio+"11",anio+"12");
     //Ingresar las etiquetas traidas del servidor
	
	 ocultarCalendario();
	 //Caso contrario de hacer click en el año completo
     $("#"+option.idResultado).val(arrPeriodos);*/
}	
	//Funcion para borrar el periodo
	function borrarPeriodo(){
	//Eliminamos la etiqueta
	var idLabel=$(this).parent("label").attr("id");
	
	/*if(idLabel=="lblAnioCompl"){
	var name=$(this).parent("label").attr("name");
	var arrAux=[];
	var con=0;
		
	//Borrar item del array de los periodos
	 for(var k=0;k<arrPeriodos.length;k=k+1){
	   var anioSplit=arrPeriodos[k].slice(-4);
	   if(anioSplit==name){
	   arrPeriodos.splice(k,1);
	   break;
	   }
	  }//for
	  //Eliminamos la etiqueta
	$("#lblAnioCompl[name='"+name+"']").remove();
	}else{*/
	
		//var num=idLabel.substring(3);
		//Borrar item del array de los periodos
		var num=idLabel.slice(-6);
		for(var k=0;k<arrPeriodos.length;k=k+1){
	     if(arrPeriodos[k]==num)
			arrPeriodos.splice(k,1);
		}
	//Eliminamos la etiqueta
	$("#"+idLabel).remove();
	//}//fin else
	
	
	
	//elemento.next("span").html(" ["+arrPeriodos+"] TOTAL:"+arrPeriodos.length);	
	$("#"+option.idResultado).val(arrPeriodos).trigger("change");
	}
		
	//Funcion que añade el periodo al inicio
	function antes(etiqueta){
	etiqueta.insertBefore(".etiqueta:first").hide().fadeIn().addClass("resaltado");
	ocultarCalendario();
	}
	
	//Funcion que añade el periodo en el medio
	function medio(pos,periodo,etiqueta){
	etiqueta.insertAfter(".etiqueta:nth-child("+pos+")").hide().fadeIn().addClass("resaltado");
	ocultarCalendario();
	}
	
	//Funcion que añade el periodo al final
	function despues(etiqueta){
	etiqueta.insertAfter(".etiqueta:last").hide().fadeIn().addClass("resaltado");
	ocultarCalendario();
	}
	
	});//end each general
  	
	return this;
 };//end function   

})(jQuery);