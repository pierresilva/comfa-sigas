/**
 * 
 */
var URL=src();

$(function(){
	// Combos Vive Persona
	$("#cboDepto").change(function () {
   		$("#cboDepto option:selected").each(function () {
			elegido=$(this).val();
			if(elegido==0){
				$("#cboCiudad").html('<option value="0">Seleccione</option>');
				$("#cboZona").html('<option value="0">Seleccione</option>');
				$("#cboBarrio").html('<option value="0">Seleccione</option>');
			} else {
				$.post(URL+"phpComunes/ciudades.php", { elegido: elegido }, function(data){
					$("#cboCiudad").html(data);
					$("#cboZona").html('<option value="0">Seleccione</option>');
					$("#cboBarrio").html('<option value="0">Seleccione</option>');
				});	
			}						
        });
   });
	
	$("#cboCiudad").change(function () {
		$("#cboCiudad option:selected").each(function () {
			elegido=$(this).val();
			if(elegido==0){
				$("#cboZona").html('<option value="0">Seleccione</option>');
				$("#cboBarrio").html('<option value="0">Seleccione</option>');
			} else {
				$.post(URL+"phpComunes/zonas.php", { elegido: elegido }, function(data){
					$("#cboZona").html(data);
					$("#cboBarrio").html('<option value="0">Seleccione</option>');
				});
			}			
		});
	});

	$("#cboZona").change(function () {
   		$("#cboZona option:selected").each(function () {			
			elegido=$(this).val();
			if(elegido==0){
				$("#cboBarrio").html('<option value="0">Seleccione</option>');
			} else {
				$.post(URL+"phpComunes/barrios.php", { elegido: elegido }, function(data){
					$("#cboBarrio").html(data);
				});
			}			
        });
	});
	
	// Combos Nace Persona
	$("#cboPais2").change(function () {
		$("#cboPais2 option:selected").each(function() {
			elegido=$(this).val();
			if(elegido==0){
				$("#cboDepto2").html('<option value="0">Seleccione</option>');
				$("#cboCiudad2").html('<option value="0">Seleccione</option>');
			} else {
				$.post(URL+"phpComunes/departamentos.php", { elegido: elegido }, function(data){
					$("#cboDepto2").html(data);
					$("#cboCiudad2").html('<option value="0">Seleccione</option>');
				});
			}					
		});
	});
   
	$("#cboDepto2").change(function () {
   		$("#cboDepto2 option:selected").each(function() {
			elegido=$(this).val();
			if(elegido==0){
				$("#cboCiudad2").html('<option value="0">Seleccione</option>');
			} else {
				$.ajax({
					url: URL+"phpComunes/ciudades.php",
					type:"POST",
					data:{elegido: elegido},
					async: false,
					success: function(data){
						$("#cboCiudad2").html(data);
					}
				});
			}			
        });
	});
	
	//municipio y dep de correspondencia
	$("#cboDeptoCorresp").change(function () {
   		$("#cboDeptoCorresp option:selected").each(function() {
			elegido=$(this).val();
			if(elegido==0){
				$("#cboCiudadCorresp").html('<option value="0">Seleccione</option>');
			} else {
				$.post(URL+"phpComunes/ciudades.php", { elegido: elegido }, function(data){
					$("#cboCiudadCorresp").html(data);
				});
			}			
        });
	});
	
	$("#cboCiudadCorresp").change(function () {
		$("#cboCiudadCorresp option:selected").each(function () {
			elegido=$(this).val();
			$.post(URL+"phpComunes/zonas.php", { elegido: elegido }, function(data){
				$("#cboZonaCorresp").html(data);
			});			
		});
	});
	
	$("#cboZonaCorresp").change(function () {
		$("#cboZonaCorresp option:selected").each(function () {
			elegido=$(this).val();
			$.post(URL+"phpComunes/barrios.php", { elegido: elegido }, function(data){
				$("#cboBarrioCorresp").html(data);
			});
		});
	});

// conyuge

$("#cboDeptoC").change(function () {
		$("#cboDeptoC option:selected").each(function () {
			elegido=$(this).val();
			$.post(URL+"phpComunes/ciudades.php", { elegido: elegido }, function(data){
			$("#cboCiudadC").html(data);
			//$("#cboZonaC").html("<option value=0>Seleccione</option>");
		});			
    });
});

//Parametros para el combo2
$("#cboCiudadC").change(function () {
		$("#cboCiudadC option:selected").each(function () {
			elegido=$(this).val();
			$.post(URL+"phpComunes/zonas.php", { elegido: elegido }, function(data){
			$("#cboZonaC").html(data);
		});			
    });
});

$("#cboCiudadCA").change(function () {
	$("#cboCiudadCA option:selected").each(function () {
		elegido=$(this).val();
		$.post(URL+"phpComunes/zonas.php", { elegido: elegido }, function(data){
		$("#cboZonaCA").html(data);
	});			
});
});


$("#cboZonaC").change(function () {
		$("#cboZonaC option:selected").each(function () {
			elegido=$(this).val();
			$.post(URL+"phpComunes/barrios.php", { elegido: elegido }, function(data){
			$("#cboBarrioC").html(data);
		});			
    });
});

$("#cboZonaCA").change(function () {
	$("#cboZonaCA option:selected").each(function () {
		elegido=$(this).val();
		$.post(URL+"phpComunes/barrios.php", { elegido: elegido }, function(data){
		$("#cboBarrioCA").html(data);
	});			
});
});
//Combos Nace Persona
	$("#cboPaisC2").change(function () {
		$("#cboPaisC2 option:selected").each(function() {
			elegido=$(this).val();
			if(elegido==0){
				$("#cboDeptoC2").html('<option value="0">Seleccione</option>');
				$("#cboCiudadC2").html('<option value="0">Seleccione</option>');
			} else {
				$.post(URL+"phpComunes/departamentos.php", { elegido: elegido }, function(data){
					$("#cboDeptoC2").html(data);
					$("#cboCiudadC2").html('<option value="0">Seleccione</option>');
				});
			}					
		});
	});

	$("#cboDeptoC2").change(function () {
		$("#cboDeptoC2 option:selected").each(function() {
			elegido=$(this).val();
			if(elegido==0){
				$("#cboCiudadC2").html('<option value="0">Seleccione</option>');
			} else {
				$.post(URL+"phpComunes/ciudades.php", { elegido: elegido }, function(data){
					$("#cboCiudadC2").html(data);
				});
			}		
	    });
	});

$("#cboDeptoCA").change(function () {
	$("#cboDeptoCA option:selected").each(function() {
		elegido=$(this).val();
		$.post(URL+"phpComunes/ciudades.php", { elegido: elegido }, function(data){
		$("#cboCiudadCA").html(data);
	});			
});
});

$("#cboDeptoC2A").change(function () {
	$("#cboDeptoC2A option:selected").each(function() {
		elegido=$(this).val();
		$.post(URL+"phpComunes/ciudades.php", { elegido: elegido }, function(data){
		$("#cboCiudadC2A").html(data);
	});			
});
});

	//	Combos Nace Beneficiario
	$("#cboPaisB").change(function () {
		$("#cboPaisB option:selected").each(function () {
			elegido=$(this).val();
			if(elegido==0){
				$("#cboDeptoB").html('<option value="0">Seleccione</option>');
				$("#cboCiudadB").html('<option value="0">Seleccione</option>');
			} else {
				$.post(URL+"phpComunes/departamentos.php", { elegido: elegido }, function(data){				
					$("#cboDeptoB").html(data);
					$("#cboCiudadB").html('<option value="0">Seleccione</option>');
				});
			}			
		});
	});

	$("#cboDeptoB").change(function () {
		$("#cboDeptoB option:selected").each(function () {
			elegido=$(this).val();
			if(elegido==0){				
				$("#cboCiudadB").html('<option value="0">Seleccione</option>');
			} else {
				$.post(URL+"phpComunes/ciudades.php", { elegido: elegido }, function(data){
					$("#cboCiudadB").html(data);					
				});
			}			
		});
	});
})