var dia,mes,anio,fecha,mesNew,indexMes,anioActual,fecTemp,mesTemp,hoy,edad;
	 
function fechalarga(obj){
	$this=jQuery(obj);
			
	if($this.val()=='')
	{
		$this.val('mmddaaaa');
		return false;
	}
	
	fecha=$this.val();
	mes=fecha.slice(0,2);
	mesTemp=mes;
	dia=fecha.slice(2,4);
	anio=fecha.slice(4,8);
	indexMes=mes.slice(0,1);	
	anioActual=new Date();
	fechahoy=new Date();
	diahoy=fechahoy.getDate();
	meshoy=fechahoy.getMonth()+1;
	annohoy=fechahoy.getFullYear();
	fechahoy=new Date(annohoy,meshoy,diahoy);
	fecha2= new Date(anio,mes,dia);
	dias=fechahoy.getTime()-fecha2.getTime();
	if(dias<=0){
		alert("La fecha es mayor al dia de hoy!");
		$this.val('MMDDAAAA');
		obj.focus();
		return false;
	}
	
	if(fecha.length<8){
		alert("La fecha debe tener 8 digitos"); 
		obj.focus();
		$this.val('MMDDAAAA');
		return;
	}else{
		if(indexMes < 0 || indexMes > 1 || mes >12|| mes <1){
			alert("El mes esta mal digitado");
			obj.focus();
			$this.val('MMDDAAAA');
		}else{
			if(dia>31|| dia <1){
				alert("El dia debe tener de 1 a 31 dias");	
				$this.focus();
				$this.val('MMDDAAAA');
			}else{
				if(anio<1900 || anio>anioActual.getFullYear()){
					$this.val('MMDDAAAA');
					alert("El a\u00D1o esta incorrecto");
					return false;
					}
				switch(mes){
				case '01':mes='Ene'; break;
				case '02':mes='Feb'; break;
				case '03':mes='Mar'; break;
				case '04':mes='Abr'; break;
				case '05':mes='May'; break;
				case '06':mes='Jun'; break;
				case '07':mes='Jul'; break;
				case '08':mes='Ago'; break;
				case '09':mes='Sep'; break;
				case '10':mes='Oct'; break;
				case '11':mes='Nov'; break;
				default:mes='Dic'; break; 
				}//end switch
				//alert(mes+"-"+dia+"-"+anio);
				$this.val(mes+", "+dia+" de "+anio);
				fecTemp=mesTemp+"/"+dia+"/"+anio;
				//alert("Valor a guardar:"+fecTemp);
				$this.siblings(":hidden").attr("value",fecTemp);
			}//end else
		}//end else
	}//end else
}