(function($){

	var t = null;

    $.fn.wizard = function(options){
    
        debug(this);
        // build main options before element iteration
        
        var opts = $.extend({}, $.fn.wizard.defaults, options);
        // iterate and reformat each matched element
        var tot = this.size();
		this.css({visibility:'hidden',display:'none'});
		t = this;
		
        return this.each(function(i){
            $this = $(this);
			
			if(tot > 1){
				if(i == 0){
					//primero
					$this.css({visibility:'visible',display:'block'});
					$this.append('<div class="navi" style="float: right; position: relative"><a href="'+(i+1)+'">Siguiente</a></div>');
					$('div.navi a').bind('click',$.fn.next);
				}else if(i > 0 && i < tot-1){
					//entre el segundo y el penultimo
					$this.append('<div class="navi" style="float: right; position: relative"><a href="'+(i+1)+'">Siguiente</a></div>');
					$this.append('<div class="navi2" style="float: left; position: relative"><a href="'+(i-1)+'">Anterior</a></div>');
					$('div.navi a').bind('click',$.fn.next);
					$('div.navi2 a').bind('click',$.fn.prev);
				}else if(i == tot-1){
					//ultimo
					$this.append('<div class="navi2" style="float: left; position: relative"><a href="'+(i-1)+'">Anterior</a></div>');
					$('div.navi2 a').bind('click',$.fn.prev);
				}
			}
        });
        
    };
    
    function debug($obj){
        if (window.console && window.console.log) 
            window.console.log('hilight selection count: ' + $obj.size());
        
    };
    
	$.fn.prev = function(){
		var i = parseInt($(this).attr('href'));
		$(t).eq(i+1).fadeOut(300, function(){
			$(this).css({visibility:'hidden', display:'none'});
			$(t).eq(i).css({visibility:'visible', display:'block'});
			$(t).eq(i).fadeIn(300);
		});
		
		return false;
	};
    
	$.fn.next = function(){
		var i = parseInt($(this).attr('href'));
		$(t).eq(i-1).fadeOut(300, function(){
			$(this).css({visibility:'hidden', display:'none'});
			$(t).eq(i).css({visibility:'visible', display:'block'});
			$(t).eq(i).fadeIn(300);
		});
		
		return false;
	};
	
    $.fn.wizard.defaults = {
        wizardParent: 'div.forma',
        wizardChildren: 'ul.contenido'
    };
    
})(jQuery)
