//... abrir ventana para actualizar datos basicos de la persona..//
function actualizaBasico(idp){
	//$("#dialog-actualiza").dialog("destroy"); //Destruir dialog para liberar recursos
	$("#dialog-actualiza").dialog({
		width: 650,
		modal: true,
		draggable:false,
		zIndex:1000,
		resizable:false,
		closeOnEscape:true,
		open: function(){
			$(this).html('');
			$(this).load(URL+'phpComunes/actualizaDatosBasicos.php',{v0:idp},function(data){
			$(this).html(data);
			});
			
			},
		buttons: {
			'Actualizar' : function() {
				if(confirm("Esta seguro de actulizar la informacion?")==true){
					
					if(actualizarDatos()==true){
						buscarRelaciones();
						buscarGrupo();
						
			    		$(this).dialog('close');
						//$("#dialog-actualiza").dialog("destroy");
					}
				}
			},
			'Cancelar': function(){
				    $(this).dialog('close');
					 $(this).dialog('destroy');
			}
				
			},
			close: function() {
				$(this).dialog("destroy");
				 
			}
	});
}//end function
//-- FIN DIRECCIONES	

function actualizarDatos(){
	var salida=false;
	num=$.trim($("#txtnumeroB").val());
	if(num.length==0||num==''){
		alert("El campo numero no puede ser vacio!");
		$("#txtnumeroB").focus();
		return false;
		}
	if($("#txtpnombreB").val()==''){
		alert("Ingrese el primer nombre.!");
		$("#txtpnombreB").focus();
		return false;
		}
	if($("#txtpapellidoB").val()==''){
		alert("Ingrese el primer apellido.!");
		$("#txtpapellidoB").focus();
		return false;
		}
	if($("#txtfechanaceB").val()==''){
		alert("Ingrese la fecha de nacimiento.!");
		$("#txtfechanaceB").focus();
		return false;
		}
	var tdoc=$("#tipoDocB").val();
	var pnom=$("#txtpnombreB").val();
	var snom=$("#txtsnombreB").val();
	var pape=$("#txtpapellidoB").val();
	var sape=$("#txtsapellidoB").val();
	var fecha=$("#txtfechanaceB").val();
	var estado=$("#civilB").val();
	var sexo=$("#sexoB").val();
	var id=$("#txtidr").val();
	
	$.ajax({
		url: URL+'phpComunes/pdo.modificar.persona.simple.php',
		type: "GET",
		async:false,
		data: {v0:tdoc,v1:num,v2:pnom,v3:snom,v4:pape,v5:sape,v6:fecha,v7:estado,v8:sexo,v9:id},
		success: function(datos){
			if(datos==0){
				alert("No se pudo actualizar el registro!");
				return false;
				}
			alert("Registro Actualizado!");
			salida=true;
			return true;
		}
	});
	return salida;
}