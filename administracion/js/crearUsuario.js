// JavaScript Document
// autor Orlando Puentes A
// fecha marzo 14 de 2010
// objeto administración de los eventos del formulario de definiciones.php
var nuevo=0;
var modificar=0;

function validarCampos(op){
	if(document.forms[0].elements[0].value.length==0){
		alert("Digite Usuario!");
		document.forms[0].elements[0].focus();
		return;
		}
	if(op==1){
		guardarDatos();
		}	
	else{
		actualizarDatos();
		}
	}
	
function nuevoR(){
	nuevo=1;
	$("#bGuardar").show();
	limpiarCampos();
	document.forms[0].elements[0].focus();
	}

function guardarDatos(){
		$("#bGuardar").hide();
		var campo0=document.forms[0].elements[0].value;
		var campo1=document.forms[0].elements[1].value;
		var campo2=document.forms[0].elements[2].value;
		var campo3=document.forms[0].elements[3].value;
		$.ajax({
			url: 'crearUsuarioG.php',
			type: "POST",
			data: "submit=&v0="+campo0+"&v1="+campo1+"&v2="+campo2+"&v3="+campo3,
			success: function(datos){
				nuevoR();
				alert(datos);
			}
		});
		return false;
	}
	
function consultaDatos(){
		if(document.forms[0].elements[0].value.length==0)
			return;
		var campo0=document.forms[0].elements[0].value;
		$.ajax({
			url: 'definicionesC.php',
			cache: false,
			type: "GET",
			data: "submit=&v0="+campo0,
			success: function(datos){
				if(datos==0){
					alert("Lo lamento, el registro no existe!");
					return false;
				}
				var result=datos.split(",");
				document.forms[0].elements[0].value=result[0];
				document.forms[0].elements[1].value=result[1];
				document.forms[0].elements[2].value=result[2];
				document.forms[0].elements[3].value=result[3];
				document.forms[0].elements[4].value=result[4];
				document.forms[0].idr.value=result[0];
			}
		});
	}	

function actualizarDatos(){
		var campo0=document.forms[0].idr.value;
		var campo1=document.forms[0].elements[1].value;
		var campo2=document.forms[0].elements[2].value;
		var campo3=document.forms[0].elements[3].value;
		var campo4=document.forms[0].elements[4].value;
		
		$.ajax({
			url: 'definicionesA.php',
			cache: false,
			type: "POST",
			data: "submit=&v0="+campo0+"&v1="+campo1+"&v2="+campo2+"&v3="+campo3+"&v4="+campo4,
			success: function(datos){
				alert(datos);
				
			}
		});
		return false;
	}
	
function eliminarDato(){
		var msg = confirm("Desea eliminar este dato?")
		if ( msg ) {
			var id_registro=document.forms[0].idr.value;
			$.ajax({
				url: 'definicionesE.php',
				type: "GET",
				data: "v0="+id_registro,
				success: function(datos){
					limpiarCampos();
					alert(datos);	
				}
			});
		}
		return false;
	}	
function ayuda(){
}	