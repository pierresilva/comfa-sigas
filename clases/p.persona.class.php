<?php
/* autor:       orlando puentes
 * fecha:       12/10/2010
 * objetivo:    
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'conexion.class.php';

class Persona{
	//constructor
	var $con;
function Persona(){
		$this->con=new DBManager;
	}
	
function actualizar_persona_sql($sql){
	if($this->con->conectar()==true){
			//echo $sql;
			return mssql_query($sql,$this->con->conect);
		}
}
function insertSimple($campos){
		if($this->con->conectar()==true){
			$sql="INSERT INTO aportes015 (identificacion,idtipodocumento,papellido,sapellido,pnombre,snombre,usuario,fechasistema) VALUES ('".$campos[0]."',".$campos[1].",'".$campos[2]."','".$campos[3]."','".$campos[4]."','".$campos[5]."','".$campos[6]."',cast(getdate() as date))";
			//echo $sql;
			return mssql_query($sql,$this->con->conect);
		}
	}

function existe_persona($idti,$numero){
	if($this->con->conectar()==true){
			$sql="SELECT COUNT(*) AS cuenta FROM aportes015 WHERE idtipodocumento=$idti AND identificacion='$numero'";
			return mssql_query($sql,$this->con->conect);
		}
}	

function buscarPersonaExistente($identificacion,$pnombre,$snombre,$papellido,$sapellido){
	if($this->con->conectar()==true){
		$sql="SELECT isnull(a91.codigo,'') codigo, a15.idpersona, a15.identificacion, a15.pnombre, a15.snombre, a15.papellido, a15.sapellido 
				FROM aportes015 a15
				LEFT JOIN aportes091 a91 ON a91.iddetalledef = a15.idtipodocumento 
				WHERE identificacion = '$identificacion' OR ( ( a15.pnombre LIKE '$pnombre' AND len(rtrim(ltrim(a15.pnombre)))>0 ) AND ( a15.snombre LIKE '$snombre' ) AND ( a15.papellido LIKE '$papellido' AND len(rtrim(ltrim(a15.papellido)))>0 ) AND ( a15.sapellido LIKE '$sapellido' ) )";
		//echo $sql;
		return mssql_query($sql,$this->con->conect);
	}
}

function buscar_ID($idti,$numero){
	if($this->con->conectar()==true){
			$sql="SELECT idpersona FROM aportes015 WHERE idtipodocumento=$idti AND identificacion='$numero'";
			return mssql_query($sql,$this->con->conect);
		}
}	

function buscar_persona_documentos($idti,$numero){
	if($this->con->conectar()==true){
			$sql="SELECT idpersona,idtipodocumento,identificacion,rutadocumentos FROM aportes015 WHERE idtipodocumento=$idti AND identificacion='$numero'";
			return mssql_query($sql,$this->con->conect);
		}
}

function buscar_persona_tarjeta($tidoc,$docum){
	if($this->con->conectar()==true){
			$sql="SELECT * FROM aportes015 INNER JOIN aportes101 ON aportes015.idpersona=aportes101.idpersona WHERE identificacion=$docum AND aportes015.idtipodocumento=$tidoc";
			return mssql_query($sql,$this->con->conect);
		}
}

function mostrar_registro($cc){
		if($this->con->conectar()==true){
			$sql="SELECT aportes015.*,aportes091.detalledefinicion AS ti,ap2.detalledefinicion AS ec FROM aportes015
INNER JOIN aportes091 ON aportes015.idtipodocumento=aportes091.iddetalledef
LEFT JOIN aportes091 ap2 ON aportes015.idestadocivil=ap2.iddetalledef WHERE identificacion='$cc'";
			return mssql_query($sql,$this->con->conect);
		}
	}

function mostrar_registro2($td,$num){
		if($this->con->conectar()==true){
			$sql="SELECT aportes015.*,bono from aportes015 left join aportes101 on aportes101.idpersona=aportes015.idpersona WHERE idtipodocumento='$td' and identificacion='$num'";
			return mssql_query($sql,$this->con->conect);
		}
	}
	

	function mostrar_registro3($td,$num){
		if($this->con->conectar()==true){
			$sql="SELECT 
					*, 
					a15.idpersona, 
					a16.estado AS estadoafil, 
					a201.idpostulacion 
				  FROM 
				  	aportes015 a15 
				  LEFT JOIN aportes016 a16 ON a16.idpersona=a15.idpersona AND a16.estado in ('A') 
				  LEFT JOIN aportes201 a201 ON a15.idpersona = a201.idpersona 
				  WHERE 
				  	idtipodocumento='$td' AND 
				  	identificacion='$num'";
			//echo $sql;
			return mssql_query($sql,$this->con->conect);
		}
	}
	
function buscar_ruta_documentos($idp){
	if($this->con->conectar()==true){
			$sql="SELECT rutadocumentos FROM aportes015 WHERE idpersona=$idp";
			//echo $sql;
			return mssql_query($sql,$this->con->conect);
		}
	}

	function mostrar_persona_id($idp){
		if($this->con->conectar()==true){
			$sql="SELECT idpersona, idtipodocumento, identificacion, papellido, sapellido, pnombre, snombre, sexo, direccion, LTRIM(RTRIM(idbarrio)) as idbarrio, telefono, celular, email, idpropiedadvivienda, idtipovivienda, LTRIM(RTRIM(idciuresidencia)) as idciuresidencia, LTRIM(RTRIM(iddepresidencia)) as iddepresidencia, LTRIM(RTRIM(idzona)) as idzona, idestadocivil, fechanacimiento, LTRIM(RTRIM(idciunace)) as idciunace, LTRIM(RTRIM(iddepnace)) as iddepnace, capacidadtrabajo, idprofesion, ruaf, biometria, rutadocumentos, nombrecorto,  datediff(dd,fechanacimiento,getdate())/365.25 as edad, idpais,idpaisreside FROM aportes015 WHERE idpersona=$idp";
			return mssql_query($sql,$this->con->conect);
		}
	}

//antes
/*function mostrar_registro_para($id,$op,$td=0){
	if($this->con->conectar()==true){
		switch($op){
			case 1 : 
				if($td==0){
				$sql="SELECT idpersona,aportes091.codigo,identificacion,papellido,sapellido,pnombre,snombre FROM aportes015 INNER JOIN aportes091 ON aportes015.idtipodocumento=aportes091.iddetalledef WHERE identificacion='$id'";
				}
				else{
				$sql="SELECT DISTINCT idpersona,pnombre,snombre,papellido,sapellido from vis_activos where idtipodocumento=$td and identificacion='$id'";
				}
				break;
				case 2 : $sql="Select idpersona,pnombre,snombre,papellido,sapellido from vis_activos where pnombre like '$id%'";
				break;
				case 3 : $sql="Select idpersona,pnombre,snombre,papellido,sapellido from vis_activos where papellido like '$id%'";
				break;
			}
			return mssql_query($sql,$this->con->conect);
		}
	}
*/

function mostrar_registro_para($id,$op,$td=0,$pn='',$pa=''){
	if($this->con->conectar()==true){
		switch($op){
			case 1 : 
				if($td==0){
					$sql="SELECT idpersona,aportes091.codigo,identificacion,papellido,sapellido,pnombre,snombre FROM aportes015 INNER JOIN aportes091 ON aportes015.idtipodocumento=aportes091.iddetalledef WHERE identificacion='$id'";
				}else{
					$sql="SELECT DISTINCT idpersona,identificacion,pnombre,snombre,papellido,sapellido from vis_activos where idtipodocumento=$td and identificacion='$id'";
				}
				break;
			case 2 : $cad=''; 
				if($pn!='' && $pa!=''){
					$cad="pnombre like '".$pn."%' and papellido like'".$pa."%'";
				}else {
					if($pn!='' && $pa==''){
						$cad="pnombre like '".$pn."%'";
					}else{
						$cad="papellido like '".$pa."%'";
					}
				}
				$sql="Select idpersona,identificacion,pnombre,snombre,papellido,sapellido from vis_activos where ".$cad;
				break;
			case 3 :
				if($td==0){
					$sql="SELECT idpersona,aportes091.codigo,identificacion,papellido,sapellido,pnombre,snombre FROM aportes015 INNER JOIN aportes091 ON aportes015.idtipodocumento=aportes091.iddetalledef WHERE identificacion='$id'";
				}else{
					$sql="SELECT DISTINCT idpersona,identificacion,pnombre,snombre,papellido,sapellido from aportes015 where idtipodocumento=$td and identificacion='$id'";
				}				
				break;
			case 4 : $cad='';
				if($pn!='' && $pa!=''){
					$cad="pnombre like '".$pn."%' and papellido like'".$pa."%'";
				}else {
					if($pn!='' && $pa==''){
						$cad="pnombre like '".$pn."%'";
					}else{
						$cad="papellido like '".$pa."%'";
					}
				}
				$sql="Select idpersona,identificacion,pnombre,snombre,papellido,sapellido from aportes015 where ".$cad;
				break;
		}		
		return mssql_query($sql,$this->con->conect);
	}
}


function mostrar_registro_inactivos($id,$op,$td=0,$pn='',$pa=''){
		if($this->con->conectar()==true){
			switch($op){
				case 1 : $sql="SELECT idpersona,pnombre,snombre,papellido,sapellido from vis_inactivos where idtipodocumento=$td and identificacion='$id'";
				break;
				case 2 : $cad=''; 
				  		if($pn!=''&&$pa!=''){
				         $cad="pnombre like '".$pn."%' and papellido like'".$pa."%'";
						 }else {
						 if($pn!=''&&$pa==''){
						 $cad="pnombre like '".$pn."%'";
						 }else{
						 $cad="papellido like '".$pa."%'";
						 }
						}
						 $sql="Select idpersona,pnombre,snombre,papellido,sapellido from vis_inactivos where ".$cad;
				break;
			}
			return mssql_query($sql,$this->con->conect);
		}
	}
/*function mostrar_registro_inactivos($id,$op,$td=0){
		if($this->con->conectar()==true){
			switch($op){
				case 1 : $sql="SELECT idpersona,pnombre,snombre,papellido,sapellido from vis_inactivos where idtipodocumento=$td and identificacion='$id'";
				break;
				case 2 : $sql="Select idpersona,pnombre,snombre,papellido,sapellido from vis_inactivos where pnombre like '$id%'";
				break;
				case 3 : $sql="Select idpersona,pnombre,snombre,papellido,sapellido from vis_inactivos where papellido like '$id%'";
				break;
			}
			return mssql_query($sql,$this->con->conect);
		}
	}*/

function insert_completo($campos){
		if($this->con->conectar()==TRUE){
			$sql="Insert into aportes015 (idtipodocumento, identificacion, papellido, sapellido, pnombre, snombre, sexo, fechanacimiento, capacidadtrabajo,fechaafiliacion,estadoafiliacion,validado,fechasistema,usuario )
            values('".$campos[0]."','".$campos[1]."','".$campos[2]."','".$campos[3]."','".$campos[4]."','".$campos[5]."','".$campos[6]."','".$campos[7]."','".$campos[8]."',cast(getdate() as date),'A','N',cast(getdate() as date),'".$campos[9]."')";
			//echo $sql;
          return mssql_query($sql,$this->con->conect);
		}
	}

function insert_todo($campos){
    if($this->con->conectar()==true){
        if($campos[24]==0)
        $sql="insert into aportes015(idtipodocumento, identificacion, pnombre,snombre, papellido,sapellido, sexo,direccion,idbarrio, telefono, celular, email, idtipovivienda, iddepresidencia,idciuresidencia,idzona,idestadocivil,fechanacimiento,iddepnace,idciunace,capacidadtrabajo,idprofesion,ruaf, biometria, fechaafiliacion,estado, validado,usuario,fechasistema,nombrecorto) values('".$campos[2]."','".$campos[3]."','".$campos[4]."','".$campos[5]."','".$campos[6]."','".$campos[7]."','".$campos[8]."','".$campos[9]."','".$campos[10]."','".$campos[11]."','".$campos[12]."','".$campos[13]."','".$campos[14]."','".$campos[15]."','".$campos[16]."','".$campos[17]."','".$campos[18]."','".$campos[19]."','".$campos[20]."','".$campos[21]."','".$campos[22]."','".$campos[23]."','N','N',cast(getdate() as date),'S','N','".$campos[25]."',cast(getdate() as date),'".$campos[27]."')";
        else
        $sql="update aportes015 set idtipodocumento='". $campos[2] ."',identificacion='". $campos[3] ."',pnombre='". $campos[4] ."',snombre='". $campos[5] ."',papellido='". $campos[6] ."',sapellido='". $campos[7] ."',sexo='". $campos[8] ."',direccion='". $campos[9]. "',idbarrio=". $campos[10] .",telefono='". $campos[11] ."',celular='". $campos[12]. "',email='". $campos[13]. "',idtipovivienda='". $campos[14] ."',iddepresidencia='". $campos[15] ."',idciuresidencia='" .$campos[16]. "',idzona='". $campos[17] ."',idestadocivil=". $campos[18] .",fechanacimiento='". $campos[19] ."',iddepnace='". $campos[20] ."',idciunace='". $campos[21]. "',capacidadtrabajo='". $campos[22] ."',idprofesion=". $campos[23] ." where idpersona=".$campos[26];
        return mssql_query($sql,$this->con->conect);
        }
}

//para la afiliacion
function insert_todo2($campos){
    if($this->con->conectar()==true){
        $sql="insert into aportes015 (idtipodocumento, identificacion, papellido, sapellido, pnombre, snombre, sexo, direccion, idbarrio, telefono, celular, email, idtipovivienda, iddepresidencia, idciuresidencia, idzona, idestadocivil, fechanacimiento, idciunace, iddepnace, capacidadtrabajo, idprofesion, nombrecorto,usuario, ruaf, biometria, fechaafiliacion, estado,validado,fechasistema) values(".$campos[1].",'".$campos[2]."','".$campos[3]."','".$campos[4]."','".$campos[5]."','".$campos[6]."','".$campos[7]."','".$campos[8]."','".$campos[9]."','".$campos[10]."','".$campos[11]."','".$campos[12]."','".$campos[13]."','".$campos[14]."','".$campos[15]."','".$campos[16]."','".$campos[17]."','".$campos[18]."','".$campos[19]."','".$campos[20]."','".$campos[21]."','".$campos[22]."','".$campos[23]."','".$campos[24]."','N','N',cast(getdate() as date),'A','N',cast(getdate() as date))";
      // echo $sql;
        return mssql_query($sql,$this->con->conect);
        }
}

function actualizar_registro($campos){
	if($this->con->conectar()==true){
		$sql="UPDATE aportes015 SET idtipodocumento=".$campos[1].", identificacion='".$campos[2]."', papellido='".$campos[3]."', sapellido='".$campos[4]."', pnombre='".$campos[5]."', snombre='".$campos[6]."', sexo='".$campos[7]."', direccion='".$campos[8]."', idbarrio='".$campos[9]."', telefono='".$campos[10]."', celular='".$campos[11]."', email='".$campos[12]."', idpropiedadvivienda='".$campos[13]."', idtipovivienda='".$campos[14]."', idciuresidencia='".$campos[16]."', iddepresidencia='".$campos[15]."', idzona='".$campos[17]."', idestadocivil='".$campos[18]."', fechanacimiento='".$campos[19]."', idciunace='".$campos[21]."', iddepnace='".$campos[20]."',idpais=".$campos[26].", capacidadtrabajo='".$campos[22]."',idprofesion='".$campos[23]."',usuario='".$campos[99]."', nombrecorto='".$campos[24]."' ,rutadocumentos='".$campos[25]."',idpaisreside=".$campos[27]." WHERE idpersona='".$campos[0]."'";
		return mssql_query($sql,$this->con->conect);
	}
}
	
function actualizar_persona($campos){
    if($this->con->conectar()==true){
        $sql="update aportes015 set idtipodocumento=".$campos[1].",identificacion='".$campos[2]."',papellido='".$campos[4]."',sapellido='".$campos[6]."',pnombre='".$campos[3]."',snombre='".$campos[5]."',sexo='".$campos[7]."',idestadocivil='".$campos[8]."',fechanacimiento='".$campos[9]."',idciunace='".$campos[10]."',iddepnace='".$campos[11]."',capacidadtrabajo='".$campos[12]."',usuario='".$campos[13]."',estadoafiliacion='A',fechasistema=cast(getdate() as date) ,nombrecorto='".$campos[24]."' where idpersona=".$campos[0];
       // echo $sql;
        return mssql_query($sql,$this->con->conect);
    }
}

function actualizar_trabajador($campos){
    if($this->con->conectar()==true){
        $sql="update aportes015 set idtipodocumento=".$campos[1].",identificacion='".$campos[2]."',papellido='".$campos[5]."',sapellido='".$campos[6]."',pnombre='".$campos[3]."',snombre='".$campos[4]."',nombrecorto='".$campos[7]."',usuario='".$campos[8]."',fechasistema=cast(getdate() as date) where idpersona=".$campos[0];
        return mssql_query($sql,$this->con->conect);
    }
}

	/**
	 * Obtiene la inforamci�n de los beneficiarios seg�n la agencia especificada 
	 * y el estado requerido (por defecto activos)
	 *
	 * @param int $idAgencia
	 * @param char $estado A o I
	 * @return ifx resultset or false
	 */

function obtener_beneficiarios_por_agencia($idAgencia, $estado = 'A'){
		if($this->con->conectar()==true){
			$sql="select
					b.idpersona,
					t.identificacion as documento_trab,
					t.papellido as papellido_trab,
					t.sapellido as sapellido_trab,
					t.pnombre as pnombre_trab,
					t.snombre as snombre_trab,
					rb.giro,
					b.idtipodocumento,
					td.codigo,
					b.identificacion as documento_ben,
					b.papellido,
					b.sapellido,
					b.pnombre,
					b.snombre,
					b.sexo,
					par.detalledefinicion as parentesco,
					b.direccion,
					b.telefono,
					b.discapacitado,
					b.fechanacimiento,
					datediff(dd,fechanacimiento,getdate())/365.25 as edad,
					CASE WHEN af.idagencia='01' THEN 'NEIVA' WHEN af.idagencia='02' THEN 'GARZON' WHEN af.idagencia='03' THEN 'PITALITO' WHEN af.idagencia='04' THEN 'LA PLATA' END as agencia,
					e.idempresa,
					e.nit,
					e.codigosucursal,
					e.razonsocial,
					e.direccion as direccion_empr,
					e.telefono as telefono_empr
				  from
					aportes015 b
					inner join aportes021 rb on rb.idbeneficiario=b.idpersona
					inner join aportes016 af on af.idpersona = rb.idtrabajador
					inner join aportes048 e on e.idempresa=af.idempresa
					inner join aportes015 t on t.idpersona=rb.idtrabajador
					inner join aportes091 td on td.iddetalledef=b.idtipodocumento and td.iddefinicion=1
					inner join aportes091 par on par.iddetalledef = rb.idparentesco
				  where
					b.estado='$estado' and
					af.idagencia='$idAgencia'
				  order by e.nit";
			return mssql_query($sql,$this->con->conect);
		}
		return false;
	}
	
	
	function contar_trabajadores_por_identificacion($identificacion){
		if($this->con->conectar()==true){
			$sql="select
					count(p.idpersona) as cuenta 
				  from
					aportes015 p
				  inner join aportes016 af on af.idpersona = p.idpersona
				  where identificacion='$identificacion'";
			$result = mssql_query($sql,$this->con->conect);
			if($result){
				$arrResult = mssql_fetch_row($result);
				mssql_free_result($result);
				return intval($arrResult["cuenta"]);
			}
			return false;
		}
		return false;
	}
	
	function contar_beneficiarios_por_identificacion($identificacion){
		if($this->con->conectar()==true){
			$sql="select
					count(p.idpersona) as cuenta 
				  from
					aportes015 p
				  inner join aportes021 af on af.idbeneficiario = p.idpersona
				  where identificacion='$identificacion'";
			$result = mssql_query($sql,$this->con->conect);
			if($result){
				$arrResult = mssql_fetch_row($result);
				mssql_free_result($result);
				return intval($arrResult["cuenta"]);
			}
			return false;
		}
		return false;
	}
	
	function contar_conyuges_por_identificacion($cedarch){
		if($this->con->conectar()==true){
			$sql="select 
					count(c.idpersona) as cuenta
				  from
					aportes015 c
					inner join aportes021 rb on rb.idconyuge = c.idpersona and rb.idconyuge=rb.idbeneficiario and rb.idparentesco=34
				  where
					c.identificacion='$cedarch'";
			$result = mssql_query($sql,$this->con->conect);
			if($result){
				$arrResult = mssql_fetch_row($result);
				mssql_free_result($result);
				return intval($arrResult["cuenta"]);
			}
			return false;
		}
		return false;
	}
	
	function obtener_datos_trabajador_identificacion($identificacion){
		if($this->con->conectar()==true){
			$sql="select 
					e.nit,
					e.codigosucursal,
					e.razonsocial,
					p.idpersona,
					p.idtipodocumento,
					p.identificacion,
					p.papellido,
					p.sapellido,
					p.pnombre,
					p.snombre,
					p.estado,
					p.direccion,
					p.telefono
				  from
					aportes015 p
					inner join aportes016 af on af.idpersona = p.idpersona
					inner join aportes048 e on e.idempresa=af.idempresa
				  where
					identificacion='$identificacion'";
			$result = mssql_query($sql,$this->con->conect);
			if($result){
				$arrResult = mssql_fetch_row($result);
				mssql_free_result($result);
				return $arrResult;
			}
			return false;
		}
		return false;
	}
	
	
	function obtener_datos_trabajador_idpersona($idpersona){
		if($this->con->conectar() == true){
			$sql="select
					e.nit,
					e.codigosucursal,
					e.razonsocial,
					p.idpersona,
					p.idtipodocumento,
					p.identificacion,
					p.papellido,
					p.sapellido,
					p.pnombre,
					p.snombre,
					p.estado,
					p.direccion,
					p.telefono
				  from
					aportes015 p
					inner join aportes016 af on af.idpersona = p.idpersona
					inner join aportes048 e on e.idempresa=af.idempresa
				  where
					p.idpersona = $idpersona";
			$result = mssql_query($sql,$this->con->conect);
			if($result){
				$arrResult = mssql_fetch_array($result);
				mssql_free_result($result);
				return $arrResult;
			}
			return false;
		}
		return false;
	}
	
	
	function obtener_conyuges_por_identificacion_trabajador($cedTrab,$compPer='S'){
		if($this->con->conectar()==true){
			$sql="select 
					c.idpersona,
					c.idtipodocumento,
					c.identificacion,
					c.papellido,
					c.sapellido,
					c.pnombre,
					c.snombre,
					c.estado,
					c.direccion,
					c.telefono
				  from
					aportes015 t
					inner join aportes021 rb on rb.idtrabajador = t.idpersona and rb.idparentesco=34
					inner join aportes015 b on b.idpersona = rb.idconyuge
				  where
					t.identificacion='$cedTrab'";
			$result = mssql_query($sql,$this->con->conect);
			if($result)
				return $result;
			return false;
		}
		return false;
	}
	
	
	function obtener_beneficiarios_trabajador($cedarch){
		if($this->con->conectar()==true){
			$sql="select 
					b.idpersona,
					b.idtipodocumento,
					b.identificacion,
					b.papellido,
					b.sapellido,
					b.pnombre,
					b.snombre,
					b.estado,
					b.direccion,
					b.telefono,
					rb.giro
				  from
					aportes015 t
				  inner join aportes021 rb on rb.idtrabajador = t.idpersona and rb.idparentesco<>34
				  inner join aportes015 b on b.idpersona = rb.idbeneficiario
				  where
					t.identificacion='$cedTrab'";
			$result = mssql_query($sql,$this->con->conect);
			if($result)
				return $result;
			return false;
		}
		return false;
	}
	
	function obtener_datos_persona($cedarch){
		if($this->con->conectar()==true){
			$sql = "select 
						idpersona,
						idtipodocumento,
						identificacion,
						papellido,
						sapellido,
						pnombre,
						snombre,
						estado,
						direccion,
						telefono,
						sexo,
						fechanacimiento,
						estado
					from
						aportes015
					where
						identificacion='$cedarch'";
			$result = mssql_query($sql,$this->con->conect);
			if($result){
				$datos = mssql_fetch_array($result);
				mssql_free_result($result);
				return $datos;	
			}
			return false;
		}
		return false;
	}
	
	function obtener_persona_tercero_por_identificacion($cedarch){
		if($this->con->conectar()==true){
			$sql = "SELECT
						aportes015.idpersona,
						idtipodocumento,
						identificacion,
						papellido,
						sapellido,
						pnombre,
						snombre,
						direccion,
						telefono,
						sexo,
						fechanacimiento,
						aportes015.estado
					FROM
						aportes015,
						aportes018
					WHERE
						aportes015.idpersona = aportes018.idtercero AND
						aportes015.identificacion='$cedarch'";
			$result = mssql_query($sql,$this->con->conect);
			if($result){
				$datos = mssql_fetch_array($result);
				mssql_free_result($result);
				return $datos;
			}
			return false;
		}
		return false;
	}
	
	
	function obtener_datos_trabajador_por_identificacion_conyuge($cedarch){
		if($this->con->conectar()==true){
			$sql = "select 
					t.idpersona,
					t.idtipodocumento,
					t.identificacion,
					t.papellido,
					t.sapellido,
					t.pnombre,
					t.snombre,
					t.estado,
					t.direccion,
					t.telefono,
					t.sexo,
					t.fechanacimiento,
					e.nit,
					e.codigosucursal,
					e.razonsocial,
					e.seccional
					from
					aportes015 c
					inner join aportes021 rb on rb.idconyuge = c.idpersona and rb.idparentesco=34
					inner join aportes015 t on t.idpersona = rb.idtrabajador 
					inner join aportes016 af on af.idpersona = t.idpersona
					inner join aportes048 e on e.idempresa = af.idempresa
					where c.identificacion ='$cedTrab'";
			$result = mssql_query($sql,$this->con->conect);
			if($result){
				$datos = mssql_fetch_row($result);
				mssql_free_result($result);
				return $datos;	
			}
			return false;
		}
		return false;
	}
	
	
	function obtener_beneficiarios_inactivar_por_edad($fechaNacimiento = ''){
		$strFecha = ($fechaNacimiento != '')?"'".$fechaNacimiento."'":"CAST(GETDATE() AS DATE)";
		if($this->con->conectar()==true){
			$query ="select distinct
					b.idpersona,
					b.idtipodocumento,
					b.identificacion,
					b.papellido,
					b.sapellido,
					b.pnombre,
					b.snombre,
					b.estado,
					b.direccion,
					b.telefono,
					rb.giro,
					b.capacidadtrabajo,
					rb.idparentesco,
					par.detalledefinicion as parentesco,
					b.discapacitado,
					b.fechanacimiento,
					year(getdate()-b.fechanacimiento -1)-1900 as edad
					from
					aportes015 t
					inner join aportes021 rb on rb.idtrabajador = t.idpersona and rb.idparentesco<>34
					inner join aportes015 b on b.idpersona = rb.idbeneficiario
					inner join aportes091 par on par.iddetalledef = rb.idparentesco
					where
					($strFecha-b.fechanacimiento) >= 6939 and
					b.estado='A' and
					rb.idparentesco in (35,37,38) and
					rb.giro='S' and
					b.capacidadtrabajo='N'";
			$result = mssql_query($query,$this->con->conect);
			if($result)
				return $result;	
			return false;
		}
		return false;
	}
	
	
	function contar_beneficiarios_inactivar_por_edad($fechaNacimiento = ''){
		$strFecha = ($fechaNacimiento != '')?"'".$fechaNacimiento."'":"
CAST(GETDATE() AS DATE)";
		if($this->con->conectar()==true){
			$query ="select count(distinct b.idpersona) as cuenta
					from
					aportes015 t
					inner join aportes021 rb on rb.idtrabajador = t.idpersona and rb.idparentesco<>34
					inner join aportes015 b on b.idpersona = rb.idbeneficiario
					inner join aportes091 par on par.iddetalledef = rb.idparentesco
					where
					($strFecha-b.fechanacimiento) >= 6939 and
					b.estado='A' and
					rb.idparentesco in (35,37,38) and
					rb.giro='S' and
					b.capacidadtrabajo='N'";
			$result = mssql_query($query,$this->con->conect);
			if($result){
				$arrResult = mssql_fetch_row($result);
				return intval($arrResult["cuenta"]);
			}
			return false;
		}
		return false;
	}
	
	function cambiar_bandera_giro_beneficiarios_por_edad($fechaNacimiento = '',$giro = 'N'){
		$strFecha = ($fechaNacimiento != '')?"'".$fechaNacimiento."'":"
CAST(GETDATE() AS DATE)";
		if($this->con->conectar()==true){
			$query ="update 
						aportes021 set giro='$giro' 
					 where 
						giro='S' and 
						idparentesco<>34 and
						idparentesco in (35,37,38) and
						idbeneficiario in (select 
							rb.idbeneficiario
						 from 
							aportes021 rb 
							inner join aportes015 b on b.idpersona = rb.idbeneficiario
						 where
							rb.idparentesco<>34 and
							($strFecha-b.fechanacimiento) >= 6939 and
							b.estado='A' and
							rb.idparentesco in (35,37,38) and
							rb.giro='S' and
							b.capacidadtrabajo='N')";
			$result = mssql_query($query,$this->con->conect);
			if($result)
				return mssql_affected_rows($result);
			return false;
		}
		return false;
	}
	
	
	function obtener_beneficiarios_sin_certificados($edad = 12,$tiposCertificados = array(),$anoActual){
		if($this->con->conectar()==true){
			$anoAnterior = $anoActual - 1;
			$query = "select distinct
						b.idpersona,
						b.idtipodocumento,
						b.identificacion,
						b.papellido,
						b.sapellido,
						b.pnombre,
						b.snombre,
						b.fechanacimiento,
						year(getdate()-b.fechanacimiento -1)-1900 as edad,
						b.estado,
						rb.giro,
						t.identificacion as identificacion_trab
					  from 
						aportes015 b
						inner join aportes021 rb on rb.idbeneficiario = b.idpersona
						inner join aportes015 t on t.idpersona = rb.idtrabajador
					  where
					    b.estado='A' and
						rb.giro='S' and
						year(getdate()-b.fechanacimiento -1)-1900 = $edad and
						b.idpersona in (select idbeneficiario from aportes026 where year(fechapresentacion)=". $anoAnterior ." and idtipocertificado in (". implode(",",$tiposCertificados) .")) and
						b.idpersona not in (select idbeneficiario from aportes026 where year(fechapresentacion)=". $anoActual ." and idtipocertificado in (". implode(",",$tiposCertificados) ."))";
			$result = mssql_query($query,$this->con->conect);
			if($result)
				return mssql_affected_rows($result);
			return false;
		}
		return false;
	}
	
	
	function obtener_responsable_embargo($idTrabajador,$idConyuge){
		if($this->con->conectar()==true){
			$condicionResponsable = (!is_numeric($idConyuge))? '' : " and idtercero=$idConyuge";
			$query = "SELECT
						r.idpersona,
						r.identificacion,
						r.papellido,
						r.sapellido,
						r.pnombre,
						r.snombre
						FROM 
						aportes018 emb
						inner join aportes015 r on r.idpersona=emb.idtercero
						WHERE 
						idtrabajador=$idTrabajador ". $condicionResponsable;
			$result = mssql_query($query,$this->con->conect);
			if($result){
				$arrResultado = mssql_fetch_row($result);
				mssql_free_result($result);
				return $arrResultado;
			}
			return false;
		}
		return false;
	}
	
function obtener_datos_trabajador_fallecido($idTrabajador){
		if($this->con->conectar()==true){
			$query = "SELECT 
						p.idpersona,
						p.identificacion,
						p.papellido,
						p.sapellido,
						p.pnombre,
						p.snombre
					  FROM 
						aportes019 mu
						inner join aportes015 p on p.idpersona=mu.idtrabajador
					  WHERE 
						idtrabajador=$idTrabajador";
			$result = mssql_query($query,$this->con->conect);
			if($result){
				$arrResultado = mssql_fetch_row($result);
				mssql_free_result($result);
				return $arrResultado;
			}
			return false;
		}
		return false;
	}

function buscar_imagenes($idp){
	if($this->con->conectar()==true){
			$sql="SELECT archivo FROM aportes050 WHERE idpersona=$idp";
			return mssql_query($sql,$this->con->conect);
		}
	}
	
//Traer datos de trabajador activo afiliado a una empresa
function buscar_persona_activa_empresa($nit, $td, $num){
	if($this->con->conectar()==true){
		$sql = "select a15.idpersona ,isnull(a15.pnombre,'') pnombre,isnull(a15.snombre,'') snombre,isnull(a15.papellido,'') papellido,isnull(a15.sapellido,'') sapellido
				from aportes015 a15
				  inner join aportes016 a16
				    on a16.idpersona=a15.idpersona
				  inner join aportes048 a48
				    on a48.idempresa=a16.idempresa
				where a15.idtipodocumento=$td and a15.identificacion='".$num."' and a48.nit='".$nit."'";
		return mssql_query($sql,$this->con->conect);
	}
}

//Traer datos basicos de la persona
function buscar_datos_basicos($td, $num){
	if($this->con->conectar()==true){
		$sql = "select a15.idpersona ,isnull(a15.pnombre,'') pnombre,isnull(a15.snombre,'') snombre,isnull(a15.papellido,'') papellido,isnull(a15.sapellido,'') sapellido
		from aportes015 a15
		where a15.idtipodocumento=$td and a15.identificacion='".$num."'";
		return mssql_query($sql,$this->con->conect);
	}
}
	//Traer datos del conyuge 
	function buscarConyugeNuevo($td,$num){
		if($this->con->conectar()==true){
			$sql="SELECT a15.idpersona,a15.pnombre,a15.snombre,a15.papellido,a15.sapellido,a15.nombrecorto,a15.sexo,a15.estado estado15,a15.idtipovivienda,a15.iddepresidencia,a15.idciuresidencia,a15.idzona,a15.idbarrio,a15.direccion,a15.email,
			a15.telefono,a15.celular,a15.fechanacimiento,a15.idpais,a15.iddepnace,a15.idciunace,a15.idestadocivil,a15.capacidadtrabajo,a15.idprofesion,a15.rutadocumentos,
			isnull(a16.idformulario,0) idformulario,a16.salario,a16.fechaingreso,a48.nit,a48.razonsocial,a16.tipoformulario,tf.detalledefinicion
			FROM aportes015 a15 LEFT JOIN aportes016 a16 ON a16.idpersona=a15.idpersona AND a16.idformulario=isnull((SELECT TOP 1 b16.idformulario FROM aportes016 b16 WHERE b16.idpersona=a15.idpersona AND b16.estado='A' AND b16.primaria='S'),0)
			LEFT JOIN aportes048 a48 ON a48.idempresa=a16.idempresa LEFT JOIN aportes091 tf ON tf.iddetalledef=a16.tipoformulario
			WHERE a15.idtipodocumento='$td' AND a15.identificacion='$num'";
			//echo $sql;
			return mssql_query($sql,$this->con->conect);
		}
	}
}
?>