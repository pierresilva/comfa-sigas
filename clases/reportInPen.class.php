<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * objetivo:   Consultar datos para cargarlos en la plantilla PDF de reportes de radicaciones.
 *
 * @author ANDRES FELIPE PERDOMO
 */

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
include_once $raiz . DIRECTORY_SEPARATOR . 'rsc' . DIRECTORY_SEPARATOR . 'conexion.class.php';
ini_set('memory_limit', '1512M');
set_time_limit(220);

/***************************CLASE reportInPen******************************************/
/***Utilizada para hacer la conexion y obtener datos necesarios para los reportes******/
/***atravez de sus metodos.************************************************************/
class reportInPen {

    var $con;
    var $fechaSistema;
    function __construct(){
        $this->con = new DBManager();
    }
    
    /***************************METODO mostrar_datos_reporte00_01**************************/
    /***El cual se encarga de ejecutar el procedimiento almacenado obtener_aportes001_01***/
    /***el cual hace la consulta para el reporte aportes001_01*****************************/
    function mostrar_datos_reporte00_01($fechaIni, $fechafin,$agencia, $usurio, $tiporadicacion, $tipoafiliado){
        
        if($this->con->conectar() == true){
            $sql = "EXEC obtener_aportes001_01 '".$fechaIni."', '".$fechafin."',$agencia, '".$usurio."', $tiporadicacion,'".$tipoafiliado."'";
            return mssql_query($sql, $this->con->conect);      
        }
    }
    
    /***************************METODO mostrarCartaAceptacionDocumento***************************************/
    /***El cual se encarga de realizar la consuñlta query por medio del numero de documento y agencia********/
    /******************* para obtener los datos para las cartas de aceptacion********************************/
    function mostrarCartaAceptacionDocumento($agencia, $numeroDoc){
        if($this->con->conectar() == true){
            $sql = "SELECT  a4.numero, a15.pnombre, a15.snombre, a15.papellido, a15.sapellido, a15.direccion, a89.municipio, a89.departmento,
	
            (SELECT detalledefinicion FROM aportes091 WHERE iddetalledef=a16.tipoafiliacion) AS tipo_afiliado, a16.tipoafiliacion AS codTipoAfiliado
            ,a500.agencia
	
            FROM aportes004 a4

                      LEFT JOIN aportes015 a15
                        ON rtrim(ltrim(REPLACE(a4.numero, CHAR(9), ''))) = a15.identificacion AND (a4.idtipodocumento=a15.idtipodocumento OR idtiporadicacion=169)
                      LEFT JOIN aportes015 b15
                        ON rtrim(ltrim(REPLACE(a4.identificacion, CHAR(9), ''))) = b15.identificacion AND a4.idtipodocumento=b15.idtipodocumento
                      INNER JOIN aportes091 a91
                        ON a4.idtiporadicacion=a91.iddetalledef AND a91.iddefinicion = 6
                      INNER JOIN aportes500 a500
                        ON a500.idagencia=a4.idagencia
                      INNER JOIN aportes519 a519
                        ON a519.usuario=a4.usuario
                      LEFT JOIN aportes048 a48 ON a4.nit=a48.nit
                      INNER JOIN aportes016 a16 
                            ON a4.idradicacion=a16.idradicacion 
                      INNER JOIN  aportes089 a89 
                            ON a89.codmunicipio=a15.idciuresidencia AND a89.coddepartamento=a15.iddepresidencia AND a89.codzona=a15.idzona 

            WHERE a15.identificacion='".$numeroDoc."' AND a16.estado='A' AND  a4.idagencia=$agencia  AND (a16.tipoafiliacion=21 OR  a16.tipoafiliacion=4209)
            ORDER BY  a4.idagencia, a4.idtiporadicacion, a4.idradicacion ASC ";
            
            return mssql_query($sql, $this->con->conect);      
        }
    }
    
    /***************************METODO mostrarCartaAceptacionFechas***************************************/
    /***El cual se encarga de realizar la consulta query por medio  de un rango de fechas y agencia*******/
    /***********para obtener los datos para las cartas de aceptacion**************************************/
    function mostrarCartaAceptacionFechas($agencia, $tipoAfi, $fechaIni, $fechaFin){
        if($this->con->conectar() == true){
            $sql = "SELECT  a4.numero, a15.pnombre, a15.snombre, a15.papellido, a15.sapellido, a15.direccion, a89.municipio, a89.departmento,
	
                            (SELECT detalledefinicion FROM aportes091 WHERE iddetalledef=a16.tipoafiliacion) AS tipo_afiliado, a16.tipoafiliacion AS codTipoAfiliado
                            ,a500.agencia

                    FROM aportes004 a4

                              LEFT JOIN aportes015 a15
                                ON rtrim(ltrim(REPLACE(a4.numero, CHAR(9), ''))) = a15.identificacion AND (a4.idtipodocumento=a15.idtipodocumento OR idtiporadicacion=169)
                              LEFT JOIN aportes015 b15
                                ON rtrim(ltrim(REPLACE(a4.identificacion, CHAR(9), ''))) = b15.identificacion AND a4.idtipodocumento=b15.idtipodocumento
                              INNER JOIN aportes091 a91
                                ON a4.idtiporadicacion=a91.iddetalledef AND a91.iddefinicion = 6
                              INNER JOIN aportes500 a500
                                ON a500.idagencia=a4.idagencia
                              INNER JOIN aportes519 a519
                                ON a519.usuario=a4.usuario
                              LEFT JOIN aportes048 a48 ON a4.nit=a48.nit
                              INNER JOIN aportes016 a16 
                                    ON a4.idradicacion=a16.idradicacion 
                              INNER JOIN  aportes089 a89 
                                    ON a89.codmunicipio=a15.idciuresidencia AND a89.coddepartamento=a15.iddepresidencia AND a89.codzona=a15.idzona 

                    WHERE  a16.estado='A' AND  a4.idagencia=$agencia  AND a16.tipoafiliacion=$tipoAfi AND a4.fecharadicacion BETWEEN '".$fechaIni."' AND '".$fechaFin."'
                    ORDER BY  a4.idagencia, a4.idtiporadicacion, a4.idradicacion ASC ";
            
            return mssql_query($sql, $this->con->conect);      
        }
    }
    
    function mostrar_datos_reporte027($fecha_inicio, $fecha_fin, $periodo){
        if($this->con->conectar() == true){
            $sql = "
               SELECT (SELECT detalledefinicion FROM aportes091 WHERE iddetalledef=a15.idtipodocumento)AS Tipo_Documento,
		a15.identificacion, 
		a15.idpersona AS idtrabajador,
		a15.pnombre, a15.snombre, a15.papellido, a15.sapellido,
                (SELECT detalledefinicion FROM aportes091 WHERE iddetalledef=a4.idtiporadicacion) AS Tipo_Radicacion,
		(SELECT detalledefinicion FROM aportes091 WHERE iddetalledef=a16.tipoafiliacion) AS tipo_afiliado,a16.tipoafiliacion, 
		a500.agencia, a15.direccion, a15.telefono, a89.municipio, a89.departmento, a15.email, 
                a601.idciiu AS ciiu,
                (SELECT descripcion FROM aportes079 a79 WHERE a79.idciiu=a601.idciiu) AS actividad,
		a10.valoraporte,
		a16.fechaingreso,
                a10.fechapago,
                a10.periodo
                

                FROM aportes010 a10 

                    RIGHT JOIN aportes015 a15 ON a15.identificacion = a10.identificacion 	
                    INNER JOIN aportes004 a4  ON a4.numero = a15.identificacion AND a4.idtipodocumento=a15.idtipodocumento 
                    INNER JOIN aportes091 a91 ON a4.idtiporadicacion=a91.iddetalledef 
                    INNER JOIN aportes500 a500 ON a500.idagencia=a4.idagencia
                    INNER JOIN aportes016 a16 ON a4.idradicacion=a16.idradicacion 
                    INNER JOIN  aportes089 a89 ON a89.codmunicipio=a15.idciuresidencia AND a89.coddepartamento=a15.iddepresidencia AND a89.codzona=a15.idzona 
                    LEFT JOIN aportes601 a601 ON a15.idpersona=a601.idpersona

                WHERE  a16.estado='A'  AND  a16.tipoafiliacion IN (19,20,21,4209,4520,4521) AND ((a16.fechaingreso BETWEEN '".$fecha_inicio."' AND '".$fecha_fin."') OR (a10.periodo = '".$periodo."'))
                ORDER BY  a4.idagencia, a4.idtiporadicacion, a4.idradicacion ASC";

            return mssql_query($sql, $this->con->conect);      
        }
    }
    function mostrar_datos_reporte028($anno, $anno_inicio, $anno_fin, $periodo_inicio, $periodo_fin, $agencia){
        if($this->con->conectar() == true){
            
                $sql = "
                    SELECT  a15.identificacion, 
                        a15.idpersona,
                        a15.pnombre, a15.snombre, a15.papellido, a15.sapellido,
                        (SELECT detalledefinicion FROM aportes091 WHERE iddetalledef=a16.tipoafiliacion) AS tipo_afiliado,a16.tipoafiliacion,a16.estado,a10.planilla,a10.tipo_planilla,a10.periodo,
                        a500.agencia, a15.direccion, a15.telefono, a89.municipio, a89.departmento, a15.email, 
                        a10.tarifaaporte,a10.valoraporte, a11.valorpagado,a31.mora,
                        a11.fechapago

                    FROM aportes010 a10 

                    INNER JOIN aportes015 a15 ON a15.identificacion = a10.identificacion  AND a15.idpersona = a10.idtrabajador
                    INNER JOIN aportes011 a11 ON a11.planilla = a10.planilla
                    INNER JOIN aportes031 a31 ON a10.planilla=a31.planilla	
                    INNER JOIN aportes004 a4  ON a4.numero = a15.identificacion AND a4.idtipodocumento=a15.idtipodocumento 
                    INNER JOIN aportes091 a91 ON a4.idtiporadicacion=a91.iddetalledef 
                    INNER JOIN aportes500 a500 ON a500.idagencia=a4.idagencia
                    INNER JOIN aportes016 a16 ON a4.idradicacion=a16.idradicacion 
                    INNER JOIN  aportes089 a89 ON a89.codmunicipio=a15.idciuresidencia AND a89.coddepartamento=a15.iddepresidencia AND a89.codzona=a15.idzona ";
                
            if($periodo_inicio != "" && $periodo_fin != ""){
                $sql .= "WHERE a4.idagencia=$agencia AND a16.tipoafiliacion IN (19,20,21,4209,4520,4521) AND ((a10.periodo BETWEEN '".$periodo_inicio."' AND '".$periodo_fin."'))
                         ORDER BY  a4.idagencia, a4.idtiporadicacion, a4.idradicacion ASC"; 
            }elseif($anno != "" ){
                $sql .= "WHERE a4.idagencia=$agencia AND  a16.tipoafiliacion IN (19,20,21,4209,4520,4521) AND ((a10.fechapago BETWEEN '".$anno."/01/01' AND '".$anno."/12/31'))
                         ORDER BY  a4.idagencia, a4.idtiporadicacion, a4.idradicacion ASC";  
            }elseif($anno_inicio != "" && $anno_fin != ""){
                $sql .= "WHERE a4.idagencia=$agencia AND a16.tipoafiliacion IN (19,20,21,4209,4520,4521) AND ((a10.fechapago BETWEEN '".$anno_inicio."/01/01' AND '".$anno_fin."/01/01'))
                         ORDER BY  a4.idagencia, a4.idtiporadicacion, a4.idradicacion ASC";
            }
            return mssql_query($sql, $this->con->conect);
        
                  
        }
    }
}
