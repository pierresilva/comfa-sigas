<?php
/* autor:       orlando puentes
 * fecha:       15/07/2010
 * objetivo:     
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'rsc' . DIRECTORY_SEPARATOR . 'conexion.class.php';

class Embargos{
 //constructor	
var $con;
var $fechaSistema;
function Embargos(){
 		$this->con=new DBManager;
 	}

function mostrar_registro($idt){
		if($this->con->conectar()==true){
			$sql="SELECT  
					idembargo,idtercero,fechaembargo, aportes018.estado,notas,motivoestado,usuarioestado,codigopago, identificacion, papellido, sapellido, pnombre, snombre
					,a91.detalledefinicion AS juzgado 
				FROM aportes018 
					INNER JOIN aportes015 ON aportes018.idtercero = aportes015.idpersona
					LEFT JOIN aportes091 a91 ON a91.iddetalledef=idjuzgado
				WHERE idtrabajador = $idt";
			return mssql_query($sql,$this->con->conect);
		}
	}
	
function contar_embargos($idt){
		if($this->con->conectar()==true){
			$sql="select count(*) as cuenta from aportes018 WHERE idtrabajador = $idt";
			return mssql_query($sql,$this->con->conect);
		}
	}
		
function beneficiarios_embargo($idemb){
	if($this->con->conectar()==true){
		$sql="SELECT DISTINCT aportes022.idbeneficiario,pnombre,snombre,papellido,sapellido,aportes021.fechaafiliacion,aportes021.giro,aportes021.estado,aportes091.detalledefinicion FROM aportes022
INNER JOIN aportes018 ON(aportes018.idembargo=aportes022.idembargo) 
INNER JOIN aportes015 ON aportes022.idbeneficiario=aportes015.idpersona
INNER JOIN aportes021 ON aportes022.idbeneficiario=aportes021.idbeneficiario
INNER JOIN aportes091 ON aportes021.idparentesco=aportes091.iddetalledef
WHERE aportes018.idembargo=$idemb AND aportes021.idtrabajador=aportes018.idtrabajador";
		return mssql_query($sql,$this->con->conect);
	}
}	
 }	
?>