<?php
/* autor:       orlando puentes
 * fecha:       26/07/2010
 * objetivo:    
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'rsc' . DIRECTORY_SEPARATOR . 'conexion.class.php';

class Administradoras {
	//constructor	
	var $con;
	function Administradoras() {
		$this->con = new DBManager ();
	}
	
	function listar_eps() {
		if ($this->con->conectar () == true) {
			$sql = "SELECT idadministradora, codigomin, razonsocial FROM aportes206 WHERE tipoadministradora='EPS'";
			return mssql_query ( $sql, $this->con->conect );
		}
	}
	
	function listar_ccf() {
		if ($this->con->conectar () == true) {
			$sql = "SELECT idadministradora, codigomin, razonsocial FROM aportes206 WHERE tipoadministradora='CCF' order by razonsocial";
			return mssql_query ( $sql, $this->con->conect );
		}
	}
	
	function listar_arp() {
		if ($this->con->conectar () == true) {
			$sql = "SELECT idadministradora, codigomin, razonsocial FROM aportes206 WHERE tipoadministradora='ARP'";
			return mssql_query ( $sql, $this->con->conect );
		}
	}
	
	function listar_afp() {
		if ($this->con->conectar () == true) {
			$sql = "SELECT idadministradora, codigomin, razonsocial FROM aportes206 WHERE tipoadministradora='AFP'";
			return mssql_query ( $sql, $this->con->conect );
		}
	}
	
	function buscar_x_razonsocial($criterio, $tipo = 'CCF') {
		if ($this->con->conectar () == true) {
			$sql = "SELECT idadministradora, codigomin, razonsocial FROM aportes206 WHERE (razonsocial LIKE '%$criterio%' OR codigomin LIKE '%$criterio%') AND tipoadministradora='$tipo'";
			return mssql_query ( $sql, $this->con->conect );
		}
	}
	
	function buscar_x_nit($criterio, $tipo = 'CCF') {
		if ($this->con->conectar () == true) {
			$sql = "SELECT idadministradora, codigomin, razonsocial FROM aportes206 WHERE nit LIKE '%$criterio%'";
			return mssql_query ( $sql, $this->con->conect );
		}
	}
	
	/**
	 * 
	 * Obtiene una administradora seg�n el id (en db) recibido
	 * @param int $id
	 */
	function buscar_x_id($id){
		if ($this->con->conectar () == true) {
			$sql = "SELECT idadministradora, codigomin, razonsocial FROM aportes206 WHERE idadministradora = $id";
			return mssql_query ( $sql, $this->con->conect );
		}
	}

}
?>