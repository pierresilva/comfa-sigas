<?php
/* autor:       orlando puentes
 * fecha:       27/08/2010
 * objetivo:     
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'rsc' . DIRECTORY_SEPARATOR . 'conexion.class.php';

class Pignoracion{
 //constructor	
	var $con;
	function Pignoracion(){
	 		$this->con=new DBManager;
	 	}
	
	function buscar_pignoracion($idt, $estado = null){
	    if($this->con->conectar()==true){
	    	
	    	$arrCondiciones = array();
	    	if($estado != null && ($estado == 'A' || $estado == 'I'))
	    		$arrCondiciones["estado"] = " estado = '$estado' ";
	    	
	    	$strCondiciones = (count($arrCondiciones)>0)? " AND ". implode(" AND ",$arrCondiciones):"";
	    		
	        $sql="SELECT idpignoracion,valorpignorado,idconvenio,fechapignoracion, estado, saldo, observaciones, pagare, facturanumero,anulado, fechaanula,motivo,aportes091.detalledefinicion,fechacancelacion,periodoinicia FROM aportes043 INNER JOIN aportes091 ON aportes043.idconvenio = aportes091.iddetalledef  WHERE idtrabajador = $idt ORDER BY fechapignoracion DESC";
	        return mssql_query($sql,$this->con->conect);
		}
	}
	
	function buscar_pignoracion_por_id($idPignoracion){
		if($this->con->conectar()==true){
			$sql="SELECT idpignoracion,valorpignorado,idconvenio,fechapignoracion, estado, saldo, observaciones, pagare, facturanumero,anulado, fechaanula,motivo,aportes091.detalledefinicion,fechacancelacion,periodoinicia FROM aportes043 LEFT JOIN aportes091 ON aportes043.idconvenio = aportes091.codigo AND aportes091.iddefinicion=25 WHERE aportes043.idpignoracion = $idPignoracion";
			return mssql_query($sql,$this->con->conect);
		}
	}
		
	function buscar_detalle($idpig){
		if($this->con->conectar()==true){
			$sql="SELECT idbeneficiario,periodo, periodoproceso, valor,aportes044.fechasistema, pnombre,snombre,papellido,sapellido FROM aportes044 INNER JOIN aportes015 ON aportes044.idbeneficiario = aportes015.idpersona where idpignoracion=$idpig";
			return mssql_query($sql,$this->con->conect);
		}
	}
	 
	function contar_pignoracion($idt){
	    if($this->con->conectar()==true){
	        $sql="SELECT count(*) as cuenta from aportes043 WHERE idtrabajador = $idt";
	        return mssql_query($sql,$this->con->conect);
		}
	}
	
	function cargar_detalles_pignoracion($idPignoracion){
		if($this->con->conectar()==true){
			$sql="SELECT iddetalle43, aportes044.idpignoracion, idbeneficiario, periodo, periodoproceso, valor, aportes044.anulado, aportes044.fechasistema, aportes043.saldo as saldo_pignoracion, aportes043.valorpignorado FROM aportes044,aportes043 where aportes044.idpignoracion=aportes043.idpignoracion and aportes044.idpignoracion=$idPignoracion";
			return mssql_query($sql,$this->con->conect);
		}
	}
 
}
?>