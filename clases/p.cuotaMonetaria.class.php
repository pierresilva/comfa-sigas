<?php
/* autor:       orlando puentes
 * fecha:       20/08/2010
 * objetivo:     
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'rsc' . DIRECTORY_SEPARATOR . 'conexion.class.php';

class CuotaMonetaria{
 //constructor	
var $con;
function CuotaMonetaria(){
 		$this->con=new DBManager;
 	}
	
function buscar_ultimos_tres($idt){
	if($this->con-->conectar()==true){
		$sql="";
		return mssql_query($sql,$this->con->conect);
		}
	}
	
	/**
	 * Consulta que obtiene los afiliados retirados el 30 � 31 del mes de giro,
	 * que tienen afiliaci�n por subsidio y que no recibieron el giro actual
	 *
	 * @return unknown
	 */
	function obtener_trab_retirados_30o31_mes_de_giro($fechaInicial, $fechaFinal, $idTipoFormSubsidio){
		if($this->con-->conectar()==true){
			$query = "select
					t.idpersona,
					t.identificacion,
					t.papellido,
					t.sapellido,
					t.pnombre,
					t.snombre,
					tipoformulario,
					tray.fecharetiro,
					e.nit,
					e.codigosucursal,
					e.razonsocial
				  from 
					aportes015 t
				  inner join aportes017 tray on t.idpersona=tray.idpersona
				  left join aportes048 e on e.idempresa = tray.idempresa
				  where
					tray.fecharetiro between '$fechaInicial' and '$fechaFinal' and 
					(day(tray.fecharetiro) in (30,31)) and
					tray.tipoformulario=$idTipoFormSubsidio and
					t.idpersona not in (select idtrabajador from aportes014)";	
			return mssql_query($query,$this->con->conect);
		}
		return false;
	}
	
	/**
	 * Obtiene el id(s) de el(los) beneficiario(s) que hayan presentado certificados de supervivencia
	 * durante el per�odo de tiempo especificado.
	 *
	 * @param string $fechaIncial
	 * @param string $fechaFinal
	 * @param int $idBeneficiario
	 * @param array $tiposCertificado arreglo sencillo con los ids de los tipos de certificado
	 * @return ifx result or false
	 */
	function obtener_idbeneficiarios_entregaron_certificados_por_fechas($fechaIncial, $fechaFinal, $tiposCertificado = array()){
		if($this->con->conectar()==true){
			$query="SELECT distinct idbeneficiario FROM aportes026 WHERE fechapresentacion BETWEEN '{$fechaIncial}' AND '{$fechaFinal}' AND idtipocertificado in (". implode(",",$tiposCertificado) .") ";
			return mssql_query($query,$this->con->conect);
		}
		return false;
	}
	
	
	/**
	 * Calcula el n�mero de certificados presentados por un beneficiario con base en el rango de fechas de presentaci�n
	 * y en el tipo de certificado
	 *
	 * @param int $idBeneficiario
	 * @param string $fechaInicial
	 * @param string $fechaFinal
	 * @param int array $arrayIdsTipoCertificado arreglo sencillo con los id de los tipos de certificado
	 * @return int
	 */
	function contar_certificados_beneficiario_por_fecha_presentacion($idBeneficiario,$fechaInicial,$fechaFinal,$arrayIdsTipoCertificado){
		if($this->con->conectar()==true){
			$query="SELECT count(*) as cuenta FROM aportes026 WHERE fechapresentacion BETWEEN $fechaInicial AND $fechaFinal AND idtipocertificado in (". implode(",",$arrayIdsTipoCertificado) .") AND idbeneficiario=$idBeneficiario";
			$result = mssql_query($query,$this->con->conect);
			if($result){
				$arrResult = mssql_fetch_row($result);
				mssql_free_result($result);
				if(is_array($arrResult))
					return intval($arrResult["cuenta"]);
				return 0;
			}
			return false;
		}
		return false;
	}
	
	
	function obtener_ultimo_pergir_por_beneficiario($idBeneficiario){
		if($this->con->conectar()==true){
			$query="SELECT 
					t.idpersona as id_trabajador, 
					max(g.periodo) as periodo
					FROM 
					aportes015 t
					INNER JOIN aportes021 rb ON t.idpersona=rb.idtrabajador
					INNER JOIN aportes009 g ON rb.idbeneficiario=g.idbeneficiario
					WHERE 
					rb.idbeneficiario=$idBeneficiario AND 
					t.estado='A' 
					GROUP BY 
					t.idpersona";
			$result = mssql_query($query,$this->con->conect);
			if($result){
				$arrResultado = mssql_fetch_row($result);
				return $arrResultado;
			}
			return false;
		}
		return false;
	}
	
	
	function obtener_datos_beneficiario_y_trabajador($idBeneficiario,$idTrabajador){
		if($this->con->conectar()==true){
			$query="SELECT
						t.identificacion,
						t.papellido as papellido_tra, 
						t.sapellido as sapellido_tra, 
						t.pnombre as pnombre_tra, 
						t.snombre as snombre_tra,
						rb.idbeneficiario, 
						b.papellido as papellido_b, 
						b.sapellido as sapellido_b, 
						b.pnombre as pnombre_b, 
						b.snombre as snombre_b,
						t.telefono as telefono_tra,
						t.estado as estado_tra,
						e.razonsocial, 
						e.telefono as telefono_emp
					FROM 
						aportes015 t
					INNER JOIN aportes021 rb ON t.idpersona=rb.idtrabajador
					INNER JOIN aportes015 b ON rb.idbeneficiario=b.idpersona
					INNER JOIN aportes016 a ON a.idpersona=t.idpersona
					INNER JOIN aportes048 e ON e.idempresa=a.idempresa
					WHERE 
						t.idpersona = $idTrabajador and
						b.idpersona = $idBeneficiario";
			$resultado = mssql_query($query,$this->con->conect);
			if($resultado){
				$arrayResult = mssql_fetch_row($resultado);
				mssql_free_result($resultado);
				return $arrayResult;
			}
			return false;
		}
		return false;
	}
	
	/**
	 * Obtiene los giros hist�ricos seg�n el per�odo especificado, para realizar 
	 * la auditor�a de giros
	 *
	 * @param string $periodo
	 * @return ifx query result
	 */
	function obtener_giros_periodo($periodo){
		if($this->con->conectar()==true){
			$query="SELECT
					gh.periodo, 
					gh.idempresa,
					e.nit,
					e.codigosucursal, 
					gh.idbeneficiario, 
					rb.idparentesco, 
					tp.detalledefinicion as parentesco,
					b.identificacion as identif_ben,
					gh.capacidadtrabajo,
					b.discapacitado,
					t.identificacion as identif_trab, 
					con.identificacion as identif_cony,
					gh.numerocuotas,
					gh.valor, 
					gh.periodoproceso, 
					gh.programa,
					gh.tipopago,
					gh.codigopago,
					gh.usuario,
					gh.tipogiro, 
					gh.descuento,
					gh.valesc, 
					gh.fechagiro, 
					gh.fechatarjeta, 
					t.fechaafiliacion, 
					t.estado, 
					af.fecharetiro,
					af.salario,
					af.agricola
				  FROM
					aportes009 as gh
				  INNER JOIN aportes015 t ON gh.idtrabajador = t.idpersona
				  INNER JOIN aportes048 e on e.idempresa = gh.idempresa
				  INNER JOIN aportes016 af on af.idpersona=t.idpersona
				  INNER JOIN aportes021 rb on rb.idbeneficiario=gh.idbeneficiario and rb.idtrabajador=gh.idtrabajador
				  INNER JOIN aportes091 tp on tp.iddetalledef=rb.idparentesco
				  INNER JOIN aportes015 b on b.idpersona=gh.idbeneficiario
				  LEFT JOIN aportes015 con on con.idpersona=gh.idconyuge
				  WHERE
					gh.periodoproceso in ('$periodo')";
			$resultado = mssql_query($query,$this->con->conect);
			if($resultado)
				return $resultado;
			return false;
		}
		return false;
	}
	
	/**
	 * Obtiene los giros actuales de personas que no est�n afiliadas
	 *
	 * @return ifx query result
	 */
	function obtener_giros_actuales(){
		if($this->con->conectar()==true){
			$query="SELECT distinct
						ga.idtrabajador,
						t.idtipodocumento,
						t.identificacion,
						t.papellido,
						t.sapellido,
						t.pnombre,
						t.snombre,
						t.estado,
						ga.idempresa,
						e.idempresa,
						e.nit,
						e.codigosucursal
					FROM 
						aportes014 ga 
					inner join aportes015 t on t.idpersona=ga.idtrabajador
					inner join aportes048 e on e.idempresa=ga.idempresa
					where
						ga.idtrabajador not in (select idpersona from aportes016)";
			$resultado = mssql_query($query,$this->con->conect);
			if($resultado)
				return $resultado;
			return false;
		}
		return false;
	}
	
	/**
	 * Obtiene los id de los beneficiarios activos que tienen certificados del a�o anterior pero
	 * que no tienen del a�o actual
	 *
	 * @param string $fechaInicialAnoAnt
	 * @param string $fechaFinalAnoAnt
	 * @param string $fechaInicialAnoAct
	 * @param string $fechaFinalAnoAct
	 * @param array $tiposCertificados
	 * @return ifx resultset
	 */
	function obtener_beneficiarios_sin_certificados_respecto_agno_anterior($fechaInicialAnoAnt,$fechaFinalAnoAnt,$fechaInicialAnoAct,$fechaFinalAnoAct, $tiposCertificados = array()){
		if($this->con->conectar()==true){
			$query="SELECT 
						idpersona
					FROM 
						aportes015
					WHERE
						estado='A' AND
						idpersona in (SELECT idbeneficiario FROM aportes026 WHERE fechapresentacion BETWEEN '{$fechaInicialAnoAnt}' AND '{$fechaFinalAnoAnt}' AND idtipocertificado in (". implode(",",$tiposCertificados) .")) 
					AND idpersona not in (SELECT idbeneficiario FROM aportes026 WHERE fechapresentacion BETWEEN '{$fechaInicialAnoAct}' AND '{$fechaFinalAnoAct}' AND idtipocertificado in (". implode(",",$tiposCertificados) ."))";
			$resultado = mssql_query($query,$this->con->conect);
			if($resultado)
				return $resultado;
			return false;
		}
		return false;
	}
	
	
	function obtener_beneficiarios_poredad_sin_certificado($edad,$fechaTopePresentacion){
		if($this->con->conectar()==true){
			$query="select 
					b.idpersona as id_beneficiario,
					t.idpersona as id_trabajador,
					t.identificacion as documento_trab,
					t.papellido as papellido_t,
					t.sapellido as sapellido_t,
					t.pnombre as pnombre_t,
					t.snombre as snombre_t,
					af.salario,
					b.idtipodocumento,
					td.codigo as tipo_doc_ben,
					b.identificacion as documento_ben,
					b.papellido as papellido_b,
					b.sapellido as sapellido_b,
					b.pnombre as pnombre_b,
					b.snombre as snombre_b,
					parent.concepto as parentesco,
					b.sexo as sexo_b,
					t.direccion,
					t.telefono,
					t.idbarrio,
					barr.detalledefinicion as barrio,
					b.discapacitado,
					b.fechanacimiento,
					year(getdate()-b.fechanacimiento -1)-1900 as edad
					from
					aportes015 b
					inner join aportes021 rb on rb.idbeneficiario=b.idpersona
					inner join aportes091 parent on parent.iddetalledef=rb.idparentesco and parent.iddefinicion=7
					inner join aportes016 af on af.idpersona = rb.idtrabajador
					inner join aportes015 t on t.idpersona=rb.idtrabajador
					inner join aportes091 td on td.iddetalledef=b.idtipodocumento and td.iddefinicion=1
					left join aportes091 barr on barr.iddetalledef=t.idbarrio and barr.iddefinicion=19
					where
					b.estado='A' and
					(year(getdate()-b.fechanacimiento -1)-1900) >= $edad and (year(getdate()-b.fechanacimiento -1)-1900) < ($edad+1) and
					b.idpersona not in (select idbeneficiario from aportes026 where fechapresentacion > $fechaTopePresentacion)
					order by t.identificacion,af.salario";
			$resultado = mssql_query($query,$this->con->conect);
			if($resultado)
				return $resultado;
			return false;
		}
		return false;
	}
	
	function Select_Cuota_Monetaria($idemp,$per){
		if($this->con->conectar()==true){
			$query="SELECT a15.identificacion, a15.pnombre pnombreA, a15.sapellido snombreA, a15.papellido papellidoA, a15.sapellido sapellidoA,
						cm.idbeneficiario, b15.pnombre pnombreB, b15.sapellido snombreB, b15.papellido papellidoB, b15.sapellido sapellidoB, cm.valor
					FROM aportes009 cm
					  INNER JOIN aportes015 a15
					    ON a15.idpersona=cm.idtrabajador
					  INNER JOIN aportes015 b15
					    ON b15.idpersona=cm.idbeneficiario
					WHERE cm.idempresa=$idemp AND cm.periodo='$per'
					UNION
					SELECT a15.identificacion, a15.pnombre pnombreA, a15.sapellido snombreA, a15.papellido papellidoA, a15.sapellido sapellidoA,
						cm.idbeneficiario, b15.pnombre pnombreB, b15.sapellido snombreB, b15.papellido papellidoB, b15.sapellido sapellidoB, cm.valor
					FROM aportes014 cm
					  INNER JOIN aportes015 a15
					    ON a15.idpersona=cm.idtrabajador
					  INNER JOIN aportes015 b15
					    ON b15.idpersona=cm.idbeneficiario
					WHERE cm.idempresa=$idemp AND cm.periodo='$per'";
			$resultado = mssql_query($query,$this->con->conect);
			if($resultado)
				return $resultado;
			return false;
		}
		return false;
	}
}
?>