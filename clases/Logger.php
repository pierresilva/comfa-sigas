<?php
/**
 * @author Jose Luis Rojas
 * @since 01 octubre 2010
 * @version 1.0.0
 */
date_default_timezone_set('America/Bogota');
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once 'Consultor.php';

class Logger{

	private $tipo = null;
	private $confArchivosMultiple = array();
	private $confArchivo = array('estado'=>'I');
	private $confBD = array('estado'=>'I');
	private $consultar;

	/**
	 * Recibe los tipos de log que se pueden realizar
	 * e inicializa la clase, los tipos de mensaje son
	 * 1.Log a archivo
	 * 2.Log a BD
	 * 3.log a archivo y base de datos
	 *
	 *
	 * @param $tipo
	 */
	public function __construct($tipo){
		if(is_numeric($tipo) && $tipo >= 1 && $tipo <= 3){
			$this->tipo = $tipo;
		}else{
			return false;
		}
	}

	/**
	 * Recibe array con la configuracion de la ruta y nombre del archivo a crear
	 * el array debe ser de la forma
	 *
	 * @param string $ruta Ruta donde se ubicara el archivo
	 * @param string $nombre Nombre del archivo
	 */
	public function confArchivo($ruta, $nombre){
		if($this->tipo == 1 || $this->tipo == 3){
			//@TODO eliminar el ultimo slash si viene en la ruta
			//$ruta = preg_match('/[\\/]{1}$/','', $ruta);
			$this->confArchivo['ruta'] = $ruta . DIRECTORY_SEPARATOR . date('Y/m/d');
			$this->confArchivo['nombre'] = $nombre;
			$this->confArchivo['rutaCompleta'] = $this->confArchivo['ruta'] . DIRECTORY_SEPARATOR . $nombre;

			$bndDir = (is_dir($this->confArchivo['ruta']))?true:@mkdir($this->confArchivo['ruta'], 0777, true);
			if($bndDir){
				$bndArc = (is_file($this->confArchivo['rutaCompleta']))?true:@touch($this->confArchivo['rutaCompleta']);
				if($bndArc){
					$this->confArchivo['estado'] = 'A';
				}else{
					$this->confArchivo['estado'] = 'I';
				}
			}
		}
	}

	public function confArchivosMultiples($conf){
		if($this->tipo == 1 || $this->tipo == 3){
			foreach($conf as $nom=>$val){
				$this->confArchivosMultiple[$nom]['ruta'] = $val[0] . DIRECTORY_SEPARATOR . date('Y/m/d');
				$this->confArchivosMultiple[$nom]['nombre'] = $val[1];
				$this->confArchivosMultiple[$nom]['rutaCompleta'] = $this->confArchivosMultiple[$nom]['ruta'] . DIRECTORY_SEPARATOR . $val[1];

				$bndDir = (is_dir($this->confArchivosMultiple[$nom]['ruta']))?true:@mkdir($this->confArchivosMultiple[$nom]['ruta'], 0777, true);
				if($bndDir){
					$bndArc = (is_file($this->confArchivosMultiple[$nom]['rutaCompleta']))?true:@touch($this->confArchivosMultiple[$nom]['rutaCompleta']);
					if($bndArc){
						$this->confArchivosMultiple[$nom]['estado'] = 'A';
					}else{
						$this->confArchivosMultiple[$nom]['estado'] = 'I';
					}
				}
			}
		}
	}

	public function confBD($conf){
		if($this->tipo == 2 || $this->tipo == 3){
			$this->confBD['tabla'] = $conf['tabla'];
			$this->confBD['campos'] = $conf['campos'];
			$this->confBD['estado'] = 'A';
			$this->confBD['sql'] = "INSERT INTO " . $conf['tabla'] . "(" . (implode(",", $conf['campos'])) . ") VALUES(?)";
			$this->consultar = new Consultor();
		}
	}

	/**
	 * Graba mensajes en el archivo configurado
	 *
	 * @param string $texto Texto que se grabara en el archivo
	 */
	public function logSimple($texto){
		if($this->confArchivo['estado'] == 'A'){
			$fh = fopen($this->confArchivo['rutaCompleta'], 'a');
			if($fh){
				$fechaHora = date('m/d/Y h:i:s');
				$ln = $fechaHora . ';' . $texto."\r\n";
				fwrite($fh, $ln);
				fclose($fh);
			}
		}
	}

	public function logSimpleMultiple($nom, $texto){
		if(array_key_exists($nom, $this->confArchivosMultiple)){
			if($this->confArchivosMultiple[$nom]['estado'] == 'A'){
				$fh = fopen($this->confArchivosMultiple[$nom]['rutaCompleta'], 'a');
				if($fh){
					$fechaHora = date('m/d/Y h:i:s');
					$ln = $fechaHora . ';' . $texto."\r\n";
					fwrite($fh, $ln);
					fclose($fh);
				}
			}
		}else{
			return false;
		}
	}

	/**
	 * Graba en los campos configurados
	 *
	 * @param array $campos con los campos en el mismo orden que se dio en la configuracion
	 */
	public function logSimpleBD($campos){
		if($this->confBD['estado'] == 'A'){
			$sql = $this->confBD['sql'];
			$values = "'" . implode("','", $campos) . "'";
			$sql = str_replace('?', $values, $sql);
			$r = $this->consultar->noRetorno($sql);
		}
	}

	public function logCompuesto($texto, $campos){
		$this->logSimple($texto);
		$this->logSimpleBD($campos);
	}
}

/*$log = new Logger(3);
$log->confArchivo('/var/www/logs', 'prueba3');
$arr = array('tabla'=>'aportes015', 'campos'=>array('idtipodocumentos', 'identificacion'));
$log->configuracion($arr);
$arr = array('E_GEN'=>array('/var/www/logs', 'prueba'), 'W_GEN'=>array('/var/www/logs', 'prueba2'));
$log->confArchivosMultiples($arr);*/
?>