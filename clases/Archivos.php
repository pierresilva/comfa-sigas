<?php
/* autor:       orlando puentes
 * fecha:       14/10/2010
 * objetivo:   
 */
 date_default_timezone_set('America/Bogota');
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'conexion.class.php';

/**
 * Manejo de archivos del directorio PU
 * 
 * USO EXCLUSIVO DEL PROCESO DE NOMINA
 * 
 * @author Jose Luis rojas
 * @version 1.0.0
 */
class Archivos{
	
	var $con = null;
	private $tipProceso = null;
	
	function __construct($pro){
		$this->con = new DBManager();
		$this->con->conectar();
		$this->tipProceso = $pro;
	}
	
	/**
	 * Copia los archivos de la carpeta publica a la carpeta donde se van a procesar los archivos
	 * 
	 * @param String $ruta Direccion del directorio a leer
	 * @param String $rutaD Direccion donde se copiaran los archivos
	 * @return Boolean $dirArray Falso en caso de error al copiar, Verdadero si es correcta la copia
	 */
	function copiarDirXProcesar($ruta, $rutaD){
		$ds = DIRECTORY_SEPARATOR;
		$dirPath = trim($ruta);
		if(is_dir($ruta)){
			//Si es directorio se leen los archivos y se guardan en un arra
			$dirArray = array();
			$dir = dir($ruta);
			$i = 0;
			$rutaD .= $ds . date('Y/m/d');
			$bndD = false;
			if(! is_dir($rutaD)){
				if(mkdir($rutaD, 0777, true)){
					$bndD = true;
				}
			}else{
				$bndD = true;
			}
			$t = chmod($rutaD, 0777);
			while(($archv = $dir->read()) !== false){
				if($archv != '.' && $archv != '..'){
					$archv = trim($archv);
					$filePath = $ruta . DIRECTORY_SEPARATOR . $archv;
					chmod($filePath, 0777);
					$fileDest = $rutaD . DIRECTORY_SEPARATOR . $archv;
					if(! is_dir($filePath) && $bndD == true){
						if(rename($filePath, $fileDest)){
							$dirArray[$i]['nombre'] = $archv;
							$dirArray[$i]['tamano'] = round((filesize($fileDest) / 1024), 1);
							$dirArray[$i]['tipoarchivo'] = $this->identificaTipoArchivo($archv);
							$dirArray[$i]['cargue'] = strftime('%a %d %b %Y, %r %p', filectime($fileDest));
							$dirArray[$i]['cargueIfx'] = date('m/d/Y', filectime($fileDest));
							$dirArray[$i]['link'] = 'http://10.10.1.121/sigas/aportes/nomina/planos_asocajas/' . $archv;
							$dirArray[$i]['fileServer'] = $fileDest;
							$dirArray[$i]['dirServer'] = $rutaD;
							$i++;
						}
					}
				}
			}
			if($this->grabarArchivosDB($dirArray)){
				return true;
			}else{
				return false;
			}
		}else{
			//No es un directorio
			return false;
		}
	}
	
	/**
	 * Lista los archivos que se encuentran listos para procesar
	 */
	public function listarArchivosXProcesar(){
		$dirArray = array();
		$sql = "SELECT ruta, idarchivopu, nombrearchivo, tamano, fechacargue  FROM aportes030 WHERE procesado='N' AND tipoproceso='$this->tipProceso' ORDER BY tipoarchivo ASC";
		$r = mssql_query($sql, $this->con->conect);
		$i = 0;
		while($rArc = mssql_fetch_array($r)){
			//@todo Verificar que el archivo exista en el sistema operativo, tal como lo dice la bd(aportes030)
			$rArc = array_map('trim', $rArc);
			$rArc = array_map('utf8_encode', $rArc);
			if(is_file($rArc['ruta'] . DIRECTORY_SEPARATOR . $rArc['nombrearchivo'])){
				$dirArray[$i]['idarchivo'] = $rArc['idarchivopu'];
				$dirArray[$i]['nombre'] = $rArc['nombrearchivo'];
				$dirArray[$i]['cargue'] = $rArc['fechacargue'];
				$dirArray[$i]['tamano'] = (string)round($rArc['tamano'], 1);
				$i++;
			}else{
				//@todo tratamiento a los archivos que no existen
			}
		}
		mssql_free_result($r);
		
		return $dirArray;
	}
	
	/**
	 * Graba en la base de datos la informacion de los archivos de Planilla unica por procesar
	 * 
	 * 
	 * @param Array $arrayArchv Array con la informacion de los archivos para guardar en BD
	 * @return boolean Retorna Falso en caso de error, Verdadero si se realiza de forma correcta la grabacion
	 */
	private function grabarArchivosDB($arrayArchv = array()){
		if(count($arrayArchv) > 0){
			$i = 0;
			$tot = count($arrayArchv);
			
			foreach($arrayArchv as $ind=>$dat){
				$sql = "INSERT INTO aportes030(nombrearchivo, ruta, tipoarchivo, tamano, fechacargue, tipoproceso) VALUES('$dat[nombre]', '$dat[dirServer]', '$dat[tipoarchivo]', $dat[tamano], '$dat[cargueIfx]', '$this->tipProceso')";
				$r = mssql_query($sql, $this->con->conect);
				if($r){
					//Se grabaron los datos en la BD
					$i++;
				}
			}
			
			if($i > 0){
				return true;
			}
		}else{
			//No hay datos en el array de archivos
			return false;
		}
	}
	
	public function leerArchivo($idArchivo){
		$sql = "SELECT nombrearchivo, ruta FROM aportes030 WHERE idarchivopu=$idArchivo AND tipoproceso='$this->tipProceso'";
		$r = mssql_query($sql, $this->con->conect);
		if(is_resource($r)){
			$arc = mssql_fetch_array($r);
			$arc = array_map('trim', $arc);
			$archivo = $arc['ruta'] . DIRECTORY_SEPARATOR . $arc['nombrearchivo'];
			if(is_file($archivo)){
				$fp = fopen($archivo, 'r');
				$html = '';
				while(! feof($fp)){
					$ln = fgets($fp);
					$ln = preg_replace('/[\s]/','&nbsp;',$ln);
					$html .= utf8_encode($ln) . '<br/>';
				}
				fclose($fp);
				//$html = file_get_contents($archivo);
				return $html;
			}
		}else{
			return false;
		}
	}
	
	private function identificaTipoArchivo($nombreArchivo){
		$tipo = false;
		$posEnct = stripos($nombreArchivo, "_A");
		if($posEnct !== FALSE){
			//Archivo tipo A
			$tipo = 'A';
		}
		
		$posEnct = stripos($nombreArchivo, "_I");
		if($posEnct !== FALSE){
			//Archivo tipo A
			$tipo = 'I';
		}
		
		return $tipo;
	}
}

?>