<?php
/* autor:       orlando puentes
 * fecha:       24/09/2010
 * objetivo:    
 */
 ini_set("display_errors","1");
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once 'clases' . DIRECTORY_SEPARATOR . 'dDefinicion.class.php';
include_once 'clases' . DIRECTORY_SEPARATOR . 'dCargos.class.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';

$objClase=new dDefinicion;
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	echo "<script>alert('Hubo un error');</script>";
	exit();
}
$consulta = $objClase->mostrar_definiciones();

$sql="Select * from aportes514";
$rs=$db->querySimple($sql);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <meta http-equiv="Content-Script-Type" content="text/javascript; charset=iso-8859-1">
		<title>Cargos</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<link href="../css/Estilos.css" rel="stylesheet" type="text/css" />
        <link href="../css/formularios/base/ui.all.css" rel="stylesheet" type="text/css" />
        <link href="../css/marco.css" rel="stylesheet" type="text/css" />
		<script language="javascript" src="../js/ui/jquery-1.4.2.js"></script>
		<script language="javascript" src="../js/comunes.js"></script>
        <script language="javascript" src="../js/ui/jq-ui/jquery.ui.core.js"></script>
        <script language="javascript" src="../js/ui/jq-ui/jquery.ui.datepicker.js"></script>
		<script language="javascript" src="js/dcargos.js"></script>
        
        <script type="text/javascript">
shortcut.add("Shift+F",function() {
		                                var URL=src();
		var url=URL+"aportes/trabajadores/consultaTrabajador.php";
    	window.open(url,"_blank");
    },{
	'propagate' : true,
	'target' : document 
        });        
</script>

	</head>
	<body >
		<form name="forma">
			<br/>
			<center>
				<table width="70%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="13" height="29" class="arriba_iz">&nbsp;</td>
						<td class="arriba_ce">
                        <span class="letrablanca">::&nbsp;Administracion - Detalle Cargos &nbsp;::</span></td>
						<td width="13" class="arriba_de" align="right">&nbsp;</td>
					</tr>
	<tr>
    <td class="cuerpo_iz">&nbsp;</td>
    <td class="cuerpo_ce">
                <img src="../imagenes/tabla/spacer.gif" width="1" height="1">
                <img src="../imagenes/tabla/spacer.gif" width="1" height="1">
                <img src="../imagenes/spacer.gif" width="1" height="1">
                <img src="../imagenes/menu/nuevo.png" title="Nuevo" width="16" height="16" style="cursor:pointer" onClick="nuevoR();">
                <img src="../imagenes/menu/spacer.gif" width="1" height="1">
                <img src="../imagenes/menu/grabar.png" title="Guardar" width="16" height="16" onClick="validarCampos(1)" style="cursor:pointer" id= "bGuardar">
                <img src="../imagenes/menu/spacer.gif" width="1" height="1">
                <img src="../imagenes/menu/modificar.png" title="Actualizar" width="16" height="16" onClick="validarCampos(2)" style="cursor:pointer">
                <img src="../imagenes/spacer.gif" width="1" height="1">
                <img src="../imagenes/menu/ico_error.png" width="16" height="16" border="0" title="Eliminar registro" style="cursor:pointer" onClick="eliminarDato()" >
                <img src="../imagenes/menu/spacer.gif" width="1" height="1">
                <img src="../imagenes/menu/imprimir.png" width="16" height="16" style="cursor:pointer" title="Imprimir" onClick="imprimir();" >
                <img src="../imagenes/spacer.gif" width="1" height="1">
                <img src="../imagenes/menu/refrescar.png" width="16" height="16" style="cursor:pointer" title="Limpiar campos" onClick="limpiarCampos(); document.forms[0].elements[1].focus();">
                <img src="../imagenes/spacer.gif" width="1" height="1">
                <img src="../imagenes/menu/buscar.png" title="Buscar" width="16" height="16" style="cursor:pointer" onClick="consultaDatos()">
                <img src="../imagenes/spacer.gif" width="1" height="1">
                <a href="../help/seguridad/manual ayuda areas.html" target="_blank" onClick="window.open(this.href, this.target, 'width=500,height=550,titlebar=0, resizable=no, scrollbars=yes'); return false;" >
                    <img src="../imagenes/menu/informacion.png" width="16" height="16" style="border:none" title="Manual" />
                </a>
                <img src="../imagenes/spacer.gif" width="1" height="1">
                <img src="../imagenes/menu/primero.png" width="16" height="16" border="0" title="Primero" style="cursor:pointer" onClick="mostrar(1);" />
                <img src="../imagenes/spacer.gif" width="1" height="1">
                <img src="../imagenes/menu/siguiente.png" width="16" height="16" border="0" title="Siguiente" style="cursor:pointer" onClick="mostrar(2);" />
                <img src="../imagenes/menu/spacer.gif" width="1" height="1">
                <img src="../imagenes/menu/anterior.png" width="16" height="16" border="0" title="Anterior" style="cursor:pointer" onClick="mostrar(3);" />
                <img src="../imagenes/spacer.gif" width="1" height="1">
                <img src="../imagenes/menu/ultimo.png" width="16" height="16" border="0" title="Ultimo" style="cursor:pointer" onClick="mostrar(4);" />
                <img src="../imagenes/menu/spacer.gif" width="1" height="1">
						</td>
						 <td class="cuerpo_de">&nbsp;</td>
					</tr>
					<tr>
						<td class="cuerpo_iz">&nbsp;</td>
						<td class="cuerpo_ce"><div id="resultado" style="color:#FF0000"></div></td>
						<td class="cuerpo_de">&nbsp;</td>
					</tr>
					<tr>
						<td class="cuerpo_iz">&nbsp;</td>
						<td class="cuerpo_ce">
							<table width="95%" border="0" cellspacing="0" class="tablero">
								<tr>
									<td width="25%">Id Cargo</td>
									<td><input name=txtId class=boxfecha id="txtId" readonly></td>
								</tr>
								<tr>
									<td>Cargo</td>
									<td>
                                    	<select name="txtIdProceso" class="boxlargo" id="txtIdProceso">
											<option value="0">[Seleccione]</option>
											<?php 
											while ($row=$rs->fetch()){
												echo "<option value=".$row['idproceso'].">".$row['proceso']."</option>";
											}
											?>
										</select>
                                   		<img src="../imagenes/menu/obligado.png" width="12" height="12">
                                    </td>
								</tr>
                                <tr>
                                    <td align="left">Codigo Detalle Cargo</td>
                                    <td  align="left"><label>
                                        <input name="txtCodDetalle" type="text" class="boxfecha" id="txtCodDetalle" />
                                        </label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">Detalle Cargo</td>
                                    <td  align="left"><label>
                                        <input name="txtDetCargo" type="text" class="boxlargo" id="txtDetCargo" >
                                        </label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">Concepto</td>
                                    <td align="left"><input name="txtConCargo" type="text" class="boxlargo" id="txtConCargo" ></td>
                                </tr>
                                <tr>
                                    <td align="left">Fecha Creaci�n</td>
                                    <td  align="left">
                                    	<input name="txtFecha" class="boxfecha" id="txtFecha" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">Usuario</td>
                                    <td align="left">
                                    	<input name="txtUsuario" class="boxlargo" id="txtUsuario">
                                    </td>
                                </tr>
                            </table>
                        </td>
						<td class="cuerpo_de">&nbsp;</td>
					</tr>
					<tr>
                    <td class="abajo_iz" >&nbsp;</td>
                    <td class="abajo_ce" ></td>
                    <td class="abajo_de" >&nbsp;</td>
					</tr>
				</table>
			</center>
			<input type="hidden" value="<?php echo $_SESSION["USUARIO"];?>" name="usuario" id="usuario" />
			<input type="hidden" name="idr" id="idr" />
		</form>
	</body>
	<script>
        mostrar(1);
	</script>
</html>