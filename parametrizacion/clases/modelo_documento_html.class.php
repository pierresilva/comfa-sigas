<?php 
date_default_timezone_set("America/Bogota");

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'utilitario.class.php';

class ModeloDocumentoHtml{
	
	/************************************
	 *  ATRIBUTOS ENTIDADES			    *
	***********************************/
	private static $conPDO = null;

	function __construct(){
 				
		try{
			self::$conPDO = IFXDbManejador::conectarDB();
			if( self::$conPDO->conexionID==null ){
				throw new Exception(self::$conPDO->error);
			}
		}catch(Exception $e){
			echo $e->getMessage();
			exit();
		}
 	}

 	function inicioTransaccion(){
 		self::$conPDO->inicioTransaccion();
 	}
 	function cancelarTransaccion(){
 		self::$conPDO->cancelarTransaccion();
 	}
 	function confirmarTransaccion(){
 		self::$conPDO->confirmarTransaccion();
 	}
 	
 	function guardar_modelo_documento_html( $datos, $usuario ){
 		$sql = "INSERT INTO dbo.aportes321(modelo_documento,nombr_model_docum,contenido_html,estado,usuario_creacion) 	
 					VALUES ( '{$datos["modelo_documento"]}', '{$datos["nombr_model_docum"]}', :contenido_html, '{$datos["estado"]}', '$usuario')";
 	
 		$statement = self::$conPDO->conexionID->prepare($sql);
 		$statement->bindValue(":contenido_html", $datos["contenido_html"],  PDO::PARAM_STR);
 		$guardada = $statement->execute();
 	
 		$id = 0;
 		if($guardada!=false){
 			$id = self::$conPDO->conexionID->lastInsertId("aportes321");
		}
 	
 		return $id;
 	}
 	
 	function actualizar_modelo_documento_html( $datos, $usuario ){
 	
 		$sql = "UPDATE dbo.aportes321
				SET modelo_documento = '{$datos["modelo_documento"]}'
					, nombr_model_docum = '{$datos["nombr_model_docum"]}'
					, contenido_html = :contenido_html
					, estado = '{$datos["estado"]}'
					, usuario_modificacion = '$usuario'
					, fecha_modificacion = getdate()					
 				WHERE id_modelo_documento = {$datos["id_modelo_documento"]}";
 	
 		$statement = self::$conPDO->conexionID->prepare($sql);
 		$statement->bindValue(":contenido_html", $datos["contenido_html"],  PDO::PARAM_STR);
 		$guardada = $statement->execute();
 		return $guardada == false ? 0 : 1;
 	}
 	
 	public function fetch_modelo_documento($arrAtributoValor){
 		$attrEntidad = array(
 				array("nombre"=>"id_modelo_documento","tipo"=>"NUMBER","entidad"=>"a321")
 				,array("nombre"=>"modelo_documento","tipo"=>"TEXT","entidad"=>"a321")
 				,array("nombre"=>"estado","tipo"=>"TEXT","entidad"=>"a321"));
 		
 		$filtroSql = Utilitario::obtenerFiltro($attrEntidad,$arrAtributoValor);
 		
 		$sql = "SELECT a321.id_modelo_documento, a321.modelo_documento, a321.nombr_model_docum, a321.contenido_html, a321.estado, a321.usuario_creacion, a321.usuario_modificacion, a321.fecha_creacion, a321.fecha_modificacion 
				FROM aportes321 a321
 				$filtroSql";
 		
 		return Utilitario::fetchConsulta($sql, self::$conPDO);
 	}
}
?>