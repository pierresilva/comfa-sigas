<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'rsc' . DIRECTORY_SEPARATOR . 'conexion.class.php';
include_once $_SESSION['RAIZ'] . DIRECTORY_SEPARATOR . '/rsc/pdo/IFXDbManejador.php';
 //include_once("../rsc/conexion.class.php");

class Departamentos{
	private $db;
	var $con;
	var $fechaSistema;

/** CONSTRUCTOR **/
function __construct(){
	$this->con=new DBManager;
	$this->db = IFXDbManejador::conectarDB();
	if($this->db->conexionID==null){
		$cadena = $this->db->error;
		exit();
	}
}

function insertar($campos){
		if($this->con->conectar()==true){
			$fechaSistema=date("Y/m/d");
			//print_r($campos);

			$Dept="";
			if($campos[2]=="")
			{
				
				$sql1="SELECT TOP 1 departmento FROM aportes089 WHERE coddepartamento='".$campos[1]."'";
				$rs1 = $this->db->querySimple($sql1);
					
				$departamento=$rs1->fetchObject()->departmento;
				$Dept=$departamento;
			}
			else
			{
				$Dept=$campos[2];
			}
			
			
			$sql="INSERT INTO aportes089 (coddepartamento, codmunicipio, codzona, departmento, municipio, zona, idpais, fechasistema, usuario) 
			VALUES ('".$campos[1]."','".$campos[4]."','".$campos[6]."','".$Dept."','".$campos[3]."','".$campos[5]."',".$campos[0].",'".$fechaSistema."','".$campos[8]."')";
			$rs=$this->db->queryInsert($sql,'aportes089');
			return $rs===null?0:1;
		}
	}

function mostrar_departamentos($nom){
		if($this->con->conectar()==true){
			$sql="SELECT * FROM aportes089 WHERE departmento LIKE '%".$nom."%'";
			return mssql_query($sql,$this->con->conect);
		}
	}
	
function mostrar_municipios($nom){
		if($this->con->conectar()==true){
			$sql="SELECT * FROM aportes089 WHERE municipio LIKE '%".$nom."%'";
			return mssql_query($sql,$this->con->conect);
		}
	}
	
function mostrar_zonas($nom){
		if($this->con->conectar()==true){
			$sql="SELECT * FROM aportes089 WHERE zona LIKE '%".$nom."%'";
			return mssql_query($sql,$this->con->conect);
		}
	}

function mostrar_registro($nom){
		if($this->con->conectar()==true){
			$sql="SELECT * FROM aportes089 WHERE codzona='".$nom."'";
			return mssql_query($sql,$this->con->conect);
		}
	}

function actualizar($campos){
		if($this->con->conectar()==true){
			//print_r($campos);
			
			$Dept="";
			if($campos[2]=="")
			{
			
				$sql1="SELECT TOP 1 departmento FROM aportes089 WHERE coddepartamento='".$campos[1]."'";
				$rs1 = $this->db->querySimple($sql1);
					
				$departamento=$rs1->fetchObject()->departmento;
				$Dept=$departamento;
			}
			else
			{
				$Dept=$campos[2];
			}
			
			$sql="UPDATE aportes089 SET coddepartamento = '".$campos[1]."', codmunicipio = '".$campos[4]."', codzona = '".$campos[6]."', departmento = '".$Dept."'
			,municipio = '".$campos[3]."', zona = '".$campos[5]."', idpais = ".$campos[0].", fechasistema = cast (getdate() as date)
			,usuario = '".$campos[7]."' WHERE codzona = '". $campos[6]."'";	
			return mssql_query($sql,$this->con->conect);
		}
	}	
/*
function eliminar($id){
	if($this->con->conectar()==true){
		$sql="DELETE FROM aportes087 WHERE idbarrio=".$id;
		return mssql_query($sql,$this->con->conect);
		}
	} 
	
function mostrar_datos(){
		if($this->con->conectar()==true){
			return mssql_query("SELECT * FROM aportes087 order by idbarrio",$this->con->conect);
		}
	}*/	
}	
?>