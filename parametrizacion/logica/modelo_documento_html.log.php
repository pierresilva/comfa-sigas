<?php 
/**
 * Script de la logica del formulario definicion_novedad
 *
 * @author Oscar
 * @version 0
 */

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz. DIRECTORY_SEPARATOR . 'parametrizacion' . DIRECTORY_SEPARATOR .'clases' . DIRECTORY_SEPARATOR . 'modelo_documento_html.class.php';

/**
 * Contiene los datos de retorno al formulario
 * @var array
 */
$arrMensaje = array('error'=>0,'data'=>null);

/*Verificar si accion existe*/
$accion = isset($_POST['accion'])?$_POST['accion']:'';

$usuario = $_SESSION["USUARIO"];

switch($accion){
    case 'I':
    	
        $datos = $_POST["datos"];
        	
        $objModeloDocumentoHtml = new ModeloDocumentoHtml();
        
        $resultado = $objModeloDocumentoHtml->guardar_modelo_documento_html($datos,$usuario);

        $arrMensaje["error"] = 0;
            
        if($resultado == 0){
        	$arrMensaje["error"] = 1;
       	}

        break;
    case 'U':

    	$datos = $_POST["datos"];

    	//var_dump(($datos["contenido_html"]));exit();
    	
    	$objModeloDocumentoHtml = new ModeloDocumentoHtml();
    	
    	$resultado = $objModeloDocumentoHtml->actualizar_modelo_documento_html($datos,$usuario);
    	
    	$arrMensaje["error"] = 0;
    	
    	if($resultado == 0){
    		$arrMensaje["error"] = 1;
    	}
    	
        break;
    case 'S':

    		$objDatosFiltro = isset($_POST["objDatosFiltro"])?$_POST["objDatosFiltro"]:array();
            
			$objModeloDocumentoHtml = new ModeloDocumentoHtml();
			$resultado=$objModeloDocumentoHtml->fetch_modelo_documento( $objDatosFiltro );
            
            /*Verificar si existen datos*/
            if(count($resultado)>0){
                $arrMensaje["error"] = 0;
                $arrMensaje["data"] = $resultado;
            }
        break;
}

echo ($accion != '') ? json_encode($arrMensaje) : "";
?>