// JavaScript Document
// autor Orlando Puentes A
// fecha marzo 14 de 2010
// objeto administración de los eventos del formulario de definiciones.php
// funcion para obtener cookie en javascript
	function getCookie(c_name)
	{
		var i,x,y,ARRcookies=document.cookie.split(";");
		for (i=0;i<ARRcookies.length;i++)
		{
  			x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
  			y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
  			x=x.replace(/^\s+|\s+$/g,"");
  			if (x==c_name)
    			{
    				return unescape(y);
    			}// fin if
		}// fin for
  	}// fin getCookie
//funcion para obtener cookie en javascript

nuevoD=0;
$(function(){
});

$(function(){
	$( "#tdefinicion" ).autocomplete({
	    source: function( request, response ) {
	    	var nomdetalledef=$("#tdefinicion").val();
	        $.ajax({
	            url: "dDefinicionesAut.php",
	            data: {v0:nomdetalledef},
	            dataType: "json",
	            success: function( data ) {
	                response( data );
	            }
	        });
	     },
	    select: function( event, ui ) {
	    	$("#tid").val(ui.item.id);
	    }
	});
});

/**MANDA EL REPORTE A EXEL**/
function procesar03(){
	var usuario=$("#usuario").val();		
	url="http://"+getCookie("URL_Reportes")+"/parametrizacion/rptExcel003.jsp?v0="+usuario;
	window.open(url,"_NEW");
}

function mostrar(){
	var opt=$("#lDefinicion").val();
	if(opt==1 || opt==18 || opt==55){
		$("#tcodDetalleDefinicion").show();
		$("#tcodDetalleDefinicion").focus();
		}else{
			$("#tcodDetalleDefinicion").hide();
		}
	
	}


function validarCampos(op){
	
	if(nuevoD==0){
	alert("Click en NUEVO !");
	return false;
	}
	
	if(document.forms[0].elements[1].value==0){
		alert("Digite Definicion!");
		document.forms[0].elements[1].focus();
		return;
		}
	/*if(document.forms[0].elements[2].value.length==0){
		alert("Digite el código!");
		document.forms[0].elements[2].focus();
		return;
		}*/
	if(document.forms[0].elements[3].value.length==0){
		alert("Digite el detalle de la definicion!");
		document.forms[0].elements[3].focus();
		return;
		}	
	if(document.forms[0].elements[4].value.length==0){
		alert("Digite el concepto!");
		document.forms[0].elements[4].focus();
		return;
		}	
	if(op==1){
		guardarDatos();
		}	
	else{
		actualizarDatos();
		}
	}
	
function nuevoDef(){
	nuevoD=1;
	$("#bGuardar").show();
	$("#btnActualizarDef").hide();
	$("#bGuardarDef").show();
	limpiarcampos();
	$("#tdefinicion2").focus();
	$("#tfecha").val(fechaHoy());
	$("#tusuario").val($("#usuario").val());
	
	}

function guardarDatos(){
		$("#bGuardar").hide();
		var campo0=document.forms[0].elements[0].value;
		var campo1=document.forms[0].elements[1].value;
		var campo2=document.forms[0].elements[2].value;
		var campo3=document.forms[0].elements[3].value;
		var campo4=document.forms[0].elements[4].value;
		var campo5=document.forms[0].elements[5].value;
		var campo6=document.forms[0].elements[6].value;
		
		$.ajax({
			url: 'dDefinicionesG.php',
			type: "POST",
			data: "submit=&v0="+campo0+"&v1="+campo1+"&v2="+campo2+"&v3="+campo3+"&v4="+campo4+"&v5="+campo5+"&v6="+campo6,
			success: function(datos){
				$("#bGuardar").show();
				nuevoDef();
				alert(datos);
				limpiarCampos();
			}
		});
		nuevoD=0;
	}
	
function consultaDatos(){
	if($("#tdefinicion").val()==''){
		alert("Ingrese el detalle definicion a buscar.");
		$("#tdefinicion").focus();
		return false;
	}
	var iddDefinicion = $("#tid").val(); 
		$.ajax({
			url: 'dDefinicionesC.php',
			cache: false,
			type: "POST",
			data: {v0:iddDefinicion},
			dataType:"json",
			success: function(data){
				if(data==0){
					alert("Lo lamento, el registro no existe!");
					return false;
				}
				/*var result=datos.split(",");
				document.forms[0].elements[0].value=result[0];
				document.forms[0].elements[1].value=result[1];
				document.forms[0].elements[2].value=result[2];
				document.forms[0].elements[3].value=result[3];
				document.forms[0].elements[4].value=result[4];
				document.forms[0].elements[5].value=result[5];
				document.forms[0].elements[6].value=result[6];
				document.forms[0].idr.value=result[0];*/
				iddefinicion=$.trim(data.iddefinicion);
				iddetalledef=iddefinicion.slice(0,2);		
				
				$("#tid").val(data.iddetalledef);
				//Cargar Definicion
				$("#lDefinicion").val(iddefinicion).trigger("change");
				$("#tdefinicion2").val(data.codigo);
				$("#tdefinicion").val($.trim(data.detalledefinicion));
				$("#tconcepto").val($.trim(data.concepto));
				$("#tfecha").val($.trim(data.fechacreacion));
				$("#tusuario").val($.trim(data.usuario));
				$("#idr").val(data.iddetalledef);//campo oculto en dDefiniciones.php
				$("#btnActualizarDef").show();
				$("#bGuardarDef").hide();
			}
		});
	}

function actualizarDatos(){
		var campo0=document.forms[0].idr.value;
		var campo1=document.forms[0].elements[1].value;
		var campo2=document.forms[0].elements[2].value;
		var campo3=document.forms[0].elements[3].value;
		var campo4=document.forms[0].elements[4].value;
		var campo5=document.forms[0].elements[5].value;
		var campo6=document.forms[0].elements[6].value;
		
		$.ajax({
			url: 'dDefinicionesA.php',
			cache: false,
			type: "POST",
			data: "submit=&v0="+campo0+"&v1="+campo1+"&v2="+campo2+"&v3="+campo3+"&v4="+campo4+"&v5="+campo5+"&v6="+campo6,
			success: function(datos){
				alert(datos);
				limpiarCampos();
				
			}
		});
		nuevoD=0;
	}
	
/*function eliminarDato(){
		var msg = confirm("Desea eliminar este dato?")
		if ( msg ) {
			var id_registro=document.forms[0].idr.value;
			$.ajax({
				url: 'dDefinicionesE.php',
				type: "GET",
				data: "v0="+id_registro,
				success: function(datos){
					limpiarCampos();
					alert(datos);	
				}
			});
		}
		return false;
	}	*/

function limpiarcampos(){
$(".tablero input").val('');
$("#msje-def").empty();
if($("#iddefinicion").val()==''){
$(".tablero select").val(0);
}else{
$(".tablero select").val($("#iddefinicion").val());
}
}

/*function buscarCodigo(cod){
	$("#msje-def").empty();
	var tipoDef=$("#lDefinicion").val();
	$.getJSON("buscarCodigo.php",{v0:cod,v1:tipoDef},function(data){
		if(data!=0){
		$("#msje-def").html("Existe un "+$("#nombre").val()+" asociado a ese codigo "+data.codigo+" : <b> '"+data.detalledefinicion+"'</b>");
		$("#tdefinicion2").val('').focus();
		}
	});
}*/