// JavaScript Document
// objeto administración de los eventos del formulario de paises.php
nuevo=0;
$(function(){
	$( "#txtPais" ).autocomplete({
	    source: function( request, response ) {
	    	var nompais=$("#txtPais").val();
	        $.ajax({
	            url: "paisAut.php",
	            data: {v0:nompais},
	            dataType: "json",
	            success: function( data ) {
	                response( data );
	            }
	        });
	     },
	    select: function( event, ui ) {
	    	$("#txtId").val(ui.item.id);
	    }
	});
});

function nuevoB(){
	nuevo=1;
	$("#bGuardar").show();
	$("#btnActualizar").hide();
	$("#bGuardar").show();
	limpiarCampos();
	document.forms[0].elements[1].focus();
	document.forms[0].elements[3].value = fechaHoy();
	document.forms[0].elements[4].value= document.forms[0].elements['usuario'].value;
	}

/**MANDA EL REPORTE A EXEL**/
function procesar04(){
	var usuario=$("#usuario").val();		
	url="http://"+getCookie("URL_Reportes")+"/parametrizacion/rptExcel004.jsp?v0="+usuario;
	window.open(url,"_NEW");
}

function validarCampos(op){
	
	if(nuevo==0){
		alert("Click en NUEVO !");
		return false;
		}
	
	if($("#txtPais").val()==''){
		alert("Digite el Pais!");
		$("#txtPais").focus();
		return;
	}
	if($("#txtSigla").val()==''){
		alert("Digite la Sigla!");
		$("#txtSigla").focus();
		return;
	}
	
	if(op==1){
		guardarDatos();
	}	
	else{
		actualizarDatos();
		$("#btnActualizar").hide();
		$("#bGuardar").show();
	}
}

function guardarDatos(){
	
	$("#bGuardar").hide();
	var campo0=$("#txtPais").val();
	var campo1=$("#txtSigla").val();
	var campo2=$("#tFecha").val();
	var campo3=$("#tusuario").val();
	$.ajax({
		url: 'paisesG.php',
		type: "POST",
		data: "submit=&v0="+campo0+"&v1="+campo1+"&v2="+campo2+"&v3="+campo3,
		success: function(datos){
			$("#bGuardar").show();
			alert(datos);
			limpiarCampos();
		}
	});
}
	
function consultaDatos(){
	if($("#txtPais").val()==''){
		alert("Ingrese el Nombre del pais a buscar.");
		$("#txtPais").focus();
		return false;
	}
		var nompais=$("#txtPais").val();
		$.ajax({
			url: 'paisesC.php',
			cache: false,
			type: "POST",
			data: {v0:nompais},
			dataType:"json",
			success: function(data){
				if(data==0){
					alert("Lo lamento, el pais no existe!");
					return false;
				}
			
				/*$("#txtId").val(data.idpais);	*/			
				$("#txtPais").val($.trim(data.pais));
				$("#txtSigla").val($.trim(data.sigla));
				$("#tFecha").val($.trim(data.fechasistema));
				$("#tusuario").val($.trim(data.usuario));
				$("#idp").val(data.idpais);//campo oculto en paises.php
				$("#btnActualizar").show();
				$("#bGuardar").hide();
			}
		});
}

function actualizarDatos(){
		var campo0=parseInt($("#idp").val());
		var campo1=$("#txtPais").val();
		var campo2=$("#txtSigla").val();
		var campo3=$("#tFecha").val();
		var campo4=$("#tusuario").val();
		
		$.ajax({
			url: 'paisesA.php',
			type: "POST",
			data: {v0:campo0,v1:campo1,v2:campo2,v3:campo3,v4:campo4},
			success: function(datos){
				alert(datos);
				limpiarCampos();
				
				$("#btnActualizar").hide();
				$("#bGuardar").show();
			}
		});
	}	
function ayuda(){
	
	}	