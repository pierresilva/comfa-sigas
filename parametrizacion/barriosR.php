<?php
/* autor:       orlando puentes
 * fecha:       26/08/2010
 * objetivo:     
 */
date_default_timezone_set("America/Bogota");
setlocale(LC_ALL,"es_ES");

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once 'clases' . DIRECTORY_SEPARATOR . 'barrios.class.php';
//require('clases/barrios.class.php');
$objClase=new Barrios;
$consulta=$objClase->mostrar_datos();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<title>Barrios</title>
		<script src="../js/jquery-1.3.2.min.js" type="text/javascript"></script>
		<link href="../css/estiloReporte.css" rel="stylesheet" type="text/css" />
	</head>
	<body>
        <table width="100%" border="0" cellspacing="0">
            <tr>
                <td width="2%">&nbsp;</td>
                <td width="5%"><img src="../imagenes/LogoSimple.gif" width="53" height="74" /></td>
                <td width="93%" align="center" valign="bottom"><img src="../imagenes/razonSocial.png" width="615" height="55" /></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td align="center"><label style="font-family:Tahoma, Geneva, sans-serif; color:#666; font-size:24px">Listado de Barrios</label></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td colspan="2">Fecha de Impresión: <?php echo strftime("%B %d de %Y, hora: %r %p") ?></td>
            </tr>
        </table>
        <center>
            <table width="80%" border="0" cellspacing="2" class="tablero">
                <tr>
                    <th width="15%">ID BARRIO</th>
                    <th width="25%">CODIGO ZONA</th>
                    <th width="40%">BARRIO</th>
                    <th width="15%">FECHA SISTEMA</th>
                    <th width="5%">USUARIO</th>
                </tr>
                <?php
                while( $row = mssql_fetch_array($consulta) ){
                    ?>	  
                    <tr>
                        <td align="left"><?php echo $row['idbarrio']; ?> </td>
                        <td align="left"><?php echo $row['codigozona']; ?></td>
                        <td align="left"><?php echo ucwords($row['barrio']); ?></td>
                        <td align="left"><?php echo $row['fechasistema']; ?></td>
                        <td align="left"><?php echo $row['usuario']; ?></td>
                    </tr>
                    <?php
                }
                ?>
            </table>
        </center>
	</body>
</html>
