<?php
/* autor:       orlando puentes
 * fecha:       02/09/2010
 * objetivo:    
 */
//mod29/08/2012 $root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
//include_once  $root;
 
$raiz=dirname(__FILE__);
$raiz2=substr($raiz,0,-6);
$imagenes=$raiz2 . DIRECTORY_SEPARATOR . "imagenes" . DIRECTORY_SEPARATOR;
$ruta_generados= $raiz2 . DIRECTORY_SEPARATOR . 'planos_generados' . DIRECTORY_SEPARATOR;
$ruta_cargados= $raiz2 . DIRECTORY_SEPARATOR . "planos_cargados" . DIRECTORY_SEPARATOR;
define('ruta_cargados2', $raiz2 . DIRECTORY_SEPARATOR . "planos_cargados" . DIRECTORY_SEPARATOR);
$ruta_movimientos= $raiz2 . DIRECTORY_SEPARATOR . "planos_cargados" . "movimientos" . DIRECTORY_SEPARATOR;
$ip=$_SERVER['SERVER_NAME'];
$url="http://".$ip."/sigas/";
$url_img="http://".$ip."/imagenes/";
$url_dig="http://".$ip."/";
$url_rep="http://".$ip."/sigas/centroReportes/";
//$ruta_reportes="http://".$ip.":8080/sigasReportes/";
//$ruta_reportes="http://10.10.1.57:8080/sigasReportes/";
$ruta_reportes="http://10.10.30.102:8080/sigasReportes/";

$url2 ="http://".$ip."/";
$conexion_ifx ="jdbc:informix-sqli://10.10.1.190:9088/aportes:informixserver=sigas_dev;user=informix;password=informix";

define("DESARROLLO",1);
define("URL_PORTAL",$url);
define("URL_SERVIDOR_ARCHIVOS_PLANOS","http://". $ip ."/");
define("DIRECTORIO_RAIZ",$raiz);
define("DIRECTORIO_CENTRO_REPORTES","centroReportes/");
define("DIRECTORIO_LOGS","logs/");
define("DIRECTORIO_PHP_COMUNES","phpComunes/");

define("RUTA_FISICA_CENTRO_REPORTES",DIRECTORIO_RAIZ.DIRECTORIO_CENTRO_REPORTES);
define("DIRECTORIO_CLASES","clases/");
define("DIRECTORIO_CLASES_RSC","rsc/");
define("RUTA_FISICA_DIRECTORIO_CLASES",DIRECTORIO_RAIZ.DIRECTORIO_CLASES);
define("RUTA_FISICA_DIRECTORIO_CLASES_RSC",DIRECTORIO_RAIZ. DIRECTORY_SEPARATOR .DIRECTORIO_CLASES_RSC);

define("RUTA_DIR_ARCHIVOS_LOGS", "/var/www/html/logs/");
define("DIR_ARCHIVOS_PLANOS_CARGADOS", "/var/www/html/planos_cargados/");
define("DIR_ARCHIVOS_PLANOS_GENERADOS", "/var/www/html/planos_generados/");
define("CADENA_CONEXION_INFORMIX",$conexion_ifx);
define("URL_JAVABRIDGE","http://127.0.0.1:8080/JavaBridge/java/Java.inc");
define("URL_REPORTES_JSP","http://10.10.1.57:8080");
define("DIRECTORIO_REPORTES_JSP","sigasReportes");

define("DIRECTORIO_ESTILOS_CSS","css/");
define("DIRECTORIO_IMAGENES","imagenes/");
define("DIRECTORIO_ESTILOS_CSS_FORMULARIOS",DIRECTORIO_ESTILOS_CSS."formularios/");

define("DIRECTORIO_SCRIPTS_JS","js/");
define("DIRECTORIO_SCRIPTS_JS_FORMULARIOS",DIRECTORIO_SCRIPTS_JS."formularios/");

define("DSN_CDI","10.10.1.157");
define("USER_DB_CDI","sigas");
define("PASS_USER_DB_CDI","654321");
define("DATABASE_CDI","Cdi");
define("PROCESO_DESARROLLO_FINANCIERO_CDI","DF");
define("AREA_SUBSIDIO_CDI",7);
define("PROCESO_AREA_SUBSIDIO_CDI","DF-07");

// servidor y rutas digitalizaci�n de documentos
define("SERVIDOR_IMAGENES_DIGITALIZADAS","10.10.1.4:8080");

global $arregloAgencias;
$arregloAgencias = array(
						"01" => "Neiva",
						"02" => "Garzon",
						"03" => "Pitalito",
						"04" => "La plata");

// 123456 = e10adc3949ba59abbe56e057f20f883e

define ('USUARIO_WEB_SIGAS','c523d9f153587fbe3c96fee41c26b471');
define ('CONTRASENA_WEB_SIGAS','711b60b69a49aa33bfa888930b2ecf10');

define('COMPROBANTE_PLANILLA_UNICA','CPU');
define('CUENTA_CONTABLE_CREDITO','410505001');
define('CUENTA_CONTABLE_DEBITO','111005049'); //183001001
define('CENTRO_DE_COSTO','010401'); //090105
define('CODIGO_INSTIT_COMFAMILIAR',2695);

global $comprobantesAportes;
$comprobantesAportes = array("99", "RC1", "RC2", "RC3", "RC4", "RC5", "RC6", "R10", "CP1", "CP2", "CP3","PU2","CO5","NC2","CRC");

// Las planillas �nicas que quedan marcadas con este usuario son las que se han cargado por medio del m�dulo de PU.
define("USUARIO_CARGA_PLANILLA_UNICA","PorPlanilla");
?>