<?php
set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");

$root= $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $_SESSION['RAIZ'] . DIRECTORY_SEPARATOR . '/rsc/pdo/IFXDbManejador.php';

class Reclamo{
	private $db;
	
/** CONSTRUCTOR **/
	function Reclamo(){
		$this->db = IFXDbManejador::conectarDB();
		if($this->db->conexionID==null){
			$cadena = $this->db->error;
			echo msg_error($cadena);
			exit();
		}
 	}
	
/** CREATE **/
 	 	 	
/** READ **/
 	 	
 	function buscarReclamosAfiliado($idp){
 		$data = array(); $con = 0;
 		$sql = "SELECT fechareclamo,periodoinicial,periodofinal,estado,fechagiro,idcausal,detalledefinicion FROM aportes055 INNER JOIN aportes091 ON aportes091.iddetalledef=aportes055.idcausal WHERE idtrabajador=$idp"; 		
 		$rs = $this->db->querySimple($sql);
 		while($row = $rs->fetch()){
 			$con++;
 			$data[]=array_map("utf8_encode",$row);
 		}
 		if($con>0) {
 			return $data;
 		} else
 			return 0;
 	}
 	 	
 	function buscarReclamosEmpresa($ide){
 		$data = array(); $con = 0;
 		$sql = "SELECT fechareclamo,periodoinicial,periodofinal,estado,fechagiro,idcausal,detalledefinicion FROM aportes060 INNER JOIN aportes091 ON aportes091.iddetalledef=aportes060.idcausal WHERE idempresa=$ide";
 		$rs = $this->db->querySimple($sql);
 		while($row = $rs->fetch()){
 		$con++;
 		$data[]=array_map("utf8_encode",$row);
 	}
 	if($con>0) {
 		return $data;
 		} else
 			return 0;
 	}
 	
 	function procesarReclamoAfiliado($idReclamo){
		
 		if( $this->estadoPeriodoActual() != "A")
 			return 1;
 		
		$totalProcesados = NULL;
		$totalNoProcesados = NULL;
		$sentencia = $this->db->conexionID->prepare ( "EXEC giros.sp_Maestro_Revision_Afiliado
 				@id = 0
 				, @usuario = null
 				, @idReclamo = $idReclamo
 				, @TotalProcesados = :TotalProcesados
 				, @TotalNoProcesados = :TotalNoProcesados" );
		$sentencia->bindParam ( ":TotalProcesados", $totalProcesados, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 4000 );
		$sentencia->bindParam ( ":TotalNoProcesados", $totalNoProcesados, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 4000 );
		$sentencia->execute ();
		if (($totalProcesados + $totalNoProcesados) > 0) {
			return 1;
		} else {
			return 0;
		}
	}
 	
 	function procesarReclamoEmpresa($idReclamo) {
		
 		if( $this->estadoPeriodoActual() != "A")
 			return 1;
 		
		$totalProcesados = NULL;
		$totalNoProcesados = NULL;
		$sentencia = $this->db->conexionID->prepare ( "EXEC giros.sp_Maestro_Revision_Empresa
 				@id = 0
 				, @usuario = null
 				, @idReclamo = $idReclamo
 				, @TotalProcesados = :TotalProcesados
 				, @TotalNoProcesados = :TotalNoProcesados" );
		
		$sentencia->bindParam ( ":TotalProcesados", $totalProcesados, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 4000 );
		$sentencia->bindParam ( ":TotalNoProcesados", $totalNoProcesados, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 4000 );
		
		$sentencia->execute ();
		if (($totalProcesados + $totalNoProcesados) > 0) {
			return 1;
		} else {
			return 0;
		}
	}
	
	function estadoPeriodoActual(){
		$sentencia = $this->db->conexionID->prepare("SELECT TOP 1 estadogiro FROM aportes012 WHERE procesado = 'N' ORDER BY periodo");
		$sentencia->execute();
		return $sentencia->fetchObject()->estadogiro;
	}
/** UPDATE **/
 	
/** DELETE **/
}	
?>