<?php
set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");

$root= $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $_SESSION['RAIZ'] . DIRECTORY_SEPARATOR . '/rsc/pdo/IFXDbManejador.php';

class Aporte{
	private $db;
	
/** CONSTRUCTOR **/
	function Aporte(){
		$this->db = IFXDbManejador::conectarDB();
		if($this->db->conexionID==null){
			$cadena = $this->db->error;
			echo msg_error($cadena);
			exit();
		}
 	}
	
/** CREATE **/
 	 	 	
/** READ **/
 	 	
 	function buscarAportesIdempresa($ide){
 		$data = array(); $con = 0;
 		$sql="select nit,aportes011.idaporte,periodo,valornomina,valoraporte,aportes011.trabajadores,ajuste,fechapago,comprobante,documento,numerorecibo,aportes011.fechasistema,aportes011.usuario from aportes011 INNER JOIN aportes048 ON aportes011.idempresa=aportes048.idempresa where aportes011.idempresa in ($ide) ORDER BY periodo DESC";
 		$rs = $this->db->querySimple($sql);
 		while($row = $rs->fetch()){
 		$con++;
 		$data[]=array_map("utf8_encode",$row);
	 	}
	 	if($con>0) {
	 		return $data;
	 	} else
	 			return 0;
 	}
 	
 	function buscarPlanillasIdpersona($idp){
 		$data = array(); $con = 0;
 		$sql="select idplanilla, planilla, aportes010.nit, aportes048.razonsocial as rsocial, salariobasico, ingresobase, diascotizados, isnull(ingreso,'')ingreso, isnull(retiro,'')retiro, isnull(var_tra_salario,'')var_tra_salario, isnull(var_per_salario,'')var_per_salario, isnull(sus_tem_contrato,'')sus_tem_contrato, isnull(inc_tem_emfermedad,'')inc_tem_emfermedad, isnull(lic_maternidad,'')lic_maternidad, isnull(vacaciones,'')vacaciones, isnull(inc_tem_acc_trabajo,'')inc_tem_acc_trabajo, isnull(tipo_cotizante,'')tipo_cotizante, isnull(periodo,'')periodo, correccion, aportes010.idempresa, idtrabajador, horascotizadas, procesado, control,  fechapago, aportes010.fechasistema, usuariomodifica, fechamodifica, valoraporte from aportes010 inner join aportes048 on aportes048.nit=aportes010.nit where idtrabajador=$idp ORDER BY periodo desc";
 		$rs = $this->db->querySimple($sql);
 		while($row = $rs->fetch()){
 			$con++;
 			$data[]=array_map("utf8_encode",$row);
 		}
 		if($con>0) {
 			return $data;
 		} else
 			return 0;
 	}
 	
/** UPDATE **/
 	
/** DELETE **/
}	
?>