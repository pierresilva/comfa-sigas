<?php
set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");

$root= $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $_SESSION['RAIZ'] . DIRECTORY_SEPARATOR . '/rsc/pdo/IFXDbManejador.php';
include_once $_SESSION['RAIZ'] . DIRECTORY_SEPARATOR . '/pdo/reclamo.class.php';
class ReclamoTrabajador {
	private $db;
	
/** CONSTRUCTOR **/
	function ReclamoTrabajador () {
		$this->db = IFXDbManejador::conectarDB();
		if($this->db->conexionID==null){
			$cadena = $this->db->error;
			exit();
		}
 	}
	
/** CREATE **/
 	
 	function guardarReclamoTrabajador($campos){
 		$sql="INSERT INTO aportes055 ( idtrabajador, fechareclamo			 , periodoinicial, periodofinal, numerocuotas, estado, idcausal, fechagiro, periodogiro, usuarioprocesa, notas, usuario, tempo1, pqr )
			   				 VALUES  (:idtrabajador,CAST(GETDATE() AS DATE)  ,:periodoinicial,:periodofinal,:numerocuotas,:estado,:idcausal,:fechagiro,:periodogiro,:usuarioprocesa,:notas,:usuario,:tempo1,:pqr )"; 		
 		
 		$statement = $this->db->conexionID->prepare($sql);
 		$campos->usuario=$_SESSION['USUARIO']; 		
 		$guardada = false;
 		
 		$count=0;
 		$tmp = get_object_vars($campos); 		
 		foreach($tmp as $indice=>$valor){ 			
 			if($count>0 && $count!=2){ 				
 				if(empty($valor)==true){ $statement->bindValue(":".$indice, null,  PDO::PARAM_STR);
 				} else { $statement->bindValue(":".$indice, $valor,  PDO::PARAM_STR); }
 			}
 			$count++;
 		} 		
 		$guardada = $statement->execute();
 		if($guardada){
 			$rs = $this->db->conexionID->lastInsertId();
 			$objReclamo = new Reclamo();
 			$objReclamo->procesarReclamoAfiliado($rs);
 			return trim($rs);
 		}else{
 			return 0;
 		} 		
 	}
 	
/** READ **/
 	
/** UPDATE **/
 	
/** DELETE **/
}	
?>