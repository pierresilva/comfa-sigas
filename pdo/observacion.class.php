<?php
set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");

$root= $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $_SESSION['RAIZ'] . DIRECTORY_SEPARATOR . '/rsc/pdo/IFXDbManejador.php';

class Observacion{
	private $db;
	
/** CONSTRUCTOR **/
	function Observacion(){
		$this->db = IFXDbManejador::conectarDB();
		if($this->db->conexionID==null){
			$cadena = $this->db->error;
			echo msg_error($cadena);
			exit();
		}
 	}
	
/** CREATE **/
 	
 	/**
 	 * METODO Encargado de guardar las observaciones de la persona o empresa
 	 * 
 	 * @param unknown_type $id Id Registro
 	 * @param unknown_type $tipo Tipo de observacion: [1] Persona - [2] Empresa
 	 * @param unknown_type $observacion
 	 * @param unknown_type $usuario
 	 * 
 	 * @return number Retorna 1 si no hay error, 0 si existe error
 	 */
 	function crearObservacion($id,$tipo,$observacion,$usuario){
 		$fechaSistema=date("m/d/Y");
 		$observacion= $fechaSistema." - ".$usuario." - ".$observacion;
 		
 		$resultado = 0;
 		$sentencia = $this->db->conexionID->prepare ( "EXEC [dbo].[sp_Almacenar_Observacion]
 				@id_registro = $id
 				, @flag_observacion = 1
 				, @observacion = '$observacion'
 				, @resultado = :resultado" );
 		
 		$sentencia->bindParam ( ":resultado", $resultado, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 4000 );
 		$sentencia->execute();
 				
 		if ($resultado > 0)
 			return 1;
 		else
 			return 0;
 	}	 	 
 
/** READ **/
 	
 	/** Trae informacion las observaciones realizadas a determinado registro **/
 	function buscarObservacion($idr,$tipo){
 		$data = array(); $con = 0;
 		$sql = "Select observaciones,idobservacion,usuarios from aportes088 where idregistro=$idr and identidad=$tipo;"; 		
 		$rs = $this->db->querySimple($sql);
 		while($row = $rs->fetch()){
 			$con++;
 			$data[]=array_map("utf8_encode",$row);
 		}
 		if($con>0) {
 			return $data;
 		} else
 			return 0;
 	}
 	
/** UPDATE **/
 	
/** DELETE **/
}	
?>