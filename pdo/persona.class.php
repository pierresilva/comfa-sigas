<?php
set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");

$root= $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $_SESSION['RAIZ'] . DIRECTORY_SEPARATOR . 'rsc/pdo/IFXDbManejador.php';

class Persona{
	private $db;
	
	
/** CONSTRUCTOR **/
	function Persona(){
		
		$this->db = IFXDbManejador::conectarDB();
		if($this->db->conexionID==null){
			$cadena = $this->db->error;
			echo msg_error($cadena);
			exit();
		}
 	}
	
/** CREATE **/
 	
 	function guardarPersona($campos){
 		$sql="INSERT INTO aportes015 ( idtipodocumento, identificacion, papellido, sapellido, pnombre, snombre, sexo, direccion, idbarrio, telefono, celular, email, idpropiedadvivienda, idtipovivienda, idciuresidencia, iddepresidencia, idzona, idestadocivil, fechanacimiento, idciunace, iddepnace, capacidadtrabajo, desplazado, reinsertado, idescolaridad, idprofesion, conyuges, hijos, hijosmenores, hermanos, padres, tarjeta, documentacionfonede, ruaf, biometria, rutadocumentos, fechaafiliacion, estado, validado, flag, tempo1, tempo2, tempo3, usuario, fechasistema          , fechaactualizacion, nombrecorto, codigoedad, edad, fechadefuncion, tipoestado, idpais )
 							  VALUES (:idtipodocumento,:identificacion,:papellido,:sapellido,:pnombre,:snombre,:sexo,:direccion,:idbarrio,:telefono,:celular,:email,:idpropiedadvivienda,:idtipovivienda,:idciuresidencia,:iddepresidencia,:idzona,:idestadocivil,:fechanacimiento,:idciunace,:iddepnace,:capacidadtrabajo,:desplazado,:reinsertado,:idescolaridad,:idprofesion,:conyuges,:hijos,:hijosmenores,:hermanos,:padres,:tarjeta,:documentacionfonede,:ruaf,:biometria,:rutadocumentos,:fechaafiliacion,:estado,:validado,:flag,:tempo1,:tempo2,:tempo3,:usuario,cast(getDate() as Date),:fechaactualizacion,:nombrecorto,:codigoedad,:edad,:fechadefuncion,:tipoestado,:idpais )"; 		
 		$statement = $this->db->conexionID->prepare($sql);
 		$campos->usuario=$_SESSION['USUARIO']; 		
 		$guardada = false; 		
 		
 		$count=0;
 		$tmp = get_object_vars($campos);
 		foreach($tmp as $indice=>$valor){
 			if($count>0 && $count!=45){
 				if(empty($valor)==true){ $statement->bindValue(":".$indice, null,  PDO::PARAM_STR);
 				} else { $statement->bindValue(":".$indice, $valor,  PDO::PARAM_STR); }
 			}
 			$count++;
 		}		
 		
 		$guardada = $statement->execute(); 		
 		if($guardada){
 			$rs = $this->db->conexionID->lastInsertId();
 			return trim($rs);
 		}else{
 			return 0;
 		} 		
 	}
 	
/** READ **/

 	function buscarPersona($idp=0,$idtd=0,$numero=0,$pnombre='',$papellido='',$op){
 		switch ($op) {
 			case 1: $sql="SELECT idpersona, idtipodocumento, identificacion, papellido, sapellido, pnombre, snombre, estado,sexo, nombrecorto,telefono,celular FROM aportes015 where idpersona = $idp"; break;
 			case 2: $sql="SELECT idpersona, idtipodocumento, identificacion, papellido, sapellido, pnombre, snombre, estado,sexo, nombrecorto,telefono,celular FROM aportes015 where idtipodocumento=$idtd and identificacion='$numero'"; break;
 			case 3: $sql="SELECT TOP 1 idpersona, idtipodocumento, identificacion, papellido, sapellido, pnombre, snombre, estado,sexo, nombrecorto FROM aportes015 where identificacion='$numero'"; break;
 			case 7: $sql="SELECT aportes015.*,bono from aportes015 left join aportes101 on aportes101.idpersona=aportes015.idpersona WHERE idtipodocumento=$idtd and identificacion='$numero'"; break;
 			
 		}
 		return $this->db->conexionID->query($sql);
 			
 	}
 	
 	function buscarPersona2($idtd=0,$num=0,$flag=0){
 		$data = array();
 		$con = 0;
 		switch($flag){
			case 1 : $sql="Select idpersona, idtipodocumento, identificacion, papellido, sapellido, pnombre, snombre, sexo, fechanacimiento,estado from aportes015 where identificacion='$num'"; break;
			case 2 : $sql="Select idpersona, idtipodocumento, identificacion, papellido, sapellido, pnombre, snombre, sexo, fechanacimiento,estado from aportes015 where idtipodocumento=$idtd and identificacion='$num'"; break;
			case 3 : $sql="Select idpersona, idtipodocumento, identificacion, papellido, sapellido, pnombre, snombre, sexo, fechanacimiento,estado from aportes015 where pnombre like '$num'"; break;
			case 4 : $sql="Select idpersona, idtipodocumento, identificacion, papellido, sapellido, pnombre, snombre, sexo, fechanacimiento,estado from aportes015 where papellido like '$num'"; break;
			case 5 : $sql="Select idpersona, idtipodocumento, identificacion, papellido, sapellido, pnombre, snombre, sexo, fechanacimiento,estado from aportes015 where idpersona=$idtd"; break;
			case 6 : $sql="Select idpersona, nombrecorto from aportes015 where idtipodocumento=$idtd and identificacion='$num'"; break;
			case 7 : $sql="Select aportes015.*,bono from aportes015 left join aportes101 on aportes101.idpersona=aportes015.idpersona where idtipodocumento=$idtd and identificacion='$num'"; break;
			case 8 : $sql="SELECT a15.*, a101.idtarjeta, a101.bono, a101.fechasolicitud, a101.estado estadoTarjeta, a101.ubicacion, a500.agencia ubicacionAgencia, a101.saldo FROM aportes015 a15 LEFT JOIN aportes101 a101 ON a101.idpersona=a15.idpersona LEFT JOIN aportes500 a500 ON a500.codigo=a101.ubicacion WHERE a15.idtipodocumento='$idtd' AND a15.identificacion='$num'"; break;
			case 9 : $sql="SELECT a15.*, a101.idtarjeta, a101.bono, a101.fechasolicitud, a101.estado estadoTarjeta, a101.ubicacion, a500.agencia ubicacionAgencia, a101.saldo FROM aportes015 a15 LEFT JOIN aportes101 a101 ON a101.idpersona=a15.idpersona LEFT JOIN aportes500 a500 ON a500.codigo=a101.ubicacion WHERE a101.bono='$num'"; break;
			case 10: $sql="SELECT TOP 1 a91.codigo tipodocumento, a15.identificacion, a15.papellido, a15.sapellido, a15.pnombre, a15.snombre, a15.estado, a15.fechadefuncion, a89.municipio AS zona, a15.direccion, a15.telefono, a15.fechanacimiento, DATEDIFF(day, fechanacimiento, getdate())/365 AS dias, a15.ruaf FROM aportes015 a15 INNER JOIN aportes091 a91 ON a91.iddetalledef=a15.idtipodocumento LEFT JOIN aportes089 a89 ON a89.codzona=a15.idzona WHERE idpersona=$num"; break;
		}
 		$rs = $this->db->querySimple($sql);
 		while($row = $rs->fetch()){
 			$con++;
 			$data[]=array_map("utf8_encode",$row);
 		}
 		if($con>0) {
 			return $data;
 		} else
 			return 0;
 	}
 	
 	function buscarPersonaAct($idtd,$num){ 		
 			$sql="Select * from aportes015 where idtipodocumento=$idtd and identificacion='$num'";
 			return $this->db->conexionID->query($sql);
 	}
 	
 	function buscarRegistroPersona($idp=0,$idtd=0,$num=''){
 		if($idp==0)
 			$sql="SELECT idpersona, idtipodocumento, identificacion, papellido, sapellido, pnombre, snombre, sexo, direccion, idbarrio, telefono, celular, email,idciuresidencia, iddepresidencia, idzona, idestadocivil, fechanacimiento, idciunace, iddepnace, capacidadtrabajo, idprofesion, rutadocumentos, fechaafiliacion, estado,nombrecorto, fechadefuncion,idpais,idpropiedadvivienda,datediff(day,fechanacimiento,getdate()) / 365.25 AS edad FROM dbo.aportes015 where idtipodocumento=$idtd and identificacion='$num'";
 		else 
 			$sql="SELECT idpersona, idtipodocumento, identificacion, papellido, sapellido, pnombre, snombre, sexo, direccion, idbarrio, telefono, celular, email,idciuresidencia, iddepresidencia, idzona, idestadocivil, fechanacimiento, idciunace, iddepnace, capacidadtrabajo, idprofesion, rutadocumentos, fechaafiliacion, estado,nombrecorto, fechadefuncion,idpais,idpropiedadvivienda,datediff(day,fechanacimiento,getdate()) / 365.25 AS edad FROM dbo.aportes015 where idpersona=$idp";
 		return $this->db->conexionID->query($sql);
 		
 	}
 	
/** UPDATE **/
 	
	function marcarIncapacitadoPersona($idb){
 		$sql="UPDATE aportes015 SET capacidadtrabajo='I', fechasistema=CAST(GETDATE() AS DATE), usuario=:campo1 WHERE idpersona=:campo0";
 		$statement = $this->db->conexionID->prepare($sql);
 		$guardada = false;
 		$statement->bindParam(":campo0",  $idb,  PDO::PARAM_INT);
 		$statement->bindParam(":campo1",  $_SESSION['USUARIO'],  PDO::PARAM_STR); 		
 		$guardada = $statement->execute();
 			
 		return $guardada ? 1 : 0;
 	}
 	
 	function actualizarPersona($campos){
 		$sql="UPDATE aportes015 SET idtipodocumento = :$campo0, identificacion= :$campo1, papellido= :$campo2, sapellido= :$campo3, pnombre= :$campo4, snombre= :$campo5, 
 		sexo= :$campo6, idestadocivil= :$campo7, fechanacimiento= :$campo8, idciunace= :$campo9, iddepnace= :$campo10,capacidadtrabajo= :$campo11,
 		nombrecorto= :$campo12"; 
 		$statement = $this->db->conexionID->prepare($sql);
 		$guardada = false;
 		$statement->bindParam(":campo0",  $campos[0],  PDO::PARAM_INT);
 		$statement->bindParam(":campo1",  $campos[1],  PDO::PARAM_STR);
 		$statement->bindParam(":campo2",  $campos[2],  PDO::PARAM_STR);
 		$statement->bindParam(":campo3",  $campos[3],  PDO::PARAM_STR);
 		$statement->bindParam(":campo4",  $campos[4],  PDO::PARAM_STR);
 		$statement->bindParam(":campo5",  $campos[5],  PDO::PARAM_STR);
 		$statement->bindParam(":campo6",  $campos[6],  PDO::PARAM_STR);
 		$statement->bindParam(":campo7",  $campos[7],  PDO::PARAM_INT);
 		$statement->bindParam(":campo8",  $campos[8],  PDO::PARAM_STR);
 		$statement->bindParam(":campo9",  $campos[9],  PDO::PARAM_STR);
 		$statement->bindParam(":campo10", $campos[10], PDO::PARAM_STR);
 		$statement->bindParam(":campo11", $campos[11], PDO::PARAM_STR);
 		$statement->bindParam(":campo12", $campos[12], PDO::PARAM_STR);
 		$guardada = $statement->execute();
 		
 		echo $guardada ? 1 : 0;
 		
 	}
 	
 	function actualizarPersonaSimple($campos){
 		//print_r($campos);
 		$usuario = $_SESSION['USUARIO'];
 		$campo00=$campos->idpersona;
 		
 		$campo08=$campos->direccion;
 		$campo09=$campos->idbarrio;
 		$campo10=$campos->telefono;
 		$campo11=$campos->celular;
 		$campo12=$campos->email;
 		$campo14=$campos->idciuresidencia;
 		$campo15=$campos->iddepresidencia;
 		$campo16=$campos->idzona;
 		$campo27= $usuario;

 			
 		$sql="UPDATE aportes015 SET direccion = :campo08, idbarrio = :campo09, telefono = :campo10, 
 		celular = :campo11, email = :campo12, idciuresidencia = :campo14, iddepresidencia = :campo15,
 		idzona = :campo16, usuario = :campo27, fechaactualizacion = CAST(GETDATE() AS DATE) 
 		where idpersona = $campo00";
 		$statement = $this->db->conexionID->prepare($sql);
 		
 		$guardada = false;
 		
 		$statement->bindParam(':campo08', $campo08, PDO::PARAM_STR);
 		$statement->bindParam(':campo09', $campo09, PDO::PARAM_STR);
 		$statement->bindParam(':campo10', $campo10, PDO::PARAM_STR);
 		$statement->bindParam(':campo11', $campo11, PDO::PARAM_STR);
 		$statement->bindParam(':campo12', $campo12, PDO::PARAM_STR);
 		$statement->bindParam(':campo14', $campo14, PDO::PARAM_STR);
 		$statement->bindParam(':campo15', $campo15, PDO::PARAM_STR);
 		$statement->bindParam(':campo16', $campo16, PDO::PARAM_STR);
 		$statement->bindParam(':campo27', $campo27, PDO::PARAM_STR);
 			
 		$guardada = $statement->execute();
 	
 		return $guardada ? 1 : 0;
 			
 	}
 	
 	// Cuando selecciona el tipo de radicacion actualiza persona y guarda auditoria
 	function insertarAuditoria($idp,$nota,$tipo){
 		$usuario = $_SESSION['USUARIO'];
 		
 		$sql="INSERT INTO aportes556 ( idpersona, notas, fechaactualiza ,usuario, tipo)
 		VALUES ($idp,'$nota',cast(getDate() as Date),'$usuario',$tipo)";
 		$statement = $this->db->conexionID->prepare($sql);
 		$guardada = false;
 		$guardada = $statement->execute();
 	
 		return $guardada ? 1 : 0;
 	}
 	
 	function actualizarPersonaCompleta($campos){
 		$usuario = $_SESSION['USUARIO'];
 		$campo00=$campos->idpersona;
 		$campo01=$campos->idtipodocumento;
 		$campo02=$campos->identificacion;
 		$campo03=$campos->papellido;
 		$campo04=$campos->sapellido;
 		$campo05=$campos->pnombre;
 		$campo06=$campos->snombre;
 		$campo07=$campos->sexo;
 		$campo08=$campos->direccion;
 		$campo09=$campos->idbarrio;
 		$campo10=$campos->telefono;
 		$campo11=$campos->celular;
 		$campo12=$campos->email;
 		$campo13=$campos->idpropiedadvivienda;
 		$campo14=$campos->idciuresidencia;
 		$campo15=$campos->iddepresidencia;
 		$campo16=$campos->idzona;
 		$campo17=$campos->idestadocivil;
 		$campo18=$campos->fechanacimiento;
 		$campo19=$campos->idciunace;
 		$campo20=$campos->iddepnace;
 		$campo21=$campos->capacidadtrabajo;
 		$campo22=$campos->idprofesion;
 		$campo23=$campos->rutadocumentos;
 		$campo24=$campos->fechaafiliacion;
 		$campo25=$campos->estado;
 		$campo26='N';
 		$campo27= $usuario;
 		$campo28='20130509';
 		$campo29=$campos->nombrecorto;
 		$campo30=1;
 		$campo31=1; 
 		$campo32=$campos->idpais;
 		
 		$sql="UPDATE aportes015 SET idtipodocumento = :campo01, identificacion = :campo02, papellido = :campo03, sapellido = :campo04, pnombre = :campo05, snombre = :campo06,
 		sexo = :campo07, direccion = :campo08, idbarrio = :campo09, telefono = :campo10, celular = :campo11, email = :campo12, idpropiedadvivienda = :campo13,  
 		idciuresidencia = :campo14, iddepresidencia = :campo15, idzona = :campo16, idestadocivil = :campo17, fechanacimiento = :campo18, idciunace = :campo19, 
 		iddepnace = :campo20, capacidadtrabajo = :campo21,idprofesion = :campo22, rutadocumentos = :campo23, fechaafiliacion = :campo24, estado = :campo25, validado = :campo26, 
 		usuario = :campo27, fechaactualizacion = CAST(GETDATE() AS DATE), nombrecorto = :campo29, codigoedad = :campo30, edad = :campo31, idpais = :campo32 where idpersona = $campo00";
 		$statement = $this->db->conexionID->prepare($sql);
 		$guardada = false;
 		
 		$statement->bindParam(':campo01', $campo01, PDO::PARAM_INT);
 		$statement->bindParam(':campo02', $campo02, PDO::PARAM_STR);
 		$statement->bindParam(':campo03', $campo03, PDO::PARAM_STR);
 		$statement->bindParam(':campo04', $campo04, PDO::PARAM_STR);
 		$statement->bindParam(':campo05', $campo05, PDO::PARAM_STR);
 		$statement->bindParam(':campo06', $campo06, PDO::PARAM_STR);
 		$statement->bindParam(':campo07', $campo07, PDO::PARAM_STR);
 		$statement->bindParam(':campo08', $campo08, PDO::PARAM_STR);
 		$statement->bindParam(':campo09', $campo09, PDO::PARAM_STR);
 		$statement->bindParam(':campo10', $campo10, PDO::PARAM_STR);
 		$statement->bindParam(':campo11', $campo11, PDO::PARAM_STR);
 		$statement->bindParam(':campo12', $campo12, PDO::PARAM_STR);
 		$statement->bindParam(':campo13', $campo13, PDO::PARAM_INT);
 		$statement->bindParam(':campo14', $campo14, PDO::PARAM_STR);
 		$statement->bindParam(':campo15', $campo15, PDO::PARAM_STR);
 		$statement->bindParam(':campo16', $campo16, PDO::PARAM_STR);
 		$statement->bindParam(':campo17', $campo17, PDO::PARAM_STR);
 		$statement->bindParam(':campo18', $campo18, PDO::PARAM_STR);
 		$statement->bindParam(':campo19', $campo19, PDO::PARAM_STR);
 		$statement->bindParam(':campo20', $campo20, PDO::PARAM_STR);
 		$statement->bindParam(':campo21', $campo21, PDO::PARAM_STR);
 		$statement->bindParam(':campo22', $campo22, PDO::PARAM_STR);
 		$statement->bindParam(':campo23', $campo23, PDO::PARAM_STR);
 		$statement->bindParam(':campo24', $campo24, PDO::PARAM_STR);
 		$statement->bindParam(':campo25', $campo25, PDO::PARAM_STR);
 		$statement->bindParam(':campo26', $campo26, PDO::PARAM_STR);
 		$statement->bindParam(':campo27', $campo27, PDO::PARAM_STR);
 		//$statement->bindParam(':campo28', $campo28, PDO::PARAM_STR);
 		$statement->bindParam(':campo29', $campo29, PDO::PARAM_STR);
 		$statement->bindParam(':campo30', $campo30, PDO::PARAM_STR);
 		$statement->bindParam(':campo31', $campo31, PDO::PARAM_STR);
 		$statement->bindParam(':campo32', $campo32, PDO::PARAM_STR);
 		
 		$guardada = $statement->execute();
 			
 		return $guardada ? 1 : 0;
 		
 	}
 	
 	
/** DELETE **/
}	
?>