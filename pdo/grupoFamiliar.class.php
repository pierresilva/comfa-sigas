<?php
set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");

$root= $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $_SESSION['RAIZ'] . DIRECTORY_SEPARATOR . '/rsc/pdo/IFXDbManejador.php';

class GrupoFamiliar{
	private $db;
	
/** CONSTRUCTOR **/
	function GrupoFamiliar(){
		$this->db = IFXDbManejador::conectarDB();
		if($this->db->conexionID==null){
			$cadena = $this->db->error;
			echo msg_error($cadena);
			exit();
		}
 	}
 	
 	public function inicioTransaccion(){
 		$this->db->conexionID->beginTransaction();
 	}
 	
 	public function cancelarTransaccion(){
 		$this->db->conexionID->rollBack();
 	}
 	
 	public function confirmarTransaccion(){
 		$this->db->conexionID->commit();
 	}
	
/** CREATE **/
 	
 	function guardarGrupoFamiliar($campos){
 		$sql="INSERT INTO aportes021 ( idtrabajador, idbeneficiario, idparentesco, idconyuge, conviven, fechaafiliacion 	  , fechaasignacion, giro, estado, tempo1, usuario, fechasistema 		  , idtiporelacion, postulacionfonede, embarga, idmotivo, fechaestado, idbiologico, fechagiro )
							  VALUES (:idtrabajador,:idbeneficiario,:idparentesco,:idconyuge,:conviven,CAST(GETDATE() AS DATE),:fechaasignacion,:giro,:estado,:tempo1,:usuario,CAST(GETDATE() AS DATE),:idtiporelacion,:postulacionfonede,:embarga,:idmotivo,:fechaestado,:idbiologico,:fechagiro )";
 			
 		$statement = $this->db->conexionID->prepare($sql);
 		$campos->usuario=$_SESSION['USUARIO'];
 		$guardada = false;
 			
 		$count=0;
 		$tmp = get_object_vars($campos);
 		foreach($tmp as $indice=>$valor){
 			if($count>0 && $count!=6 && $count!=12){
 				if(empty($valor)==true){ $statement->bindValue(":".$indice, null,  PDO::PARAM_STR);
 				} else { $statement->bindValue(":".$indice, $valor,  PDO::PARAM_STR); }
 			}
 			$count++;
 		}
 		$guardada = $statement->execute();
 		if($guardada){
 			$rs = $this->db->conexionID->lastInsertId();
 			return trim($rs);
 		}else{
 			return 0;
 		}
 	}
 	 	 	
/** READ **/
 	
 	/** Trae informacion basica de todos los(as) compaņeros(as) que ha notificado un afiliado **/
 	function buscarConyuges($idp){
 		$data = array(); $con = 0;
 		$sql = "SELECT DISTINCT a21.idbeneficiario idconyuge, a91.detalledefinicion, c91.codigo, a15.identificacion, a15.pnombre, a15.snombre, a15.papellido, a15.sapellido, a21.conviven, a21.estado, a21.fechaafiliacion
 		FROM aportes021 a21 INNER JOIN aportes091 a91 ON a91.iddetalledef=a21.idparentesco INNER JOIN aportes015 a15 ON a15.idpersona=a21.idbeneficiario INNER JOIN aportes091 c91 ON c91.iddetalledef=a15.idtipodocumento
 		WHERE a21.idtrabajador=$idp AND a21.idparentesco=34";
 		$rs = $this->db->querySimple($sql);
 		while($row = $rs->fetch()){
 			$con++;
 			$data[]=array_map("utf8_encode",$row);
 		}
 		if($con>0) {
 			return $data;
 		} else
 			return 0;
 	}
 	
 	/** Trae informacion basica de todos los(as) compaņeros(as) que ha notificado un afiliado **/
 	function buscarConyugeConvive($idp){
 		$data = array(); $con = 0;
 		$sql = "SELECT DISTINCT a21.idbeneficiario idconyuge, a91.detalledefinicion, c91.codigo, a15.identificacion, a15.pnombre, a15.snombre, a15.papellido, a15.sapellido, a21.conviven, a21.estado, a21.fechaafiliacion
				FROM aportes021 a21 INNER JOIN aportes091 a91 ON a91.iddetalledef=a21.idparentesco INNER JOIN aportes015 a15 ON a15.idpersona=a21.idbeneficiario INNER JOIN aportes091 c91 ON c91.iddetalledef=a15.idtipodocumento 
 				WHERE a21.idtrabajador=$idp AND a21.idparentesco=34 AND a21.conviven='S'";
 		$rs = $this->db->querySimple($sql);
 		while($row = $rs->fetch()){
	 		$con++;
	 		$data[]=array_map("utf8_encode",$row);
	 	}
	 	if($con>0) {
 			return $data;
 		} else
 			return 0;
 	}
 	
 	/** Trae informacion basica de todos los(as) compaņeros(as) que ha notificado un afiliado **/
 	function buscarBeneficiarios($idp){
 		$data = array(); $con = 0;
 		$sql = "SELECT DISTINCT a21.idbeneficiario idconyuge, a91.detalledefinicion
				,c91.codigo, a15.identificacion, a15.pnombre, a15.snombre, a15.papellido
				,a15.sapellido, isnull(a15.capacidadtrabajo,'N') capacidadtrabajo
				,a21.conviven, a21.estado, a21.embarga, a21.fechaafiliacion
				,isnull(a26.idcertificado,0) idcertificado
				FROM aportes021 a21 
				INNER JOIN aportes091 a91 ON a91.iddetalledef=a21.idparentesco 
				INNER JOIN aportes015 a15 ON a15.idpersona=a21.idbeneficiario 
				INNER JOIN aportes091 c91 ON c91.iddetalledef=a15.idtipodocumento
				LEFT JOIN aportes026 a26 ON a26.idcertificado=(SELECT TOP 1 b26.idcertificado FROM aportes026 b26 
				WHERE a21.idbeneficiario=b26.idbeneficiario ORDER BY b26.estado, b26.fechapresentacion DESC)
				WHERE a21.idtrabajador=$idp AND a21.idparentesco in (35,36,37,38)";
 		$rs = $this->db->querySimple($sql);
 		while($row = $rs->fetch()){
 			$con++;
 			$data[]=array_map("utf8_encode",$row);
 		}
 		if($con>0) {
 			return $data;
 		} else
 			return 0;
 	}
 	
 	function buscarBeneficiariosTodos($idp){
 		$data = array(); $con = 0;
 		$sql = "SELECT DISTINCT a21.idbeneficiario, a21.idconyuge, a91.detalledefinicion parentesco, c91.codigo, a15.identificacion
		 		,a15.pnombre, a15.snombre, a15.papellido, a15.sapellido, a15.fechanacimiento, a21.idparentesco
		 		,a21.giro, a21.fechaasignacion, isnull(a15.capacidadtrabajo,'N') capacidadtrabajo, a21.conviven
		 		,a21.estado,a21.embarga, a21.fechaafiliacion, isnull(a26.idcertificado,0) idcertificado
		 		FROM aportes021 a21
		 		INNER JOIN aportes091 a91 ON a91.iddetalledef=a21.idparentesco
		 		INNER JOIN aportes015 a15 ON a15.idpersona=a21.idbeneficiario
		 		INNER JOIN aportes091 c91 ON c91.iddetalledef=a15.idtipodocumento
		 		LEFT JOIN aportes026 a26 ON a26.idcertificado=(SELECT TOP 1 b26.idcertificado FROM aportes026 b26 WHERE a21.idbeneficiario=b26.idbeneficiario ORDER BY b26.estado, b26.fechapresentacion DESC)
		 		WHERE a21.idtrabajador=$idp AND a21.idparentesco in (35,36,37,38)";
 		$rs = $this->db->querySimple($sql);
 		while($row = $rs->fetch()){
 			$con++;
 			$data[]=array_map("utf8_encode",$row);
 		}
 		if($con>0) {
 			return $data;
 		} else
 			return 0;
 	}
 	
 	function buscarBeneficiariosActivos($idp){
 		$data = array(); $con = 0;
 		$sql="SELECT  a21.idbeneficiario, a21.idconyuge, a15.identificacion
	 			,a15.pnombre,a15.snombre, a15.papellido, a15.sapellido, a15.fechanacimiento
	 			,isnull(a15.capacidadtrabajo,'N') capacidadtrabajo, a21.idparentesco, a21.giro, a21.fechaasignacion
	 			,a21.embarga, a91.detalledefinicion parentesco, c15.identificacion as identificacionC
	 			,c15.pnombre as pnombreC, c15.snombre as snombreC, c15.papellido as papellidoC
	 			,c15.sapellido as sapellidoC, isnull(a26.idcertificado,0) idcertificado
		 	  FROM aportes021 a21 
		 	  INNER JOIN aportes015 a15 ON a15.idpersona = a21.idbeneficiario 
		 	  INNER JOIN aportes091 a91 ON a91.iddetalledef = a21.idparentesco 
		 	  LEFT JOIN aportes015 c15 ON c15.idpersona = a21.idconyuge
		 	  LEFT JOIN aportes026 a26 ON a26.idcertificado=(SELECT TOP 1 b26.idcertificado FROM aportes026 b26 WHERE a21.idbeneficiario=b26.idbeneficiario ORDER BY b26.estado, b26.fechapresentacion DESC)
		 	  WHERE a21.idtrabajador=$idp AND a21.idparentesco IN (35,36,37,38) AND a21.estado='A' AND a15.estado<>'M' ORDER BY a15.fechanacimiento DESC";
 		$rs = $this->db->querySimple($sql);
 		while($row = $rs->fetch()){
 		$con++;
 		$data[]=array_map("utf8_encode",$row);
 		}
 		if($con>0) {
 		return $data;
 	} else
 			return 0;
 		}
 	
 	/** Trae informacion basica de todos los(as) compaņeros(as) que ha notificado un afiliado **/
 	function buscarBasicoConyuges($idp){
 		$data = array(); $con = 0;
 		$sql = "SELECT a21.idbeneficiario,b91.codigo,a15.identificacion,a15.pnombre,a15.snombre,a15.papellido,a15.sapellido,a21.conviven,a15.fechanacimiento,a21.fechasistema,
 				a15.sexo,a21.estado estadoRelacion,a91.detalledefinicion,CASE WHEN a16.estado IS NULL THEN 'N' ELSE 'S' END labora, a16.salario,a16.fechaingreso,a21.idrelacion,a21.idconyuge
				FROM aportes021 a21 INNER JOIN aportes015 a15 ON a21.idbeneficiario=a15.idpersona LEFT JOIN aportes016 a16 
					ON a16.idformulario=(SELECT TOP 1 b16.idformulario FROM aportes016 b16 WHERE a21.idbeneficiario=b16.idpersona AND b16.estado='A' ORDER BY b16.fechaingreso)
  				INNER JOIN aportes091 a91 ON a21.idparentesco=a91.iddetalledef INNER JOIN aportes091 b91 ON b91.iddetalledef=a15.idtipodocumento WHERE a21.idparentesco=34 AND a21.idtrabajador=$idp"; 		
 		$rs = $this->db->querySimple($sql);
 		while($row = $rs->fetch()){
 			$con++;
 			$data[]=array_map("utf8_encode",$row);
 		}
 		if($con>0) {
 			return $data;
 		} else
 			return 0;
 	}
 	
	/*** consulta del grupo familiar ****/
 	function buscarGrupoFamiliar($idp){
 		$sql="SELECT idrelacion, idbeneficiario, idparentesco, idconyuge, conviven, aportes021.fechaafiliacion, fechaasignacion, giro,aportes021.estado,embarga,fechaestado,aportes015.idtipodocumento,aportes015.identificacion,aportes015.papellido,aportes015.sapellido,aportes015.pnombre,aportes015.snombre
		FROM aportes021 INNER JOIN aportes015 ON aportes021.idbeneficiario=aportes015.idpersona WHERE idtrabajador=$idp AND idparentesco IN (35,36,37,38)";
 		$rs=$this->db->conexionID->query($sql);
 		$filas = array();
 		while ($row=$rs->fetch()){
 			$filas[] = $row;
 		}
 		if (count($filas) == 0)  
 			echo 0; 
 		else  
 			echo $filas; 
 	} 	
 	
 	/**consulta las relaciones existentes de un CONYUGE**/
 	function relacionesPorConyuge($idc){
 		$sql="SELECT idrelacion, idtrabajador, idbeneficiario, idparentesco, idconyuge, conviven, idtiporelacion,(pnombre + ' ' + papellido) AS nombre,identificacion FROM aportes021 a21 INNER JOIN aportes015 ON a21.idtrabajador=aportes015.idpersona WHERE a21.idparentesco=34 AND a21.idconyuge = $idc";
 		$rs=$this->db->conexionID->query($sql);
 		return $rs;
 	}
 	
 	/** Trae informacion basica de el(la) compaņero(a) actual **/
 	function buscarActualConyuge($idp){
 		$data = array(); $con = 0;
 		$sql = "SELECT DISTINCT a21.fechaafiliacion,a15.identificacion,a15.papellido,a15.sapellido,a15.pnombre,a15.snombre,a21.conviven,a21.idparentesco,a21.estado,a21.idconyuge,a15.idpersona FROM aportes021 a21 INNER JOIN aportes015 a15 on a21.idconyuge=a15.idpersona WHERE a21.conviven='S' AND a21.idparentesco=34 AND a21.estado='A' AND a21.idtrabajador=$idp";
 		$rs = $this->db->querySimple($sql);
 		while($row = $rs->fetch()){
 		$con++;
 		$data[]=array_map("utf8_encode",$row);
 	}
 	if($con>0) {
 		return $data[0];
 		} else
 			return 0;
 	}
 	
 	function contarExisteRelacion($idp,$idb){
 		$sql="SELECT COUNT(*) cuenta, MAX(conviven) conviven FROM aportes021 WHERE idtrabajador=$idp AND idbeneficiario=$idb";
 		$cuenta = $this->db->conexionID->query($sql)->fetch(PDO::FETCH_OBJ)->cuenta;
 		return $cuenta;
 	}
 	
 	/** Trae informacion basica de todos los(as) compaņeros(as) que ha notificado un afiliado **/
 	function buscarBasicoGrupoFamiliar($idp){
 		$data = array(); $con = 0;
 		$sql = "SELECT a21.idrelacion, a21.idbeneficiario,td.codigo,b15.identificacion identificacionBeneficiario,b15.pnombre,b15.snombre,b15.papellido,b15.sapellido,a21.idparentesco,a91.detalledefinicion parentesco,
						b15.estado estadoBeneficiario,CASE WHEN b15.estado='N' THEN b15.fechasistema ELSE b15.fechadefuncion END fechaEstadoBeneficiario,b15.fechanacimiento,
						CAST(dateDiff(day, b15.fechanacimiento,getDate())/365.25 AS DECIMAL(4,2)) as edad,a21.fechaafiliacion,a26.fechapresentacion,a21.fechaasignacion,c15.identificacion identificacionConyuge,
						a21.giro,CASE WHEN b15.capacidadtrabajo='I' THEN 'S' ELSE 'N' END discapacitado,CASE WHEN a21.embarga='S' THEN 'S' ELSE 'N' END embarga,a21.estado estadoAfiliacion,
						CASE WHEN a21.fechaestado IS NULL OR a21.fechaestado='19000101' THEN a21.fechasistema ELSE a21.fechaestado END fechaEstadoAfiliacion, b91.detalledefinicion motivoInactivo,b15.tipoestado						
				FROM aportes021 a21 INNER JOIN aportes015 b15 ON a21.idbeneficiario=b15.idpersona INNER JOIN aportes091 a91 ON a21.idparentesco=a91.iddetalledef LEFT JOIN aportes026 a26
				    ON a26.idcertificado=(SELECT TOP 1 b26.idcertificado FROM aportes026 b26 WHERE a21.idbeneficiario=b26.idbeneficiario AND b26.estado='A' ORDER BY b26.fechapresentacion DESC)
				  LEFT JOIN aportes015 c15 ON a21.idconyuge=c15.idpersona LEFT JOIN aportes091 b91 ON a21.estado='I' AND a21.idmotivo=b91.iddetalledef LEFT JOIN aportes091 td ON b15.idtipodocumento = td.iddetalledef WHERE a21.idtrabajador=$idp AND a21.idparentesco IN(35,36,37,38) ORDER BY a21.idparentesco";
 		$rs = $this->db->querySimple($sql);
 		while($row = $rs->fetch()){
 		$con++;
 		$data[]=array_map("utf8_encode",$row);
 	}
 	if($con>0) {
 		return $data;
 		} else
 			return 0;
 	}
 	
 	function contarRelacionesActivas($idb){
 		$sql="Select count(*) as cuenta from aportes021 where estado='A' and idbeneficiario = $idb";
 		$cuenta = $this->db->conexionID->query($sql)->fetch(PDO::FETCH_OBJ)->cuenta;
 		return $cuenta;
 	} 	
 	
/** UPDATE **/
 	
 	function updateInactivarBeneficiarioHijastro($campos){
 		$sql="UPDATE aportes021 SET estado=:estado, usuario=:usuario, idmotivo=:idmotivo, fechaestado=CAST(GETDATE() AS DATE) WHERE idtrabajador=:idtrabajador AND idconyuge=:idconyuge AND idparentesco=:idparentesco";
 		$statement = $this->db->conexionID->prepare($sql);
 		$campos->usuario=$_SESSION['USUARIO'];
 		$guardada = false;
 		
 		$count=0;
 		$tmp = get_object_vars($campos);
 		foreach($tmp as $indice=>$valor){
 			if($count==1 || $count==3 || $count==4 || $count==9 || $count==11 || $count==16){
 				if(empty($valor)==true){ $statement->bindValue(":".$indice, null,  PDO::PARAM_STR);
 				} else { $statement->bindValue(":".$indice, $valor,  PDO::PARAM_STR); }
 			}
 			$count++;
 		}
 		$guardada = $statement->execute();
 	
 		return $guardada ? 1 : 0;
 	}
 	
 	function updateCrearConvivencia($campos){
 		$sql="UPDATE aportes021 SET conviven=:conviven, idtiporelacion=:idtiporelacion, usuario=:usuario, fechasistema=CAST(GETDATE() AS DATE) WHERE idtrabajador=:idtrabajador AND idbeneficiario=:idbeneficiario";
 		$statement = $this->db->conexionID->prepare($sql);
 		$campos->usuario=$_SESSION['USUARIO'];
 		$guardada = false;
 			
 		$count=0;
 		$tmp = get_object_vars($campos);
 		foreach($tmp as $indice=>$valor){
 			if($count==1 || $count==2 || $count==5 || $count==11 || $count==13){
 				if(empty($valor)==true){
 					$statement->bindValue(":".$indice, null,  PDO::PARAM_STR);
 				} else { $statement->bindValue(":".$indice, $valor,  PDO::PARAM_STR);
 				}
 			}
 			$count++;
 		}
 		$guardada = $statement->execute();
 	
 		return $guardada ? 1 : 0;
 	}
 	
 	function updateDisolverConvivencia($campos){
 		$sql="UPDATE aportes021 SET conviven=:conviven, usuario=:usuario, fechasistema=CAST(GETDATE() AS DATE) WHERE idtrabajador=:idtrabajador AND idbeneficiario=:idbeneficiario";
 		$statement = $this->db->conexionID->prepare($sql);
 		$campos->usuario=$_SESSION['USUARIO'];
 		$guardada = false;
 		
 		$count=0;
 		$tmp = get_object_vars($campos);
 		foreach($tmp as $indice=>$valor){
 			if($count==1 || $count==2 || $count==5 || $count==11){
 				if(empty($valor)==true){ $statement->bindValue(":".$indice, null,  PDO::PARAM_STR);
 				} else { $statement->bindValue(":".$indice, $valor,  PDO::PARAM_STR); }
 			}
 			$count++;
 		}
 		$guardada = $statement->execute();
 	
 		return $guardada ? 1 : 0;
 	}
 	
 	function updateGrupoFamiliar($campos){
 		$sql="UPDATE aportes021 SET idparentesco = :idparentesco, idconyuge = :idconyuge, conviven = :conviven, giro = :giro, estado = :estado, tempo1 = :tempo1, usuario = :usuario, fechasistema = CAST(GETDATE() AS DATE), idtiporelacion = :idtiporelacion, postulacionfonede = :postulacionfonede, embarga = :embarga, idmotivo = :idmotivo, fechaestado = :fechaestado, idbiologico = :idbiologico, fechagiro = :fechagiro WHERE idtrabajador = :idtrabajador AND idbeneficiario = :idbeneficiario";
 		$statement = $this->db->conexionID->prepare($sql);
 		$campos->usuario=$_SESSION['USUARIO'];
 		$guardada = false;
 		 	
 		$count=0;
 		$tmp = get_object_vars($campos);
 		foreach($tmp as $indice=>$valor){
 			if($count>0 && $count!=6 && $count!=7 && $count!=12){
 				if(empty($valor)==true){ $statement->bindValue(":".$indice, null,  PDO::PARAM_STR);
 				} else { $statement->bindValue(":".$indice, $valor,  PDO::PARAM_STR); }
 			}
 			$count++;
 		}
 		$guardada = $statement->execute();		
 		
 		return $guardada ? 1 : 0;
 	}
 	
/** DELETE **/
}	
?>