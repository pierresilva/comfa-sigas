<?php
set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");

$root= $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $_SESSION['RAIZ'] . DIRECTORY_SEPARATOR . '/rsc/pdo/IFXDbManejador.php';

class Embargo{
	private $db;
	
/** CONSTRUCTOR **/
	function Embargo(){
		$this->db = IFXDbManejador::conectarDB();
		if($this->db->conexionID==null){
			$cadena = $this->db->error;
			echo msg_error($cadena);
			exit();
		}
 	}
	
/** CREATE **/
 	 	 	
/** READ **/
 	 	
 	function buscarCantidadEmbargos($idp){
 		$con=0;
 		$sql="select count(*) as cant from aportes018 WHERE idtrabajador = $idp";
 		$rs = $this->db->querySimple($sql);
 		while($row = $rs->fetch()){
 			$con=$row['cant'];
 		}
 		return $con;
 	}
 	
 	function buscarEmbargos($idp){
 		$data = array(); $con = 0;
 		$sql = "SELECT  idembargo,idtercero,fechaembargo, aportes018.estado,notas,codigopago, identificacion, papellido, sapellido, pnombre, snombre FROM aportes018 INNER JOIN aportes015 ON aportes018.idtercero = aportes015.idpersona WHERE idtrabajador = $idp";
 		$rs = $this->db->querySimple($sql);
 		while($row = $rs->fetch()){
 		$con++;
 		$data[]=array_map("utf8_encode",$row);
	 	}
	 	if($con>0) {
	 		return $data;
	 	} else
	 			return 0;
 	}
 	
 	function buscarBeneficiarioEmbargo($ide){
 		$data = array(); $con = 0;
 		$sql = "SELECT DISTINCT aportes022.idbeneficiario,pnombre,snombre,papellido,sapellido,aportes021.fechaafiliacion,aportes021.giro,aportes021.estado,aportes091.detalledefinicion FROM aportes022
				INNER JOIN aportes018 ON(aportes018.idembargo=aportes022.idembargo) 
				INNER JOIN aportes015 ON aportes022.idbeneficiario=aportes015.idpersona
				INNER JOIN aportes021 ON aportes022.idbeneficiario=aportes021.idbeneficiario
				INNER JOIN aportes091 ON aportes021.idparentesco=aportes091.iddetalledef
				WHERE aportes018.idembargo=$ide AND aportes021.idtrabajador=aportes018.idtrabajador";
 		$rs = $this->db->querySimple($sql);
 		while($row = $rs->fetch()){
 			$con++;
 			$data[]=array_map("utf8_encode",$row);
 		}
 		if($con>0) {
 			return $data;
 		} else
 			return 0;
 	}
 	
/** UPDATE **/
 	
/** DELETE **/
}	
?>