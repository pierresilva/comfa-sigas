<?php
set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");

$root= $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $_SESSION['RAIZ'] . DIRECTORY_SEPARATOR . '/rsc/pdo/IFXDbManejador.php';

class Beca{
	private $db;
	
/** CONSTRUCTOR **/
	function Beca(){
		$this->db = IFXDbManejador::conectarDB();
		if($this->db->conexionID==null){
			$cadena = $this->db->error;
			echo msg_error($cadena);
			exit();
		}
 	}
	
/** CREATE **/
 	 	 	
/** READ **/
 	
 	/** Trae informacion basica de todos los certificados de una persona **/
 	function buscarBasicoBecas($idp){
 		$data = array(); $con = 0;
 		$sql = "SELECT idbeneficiario,grado,aportes015.pnombre,aportes015.snombre,aportes015.papellido,aportes015.sapellido, dateDiff(day, aportes015.fechanacimiento, getDate())/365 AS edad, aportes048.razonsocial FROM aportes075 INNER JOIN aportes015 ON aportes075.idbeneficiario = aportes015.idpersona INNER JOIN aportes048 ON aportes075.idcolegio=aportes048.idempresa WHERE idafiliado=$idp"; 		
 		$rs = $this->db->querySimple($sql);
 		while($row = $rs->fetch()){
 			$con++;
 			$data[]=array_map("utf8_encode",$row);
 		}
 		if($con>0) {
 			return $data;
 		} else
 			return 0;
 	}
 	
/** UPDATE **/
 	
/** DELETE **/
}	
?>