<?php	
	set_time_limit(0);
	ini_set("display_errors",'1');
	date_default_timezone_set("America/Bogota");
	$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
	include_once  $root;
	include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';
	
	//Obtener la fecha de la version
	$fecver = date('Ymd h:i:s A',filectime('regisNotifUsuar.frm.php'));
		
	$usuario = $_SESSION["USUARIO"];
	$idUsuario = $_SESSION["ID_USUARIO"];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<title>::Notificacion | Configurar::</title>
		
		<link type="text/css" href="<?php echo URL_PORTAL; ?>newcss/Estilos.css" rel="stylesheet">
		<link type="text/css" href="<?php echo URL_PORTAL; ?>newcss/css.1.8/jquery-ui-1.8.17.custom.css" rel="stylesheet">
		<link type="text/css" href="<?php echo URL_PORTAL; ?>newcss/marco.css" rel="stylesheet">
		
		<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/1.6/jquery-1.6.2.min.js"></script>
		<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/1.6/jquery-ui-1.8.16.custom.min.js"></script>
		<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/comunes.js"></script>
		<script type="text/javascript" src="js/regisNotifUsuar.js"></script>
		
	</head>
	<body>
		<table width="75%" border="0" align="center" cellpadding="0" cellspacing="0">
			<tbody>
			  	<!-- ESTILOS SUPERIOR TABLA-->
			  	<tr>
				    <td width="13" height="29" class="arriba_iz">&nbsp;</td>
				    <td class="arriba_ce">
				    	<span class="letrablanca">::&nbsp;Registro Notificaci&oacute;n Usuario::</span>
				    	<div style="text-align:right;float:right;">
								<?php echo 'Versi&oacute;n: ' . $fecver; ?>
								<input type="hidden" name="hidIdUsuario" id="hidIdUsuario" value="<?php echo $idUsuario;?>"/>
						</div>
				    </td>
				    <td width="13" class="arriba_de" align="right">&nbsp;</td>
			  	</tr> 
			  	<!-- ESTILOS ICONOS TABLA-->
			  	<tr>
			    	<td class="cuerpo_iz">&nbsp;</td>
			    	<td class="cuerpo_ce">
						<img width="3" height="1" src="../imagenes/spacer.gif" />     
			    	</td>
			    	<td class="cuerpo_de">&nbsp;</td>
			  	</tr>
			  	<!-- ESTILOS MEDIO TABLA-->
			  	<tr>
			    	<td class="cuerpo_iz">&nbsp;</td>
			    	<td class="cuerpo_ce">			    		
			    	</td>
			    	<td class="cuerpo_de">&nbsp;</td>
			  	</tr>  
			  	<!-- CONTENIDO TABLA-->
			  	<tr>
			   		<td class="cuerpo_iz">&nbsp;</td>
			   		<td class="cuerpo_ce">
						<table cellspacing="0" width="90%" border="0" class="tablero" align="center">
							<thead>
								<tr>
									<td>Enunciado</td>
									<td>Descripcion</td>
									<td>Fecha</td>
									<td>Visto</td>									
								</tr>
							</thead>
			      			<tbody id="tbNotificacion">&nbsp;</tbody>
			      		</table>
					</td>
		    		<td class="cuerpo_de">&nbsp;</td>
	  			</tr>		  	  
		  		<!-- ESTILOS PIE TABLA-->
		  		<tr>
		    		<td class="abajo_iz" >&nbsp;</td>
		    		<td class="abajo_ce" ></td>
		    		<td class="abajo_de" >&nbsp;</td>
		  		</tr>		  
			</tbody>
		</table>
	</body>
</html>