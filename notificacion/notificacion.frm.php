<?php	
	set_time_limit(0);
	ini_set("display_errors",'1');
	date_default_timezone_set("America/Bogota");
	$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
	include_once  $root;
	include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';
	
	//Obtener la fecha de la version
	$fecver = date('Ymd h:i:s A',filectime('notificacion.frm.php'));
		
	$usuario = $_SESSION["USUARIO"];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<title>::Notificacion | Configurar::</title>
		
		<link type="text/css" href="<?php echo URL_PORTAL; ?>newcss/Estilos.css" rel="stylesheet">
		<link type="text/css" href="<?php echo URL_PORTAL; ?>newcss/css.1.8/jquery-ui-1.8.17.custom.css" rel="stylesheet">
		<link type="text/css" href="<?php echo URL_PORTAL; ?>newcss/marco.css" rel="stylesheet">
		
		<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/1.6/jquery-1.6.2.min.js"></script>
		<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/1.6/jquery-ui-1.8.16.custom.min.js"></script>
		<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/comunes.js"></script>
		<script type="text/javascript" src="js/notificacion.js"></script>
	</head>
	<body>
		<table width="75%" border="0" align="center" cellpadding="0" cellspacing="0">
			<tbody>
			  	<!-- ESTILOS SUPERIOR TABLA-->
			  	<tr>
				    <td width="13" height="29" class="arriba_iz">&nbsp;</td>
				    <td class="arriba_ce">
				    	<span class="letrablanca">::&nbsp;Notificaci&oacute;n::</span>
				    	<div style="text-align:right;float:right;">
								<?php echo 'Versi&oacute;n: ' . $fecver; ?>
						</div>
				    </td>
				    <td width="13" class="arriba_de" align="right">&nbsp;</td>
			  	</tr> 
			  	<!-- ESTILOS ICONOS TABLA-->
			  	<tr>
			    	<td class="cuerpo_iz">&nbsp;</td>
			    	<td class="cuerpo_ce">
						<img src="../imagenes/menu/nuevo.png" name="iconNuevo" width="16" height="16" id="btnNuevo" style="cursor:pointer" title="Nuevo" onClick="nuevaNotificacion();" />
						<img width="3" height="1" src="../imagenes/spacer.gif" />
						<img src="../imagenes/menu/grabar.png" name="iconGuardar" width="16"  height=16 id="btnGuardar" style="cursor:pointer" title="Guardar" onClick="guardar();" />
						<img width="3" height="1" src="../imagenes/spacer.gif" />
						<img src="../imagenes/menu/modificar.png" name="iconActualizar" width="16"  height=16 id="btnActualizar" style="cursor:pointer;display:none;" title="Actualizar" onClick="actualizar();" />
						<img width="3" height="1" src="../imagenes/spacer.gif" />
						<img src="../imagenes/menu/visitas.png" width="16" height="16" title="Buscar" style="cursor: pointer" id="btnBuscar" name="btnBuscar"  onclick="buscar();">     
			    	</td>
			    	<td class="cuerpo_de">&nbsp;</td>
			  	</tr>
			  	<!-- ESTILOS MEDIO TABLA-->
			  	<tr>
			    	<td class="cuerpo_iz">&nbsp;</td>
			    	<td class="cuerpo_ce">
			    	
			    		<input type="hidden" name="hidIdNotificacion" id="hidIdNotificacion"/>
			    		
			    	</td>
			    	<td class="cuerpo_de">&nbsp;</td>
			  	</tr>  
			  	<!-- CONTENIDO TABLA-->
			  	<tr>
			   		<td class="cuerpo_iz">&nbsp;</td>
			   		<td class="cuerpo_ce">
						<table cellspacing="0" width="90%" border="0" class="tablero" align="center">
			      			<tbody>
			      				<tr>
			      					<th colspan="2">Datos Generales</th>
				        		</tr>
				        		<tr>
				        			<td>Codigo</td>
				        			<td>
				        				<input type="text" name="txtCodigo" id="txtCodigo" class="box2 element-required"/>
				        				<img src="../imagenes/menu/obligado.png" alt="" width="12" height="12" />
				        			</td>
				        		</tr>
				        		<tr>
				        			<td>Enunciado</td>
				        			<td>
				        				<input type="text" name="txtEnunciado" id="txtEnunciado" class="box2 element-required"/>
				        				<img src="../imagenes/menu/obligado.png" alt="" width="12" height="12" />
				        			</td>
				        		</tr>
				        		<tr>
				        			<td>Descripci&oacute;n</td>
				        			<td>
				        				<textarea name="txaDescripcion" id="txaDescripcion" rols="5" cols="60" class="box2 element-required"></textarea>
				        				<img src="../imagenes/menu/obligado.png" alt="" width="12" height="12" />
				        			</td>
				        		</tr>
				        		<tr>
				        			<td>Query Consulta</td>
				        			<td>
				        				<textarea name="txaQuery" id="txaQuery" rols="5" cols="60" class="box2 element-required"></textarea>
				        				<img src="../imagenes/menu/obligado.png" alt="" width="12" height="12" />
				        			</td>
				        		</tr>
				        		<tr>
				        			<td>Uri</td>
				        			<td>
				        				<input type="text" name="txtUri" id="txtUri" class="box2 element-required"/>
				        				<img src="../imagenes/menu/obligado.png" alt="" width="12" height="12" />
				        			</td>
				        		</tr>
				        		<tr>
				        			<td>Tiempo Expiraci&oacute;n (# DIAS)</td>
				        			<td>
				        				<input type="text" name="txtTiempoExpiracion" id="txtTiempoExpiracion" class="box2 element-required"/>
				        				<img src="../imagenes/menu/obligado.png" alt="" width="12" height="12" />
				        			</td>
				        		</tr>
				        		<tr>
				        			<td>Estado</td>
				        			<td>
				        				<select name="cmbEstadoNotificacion" id="cmbEstadoNotificacion" class="box2 element-required">
				        					<option value="A">Activo</option>
				        					<option value="I">Inactivo</option>
				        				</select>
				        				<img src="../imagenes/menu/obligado.png" alt="" width="12" height="12" />
				        			</td>
				        		</tr>
				        		<tr>
				        			<th colspan="2">D&iacute;as de Ejecuci&oacute;n</th>
				        		</tr>
				        		<tr>
				        			<td colspan="2" style="text-align: center;">
					        			<input type="checkbox" name="chkDiaEjecucion" id="chkDomingo" value="1"/>
					        			Domingo
					        			<input type="checkbox" name="chkDiaEjecucion" id="chkLunes" value="2"/>
					        			Lunes
					        			<input type="checkbox" name="chkDiaEjecucion" id="chkMartes" value="3"/>
					        			Martes
					        			<input type="checkbox" name="chkDiaEjecucion" id="chkMiercoles" value="4"/>
					        			Miercoles
					        			<input type="checkbox" name="chkDiaEjecucion" id="chkJueves" value="5"/>
					        			Jueves
					        			<input type="checkbox" name="chkDiaEjecucion" id="chkViernes" value="6"/>
					        			Viernes
					        			<input type="checkbox" name="chkDiaEjecucion" id="chkSabado" value="7"/>
					        			Sabado
					        		</td>
				        		</tr>
				        		<tr>
				        			<th colspan="2">Tiempos de Ejecucion (HORA/formato:0-23)</th>
				        		</tr>
				        		<tr>
				        			<td colspan="2">
				        				<table style="width:25%; margin:auto;" id="tblTiempoEjecucion" border="0" class="tablero" >
							      			<thead>
								         		<tr>
								         			<th>&nbsp;</th>
								         			<th>Hora</th>
								         			<th>Estado</th>
								         		</tr>
								         	</thead>
								         	<tbody>
							         		</tbody>
							         		<tfoot>
							         			<tr>
							         				<td colspan="9" style="text-align: center;">
							         					<input type="button" name="btnAddTrHoraEjecu" id="btnAddTrHoraEjecu" value="Adicionar Hora" onclick="addTrHoraEjecu();"/>
							         				</td>
							         			</tr>
							         		</tfoot>
						    			</table>
				        			</td>
				        		</tr>
				        		<tr>
				        			<th colspan="2">Medio de Difusi&oacute;n</th>
				        		</tr>
				        		<tr>
				        			<td colspan="2">
				        				<table style="width:70%; margin:auto;" id="tblMedioDifusion" border="0" class="tablero" >
								         	<tbody>
							         		</tbody>
						    			</table>
				        			</td>
				        		</tr>
				        		<tr>
				        			<th colspan="2">Rol</th>
				        		</tr>
				        		<tr>
				        			<td colspan="2">
				        				<table style="width:70%; margin:auto;" id="tblRol" border="0" class="tablero" >
								         	<tbody>
							         		</tbody>
						    			</table>
				        			</td>
				        		</tr>
			      			</tbody>
			      		</table>
					</td>
		    		<td class="cuerpo_de">&nbsp;</td>
	  			</tr>		  	  
		  		<!-- ESTILOS PIE TABLA-->
		  		<tr>
		    		<td class="abajo_iz" >&nbsp;</td>
		    		<td class="abajo_ce" ></td>
		    		<td class="abajo_de" >&nbsp;</td>
		  		</tr>		  
			</tbody>
		</table>
		
		<div id="divBuscarNotificacion" style="display:none;" title="NOTIFICACIONES | BUSCAR">
			<table style="width:95%; margin:auto;" id="tblBuscarNotificacion" border="0" class="tablero" >
      			<thead>
	         		<tr>
	         			<th>Codigo</th>
	         			<th>Enunciado</th>
	         		</tr>
	         	</thead>
	         	<tbody>
         		</tbody>
    		</table>
		</div>
		
	</body>
</html>