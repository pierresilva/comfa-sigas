<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once 'clases'.DIRECTORY_SEPARATOR.'notificacion.class.php';

//Parametros recibidos
$objDatosFiltro = isset($_REQUEST['objDatosFiltro'])?$_REQUEST['objDatosFiltro']:null;

$objNotificacion = new Notificacion;

$arrResultado = array("error"=>0,"data"=>null);

$consultaNotificacion = $objNotificacion->select_simpl_notificacion($objDatosFiltro);

foreach( $consultaNotificacion as $rowNotificacion){
	$arrBanderaResultado = array("datos_generales"=>null,"tiempo_ejecucion"=>null,"medio_difusion"=>null,"rol"=>null);
	$arrBanderaFiltro = null;
	
	//Add notificacion
	$arrBanderaResultado["datos_generales"] = $rowNotificacion;
	
	//Add tiempo ejecucion
	$arrBanderaFiltro = array("id_notificacion"=>$rowNotificacion["id_notificacion"]);
	$arrBanderaResultado["tiempo_ejecucion"] = $objNotificacion->select_simpl_hora_notif($arrBanderaFiltro);
		
	//Add medios de difusion
	$arrBanderaResultado["medio_difusion"] = $objNotificacion->select_simpl_notif_medio_difus($arrBanderaFiltro);
	
	//Add roles
	$arrBanderaResultado["rol"] = $objNotificacion->select_simpl_notif_rol($arrBanderaFiltro);
		
	$arrResultado["data"][$rowNotificacion["id_notificacion"]] = $arrBanderaResultado;
}

echo (count($arrResultado)>0) ? json_encode($arrResultado) : 0;
?>