<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once 'clases'.DIRECTORY_SEPARATOR.'notificacion.class.php';

//Parametros recibidos
$objDatosFiltro = $_REQUEST['objDatosFiltro'];

$objNotificacion = new Notificacion;

if($objDatosFiltro["accion"]=="NEW"){
	$consulta=$objNotificacion->select_compl_notif_usuar_new($objDatosFiltro);
}else if($objDatosFiltro["accion"]=="ALL"){
	$consulta=$objNotificacion->select_compl_notif_usuar_all($objDatosFiltro);
}
echo (count($consulta)>0) ? json_encode($consulta) : 0;
?>