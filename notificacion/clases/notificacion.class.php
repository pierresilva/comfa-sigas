<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';

include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'utilitario.class.php';

class Notificacion {
	/************************************
	 *  ATRIBUTOS ENTIDADES			    *
	***********************************/
	private static $con = null;
	
	function __construct(){
		try{
			self::$con = IFXDbManejador::conectarDB();
			if( self::$con->conexionID==null ){
				throw new Exception(self::$con->error);
			}
		}catch(Exception $e){
			echo $e->getMessage();
			exit();
		}
	}
	
	function inicioTransaccion(){
		self::$con->inicioTransaccion();
	}
	function cancelarTransaccion(){
		self::$con->cancelarTransaccion();
	}
	function confirmarTransaccion(){
		self::$con->confirmarTransaccion();
	}
	
	/************************************
	 *  INSERT 						    *
	 ***********************************/
	/**
	 * Guardar las notificaciones <br/>
	 * Retorna 0 o el id del registro insertado
	 * 
	 * @param unknown_type $datos
	 * @return number: 0|1
	 */
	function insert_notificacion($datos, $usuario){
		$sql = "INSERT INTO dbo.aportes530 (codigo,enunciado,descripcion,query,uri,dia_ejecucion,tiempo_expiracion,estado,usuario_creacion)
					VALUES ('{$datos["codigo"]}','{$datos["enunciado"]}',:descripcion,:query,:uri,'{$datos["dia_ejecucion"]}',{$datos["tiempo_expiracion"]},'{$datos["estado"]}','$usuario')";
		
		$statement = self::$con->conexionID->prepare($sql);
		$statement->bindValue(":descripcion", $datos["descripcion"],  PDO::PARAM_STR);
		$statement->bindValue(":query", $datos["query"],  PDO::PARAM_STR);
		$statement->bindValue(":uri", $datos["uri"],  PDO::PARAM_STR);
		$guardada = $statement->execute();
		
		$idRegistro = 0;
		if($guardada!=false){
			$idRegistro = self::$con->conexionID->lastInsertId("aportes530");
		}
		
		return $idRegistro;
	}

	/**
	 * Guardar las horas en las cuales se va ha notificar <br/>
	 * Retorna 0 o el id del registro insertado
	 * 
	 * @param unknown_type $datos
	 * @return number: 0|1
	 */
	function insert_hora_notif($datos, $usuario){
		$sql = "INSERT INTO dbo.aportes531 (hora_ejecucion,fecha_ulima_ejecu,id_notificacion,estado,usuario_creacion)
					VALUES ({$datos["hora_ejecucion"]},'{$datos["fecha_ulima_ejecu"]}',{$datos["id_notificacion"]},'{$datos["estado"]}','$usuario')";
		
		$statement = self::$con->conexionID->prepare($sql);
		$guardada = $statement->execute();
		
		$idRegistro = 0;
		if($guardada!=false){
			$idRegistro = self::$con->conexionID->lastInsertId("aportes531");
		}
		
		return $idRegistro;
	}
	
	/**
	 * Guardar medio de difusion para las notificaciones <br/>
	 * Retorna 0 o el id del registro insertado
	 *
	 * @param unknown_type $datos
	 * @return number: 0|1
	 */
	function insert_medio_difus($datos, $usuario){
		$sql = "INSERT INTO dbo.aportes532 (codigo,	descripcion, estado, usuario_creacion)
					VALUES ('{$datos["codigo"]}',:descripcion,'{$datos["estado"]}','$usuario')";
	
		$statement = self::$con->conexionID->prepare($sql);
		$statement->bindValue(":descripcion", $datos["descripcion"],  PDO::PARAM_STR);
		$guardada = $statement->execute();
	
		$idRegistro = 0;
		if($guardada!=false){
			$idRegistro = self::$con->conexionID->lastInsertId("aportes532");
		}
	
		return $idRegistro;
	}
	
	/**
	 * Guardar la relacion del medio de difusion con la notificacion <br/>
	 * Retorna 0 o el id del registro insertado
	 *
	 * @param unknown_type $datos
	 * @return number: 0|1
	 */
	function insert_notif_medio_difus($datos, $usuario){
		$sql = "INSERT INTO dbo.aportes533(id_notificacion, id_medio_difusion, usuario_creacion)
					VALUES ({$datos["id_notificacion"]},{$datos["id_medio_difusion"]},'$usuario')";
	
		$statement = self::$con->conexionID->prepare($sql);
		$guardada = $statement->execute();
	
		$idRegistro = 0;
		if($guardada!=false){
			$idRegistro = self::$con->conexionID->lastInsertId("aportes533");
		}
	
		return $idRegistro;
	}
	
	/**
	 * Guardar la relacion del rol con la notificacion <br/>
	 * Retorna 0 o el id del registro insertado
	 *
	 * @param unknown_type $datos
	 * @return number: 0|1
	 */
	function insert_notif_rol($datos, $usuario){
		$sql = "INSERT INTO dbo.aportes534 (id_notificacion, id_rol, usuario_creacion)
					VALUES ({$datos["id_notificacion"]},{$datos["id_rol"]},'$usuario')";
	
		$statement = self::$con->conexionID->prepare($sql);
		$guardada = $statement->execute();
	
		$idRegistro = 0;
		if($guardada!=false){
			$idRegistro = self::$con->conexionID->lastInsertId("aportes534");
		}
	
		return $idRegistro;
	}
	
	/**
	 * Guardar la registro de la notificacion <br/>
	 * Retorna 0 o el id del registro insertado
	 *
	 * @param unknown_type $datos
	 * @return number: 0|1
	 */
	function insert_regis_notif($datos, $usuario){
		$sql = "INSERT INTO dbo.aportes535 (id_notificacion, estado, usuario_creacion)
					VALUES ({$datos["id_notificacion"]},'{$datos["estado"]}','$usuario')";
	
		$statement = self::$con->conexionID->prepare($sql);
		$guardada = $statement->execute();
	
		$idRegistro = 0;
		if($guardada!=false){
			$idRegistro = self::$con->conexionID->lastInsertId("aportes535");
		}
	
		return $idRegistro;
	}
	
	/**
	 * Guardar la relacion del usuario con el registro de la notificacion <br/>
	 * Retorna 0 o el id del registro insertado
	 *
	 * @param unknown_type $datos
	 * @return number: 0|1
	 */
	function insert_regis_notif_usuar($datos, $usuario){
		$sql = "INSERT INTO dbo.aportes536 (id_registro_notificacion, id_usuario, estado, usuario_creacion)
					VALUES ({$datos["id_registro_notificacion"]},{$datos["id_usuario"]},'{$datos["estado"]}','$usuario')";
	
		$statement = self::$con->conexionID->prepare($sql);
		$guardada = $statement->execute();
	
		$idRegistro = 0;
		if($guardada!=false){
			$idRegistro = self::$con->conexionID->lastInsertId("aportes536");
		}
	
		return $idRegistro;
	}
	
	/***********************************
	 *  DELETE
	* *********************************/
	
	/**
	 * Elimina la relacion del medio de difusion con la notificacion
	 * 
	 * @param unknown_type $ID
	 * @return Ambigous <number, unknown>
	 */
	function delete_notif_medio_difus( $idRegistro ){
		$sql = "DELETE FROM dbo.aportes533 WHERE id_notif_medio_difus=$idRegistro";
		$statement = self::$con->queryActualiza($sql);
		return $statement === null ? 0: $statement;
	}
	
	/**
	 * Elimina la relacion del rol con la notificacion
	 *
	 * @param unknown_type $ID
	 * @return Ambigous <number, unknown>
	 */
	function delete_notif_rol( $idRegistro ){
		$sql = "DELETE FROM dbo.aportes534 WHERE id_notificacion_rol=$idRegistro";
		$statement = self::$con->queryActualiza($sql);
		return $statement === null ? 0: $statement;
	}
	
	/***********************************
	 *  UPDATE
	 * *********************************/
	
	/**
	 * Actualiza la notificacion
	 *
	 * @param unknown_type $datos
	 * @param unknown_type $usuario
	 * @return Ambigous <number, unknown>
	 */
	function update_notificacion($datos,$usuario){
		$sql = "UPDATE dbo.aportes530
				SET codigo = '{$datos["codigo"]}',
					enunciado = '{$datos["enunciado"]}',
					descripcion = :descripcion,
					query = :query,
					uri = :uri,
					dia_ejecucion = '{$datos["dia_ejecucion"]}',
					tiempo_expiracion = {$datos["tiempo_expiracion"]},
					estado = '{$datos["estado"]}',
					usuario_modificacion = '$usuario',
					fecha_modificacion = getdate()
				WHERE id_notificacion = {$datos["id_notificacion"]}";
		
		$statement = self::$con->conexionID->prepare($sql);
		$statement->bindValue(":descripcion", $datos["descripcion"],  PDO::PARAM_STR);
		$statement->bindValue(":query", $datos["query"],  PDO::PARAM_STR);
		$statement->bindValue(":uri", $datos["uri"],  PDO::PARAM_STR);
		$guardada = $statement->execute();
		return $guardada == false ? 0 : 1;		
	}
	
	
	/**
	 * Actualiza la hora de la notificacion
	 *
	 * @param unknown_type $datos
	 * @param unknown_type $usuario
	 * @return Ambigous <number, unknown>
	 */
	function update_hora_notif($datos,$usuario){
		$sql = "UPDATE dbo.aportes531
				SET	hora_ejecucion = {$datos["hora_ejecucion"]},
					fecha_ulima_ejecu = '{$datos["fecha_ulima_ejecu"]}',
					id_notificacion = {$datos["id_notificacion"]},
					estado = '{$datos["estado"]}',
					usuario_modificacion = '$usuario',
					fecha_modificacion = getdate()
				WHERE id_tiempo_ejecucion = {$datos["id_tiempo_ejecucion"]}";
	
		$statement = self::$con->conexionID->prepare($sql);
		$guardada = $statement->execute();
		return $guardada == false ? 0 : 1;
	}
	
	/**
	 * Actualiza el medio difusion para las notificaciones
	 *
	 * @param unknown_type $datos
	 * @param unknown_type $usuario
	 * @return Ambigous <number, unknown>
	 */
	function update_medio_difus($datos,$usuario){
		$sql = "UPDATE dbo.aportes532
				SET codigo = '{$datos["codigo"]}',
					descripcion = :descripcion,
					estado = '{$datos["estado"]}',
					usuario_modificacion = '$usuario',
					fecha_modificacion = getdate()
				WHERE id_medio_difusion = {$datos["id_medio_difusion"]}";
	
		$statement = self::$con->conexionID->prepare($sql);
		$statement->bindValue(":descripcion", $datos["descripcion"],  PDO::PARAM_STR);
		$guardada = $statement->execute();
		return $guardada == false ? 0 : 1;
	}
	
	/**
	 * Actualiza la relacion del medio difusion con la notificacion
	 *
	 * @param unknown_type $datos
	 * @param unknown_type $usuario
	 * @return Ambigous <number, unknown>
	 */
	function update_notif_medio_difus($datos,$usuario){
		$sql = "UPDATE dbo.aportes533
				SET id_notificacion = {$datos["id_notificacion"]},
					id_medio_difusion = {$datos["id_medio_difusion"]},
					usuario_modificacion = '$usuario',
					fecha_modificacion = getdate()
				WHERE id_notif_medio_difus = '{$datos["id_notif_medio_difus"]}'";
	
		$statement = self::$con->conexionID->prepare($sql);
		$guardada = $statement->execute();
		return $guardada == false ? 0 : 1;
	}
	
	/**
	 * Actualiza la relacion del rol con la notificacion
	 *
	 * @param unknown_type $datos
	 * @param unknown_type $usuario
	 * @return Ambigous <number, unknown>
	 */
	function update_notif_rol($datos,$usuario){
		$sql = "UPDATE dbo.aportes534
				SET id_notificacion = {$datos["id_notificacion"]},
					id_rol = {$datos["id_rol"]},
					usuario_modificacion = '$usuario',
					fecha_modificacion = getdate()
				WHERE id_notificacion_rol = {$datos["id_notificacion_rol"]}";
	
		$statement = self::$con->conexionID->prepare($sql);
		$guardada = $statement->execute();
		return $guardada == false ? 0 : 1;
	}
	
	
	/**
	 * Actualiza el registro de la notificacion
	 *
	 * @param unknown_type $datos
	 * @param unknown_type $usuario
	 * @return Ambigous <number, unknown>
	 */
	function update_regis_notif($datos,$usuario){
		$sql = "UPDATE dbo.aportes535
				SET id_notificacion = {$datos["id_notificacion"]},
					estado = '{$datos["estado"]}',
					usuario_modificacion = '$usuario',
					fecha_modificacion = getdate()
				WHERE id_registro_notificacion = {$datos["id_registro_notificacion"]}";
	
		$statement = self::$con->conexionID->prepare($sql);
		$guardada = $statement->execute();
		return $guardada == false ? 0 : 1;
	}
	
	/**
	 * Actualiza la relacion del usuario con el registro de la notificacion
	 *
	 * @param unknown_type $datos
	 * @param unknown_type $usuario
	 * @return Ambigous <number, unknown>
	 */
	function update_regis_notif_usuar($datos,$usuario){
		$sql = "UPDATE dbo.aportes536
				SET id_registro_notificacion = {$datos["id_registro_notificacion"]},
					id_usuario = {$datos["id_usuario"]},
					estado = '{$datos["estado"]}',
					usuario_modificacion = '$usuario',
					fecha_modificacion = getdate()
				WHERE id_regis_notif_usuar = {$datos["id_regis_notif_usuar"]}";
	
		$statement = self::$con->conexionID->prepare($sql);
		$guardada = $statement->execute();
		return $guardada == false ? 0 : 1;
	}
	
	/***********************************
	 *  SELECT
	 * *********************************/
	
	/**
	 * Consulta simple: notificaciones <br/>
	 * El parametro debe ser un array indice valor array("atributo"=>valor,...)
	 * 
	 * @param unknown_type $arrAtributoValor
	 * @return multitype:NULL | array
	 */
	function select_simpl_notificacion($arrAtributoValor){
		$attrEntidad = array(
				array("nombre"=>"id_notificacion","tipo"=>"NUMBER","entidad"=>"a530")
				,array("nombre"=>"codigo","tipo"=>"TEXT","entidad"=>"a530")
				,array("nombre"=>"tiempo_expiracion","tipo"=>"NUMBER","entidad"=>"a530")
				,array("nombre"=>"estado","tipo"=>"TEXT","entidad"=>"a530")
				,array("nombre"=>"usuario_creacion","tipo"=>"TEXT","entidad"=>"a530")
				,array("nombre"=>"usuario_modificacion","tipo"=>"TEXT","entidad"=>"a530"));
		
		$filtroSql = Utilitario::obtenerFiltro($attrEntidad,$arrAtributoValor);
		$sql = "SELECT id_notificacion,	codigo,	enunciado, descripcion,	query, uri,	dia_ejecucion, tiempo_expiracion, estado, usuario_creacion,	usuario_modificacion, fecha_creacion, fecha_modificacion
				FROM aportes530 a530
				$filtroSql";
		return $this->fetchConsulta($sql);
	}
	
	/**
	 * Consulta simple: horas de las notificaciones <br/>
	 * El parametro debe ser un array indice valor array("atributo"=>valor,...)
	 *
	 * @param unknown_type $arrAtributoValor
	 * @return multitype:NULL | array
	 */
	function select_simpl_hora_notif($arrAtributoValor){
		$attrEntidad = array(
				array("nombre"=>"id_tiempo_ejecucion","tipo"=>"NUMBER","entidad"=>"a531")
				,array("nombre"=>"hora_ejecucion","tipo"=>"NUMBER","entidad"=>"a531")
				,array("nombre"=>"id_notificacion","tipo"=>"NUMBER","entidad"=>"a531")
				,array("nombre"=>"estado","tipo"=>"TEXT","entidad"=>"a531")
				,array("nombre"=>"usuario_creacion","tipo"=>"TEXT","entidad"=>"a531")
				,array("nombre"=>"usuario_modificacion","tipo"=>"TEXT","entidad"=>"a531"));
	
		$filtroSql = Utilitario::obtenerFiltro($attrEntidad,$arrAtributoValor);
		$sql = "SELECT id_tiempo_ejecucion, hora_ejecucion, fecha_ulima_ejecu, id_notificacion, estado, usuario_creacion, usuario_modificacion, fecha_creacion, fecha_modificacion
				FROM aportes531 a531
				$filtroSql
				ORDER BY hora_ejecucion";
		return $this->fetchConsulta($sql);
	}
	
	/**
	 * Consulta simple: Medio de difusion para las notificaciones <br/>
	 * El parametro debe ser un array indice valor array("atributo"=>valor,...)
	 *
	 * @param unknown_type $arrAtributoValor
	 * @return multitype:NULL | array
	 */
	function select_simpl_medio_difus($arrAtributoValor){
		$attrEntidad = array(
				array("nombre"=>"id_medio_difusion","tipo"=>"NUMBER","entidad"=>"a532")
				,array("nombre"=>"codigo","tipo"=>"TEXT","entidad"=>"a532")
				,array("nombre"=>"estado","tipo"=>"TEXT","entidad"=>"a532")
				,array("nombre"=>"usuario_creacion","tipo"=>"TEXT","entidad"=>"a532")
				,array("nombre"=>"usuario_modificacion","tipo"=>"TEXT","entidad"=>"a532"));
	
		$filtroSql = Utilitario::obtenerFiltro($attrEntidad,$arrAtributoValor);
		$sql = "SELECT id_medio_difusion, codigo, descripcion, estado, usuario_creacion, usuario_modificacion, fecha_creacion, fecha_modificacion
				FROM aportes532 a532
				$filtroSql";
		return $this->fetchConsulta($sql);
	}
	
	/**
	 * Consulta simple: Relacion Medio de difusion con las notificaciones <br/>
	 * El parametro debe ser un array indice valor array("atributo"=>valor,...)
	 *
	 * @param unknown_type $arrAtributoValor
	 * @return multitype:NULL | array
	 */
	function select_simpl_notif_medio_difus($arrAtributoValor){
		$attrEntidad = array(
				array("nombre"=>"id_notif_medio_difus","tipo"=>"NUMBER","entidad"=>"a533")
				,array("nombre"=>"id_notificacion","tipo"=>"NUMBER","entidad"=>"a533")
				,array("nombre"=>"id_medio_difusion","tipo"=>"NUMBER","entidad"=>"a533")
				,array("nombre"=>"estado","tipo"=>"TEXT","entidad"=>"a533")
				,array("nombre"=>"usuario_creacion","tipo"=>"TEXT","entidad"=>"a533")
				,array("nombre"=>"usuario_modificacion","tipo"=>"TEXT","entidad"=>"a533"));
	
		$filtroSql = Utilitario::obtenerFiltro($attrEntidad,$arrAtributoValor);
		$sql = "SELECT 
					a530.id_notificacion, a530.codigo AS codigo_notificacion, a530.enunciado, a530.descripcion AS descripcion_notificacion, a530.query, a530.uri, a530.dia_ejecucion, a530.tiempo_expiracion, a530.estado AS estado_notificacion
					, a532.id_medio_difusion, a532.codigo AS codig_medio_difus, a532.descripcion AS descr_medio_difus, a532.estado AS estad_medio_difus
					, a533.id_notif_medio_difus
				FROM aportes533 a533
					INNER JOIN aportes530 a530 ON a533.id_notificacion=a530.id_notificacion
					INNER JOIN aportes532 a532 ON a532.id_medio_difusion=a533.id_medio_difusion
				$filtroSql";
		return $this->fetchConsulta($sql);
	}
	
	/**
	 * Consulta simple: Relacion rol con las notificaciones <br/>
	 * El parametro debe ser un array indice valor array("atributo"=>valor,...)
	 *
	 * @param unknown_type $arrAtributoValor
	 * @return multitype:NULL | array
	 */
	function select_simpl_notif_rol($arrAtributoValor){
		$attrEntidad = array(
				array("nombre"=>"id_notificacion_rol","tipo"=>"NUMBER","entidad"=>"a534")
				,array("nombre"=>"id_notificacion","tipo"=>"NUMBER","entidad"=>"a534")
				,array("nombre"=>"id_rol","tipo"=>"NUMBER","entidad"=>"a534")
				,array("nombre"=>"usuario_creacion","tipo"=>"TEXT","entidad"=>"a534")
				,array("nombre"=>"usuario_modificacion","tipo"=>"TEXT","entidad"=>"a534"));
	
		$filtroSql = Utilitario::obtenerFiltro($attrEntidad,$arrAtributoValor);
		$sql = "SELECT 
					a530.id_notificacion, a530.codigo AS codigo_notificacion, a530.enunciado, a530.descripcion AS descripcion_notificacion, a530.query, a530.uri, a530.dia_ejecucion, a530.tiempo_expiracion, a530.estado AS estado_notificacion
					, a516.rol
					, a534.id_notificacion_rol, a534.id_rol
				FROM aportes534 a534
					INNER JOIN aportes530 a530 ON a530.id_notificacion=a534.id_notificacion
					INNER JOIN aportes516 a516 ON a516.idrol=a534.id_rol
				$filtroSql";
		return $this->fetchConsulta($sql);
	}
	
	/**
	 * Consulta simple: Registro de las notificaciones <br/>
	 * El parametro debe ser un array indice valor array("atributo"=>valor,...)
	 *
	 * @param unknown_type $arrAtributoValor
	 * @return multitype:NULL | array
	 */
	function select_simpl_regis_notif($arrAtributoValor){
		$attrEntidad = array(
				array("nombre"=>"id_registro_notificacion","tipo"=>"NUMBER","entidad"=>"a535")
				,array("nombre"=>"id_notificacion","tipo"=>"NUMBER","entidad"=>"a535")
				,array("nombre"=>"estado","tipo"=>"TEXT","entidad"=>"a535")
				,array("nombre"=>"usuario_creacion","tipo"=>"TEXT","entidad"=>"a535")
				,array("nombre"=>"usuario_modificacion","tipo"=>"TEXT","entidad"=>"a535"));
	
		$filtroSql = Utilitario::obtenerFiltro($attrEntidad,$arrAtributoValor);
		$sql = "SELECT 
					a530.id_notificacion, a530.codigo AS codigo_notificacion, a530.enunciado, a530.descripcion AS descripcion_notificacion, a530.query, a530.uri, a530.dia_ejecucion, a530.tiempo_expiracion, a530.estado AS estado_notificacion
					, a535.id_registro_notificacion, a535.estado AS estad_regis_notif
				FROM aportes535 a535
					INNER JOIN aportes530 a530 ON a530.id_notificacion=a535.id_notificacion
				$filtroSql";
		return $this->fetchConsulta($sql);
	}
	
	/**
	 * Consulta simple: Registro de las notificaciones <br/>
	 * El parametro debe ser un array indice valor array("atributo"=>valor,...)
	 *
	 * @param unknown_type $arrAtributoValor
	 * @return multitype:NULL | array
	 */
	function select_simpl_regis_notif_usuar($arrAtributoValor){
		$attrEntidad = array(
				array("nombre"=>"id_regis_notif_usuar","tipo"=>"NUMBER","entidad"=>"a536")
				,array("nombre"=>"id_registro_notificacion","tipo"=>"NUMBER","entidad"=>"a536")
				,array("nombre"=>"id_usuario","tipo"=>"NUMBER","entidad"=>"a536")
				,array("nombre"=>"estado","tipo"=>"TEXT","entidad"=>"a536")
				,array("nombre"=>"usuario_creacion","tipo"=>"TEXT","entidad"=>"a536")
				,array("nombre"=>"usuario_modificacion","tipo"=>"TEXT","entidad"=>"a536"));
	
		$filtroSql = Utilitario::obtenerFiltro($attrEntidad,$arrAtributoValor);
		$sql = "SELECT 
					a530.id_notificacion, a530.codigo AS codigo_notificacion, a530.enunciado, a530.descripcion AS descripcion_notificacion, a530.query, a530.uri, a530.dia_ejecucion, a530.tiempo_expiracion, a530.estado AS estado_notificacion
					, a535.id_registro_notificacion, a535.estado AS estad_regis_notif
					, a536.id_regis_notif_usuar, a536.id_usuario, a536.estado AS estad_regis_notif_usuar
				FROM aportes536 a536
					INNER JOIN aportes535 a535 ON a535.id_registro_notificacion=a536.id_registro_notificacion
					INNER JOIN aportes530 a530 ON a530.id_notificacion=a535.id_notificacion
				$filtroSql";
		return $this->fetchConsulta($sql);
	}
	
	/**
	 * Consulta simple: Registro de las notificaciones <br/>
	 * El parametro debe ser un array indice valor array("atributo"=>valor,...)
	 *
	 * @param unknown_type $arrAtributoValor
	 * @return multitype:NULL | array
	 */
	function select_simpl_rol($arrAtributoValor){
		$attrEntidad = array(
				array("nombre"=>"idrol","tipo"=>"NUMBER","entidad"=>"a516")
				,array("nombre"=>"estado","tipo"=>"TEXT","entidad"=>"a516"));
	
		$filtroSql = Utilitario::obtenerFiltro($attrEntidad,$arrAtributoValor);
		$sql = "SELECT idrol, rol, estado, restriccion, diasrestid, jornadaini, jornadafin, horasrestini, horasrestfin, adiciona, modifica, usuario, fechasistema
				FROM aportes516 a516
				$filtroSql
				ORDER BY rol";
		return $this->fetchConsulta($sql);
	}
	
	/**
	 * Consulta compleja: Consultar las nuevas notificaciones para el usuario<br/>
	 * El parametro debe ser un array indice valor array("atributo"=>valor,...)
	 *
	 * @param unknown_type $arrAtributoValor
	 * @return multitype:NULL | array
	 */
	function select_compl_notif_usuar_new($arrAtributoValor){
		$sql = "SELECT 
					a535.id_registro_notificacion,a535.id_notificacion,a535.estado AS estad_regis_notif_usuar, a535.fecha_creacion AS fecha_creac_regis_notif
					, a530.codigo AS codigo_notificacion,a530.enunciado AS enunciado_notificacion,a530.descripcion AS descripcion_notificacion,a530.uri AS uri_notificacion
				FROM aportes535 a535
					INNER JOIN aportes530 a530 ON a530.id_notificacion=a535.id_notificacion
					INNER JOIN aportes534 a534 ON a534.id_notificacion=a530.id_notificacion
					INNER JOIN aportes519 a519 ON a519.idrol=a534.id_rol
				WHERE a519.idusuario={$arrAtributoValor["id_usuario"]}
					AND NOT EXISTS(
						SELECT * 
						FROM aportes536 
						WHERE id_usuario=a519.idusuario
						AND id_registro_notificacion=a535.id_registro_notificacion
					)";
		return $this->fetchConsulta($sql);
	}
	
	/**
	 * Consulta compleja: Consultar todas las notificaciones para el usuario<br/>
	 * El parametro debe ser un array indice valor array("atributo"=>valor,...)
	 *
	 * @param unknown_type $arrAtributoValor
	 * @return multitype:NULL | array
	 */
	function select_compl_notif_usuar_all($arrAtributoValor){
		$sql = "SELECT
					a535.id_registro_notificacion,a535.id_notificacion,a535.estado AS estad_regis_notif_usuar, a535.fecha_creacion AS fecha_creac_regis_notif
					, a530.codigo AS codigo_notificacion,a530.enunciado AS enunciado_notificacion,a530.descripcion AS descripcion_notificacion,a530.uri AS uri_notificacion
					, a536.id_regis_notif_usuar, a536.id_usuario
				FROM aportes535 a535
					INNER JOIN aportes530 a530 ON a530.id_notificacion=a535.id_notificacion
					INNER JOIN aportes534 a534 ON a534.id_notificacion=a530.id_notificacion
					INNER JOIN aportes519 a519 ON a519.idrol=a534.id_rol
					LEFT JOIN aportes536 a536 ON a536.id_registro_notificacion=a535.id_registro_notificacion AND a536.id_usuario=a519.idusuario
				WHERE a519.idusuario={$arrAtributoValor["id_usuario"]}";
		return $this->fetchConsulta($sql);
	}
	
	/**
	 * Retorna los valores obtenidos en la consulta
	 * @return multitype:array
	 */
	private function fetchConsulta($querySql){
		$resultado = array();
		$rs = self::$con->querySimple($querySql);
		while($row = $rs->fetch())
			$resultado[] = array_map("trim",$row);
			//$resultado[] = array_map("utf8_encode",$row);
		return $resultado;
	}
	
	/**
	 * Ejecuta el procedimiento almacenado que almacena los periodos
	 * en letras y numeros en la entidad de la liquidacion
	 * 
	 * @param unknown_type $idLiquidacion
	 * @return {int} ( 	0 => No se pudo realizar el procedimiento,
	 * 					1 => El procedimiento se ejecuto correctamente)
	 */
	function ejecutar_sp_observacion_liq($idLiquidacion){
		$resultado = 0;
		$sentencia = self::$con->conexionID->prepare ( "EXEC [kardex].[sp_020_Observacion_Liquidacion]
				@id_liquidacion = $idLiquidacion
				, @resultado = :resultado" );
		$sentencia->bindParam ( ":resultado", $resultado, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 4000 );
		$sentencia->execute ();
		if ($resultado > 0)
			return 1;
		else
			return 0;
	}
}
?>