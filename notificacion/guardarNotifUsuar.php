<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once 'clases'.DIRECTORY_SEPARATOR.'notificacion.class.php';

$objNotificacion = new Notificacion();

$uri = $_REQUEST["uri"];
$idRegistroNotificacion = $_REQUEST["id_registro_notificacion"];
$idUsuarioNotificacion = $_REQUEST["id_usuario_notificacion"];

$usuario = $_SESSION["USUARIO"];

$datosInsert = array(
		"id_registro_notificacion"=>$idRegistroNotificacion
		,"id_usuario"=>$idUsuarioNotificacion
		,"estado"=>"A");

$datosSelect = array(
		"id_registro_notificacion"=>$idRegistroNotificacion
		,"id_usuario"=>$idUsuarioNotificacion);

//Verificar si ya esta guardada

$resultadoNotificacion = $objNotificacion->select_simpl_regis_notif_usuar($datosSelect);

$idRegistro = 0;
if($resultadoNotificacion == 0 || count($resultadoNotificacion)==0){
	$idRegistro = $objNotificacion->insert_regis_notif_usuar($datosInsert, $usuario);
}

if($idRegistro>0 || count($resultadoNotificacion)>0){
	//Se guardo correctamente
	
	//Abrir la uri
	echo "<script languaje='javascript' type='text/javascript'>
			var url= '$uri'
			window.open(url,'_parent');</script>";
}else{
	//Mensaje de error
	echo "<script languaje='javascript' type='text/javascript'>
		alert('Error al guardar la relacion del usuario con la notificacion!!');
		</script>";
}

?>