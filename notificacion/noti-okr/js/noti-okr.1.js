/**
 * @author okarook
 */

(function(window, $){
	/*Declaracion de variables locales*/
	
    var 
		information = {
			autor: "Oscar Uriel Rodriguez Tovar",
			version: 1
		},
		
		selectClass = ".",
		
		selectId = "#",
		
		//Inicializacion del objeto
		jOur = function(selector){
	            return new jOur.fn.init(selector);
		};

    //Adiccionar metodos al objeto	
    jOur.fn = jOur.prototype = {
    		
            //Contenedor de nodos
            context: undefined,

            //Obtener los elementos
            init: function(selector){
            	context = $(selector);
            },
            /**
             * @params information Object {uriImg:...,textTitle:...,textBody:...,textBtnAll:...,uriBtnAll:..}
             */
            create: function(information){
            	//Metodo encargado de adicionar los elementos necesarios para las notificaciones
            	
            	var htmlEstructura = '<span class="noti-count" style="display:none;" class="parpadeo">&nbsp;</span>'+
            						'<a href="#" id="noti-link" ><img src="'+information.uriImg+'" title="'+information.textTitle+'" width="16" height="16" style="border:none; padding:0; margin:0; vertical-align:middle"/></a>'+
            						'<div class="noti-container">'+
            							'<div class="noti-title">'+information.textTitle+'</div>'+
            							'<div class="noti-body">'+information.textBody+'</div>'+
            							'<div class="noti-footer">'+
            								'<a href="'+information.uriBtnAll+'" target="myFrame" style="color:#000;">'+information.textBtnAll+'</a>'+
            							'</div>'+
            						'</div>';
            	
            	context.html(htmlEstructura);
            	
            	//Add Eventos
            	
            	//Document Click
            	$(document).click(this.hide);
            	
            	$("#noti-link").click(this.show);
            	
            	//Popup Click
            	/*$("#noti-container").click(function() {
            		return false
            	});*/
            	
            },
            
            /**
             * @params information Array[Object {
             * 				id_notificacion: int
             * 				, encabezado: String
             * 				, descripcion: String
             * 				, uri: String
             * 				, fecha: String
             * 				, read: Boolean
             * 			},...]
             */
            add: function(information){
            	var idLi = "#"+context.attr("id"); 
            	var thisNotiBody = $(idLi+" .noti-body");
            	
            	//Comprobar el parametro es un objeto o array
            	var count = 0;
            	
            	if(information && typeof information == "object"){
            		var objInformacion;
            		for(i = 0, len = information.length; i<len; i++){
            			objInformacion = information[i]; 
            			if(objInformacion.read==false)count++;
                    	
                    	var htmlNoti = '<div id="id-noti-'+objInformacion.id_notificacion+'" class="noti-body-div");"><a href="'+objInformacion.uri+'" target="_blank">'+
        									'<div class="noti-body-head">'+objInformacion.encabezado+'</div>'+
        									'<div class="noti-body-body">'+objInformacion.descripcion+'</div>'+
        									'<div class="noti-body-footer">'+objInformacion.fecha+'</div>'+
        								'</a></div>';
                    	
                    	thisNotiBody.append(htmlNoti);
            		}
            		
            		this.addCount(count);
            	}
            },
            
            remove: function(){
            	var id_notificacion = 0;
            	var idLi = "#"+context.attr("id"); 
            	if(arguments.length>0){
            		id_notificacion = arguments[0];
	            	
	            	var thisNotiBody = $(idLi+" #id-noti-"+id_notificacion);
	            	thisNotiBody.remove();
            	}else{
            		$(idLi +" .noti-body-div").remove();
            	}
            	
            	this.addCount($(idLi+" .noti-body-div").length)
            },
            
            addCount: function(count){
            	if(count>0){
            		
            		var idLi = "#"+context.attr("id"); 
            		var thisNotiCount = $(idLi+" .noti-count");

            		thisNotiCount.html(count);
            		thisNotiCount.show("slow");
            	}
            },
            
            addUriBtnAll: function(uri){
            	console.log("addUriBtnAll");
            },
            
            hide: function(){
            	$(".noti-container").hide();
            	//return false;
            },
            
            show: function(){
            	//$(".noti-container").fadeToggle(300);
            	$(".noti-container").show();
        		$(".noti-count").hide("slow");
        		return false;
            }
    };

    //Adiciona los atributos del jOur.fn, convirtiendo jOur.fn.init como infinito
    jOur.fn.init.prototype = jOur.fn;

    //Creacion del objeto global
    window.noti = jOur;//window.$ = jOur;

})(window,jQuery);
