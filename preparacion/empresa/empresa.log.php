<?php
set_time_limit(0);
date_default_timezone_set("America/Bogota");
$raiz = "";
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$url=$_SERVER['PHP_SELF'];
include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'auditoria.php';
auditar($url);

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

$accion = $_POST["v0"];
$datos = "";
switch ($accion){
	case "NIT":
		$datos = array("empresa"=>array(),"trabajadores"=>0);
		$sql = "SELECT idempresa,nit,razonsocial,fechasistema,fechaafiliacion,claseaportante,tipoaportante,a88.observaciones as nota,idtipoafiliacion,tipoaportante,estado
				FROM APORTES048 a48
				LEFT JOIN aportes088 a88 on a88.idregistro=a48.idempresa
				WHERE nit='".$_POST["v1"]."'";
		$rs = $db->querySimple($sql);
		$con = 0;
		$idEmpresa = "";
		while(($row=$rs->fetch())==true){
			$datos["empresa"][] = array_map("trim",$row);
			$idEmpresa = $row["idempresa"];
			$con++;
		} 
		if($con==0){
			$datos = "N";
		}else{
			$sqlTrabajadores = "SELECT COUNT(*) as trabajadores FROM Aportes016 where idempresa=$idEmpresa";
			$rsTrabajadores = $db->querySimple($sqlTrabajadores);
			if(($rowTrabajadores = $rsTrabajadores->fetch())==true){
				$datos["trabajadores"] = $rowTrabajadores["trabajadores"];
			}
		}
		break;
	case "GUARDAR":
		$idEmpresa = $_POST["v1"];
		$razon = $_POST["v2"];
		$fechaAfiliacion = $_POST["v3"];
		$clase = $_POST["v5"];
		$tipo = $_POST["v6"];
		$aportante = $_POST["v7"];
		$estado = $_POST["v8"];
		$sql = "UPDATE APORTES048 SET razonsocial='$razon',fechaafiliacion='$fechaAfiliacion',claseaportante=$clase,idtipoafiliacion=$tipo,tipoaportante=$aportante,estado='$estado' WHERE idempresa=$idEmpresa";
		$rs = $db->queryActualiza($sql);
		if($rs == 0){
			$datos = "U";
			break;
		}
		
		//guardar nota en la 88
		$nota = date("m/d/Y H:i:s")." - ".$_SESSION["USUARIO"]." - Modificacion de fechaafiliacion,fechasistema,razonsocial,claseaportante,tipoaportante,idtipoafiliacion de la empresa ";
		$sqlNota = "INSERT INTO aportes088 (idregistro,identidad,observaciones,usuarios)VALUES($idEmpresa,2,'$nota','".$_SESSION["USUARIO"]."');";
		$rsNota = $db->queryInsert($sqlNota, "aportes088");
		if($rsNota == 0){
			$datos = "I";
		}
		break;
}

echo json_encode($datos);
?>