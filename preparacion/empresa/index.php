<?php

$raiz = "";
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$url=$_SERVER['PHP_SELF'];
include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'auditoria.php';
auditar($url);

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}
$sqlClase = "select iddetalledef,detalledefinicion from aportes091 where iddefinicion=33";
$sqlAportante = "select iddetalledef,detalledefinicion from aportes091 where iddefinicion=34";
$sqlTipo = "select iddetalledef,detalledefinicion from aportes091 where iddefinicion=56";
$rsClase = $db->querySimple($sqlClase);
$rsAportante = $db->querySimple($sqlAportante);
$rsTipo = $db->querySimple($sqlTipo);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<title>Empresa</title>
		<link type="text/css" rel="stylesheet" href="../../css/css.1.8/jquery-ui-1.8.17.custom.css" />
		<script type="text/javascript" src="../../js/1.6/jquery-1.6.2.min.js"></script>
		<script type="text/javascript" src="../../js/1.6/jquery-ui-1.8.16.custom.min.js"></script>
		<script type="text/javascript" src="empresa.js"></script>
	</head>
	<body>
		<table border="0" align="center">
			<tr>
				<th colspan="2">Modificar Datos De La Empresa </th>
			</tr>
			<tr><td colspan="2">&nbsp;</td></tr>
			<tr>
				<td align="right">Nit: <input type="hidden" name="hdnIdEmpresa" id="hdnIdEmpresa"/></td>
				<td><input type="text" name="txtNit" id="txtNit" /></td>
			</tr>
			<tr>
				<td align="right">Razon Social: </td>
				<td ><input type="text" name="txtEmpresa" id="txtEmpresa" size="50" /></td>
			</tr>
			<tr>
				<td align="right">Fecha Afiliacion: </td>
				<td><input type="text" name="txtFechaAfiliacion" id="txtFechaAfiliacion" size="10" readonly="readonly"/></td>
			</tr>
			<tr>
				<td align="right">Fecha Sistema: </td>
				<td><input type="text" name="txtFechaSistema" id="txtFechaSistema" size="10" readonly="readonly"/></td>
			</tr>
			<tr>
				<td align="right">Estado: </td>
				<td>
					<select name="cmbEstado" id="cmbEstado">
						<option value="A">Activo</option>
						<option value="P">Por planilla</option>
					</select>
				</td>
			</tr>
			<tr>
				<td align="right">Clase Aportante: </td>
				<td>
					<select name="cmbClaseAportante" id="cmbClaseAportante">
						<option value="0">Seleccione..</option>
						<?php 
							while(($rowClase=$rsClase->fetch())==true){
								echo "<option value='".$rowClase["iddetalledef"]."'>".$rowClase["detalledefinicion"]."</option>";
							}
						?>
					</select>
				</td>
			</tr>
			<tr>
				<td align="right">Tipo Aportante: </td>
				<td>
					<select name="cmbTipoAportante" id="cmbTipoAportante">
						<option value="0">Seleccione..</option>
						<?php 
							while(($rowAportante=$rsAportante->fetch())==true){
								echo "<option value='".$rowAportante["iddetalledef"]."'>".$rowAportante["detalledefinicion"]."</option>";
							}
						?>
					</select>
				</td>
			</tr>
			<tr>
				<td align="right">Tipo Afiliacion: </td>
				<td>
					<select name="cmbTipoAfiliacion" id="cmbTipoAfiliacion">
						<option value="0">Seleccione..</option>
						<?php 
							while(($rowTipo=$rsTipo->fetch())==true){
								echo "<option value='".$rowTipo["iddetalledef"]."'>".$rowTipo["detalledefinicion"]."</option>";
							}
						?>
					</select>
				</td>
			</tr>
			<tr>
				<td align="right">Trabajadores: </td>
				<td id="tdTrabadores"></td>
			</tr>
			<tr>
				<td colspan="2" align="center">Notas</td>
			</tr>
			<tr>
				<td id="tdNotas" colspan="2"></td>
			</tr>
			<tr>
				<td colspan="4" align="center"><input type="button" onclick="guardar();" value="Actualizar"/></td>
			</tr>
		</table>
	</body>
</html>
