$(function(){
	$('#txtFechaAfiliacion').datepicker({
		maxDate:"+0D",
		changeMonth: true,
		changeYear: true,
	});
	
	$("#txtNit").blur(function(){
		nuevo();
		if($("#txtNit").val().trim()=="")
			return false;
		$.ajax({
			url:"empresa.log.php",
			type:"POST",
			data:{v0:"NIT",v1:this.value},
			dataType:"json",
			async:false,
			success:function(dato){
				
				if(dato=="N"){
					alert("La empresa no existe");
					return false;
				}
				$("#tdTrabadores").html(dato.trabajadores);
				$.each(dato.empresa,function(i,datos){
					$("#hdnIdEmpresa").val(datos.idempresa);
					$("#txtEmpresa").val(datos.razonsocial);
					$("#txtFechaAfiliacion").val(datos.fechaafiliacion);
					$("#txtFechaSistema").val(datos.fechasistema);
					$("#cmbEstado").val(datos.estado);
					$("#cmbClaseAportante").val(datos.claseaportante);
					$("#cmbTipoAportante").val(datos.tipoaportante);
					$("#cmbTipoAfiliacion").val(datos.idtipoafiliacion);
					
					$("#tdNotas").append(datos.nota+"<br/>");
				});
			}
		});
	});
	
});

function guardar(){
	var idEmpresa = $("#hdnIdEmpresa").val();
	var razon = $("#txtEmpresa").val().trim();
	var fechaAfiliacion =$("#txtFechaAfiliacion").val().trim();
	var clase = $("#cmbClaseAportante").val().trim();
	var tipo = $("#cmbTipoAfiliacion").val().trim();
	var aportante = $("#cmbTipoAportante").val();
	var estado = $("#cmbEstado").val();
	if(idEmpresa=="" || isNaN(parseInt(idEmpresa))){
		alert("Los datos no se cargaron correctamente!");
		return false;
	}
	if(razon==""){
		alert("Debe digitar la razon social!");
		return false;
	}
	if(fechaAfiliacion==""){
		alert("Debe digitar la fecha afiliacion!");
		return false;
	}
	
	$.ajax({
		url:"empresa.log.php",
		type:"POST",
		data:{v0:"GUARDAR",v1:idEmpresa,v2:razon,v3:fechaAfiliacion,v5:clase,v6:tipo,v7:aportante,v8:estado},
		dataType:"json",
		async:false,
		success:function(datos){
			
			if(datos=="U"){
				alert("No se realizo la actualizacion");
				return false;
			}
			if(datos=="I" ){
				alert("La nota no se guardo");
				return false;
			}
			alert("Los datos se actualizaron correctamente");
			nuevo();
		}
	});
}
function nuevo(){
	$("#hdnIdEmpresa").val("");
	$("#txtEmpresa").val("");
	$("#txtFechaAfiliacion").val("");
	$("#txtFechaSistema").val("");
	$("#cmbClaseAportante").val(0);
	$("#cmbTipoAfiliacion").val(0);
	$("#tdNotas").html("");
	$("#cmbTipoAportante").val(0);
	$("#tdTrabadores").html("");
}
