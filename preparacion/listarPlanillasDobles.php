<?php

include_once '../rsc/pdo/IFXDbManejador.php';
include_once '../rsc/pdo/IFXerror.php';

$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

$sql = "SELECT top 100 idplanilla, planilla, nit, identificacion, valoraporte, salariobasico, ingresobase, diascotizados, ingreso, retiro, var_tra_salario,sus_tem_contrato,periodo, planillasdobles.idempresa, idtrabajador,fechapago,papellido, sapellido, pnombre, snombre FROM aportes010
INNER JOIN planillasdobles ON aportes010.idtrabajador=planillasdobles.idpersona AND aportes010.idempresa=planillasdobles.idempresa
 WHERE periodo='201202' ORDER BY identificacion";

$sqlOrderBy= "SELECT count(idplanilla)as cantidad,MIN(idplanilla) as idplanillam, planilla, nit, identificacion, valoraporte, salariobasico, ingresobase, diascotizados, periodo, planillasdobles.idempresa, idtrabajador,fechapago,papellido, sapellido, pnombre, snombre
FROM aportes010
INNER JOIN planillasdobles ON aportes010.idtrabajador=planillasdobles.idpersona AND aportes010.idempresa=planillasdobles.idempresa
 WHERE periodo='201202' 
group by planilla, nit, identificacion, valoraporte, salariobasico, ingresobase, diascotizados, periodo, planillasdobles.idempresa, idtrabajador,fechapago,papellido, sapellido, pnombre, snombre
ORDER BY identificacion";

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Requerimiento</title>
<link type="text/css" href="../css/Estilos.css" rel="stylesheet"/>
<link type="text/css" href="../css/marco.css" rel="stylesheet"/>
<link type="text/css" rel="stylesheet" href="../css/css.1.8/jquery-ui-1.8.17.custom.css" />
<script language="javascript" src="../js/1.6/jquery-1.6.2.min.js"></script>
<script language="javascript" src="../js/1.6/jquery-ui-1.8.16.custom.min.js"></script>
<script language="javascript" src="js/listarPlanillasDobles.js"></script>

<style type="text/css">
  	table tr:hover td{
		bakground-color:red;
  	}
 </style>

</head>

<body>
<center>

<input type="submit" name="btnBorrar" id="btnBorrar" onclick="borrar();" value="borrar"/>

<table width="80%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td class="arriba_iz" >&nbsp;</td>
<td class="arriba_ce" ><span class="letrablanca">::Planillas Dobles::</span></td>
<td class="arriba_de" >&nbsp;</td>
</tr>
<tr>
<td class="cuerpo_iz">&nbsp;</td>
<td class="cuerpo_ce">
</td>
<td class="cuerpo_de">&nbsp;</td>
</tr>
<tr>
<td class="cuerpo_iz" >&nbsp;</td>
<td class="cuerpo_ce" >
    
<table width="100%" border="0" class="tablero" cellspacing="0">
  <tr color="blue">
    <td>Eliminar</td>
    <td>IdPlanilla</td>
    <td>planilla</td>
    <td>diascotizados</td>
    <td>nit</td>
    <td>identificacion</td>
    <td>valoraporte</td>
    <td>salariobasico</td>
    <td>ingresobase</td>
    
    <td>ingreso</td>
    <td>retiro</td>
    <td>var_tra_salario</td>
    <td>sus_tem_contrato</td>
    <td>periodo</td>
    <td>idempresa</td>
    <td>idtrabajador</td>
    <td>fechapago</td>
    <td>papellido</td>
    <td>sapellido</td>
    <td>pnombre</td>
    <td>snombre</td>
  </tr>
    <?php
    	$consulta= $db->querySimple($sql);
    	$consultaQuery= $db->querySimple($sqlOrderBy);
					
        $cont = 0;
        $bn=0;
        $colorTr='#C1FBD1';
        $colorText = '#000';
        $array=array();
        while($rowOrder = $consultaQuery->fetchObject()){
            $array[] = $rowOrder->idplanillam;
        }
        while($row = $consulta->fetchObject()){
        	$checket='';
            $contText = 0;
            if($row->identificacion!=$bn){
                if($colorTr =='#C1FBD1')
                    $colorTr='#C6C1FB';
                else
                    $colorTr='#C1FBD1';
            }
            
            foreach($array as $idPlanilla){
                if($idPlanilla==$row->idplanilla)
                    $contText++;
            }
            if($contText>0){
                $colorText = '#BA0437';
                $checket ='';
            }
            else{
                $colorText = '#000';
                $checket ='checked';
            }
            
              
            echo "<tr style='background:$colorTr;color:$colorText;'><td><input type='checkbox' $checket name='ckeBorrar$cont' id='ckeBorrar$cont' value='".$row->idplanilla."' /></td><td><label id='lblIdPlanilla$cont'>".$row->idplanilla."</label></td>
                    <td>$row->planilla</td>
                    <td>$row->diascotizados</td>
                    <td>$row->nit</td>
                    <td>$row->identificacion</td>
                    <td>$row->valoraporte</td>
                    <td>$row->salariobasico</td>
                    <td>$row->ingresobase</td>
                    
                    <td>$row->ingreso</td>
                    <td>$row->retiro</td>
                    <td>$row->var_tra_salario</td>
                    <td>$row->sus_tem_contrato</td>
                    <td>$row->periodo</td>
                    <td>$row->idempresa</td>
                    <td>$row->idtrabajador</td>
                    <td>$row->fechapago</td>
                    <td>$row->papellido</td>
                    <td>$row->sapellido</td>
                    <td>$row->pnombre</td>
                    <td>$row->snombre</td>
                    </tr>";
            $cont++;
            $bn = $row->identificacion;
        }
    ?>
    
</table></td>
<td class="cuerpo_de" >&nbsp;</td>
</tr>
<tr>
<td class="cuerpo_iz" >&nbsp;</td>
<td class="cuerpo_ce" ></td>
<td class="cuerpo_de" >&nbsp;</td>
</tr>
</table>
</center>
<input type='hidden' name='txtCantidadRegistros' id='txtCantidadRegistros' value='<?php echo $cont;?>' />
</body>
</html>