<?php
/* autor:       orlando puentes
 * fecha:       30/08/2010
 * objetivo:     
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;


$idpersona= $_REQUEST['v0'];
include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'p.planilla.unica.class.php';
$objClase=new Planilla();
$consulta = $objClase->buscar_planillas($idpersona);
$cont=0;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Documento sin título</title>
</head>

<body>
<h4>Planilla Unica</h4>
<table width="100%" border="0" cellspacing="0" class="tablero">
<tr>
<th >Planilla</th>
<th >Periodo</th>
<th >I.Base</th>
<th >S.Basico</th>
<th >D&iacute;as</th>
<th>Fecha</th>
<th >I</th>
<th >R</th>
<th>VS</th>
<th>V</th>
<th>M</th>
<th>TC</th>
<th>P</th>
<th>C</th>
</tr>
<?php 
while($row=mssql_fetch_array($consulta)){
?>
<tr>
    <td style="text-align:right"><?php echo $row['planilla']; ?></td>
    <td style="text-align:center"><?php echo $row['periodo']; ?></td>
    <td style="text-align:right"><?php echo number_format($row['ingresobase']); ?></td>
    <td style="text-align:right"><?php echo number_format($row['salariobasico']); ?></td>
    <td style="text-align:right"><?php echo $row['horascotizadas']; ?></td>
    <td style="text-align:center"><?php echo $row['fechasistema']; ?></td>
    <td><?php echo $row['ingreso']; ?></td>
    <td><?php echo $row['retiro']; ?></td>
    <td><?php echo $row['var_tra_salario']; ?></td>
    <td><?php echo $row['vacaciones']; ?></td>
    <td><?php echo $row['lic_maternidad']; ?></td>
    <td><?php echo $row['tipo_cotizante']; ?></td>
    <td><?php echo $row['procesado']; ?></td>
    <td><?php echo $row['correccion']; ?></td>
    
  </tr>
  <?php }?>
</table>
<br />
  Convenciones:

<ul>
  <li>I.....Ingreso</li>
  <li>R....Retiro</li>
  <li>VS..Variaci&oacute;n Salarial</li>
  <li>V....Vacaciones</li>
  <li>M...Maternidad</li>
  <li>TC..Tipo Cotizante</li>
  <li>P....Procesado</li>
  <li>C...Correcci&oacute;n</li>
</ul>
</body>
</html>