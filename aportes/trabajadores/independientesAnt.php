<?php
/* autor:       Orlando Puentes
 * fecha:       Julio 23 de 2010
 * objetivo:    Almacenar en la base de datos Aportes la información de los trabajadores que se van a afiliar a Comfamiliar Huila.
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'ciudades.class.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'funcionesComunes' . DIRECTORY_SEPARATOR . 'funcionesComunes.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'p.definiciones.class.php';

$objClase=new Definiciones();
$objCiudad=new Ciudades();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>::Afiliacion Independientes::</title>

<link type="text/css" rel="stylesheet" href="<?php echo URL_PORTAL; ?>newcss/marco.css" />
<link type="text/css" rel="stylesheet" href="<?php echo URL_PORTAL; ?>css/Estilos.css" />
<link type="text/css" rel="stylesheet" href="<?php echo URL_PORTAL; ?>css/css.1.8/jquery-ui-1.8.17.custom.css" />

<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/1.6/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/1.6/jquery-ui-1.8.16.custom.min.js"></script>

<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/comunes.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/direccion.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/llenarCampos.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/jquery.combos2.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/jquery.fechaLarga.js"></script>

<script type="text/javascript" src="js/independientes.js" ></script>
<script type="text/javascript" src="js/afiliacionTab.js"></script>
<script type="text/javascript" src="js/grupoTab.js"></script>

<script type="text/javascript">
	shortcut.add("Shift+F",function() {
		var URL=src();
		var url=URL+"aportes/trabajadores/consultaTrabajador.php";
		window.open(url,"_blank");
    },{
		'propagate' : true,
		'target' : document 
	});
	
	$(function() {
		$('#fecIngreso2').datepicker({
			changeMonth: true,
			changeYear: true
		});
		
		var anoActual = new Date();
		var strYearRange = anoActual.getFullYear()-120 +":"+ anoActual.getFullYear();
		$(".boxfecha").datepicker( "option", "yearRange", strYearRange );
	});
</script>
</head>
<body>
<form name="forma">
<!-- TABLA VISIBLE CON BOTONES -->
<table width="97%" border="0" cellspacing="0" cellpadding="0" align="center">
	<tr>
		<td width="13" height="29" class="arriba_iz">&nbsp;</td>
		<td class="arriba_ce"><span class="letrablanca">::Afiliaci&oacute;n Independientes&nbsp;::</span></td>
		<td width="13" class="arriba_de" align="right">&nbsp;</td>
	</tr>      
	<tr>
 		<td class="cuerpo_iz">&nbsp;</td>
		<td class="cuerpo_ce">
			<img src="../../imagenes/tabla/spacer.gif" width="1" height="1"/>
			<img src="../../imagenes/spacer.gif" width="1" height="1"/>
			<img src="../../imagenes/menu/nuevo.png" title="Nuevo" width="16" height="16" style="cursor:pointer" onclick="nuevoR();"/>
			<img src="../../imagenes/spacer.gif" width="9" height="1"/>
			<img src="../../imagenes/menu/refrescar.png" width="16" height="16" style="cursor:pointer" title="Limpiar campos" onclick="limpiarCampos(); document.forms[0].elements[0].focus();"/>
			<img src="../../imagenes/spacer.gif" width="6" height="1"/>
			<img src="../../imagenes/menu/informacion.png" width="16" height="16" style="cursor:pointer" title="Manual" onclick="mostrarAyuda();" />
			<img src="../../imagenes/spacer.gif" width="2" height="1"/>
			<img src="../../imagenes/menu/notas.png" width="16" height="16" style="cursor: pointer" title="Colaboración en línea" onclick="notas();" /> 	
			<br/>
			<font size=1 face="arial">Nuevo&nbsp;Limpiar&nbsp;Info&nbsp;Ayuda</font>
		</td>
		<td class="cuerpo_de">&nbsp;</td>
	</tr> 
	<tr>     
		<td class="cuerpo_iz">&nbsp;</td>
		<td class="cuerpo_ce"><div id="error" style="color:#FF0000"></div></td>
		<td class="cuerpo_de">&nbsp;</td>
	</tr>
	<tr>
		<td class="cuerpo_iz">&nbsp;</td>		
		<td class="cuerpo_ce">
			<center>
				<table width="90%" border="0" cellspacing="0" class="tablero">
					<tr>
						<td width="25%">Afiliaciones pendientes</td>
						<td width="25%"><select name="pendientes" id="pendientes" class="boxfecha" onchange="buscarRadicacion();">
							</select><input type="hidden" name="radicacion" id="radicacion" />
						</td>
						<td width="25%">Fecha Radicaci&oacute;n</td><td><input name="fecha" type="text" class="boxfecha" id="textfield"  readonly="readonly" /></td>
					</tr>
				</table>
			</center>
			<div id="errores" align="left"></div>
		</td>
		<td class="cuerpo_de">&nbsp;</td>
	</tr>
	<tr>
		<td class="abajo_iz" >&nbsp;</td>
		<td class="abajo_ce" ></td>
		<td class="abajo_de" >&nbsp;</td>
	</tr>
</table>  

<div style="margin:0 20px">
	<div id="tabsR">
		<ul id="ul">
			<li><a href="#tabs-1" id="a0">Afiliaci&oacute;n</a></li>	
    		<li><a href="#tabs-2" id="a1">Datos Personales</a></li>
    		<li><a href="#tabs-3" id="a2">Afiliaci&oacute;n</a></li>
    		<li><a href="#tabs-4" id="a3">Grupo Familiar</a></li>
    		<li><a href="#tabs-5" id="a4">Documentos</a></li>
		</ul>
		
 		<div id="tabs-1">	
			<table width="100%" border="0" cellspacing="0" cellpadding="0">      
				<tr>
					<td background="../../imagenes/tabla/centro.gif">
						<img src="../../imagenes/tabla/spacer.gif" width="1" height="1" />
					</td>      
				</tr> 
				<tr>				
					<td align="center" background="../../imagenes/tabla/centro.gif"><br />
						<h4 align="left"> DATOS EMPRESA ACTUAL </h4>
						<table width="100%" border="0" cellspacing="0" class="tablero">
    						<tr>
    							<td width="52">Nit</td>
    							<td width="171"><input name="nitActual" class="box1" id="nitActual" readonly="readonly" onblur="buscarSucursalesCombo(this);" /></td>
    							<td width="101">Raz&oacute;n Social</td>
    							<td width="354"><input name="razonSocial" type="text" class="boxmediano" id="razonSocial"  readonly="readonly" /></td>
    							<td width="87">Sucursal</td>
    							<td width="289"><select name="comboSucursal" class="box1" id="comboSucursal" ></select> <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
    						</tr>
    					</table> <br /> 
						<hr color="#CCCCCC" /> 
						<h4 align="left"> ACTUALIZACI&Oacute;N DE LA AFILIACI&Oacute;N</h4>
						<table width="100%" border="0" cellspacing="0" class="tablero" id="tablaAfiTab">
 							<tr>
								<td>Afiliado</td>
								<td colspan="3" id="nomAfi">&nbsp;</td>
  							</tr>
    						<tr>
      							<td width="15%">Tipo  Formulario</td>
      							<td width="35%">
        							<select name="tipForm2" class="box1" id="tipForm2" onblur="validarSubsidio();" style="width:240px" >
          								<option selected="selected" value=49>Inscripcion Servicios</option>
          							</select> <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
          						</td>
								<td width="15%">Tipo Afiliaci&oacute;n</td>
      							<td width="35%">
        							<select name="tipAfiliacion2" class="box1" style="width:240px" id="tipAfiliacion2" onblur="validarSubsidio();" onchange="onChangeTipoAfiliacion(this);" >
		      						<?php
			    						$consulta1 = $objClase->mostrar_datos(2,3);
			    						while( $row = mssql_fetch_array( $consulta1 ) ){
			    							if(($row['iddetalledef'])!="18"){
					  							echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
			    							}
										}
									?>
          							</select> <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
          						</td>
    						</tr>
    						<tr>
    							<td><label for="indice">Porcentaje de aportes</label></td>
    							<td>
    								<select id="indice" name="indice" class="box1" style="width:240px">
    									<option value="0" selected="selected">Seleccione...</option>
									<?php
		    							$consulta = $objClase->mostrar_datos(18);
		    							while( $row = mssql_fetch_array($consulta)){
		    								if($row['iddetalledef'] !="2920" && $row['iddetalledef'] !="108" )
				  								echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
										}
									?>
    								</select>
    							</td>
    							<td>Fecha Ingreso</td>
    							<td><input name="fecIngreso2" type="text" class="boxfecha" style="width:240px" id="fecIngreso2" onchange="validarFechaIngresoGrabacion('fecIngreso2');" /> <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
    						</tr>
    						<tr>
    							<td>Horas D&iacute;a</td>
    							<td><input name="horasDia2" type="text" class="box1" style="width:240px" id="horasDia2"  maxlength="1"/> <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
    							<td>&nbsp;</td>
    							<td>&nbsp;</td>
    						</tr>
    						<tr>
    							<td>Horas Mes</td>
    							<td><input name="horasMes2" type="text" class="box1" style="width:240px" id="horasMes2" maxlength="3" /> <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
    							<td>Tipo Pago</td>
    							<td>
    								<select name="tipoPago2" class="box1" id="tipoPago2" style="width:240px" disabled="disabled">
    									<option value="0">Seleccione..</option>
    									<option value="T" selected="selected" >Tarjeta</option>
    									<option value="C">Cheque</option>
    								</select> <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />    
    							</td>
    						</tr>
    						<tr>
    							<td>Salario</td>
    							<td><input name="salario2" type="text" class="box1" style="width:240px" id="salario2" onblur="calcularCategoria2(this.value)"/> <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
    							<td>Categor&iacute;a</td>
    							<td><input name="categoria2" type="text" class="boxfecha" id="categoria2" readonly="readonly" /></td>
    						</tr>
    						<tr>
    							<td>Agr&iacute;cola</td>
    							<td>
    								<select name="agricola2" class="box1" id="agricola2" style="width:240px" >
      									<option value="0">Seleccione..</option>
      									<option value="S">SI</option>
      									<option value="N" selected="selected">NO</option>
    								</select> <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
    							</td>
    							<td>Cargo</td>
    							<td>
    								<select name="cargo2" class="box1" id="cargo2" style="width:240px" >
      									<option value="0" selected="selected">Seleccione..</option>
      								<?php
										$consulta=$objClase->mostrar_datos(21, 4);
										while($row=mssql_fetch_array($consulta)){
											echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
										}
									?>
										<option value="0">Otro</option>
    								</select> <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /><input type="hidden" name="primaria" id="primaria" />
    							</td>
    						</tr>
    						<tr>
    							<td>Estado</td>
    							<td>
    								<select name="estado2" class="box1" id="estado2" style="width:240px" disabled="disabled" >
      									<option value="0" selected="selected">Seleccione..</option>
      									<option value="A" >ACTIVO</option>
      									<option value="I">INACTIVO</option>
      									<option value="P" >PENDIENTE</option>
    								</select> <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
    							</td>
    							<td>Tipo</td>
    							<td>
    								<select name="cboTipo2" class="box1" id="cboTipo2" style="width:240px" disabled="disabled" >
      									<option value="0">Seleccione..</option>
      									<option value="S" selected="selected">Primaria</option>
      									<option value="N">Adicional</option>
    								</select>
    							</td>
    						</tr>
						</table><br />
						<a id="actualizarR" value="Actualizar datos" class="ui-state-default"  onClick="actualizarAfiliacion2();" style="text-decoration:none; padding:4px; cursor:pointer;" >Actualizar datos</a>
						<br /><hr color="#CCCCCC" />			
						<h4 align="left"> DATOS OTRAS EMPRESAS LABORA EL TRABAJADOR </h4>
						<table width="100%" border="0" cellspacing="0" class="tablero">
    						<tr>
    							<td width="17%">Nit</td>
    							<td width="29" colspan="3">&nbsp;</td>
    							<td>&nbsp;</td>
     						</tr>
     					</table>
					</td>
				</tr>
			</table>  
 		</div>
		<!-- TABS-2 DATOS PERSONA -->  
		<div id="tabs-2"></div> 
		<!-- TABS-3 DATOS AFILIACION -->  
		<div id="tabs-3"></div>
		<!-- TABS-4 GRUPO F.--> 
		<div id="tabs-4"></div>
		<!-- TABS-5 -->  
		<div id="tabs-5">
			<div class="galleria-thumbnails" id="galeria"></div>
		</div>
		<div id="imagen"  title="DOCUMENTOS"></div>
		<!-- FIN TABS-5 --> 
 	</div> 
 	<!-- FIN DIV TABS GENERAL -->
</div>
   
<input id="txtHiddenSexoR" type="hidden" value="" />
<input id="txtSMLV" type="hidden" value="" /> 

<!-- VENTANA DIALOG PORFESION -->
<div id="profesionDialog" title="NUEVA PROFESI&Oacute;N" style="display:none;">
	<p><label>Digitar Profesi&oacute;n:</label>
    <input id="nuevaProf" type="text" style="width:200px" /></p>
   
</div>

<!-- formulario direcciones  --> 
<div id="dialog-form" title="Formulario de direcciones"></div>

<!-- colaboracion en linea -->
<div id="div-observaciones-tab"></div>
<div id="dialog-form2" title="Colaboraci&oacute;n en l&iacute;nea">
<p>Por favor diligencie este formulario para enviar comentarios, errores o falencias encontradas en el proceso. M&aacute;ximo 250 caracteres </p>
<label>Tus comentarios:</label><br />
<textarea name="notas" id="notas" cols="60" rows="10"></textarea>
</div>

<!-- fin colaboracion -->

<!-- ayuda en linea -->
<div id="ayuda" title="Manual .:. Afiliación Trabajador Independiente" style="background-image:url(../../imagenes/FondoGeneral0.png)"></div>
<!-- fin ayuda en linea -->
<!-- VENTANA DIALOG BARRIO -->
<div id="barrioDialog" title="NUEVO BARRIO" style="display:none;">
	<p><label>Digitar Barrio:</label><input id="nuevoBarrio" type="text" style="width:200px" /></p>
   
</div>
<!-- VENTANA DIALOG CARGO -->

<div id="cargoDialog" title="NUEVO CARGO" style="display:none;">
	<p><label>Digitar Cargo:</label><input id="nuevoCargo" type="text" style="width:200px" /></p>
   
</div>
<input type="hidden" id="idPersona" name="idPersona" />
<input type="hidden" id="noPersona" name="noPersona" />
<input type="hidden" id="idEmpresa" name="idEmpresa" />
</form>

</body>
<script language="javascript">
function mostrarAyuda(){
	$("#ayuda").dialog('open' );
	}

function notas(){
	$("#dialog-form2").dialog('open');
	}	
function siguienteFocus(field){
    $("#cboZona").focus();
	}
</script>
</html>
