function setCookie(c_name,value,exdays)
{
	var exdate=new Date();
	exdate.setDate(exdate.getDate() + exdays);
	var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
	document.cookie=c_name + "=" + c_value;
}// fin setCookie

// funcion para obtener cookie en javascript
	function getCookie(c_name)
	{
		var i,x,y,ARRcookies=document.cookie.split(";");
		for (i=0;i<ARRcookies.length;i++)
		{
  			x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
  			y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
  			x=x.replace(/^\s+|\s+$/g,"");
  			if (x==c_name)
    			{
    				return unescape(y);
    			}// fin if
		}// fin for
  	}// fin getCookie
//funcion para obtener cookie en javascript

/**
 * 
 */
var URL=src();
var idPersona=0;
var idAfiliacion=0;
var idnp=0;
var idempresa=0;
var existe=0;
var existenDatos=0;
var idnb=0;
var conClick=0;
var idTarjeta=0;
var ruta=false;
var idtrelacion=0;

$(document).ready(function(){
	var smlv=salarioMinimo();
	smlv=(isNaN(smlv) && smlv>0)?616000:smlv;
	$("#txtSMLV").val(smlv);
	
	var campo0="2926,4519"; //iddetalledef en 091
    $('#pendientes option').remove();
	$.post(URL+'phpComunes/buscarRadicaTipoID.php',{v0:campo0},function(datos){
    	var cmbRadicacion = $("#pendientes");
	   	$(cmbRadicacion).html(datos);
	   	if($(cmbRadicacion).children().length==1){
			alert('Lo lamento NO hay radicaciones pendientes de grabar!');
			return false;
         }
	});
	
	$("#fecIngreso").datepicker({ changeMonth: true, changeYear: true });
	$(".ui-datepicker").css("z-index",3000);		

	var idDiv;
	//errorTab;
	var lista;
	var op;

	//documentos
	$("#tabsR #a4").unbind("click");
	$("#tabsR #a4").bind("click",function(){
		$("#tabs-5").load("tabConsulta/documentosTab.php",{v0:idPersona});
	});
   
   //$("#tabsR ul li a:not(':first',':last')").one("click",function(){ // a:not(':first,:last')"
	$("#tabsR ul li a:not(':first',':last')").click(function(){
		 var aid=$(this).attr("id");
		 idPersona=$("#idPersona").val();
		 switch(aid){
			case "a1": $("#tabs-2").load("tabConsulta/personaTab.php",{v0:idPersona}); break; 
			case "a2": $("#tabs-3").load("tabModifica/afiliacionTab.php",{v0:idPersona}); break; 
			case "a3": $("#tabs-4").load("tabModifica/grupoTab.php?v0="+idPersona+"&v1=R");    break; 
			//case "a4": $("#tabs-5").load("tabConsulta/documentosTab.php",{v0:idPersona}); break;
		 }
   });

	//Carga los tabs
	$("#tabsR").tabs({
		spinner:'<em>Loading&#8230;</em>',
		selected:0,
		show:function(event,ui){
		 if(ui.index==0&&$("#actualizarR").is(":visible")){	
		 $("#tabsR ul li a:gt(0)").hide();
	     }else{
		 $("#tabsR ul li a:gt(0)").show();
		 }
		},
	});//end tab		
	
	
	//Dialogs usados
	$("#ayuda").dialog({
		autoOpen: false,
		height: 500, 
		width: 700, 
		draggable:true,
		modal:false,
		open: function(evt, ui){
			$('#ayuda').html('');
			$.get('../../help/aportes/manualayudaIndependientes.html',function(data){
				$('#ayuda').html(data);
			});
		}
	});
	
	$("#dialog-form2").dialog("destroy");
	$("#dialog-form2").dialog({
		autoOpen: false,
		height: 400,
		width: 500,
		modal: true,
		buttons: {
			'Enviar': function() {
			var bValid = true;
			var campo=$('#notas').val();
			var campo0=$.trim(campo);
			if (campo0==""){
				$(this).dialog('close');
					return false;
				}
			var campo1=$('#usuario').val();
			var campo2="trabajadorNuevo.php";
			$.post(URL+'phpComunes/colaboracion.php',{v0:campo0,v1:campo1,v2:campo2}, 								
			function(datos){
				if(datos=='1'){
					alert("Su comentario fue enviado correctamente!. Gracias por participar en el mejoramiento de SIGAS");
				}else{
					alert(datos);
				}
		});
		$(this).dialog('close');
		},
		Cancelar: function() {
			$(this).dialog('close');
			$("#dialog-form2").dialog("destroy");
		}
	}		
	});
	
});//document ready


function nuevoR(){
	nuevo=1;
	conClick=0;
	
	$("#errores").empty();
	$("div#tabsR #a0").trigger('click');
	//Reiniciar el boton actualizar y ocultar tabs

	$("#tabsR ul li a:gt(0)").hide();
	
	//$("#tabsR ul li a:gt(0)").show();
    $("#actualizarR").show();
	msg="";	
	$("#email2").val('');
	$("#pendientes").focus();
	limpiarCampos();
	$("#tipoPago2").val('T');
	$("#razonSocial").val('');
}

function limpiarCampos(){
	$("table.tablero input:text").val('');
	$("table.tablero select").val('Seleccione..');
	$("#tablaB tbody tr").remove();
	$("#tablaC tbody tr").remove();	
	//tab renovacion
	$("#tablaAfiTab  input:text").val('');
	$("#tablaAfiTab  select").val('');
	$("#tablaAfiTab  #cboPais").val('48');
	$("#nomAfi").empty();
	$("#txtHiddenSexoR").val('');
	//$("#a3").unbind();
	$("#tipAfiliacion2").trigger("change");
}

/*
 * Calcular Categoria del Independiente Segun Honorarios y Tipo de Afiliaci�n:
 * 
 * 	-Los independientes facultativos,extranjeros, e Independientes agremiados siempre la categor�a ser� B.
 * 	-Los pensionados de r�gimen especial y sistemas de cajas deben ser A.
 * 	-Los pensionados voluntarios,  la categor�a var�a seg�n el valor de la mesada.
 * 
 * 	ANDRES LARA 2016
 */

function calcularCategoriaIndependiente(honorarios){
	
	var categoria='';
	
	var salario=parseInt(honorarios);
	
	//Campo Oculto Con el SMLV en el formulario
	var salarioMinimoLegalVigente=$('#txtSMLV').val();
	
	//Tipo de Afiliacion
	var tipoAfiliacion=$("#tipAfiliacion2").val();
	
	if(tipoAfiliacion=='19'||tipoAfiliacion=='4520'||tipoAfiliacion=='4521'){
		categoria='B';
	}else{
		
		if(tipoAfiliacion=='21'||tipoAfiliacion=='4209'||tipoAfiliacion=='3320'||tipoAfiliacion=='2938'){
			categoria='A';
		}else{
			
			var totalSalario=parseFloat(salario/salarioMinimoLegalVigente);	
		   
		    if(totalSalario<=2){
		        categoria='A';
		    }
		    if(totalSalario>2 && totalSalario<=4){
		    	 categoria='B';
		    }
		    if(totalSalario>4){
		    	 categoria='C';
		    }
			
		}
		
	}
	
	 $('#categoria2').val(categoria);
	
}

function calcularCategoria2(sal){
	var salario=parseInt(sal);
	var smlv=$('#txtSMLV').val();
	smlv=(isNaN(smlv) && smlv>0)?616000:smlv;
	var sm=parseFloat(salario/smlv);	
    
    if(sm<=2){
        $('#categoria2').val('A');
    }
    if(sm>2 && sm<=4){
        $('#categoria2').val('B');
    }
    if(sm>4){
        $('#categoria2').val('C');
    }
}


function buscarRadicacion(){
    var campo0 =$('#pendientes').val();
	var idtd=0;
	$("#a0").trigger("click");
	$("#tipoPago2").val('T');
	var cont=false;
	//Buscar datos persona para idpersona
	$.ajax({
		url: URL+'phpComunes/buscarRadicacion2.php',
		type: "POST",
		data: {v0:campo0},
		async: false,
		dataType: "json",
		success:function(data){
			
			
			if(data==0){
				alert("Verifique que se grabo primero la empresa del Independiente!");
				return false;
			}
			$.each(data,function(i,fila){
				$('#radicacion').val(fila.idradicacion);
				$('#textfield').val(fila.fecharadicacion);
				$('#txtNit').val(fila.nit);
				$('#razonSocial').val(fila.razonsocial);
				//Campos ocultos del php
				$("#noPersona").val(fila.numero);
				
				idempresa=fila.idempresa;
				idtd=fila.idtipodocumentoafiliado;
				cont=true;
           });
		},//succes
		complete:function(){
			if(cont==false) return false;
			//Buscar datos persona para idpersona
		   $.ajax({
				url: URL+'phpComunes/buscarPersona2.php',
				type: "POST",
				data: {v0:idtd,v1:$("#noPersona").val()},
				async: false,
				dataType: "json",
				success:function(dato){
					if(dato==0){
						alert("No hay datos de la persona");
						return false;
					}
	
					$.each(dato,function(i,fila){
						$("#idPersona").val(fila.idpersona);
						idPersona=fila.idpersona;
						nom=idPersona+"-"+idtd+"-"+fila.identificacion+" - "+fila.pnombre+" "+fila.snombre+" "+fila.papellido+" "+fila.sapellido;
						$("#nomAfi").html(nom);
						$("#txtHiddenSexoR").val(fila.sexo);
					});
	
					$.getJSON('buscarAfiliacionIdr.php', {v0:campo0}, function(datos){
						if(datos==0){
							alert("No existe una afiliacion asociada a esta radicacion!");
							return false;
						}
	
						$.each(datos,function(i,fila){
							$('#tipForm2').val(fila.tipoformulario);
	            			$('#fecIngreso2').val(fila.fechaingreso);
							$('#tipAfiliacion2').val(fila.tipoafiliacion);
	            			$('#horasDia2').val(8);
	            			$("#horasMes2").val(240);
							$('#tipoPago2').val(fila.tipopago);
							if($('#tipoPago2').val() == '0'){
								$('#tipoPago2').val('T');
							}
							$('#salario2').val(fila.salario);
							$('#agricola2').val(fila.agricola);
							$('#cargo2').val(fila.cargo);
							$('#estado2').val('A');
							idAfiliacion=fila.idformulario;
							//A�adir Idempresa de la afiliacion
							$("#idEmpresa,#idEmpresaTemporal").val(fila.idempresa);
						});
	
						$.getJSON('calcularPrimaria.php',{v0:idPersona,v1:campo0},function(datos2){
							$('#cboTipo2').val(datos2);
						});
						
						//Despues de 2 Seg, activar evento del tipo de afiliaciacion
						setTimeout(function(){
							$('#tipAfiliacion2').trigger("change");
						}
						,2000)
						
					});
				}
	    	});//ajax*/
		
		}
    });
	
 
}

function preliminares(){
	if(idAfiliacion==1)return false;
	idPersona=$("#idPersona").val();
	if(existenDatos==1){
		$.getJSON(URL+'phpComunes/buscarTarjetaIDT.php', {v0:idPersona}, function(datos){
			if(datos==0){
				idTarjeta=0;
				$("#tarjeta").val("Sin bono");
				return false;
			}
				$.each(datos,function(i,fila){
					$("#tarjeta").val(fila.bono);
					idTarjeta=fila.idtarjeta;
					return;
				});
		});
	}

	
}

function urlImagenes(){
	if(idTarjeta==false){
	var fecha=$("#fecNacHide").val();
	var num =$("#identificacion").val();
	var mes=fecha.substring(0,2);
	var dia=fecha.substring(3,5);
	var anio=fecha.substring(6,10);
	var ruta ='digitalizacion/afiliados/'+anio+'/'+mes+'/'+dia+'/'+num;
	$.ajax({
		url: URL+'phpComunes/crearDirectorio.php',
		type: 'POST',
		async: false,
		data: {v0:ruta},
		success: function(datos){
			if(datos==0){
				MENSAJE("El directorio de las imagenes no se pudo crear,<br> informe al administrador del sistema");
				$("#rutaDoc").val('');
				return false;
			}
			$("#rutaDoc").val(datos);
		}
	});
	}
}

function guardarRelacionBene(idtrabajador, idbeneficiario, idparentesco, idconyuge, giro){
	var campo0 = idtrabajador;
	var campo1 = idbeneficiario;
	var campo2 = idparentesco;
	var campo3 = idconyuge;
	var campo4 = giro;
	var x=0;
	$.ajax({
		url: URL+'phpComunes/guardarBeneficiario.php',
		type: "POST",
		async: false,
		data: "submit &v0="+campo0+"&v1="+campo1+"&v2="+campo2+"&v3="+campo3+"&v4="+campo4,
		success: function(datos){
			x=datos;
		}
	});
	return x;
	
}
function guardarRelacion(idT,idB,idTp,idC,conviven,tipRel){
	var campo0 = idT;
	var campo1 = idB;
	var campo2 = idTp;
	var campo3 = idC;
	var campo4 = conviven;
	var campo5 = tipRel;
	var x=0;
	$.ajax({
		url: URL+'phpComunes/guardarConyuge.php',
		type: "POST",
		async: false,
		data: "submit &v0="+campo0+"&v1="+campo1+"&v2="+campo2+"&v3="+campo3+"&v4="+campo4+"&v5="+campo5,
		success: function(datos){
			x=datos;
		}
	});
	return x;
	}


//Cuando llegue al ultimo campo del formulario pasar al siguiente automaticomente	
function abrirTab(op){
	switch(op){
	case 1 : $("#a1").trigger('click'); break;
	case 2 : $("#a2").trigger("click"); break;
	case 3 : $("#a3").trigger("click"); break;
	}
}

function buscarPersona_borrar(num){
	if($("#conviven").val()==0){
		MENSAJE("Seleccione tipo de convivencia!");
		return false;
	}
	var convive=$("#conviven").val();
	if($("#tipoDoc2").val()==0){
	    alert("Falta tipo documento");
	    return false;
	}
	var idtd=$("#tipoDoc2").val();
	$.ajax({
		url: URL+'phpComunes/buscarPersona2.php',
		type: "POST",
		data: "submit &v0="+idtd+"&v1="+num,
		async: false,
		dataType: "json",
		success: function(data){
			if(data==0){
				existe=0;
				idnb=0;
				return false;
			}
			else{
			existe=1;
			$.each(data,function(i,fila){
				idnb=fila.idpersona;
				$('#pNombre2').val($.trim(fila.pnombre));
				$('#sNombre2').val($.trim(fila.snombre));
				$('#pApellido2').val($.trim(fila.papellido));
				$('#sApellido2').val($.trim(fila.sapellido));
				$("#direccion2").val($.trim(fila.direccion));
				$("#cboBarrioC").val(fila.idbarrio);
				$("#telefono2").val(fila.telefono);
				$("#celular2").val(fila.celular);
				$("#email2").val($.trim(fila.email));
				$("#tipoVivienda2").val(fila.idtipovivienda);
				$("#cboDeptoC").val(fila.iddepresidencia).trigger('change');
				setTimeout((function(){
					$("#cboCiudadC").val(fila.idciuresidencia);
				    $("#cboCiudadC").trigger("change");
					 }),1000);
					
					setTimeout((function(){
					$("#cboZonaC").val(fila.idzona);
					$("#cboZonaC").trigger("change");
					}),2000);
					setTimeout((function(){
					$("#cboBarrioC").val(fila.idbarrio);	
					$("#cboBarrioC").trigger("change");
					}),3000);
				$("#estadoCivil2").val(fila.idestadocivil);
				$("#cboDeptoC2").val(fila.iddepnace);
				$("#ciudadNac").val(fila.idciunace);
				$("#capTrabajo2").val(fila.capacidadtrabajo);
				$("#profesion2").val(fila.idprofesion);
				var fecha=fila.fechanacimiento;
				fecha=fecha.replace("/","");
				$('#fecNac2').val(fecha.replace("/",""));
				$('#fecNac2').click().blur();
				$('#sexo2').val(fila.sexo);
			}); //each
//			TABLA DINAMICA PARA DATOS EMPRESA TRABAJDOR ACTIVO
			$("table[name='dinamicTable']").remove();
			var tabla="<table class='tablero' width='100%' border='0' cellspacing='0' cellpadding='0' name='dinamicTable' style='margin-first:8px;'>";
			$.ajax({
				url: URL+'phpComunes/buscarAfiliacion.php',
				type: "POST",
				data: "submit &v0="+idnb,
				async: false,
				dataType: "json",
				success: function(datos){
					if(datos != 0){
						alert("La persona que trata de Afiliar es un trabajador activo!");
					    $.each(datos,function(i,fila){
					    	$('#nitConyuge').val(fila.nit);
					    	$('#empCony').val(fila.razonsocial);
					    	$('#salCony').val(fila.salario);
					    	$('#subCony').val(fila.detalledefinicion);
					    	tabla+="<tr><th>Salario</th><th>Nit</th><th>Raz\u00F3n social</th><th>Tipo</th><th>Detalle</th></tr>"+"<tr><td>$"+ fila.salario +"</td><td>"+ fila.nit +"</td><td>"+fila.razonsocial+"</td><td>"+fila.tipoformulario+"</td><td>"+fila.detalledefinicion+"</td></tr>";
					    });//end each
					  //Agregar tabla despues de la tabla  beneficiarios
					    $("#conyuge .tablero").after(tabla+="</table>");
					    $("table[name='dinamicTable']").hide().fadeIn(800);
					}
					var tabla2="<table class='tablero' width='100%' border='0' cellspacing='0' cellpadding='0' name='dinamicTable' style='margin-first:8px;'>";
					var conv=0;
					$.ajax({
						url: URL+'phpComunes/buscarConvivenciaConyuge.php',
						type: "POST",
						data: "submit &v0="+idnb,
						dataType: "json",
						success: function(dato){
							if(dato!=0){
							 alert("La persona que esta afiliando tiene relacion de convivencia (ver tabla parte inferior)");
							 $.each(dato,function(i,fila){
								   var nombre=fila.pnombre+" "+fila.snombre+" "+fila.papellido+" "+fila.sapellido;
								   conv=fila.conviven
								   idtrelacion=fila.idtrabajador;
								   tabla2+="<tr><th>Identificacion</th><th>Nombre</th><th>Conviven</th></tr>"+"<tr><td>"+ fila.identificacion +"</td><td>"+ nombre +"</td><td>"+fila.conviven+"</td></tr>";
								   return;
							 });
							 if(idPersona==idtrelacion){
								 idtrelacion=1;
							 }
							 $("#div-relacion").append(tabla2+="</table>");
							 $("table[name='dinamicTable']").hide().fadeIn(800);
							 if(conv='S' && idtrelacion==0){
								 $('#conyuge .tablero input:text').val("");
								 $('#conyuge .tablero select').val("0");
							 }
						}
						}
					}); //FIN AJAX 3
				}
			}); //FIN AJAX 2
			
			}//fin else primer success
		}//fin primer success
	})
}
function buscarPersonaTres(obj){
	idnb=0;
	$('#pNombre3').val("");
	$('#sNombre3').val("");
	$('#pApellido3').val("");
	$('#sApellido3').val("");
	$("#fecNacHide3").val('');
	$("#capTrabajo").val("N"); $("#capTrabajo").attr("disabled", false);
	$("#filaDiscapacidad,#filaEscolaridad,#filaUniversidad,#filaSupervivencia,#cerVigencia").hide();
	$("#fDiscapacidad,#fEscolaridad,#fUniversidad,#fSupervivencia,#pInicialV3,#pFinalV3").val('');
	$("#cDiscapacidad,#cEscolaridad,#cUniversidad,#cSupervivencia,#vigencia1,#vigencia2").attr("checked",false);

	if(obj.value!=""){ 
		if($("#parentesco").val()=="0"){
			alert("Seleccione primero un parentesco");
			$("#parentesco").focus();
	    	obj.value='';	    	
	    	return false;
		}
		if($("#tipoDoc3").val()==0){
	   		alert("Falta tipo documento");
	   		$("#tipoDoc3").focus();
		   	obj.value='';
		   	return false;
		}	
	
		validarLongNumIdent($("#tipoDoc3").val(),obj);
		if(obj.value!=""){ buscarPersonaDos(obj.value); }
	}	
}

function buscarPersonaDos(num){
	var idtd=$("#tipoDoc3").val();
	var idp=$("#idPersona").val();
	var parentesco = $("#parentesco").val();
	
	$.ajax({
		url: URL+'phpComunes/buscarPersona2.php',
		async: false,
		data: {v0:idtd,v1:num},		
		beforeSend: function(objeto){
        	dialogLoading('show');
        },        
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }            
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("En buscarPersonaDos Paso lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
		success: function(data){	
		    if(data==0){
		    	existe=0;
		    	buscarPersonaExistente($("#identificacion3").val(),$("#pNombre3").val(),$("#sNombre3").val(),$("#pApellido3").val(),$("#sApellido3").val(),false,existe);
		        return false;
		    } else {
			    $.each(data,function(i,fila){
			    	if ( fila.idpersona == idPersona ) {
						alert("No se puede crear una relacion con la misma persona");		    		
						$("#identificacion3").val(""); 
					   	return false;
					} else if ( fila.estado == "M" ){
						alert("El beneficiario se encuentra FALLECIDO!!!\n"+
							"No es posible relacionar a este beneficiario");		    		
						$("#identificacion3").val(""); 
					   	return false;
					} else {			    	
				    	var max=0;
				    	var salir=false;		    	
				    	idnb=fila.idpersona;
				    	
				    	/*** VALIDACIONES ***/
				    	if(salir==false){
							var retorno = buscarRelacionActivasBeneficiario( $("#idPersona").val(), idnb, parentesco, true, false);
							if ( retorno > 1 ){
								salir=true;
								
								if ( retorno == 4 ){
									//Ya esta la validacion. Ya esta Afiliado
								} else if ( retorno == 3 ){
									alert("Ocurrio un error validando relaciones del Beneficiario!!");						
								} else {					
									$("#divRelacionesActivas").dialog('open');
									
									if ( retorno == 2 ){
										alert("Beneficiario tiene el maximo numero de Relaciones!!");
									} else if ( retorno == 34 ) {
										alert("Tiene afiliaciones Activas como Conyuge!!");						
									} else if ( retorno == 35 ) {
										alert("El beneficiario tiene Afiliaciones Activas como Hijo!!!\n"+
												"Revise si es correcto el parentesco que esta seleccionando");						
									} else if ( retorno == 36 ) {
										alert("El beneficiario tiene Afiliaciones Activas como Padre!!!\n"+
												"Revise el parentesco que esta seleccionando");						
									} else if ( retorno == 37 ) {
										alert("El beneficiario tiene Afiliaciones Activas como Hermano!!!\n"+
												"Revise el parentesco que esta seleccionando");						
									} else if ( retorno == 38 ) {
										alert("El beneficiario tiene Afiliaciones Activas como Hijastro!!!\n"+
												"Revise el parentesco que esta seleccionando");						
									} else if ( retorno == 3538 ) {
										var cadena="EL PADRE ";
										if ( $("#txtHiddenSexo").val() == "F"){
											cadena="LA MADRE ";
										}
										alert("El beneficiario tiene Afiliacion Activa como HIJO!!!\n" +
												cadena + "LE TIENE AFILIADO");						
									} else if ( retorno == 3835 ) {
										var cadena="EL PADRASTRO ";
										if ( $("#txtHiddenSexo").val() == "F"){
											cadena="LA MADRASTRA ";
										}
										alert("El beneficiario tiene Afiliaciones Activa como HIJASTRO!!!\n" +
												cadena + "LE TIENE AFILIADO");						
									}
								}
							}
				    	}
					
						if(salir){				   	
						   	$("#identificacion3").val(""); 
						   	return false; 
						}
		
						salir = (buscarAfiliacionBeneficiario(idnb)==0) ? false : true;
						if(salir){
						   	alert("La persona que trata de Afiliar es un trabajador activo!");
						   	$("#identificacion3").val(""); return false; 
						}	 	
						
						existe=1;
						$('#pNombre3').val($.trim(fila.pnombre));
				    	$('#sNombre3').val($.trim(fila.snombre));
				    	$('#pApellido3').val($.trim(fila.papellido));
				    	$('#sApellido3').val($.trim(fila.sapellido));
				    	var fecha=$.trim(fila.fechanacimiento);
				    	fecha=fecha.replace("-","/");
				    	$('#fecNacHide3').val(fecha.replace("-","/")).trigger('change');	    	
				    	$('#sexo3').val(fila.sexo);
				    	$('#estadoCivil3').val(fila.idestadocivil);	    	
				    	$("#capTrabajo").val(fila.capacidadtrabajo).trigger('change');
				    	if(fila.capacidadtrabajo=="I"){ $("#capTrabajo").attr("disabled", true); }
				    	
				    	$("#cboPaisB").val($.trim(fila.idpais)).trigger('change');
				    	setTimeout((function(){		    		
				    		$("#cboDeptoB").val($.trim(fila.iddepnace)).trigger('change');
				    	}),1200);
				    	setTimeout((function(){		    		
				    		$("#cboCiudadB").val($.trim(fila.idciunace));
				    	}),2400);
				    	
				    	return true;
					}
				}); //each
			}
		},
		type: "GET"
	}); //getJSON
}//end funcion			

function buscarRelacionesAnt(){
	//falta sql
	return false;
var campo0=$("#tipoDoc").val();
var campo1=$("#identificacion").val();
$.ajax({
	url: URL+"phpComunes/buscarIDPersona.php",
	type: "POST",
	async: false,
	data: "submit=&v0="+campo0+"&v1="+campo1,
	dataType: "json",
	success: function(datos){
		if(datos==0){
			MENSAJE("No se pudo el ID del conyuge!");
			return false;
		}
		else{
			alert(datos);
		}
	}
});
}

function validarNumero(obj){
	var td=$("#tipoDoc").val();
}

function nomCorto(pn,pa,sn,sa){
	var patron=new RegExp('[\u00F1|\u00D1]');
	//---Reemplazar Ã'

	for(p=1;p<=pn.length;p++){
		pn=pn.replace(patron,"&");
		}
	for(p=1;p<=sn.length;p++){
		sn=sn.replace(patron,"&");
		}
	for(p=1;p<=pa.length;p++){
		pa=pa.replace(patron,"&");
	}
	for(p=1;p<=sa.length;p++){
		sa=sa.replace(patron,"&");
		}

	txt=pn+" "+sn+" "+pa+" "+sa;
	txt=txt.toUpperCase();
	num=parseInt(txt.length);	

	if(num>30){
		if(sn.length>0){
			sn=sn.slice(0,1);
			txt=pn+" "+sn+" "+pa+" "+sa;
			txt=txt.toUpperCase();
			num=parseInt(txt.length);
		}
		if(num>30)
			if(sa.length>0){
				sa=sa.slice(0,1);
				txt=pn+" "+sn+" "+pa+" "+sa;
				txt=txt.toUpperCase();
				num=parseInt(txt.length);
			}
	}
return txt;
}

function cerrarRadicacion(msg){
    var campo0=$("#pendientes").val();
	$.ajax({
		url: URL+'phpComunes/cerrarRadicacion.php',
		type: "POST",
		data: "submit=&v0="+campo0,
		success: function(datos){
			msg+="<br>"+datos;
			MENSAJE(msg);
			$("#pendientes").find("option[value='"+campo0+"']").remove();
		}
	});
	  }

function validarSubsidio(){
	var afilia=$("#tipAfiliacion").val();
	var formul=$("#tipForm").val();
	if(afilia==0)return;
	if(formul==48 && afilia!=18){
		MENSAJE("Solo tienen derecho a subsidio familiar las afiliaciones de trabajadores DEPENDIENTES!, cambie el tipo de formulario o el tipo de afiliaci&oacute;n");
		$("#tipAfiliacion").val(0);
		$("#tipForm").val(0);
		$("#tipForm").focus();
	}
	
}

/* Nuevo 2016- Andr�s Lara
 * 
 * Buscar Empresa Cuando es Agremiado
 * 
 *
 * */
function buscarEmpresaAgremiada(txtNit){
	
	
	//Setear Datos
	var v0=txtNit;
	//Buscar por Nit
	var v1=1
	$("#empresaAgremiada").empty();
	$("#idEmpresa").val('');
	$("#spnAgremiacion").hide();
	
	$.ajax({
		url: URL+'aportes/empresas/buscarTodas.php',
		type: "GET",
		data: {v0:v0.toUpperCase(),v1:v1},
		async: false,
		dataType: 'json',
		success: function(data){
			
			if(data==''){
				$("#empresaAgremiada").html("NO EXISTE");
				return false;
			}
			
			$("#spnAgremiacion").show();
			var idemp,nitemp,razemp;
			
			$.each(data,function(i,fila){
			
				 idemp=fila.idempresa;
				 nitemp=fila.nit;
				 razemp=fila.razonsocial;
				
								
			});
			
			
			$("#idEmpresa").val(idemp);
			
			if(idemp=='0'){
				$("#empresaAgremiada").html('INVALIDO PARA AGREMIACION');
			}else{
				$("#empresaAgremiada").html(razemp);
			}
			
			
		}
	});//fin ajax
	
}

function onChangeTipoAfiliacionIndp(obj){
	
	//alert("idempresa es: "+$("#idEmpresa").val());
	
	/*------------------------------------
	 *  TIPOS DE AFILIACION
	 * 
	 *  19 		IND FACULTATIVO 2%
	 *  20 		P. VOLUNTARIO 2%
	 *  21 		P. SIS. CAJAS 0%
	 *  23 		FIDELIDAD 0.6%  2%  0.0%
	 *  4209	P. REG. ESPECIAL 0%
	 *  4520	IND AGREMIADO
	 *  4521	IND EXTRANJERO
	 *  3320	EXCENTO
	 *  2938	DESEMPLEADO
	 *  
	 -----------------------------------*/
	
	//Resetear y ocultar todas las opciones
	$("#indice").val(0);
	$("#indice option:not(':first')").attr("hidden", true);
	$("#spnAgremiacion").hide();
	$("#empresaAgremiada").empty();
	$("#cboReside,#cboPais").attr("disabled","disabled");
	$("#txtNit,#idEmpresa").val('0');
	//Fila Nit
	$("#filaNit:visible").hide();
	
	$("#tablaAfiTab  #cboPais").val('48');
	
	//Mostrar Indice de Aportes segun tipo de Afiliacion
	if(obj.value==4209 || obj.value==21){
		$("#indice option[value='4006']").attr("hidden", false);	
	}else if(obj.value==20){
		$("#indice option[value='107']").attr("hidden", false);
	}else if(obj.value==19||obj.value==4521){
		
		//Habilitar los campos pais cuando es Ind. Extranjero
		if(obj.value==4521){
			$("#cboReside,#cboPais").removeAttr("disabled");
		}
	
		$("#indice option[value='106'] ,#indice option[value='107']").attr("hidden", false);
	}else if(obj.value==2938){
		$("#indice option[value='106']").attr("hidden", false);
	}else if(obj.value==18 || obj.value==23 || obj.value==3320){
		$("#indice option[value='107'], #indice option[value='106'], #indice option[value='4006']").attr("hidden", false);
	}else if(obj.value==4520 ){
		
		$("#filaNit").show();
					
			//Buscamos la empresa Agremiada, despues de 2 Seg que termine de cargar la Pagina.
			setTimeout(function(){
				
				var idempresaAgremiada=$("#idEmpresaTemporal").val();
				
				//Buscar Empresa Agremiada
				$.ajax({
					url: URL+"aportes/empresas/buscarEmpresa1.php",
					type: "POST",
					data: {v0:idempresaAgremiada},
					dataType: "json",
					success: function(empresa){
						if(empresa==0){
							MENSAJE("No existe la empresa agremiada.");
							return false;
						}
						else{
							$("#empresaAgremiada").html(empresa[0].razonsocial);
							//Mostrar datos de la empresa agremiada
							$("#spnAgremiacion").show();
							$("#txtNit").val(empresa[0].nit);
							$("#idEmpresa").val(empresa[0].idempresa);
						}
					}
				});
				
			},1000);
			
			
		
		
		
		//Mostrar independientes 2% y agremiados 2%
		$("#indice option[value='4522'],#indice option[value='106']").attr("hidden", false);
	}
	
	
	//Recalcular la categoria
	$("#salario2").focus().trigger("blur");
}

function actualizarAfiliacion2(){
	setCookie('indiceCookie','',-1);
	
	//------------------DATOS AFILIACION----------------------
	
	var idradicacion=$("#pendientes").val();
	var tipoformulario= $("#tipForm2").val();
	var tipoafiliacion= $("#tipAfiliacion2").val();
	var nit				=$("#txtNit").val();
	var idempresa		=$("#idEmpresa").val();
	var fechaingreso=   $("#fecIngreso2").val();
	var horasdia=	    $("#horasDia2").val();
	var horasmes=       $("#horasMes2").val();
	var tipopago=		$("#tipoPago2").val();
	var idpais=			$("#cboPais").val();		
	var salario=		$("#salario2").val();
	var categoriaSalario=$("#categoria2").val();
	var agricola=		$("#agricola2").val();
	var cargo=			$("#cargo2").val();
	var primaria=		$("#cboTipo2").val();
	var estado=			$("#estado2").val();
	var codigo=			$("#codigo").val();
	var indiceAportes = $("#indice").val();
	setCookie("indiceCookie", indiceAportes, 1);
	
	
	//------------------DATOS ACTIVIDAD ECONOMICA INDEPENDIENTE---
	
	var idciiu		=$("#select7").val();
	var categoria	=$("#codigoCIU").val();
	
	
	//------------------VALIDACIONES----------------------
	
	var error=0;
	$("table.tablero :input.ui-state-error,select.ui-state-error").removeClass("ui-state-error");
	
	
	if(tipoformulario=='0'){ 
   		$("#tipForm2").addClass("ui-state-error");
   		error++;
   	}
	if(tipoafiliacion=='0'){ 
   		$("#tipAfiliacion2").addClass("ui-state-error");
   		error++;
   	}
	
	if($("#filaNit").is(":visible")){
		if(nit==''||nit=='0'){
			$("#txtNit").addClass("ui-state-error");
			error++
		}else{
			//Sino si no existe la empresa, genere error
			if(idempresa==''){
				$("#txtNit").addClass("ui-state-error");
				error++
			}
			
			//Sino si  la empresa es igual al id persona, genere error
			if(nit==$("#noPersona").val()){
				$("#txtNit").addClass("ui-state-error");
				$("#empresaAgremiada").html("INVALIDO EMPRESA");
				error++
			}
		}
	}
	if(fechaingreso.length==0){ 
   		$("#fecIngreso2").addClass("ui-state-error");
   		error++;
   	}
	if(horasdia.length==0){ 
   		$("#horasDia2").addClass("ui-state-error");
   		error++;
   	}
	if(horasmes.length==0){ 
   		$("#horasMes2").addClass("ui-state-error");
   		error++;
   	}
	if(tipopago=='0'){ 
   		$("#tipoPago2").addClass("ui-state-error");
   		error++;
   	}
	if(salario=='' || isNaN(salario)){ 
   		$("#salario2").addClass("ui-state-error");
   		error++;
   	}
	if(categoriaSalario.length==0){ 
   		$("#categoria2").addClass("ui-state-error");
   		error++;
   	}
	if(agricola=='0'){ 
   		$("#agricola2").addClass("ui-state-error");
   		error++;
   	}
	if(cargo=='0'){ 
   		$("#cargo2").addClass("ui-state-error");
   		error++;
   	}
	if(indiceAportes=='0'){ 
   		$("#indice").addClass("ui-state-error");
   		error++;
   	}
	
	if(idciiu=='0'){ 
   		$("#select7").addClass("ui-state-error");
   		error++;
   	}
	
	if(categoria==''){ 
   		$("#codigoCIU").addClass("ui-state-error");
   		error++;
   	}
	 
	if(error>0){
		MENSAJE("Llene los campos obligados!");
		return false; 
	}
	
	/*
	 * ACTUALIZAR pais inicialmente si NO es colombia '48'
	 */
	
	if(idpais!='48'){
		$.ajax({
			type:"POST",
			url:"actualizarPais.php",
			data:{idpais:idpais,idpersona:idPersona},
			async:false,
			success:function(data){
					if(data=='1'){
						alert("Pais de residencia Actualizado");
					}
					
			},
			error:function(xhr,obj,wtf){
				alert("Error "+obj+" al actualizar pais de residencia "+wtf);
				}
			});
	}
	
	/*
	 * VERIFICAR ACTIVIDAD ANTES , INSERTAR ACTIVIDAD ECONOMICA INDEPENDIENTE
	 * */
	
	$.ajax({
		url: URL+'aportes/trabajadores/buscarActividadIndependiente.php', 
		type: "POST",
		data: {idpersona:idPersona,idradicacion:idradicacion},
		dataType: "json",
		success: function(actividad){
			
			var msjeActividad="";
			
			if(actividad==false){
				
				/////////////////////////////////////////////////////////////////////////////////
				$.ajax({
					type:"POST",
					url:"actualizarActividadIndependiente.php",
					data:{idciiu:idciiu,categoria:categoria,idpersona:idPersona,idradicacion:idradicacion,accion:"I"},
					success:function(data){
						
							if(data=='1'){
								msjeActividad=". Actividad Creada correctamente!!";
								
							}
							
					},
					error:function(xhr,obj,wtf){
						msjeActividad="Error "+obj+" al insertar actividad economica "+wtf;
						}
					});
				/////////////////////////////////////////////////////////////////////////////////
				
				
			}else{
				
				msjeActividad=" , actividad economica independiente existente con el No. radicado "+idradicacion;
			}
			
			
			/*
			 * ACTUALIZAR AFILIACION
			 */
			
			$.getJSON('actualizarAfiliacion.php',{v0:idAfiliacion,v1:tipoformulario,v2:tipoafiliacion,v3:idempresa,v4:fechaingreso,v5:horasdia,v6:horasmes,v7:salario,v8:agricola,v9:cargo,v10:primaria,v11:estado,v12:tipopago,v13:categoriaSalario,v15:idPersona,v16:"" ,indice: indiceAportes, estadoActual: estadoActualAfiliacion},function(datos){
				
				if(datos==0){
					MENSAJE("El registro NO fue actualizado!");
				}else{
					
					$("#div-msje").remove();
					//cerrarRadicacion(msg);
					alert("Registro actualizado "+msjeActividad);
					//Mostramos el resto de tabs
					$("#tabsR ul li a:gt(0)").show();
				    $("#actualizarR").hide();
				}
			});
		}//success
	});	
	
	
	
	

}