var arrIde=new Array();
var arrID=new Array();
var idEmpresa=0;
var idPersona=0;

//Evento para actualizar datos de beneficiarios y conyuges ACTUALIZA BASICO
$("body").delegate("label[name='labelActualizaBasico']","click",function(){
	var idpersonaLabel=parseInt($(this).text());
    actualizaBasico(idpersonaLabel);
});

//Boton modificar beneficiario
$("[name='actBen']").live("click",function(){
	 //validarComboParentesco();
	//Id global del beneficiario seleccionado
	 idbeneficiario=$(this).parent().parent().children(":eq(0)").text();
	 var temp=($(this).parent().parent().children(":eq(5)").text()).split(' ');
	 cedulaPadreBeneficiario=temp[0];
	 idrelacion=$(this).parent().parent().children(":eq(15)").text();	 
	//Enviar parametros
	 p=$.trim($(this).parent().parent().children(":eq(4)").text());
	 buscarBeneficiarioUpdate(idbeneficiario,p,cedulaPadreBeneficiario,idrelacion);
});

$(document).ready(function(){
	var smlv=salarioMinimo();
	smlv=(isNaN(smlv) && smlv>0)?616000:smlv;
	$("#txtSMLV").val(smlv);
	

var URL=src();
$("#buscarPor").change(function(){
$("#pn,#pa,#idT").val('');
	if($(this).val()=='2'){
		$("#tipoDocumento").hide();
		$("#pn,#pa").show();
		$("#idT").hide();
	}else{
		$("#tipoDocumento").show();
		$("#pn,#pa").hide();
		$("#idT").show();
		$("#idT").focus();
	}
});

//Buscar Trabajador
$("#buscarT").click(function(){
	eventClick();
});//fin click
});//fin ready

var nuevo=0;
var modificar=0;
var continuar=true;
var msg="";

$(function() {
		$("#ayuda").dialog({
		 	autoOpen: false,
			height: 450, 
			width: 700, 
			draggable:true,
			modal:false,
			open: function(evt, ui){
					$('#ayuda').html('');
					$.get(URL+'help/aportes/modificararfiliaciontrabajador.html',function(data){
							$('#ayuda').html(data);
					});
			 }
		});
		$("#txtFAsignacion").datepicker({
			changeMonth: true,
		    changeYear: true,
		    constrainInput: false,
		    maxDate: '+OD',
		});//fin dialog
	});

function runSearch(e){
	if(e.keyCode == 13){
		eventClick();
	} 
}


function observacion(id,tipo){
	//Andres lara Sep 28 de 2011
	var URL=src();
	$("div[name='div-observaciones-tab']").dialog("destroy");
	$("textarea[name='observacionGrupo']").val("");
	$("div[name='div-observaciones-tab']").dialog({
		  modal:true,
		  width:520,
		  resizable:false,
		  draggable:false,
		  closeOnEscape:false,
		  open:function(){
			  $(this).prev().children().hide();//Oculto la barra de titulo
			  $(this).prev().html("OBSERVACIONES...");//Pongo un nuevo titulo
			  $("textarea[name='observacionGrupo']").focus();
			  },
		  buttons:{
		    'Guardar':function(){
		    	var comentario='Cambio Fecha de Asignacion:';
		    	var observacioncaptura=$("textarea[name='observacionGrupo']").val();
				var observacion=comentario+' - '+observacioncaptura;
				var observacionSinEspacios = observacion.replace(/^\s+|\s+$/g,"");
				//Si observacion tiene algun valor, se guarda de lo contrario no pasa nada
			    if(observacionSinEspacios!=""){
				 $.ajax({
					   url:URL+"phpComunes/pdo.insert.notas.php",
					   type:"POST",
					   data:{v0:id,v1:observacion,v2:tipo},
					   success:function(data){
					    $("[name='rtaObservacion']").fadeIn();//el mensaje esta oculto en la capa div-observacion
						$("div[name='div-observaciones-tab']").dialog("close");
					   }
			     });//ajax
				 alert("Observacion guardada"); 
				}//if observacion
				else{
					alert("No se pudo guardar la observaci\xf3n");
				}
			 }//guardar
		  }//buttons
		  });   
} 

function buscarBeneficiarioUpdate(idbenef,p,cedulaPadreBeneficiario,idrel){
	jQuery('div[aria-labelledby^=ui-dialog-title-modificarBeneficiarios]').remove();
	jQuery('div[class^=ui-effects-wrapper]').remove();
	$("#modificarBeneficiarios").dialog('destroy');
	
	idbenef=$.trim(idbenef);
	 $.ajax({
		url:URL+'phpComunes/buscarDatosRelacionID.php',
		type:"POST",
		data:{v0:idrel},
		dataType:"json",
		async:false,
		success:function(data){
			if(data==0){
				alert("No se encontro el beneficiario!! "+idbenef+"-"+p+"-"+$("#idPersona").val());
				return false;
		    } else {
		    	$("#modificarBeneficiarios").dialog({
		    		show: "drop",
		    		hide: "clip",
		    		width:750,
		    		modal: true,
		    		open:function(event,ui){
		    			$("#parentescoUpd").focus();
		    			$("#padreBiologicoUpd").hide();
	 					$("#nombreBiologicoUpd").hide();
		    			
		    			$.each(data,function(i,fila){		    				
		    				$("#hidIdRelacion3Upd").val(idrel);
		    				$("#hidIdPersona3Upd").val(fila.idpersona);
							$('#tipoDoc3Upd').val(fila.idtipodocumento);
							$('#identificacion3Upd').val(fila.identificacion);
					   	 	$('#pNombre3Upd').val(fila.pnombre);
					   	 	$('#sNombre3Upd').val(fila.snombre);
					   	 	$('#pApellido3Upd').val(fila.papellido);
					   	 	$('#sApellido3Upd').val(fila.sapellido);
					   	 	$('#fecNac3Upd').val(fila.fechanacimiento);
					   	 	$('#sexo3Upd').val(fila.sexo);
					   	 	$('#capTrabajoUpd').val(fila.capacidadtrabajo);					   	 	
					   	 	$("#parentescoUpd").val(fila.idparentesco);
					   	 	$("#estadoUpd").val(fila.estado);
					   	 	$("#txtFAsignacion").val(fila.fechaasignacion);
					   	 	$("#cedMamaUpd").val(fila.idconyuge);
					   	 	$("#txtFechaAfiliacion").val(fila.fechaafiliacion);
					   	 	$("#hdnEstadoUpd").val(fila.estado);
					   	 	if(fila.estado == 'I'){
					   	 		$("#hdnMotivoUpd").val(fila.idmotivo);
					   	 		$("#fechaRetiro").val(fila.fechaestado);
					   	 	}
					   	 	if(fila.idparentesco==36 || fila.idparentesco==37){
				 			 	$("#cedMamaUpd").attr("disabled",true);
				 			} else {
				 				if(fila.idparentesco == 38 && fila.idbiologico>0 ){
				 					$("#padreBiologicoUpd").show();
				 					$("#nombreBiologicoUpd").show();
				 					$("#tipoIdPadreBiolUpd").val(fila.idtipodocumentoBiol);
				 					$("#IdPadreBiolUpd").val(fila.identificacionBiol).trigger("blur")
				 				}
				 				
				 			 	//$("#cedMamaUpd").attr("disabled",false);				 			 	
				 			 	if(parseInt(cedulaPadreBeneficiario)>0)
				 					$('#cedMamaUpd').children('option[text='+cedulaPadreBeneficiario+']').attr('selected','selected');
				 			}			    		 
					   	 	if(fila.giro=='S'){
					   	 		$("#tipoAfiliacion3Upd").val(48);
					   	 	} else {
					   	 		$("#tipoAfiliacion3Upd").val(49);
					   	 	}					   	 	
						}); //fin each
		    		},		    		
		    		buttons: {
		    			'Actualizar': function() {
			    		    var validado=validarCamposBen();
			    			if(validado>0){ return false; }
			    			
			    			var idPersona=$("#idPersona").val();
			    			
			    			var idRelacion=$("#hidIdRelacion3Upd").val();
			    			var idBeneAct=$("#hidIdPersona3Upd").val();
			    			var parentesco=$("#parentescoUpd").val();
			    			var idpadremadre=$("#cedMamaUpd").val();
			    			var idpadrebiol=0;
			    			var tdoc=$("#tipoDoc3Upd").val();
			    			var num=$("#identificacion3Upd").val();
			    		   	var pnom=$("#pNombre3Upd").val();
			    			var snom=$("#sNombre3Upd").val();
			    			var pape=$("#pApellido3Upd").val();
			    			var sape=$("#sApellido3Upd").val();
			    			var sexo=$("#sexo3Upd").val();
			    			var fecha=$("#fecNac3Upd").val();
			    			var tafil3=($("#tipoAfiliacion3Upd").val()==48)?'S':'N';
			    			var capTrab=$("#capTrabajoUpd").val();
			    			var fasignacion=$("#txtFAsignacion").val();
			    			var fafiliacion=$("#txtFechaAfiliacion").val();			    			
			    			var estado=$("#estadoUpd").val();
			    			var estadoAnt=$("#hdnEstadoUpd").val();
			    			var motivo="";
		         			var fecharetiro = "";
			    			
			    			if ( parentesco == '38' && ($("#padreBiologicoUpd").is(":visible")) && parseInt($("#hideIdbiolUpd").val()) > 0 ) {
			    				idpadrebiol=parseInt($("#hideIdbiolUpd").val());         			
			    			}			    			
			    			if ( estado == 'I' ){
			         			motivo=( parseInt($("#hdnMotivoUpd").val()) > 0 ) ? $("#hdnMotivoUpd").val() : 0;
			         			fecharetiro = $("#fechaRetiro").val();			         			
			    			}
			    			
			    			$.ajax({
								url: URL +"phpComunes/verifIdentificacionDisponible2.php",
								type: "POST",
								data: {idTipo: tdoc, numero: num, idPersona: idBeneAct},
								async: false,
								dataType: "json",
								success: function(respuesta){
									if(respuesta){
										$("#tbDocumentoDoble").empty();
										$.each(respuesta,function(i,fila){
											nom = fila.pnombre + " " +fila.snombre +" "+ fila.papellido + " " + fila.sapellido;
											$("#tbDocumentoDoble").append(
													"<tr>" +
														"<td>" + fila.codigo + "</td>" +
														"<td style='text-align:right'>" + fila.identificacion + "</td>" +
														"<td>" + fila.idpersona + " - " + nom + "</td>" +									
													"</tr>");
										});
										alert("Verifique la informaci\xf3n, ya existe una persona con ese tipo y numero de documento!!!");
										$("#divDocumentoDoble").dialog("open");
										return false;
									} else {																				
										$.ajax({
						    				url:"pdo.act.afiliacion.php",
						    				type:"POST",
						    				async:false,
						    				data:{v0:idRelacion,v1:tafil3,v2:estado,v3:motivo,v4:fasignacion,v5:fafiliacion,v6:fecharetiro,v7:parentesco,v8:idpadremadre,v9:idpadrebiol},
						    				success:function(data){
						    			 		if(data==0){
						    			    		alert("No se pudo modificar la afiliaci\u00F3n del beneficiario.");
						    						return false;
						    			 		} else if (data>0) {
						    			 			alert("Afiliacion actualizada!");
						    						$("#modificarBeneficiarios").dialog("close");
						    						//buscarGrupo();
						    						var patron = /\W/g;						    						
						    						$("#tdFechaAsignacion"+num).text(fasignacion.replace(patron,"-"));
						    						observacion(idPersona,1);					
						    					} else {
						    						alert(datos);
						    						return false;
						    					}
						    				}
						    			});
									}
								}
							});			    			
		    			}			    					    					    			
		    		},		    		
		    		close:function(){			
		    			$("#modificarBeneficiarios ul").remove();
		    			$("#modificarBeneficiarios  table.tablero input:text").val('');
		    			$("#modificarBeneficiarios  table.tablero input:hidden").val('');//fecha nacimiento
		    			$("#modificarBeneficiarios  table.tablero select").val('Seleccione');
		    			$("#modificarBeneficiarios input:checkbox,#modificarBeneficiarios input:radio").attr("checked",false);
		    			$("#modificarBeneficiarios .tablero :input.ui-state-error,#modificarBeneficiarios .tablero select.ui-state-error").removeClass("ui-state-error");
		    			// TABLA DINAMICA PARA DATOS EMPRESA TRABAJADOR ACTIVO
		    			$("table[name='dinamicTable']").remove();
		    			$("#motivoUpd").attr("disabled",true);
		    			$("#motivoUpd").val(0);
		    			$("#fechaRetiro").attr("disabled",true);
		    			$("#fechaRetiro").val('');
		    			jQuery('div[aria-labelledby^=ui-dialog-title-modificarBeneficiarios]').remove();
		    			jQuery('div[class^=ui-effects-wrapper]').remove();
		    			//$("#cedMamaUpd").attr("disabled",false);
		    		}//end funcion close											
		    	 });//termina funciones del dialog
		    }
		 }//fin funcion success
	 }); 	
}

function validarCamposBen(){
	num=$.trim($("#identificacion3Upd").val());
    var error=0;
	//reseteo clase error
	$("#modificarBeneficiarios .tablero :input.ui-state-error,#modificarBeneficiarios .tablero select.ui-state-error").removeClass("ui-state-error");
	
	if($("#parentescoUpd").val()=='0'){
		$("#parentescoUpd").addClass("ui-state-error");
		error++;
		}
	if( $("#parentescoUpd").val() == '38' && ($("#padreBiologicoUpd").is(":visible")) && ( isNaN($("#hideIdbiolUpd").val()) || $("#hideIdbiolUpd").val()<1 || ($("#hideIdbiolUpd").val()).length == 0 )){
		$("#IdPadreBiolUpd").addClass("ui-state-error");
		error++;		
	}
	if($("#tipoDoc3Upd").val()=='0'){
	$("#tipoDoc3Upd").addClass("ui-state-error");
	error++;
	}
	if(num.length==0||num==''){
    $("#tipoDoc3Upd").addClass("ui-state-error");
	error++;
	}
	if($("#pNombre3Upd").val()==''){
	$("#pNombre3Upd").addClass("ui-state-error");
	error++;
	}
	if($("#pApellido3Upd").val()==''){
	$("#pApellido3Upd").addClass("ui-state-error");
	error++;
	}
	if($("#sexo3Upd").val()=='0'){
	$("#sexo3Upd").addClass("ui-state-error");
	error++;
	}
	if($("#fecNac3Upd").val()==''){
	$("#fecNac3Upd").addClass("ui-state-error");
	error++;
	}	
	if( $("#tipoAfiliacion3Upd").val() == "48" && $("#txtFAsignacion").val()==''){
		$("#txtFAsignacion").addClass("ui-state-error");
		error++;
	}
	if($("#txtFechaAfiliacion").val()==''){
		$("#txtFechaAfiliacion").addClass("ui-state-error");
		error++;
	}
	
	if(error==0){
	return 0;
	}else{
	return error;
	}
}

function buscarGrupo(idp){
	
	//BUSCAMOS LOS BENEFICIARIO DE ACUERDO A LAS RELACIONES DE CONVIVENCIA
	$.ajax({
		url:URL+"phpComunes/pdo.buscar.grupo2.php",
		async: false,
		data :{v0:idp},		
		beforeSend: function(objeto){
        	dialogLoading('show');
        },        
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }            
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("En buscarGrupo Paso lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
		success:function(data){
			$("#tabTableGrupo tbody tr").remove();		
			if(data){
				var cadena="";

				$.each(data,function(i,fila){
					idrel=fila.idrelacion;
					idb=fila.idbeneficiario;
					idparent=fila.idparentesco;
					idc=fila.idconyuge;
					nombreB=$.trim(fila.pnombre)+" "+$.trim(fila.snombre)+" "+$.trim(fila.papellido)+" "+$.trim(fila.sapellido);
					nombreC=$.trim(fila.pnombreC)+" "+$.trim(fila.snombreC)+" "+$.trim(fila.papellidoC)+" "+$.trim(fila.sapellidoC);
					
					identificacionConyuge=fila.identificacionConyuge;								
					if(fila.identificacionConyuge != ""){	
						identificacionConyuge="<a class='dialogoEmergente' >" +  identificacionConyuge + "<span>" + nombreC + "</span></a>"; 
					}
					motivo="<a class='dialogoEmergente' >" +  fila.estadoAfiliacion + "<span>" + fila.fechaEstadoAfiliacion + " " + fila.motivoInactivo + "</span></a>";
					fechagiro="<a class='dialogoEmergente' >" +  fila.giro + "<span>" + fila.fechaGiroAfiliacion + "</span></a>";
					
					estadoBeneficiario=fila.fechaEstadoBeneficiario;
					if(fila.estadoBeneficiario=='M'){									
						if ( fila.tipoestado == 0 ) {
							estadoBeneficiario = estadoBeneficiario + " Por Subsidio Funebre";
						} else if ( fila.tipoestado == 1 ) {
							estadoBeneficiario = estadoBeneficiario + " Por Proceso Cruce Registraduria";
						} else if ( fila.tipoestado == 2 ) {
							estadoBeneficiario = estadoBeneficiario + " Sin Subsidio Funebre";
						} 									
					}
					
					
					cadena+="<tr>" +
								"<td style='text-align:center' >" + fila.codigo + "</td>" +
								"<td style='text-align:right' ><label (" + idb + ",'" + fila.giro + "','" + fila.idparentesco + "','" + fila.discapacitado + "'); >" + fila.identificacionBeneficiario + "</label></td>" +
							    "<td style='text-align:left' >" + idb + " - " + nombreB + "</td>" +
							    "<td style='text-align:center' >" + fila.parentesco + "</td>" +
							    "<td style='text-align:center' ><a class='dialogoEmergente' >" + fila.estadoBeneficiario + "<span>" + estadoBeneficiario + "</span></a></td>" +	    
							    "<td style='text-align:center' >" + fila.fechanacimiento + "</td>" +
							    "<td style='text-align:center' >" + Math.floor(fila.edad) + "</td>" +
							    "<td style='text-align:center' >" + fila.fechaafiliacion + "</td>" +
							    "<td style='text-align:center' ><a ('" + idb + "') >" + fila.fechapresentacion + "</a></td>" +
							    "<td style='text-align:center' id='tdFechaAsignacion"+fila.identificacionBeneficiario+"'>" + fila.fechaasignacion + "</td>" +
							    "<td style='text-align:center' >" + identificacionConyuge + "</td>" +
							    "<td style='text-align:center' ><label (" + idrel+",'" + fila.giro + "','" + fila.fechaasignacion + "',false,'" + fila.discapacitado + "','" + idparent+ "','" + fila.fechanacimiento + "','" + idb + "','" + fila.estadoAfiliacion+"','" + fila.estadoBeneficiario + "');>" + fechagiro + "</label></td>" +
							    "<td style='text-align:center' >" + fila.discapacitado + "</td>" +
							    "<td style='text-align:center' >" + fila.embarga + "</td>" +
							    "<td style='text-align:center' ><label style=cursor:pointer onclick=actualizarEstadoBeneficiario(" + idrel + "," + idb + "," + idparent + ",'" + fila.estadoAfiliacion+"','" + fila.estadoBeneficiario + "','" + fila.giro + "','" + fila.discapacitado + "','" + fila.fechanacimiento + "','" + idc + "');>" + motivo + "</label></td>" +
							    "<td style='text-align:center' ><img style='cursor:pointer' src='"+URL+"imagenes/menu/modificar.png' id='actBen"+idb+"' title='Modificar Beneficiario' onclick=buscarBeneficiarioUpdate(" + idb + ",'" + fila.parentesco + "','" + fila.identificacionConyuge + "'," + fila.idrelacion + "); /></td>"
					    	"</tr>";					
				});//each
				con=data.length;
				$("#tabTableGrupo tbody").append(cadena);
			} else {
				msg='';
				msg+="El trababajador NO tiene beneficiarios.";
				MENSAJE(msg);
				return false;
			}			
		},//succes
		type: "POST"
	});//ajax beneficiarios		
}

function eventClick(){
	$("#trabajadores").find("p").remove();
	//espera
	dialogLoading("show");
	
	if($("#idT").is(":visible")&&$("#idT").val()==''){
		$(this).next("span.Rojo").html("Ingrese un valor a buscar.").hide().fadeIn("slow");
		dialogLoading("hide");
		$("#idT").focus();
		return false;
	}	
	if($("#pn").is(":visible")&&$("#pn").val()==''&&$("#pa").val()==''){
		$(this).next("span.Rojo").html("Ingrese un valor a buscar.").hide().fadeIn("slow");
		dialogLoading("hide");
		$("#pn").focus();
		return false;
	}	
		
	var v0=$("#idT").val();
	var v1=$("#buscarPor").val();
	var v2=$("#tipoDocumento").val();
	var v3=$("#pn").val();//campo nombre
	var v4=$("#pa").val();//campo nombre
	var contadorAfi=0;
	var contadorTemp=0;
	var cadenaAfiliados='';
	
	$.ajax({
		url: URL+'phpComunes/buscarPersonaPara.php',
		type: "POST",
		data: {v0:v0,v1:v1,v2:v2,v3:v3.toUpperCase(),v4:v4.toUpperCase()},
		async: false,
		dataType: 'json',
		success: function(data){
			if(data==0){
				$("#buscarT").next("span.Rojo").html("No se encontr&oacute; la persona").hide().fadeIn("slow");
				dialogLoading("hide");	
				return false;
			}		
			$("#lstT").remove();
			
			$.each(data,function(i,fila){
				$("#buscarT").next("span.Rojo").html(""); 
				$("#idT").val('');
				idp=fila.idpersona;
				$("#idPersona").val(idp);
				document.gete
				nom =fila.pnombre+" "+fila.snombre+" "+fila.papellido+" "+fila.sapellido;
				$.ajax({
					url:URL+'phpComunes/buscarAfiliacionIDP.php',
					type: "POST",
					data: {v0:idp},
					async: false,
					dataType: 'json',
					success: function(datos){
						if(datos==0){
							$("#buscarT").next("span.Rojo").html("No se encontraron AFILIACIONES.").hide().fadeIn("slow");
							dialogLoading("hide");	
							return false;
						}
						$.each(datos,function(i,fila){
							if(fila.idempresa!=null) {
								arrIde[i]=fila.idempresa;
							}
							return;
						});						
						contadorAfi++;
						cadenaAfiliados+='<p align="center" style="cursor:pointer;" onclick="buscarGrupo('+idp+')">'+idp+" "+nom+'</p>';
					}//success
				});
			});//fin each	for
		    
			$("#trabajadores").append(cadenaAfiliados);
			dialogLoading("hide");	
			$("#trabajadores").before("<p id='lstT' align='center'>Trabajadores encontrados:<b> "+contadorAfi+"</b></p>");					
		}//success
	});
}