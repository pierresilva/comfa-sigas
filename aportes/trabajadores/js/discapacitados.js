URL=src();
nuevo=0;
var idPersona=0;

$(document).ready(function(){
	$("#fecha").datepicker({
		changeMonth: true,
		changeYear: true,
		maxDate: "+0D",
		minDate: "-2M"
	});
	
	
	
//Dialog Colaboracion en linea
  $("#dialog-form2").dialog({
		autoOpen: false,
		height: 400,
		width: 500,
		draggable:false,
		modal: true,
		buttons: {
		    'Enviar': function() {
			var bValid = true;
			var campo=$('#notas').val();
			var campo0=$.trim(campo);
			if (campo0==""){
				$(this).dialog('close');
					return false;
				}
			var campo1=$('#usuario').val();
			var campo2="empresaSinConv.php";
			$.post(URL+'phpComunes/colaboracion.php',{v0:campo0,v1:campo1,v2:campo2},
			function(datos){
				if(datos=='1'){
					alert("Su comentario fue enviado correctamente!. Gracias por participar en el mejoramiento de SIGAS");
				}else{
					alert(datos);
				}
		});
		$(this).dialog('close');
		},
		Cancelar: function() {
			$(this).dialog('close');
			$("#dialog-form2").dialog("close");
		}
	}
  });
   //Dialog ayuda

  
  $("#ayuda").dialog({
	autoOpen: false,
	height: 450,
	width: 700,
	draggable:true,
	modal:false,
	open: function(evt, ui){
		$('#ayuda').html('');
		$.get(URL +'help/aportes/ayudaDiscapacitados.html',function(data){
			$('#ayuda').html(data);
		});
	}
  });
  
	$("#buscarPor_rad").bind("click",function(){
		//se cargan las radicaciones de certificados para grabarlos
		$("#td_radicaciones").show();
		$("#fecha").attr("disabled",true);
		var idTipoRad = 32; //iddetalledef en 091
		$('#idradicacion option').remove();
		$.post(URL+'phpComunes/buscarRadicaTipoID.php',{v0:idTipoRad},function(datos){
			var cmbRadicacion = $("#idradicacion");
			$(cmbRadicacion).html(datos);
			if($(cmbRadicacion).children().length==1){
				alert('Lo lamento NO hay radicaciones pendientes de grabar!');
				return false;
			}
		});
	});
  
  $("#buscarPor_form").bind("click",function(){
	  // debe permitir grabar la discapacidad sin pedir n�mero de radicaci�n
	  $("#td_radicaciones").hide();
	  $("#fecha").attr("disabled",false);
	  $("#idradicacion").empty();
  });
  $("#buscarPor_form").trigger("click");
  
  $("#idradicacion").bind("change",function(){
	  buscarRadicacion($(this).val());
  });
  
});//ready

function nuevoD(){
	nuevo=1;
	$("#bGuardar").show();
	limpiarCampos();
	$('#idradicacion option').remove();
	$("#buscarPor_form").trigger("click");
}
//---------------------------------------GUARDAR VERTIFICADO
function guardar(){
   if(nuevo==0){
	   MENSAJE("Si es un registro nuevo haga click en NUEVO.");
	   $("#bGuardar").show();
	   return false;
   }
   idb = $("#beneficiarios").val();
   if($("#beneficiarios").val()==0){
	   alert("Seleccione el beneficiario discapacitado!");
	   $("#beneficiarios").focus();
	   return false;
	}
    var ingresaPor = $("input[name=buscarPor]:checked").val();
	var fecha=$("#fecha").val();
	var tpar =$("#beneficiarios option:selected").attr('id');
	if(fecha.length==0 || !fecha.match(/^[0-9]{4}\/[0-9]{2}\/[0-9]{2}$/) || fecha == ''){
		alert("Verifique la fecha del certificado.");
		$("#fecha").focus();
		return false;
	}
	
	$.ajax({
		url: 'marcarDiscapacitado.php',
		type:"POST",
		dataType:"json",
		data:{v0:idPersona,v1:idb,v2:tpar, fechaPresentacion: fecha},
		success:function(datos){
			var msg = "";
			if(datos==1){
				msg = "Beneficiario marcado como Discapacitado exitosamente.<br/>Certificado de Discapcidad grabado.<br/>Certificado de supervivencia grabado.";
				if(ingresaPor == "radicacion"){
					cerrarRadicacion($("#idradicacion").val());
				}
			}else{
				msg = "Hubo problemas al enviar los datos: \n"+datos;
			}
			MENSAJE(msg);	
		}
	});
	nuevo=0;
}

//----------------------------------------VENTANA DE DIALOGO DE AYUDA
function mostrarAyuda(){
	$("#ayuda").dialog('open');
}
//----------------------------------------VENTANA DE DIALOGO DE MANUAL
function notas(){
	$("#dialog-form2").dialog('open');
}
function limpiarCampos(){
	$("table.tablero input:text").val('');
	$("table.tablero select").val(0);
	$("#tipoDoc").focus();
	$("#idPersona").val('');
}
//---------------------------------------BUSCAR AFILIADO-PERSONA
function buscarPersona(td,numero){
	if(numero != ''){
		if(nuevo == 0){
			MENSAJE("Si es un registro nuevo haga click en NUEVO.");
			return false;
		}
		
		$.ajax({
			url:URL+'phpComunes/buscarPersona2.php',
			type:"POST",
			dataType:"json",
			data:{v1:numero,v0:td},
			success:function(datos){
				if(datos==0){
					MENSAJE("No existe el afiliado.");
					return false;
				}
				$.each(datos,function(i,fila){
					nom = $.trim(fila.pnombre)+" "+$.trim(fila.snombre)+" "+$.trim(fila.papellido)+" "+$.trim(fila.sapellido);
					idPersona=fila.idpersona;
					idTipoDoc = fila.idtipodocumento;
					identificacion = fila.identificacion;
				});//each
				$("#tipoDoc").val(idTipoDoc);
				$("#identificacion").val(identificacion);
				$("#nombreCompleto").val(nom);
				$("#idPersona").val(idPersona);
		  },
		  complete: buscarBeneficiarios
		});//ajax
	}
	
}
//---------------------------------------BUSCAR BENEFICIARIOS
function buscarBeneficiarios(){
	var numero=	$("#idPersona").val();
	$.getJSON(URL+'phpComunes/buscarGrupoSinC.php',{v0:numero},function(data){
		$("#beneficiarios option:not(':first')").remove();
		if(data != 0){
			$.each(data,function(i, fila){
				var nom = $.trim(fila.identificacion) +" "+ $.trim(fila.pnombre) +" "+ $.trim(fila.snombre) +" "+ $.trim(fila.papellido) +" "+ $.trim(fila.sapellido);
				//Hallo el parentesco Ago 2011
				var parentesco = 'Hijo/Hijastro';
				switch(fila.idparentesco){
					case '36' : parentesco = 'Padre'  ; break;
					case '37' : parentesco = 'Hermano'; break;
					default   : parentesco = 'Hijo/Hijastro'; break;
				}//switch
				$('#beneficiarios').append("<option id='"+fila.idparentesco+"' value="+fila.idbeneficiario+">"+nom+" ("+ parentesco +")</option>");
			});	
		}else{
			MENSAJE("El afiliado no tiene beneficiarios sin certificado.");
		}
	});
}

function buscarDiscapacidad(){
	var idb=$('#beneficiarios').val();
	if(idb != 0){
		$.getJSON('buscarDiscapacidad.php',{v0:idb},function(datos){
			if(datos>0){
				MENSAJE("El beneficiario YA esta marcado como discapacitado!");
				$('#beneficiarios').val('');
			}
		});
	}	
}
	
function imprimir(){
	location.href = URL +"centroReportes/aportes/trabajadores/reporte002.php";
}

function buscarRadicacion(idRadicacion){
	$.ajax({
		url: URL+'phpComunes/buscarRadicacionIndividual.php',
		type: "POST",
		data: {idRadicacion: idRadicacion},
		async: false,
		dataType: "json",
		success:function(data){
			if(data == 0){
				alert("No se encuentra la radidaci\xf3n!");
				return false;
			}else{
				$.each(data,function(i,fila){
					// cargar los datos del afiliado
					if(fila.procesado == 'N'){
						$("#fecha").val(fila.fechasistema.replace(/\-/g,"/"));
						// cargar datos del afiliado
						buscarPersona(fila.idtipodocumento2, fila.identificacion2);
					}else
						alert("La radicaci\xf3n ya fue procesada.");
	           });
			}
			
		}
    });
}

function cerrarRadicacion(idradicacion){
	$.ajax({
		url: URL+'phpComunes/cerrarRadicacion.php',
		type: "POST",
		data: "submit=&v0="+idradicacion,
		success: function(datos){
			alert(datos);
		}
	});
}