$(document).ready(function(){
	
	$("#alternate2").datepicker({
		changeMonth: true,
		changeYear: true,
		maxDate: "+0D",
		//dateFormat: "mmddyy",
	});
	var anoActual = new Date();
	var strYearRange = anoActual.getFullYear()-120 +":"+ anoActual.getFullYear();
	$("#alternate2").datepicker( "option", "yearRange", strYearRange);
	
	$("#ayuda").dialog({
	 	autoOpen: false,
		height: 450,
		width: 600,
		draggable:true,
		modal:false,
		open: function(evt, ui){
				$('#ayuda').html('');
				$.get('../../help/aportes/nuevosBeneficiarios.html',function(data){
						$('#ayuda').html(data);
				});
		}
	});
	
	$("#dialog-form2").dialog("destroy");
	$("#dialog-form2").dialog({
		autoOpen: false,
		height: 400,
		width: 500,
		modal: true,
		buttons: {
			'Enviar': function() {
			var campo=$('#notas').val();
			var campo0=$.trim(campo);
			if (campo0==""){
				$(this).dialog('close');
					return false;
				}
			var campo1=$('#usuario').val();
			var campo2="radicacion.php";
			$.post(URL+'phpComunes/colaboracion.php',{v0:campo0,v1:campo1,v2:campo2},
			function(datos){
				if(datos=='1'){
					alert("Su comentario fue enviado correctamente!. Gracias por participar en el mejoramiento de SIGAS");
				}else{
					alert(datos);
				}
		});
		$(this).dialog('close');
		},
		Cancelar: function() {
			$(this).dialog('close');
			$("#dialog-form2").dialog("destroy");
		}
	}
	});
});

function buscarPersona(){
	var tipd=$("#tipoDoc2").val();
	var num=$("#identificacion2").val();
	$("#pNombre2,#sNombre2,#pApellido2,#sApellido2,#alternate2").val("");
	$("#sexo2,#tipoVivienda2,#cboDeptoC,#cboCiudadC,#cboZonaC,#cboBarrioC,#cboDeptoC2,#cboCiudadC2").val(0);
	$("#sexo2,#tipoVivienda2,#cboDeptoC,#cboCiudadC,#cboZonaC,#cboBarrioC,#cboDeptoC2,#cboCiudadC2").trigger("change");
	
	$.ajax({
		url:URL+'phpComunes/pdo.buscar.persona.php',
		type: 'GET',
		data:{v0:tipd,v1:num,v2:2},
		async:false,
		dataType: 'json',
		success: function(datos){
			if(datos!=0) {
				alert("Ya existe una persona con el mismo numero de documento: "+datos[0].pnombre+" "+datos[0].snombre+" "+datos[0].papellido+" "+datos[0].sapellido);
				$("#pNombre2,#sNombre2,#pApellido2,#sApellido2,#identificacion2").val("");
				return false;
			}
		}
	});
}

function verificarCampos(){// verificarCampos Nueva Persona
	var error=0;
	$(".tablero input.ui-state-error,.tablero select.ui-state-error").removeClass("ui-state-error");
	var tipoDoc2=$("#tipoDoc2").val();
	var cedula2=$("#identificacion2").val();
	var pNombre2=$("#pNombre2").val();
	var sNombre2=$("#sNombre2").val();
	var pApellido2=$("#pApellido2").val();
	var sApellido2=$("#sApellido2").val();
	var sexo2=$("#sexo2").val();
	var tipVivienda2=$("#tipoVivienda2").val();
	var txtCasa=$("#txtCasa").val();	
	var capacidadTrabajo=$("#txtCapacidad").val();
	var estadoCivil2=$("#estadoCivil2").val();
	var profesion2=$("#profesion2").val();
	var fechaNace=$("#alternate2").val();
	var cboDeptoC=$("#cboDeptoC").val();
	var cboCiudadC =$("#cboCiudadC").val();
	var zonaRes2=$("#cboZonaC").val();
	var barrio2=$("#cboBarrioC").val();
	var direccion2=$("#direccion2").val();
	var telefono2=$("#telefono2").val();
	var celular2=$("#celular2").val();
	var email2=$("#email2").val();
	var cboDeptoC2=$("#cboDeptoC2").val();//no obligado
	var cboCiudadC2=$("#cboCiudadC2").val();//no obligado

	//---------------------TIPO DOC----------------------  
	if(tipoDoc2=='0'){
		$("#tipoDoc2").addClass("ui-state-error");
		error++;
		}
	//--------------------CEDULA-----------------------   
	if(cedula2==''){
		$("#identificacion2").addClass("ui-state-error");
		error++;
		}
	//----------------PRIMER NOMBRE Y APELLIDO------------
	if(pNombre2==''){
		$("#pNombre2").addClass("ui-state-error");
		error++;
		}
	if(pApellido2==''){
		$("#pApellido2").addClass("ui-state-error");
		error++;
		}
	//-------------------SEXO-----------------------
	if(sexo2=='0'){
		$("#sexo2").addClass("ui-state-error");
		error++;
		}
	//--------------------FECHA DE NACIMIENTO-----------
	if(fechaNace==''){
		$("#alternate2").addClass("ui-state-error");
		error++;
	}

	//----------------UBICACION VIVIENDA--------------------//
	if(tipVivienda2=='0')
		{
		$("#tipoVivienda2").addClass("ui-state-error");
		error++;
		}
	//----------------TIPO DE VIVIENDA--------------------//
	if(txtCasa=='0')
		{
		$("#txtCasa").addClass("ui-state-error");
		error++;
		}
	//----------------CAPACIDAD DE TRABAJO--------------------//
	if(capacidadTrabajo=='0')
		{
		$("#txtCapacidad").addClass("ui-state-error");
		error++;
		}
	//----------------PROFESION--------------------//
	if(profesion2=='0')
		{
		$("#profesion2").addClass("ui-state-error");
		error++;
		}
	//----------------DEPTO RESIDENCIA--------------------//
	if(cboDeptoC=='0')
		{
		$("#cboDeptoC").addClass("ui-state-error");
		error++;
		}
	//---------------------CIUDAD RESIDENCIA------------------//
	if(cboCiudadC=='0'){
		$("#cboCiudadC").addClass("ui-state-error");
		error++;
	}
	//---------------------ZONA------------------//
	if(zonaRes2=='0'){
		$("#cboZonaC").addClass("ui-state-error");
		error++;
	}
	//---------------------BARRIO------------------//
	if(barrio2=='0'){
		$("#cboBarrioC").addClass("ui-state-error");
		error++;
	}
	//---------------------DIRRECCION------------------//
	if(direccion2==''){
		$("#direccion2").addClass("ui-state-error");
		error++;
	}
	//---------------------DEPTO NACE---------------//
	if(cboDeptoC2=='0'){
		$("#cboDeptoC2").addClass("ui-state-error");
		error++;
	}
	//---------------------DEPTO NACE---------------//
	if(cboCiudadC2=='0'){
		$("#cboCiudadC2").addClass("ui-state-error");
		error++;
	}
	if(error==0){
		$.ajax({
			url: 'insertPersona.php',
			type: "POST",
			data: {v1:tipoDoc2,v2:cedula2,v3:pApellido2,v4:sApellido2,v5:pNombre2,v6:sNombre2,v7:sexo2,v8:direccion2,v9:barrio2,v10:telefono2,v11:celular2,v12:email2,v13:tipVivienda2,v14:cboDeptoC,v15:cboCiudadC,v16:zonaRes2,v17:estadoCivil2,v18:fechaNace,v19:cboCiudadC2,v20:cboDeptoC2,v21:capacidadTrabajo,v22:profesion2,v23:'',v25:txtCasa},
			async: false,
			//dataType: "json",
			success: function(data){
 			if(data==0){
				alert("No se pudo guardar la persona!");
				return false;
				}
			if(data<0){
			alert("Error al insertar la persona, codigo: "+data);
			return false;
			}
			alert("La persona fue grabada!");
			limpiarCampos();
			}
		});		
	}
}//end function verificarCampos Nueva Persona

function limpiarCampos(){
	$("table.tablero input:text").val('');
	$("table.tablero select").val(0);
	$(".tablero input.ui-state-error,.tablero select.ui-state-error").removeClass("ui-state-error");
}