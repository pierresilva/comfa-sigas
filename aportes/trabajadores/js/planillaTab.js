/* 
* CONTROL DE VALIDACIONES DE LIQUIDACIONES
* Autor: Andrés Felipe Lara Nieto
* Fecha: 04-Mar-2011
*/


URL=src();
function buscarPlanillas(){	
  //$("#tPlanilla").tablesorter()//.tablesorterPager({container: $("#pager")});
  $("#tPlanilla tbody tr").remove();
 //var ide=$("#txtIde").val();
  $.ajax({
	      url:URL+'phpComunes/buscarPlanillas.php',
		  type:"POST",
		  dataType:"json",
		  data: {v0:idPersona},
		  async:false,
		  success:function(data){
	 			  if(data==0){MENSAJE("NO existen planillas.");return false;}
					  $.each(data, function(i,n){ 
						  var row=(data==false ? "-" : data.ultimo );
						  var no=data.length;
						  $("#tPlanilla caption span").html(no+" registros encontrados."); 
					     $("#tPlanilla tbody").append("<tr><td>"+n.nit+"</td><td>"+n.planilla+"</td><td style=text-align:center>"+n.periodo+"</td><td style=text-align:right>"+formatCurrency(n.salariobasico)+"</td><td style=text-align:right>"+formatCurrency(n.ingresobase)+"</td><td style=text-align:right>"+n.diascotizados+"</td><td style=text-align:center>"+n.fechapago+"</td><td>"+n.ingreso+"</td><td>"+n.retiro+"</td><td>"+n.var_tra_salario+"</td><td>"+n.vacaciones+"</td><td>"+n.lic_maternidad+"</td><td>"+n.tipo_cotizante+"</td><td>"+n.procesado+"</td><td>"+n.correccion+"</td></tr>"); 
         	          });//end each emrpesa Id
			},//succes
			complete:function(){
				  //Actualizar Tabla de sorter 
				  $("#tPlanilla tbody tr:odd").addClass("vzebra-odd");
				  $("#tPlanilla").tablesorter({headers: {7: { sorter: false}}});
				  $("#tPlanilla").tablesorterPager({container: $("#pager"),seperator:" de "}).trigger("updateCell");
				  $("#tPlanilla").trigger("update");
			 }						
	     });//ajax 
}		   