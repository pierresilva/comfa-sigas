var URL=src();
var existe=0;
var idnb=0;
var idbeneficiario=0;//para actualziar estado y giro
var chequeado1=1;
var chequeado2=1;
var idPersona;
var fnace=0;
var msg="";
var idtipcert;
var idcert;
var idrelacion=0;

//Evento para actualizar datos de beneficiarios y conyuges ACTUALIZA BASICO
$("body").delegate("label[name='labelActualizaBasico']","click",function(){
	var idpersonaLabel=parseInt($(this).text());
    actualizaBasico(idpersonaLabel);
});

//Boton modificar beneficiario
$("[name='actBen']").live("click",function(){
//Id global del beneficiario seleccionado
 idbeneficiario=$(this).parent().parent().children(":eq(0)").text();	
 //Enviar parametros
 var ideb=$(this).parent().parent().children(":eq(0)").text();
 var p   =$.trim($(this).parent().parent().children(":eq(4)").text());
 buscarBeneficiarioUpdate(ideb,p);
});
	
function validarComboParentesco(){
	//setea la visiblidad de la fila #padreBiologico
	$("#padreBiologico").hide();
	$("#nombreBiologico").hide();
	$("#tipoIdPadreBiol").val('0');
	$("#IdPadreBiol").val("");
	$("#nomBiologico").html(" ");
	
	if( ($("#parentesco").val()=="36") || ($("#parentesco").val()=="37") ){
		$("#cedMama").attr("disabled",true);
		$("#cedMama").val(0);
	}else{
			//cod 38=hijastro
		if($("#parentesco").val()=='38'){
			$("#padreBiologico").show(); 
			$("#nombreBiologico").show();
			$("#tipoIdPadreBiol").val(0);
			$("#IdPadreBiol").val("");
			}
	$("#cedMama").attr("disabled",false);
	}
	//validarCertificados();
}	

function validarCertificados(){
if($("#fecNacHide3").val()=="") return false
idPersona=parseInt($("#idPersona").val());
var continuar=1;
if(isNaN(idPersona)){
	alert("No hay ID de la persona!");
	limpiarBene();
	fnace=0;
	return false;
}
$.ajax({
	url:URL+"phpComunes/pdo.b.fnaceafi.php",
	type:'POST',
	dataType:'json',
	data :{v0:idPersona},
	async:false,
	success:function(data){
		if(data==0){
			alert("No hay fecha de nacimiento del afiliado, por favor actualice la informacion!");
			fnace=0;
			limpiarBene();
			continuar=0;
			}
		fnace=data;
	}
	})
if(continuar==0) return false
	var fx=fnace.split('-');
	var ax=fx[0];
	var mx=fx[1];
	var dx=fx[2];
	fnace=mx+"/"+dx+"/"+ax;
	var dd=diferenciaFechas(fnace,$("#fecNacHide3").val());
	if((dd >= 0)  ){
		alert("El beneficiario es Mayor que el afiliado?, por favor revise las fechas de nacimiento!");
		$("#fecNacHide3").val('')
		return false;
		}
	$("#filaDiscapacidad").hide();
	$("#filaEscolaridad").hide();
	$("#filaUniversidad").hide();
	$("#filaSupervivencia").hide();
	$("#fEscolaridad,#fUniversidad,#fSupervivencia,#fDiscapacidad").val('');
	$("#cEscolaridad,#cUniversidad,#cSupervivencia,#cDiscapacidad").attr("checked",false);
	var p=$("#parentesco").val();
	var f=new Date();
	var fechaOculta=0;
	fechaOculta=$("#fecNacHide3").val();
	var ano=f.getFullYear();
	var anoFechaOculta=fechaOculta.slice(-4);
	var edadB=parseInt(ano)-parseInt(anoFechaOculta);
	//--------PREGUNTAR PRIMERO SI ES SUBSIDIO (48)
	if($("#tipoAfiliacion3").val()=='48'){
		 if($("#capTrabajo").val()=='I'){
		 	$("#filaDiscapacidad").show();
			return false;
		 }
		 if($("#parentesco").val()=="36"){
			 if(edadB>=60){
				 $("#filaSupervivencia").show();
				 return false;
				 }
			 }
		var pa=$("#parentesco").val();
		if( (pa=="35") || (pa=="37") || (pa=="38") ) {
			if(edadB>19){
				$("#tipoAfiliacion3").val(49);
				return false
				}
			else{
				if(edadB>11){
					$("#filaEscolaridad").show();
					$("#filaUniversidad").show();
				}
				}
			}
	}
}

function buscarRelaciones(){
idPersona=$("#idPersona").val();
$.ajax({
	url:URL+"phpComunes/pdo.buscar.relaciones.php",
	type:'POST',
	dataType:'json',
	async:false,
	data :{v0:idPersona},
	success:function(data){
	//Limpiar option de conyuge, y tablas
	$("#tabTableRelaciones tbody tr").remove();
	$("#beneficiarios #cedMama option:not(':first',:eq(1))").remove();
	if(data==0){
		msg="El trababajador NO tiene relaciones de convivencias.\n\r";
		return false;
		}
		$.each(data,function(i,fila){
			idr=fila.IDRELACION;
			idc=fila.IDCONYUGE; 
			numC=fila.IDENTIFICACION;	
			nombreC=$.trim(fila.pnombre)+" "+$.trim(fila.snombre)+" "+$.trim(fila.papellido)+" "+$.trim(fila.sapellido);
			conviven=fila.CONVIVEN;
			//Insertamos las convivencias en la tablas
			if(conviven=='S')
				par=idr+",0";
			else
				par=idr+",1";
			$("#tabTableRelaciones tbody").append("<tr><td><b><label name='labelActualizaBasico' style='cursor:pointer;text-decoration:none;'>"+idc+"</label></b></td><td>"+numC+"</td><td style=text-align:center><label style=cursor:pointer onclick=cambiarC("+par+");>"+conviven+"</label></td><td>"+nombreC+"</td></tr>");
			//Agregar a la tabla beneficiarios oculta los datos del conyuge
			$("#beneficiarios table.tablero td #cedMama").append("<option value='"+numC+"' name='"+idc+"'>"+numC+"</option>");
		});//each
	}
	});
}

function buscarGrupo(){
//BUSCAMOS LOS BENEFICIARIO DE ACUERDO A LAS RELACIONES DE CONVIVENCIA
	$.ajax({
		url:URL+"phpComunes/pdo.buscar.grupo.php",
		type:'POST',
		dataType:'json',
		async:false,
		data :{v0:idPersona},
		//timeout:5000,
		success:function(data){
			$("#tabTableGrupo tbody tr").remove();
			if(data==0){
				msg+="El trababajador NO tiene beneficiarios.";
				MENSAJE(msg);
				return false;
			}
		$.each(data,function(i,fila){
			idb=fila.IDBENEFICIARIO;
			idc=fila.IDCONYUGE;
			tdo=fila.CODIGO;
			numero=fila.IDENTIFICACION;
			nombreB=$.trim(fila.pnombre)+" "+$.trim(fila.snombre)+" "+$.trim(fila.papellido)+" "+$.trim(fila.sapellido);
			parentesco=fila.DETALLEDEFINICION;
			fecnac=fila.FECHANACIMIENTO;
			edad=fila.EDAD;
			embargo=fila.EMBARGA;
			discapacidad=(fila.CAPACIDADTRABAJO=='I')?'S':'N';
			giro=fila.GIRO;
			fasig=fila.FECHAASIGNACION;
			estado=fila.ESTADO;
			fecafi=fila.FECHAAFILIACION;
			ced_cony=fila.CED_CON;
			$("#tabTableGrupo tbody").append("<tr><td><b><label name='labelActualizaBasico' style='cursor:pointer;text-decoration:underline;'>"+idb+"</label></b></td><td>"+tdo+"</td><td>"+numero+"</td><td>"+nombreB+"</td><td>"+parentesco+"</td><td style=text-align:center><label style='cursor:pointer' onclick='buscarNombre("+idc+");'>"+ced_cony+"</label></td><td style=text-align:center>"+fecnac+"</td><td style=text-align:center>"+edad+"</td><td style=text-align:center>"+embargo+"</td><td style=text-align:center>"+discapacidad+"</td><td id='g_"+idb+"'>"+giro+"</td><td>"+fasig+"</td><td id='e_"+idb+"'>"+estado+"</td><td style=text-align:center>"+fecafi+"</td><td style='text-align:center'><img style='cursor:pointer' src='"+URL+"imagenes/menu/modificar.png' id='actBen"+idb+"' name='actBen' title='Modificar Beneficiario' /></td></tr>"); 
		});//each
		//macheteado para eliminar duplicados ojo! eliminar despues....
		con=data.length;
		//con=parseInt(con)-1;
		$("#tabTableGrupo tbody tr:lt("+con+")").remove();	
	}//succes
	});//ajax beneficiarios		
}

function buscarNombre(idc){
	$.getJSON(URL+'phpComunes/pdo.solo.nombres.php',{v0:idc},function(datos){
		nom=datos[0].pnombre;
		
	})
	}
	
function buscarPersona(num){
	limpiarConyuge();
	num=$.trim(num);
	if(num.length==0){return false;}
	if($("#conviven").val()==0){
		alert("Seleccione tipo de convivencia!")
		return false;
	}
	var conviven=$("#conviven").val();
	if($("#conyuge").is(":visible")){
	if($("#tipoDoc2").val()==0){
	    alert("Falta tipo documento");
	    return false;
	}
	}
	var idtd=$("#tipoDoc2").val();
	$.ajax({
		url: URL+'phpComunes/buscarPersona2.php',
		type: "POST",
		data: "submit &v0="+idtd+"&v1="+num,
		async: false,
		dataType: "json",
		success: function(data){
			if(data==0){
				existe=0;
				idnb=0;
				return false;
			}
			else{
			existe=1;
			$.each(data,function(i,fila){
				idnb=fila.idpersona;
				
				$('#pNombre2').val($.trim(fila.pnombre));
				$('#sNombre2').val($.trim(fila.snombre));
				$('#pApellido2').val($.trim(fila.papellido));
				$('#sApellido2').val($.trim(fila.sapellido));
				$("#direccion2").val($.trim(fila.direccion));
				$("#cboBarrioC").val(fila.idbarrio);
				$("#telefono2").val(fila.telefono);
				$("#celular2").val(fila.celular);
				$("#email2").val($.trim(fila.email));
				$("#tipoVivienda2").val(fila.idtipovivienda);
				$("#cboDeptoC").val(fila.iddepresidencia).trigger('change');
				setTimeout((function(){
					$("#cboCiudadC").val(fila.idciuresidencia);
					$("#cboCiudadC").trigger("change");
				}),1000);
				setTimeout((function(){
					$("#cboZonaC").val(fila.idzona);
					$("#cboZonaC").trigger("change");
				}),2000);
				setTimeout((function(){
					$("#cboBarrioC").val(fila.idbarrio);	
					$("#cboBarrioC").trigger("change");
				}),3000);
				$("#estadoCivil2").val(fila.idestadocivil);
				$("#cboDeptoC2").val(fila.iddepnace).trigger('change');
				
				setTimeout((function(){
					$("#cboCiudadC2").val(fila.idciunace);
					$("#cboCiudadC2").trigger("change");
				}),4000);
				
				//$("#ciudadNac").val(fila.idciunace);
				$("#capTrabajo2").val(fila.capacidadtrabajo);
				$("#profesion2").val(fila.idprofesion);
				$("#fecNacHide2").val(fila.fechanacimiento);
				
				$('#sexo2').val(fila.sexo);
			}); //each
//			TABLA DINAMICA PARA DATOS EMPRESA TRABAJDOR ACTIVO
			$("table[name='dinamicTable']").remove();
			var tabla="<table class='tablero' width='100%' border='0' cellspacing='0' cellpadding='0' name='dinamicTable' style='margin-first:8px;'>";
			$.ajax({
				url: URL+'phpComunes/buscarAfiliacion.php',
				type: "POST",
				data: "submit &v0="+idnb,
				async: false,
				dataType: "json",
				success: function(datos){
					if(datos != 0){
						alert("La persona que trata de Afiliar es un trabajador activo!");
					    $.each(datos,function(i,fila){
					    	$('#nitConyuge').val(fila.nit);
					    	$('#empCony').val(fila.razonsocial);
					    	$('#salCony').val(fila.salario);
					    	$('#subCony').val(fila.detalledefinicion);
					    	tabla+="<tr><th>Salario</th><th>Nit</th><th>Raz\u00F3n social</th><th>Tipo</th><th>Detalle</th></tr>"+"<tr><td>$"+ fila.salario +"</td><td>"+ fila.nit +"</td><td>"+fila.razonsocial+"</td><td>"+fila.tipoformulario+"</td><td>"+fila.detalledefinicion+"</td></tr>";
					    });//end each
					  //Agregar tabla despues de la tabla  beneficiarios
					    $("#conyuge .tablero").after(tabla+="</table>");
					    $("table[name='dinamicTable']").hide().fadeIn(800);
					}
					var tabla2="<table class='tablero' width='100%' border='0' cellspacing='0' cellpadding='0' name='dinamicTable' style='margin-first:8px;'>";
					var conv=0;
					$.ajax({
						url: URL+'phpComunes/buscarConvivenciaConyuge.php',
						type: "POST",
						data: "submit &v0="+idnb,
						dataType: "json",
						success: function(dato){
							if(dato!=0){
							 alert("La persona que esta afiliando tiene relacion de convivencia (ver tabla parte inferior)");
							 $.each(dato,function(i,fila){
								   var nombre=fila.pnombre+" "+fila.snombre+" "+fila.papellido+" "+fila.sapellido;
								   conv=fila.conviven;
								   if(conv=='S'){
									   alert("El conyuge tiene relacion de convivencia Activa!");
									   $("#conviven").val('N');
									   }
								   idtrelacion=fila.idtrabajador;
								   tabla2+="<tr><th>Identificacion</th><th>Nombre</th><th>Conviven</th></tr>"+"<tr><td>"+ fila.identificacion +"</td><td>"+ nombre +"</td><td>"+fila.conviven+"</td></tr>";
								   return;
							 });
							 if(idPersona==idtrelacion){
								 idtrelacion=1;
							 }
							 $("#div-relacion").append(tabla2+="</table>");
							 $("table[name='dinamicTable']").hide().fadeIn(800);
							 if(conv='S' && idtrelacion==0){
								 $('#conyuge .tablero input:text').val("");
								 $('#conyuge .tablero select').val("0");
							 }
						}
						}
					}); //FIN AJAX 3
				}
			}); //FIN AJAX 2
			}//fin else primer success
		}//fin primer success
	})
}	

function verificarC(){
	$("#div-conyuge ul").remove();
	lista="<ul class='Rojo'>";
	var tipRel=$("#tipRel").val();
	var txtRelacion=$("#tipRel").children("option:selected").html();
	var conviven=$("#conviven").val();
	var txtConvive=$("#conviven").children("option:selected").html();  //esto para q es?????
	var tipoDoc2=$("#tipoDoc2").val();
	var optionSel=$("#tipoDoc2").val();		//children("option:selected").html();//valor del texto del option
	var cedula2=$("#identificacion2").val();
	var pNombre2=$("#pNombre2").val();
	var pApellido2=$("#pApellido2").val();
	var sNombre2=$("#sNombre2").val();//no obligado
	var sApellido2=$("#sApellido2").val();//no obligado
	var sexo2=$("#sexo2").val();
	var barrio2=$("#cboBarrioC").val();
	var direccion2=$("#direccion2").val();
	var telefono2=$("#telefono2").val();
	var celular2=$("#celular2").val();
	var email2=/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test($("#email2").val());
	var tipVivienda2=$("#tipoVivienda2").val();
	var cboDeptoC=$("#cboDeptoC").val();
	var cboCiudadC =$("#cboCiudadC").val();
	var zonaRes2=$("#cboZonaC").val();
	var estadoCivil2=$("#estadoCivil2").val();
	var cboDeptoC2=$("#cboDeptoC2").val();//no obligado
	var cboCiudadC2=$("#cboCiudadC2").val();//no obligado
	var capTrabajo2=$("#capTrabajo2").val();//no obligado
	var profesion2=$("#profesion2").val();//no obligado
	var fecNac2=$("#fecNacHide2").val();
	if(tipRel=='0'){lista+='<li>Seleccione un tipo de relacion.</li>';}
	//------------------------CONVIVEN----------------------
	if(conviven=='0'){lista+='<li>Seleccione el tipo de convivencia.</li>';}
	//---------------------TIPO DOC----------------------  
	if(tipoDoc2=='0'){lista+='<li>Seleccione un tipo de documento en CONYUGE.</li>';}
	//--------------------CEDULA-----------------------   
	/*if(conviven=='S'){
	if(tipRel==2147 && estadoCivil2!=51){lista+='<li>No concuerda el tipo de relacion con el estado civil!.</li>';}
	if(tipRel==2148 && estadoCivil2!=52){lista+='<li>No concuerda el tipo de relacion con el estado civil!.</li>';}
	}*/
	if(cedula2==''){
		lista+='<li>Ingrese la identificaci\u00F3n del CONYUGE.</li>';
		} else {
	    if(cedula2.length<3){
	    	lista+='<li>La identificaci\u00F3n NO debe tener 2 d\u00EDgitos.</li>';
	    }else {
		if(cedula2.length==9){
			lista+='<li>La identificaci\u00F3n NO debe tener 9 d\u00EDgitos.</li>';
			}
		}//end else
		}//end else
	//----------------PRIMER NOMBRE Y APELLIDO---------------------------  
	if(pNombre2==''){lista+='<li>Ingrese el NOMBRE del CONYUGE.</li>';}
	if(pApellido2==''){lista+='<li>Ingrese el APELLIDO del CONYUGE.</li>';}
	//-------------------SEXO-----------------------
	if(sexo2=='0'){lista+='<li>Seleccione el sexo del CONYUGE.</li>';}
	//-------------------TELEFONO----------------------
	if(telefono2!=''){
		if(telefono2.length<7){
			lista+='<li>Faltan d\u00EDgitos en el  TELEFONO.</li>';
		}
	}
	//---------------------EMAIL------------------------
	if($("#email2").val()!=''){
		if(!email2) {
			lista+='<li>El E-mail aparentemente es incorrecto.</li>';	
		}else{
			email2=$("#email2").val();
		}
	}
	//------------------------------------------------------
	/*if(deptRes2=='Seleccione..'){lista+='<li>Seleccione el Departamento de residencia en CONYUGE.</li>';}
	if(ciudadRes2=='Seleccione..'){lista+='<li>Seleccione la Ciudad de residencia en CONYUGE.</li>';}
	if(zonaRes2=='Seleccione..'){lista+='<li>Seleccione la zona de residencia en CONYUGE.</li>';}*/
	//----------------------FECHA DE NACIMIENTO---------------
	if(fecNac2==''){lista+='<li>Escriba la fecha de nacimiento del CONYUGE.</li>';	}
	//---------------DOCUMENTOS A SOLICITAR
	/*if($("#cFotocopia").attr("checked")==false){
	lista+='<li>Seleccione la fotocopia de la c\u00E9dula .</li>';
	}else{
	$("#cFotocopia").attr("value",'S');
	}
	
	if(estadoCivil2=='0'){
		lista+='<li>Ingrese el estado civil del conyuge.</li>';
	}*/
	//..MOSTRAR VALIDACIONES..//
	$(lista+"</ul>").prependTo($("#div-conyuge"));
	$("#div-conyuge ul.Rojo").hide().fadeIn("slow");
	if($("#div-conyuge ul.Rojo").is(":empty")){				 
		//limpiarCampos();
		anioActual=new Date();
		hoy=anioActual.getFullYear();
		var edadC=parseInt(hoy)- parseInt(anio);	
		var filas=$("#tabTableRelaciones tbody tr").length;
		var con=0;
		//Verificar conyuge existente en tablaC de nuevos conyuges
		while(con < filas){
		var ccony=$("#tabTableRelaciones tbody tr:eq("+con+") td:eq(1)").text();
		//cargar los datos en la tabla conyuge
		if(cedula2==ccony){
		alert("El Conyuge con c\u00E9dula '"+cedula2+"' ya esta agregado");
		return false;
		}
		con++;		
		}
	
		//Verificar en cada fila si existe un conyuge con convivencia
		estadoC=0;
		$("#tabTableRelaciones tbody tr").each(function(i){
		    con=$(this).children("td:eq(2)").text()//.slice(0,1);
			
			if(con==conviven&&con=='S'){//conviven=con
			cony=$(this).children("td:eq(3)").text();
		    alert("Ya existe una RELACION con estado de convivencia activa: "+cony);
		    estadoC=1;
		    }
		});
		
		if(estadoC==0){
        
		var idcony=0;
		if(existe==0){
			$.ajax({
				url: 'insertPersona.php',
				type: "POST",
				data: {v1:tipoDoc2,v2:cedula2,v3:pApellido2,v4:sApellido2,v5:pNombre2,v6:sNombre2,v7:sexo2,v8:direccion2,v9:barrio2,v10:telefono2,v11:celular2,v12:email2,v13:tipVivienda2,v14:cboDeptoC,v15:cboCiudadC,v16:zonaRes2,v17:estadoCivil2,v18:fecNac2,v19:cboCiudadC2,v20:cboDeptoC2,v21:capTrabajo2,v22:profesion2,v23:''},
				async: false,
				//dataType: "json",
				success: function(data){
	 			if(data==0){
					alert("No se pudo guardar la persona!");
					return false
					}
				if(data<0){
				alert("Error al insertar la persona, codigo: "+data);
				return false;
				}	
				
			   alert("La persona fue grabada!");
				idcony=data;	
				}
			});
			}
		else{
			//actualizar persona
			idcony=idnb;
			idPersona=$("#idPersona").val();
			
						
	var datos={v0:idPersona,v1:tipoDoc2,v2:cedula2,v3:pApellido2, v4:sApellido2, v5:pNombre2, v6:sNombre2, v7:sexo2, v8:direccion2, v9:barrio2, v10:telefono2, v11:celular2, v12:email2, v13:'', v14:tipVivienda2, v15:cboDeptoC, v16:cboCiudadC, v17:zonaRes2, v18:estadoCivil2, v19:fecNac2, v20:cboDeptoC2, v21:cboCiudadC2, v22:capTrabajo2, v23:'', v24:profesion2};
		$.ajax({
			url: URL+'phpComunes/actualizarPersona.php',
			type: "POST",
			data: datos,
			async: false,
			//dataType: "json",
			success: function(data){
	 		if(data==0){
			alert("No se pudo actualizar la persona!");
			return false
			}
			alert("se actualizo la persona.");
			
			}
		});//ajax*/
	
	}
		//grabar relacion
		if(idcony>0){
			var campo0=$("#idPersona").val();
			var campo1=idcony;
			var campo2=34;
			var campo3=idcony;
			var campo4=conviven;
			var campo5=tipRel;
			$.getJSON(URL+'phpComunes/guardarConyuge.php',{v0:campo0,v1:campo1,v2:campo2,v3:campo3,v4:campo4,v5:campo5},function(data){
    		if(data==0){
    			alert("No se pudo guardar la relaci\u00F3n!, intente de nuevo!");
        		return false;
     		}
     		});
     		$.getJSON(URL+'phpComunes/guardarConyuge.php',{v0:campo1,v1:campo0,v2:campo2,v3:campo0,v4:campo4,v5:campo5},function(data){
        	if(data==0){
				alert("No se pudo crear la relaci\u00F3n, informe al administrdor del sistema!")
				return false;
				}
			alert("La relaci\u00F3n de convivencia fue creada!");
			$("#div-conyuge").dialog("close");
			buscarRelaciones();	
			});
			}
		}//estado		
	}		
}

function cambiarC(idr,c){
	var cont=true;
	var idc1=0;
	var idc2=0;
	if(c==0){
		if(confirm("Esta seguro de disolver la CONVIVENCIA?")==true){
			$.ajax({
				url:'buscarNomCon.php',
				type:'POST',
				dataType:'json',
				data:{v0:idr},
				success:function(data){
					if(data==0){
						alert("No existen datos del conyuge!");
						cont=false;
						return false;
					}
					var nom='';
					$.each(data,function(i,fila){
						if(i==0){
							nom+=fila.pnombre+" "+fila.snombre+" "+fila.papellido+" "+fila.sapellido+"\r\n";
							idc1=fila.IDTRABAJADOR;
						}else{
							nom+=fila.pnombre+" "+fila.snombre+" "+fila.papellido+" "+fila.sapellido;
							idc2=fila.IDTRABAJADOR;
						}
					})
					if(confirm("Se va a disolver la relacion de convivencia entre:\r\n"+nom)){
						$.ajax({
							url:'disolverC.php',
							type:'POST',
							dataType:'json',
							data:{v0:idc1,v1:idc2},
							success:function(data){
								if(data==0){
									alert("No se pudo disolver la relacion!");
									cont=false;
									return false;
								}else{
									alert("La relacion se disolvio!\r\nRefresque la pagina!")
									observacionesTab(idc1,1);
									//observacionesTab(idc2,1);
									return false;
								}
							}
						})
					}
				}
			})
			}
	}
	else{
		if(confirm("Esta seguro de crear la CONVIVENCIA?")==true){
			$.ajax({
				url:'buscarNomCon.php',
				type:'POST',
				dataType:'json',
				data:{v0:idr},
				success:function(data){
					if(data==0){
						alert("No existen datos del conyuge!");
						cont=false;
						return false;
					}
					var nom='';
					$.each(data,function(i,fila){
						if(i==0){
							nom+=fila.pnombre+" "+fila.snombre+" "+fila.papellido+" "+fila.sapellido+"\r\n";
							idc1=fila.IDTRABAJADOR;
						}else{
							nom+=fila.pnombre+" "+fila.snombre+" "+fila.papellido+" "+fila.sapellido;
							idc2=fila.IDTRABAJADOR;
						}
					})
					if(confirm("Se va a crear la relacion de convivencia entre:\r\n"+nom)){
						$.ajax({
							url:'crearC.php',
							type:'POST',
							dataType:'json',
							data:{v0:idc1,v1:idc2},
							success:function(data){
								if(data==0){
									alert("No se pudo crear la relacion!");
									cont=false;
									return false;
								}
								if(data==2){
									alert("Una o las dos personas tienen relacion de convivencia Activa!");
									return false;
									}
								else{
									alert("La relacion se creo!\r\nRefresque la pagina!")
									observacionesTab(idc1,1);
									//observacionesTab(idc2,1);
									return false;
								}
							}
						})
					}
				}
			})
			}
	}
	}
	
function validarFecha(obj){
	
 var fecha=obj.value;
 
 //Expresion regular para validar
/* if (/^\d{2}\/\d{2}\/\d{4}$/.test(fecha)){
	 fechaTemp=obj.value;
	 obj.value='';
	 
 }
 if(fecha==''){
 obj.value=fechaTemp;
 }else{
 */
 if(( fecha.length<8) || (fecha.length>8)){
 	alert("La fecha esta mal escrita");
 	$("#txtfechanace").focus().val('');
 	return false;
 }	
 mes=fecha.slice(0,2);
 dia=fecha.slice(2,4);
 ano=fecha.slice(4,8);
 
 
 switch(mes){
        case "01": case "03":  case "05": case "07": case "08": case "10": case "12":numDias=31;break;
        case "04": case "06": case "09": case "11": numDias=30; break;
        case "02":
		       if ( ( anio % 100 != 0) && ((anio % 4 == 0) || (anio % 400 == 0))) {
			   numDias=29;
			   }else{
				   numDias=28;
				   }
                break;
        default:
	            alert("Mes introducido inv\u00C1lido.");
		        return false;
    }
       //Validar Dias
        if (parseInt(dia)>numDias || parseInt(dia)==0){
            alert("Fecha introducida inv\u00C1lida.");
            return false;
        }
		
		//Validar años
		var f=new Date();
		 if (parseInt(ano)>f.getFullYear()){
            alert("A\u00F1o inv\u00C1lido mayor al actual.");
            return false;
        }
		
		var fechaFinal=mes+"/"+dia+"/"+ano;
		obj.value=fechaFinal;
// }//else

	}
	
function limpiarConyuge(){
	    $('#pNombre2').val('');
		$('#tNombre2').val('');
		$('#sNombre').val('');
		$('#pApellido2').val('');
		$('#sApellido2').val('');
		$('#sexo2').val('0');
		$('#tipoVivienda2').val('0');
		$('#cboDeptoC').val('0');
		$('#cboCiudadC').val('0');
		$('#cboZonaC').val('0');
		$('#direccion2').val('');
		$('#cboBarrioC').val('0');
		$('#email2').val('');	
		$('#telefono2').val('');
		$('#celular2').val('');	
		$('#fecNacHide2').val('');
		$('#alternate2').val('');			
		$('#cboDeptoC2').val('0');
		$('#cboCiudadC2').val('0');		
		$('#nitConyuge').val('');			
		$('#empCony').val('');
		$('#salCony').val('');	
		$('#subCony').val('');			
	}	

function verificarB(){
	$("#beneficiarios ul").remove();
	$("table[name='dinamicTable']").remove();
	lista="<ul class='Rojo'>";
			
	//...BENEFICIARIOS
	var parentesco=$("#parentesco").val();	//children("option:selected").html();//valor del texto del option PARENTESCO
	var cedMama=$("#cedMama").val();//no obligado
	
	var tipoDoc3=$("#tipoDoc3").val();
	var cedula3=$("#identificacion3").val();
	var pNombre3=$("#pNombre3").val();
	var pApellido3=$("#pApellido3").val();
	var sNombre3=$("#sNombre3").val();//no obligado
	var sApellido3=$("#sApellido3").val();//no obligado
	var sexo3=$("#sexo3").val();
	var fecNac3=$("#fecNacHide3").val();
	
	var tipoAfiliacion3=$('#tipoAfiliacion3').val();
	var capacidad=$('#capTrabajo').val();
	var cboDeptoB=$('#cboDeptoB').val();
	var cboCiudadB=$("#cboCiudadB").val();
	var estadoCivil3=$("#estadoCivil3").val();
	var fEscolaridad=$("#fEscolaridad").val();
	var fUniversidad=$("#fUniversidad").val();
	var fDiscapacidad=$("#fDiscapacidad").val();
	var fSupervivencia=$("#fSupervivencia").val();	
	var fAsignacion=$("#fAsignacion").val();
	if($("#tipoAfiliacion3").val()=='48'){
		if(fAsignacion==''){
			lista+='<li>Ingrese la fecha de Asignacion.</li>';
		}
	}
	if(cedMama=='undefined'){
		cedMama=0;
	}
	//VALORES DE CERTIFICADO
	$("#cEscolaridad,#cUniversidad,#cSupervivencia").attr("value","N");
	
	//-------------------PARENTEZCO-----------------------
	if(parentesco==0||parentesco=='Seleccione..'){
		lista+='<li>Seleccione el tipo de beneficiario.</li>';
	}
	//---------------------TIPO DOC----------------------  
	if(tipoDoc3==0){lista+='<li>Seleccione un tipo de documento en BENEFICIARIO.</li>';}
	//--------------------CEDULA-----------------------   
	if(cedula3==''){
		lista+='<li>Ingrese la identificaci\u00F3n del BENEFICIARIO.</li>';
	} else {
		if(cedula3.length<3){
			lista+='<li>La identificaci\u00F3n NO debe tener 2 d\u00EDgitos.</li>';
		}else {
			if(cedula3.length==9){
				lista+='<li>La identificaci\u00F3n NO debe tener 9 d\u00EDgitos.</li>';
			}
		}//end else
	}//end else
	//----------------PRIMER NOMBRE Y APELLIDO---------------------------  
	if(pNombre3==''){lista+='<li>Ingrese el NOMBRE del BENEFICIARIO.</li>';}
	if(pApellido3==''){lista+='<li>Ingrese el APELLIDO del BENEFICIARIO.</li>';}
	//-------------------SEXO-----------------------
	if(sexo3=='0'){lista+='<li>Seleccione el sexo del BENEFICIARIO.</li>';}
	//----------------------FECHA DE NACIMIENTO 3---------------
	if(fecNac3==''||fecNac3=='mmddaaaa'){lista+='<li>Escriba la fecha de nacimiento del BENEFICIARIO.</li>';afiliacion=false;}
	
	//-------------------TIPO AFILIACION-----------------------
	if(tipoAfiliacion3=='0'){
		lista+='<li>Seleccione tipo de afiliacion.</li>';
		afiliacion=false;	
	}
	//-------------------CAPACIDAD--------------------
	if(capacidad=='0'){
		lista+='<li>Seleccione capacidad de trabajo.</li>';
		afiliacion=false;	
	}
	//-------------------DEPARTAMENTO-----------------------
	if(cboDeptoB=='0'){
		lista+='<li>Seleccione Departamento de nacimiento.</li>';
		afiliacion=false;	
	}
	if(cboCiudadB==''){
		lista+='<li>Seleccione la ciudad de nacimiento.</li>';
		afiliacion=false;	
	}
	//Validar certificados de beneficiario 	
	
	if($("#cUniversidad").attr("checked")==true){
		if($("#fUniversidad").val()==''){
			lista+='<li>Ingrese la fecha de entrega del certificado de UNIVERSIDAD.</li>';
		}
	}
	
	if($("#cEscolaridad").attr("checked")==true){
		if($("#fEscolaridad").val()==''){
			lista+='<li>Ingrese la fecha de entrega del certificado de ESCOLARIDAD.</li>';
		}
	}
	
	if($("#cSupervivencia").attr("checked")==true){
		if($("#fSupervivencia").val()==''){
			lista+='<li>Ingrese la fecha de entrega del certificado de SUPERVIVENCIA.</li>';
		}
	}
	if($("#cDiscapacidad").attr("checked")==true){
		if($("#fSupervivencia").val()==''){
			lista+='<li>Ingrese la fecha de entrega del certificado de DISCAPACIDAD.</li>';
		}							
	}
	//lista+='<li>...........................</li>';
	//-------------------estado civil---------------------
					
	//..MOSTRAR VALIDACIONES..//
	$(lista+"</ul>").prependTo($("#beneficiarios"));
	$("#beneficiarios ul.Rojo").hide().fadeIn("slow");
	//Si no hay errores guardar 
	if($("#beneficiarios ul.Rojo").is(":empty")){
		var idben=0;
		var idcony=0;
		if(existe==0){
			alert("El beneficiario es nuevo.");
			$.ajax({
				url: 'insertPersona.php',
				type: "POST",
				data: {v1:tipoDoc3,v2:cedula3,v3:pApellido3,v4:sApellido3,v5:pNombre3,v6:sNombre3,v7:sexo3,v17:estadoCivil3,v18:fecNac3,v19:cboCiudadB,v20:cboDeptoB,v21:capacidad},
				async: false,
				success: function(data){
	 			if(data==0){
					alert("No se pudo guardar la persona!");
					return false;
					}
				if(data<0){
				alert("Error al insertar la persona, codigo: "+data);
				return false;
				}	
			   alert("La persona fue grabada!");
				idben=data;	
				}
			});
			}
		else{
			//actualizar persona
			idben=parseInt(idnb);
								
	var datos={v0:idben,v1:tipoDoc3,v2:cedula3,v3:pApellido3,v4:sApellido3,v5:pNombre3,v6:sNombre3,v7:sexo3,v8:'',v9:0,v10:'',v11:'',v12:'',v13:0,v14:'',v15:'',v16:'',v17:'',v18:estadoCivil3,v19:fecNac3,v20:cboDeptoB,v21:cboCiudadB,v22:capacidad,v23:'',v24:'',v25:''};
	$.ajax({
			url: URL+'phpComunes/actualizarPersona.php',
			type: "POST",
			data: datos,
			async: false,
			success: function(data){
	 		if(data==0){
			alert("No se pudo actualizar la persona!");
			return false
			}
			alert("se actualizo la persona. idben="+idben);
			}
		});//ajax*/
	}//if existe
	//crear relacion
	var giro='';
	if($("#tipoAfiliacion3").val()=='48'){
		giro='S';	
	}else{
		giro='N';
	}
	var campo0 = idPersona;
	var campo1 = idben;
	var campo2 = parentesco;
	var campo3 = ($("#cedMama").is(":disabled")?idcony: $("#cedMama option:selected").attr("name"));
	var campo4 = giro;
	var campo5 = $("#fAsignacion").val();
	$.ajax({
	url: URL+'phpComunes/guardarBeneficiario.php',
	type: "POST",
	async: false,
	data: "submit &v0="+campo0+"&v1="+campo1+"&v2="+campo2+"&v3="+campo3+"&v4="+campo4+"&v5="+campo5,
	success: function(datos){
		if(datos==0){
			alert("NO se pudo guardar la relacion!");
			return false;
			}
		var cert=0;
		if($("#cDiscapacidad").is(":checked")){
			cert=2;
			var v1 = idben;			//idbeneficiario
			var v2 = parentesco;	//idparentesco
		}
		if($("#cEscolaridad,#cSupervivencia").is(":checked")){
			cert=1;
			f=$("#fEscolaridad").val();
			mes=f.slice(0,2);
			ano=f.slice(-4);
			periodo=ano+mes;
			var v0 = idben;			//idbeneficiario
			var v1 = parentesco;	//idparentesco
			var v2 = ($("#cEscolaridad").is(":checked")?'55' : '57');				//idtipocertificado
			var v3=periodo;		//periodo inicio
			var v4=ano+'12';	//periodo final
			var v5='P';			//forma presentacion
		}
		if($("#cUniversidad").is(":checked")){
			cert=1;
			f=$("#fUniversidad").val();
			mes=f.slice(0,2);
			ano=parseInt(f.slice(-4));
			m=parseInt(mes);
			if(m<=6) {
				mf='06'
				}
			else{
				mf='12';
				}
			periodo=ano+mes;
			var v0 = idben;			//idbeneficiario
			var v1 = parentesco;	//idparentesco
			var v2 = '56';			//idtipocertificado
			var v3=periodo;			//periodo inicio
			var v4=eval(ano+1)+mf;	//periodo final
			var v5='P';				//forma presentacion
		}
		if(cert==1){
			$.ajax({
			url:URL+"aportes/radicacion/grabarCertificado.php",
			type:"POST",
			data:{v0:v0,v1:v1,v2:v2,v3:v3,v4:v4,v5:v5},
			async:false,
			success:function(data){
			alert(data);
			},
		});
		}//if cert
		//Discapacidad
		if(cert==2){
			$.ajax({
			url:URL+"aportes/trabajadores/marcarDiscapacitado.php",
			type:"POST",
		    data:{v0:idPersona,v1:v1,v2:v2},
			async:false,
			success:function(datos){
			if(datos==1){
				alert("Beneficiario marcado como Discapacitado y grabado el certificado!");
			}
		  	else{
				alert("No se pudo guardar el certificado de discapacidad");
			}
		
			}
		});
		}//if cert
		alert("Se grabo la relacion del beneficiario.");
		$("#beneficiarios").dialog("close");
		buscarGrupo();
		observacionesTab(idPersona,1);
		}
	});
	}//end si					
}

function buscarBeneficiarioUpdate(numero,p){
	$("#modificarBeneficiarios").dialog("destroy");	
	$("#modificarBeneficiarios").dialog({
		width:750,
		modal: true,
		draggable:false,
		open:function(event,ui){
			$("#parentescoUpd").focus();
			$("#cedMamaUpd").attr("disabled",false);
			$('#filaEscolaridadUpd').hide();
			buscarRelacionesParaBeneficiario();
		},
		buttons: {
			'Actualizar': function() {
		    var validado=validarCamposBen();
			if(validado>0){
			return false;
			}
			//PARAMETROS PARA MODIFICAR LA INFORMACION PERSONAL DEL BENEFICIARIO
			var parentesco=$("#parentescoUpd").val();
			var idpadremamdre=$("#cedMamaUpd").val();
			var tdoc=$("#tipoDoc3Upd").val();
			var num=$("#identificacion3Upd").val();
		   	var pnom=$("#pNombre3Upd").val();
			var snom=$("#sNombre3Upd").val();
			var pape=$("#pApellido3Upd").val();
			var sape=$("#sApellido3Upd").val();
			var fecha=$("#fecNac3Upd").val();
			var sexo=$("#sexo3Upd").val();
			var tafil3=$("#tipoAfiliacion3Upd").val();
			var capTrab=$("#capTrabajoUpd").val();
			var estado=$("#estadoupd").val();
			var idconyu=$("#cedMamaUpd").val();
			var fasignacion=$("#txtFAsignacion").val();			
			if(estado == 'I')
			 {
     			var motivo=$("#motivoUpd").val();			 
			 }
			 else
			 {
     			var motivo="";			 
			 }			 
				
			$.ajax({
			 url: URL+'phpComunes/pdo.modificar.persona.simple.php',
			 type: "POST",
			 async:false,
			 data: {v0:tdoc,v1:num,v2:pnom,v3:snom,v4:pape,v5:sape,v6:fecha,v7:estado,v8:sexo,v9:idbeneficiario,v10:fasignacion},
			 success: function(datos){
				if(datos==0){
				alert("No se pudo actualizar los datos personales del beneficiario!");
				return false;
				}
				else if (datos>=0) {
					alert("Datos persona actualizados!");
				}
				else{
					alert(datos);
					return false;
				}
			}
		    });
			//PARAMETROS PARA CAMBIAR BANDERA DE GIRO Y TIPO AFILIACION	
			var v0= idrelacion ; 						//idb
			var v1=($("#tipoAfiliacion3Upd").val()==48)?'S':'N';   	//giro
			var v2= $("#estadoUpd").val();   						//estado
			var v3= parseInt($("#motivoUpd").val());				//idmotivo  
			var v4=$("#txtFAsignacion").val();
			//Cambio la bandera 
	   		$.ajax({
				url:"pdo.act.afiliacion.php",
				type:"POST",
				async:false,
				data:{v0:v0,v1:v1,v2:v2,v3:v3,v4:v4},
				success:function(data){
		 		if(data==0){
		    		alert("No se pudo modificar la afiliaci\u00F3n del beneficiario.");
					return false;
		 		}else if (datos>=0) {
					alert("Afiliacion actualizada!");
				}
				else{
					alert(datos);
					return false;
				}
			  }});
			//PARAMETROS PARA ANEXAR CERTIFICADOS(VALIDO PARA RENOVACIONES)
			if($("#filaEscolaridadUpd").is(':visible') ){
				if($("#certificado3Upd").val()== 0)	
				{
		          $.getJSON('anularCertificado.php',{v0:idcert},function(datos2){
					  if(datos2 ==0)
					  {
						  alert("No se pudo anular el Certificado");
					  }
					  				  
				  });
				}
			else	
				{
					idtipcert=$("#certificado3Upd").val();
					var fchcertificado=$("#fEscolaridadUpd").val();
				  $.getJSON('actualizarCertificado.php',{v0:idcert,v1:idtipcert,v2:fchcertificado},function(datos2){
					  if(datos2==0)
					  {
						alert("No se pudo Actulizar el Certificado");  
					   }
					  
				});
				}						
				
			}	//if PARAMETROS CERTIFICADOS  
			  $("#modificarBeneficiarios").dialog("close");
			  buscarGrupo();//recargar la lista de beneficiarios
			}//actualizar		  
		},
		close:function(){
			$("#modificarBeneficiarios ul").remove();
			$("#modificarBeneficiarios  table.tablero input:text").val('');
			$("#modificarBeneficiarios  table.tablero input:hidden").val('');//fecha nacimiento
			$("#modificarBeneficiarios  table.tablero select").val('Seleccione');
			$("#modificarBeneficiarios input:checkbox,#modificarBeneficiarios input:radio").attr("checked",false);
			// TABLA DINAMICA PARA DATOS EMPRESA TRABAJDOR ACTIVO
			$("table[name='dinamicTable']").remove();
			//$("#cedMamaUpd").attr("disabled",false);
		}//end boton												
	});	
		
	var num=$.trim(numero);
	 $.ajax({
		url:URL+'phpComunes/buscarPersona.php',
		type:"POST",
		data:{v0:num},
		dataType:"json",
		async:false,
		success:function(data){
	    if(data==0){
	    return false;
	    }
	    
		$.each(data,function(i,fila){
			idnb=fila.idpersona;
			$('#tipoDoc3Upd').val(fila.idtipodocumento);
			$('#identificacion3Upd').val(fila.identificacion);
	   	 	$('#pNombre3Upd').val(fila.pnombre);
	   	 	$('#sNombre3Upd').val(fila.snombre);
	   	 	$('#pApellido3Upd').val(fila.papellido);
	   	 	$('#sApellido3Upd').val(fila.sapellido);
	   	 	$('#fecNac3Upd').val(fila.fechanacimiento);
	   	 	$('#sexo3Upd').val(fila.sexo);
	   	 	/* if(fila.capacidadtrabajo=='I'){
	   	 		$('#capTrabajoUpd').attr("disabled",true);
	   	 	}else{
	   	 		$('#capTrabajoUpd').attr("disabled",false);
	   	 	} */
	   	 	$('#capTrabajoUpd').val(fila.capacidadtrabajo);
		 }); //each
	     },
		complete:function(){
		 //Buscar afiliacion por idbeneficiarios, parentesco y e id trabajador
		 var idtrabajador=$("#idPersona").val();
		 //idnb=viene del each anterior
		 //analizar parentesco (p)
		 var idtrabajador=$("#idPersona").val();
			switch(p){
			 	case "HERMANO(A)"  : var parentesco=37; break;
			 	case "PADRE/MADRE" : var parentesco=36; break;
			 	case "HIJASTRO" : var parentesco=38; break;
			 	default : var parentesco=35; break;
			}
			$.ajax({
				url:URL+'phpComunes/buscarBeneficiariosP.php',
				type:"POST",
				async:false,
				dataType : "json",
				data:{v0:idnb,v1:parentesco,v2:idtrabajador},
				success: function(data){
					if(data != 0){
		    		$("#parentescoUpd").val(data[0].idparentesco);
					if(data[0].giro=='S'){
						$("#tipoAfiliacion3Upd").val(48);
					}else{
						$("#tipoAfiliacion3Upd").val(49);
					}
					$("#estadoUpd").val(data[0].estado);
					$('#txtFAsignacion').val(data[0].fechaasignacion);
			     	$("#cedMamaUpd").val(data[0].idconyuge);
			     	idrelacion=data[0].idrelacion;
				}
				}
			})
		}
	     
	 }); //ajax
		$.getJSON(URL+'phpComunes/pdo.b.certificado.idp.php',{v0:idnb},function(datos){
			  if(datos != 0){
				 idcert=datos[0].IDCERTIFICADO;
				 idtipcert  =datos[0].IDTIPOCERTIFICADO;
				 var fchpresen  =voltiarFecha(datos[0].FECHAPRESENTACION);
				 $('#certificado3Upd').val(idtipcert);
				 $('#fEscolaridadUpd').val(fchpresen);
				 $('#filaEscolaridadUpd').show();
			  }
		    });
			
			
		//????	  
		$.ajax({
		url:URL+'phpComunes/buscarBeneficiarioTodo021.php',
		type:"POST",
		data:{v0:idnb},
		dataType:"json",
		async:false,
		success:function(data){
	    if(data==0){
	    return false;
	    }
	    
		$.each(data,function(i,fila){
			//$("#cedMamaUpd").val(fila.idconyuge);
		});
		}});
}

function buscarRelacionesParaBeneficiario(){
	idPersona=$("#idPersona").val();
	$.ajax({
		url:URL+"phpComunes/pdo.buscar.relaciones.php",
		type:'POST',
		dataType:'json',
		async:false,
		data :{v0:idPersona},
		success:function(data){
		$("#cedMamaUpd option:not(':first',:eq(1))").remove();
		if(data==0){
			MENSAJE("El trababajador NO tiene relaciones de convivencias.");
			return false;
			}
			$.each(data,function(i,fila){
				idc=fila.IDCONYUGE; 
				numC=fila.IDENTIFICACION;	
				nombreC=$.trim(fila.pnombre)+" "+$.trim(fila.snombre)+" "+$.trim(fila.papellido)+" "+$.trim(fila.sapellido);
				conviven=fila.CONVIVEN;
				//Agregar a la tabla beneficiarios oculta los datos del conyuge
				$("#cedMamaUpd").append("<option value='"+idc+"' name='"+numC+"'>"+numC+"</option>");
			});//each
		}
		});
}

function persona_simple(obj,obj2,td){
	//$("#dialog-persona").dialog("destroy"); //Destruir dialog para liberar recursos
	var num=obj.value;
	var id=0;
	
	$('#dialog-persona select,#dialog-persona input[type=text]').val('');
	$("#txtNumeroP").val(num);
	$('#pNombre').focus();
	$("#tDocumento").val(td);
	$("#dialog-persona").dialog({
		height: 225,
		width: 650,
		draggable:false,
		modal: true,
		/*open: function(){
			$('#dialog-persona').html('');
			$.get(URL+'phpComunes/personaSimple.php',function(data){
				$('#dialog-persona').html(data);
			});
			$('#dialog-persona select,#dialog-persona input[type=text]').val('');
			$("#txtNumeroP").val(num);
			$('#pNombre').focus();
			alert(num);
		},*/
		buttons: {
			'Guardar datos': function() {
				//$("#dialog-persona").dialog("destroy");
			    //obj.value=$('#tDirCompleta').val();
				var campo0=$("#tDocumento").val();
				var campo1=$.trim( $("#txtNumeroP").val() );
				var campo4=$.trim($("#pNombre").val());
				var campo5=$.trim($("#sNombre").val());
				var campo2=$.trim($("#pApellido").val());
				var campo3=$.trim($("#sApellido").val());
				if(campo1==""){
					alert("Falta numero de identificacion");
					return false;
					}
				if(campo4==""){
					alert("Falta primer nombre");
					return false;
					}
				if(campo2==""){
					alert("Falta primer apellido");
					return false;
					}
				$.ajax({
				url: URL+'phpComunes/pdo.insert.persona.php',
				type: "POST",
				data: "submit=&v0="+campo0+"&v2="+campo2+"&v3="+campo3+"&v4="+campo4+"&v5="+campo5+"&v1="+campo1,
				success: function(datos){
				if(datos==0){
					alert("NO se guardaron los datos de la persona!");
				}
				else{
					obj.focus();
					obj2.value=datos;
					id=datos;
				}
				}
			});
				obj.focus();
				$(this).dialog('close');
				return id;
//console.log(obj2);
				$('#dialog-persona select,#dialog-persona input[type=text]').val('');
			}
		},
		close: function() {
			//$("#dialog-persona").dialog('close');
			$('#dialog-persona select,#dialog-persona input[type=text]').val('');
			//obj2.focus();
			$(this).dialog("destroy");
			//focus al siguiente cmapo de texto; 
		}
	});
}

function validarMotivo(valor){
	 if(valor=='I'){
		if(confirm("Esta seguro de inactivar esta afiliacion?")==true){
		$("#motivoUpd").attr("disabled",false);
		}	
	 }else{
		$("#motivoUpd").attr("disabled",true);
		$("#motivoUpd").val(0);
	}	
	}//fn

function validarCamposBen(){
	num=$.trim($("#identificacion3Upd").val());
    var error=0;
	//reseteo clase error
	$("#modificarBeneficiarios .tablero :input.ui-state-error,#modificarBeneficiarios .tablero select.ui-state-error").removeClass("ui-state-error");
	
	if($("#tipoDoc3Upd").val()=='0'){
	$("#tipoDoc3Upd").addClass("ui-state-error");
	error++;
	}
	if(num.length==0||num==''){
    $("#tipoDoc3Upd").addClass("ui-state-error");
	error++;
	}
	if($("#pNombre3Upd").val()==''){
	$("#pNombre3Upd").addClass("ui-state-error");
	error++;
	}
	if($("#pApellido3Upd").val()==''){
	$("#pApellido3Upd").addClass("ui-state-error");
	error++;
	}
	if($("#sexo3Upd").val()=='0'){
	$("#sexo3Upd").addClass("ui-state-error");
	error++;
	}
	if($("#fecNac3Upd").val()==''){
	$("#fecNac3Upd").addClass("ui-state-error");
	error++;
	}
	if($("#estadoUpd").val()=='I'){
	 	if($("#motivoUpd").val()=='0'){
	 	$("#motivoUpd").addClass("ui-state-error");
		error++;
	 	}
	}
	if($("#txtFAsignacion").val()==''){
		$("#txtFAsignacion").addClass("ui-state-error");
		error++;
		}
	
	if(error==0){
	return 0;
	}else{
	return error;
	}
}

function limpiarBene(){
	$("#parentesco").val(0);
	$("#tipoAfiliacion3").val(0);
	$("#pNombre3").val('');
	$("spNombre3").val('');
	$("#pApellido3").val('');
	$("#sApellido3").val('');
	$("#fecNacHide3").val('');
}

