/* 
* CONTROL DE VALIDACIONES DE LIQUIDACIONES
* Autor: Andrés Felipe Lara Nieto
* Fecha: 04-Mar-2011
*/


URL=src();
var aportesOK=0;
function buscarAportes(){	
	aportesOK=1;
  //$("#aportes").tablesorter()//.tablesorterPager({container: $("#pager")});
  $("#aportes tbody tr").remove();
 //var ide=$("#txtIde").val();
  $.ajax({
	      url:URL+'phpComunes/buscarAportes.php',
		  type:"POST",
		  dataType:"json",
		  data: {v0:idEmpresa},
		  async:false,
		  success:function(data){
	 			  if(data==0){MENSAJE("NO existen aportes.");return false;}
					  $.each(data, function(i,n){ 
						  var row=(data==false ? "-" : data.ultimo );
						  var no=data.length;
						  $("#aportes caption span").html(no+" registros encontrados."); 
					     $("#aportes tbody").append("<tr><td>"+n.nit+"</td><td>"+n.idaporte+"</td><td style=text-align:center>"+n.periodo+"</td><td style=text-align:right>"+formatCurrency(n.valornomina)+"</td><td style=text-align:right>"+formatCurrency(n.valoraporte)+"</td><td style=text-align:right>"+n.trabajadores+"</td><td style=text-align:center>"+n.ajuste+"</td><td>"+n.fechapago+"</td></tr>"); 
         	          });//end each emrpesa Id
			},//succes
			complete:function(){
				  //Actualizar Tabla de sorter 
				  $("#aportes tbody tr:odd").addClass("vzebra-odd");
				  $("#aportes").tablesorter({headers: {7: { sorter: false}}});
				  $("#aportes").tablesorterPager({container: $("#pager"),seperator:" de "}).trigger("updateCell");
				  $("#aportes").trigger("update");
			 }						
	     });//ajax 
}		   
  