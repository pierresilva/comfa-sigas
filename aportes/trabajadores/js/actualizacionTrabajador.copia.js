/*
* @autor:      Ing. Orlando Puentes
* @fecha:      Octubre 1 de 2010
* objetivo:
*/
var idPersona=0;
var idnp=0;
var idempresa=0;
var existe=0;
var idnb=0;

var URL=src();
$(document).ready(function(){

$("#tabs").hide();

//Dialog  documentos	
	 $("#imagen").dialog({
	 	autoOpen:false,
		modal: true,
		hide: 'slide',
		show:'fade',
		width:900,
		height:700
	  });

//DATEPICKERS
$("#fecIngreso").datepicker({changeMonth: true,changeYear: true});
$("#fEscolaridad,#fUniversidad,#fDiscapacidad,#fSupervivencia").datepicker({changeMonth: true,changeYear: true});
$(".ui-datepicker").css("z-index",3000);

var idDiv;
var errorTab;
var lista;
var op;

//... TABS...//
$("#tabs").tabs(); //end tab

//VALIDAR TECLA ENTER
$("#cedula").keypress(function(event){
		if(event.keyCode==13){
	       $("#buscarTrabajador").click();
	      }
     });

//... BUSCAR TRABAJADOR 
$("#buscarTrabajador").bind("click",function(){
	 $(" #tabs-5").load("tab/tablaImagenes.php",{v0:$("#cedula").val()});
	 limpiarCampos();
	 $("#tablaCExiste tr:not(':first')").remove();//limpiar tabla conyuges
	 $("#tablaBExiste tr:not(':first')").remove();//limpiar tabla beneficiarios
     //Limpiar span de validacion
     $(this).next("span.Rojo").html(""); 

    if($("#cedula").val() ==''){	
   		  $(this).next("span.Rojo").html("Ingrese  el No. de la identificaci\u00F3n.").hide().fadeIn("slow");
		  $("#tabs").hide();
		  $("#tabs").tabs("destroy"); 
   }else{
	 if($("#cedula").val().length==9){
		 $(this).next("span.Rojo").html("La identificaci\u00F3n NO debe ser de 9 digitos.").hide().fadeIn("slow");
		  $("#tabs").hide();
		  $("#tabs").tabs("destroy"); 
		 }
		//Aqui iria el ajax para traer los datos de la bas de datos
 	    
	
				 
 				    
}//end else
});//fin buscarT-----------------------------------

//... ACTUALIZAR REGISTRO...//
$("#actualizar").click(function(){		      
   	var error=0;
	 $("table.tablero td input:text.ui-state-error,table.tablero td select.ui-state-error").removeClass("ui-state-error");
	
              	    //...DATOS PERSONALES 
	                var tipoDoc=$("#tipoDoc").val();
	                var cedula=$("#identificacion").val();
	                var pNombre=$("#pNombre").val();
	                var pApellido=$("#pApellido").val();
	                var sNombre=$("#sNombre").val();//no obligado
	                var sApellido=$("#sApellido").val();//no obligado
			var nombreCorto=$('#nombreCorto').val(); //nombre corto
					
	                var sexo=$("#sexo").val();
	                var direccion=$("#tDireccion").val();
	                var barrio=$("#barrio").val();
	                var telefono=$("#telefono").val();
	                var celular=$("#celular").val();
	                var email=/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test($("#email").val());
	                var tipVivienda=$("#tipoVivienda").val();
	                var deptRes=$("#combo1").val();//$("#deptRes").val();
	                var ciudadRes=$("#combo2").val();//$("#ciudadRes").val();
	                var zonaRes=$("#combo3").val();//$("#zonaRes").val();
	                var estadoCivil=$("#estadoCivil").val();
	                var depNac=$("#combo4").val();
	                var ciudadNac=$("#combo5").val();
	                var capTrabajo=$("#capTrabajo").val();
	                var profesion=$("#profesion").val();
	                var fecNac=$("#fecNacHide").val();	//rescato elv alor de la fecha del cmapo oculto
					
				   var tipForm=$("#tipForm").val();
	               var tipAfiliacion=$("#tipAfiliacion").val();
	               var fecIngreso=$("#fecIngreso").val();
	               var horasDia=$("#horasDia").val();
	               var horasMes=$("#horasMes").val();
	               var salario=$("#salario").val();
	               var agricola=$("#agricola").val();
	               var cargo=$("#cargo").val();
	               var estado=$("#estado").val();
	               var traslado=$("#traslado").val();
	               var codCaja=$("#codCaja").val();	 
                    
					//BENEFICIARIOS - CONYUGES-CERTIFICADOS
				   arrayFinalB;
				   arrayFinalC; 
				   arrayFinalCert;
				   
				    //---------------------TIPO DOC----------------------  
                    if(tipoDoc=='0'){$("#tipoDoc").addClass("ui-state-error");error++}
					
					  if(cedula==''){
						  $("#identificacion").addClass("ui-state-error");error++;
					} else {
                        
                      if(cedula.length<3){
                      $("#identificacion").addClass("ui-state-error");error++;
	                  }else {
	                        if(cedula.length==9){
	                           $("#identificacion").addClass("ui-state-error");error++;
	                          }
					    }//end else
	                }//end else
                  //----------------PRIMER NOMBRE Y APELLIDO---------------------------   
                  if(pNombre==''){ $("#pNombre").addClass("ui-state-error");error++;}
				  if(pApellido==''){ $("#pApellido").addClass("ui-state-error");error++;}
			      
				  //-------------------SEXO-----------------------
			      if(sexo=='0'){ $("#sexo").addClass("ui-state-error");error++;}
                  
				  //-------------------DIRECCION-----------------------
			      if(direccion==''){ $("#tDireccion").addClass("ui-state-error");error++;}
			      
				  //-------------------BARRIO----------------------
				  if(barrio=='0'){ $("#barrio").addClass("ui-state-error");error++;}
				//-------------------TELEFONO----------------------
			    if(telefono==''){
					 $("#telefono").addClass("ui-state-error");error++;
				}else{ 
				     	
					 if(telefono.length<7){
						  $("#telefono").val('Faltan digitos').addClass("ui-state-error");error++;
					 }
				}
			  //---------------------EMAIL------------------------
			if($("#email").val()!=''){
			
			    if(!email) {
							 $("#email").addClass("ui-state-error");error++;
							}else{
								  email=$("#email").val();
								  }
			}else{
				 email="";
				}
			//------------------------------------------------------
			
			if(tipVivienda=='0'){$("#tipoVivienda").addClass("ui-state-error");error++;}
			if(deptRes=='0'){$("#combo1").addClass("ui-state-error");error++;}
			if(ciudadRes=='0'||ciudadRes==''){$("#combo2").addClass("ui-state-error");error++;}
			if(zonaRes=='0'){$("#combo3").addClass("ui-state-error");error++;}
			if(estadoCivil=='0'){$("#estadoCivil").addClass("ui-state-error");error++;}
			if(depNac=='0'){$("#depNac").addClass("ui-state-error");error++;}
			if(ciudadNac=='0'||ciudadNac==''){$("#ciudadNac").addClass("ui-state-error");error++;}
			if(capTrabajo=='0'){$("#capTrabajo").addClass("ui-state-error");error++;}
			
			//----------------------FECHA DE NACIMIENTO---------------
			if(fecNac=='' || fecNac=='mmddaaaa'){
				$("#fecNac").addClass("ui-state-error");error++;
			}
			
			//------------------DATOS AFILIACION-------------------------------
			  
			  //----------------------TIPO FORMULARIO-----------------
			       if(tipForm=='Seleccione..'){$("#tipForm").addClass("ui-state-error");error++;}
			
			       //------------------TIPO AFILIACION--------------------------
			 	   if(tipAfiliacion=='Seleccione..'){$("#tipAfiliacion").addClass("ui-state-error");error++;}
			
			        //------------------TIPO AFILIACION--------------------------
			 	   if(fecIngreso==''){$("#fecIngreso").addClass("ui-state-error");error++;}
				   
				   //----------------HORAS DIA-------------------------------
				   if(horasDia==''){$("#horasDia").addClass("ui-state-error");error++;}
			
			       //--------------HORAS MES---------------------------------
				   if(horasMes==''){
					   $("#horasMes").addClass("ui-state-error");error++;
				   }else{
					   if(horasMes>240||horasMes<=0){
						$("#horasMes").addClass("ui-state-error");error++;
						 }
					}
								
					//------------------------SALARIO-------------------------
			         if(salario==''){$("#salario").addClass("ui-state-error");error++;}
			
			 		//--------------AGRICOLA----------------------------------
					if(agricola=='Seleccione..'){$("#agricola").addClass("ui-state-error");error++;}
					//-------------------CARGO-------------------------------
					if(cargo=='Seleccione..'){$("#cargo").addClass("ui-state-error");error++;}
					//---------------------ESTADO-----------------------------
			        if(estado=='Seleccione..'){$("#estado").addClass("ui-state-error");error++;}
			        //----------------------TRASLADO------------------------
			  	    if(traslado=='Seleccione..'){
						$("#traslado").addClass("ui-state-error");error++;
						}
			
			      //------------------------COD CAJA----------------------
			     if(traslado=='S'){
					  if(codCaja=='0'){$("#codCaja").addClass("ui-state-error");error++;}
					  }
				 
					
                   
            //-----------------------FORMULARIO SIN ERRORES------------------------------------//
             if(error==0){
     alert("FORMULARIO COMPLETO\n\nCONYUGES\n["+arrayFinalC.join("]\n\n[")+"]\n\nBENEFICIARIOS\n["+arrayFinalB.join("]\n[")+"]\n\nCERTIFICADO\n["+arrayFinalCert.join("]\n[")+"]");
			 }else{
				 alert("Faltan datos en por rellenar ,revisar los TABS las casillas en ROJO");
				 }
			 
  });//actualizar
	
//... ELIMINAR REGISTRO................//	
$("#eliminarRegistro").click(function(){
	if($("#cedula").val()!="" && $("#tabs").is(":visible")){
	   if(confirm("\u00BFDesea eliminar este registro?")){
		    alert("Eliminar");
			
		    //$.post('',{},function(data){v0:$("#cedula").val()});
			 $("#buscarTrabajador").next("span.Rojo").html("");
			 pn=""; sn="";pa=""; sa="";//Limpiar nombre corto
			 limpiarCampos();
			 $("#tabs").hide();
			 $("#tabs").tabs("destroy"); 
		  }
	}else{
			 $("#buscarTrabajador").next("span.Rojo").html("Ingrese  un valor para eliminar.").hide().fadeIn("slow");
			}
		
});//........................elminiar click
	

 //----TRASLADO VALIDACION
  $("#traslado").change(function(){
	  
	  if( $("#traslado").val()=='S'){
								$("#codCaja").next("img").show();
								
								}else{
									$("#codCaja").next("img").hide();
									$("#codCaja").val('Seleccione..');
									}
	  
	  });

//...ABRIR DIALOG PROFESION...//

$("#profesionDialog").dialog({
	autoOpen:false,
	modal: true,
	width: 400,
	buttons: {'Grabar': function() {
		$(this).dialog("close");
		//alert($("#nuevaProf").val());
		var newOption= $("#nuevaProf").val();
		var iddefinicion=20;
		var codigo=$('#profesion option').length;
		var detalledefinicion=$("#nuevaProf").val();
		var concepto=$("#nuevaProf").val();
		$.post(URL+'phpComunes/nuevoDetalle.php',{v0:iddefinicion,v1:codigo,v2:detalledefinicion,v3:concepto},
		function(datos){
			if(datos==1){
				alert("La profesion no se pudo guardar!, intente de nuevo");
				}
			else{
				alert("La profesion:  "+ detalledefinicion +" se grab\u00F3");
				}
			});
			
		$.post(URL+"phpComunes/barrios.php", {v0:20,v1:1}, function(data){
				$("#profesion").html(data);
				$('#profesion option:last').after("<option value='otro'>Otro..</option>");
				//$('#barrio option:fist').before("<option value=0>Seleccione..</option>");
				$("#profesionDialog").dialog("close");
		        $("#profesion").trigger("change");
		});
		}		  
		}//end boton
		});
//...DIALOG PROFESION...//
	   	$("#profesion").change(function(){
    	if($(this).val()=='Otro..'){
        $('#profesionDialog').dialog('open');
        }//end if
		});//fin change
		
//...DIALOG BARRIOS...//
	   	$("#barrio,#barrio2").change(function(){
              var elemento=$(this);
		      $("#barrioDialog").dialog("destroy");  
				
			  if($(this).val()=='Otro..'){
    		
			       $("#barrioDialog").dialog({
	                  	modal: true,
	                    width: 400,
						data:{elemento:elemento},
	                    buttons: {'Grabar': function() {
		                $(this).dialog("close");
		               if($("#nuevoBarrio").val()==""){
			            return false;
			             }
		var newOption= $("#nuevoBarrio").val();
		var iddefinicion=19;
		var codigo=elemento.children('option').length;
		var detalledefinicion=$("#nuevoBarrio").val();
		var concepto=$("#nuevoBarrio").val();
		$.post(URL+'phpComunes/nuevoDetalle.php',{v0:iddefinicion,v1:codigo,v2:detalledefinicion,v3:concepto},
		function(datos){
			if(datos==1){
				alert("El barrio no se pudo guardar!, intente de nuevo");
				}
			else{
				alert("El barrio "+ detalledefinicion +" se grab\u00F3");
				$("#nuevoBarrio").val('');
				}
			});
			
		$.post(URL+"phpComunes/barrios.php", {v0:19,v1:1}, function(data){
		
			elemento.children('option:last').after("<option value='otro'>Otro..</option>");
			elemento.trigger("change");
				//$('#barrio option:fist').before("<option value=0>Seleccione..</option>");
		        
		});
		
		elemento.val('Seleccione..');
		}		  
		}//end boton
		});
			
        }//end if
		});//fin change
				
// fin nuevo barrio

//...DIALOG CARGO ABRIR...//
	   	$("#cargo").change(function(){
    	if($(this).val()=='Otro..'){
        $("#cargoDialog").dialog('open');
        }//end if
		});//fin change		
		

//... DIALOG CARGO ...//
$("#cargoDialog").dialog({
	autoOpen:false,
	modal: true,
	width: 400,
	buttons: {'Grabar': function() {
		$(this).dialog("close");
		if($("#nuevoCargo").val()==""){
			return false;
			}
		var newOption= $("#nuevoCargo").val();
		var iddefinicion=19;
		var codigo=$('#cargo option').length;
		var detalledefinicion=$("#nuevoCargo").val();
		var concepto=$("#nuevoCargo").val();
		//AGREGAR CARGO AL OPTION
		/*$.post(URL+'phpComunes/nuevoDetalle.php',{v0:iddefinicion,v1:codigo,v2:detalledefinicion,v3:concepto},
		function(datos){
			if(datos==1){
				alert("El cargo no se pudo guardar!, intente de nuevo");
				}
			else{
				alert("El cargo "+ detalledefinicion +" se grab\u00F3");
				}
			});*/
			/*
		$.post(URL+"phpComunes/barrios.php", { v0:19,v1:1 }, function(data){
				$("#barrio").html(data);
				$('#barrio option:last').after("<option value='otro'>Otro..</option>");
				//$('#barrio option:fist').before("<option value=0>Seleccione..</option>");
		      
		});
		*/
		$("#cargo").val('Seleccione..');
		}		  
		},//end boton
		close:function(){
			
			$("#nuevoCargo").val('');
			}
		});
//-----------------

 //...FUNCION LIMPIAR CAMPOS...//
	function limpiarCampos(){
		
		            $("table.tablero input:text").not("#cedula").val('');
					$("table.tablero select").val('Seleccione..');
		
		}//end limpÃ­ar 
	
//...VALIDACION DE LA FECHA DE NACIMIENTO...//
	
	 var dia,mes,anio,fecha,mesNew,indexMes,anioActual,fecTemp,mesTemp,hoy,edad;
	 
	 $("#fecNac,#fecNac2").blur(function(){
								
								
							 						
								if($(this).val()=='')
								{
									$(this).val('mmddaaaa');
								}else {
								 fecha=$(this).val();
								 mes=fecha.slice(0,2);
								 mesTemp=mes;
								 dia=fecha.slice(2,4);
								 anio=fecha.slice(4,8);
								 indexMes=mes.slice(0,1);	
								 anioActual=new Date();
								 hoy=anioActual.getFullYear();
								 edad=parseInt(hoy)- parseInt(anio);
								 
								 if(fecha.length<8){
									 
									alert("La fecha debe tener 8 digitos"); 
									$(this).val('');
									$(this).focus();
								 }else{
											
									if(indexMes < 0 || indexMes > 1 || mes >12|| mes <1){
										 
										 alert("El mes esta mal digitado");
										 $(this).focus();
									 }else{
									 
									    if(dia>31|| dia <1){
										alert("El dia debe tener de 1 a 31 dias");	
										$(this).focus();
										}else{
									          if(anio<1900 || anio>anioActual.getFullYear()){
												  
												  alert("El aÃ±o esta incorrecto");
												  $(this).focus();
											  }else{
												  
												 
													    if(edad<15||edad>65){
													     alert("La edad NO debe ser de 15 a 65 aÃ±os.");
												    	 $(this).focus();
														}
													 
												else{		  
												  
								 switch(mes){
									case '01':mes='Ene';
									          break;
									case '02':mes='Feb';
									          break;
									case '03':mes='Mar';
									          break;
									case '04':mes='Abr';
									          break;
									case '05':mes='May';
									          break;
									case '06':mes='Jun';
									          break;
									case '07':mes='Jul';
									          break;
									case '08':mes='Ago';
									          break;
									case '09':mes='Sep';
									          break;
									case '10':mes='Oct';
									          break;
									case '11':mes='Nov';
									          break;
									default:mes='Dic';
									         break; 
									 
								 }//end switch
								      //alert(mes+"-"+dia+"-"+anio);
									  $(this).val(mes+", "+dia+" de "+anio);
									  fecTemp=mesTemp+"/"+dia+"/"+anio;
									  //alert("Valor a guardar:"+fecTemp);
									  $(this).siblings(":hidden").attr("value",fecTemp);
									  
											
										}//end else edad
									 }//en else
								   }//end else
								 }//end else
							  }//end else
							}//else principal
							}).focus(function(){
								  if($(this).val!=''){
									  if($(this).val()!='mmddaaaa')
									   $(this).val(mesTemp+dia+anio); 
								  }else{
									  $(this).val(''); 
								  }
								 
													
								
								});//end blur y focus
								
								
		//-------------------------VALIDACION FECHA BENEFICIARIOS
		
		$("#fecNac3").blur(function(){
								
								$("#parentesco").val("0");
								$("#tipoAfiliacion3").val("0");
								
								if($(this).val()=='')
								{
									$(this).val('mmddaaaa');
								}else {
								 fecha=$(this).val();
								 mes=fecha.slice(0,2);
								 mesTemp=mes;
								 dia=fecha.slice(2,4);
								 anio=fecha.slice(4,8);
								 indexMes=mes.slice(0,1);	
								 anioActual=new Date();
								 hoy=anioActual.getFullYear();
								 edad=parseInt(hoy)- parseInt(anio);
								 
								 if(fecha.length<8){
									 
									alert("La fecha debe tener 8 digitos"); 
									$(this).focus();
								 }else{
											
									if(indexMes < 0 || indexMes > 1 || mes >12|| mes <1){
										 
										 alert("El mes esta mal digitado");
										 $(this).focus();
									 }else{
									 
									    if(dia>31|| dia <1){
										alert("El dia debe tener de 1 a 31 dias");	
											$(this).focus();
										}else{
									          if(anio<1900 || anio>anioActual.getFullYear()){
												  
												  alert("El aÃ±o esta incorrecto");
												  $(this).focus();
											  }else{
												  							   
												  
								 switch(mes){
									case '01':mes='Ene';
									          break;
									case '02':mes='Feb';
									          break;
									case '03':mes='Mar';
									          break;
									case '04':mes='Abr';
									          break;
									case '05':mes='May';
									          break;
									case '06':mes='Jun';
									          break;
									case '07':mes='Jul';
									          break;
									case '08':mes='Ago';
									          break;
									case '09':mes='Sep';
									          break;
									case '10':mes='Oct';
									          break;
									case '11':mes='Nov';
									          break;
									default:mes='Dic';
									         break; 
									 
								 }//end switch
								      //alert(mes+"-"+dia+"-"+anio);
									  $(this).val(mes+", "+dia+" de "+anio);
									  fecTemp=mesTemp+"/"+dia+"/"+anio;
									  //alert("Valor a guardar:"+fecTemp);
									  $(this).siblings(":hidden").attr("value",fecTemp);
									  
											
										
									 }//en else
								   }//end else
								 }//end else
							  }//end else
							}//else principal
							}).focus(function(){
								  if($(this).val!=''){
									  if($(this).val()!='mmddaaaa')
									   $(this).val(mesTemp+dia+anio); 
								  }else{
									  $(this).val(''); 
								  }
								   
								
								});//end blur y focus						
								
	 
	//...FIN VALIDACION FECHA...//
	var txt="",num=0;
// CREAR NOMBBRE CORTO PARA TARJETA	
/*
$("#pNombre,#sNombre,#pApellido,#sApellido").blur(function(){
	var id=$(this).attr("id");
	 switch(id){
		 case "pNombre": txt=""; break;
		 }
		txt+=$(this).val()+" ";
		$("#nombreCorto").val(txt);
		num=parseInt(txt.length);
		$("#nombreCorto").next("span").html(num);		
});
*/
var pn="";
var sn="";
var pa="";
var sa="";
var nu=0;
var i;
var patron=new RegExp('[\u00F1|\u00D1]');
$("#pNombre").blur(function(){
	pn=$(this).val();
	//---Reemplazar Ã‘
	for(i=1;i<=pn.length;i++){
	pn=pn.replace(patron,"&");
	}
	//-------------
	txt=pn+" "+sn+" "+pa+" "+sa;
	txt=txt.toUpperCase();
	
	
	$("#nombreCorto").val(txt)
	num=parseInt(txt.length);
	$("#nombreCorto").next("span").html(num);
	$("#nombreCorto").trigger("change");
});

$("#sNombre").blur(function(){
	sn=$(this).val();
	//---Reemplazar Ã‘
	for(i=1;i<=sn.length;i++){
	sn=sn.replace(patron,"&");
	}
	//-------------
	txt=pn+" "+sn+" "+pa+" "+sa;
	txt=txt.toUpperCase();
	$("#nombreCorto").val(txt)
	num=parseInt(txt.length);
	$("#nombreCorto").next("span").html(num);
	$("#nombreCorto").trigger("change");
});
$("#pApellido").blur(function(){
	pa=$(this).val();
	//---Reemplazar Ã‘
	for(i=1;i<=pa.length;i++){
	pa=pa.replace(patron,"&");
	}
	//-------------
	txt=pn+" "+sn+" "+pa+" "+sa;
	txt=txt.toUpperCase();
	$("#nombreCorto").val(txt)
	num=parseInt(txt.length);
	$("#nombreCorto").next("span").html(num);
	$("#nombreCorto").trigger("change");
});
$("#sApellido").blur(function(){
	sa=$(this).val();
	//---Reemplazar Ã‘
	for(i=1;i<=sa.length;i++){
	sa=sa.replace(patron,"&");
	}
	//-------------
	txt=pn+" "+sn+" "+pa+" "+sa;
	txt=txt.toUpperCase();
	$("#nombreCorto").val(txt)
	num=parseInt(txt.length);
	$("#nombreCorto").next("span").html(num);
	$("#nombreCorto").trigger("change");
});

$("#nombreCorto").change(function(){
	if(num>30){
		if(sn.length>0){
			sn=sn.slice(0,1);
			txt=pn+" "+sn+" "+pa+" "+sa;
			txt=txt.toUpperCase();
			$("#nombreCorto").val(txt);
			num=parseInt(txt.length);
			$("#nombreCorto").next("span").html(num);
			}
		if(num>30)
		if(sa.length>0){
			sa=sa.slice(0,1);
			txt=pn+" "+sn+" "+pa+" "+sa;
			txt=txt.toUpperCase();
			$("#nombreCorto").val(txt);
			num=parseInt(txt.length);
			$("#nombreCorto").next("span").html(num);
			}
		}
	});
	$("#nuevoGrupo").click(function(){
	if($("#selParentesco").val()=='Seleccione..'){
		$(this).next("span.Rojo").html("Seleccione un parentesco.");
	}						
	if($("#selParentesco").val()=='c'){
		$(this).next("span.Rojo").html("");
		conyugeDialog(1);//1 es para insertar 2 para actualizar
		//$("#conyuge").dialog('open');							 
	}								 
	if($("#selParentesco").val()=='b'){
		$(this).next("span.Rojo").html("");
		beneficiarioDialog(1);
		//$("#beneficiarios").dialog('open');
	}
	});
	
 
//-----TIPO AFILIACION BENEFICIARIOS CHANGE PARA CERTIFICADOS			
$("#tipoAfiliacion3,#parentesco,#capacidad").change(function(){
	
	 var p=$("#parentesco").val();
	 $("#filaEscolaridad,#filaUniversidad,#filaSupervivencia,#filaDiscapacidad").hide();
	  //...MOSTRARA COMBO Y CAMPO DE CEDULA DE LA CONYUGE O MADRE ...//
	  $("#cedMama").show();
	  $("#cedMama").parent("td").prev().html('C&eacute;dula Padre &oacute; Madre');
	 
	 if($("#fecNac3").val()==''||$("#fecNac3").val()=='mmddaaaa'){
		 alert("Ingrese una fecha de nacimiento para el BENEFICIARIO.");
		 return false;
	}
	   
	//cod 35=HIJO    
	if(p=="35"){
		  if(edad>12&&edad<=40&&$("#tipoAfiliacion3").val()=='48'){
		
		   $("#filaEscolaridad,#filaUniversidad").show();//Mostrar certificado de  estudios
			
		   }
		   
		if($("#capacidad").val()=='D'){
		$("#filaDiscapacidad").show();//Certificado Discapacidad
		$("#filaEscolaridad").hide();
				$("#filaUniversidad").hide();//Los discapacitados no presentan ni escolaridad ni universidad
		}
	}
	//    cod 36= PADRE/MADRE    
	if(p=="36"){
	    
	    //si edad es mayor a 60 y es subsidio
		if(edad>60&&$("#tipoAfiliacion3").val()=='48')	{
		$("#filaSupervivencia").show();//Mostrar certificado de supervivencia
		}
		if($("#capacidad").val()=='D'){
		$("#filaDiscapacidad").show();//Certificado Discapacidad
		}	
		$("#cedMama").hide();
		$("#cedMama").parent("td").prev().html('');
	}
	// cod 37 HERMANO	
	if(p=="37"){
	
	 if(edad>12&&edad<=40&&$("#tipoAfiliacion3").val()=='48'){
		   $("#filaEscolaridad,#filaUniversidad").show();//Mostrar certificado de estudios
		}
      if($("#capacidad").val()=='D'){
		$("#filaDiscapacidad").show();//Certificado Discapacidad
		}
		$("#cedMama").hide();
		$("#cedMama").parent("td").prev().html('');
	}
		
});//end certificados		
			
/*
$('#identificacion2').blur(function(){
	var campo0=$(this).val();
	$.getJSON('../../phpComunes/buscarPersona.php',{v0:campo0},function(data){
		if(data != 0){
		$.each(data, function(i, fila){
		idPersona=fila.idpersona;
		$('#pNombre2').val(fila.pnombre);
		$('#sNombre2').val(fila.snombre);
		$('#pApellido2').val(fila.papellido);
		$('#sApellido2').val(fila.sapellido);
		$('#direccion2').val(fila.direccion);
		$('#telefono2').val(fila.telefono);
		$('#fecNac2').val(fila.fechanacimiento);
		});
		$.getJSON('../../phpComunes/buscarAfiliacion.php',{v0:idPersona},function(data){
			if(data != 0){
				$.each(data, function(i, fila){
				$('#nitConyuge').val(fila.nit);
				$('#empCony').val(fila.razonsocial);
				$('#salCony').val(fila.salario);
				$('#subCony').val(fila.detalledefinicion);
			});
			}
		});
		}
	});
	});		 			
*/
});//FIN READY--------


//...GRUPO FAMILIAR ...//
function conyugeDialog(opc){
	  
	  $("#conyuge").dialog("destroy");
	  $("#conyuge").dialog({
			                        
									width:720,
									modal: true,
									data:{opc:opc},
									buttons: {
				                              'Grabar': function() {
					                                          verificarC(opc);
				                                              }		  
				                                            },
									open:function(){
										if(opc==2){
											
											var idt=$("#cedula").val();//cedula del trabajdor
											
											//traer datos del conyuge
											/*$.post(URL+'',{v0:idt},function(data){ 
											 $("#cedula2").val(data.identificacion);?
											 $("tipIdentificacion").val(data.tipodocumento);
											 
											 ... etc
											 certificados
											 if(data.certificado1=='S'){
											 	$("#cFotocopia").attr("checked",true);//si lo tiene lo checkea
												
											 }
											 
											  if(data.certificado2=='S'){
											 	$("#cMatrimonio").attr("checked",true);//si lo tiene lo checkea
												
											 }
											 });*/
											
											//mientras tanto
											$("#identificacion2").val('123');
											$("#cFotocopia").attr("checked",true);//si lo tiene de la BD la focopia lo checkea
											////////fin minetrs tanto/////////////
											
											
											}
										},						
												close:function(){
													  $("#conyuge ul").remove();
												      $("#conyuge table.tablero input:text").val('');
													  $("#conyuge table.tablero select").val('Seleccione');
																							 
												}//end boton
										        });
}//end function

//dialog BENEFICIARIOS

function beneficiarioDialog(op){
	
	$("#beneficiarios").dialog("destroy");
	$("#beneficiarios").dialog({
			                        width:640,
									modal: true,
									data:{op:op},
									buttons: {
				                              'Grabar': function() {
					                                          verificarB(op);
				                                              }		  
				                                            },
															
											open:function(){
										if(op==2){
											
											var idt=$("#cedula").val();
											
											//traer datos del conyuge
											/*$.post(URL+'',{v0:idt},function(data){ 
											 $("#cedula2").val(data.identificacion);?
											 $("tipIdentificacion").val(data.tipodocumento);
											 ... etc
											 
											//certificados beneficiarios
											if(data.cuniversidad=='S'){
                                           $("#cUniversidad").attr("checked",true);												
												}
											if(data.fescolaridad=='S'){
												 $("#cEscolaridad").attr("checked",true);		
												}
												
											if(data.fdiscapacidad=='S'){
												 $("#cdiscapacidad").attr("checked",true);		
												}
											if(data.supervivencia=='S'){
												 $("#csupervivencia").attr("checked",true);		
												}			
											 
										
											 });*/
											 
											//mientras tanto solo pongo la cedula del beneficiario....y un certificado si lo tiene
											$("#identificacion3").val('9999');
											$("#cEscolaridad").attr("checked",true);
											
											}
										},					
												close:function(){
													  $("#beneficiarios ul").remove();
													  $("#beneficiarios  table.tablero input:text").val('');
													  $("#beneficiarios  table.tablero select").val('Seleccione');
													  $("#beneficiarios input:checkbox").attr("checked",false);
													  // TABLA DINAMICA PARA DATOS EMPRESA TRABAJDOR ACTIVO
			                                          $("table[name='dinamicTable']").remove();
													  $("#filaEscolaridad,#filaUniversidad,#filaSupervivencia,#filaDiscapacidad").hide();
													}//end boton
										                   
												
												});
}


// ERORRES CONYUGE
        
		//accion=1 significa NUEVO y accion=2 significa ACTUALIZACION
		 function verificarC(accion){
          //accion recibe parametros de opcion 1=insertar; 2=actualizar
               $("#conyuge ul").remove();
		       lista="<ul class='Rojo'>";
			     var tipRel=$("#tipRel").val();
			   var conviven=$("#conviven").val();
			   var tipoDoc2=$("#tipoDoc2").val();
			   var optionSel=$("#tipoDoc2").children("option:selected").html();//valor del texto del option
			   var cedula2=$("#identificacion2").val();
			   var pNombre2=$("#pNombre2").val();
			   var pApellido2=$("#pApellido2").val();
			   var sNombre2=$("#sNombre2").val();//no obligado
			   var sApellido2=$("#sApellido2").val();//no obligado
			   var sexo2=$("#sexo2").val();
			   var barrio2=$("#barrio2").val();
			   var direccion2=$("#direccion2").val();
			   var telefono2=$("#telefono2").val();
			   var celular2=$("#celular2").val();
			   var email2=/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test($("#email2").val());
			   var tipVivienda2=$("#tipoVivienda2").val();
			   var deptRes2=$("#deptRes2").val();
			   var ciudadRes2=$("#ciudadRes2").val();
			   var zonaRes2=$("#zonaRes2").val();
			   var estadoCivil2=$("#estadoCivil2").val();
			   var depNac2=$("#depNac2").val();//no obligado
			   var ciudadNac2=$("#ciudadNac2").val();//no obligado
			   var capTrabajo2=$("#capTrabajo2").val();//no obligado
			   var profesion2=$("#profesion2").val();//no obligado
			   var fecNac2=$("#fecNac2").val();
                $("#cMatrimonio").attr("value",'N');
				$("#cFotocopia").attr("value",'N');
               
			if(tipRel=='0'){
					lista+='<li>Seleccione un tipo de relacion.</li>';
					
				}
			
			//------------------------CONVIVEN----------------------
			if(conviven=='0'){
					lista+='<li>Seleccione el tipo de convivencia.</li>';
					
				}
			
					
			//---------------------TIPO DOC----------------------  
			   if(tipoDoc2=='0'){
					lista+='<li>Seleccione un tipo de documento en CONYUGE.</li>';
					
				}
		    //--------------------CEDULA-----------------------   
			   if(cedula2==''){
					lista+='<li>Ingrese la identificaci\u00F3n del CONYUGE.</li>';
						
				} else {
                         if(cedula2.length<3){
					     lista+='<li>La identificaci\u00F3n NO debe tener 2 d\u00EDgitos.</li>';
						 
				         }else {
							 if(cedula2.length==9){
					           lista+='<li>La identificaci\u00F3n NO debe tener 9 d\u00EDgitos.</li>';
							  
				              }
							   
						 }//end else
				  }//end else
		    //----------------PRIMER NOMBRE Y APELLIDO---------------------------  
			   if(pNombre2==''){
					lista+='<li>Ingrese el NOMBRE del CONYUGE.</li>';
						
				}
				
				 if(pApellido2==''){
					lista+='<li>Ingrese el APELLIDO del CONYUGE.</li>';
						
				}
			
			
			//-------------------SEXO-----------------------
			if(sexo2=='0'){
					lista+='<li>Seleccione el sexo del CONYUGE.</li>';
					
				}
				
			//-------------------TELEFONO----------------------
			    if(telefono2!=''){
					 if(telefono2.length<7){
						 lista+='<li>Faltan d\u00EDgitos en el  TELEFONO.</li>';
						
					 }
				}
			//---------------------EMAIL------------------------
			
			if($("#email2").val()!=''){
			
			    if(!email2) {
							lista+='<li>El E-mail aparentemente es incorrecto.</li>';	
							
									
							}else{
								  email2=$("#email2").val();
								  }
			}
			//------------------------------------------------------
			
			/*if(deptRes2=='Seleccione..'){lista+='<li>Seleccione el Departamento de residencia en CONYUGE.</li>';}
			if(ciudadRes2=='Seleccione..'){lista+='<li>Seleccione la Ciudad de residencia en CONYUGE.</li>';}
			if(zonaRes2=='Seleccione..'){lista+='<li>Seleccione la zona de residencia en CONYUGE.</li>';}*/
				
			
			//----------------------FECHA DE NACIMIENTO---------------
			if(fecNac2==''||fecNac2=='mmddaaaa'){
				lista+='<li>Escriba la fecha de nacimiento del CONYUGE.</li>';
				
			}
			
			//---------------DOCUMENTOS A SOLICITAR
				
			if($("#cFotocopia").attr("checked")==false){
				lista+='<li>Seleccione la fotocopia de la c\u00E9dula .</li>';
			}else{
				$("#cFotocopia").attr("value",'S');
				}
				
			//no obligado	
			if($("#cMatrimonio").attr("checked")==false){
				$("#cMatrimonio").attr("value",'N');
			}else{
				$("#cMatrimonio").attr("value",'S');
				}	
			
			
			   if(accion==1){
			   $("#cFotocopia,#cMatrimonio").attr("checked",false);
			   
      		//..MOSTRAR VALIDACIONES..//
			   $(lista+"</ul>").prependTo($("#conyuge"));
			   $("#conyuge ul.Rojo").hide().fadeIn("slow");
			   
			      if($("#conyuge ul.Rojo").is(":empty")){
					 
				    //limpiarCampos();
					anioActual=new Date();
					hoy=anioActual.getFullYear();
					var edadC=parseInt(hoy)- parseInt(fecNac2.slice(-4));

					  
//cargar los datos en la tabla conyuge
if(cedula2==$("#tablaC td:eq(1)").text()){

alert("El Conyuge con c\u00E9dula '"+cedula2+"' ya esta agregado");
	
}else{
	
$("#tablaC").append("<tr><td>"+optionSel+"</td><td>"+cedula2+"</td><td>"+pNombre2.toUpperCase()+" "+sNombre2.toUpperCase()+" "+ 
                                  pApellido2.toUpperCase()+" "+sApellido2.toUpperCase()+"</td><td>"+
				                  fecNac2+"</td><td>"+edadC+"</td>"+
							      "<td align='center'><img src='../../imagenes/menu/ico_error.png' width='16' height='16' alt='Eliminar' /></td></tr>");
					  
                                     //aqui se agrega la cedula de la conyuge en el fomrulario de NUEVO BENEFICIARIO
                                     $("#beneficiarios table.tablero td #cedMama").append("<option value='"+cedula2+"'>"+cedula2+"</option>");
		//LLama la funcion que genera el array	con valores de conyuge		  
		generarArrayC(0,tipRel,conviven,tipoDoc2,cedula2,pNombre2,sNombre2,pApellido2,sApellido2,sexo2,direccion2,barrio2,telefono2,celular2,email2,tipVivienda2,deptRes2,ciudadRes2,                                zonaRes2,estadoCivil2,$("#fecNacHide2").attr("value"),depNac2,ciudadNac2,capTrabajo2,profesion2,$("#cMatrimonio").attr("value"),$("#cFotocopia").attr("value"),"insertar");
		
}//end else
	
			
}
 }//end if accion=1
   //accion es 2 ACTUALIZAR
	
	if(accion==2){
		   
		//..MOSTRAR VALIDACIONES..//
			   $(lista+"</ul>").prependTo($("#conyuge"));
			   $("#conyuge ul.Rojo").hide().fadeIn("slow");
			   
			      if($("#conyuge ul.Rojo").is(":empty")){
					 
				    //limpiarCampos();
					anioActual=new Date();
					hoy=anioActual.getFullYear();
					var edadC=parseInt(hoy)- parseInt(fecNac2.slice(-4));

					  
//reemplazar los datos en la tabla conyuge
if(cedula2==$("#tablaCExiste td:eq(1)").text()){

$("#tablaCExiste td:contains('"+cedula2+"')").parents("tr").remove();

$("#tablaCExiste").append("<tr><td>"+optionSel+"</td><td>"+cedula2+"</td><td>"+pNombre2.toUpperCase()+" "+sNombre2.toUpperCase()+" "+ 
                                                             pApellido2.toUpperCase()+" "+sApellido2.toUpperCase()+"</td><td>"+fecNac2+"</td><td>"+edadC+"</td><td>"+
				                            "<img src='../../imagenes/menu/grabar.png' width='16' height='16' alt='Modificar'  onclick='conyugeDialog(2)' style='cursor:pointer;'/></td></tr>");
					  
                 //aqui se agrega la cedula de la conyuge en el fomrulario de NUEVO BENEFICIARIO antes borrar el anterior
                       $("#beneficiarios table.tablero td #cedMama option[value='"+cedula2+"']").remove();
				       $("#beneficiarios table.tablero td #cedMama").append("<option value='"+cedula2+"'>"+cedula2+"</option>");
		
		    
						 //Borrar elemento del Array conyuge para modificar por el actual
				  for(i=0;i<arrayFinalC.length;++i){
					     if(cedula2==arrayFinalC[i][4]){
							  arrayFinalC.splice(i,1);
						 }
					}//end for 
				
	//LLama la funcion que genera el array	con valores de conyuge		  
	generarArrayC(0,tipRel,conviven,tipoDoc2,cedula2,pNombre2,sNombre2,pApellido2,sApellido2,sexo2,direccion2,barrio2,telefono2,celular2,email2,tipVivienda2,deptRes2,ciudadRes2,                            zonaRes2,estadoCivil2,$("#fecNacHide2").attr("value"),depNac2,ciudadNac2,capTrabajo2,profesion2,$("#cMatrimonio").attr("value"),$("#cFotocopia").attr("value"),"modificar");
    }
}

}//end accion 2 actualizar

//ELIMINAR FILA DE LA TABLA CONYUGE NUEVO
$("#tablaC td:contains('"+cedula2+"')~td img[alt='Eliminar'],#tablaCExiste td:contains('"+cedula2+"')~td img[alt='Eliminar']").click(function(){
  		  if(confirm("Â¿Est\u00E1 seguro de ELIMINAR este CONYUGE?")){
				  $(this).parents("tr").find("td:contains('"+cedula2+"')").parent("tr").remove();
				  $("#tablaB").find("td:contains('"+cedula2+"')").parent("tr").remove();
				   $("#tablaBExiste").find("td:contains('"+cedula2+"')").parent("tr").remove();
		              $("#beneficiarios table.tablero td #cedMama option:contains('"+cedula2+"')").remove();
				           
						 //Borrar elemento del Array conyuge
				  for(i=0;i<arrayFinalC.length;++i){
					     if(cedula2==arrayFinalC[i][4]){
							  arrayFinalC.splice(i,1);
						 }
					}//end for 
				
				  //Borrar elemento del Array Beneficiarios
				 for(i=0;i<arrayFinalB.length;++i){
				    if(cedula2==arrayFinalB[i][10]){//[2] pero es  [10] por q compara la relacion y no la cedula del beneficiario
					              arrayFinalB.splice(i,1);
								  arrayFinalCert.splice(i,1);//eliminar de una vez el certificado de ese beneficiario
				     }
				}//end for  
		 }//end confirm
							
});//end click eliminar
		 
		 
	 }//end funciton veriicar CONYUGE
	


/* ERRORES BENEFICIARIO*/
		 
function verificarB(accion){		  
$("#beneficiarios ul").remove();
$("table[name='dinamicTable']").remove();
lista="<ul class='Rojo'>";

var tipoDoc3=$("#tipoDoc3").val();
var option2Sel=$("#tipoDoc3").children("option:selected").html();//valor del texto del option TIPO DOC
var option3Sel=$("#parentesco").children("option:selected").html();//valor del texto del option PARENTESCO
var cedula3=$("#identificacion3").val();
var pNombre3=$("#pNombre3").val();
var pApellido3=$("#pApellido3").val();
var sNombre3=$("#sNombre3").val();//no obligado
var sApellido3=$("#sApellido3").val();//no obligado
var sexo3=$("#sexo3").val();
var fecNac3=$("#fecNac3").val();
var parentesco=$("#parentesco").val();
var cedMama=$("#cedMama").val();//no obligado
var tipoAfiliacion3=$('#tipoAfiliacion3').val();
var optionTipoAf=$('#tipoAfiliacion3').children("option:selected").html();//VALOR DEL OPTION DE TIPO AFILIACION	
var capacidad=$('#capacidad').val();
var depNac3=$('#depNac3').val();
var ciudadNac3=$("#ciudadNac3").val();
var estadoCivil3=$("#estadoCivil3").val();

var fEscolaridad=$("#fEscolaridad").val();
var fUniversidad=$("#fUniversidad").val();
var fDiscapacidad=$("#fDiscapacidad").val();
var fSupervivencia=$("#fSupervivencia").val();	
		
//VALORES DE CERTIFICADO
 $("#cEscolaridad,#cUniversidad,#cSupervivencia,#cDiscapacidad").attr("value","N");
	
		  //---------------------TIPO DOC----------------------  
			   if(tipoDoc3=='Seleccione..'){
					lista+='<li>Seleccione un tipo de documento en BENEFICIARIO.</li>';
					}
		    //--------------------CEDULA-----------------------   
			   if(cedula3==''){
					lista+='<li>Ingrese la identificaci\u00F3n del BENEFICIARIO.</li>';
						
				} else {
                         if(cedula3.length<3){
					     lista+='<li>La identificaci\u00F3n NO debe tener 2 d\u00EDgitos.</li>';
						 
				         }else {
							 if(cedula3.length==9){
					           lista+='<li>La identificaci\u00F3n NO debe tener 9 d\u00EDgitos.</li>';
							    }
							   
						 }//end else
				  }//end else
		    //----------------PRIMER NOMBRE Y APELLIDO---------------------------  
			   if(pNombre3==''){
					lista+='<li>Ingrese el NOMBRE del BENEFICIARIO.</li>';
						
				}
				
				 if(pApellido3==''){
					lista+='<li>Ingrese el APELLIDO del BENEFICIARIO.</li>';
					
				}
			
			
			//-------------------SEXO-----------------------
			if(sexo3=='0'){
					lista+='<li>Seleccione el sexo del BENEFICIARIO.</li>';
					
				}
			
			//----------------------FECHA DE NACIMIENTO 3---------------
			if(fecNac3==''||fecNac3=='mmddaaaa'){
				lista+='<li>Escriba la fecha de nacimiento del BENEFICIARIO.</li>';
				afiliacion=false;	
			}
			
			//-------------------PARENTEZCO-----------------------
			if(parentesco=='Seleccione..'){
					lista+='<li>Seleccione el parentesco.</li>';
					afiliacion=false;	
				}
			
			//-------------------TIPO AFILIACION-----------------------
			if(tipoAfiliacion3=='0'){
					lista+='<li>Seleccione tipo de afiliacion.</li>';
					afiliacion=false;	
				}
			//-------------------CAPACIDAD--------------------
			if(capacidad=='0'){
					lista+='<li>Seleccione capacidad de trabajo.</li>';
					afiliacion=false;	
				}
			//-------------------DEPRMENTO-----------------------
			if(depNac3=='0'){
					lista+='<li>Seleccione Departamento de nacimiento.</li>';
					afiliacion=false;	
				}
		
		if(ciudadNac3==''){
			lista+='<li>Seleccione la ciudad de nacimiento.</li>';
					afiliacion=false;	
			}
		
		//-------------------ESTADO CIVIL	-----------------------
			if(estadoCivil3=='0'){
					lista+='<li>Seleccione el estado civil.</li>';
					afiliacion=false;	
				}
		
		//Validar certificados de beneficiario ,,#filaDiscapacidad
		if( $("#filaEscolaridad").is(":visible")&&$("#filaUniversidad").is(":visible")){
			
			if($("#cEscolaridad").attr("checked")==false&&$("#cUniversidad").attr("checked")==false||$("#cEscolaridad").attr("checked")==true&&$("#cUniversidad").attr("checked")==true){
				
				lista+='<li>Seleccione al menos  un certificado de estudio.</li>';
					afiliacion=false;	
				}else{
				  if($("#cEscolaridad").attr("checked")==true){
				   var temp=0;
				   }else{//AGREGADO AYER
				      $("#cEscolaridad").attr("value","N");	   
					   }
				   
				    if($("#cUniversidad").attr("checked")==true){
				   var temp=1;
				   }else{//AGREGADO AYER
			               $("#cUniversidad").attr("value","N");		   
					   }
				 }
			
			if(temp==0){
				$("#cEscolaridad").attr("value","S");
			if($("#fEscolaridad").val()==''){
					lista+='<li>Ingrese la fecha de entrega del certificado de ESCOLARIDAD.</li>';
					afiliacion=false;	
				}
			}
			
			if(temp==1){
			$("#cUniversidad").attr("value","S");
			if($("#fUniversidad").val()==''){
					lista+='<li>Ingrese la fecha de entrega del certificado de UNIVERSIDAD.</li>';
					afiliacion=false;	
				}
			}
			
			}		
		//---------------------------------------------------------------------
			if( $("#filaSupervivencia").is(":visible")){
			
			if($("#cSupervivencia").attr("checked")==false){
			
			  $("#cSupervivencia").attr("value","N");//AGREGADO AYER
				lista+='<li>Seleccione el certificado de SUPERVIVENCIA.</li>';
					afiliacion=false;	
				}else{
					$("#cSupervivencia").attr("value","S");
					}
			
			if($("#fSupervivencia").val()==''){
					lista+='<li>Ingrese la fecha de entrega del certificado de SUPERVIVENCIA.</li>';
					afiliacion=false;	
				}
			
			}							
	//----------------------------------------------------------------------------------
			if( $("#filaDiscapacidad").is(":visible")){
			
			
			if($("#cDiscapacidad").attr("checked")==false){
				
				$("#cDiscapacidad").attr("value","N");//AGREGADO AYER
				
				lista+='<li>Seleccione el certificado de DISCAPACIDAD.</li>';
					afiliacion=false;	
				}else{
					$("#cDiscapacidad").attr("value","S");
					}
			
			if($("#fDiscapacidad").val()==''){
					lista+='<li>Ingrese la fecha de entrega del certificado de DISCAPACIDAD.</li>';
					afiliacion=false;	
				}
			
			}						

		
if(accion==1){		
//...BENEFICIARIOS

				//..MOSTRAR VALIDACIONES..//
			   $(lista+"</ul>").prependTo($("#beneficiarios"));
			   $("#beneficiarios ul.Rojo").hide().fadeIn("slow");
			   
			      if($("#beneficiarios ul.Rojo").is(":empty")){
					  //alert("Datos completos..LIMIPARLOS");
					  
					anioActual=new Date();
					hoy=anioActual.getFullYear();
					var edadB=parseInt(hoy)- parseInt(fecNac3.slice(-4));

if(cedula3==$("#tablaB td:eq(1)").text()){

alert("El Beneficiario con c\u00E9dula '"+cedula3+"' ya esta agregado");
	
}else{
//cargar los datos en la tabla conyuge
$("#tablaB").append("<tr><td>"+option2Sel+"</td><td>"+cedula3+"</td><td>"+cedMama+"</td><td>"+option3Sel.toUpperCase()+"</td><td>"+pNombre3.toUpperCase()+" "+sNombre3.toUpperCase()+" "+ 
 pApellido3.toUpperCase()+" "+sApellido3.toUpperCase()+"</td><td>"+fecNac3+"</td><td>"+optionTipoAf+"</td><td>"+capacidad+"</td><td>"+edadB+"</td><td align='center'><img src='../../imagenes/menu/ico_error.png' width='16' height='16' alt='Eliminar' /></td></tr>");
					  
generarArrayB(1,tipoDoc3,cedula3,pNombre3,pApellido3,sNombre3,sApellido3,sexo3,$("#fecNacHide3").attr("value"),parentesco,cedMama,tipoAfiliacion3,capacidad,depNac3,ciudadNac3,estadoCivil3,"insertar");

arrayCertificados(cedula3,$("#cEscolaridad").attr("value"),fEscolaridad,$("#cUniversidad").attr("value"),fUniversidad,$("#cSupervivencia").attr("value"),fSupervivencia,$("#cDiscapacidad").attr("value"),fDiscapacidad,"insertar");
					  
}//end if ul beneficiarios
						
						
				  }//end else 
	
}//end accion=1
	if(accion==2){
		//..MOSTRAR VALIDACIONES..//
			   $(lista+"</ul>").prependTo($("#beneficiarios"));
			   $("#beneficiarios ul.Rojo").hide().fadeIn("slow");
			   
			      if($("#beneficiarios ul.Rojo").is(":empty")){
					  //alert("Datos completos..LIMIPARLOS");
					  
					anioActual=new Date();
					hoy=anioActual.getFullYear();
					var edadB=parseInt(hoy)- parseInt(fecNac3.slice(-4));

if(cedula3==$("#tablaBExiste td:eq(1)").text()){

//cargar los datos en la tabla beneficiarios
$("#tablaBExiste td:contains('"+cedula3+"')").parents("tr").remove();
$("#tablaBExiste").append("<tr><td>"+option2Sel+"</td><td>"+cedula3+"</td><td>"+cedMama+"</td><td>"+option3Sel.toUpperCase()+"</td><td>"+pNombre3.toUpperCase()+" "+sNombre3.toUpperCase()+" "+ 
 pApellido3.toUpperCase()+" "+sApellido3.toUpperCase()+"</td><td>"+fecNac3+"</td><td>"+optionTipoAf+"</td><td>"+capacidad+"</td><td>"+edadB+"</td><td align='center'><img src='../../imagenes/menu/grabar.png' width='16' height='16' alt='Modificar'  onclick='beneficiarioDialog(2)' style='cursor:pointer;'/></td></tr>");
				
 //Borrar elemento del Array Beneficiarios anterior para agregar el actual modificado
				  for(i=0;i<arrayFinalB.length;++i){
					    if(cedula3==arrayFinalB[i][2]){
						   arrayFinalB.splice(i,1);
						   arrayFinalCert.splice(i,1);//eliminar de una vez el certificado de ese beneficiario
						}
					    }//end for
					  
generarArrayB(1,tipoDoc3,cedula3,pNombre3,pApellido3,sNombre3,sApellido3,sexo3,$("#fecNacHide3").attr("value"),parentesco,cedMama,tipoAfiliacion3,capacidad,depNac3,ciudadNac3,estadoCivil3,"modificar");

arrayCertificados(cedula3,$("#cEscolaridad").attr("value"),fEscolaridad,$("#cUniversidad").attr("value"),fUniversidad,$("#cSupervivencia").attr("value"),fSupervivencia,$("#cDiscapacidad").attr("value"),fDiscapacidad,"modificar");


	
}
						
 }//end else 
		
		
		}//end accion2

//ELIMINAR FILA DE LA TABLA BENEFICIARIO
$("#tablaB td:contains('"+cedula3+"')~td img[alt='Eliminar']").click(function(){
  		  if(confirm("Â¿Est\u00E1 seguro de ELIMINAR este BENEFICIARIO?")){
				  $(this).parents("tr").find("td:contains('"+cedula3+"')").parent("tr").remove();
				  
				  			
				 //Borrar elemento del Array Beneficiarios
				  for(i=0;i<arrayFinalB.length;++i){
					    if(cedula3==arrayFinalB[i][2]){
						   arrayFinalB.splice(i,1);
						   arrayFinalCert.splice(i,1);//eliminar de una vez el certificado de ese beneficiario
						}
					    }//end for
						
			
				 
				  }//end confirm
	 			  
});//end click eliminar

							
		  }//FIn function beneficiarios



//---FUNCION PARA GENERAR ARRAY DE CONYUGES 
var arrayFinalC=[];
var arrayFinalB=[];
var arrayFinalCert=[];
var arrayActual=[];
function generarArrayC(tabla,tipRel,conviven,tipDoc,cedula,pNombre,pApellido,sNombre,sApellido,sexo,direccion,barrio,telefono,celular,email,tipVivienda,deptRes,ciudadRes,zonaRes,estadoCivil,fecNac,deptNac,ciudadNac,capTrabajo,profesion,registroMatrimonio,fotocopiaCed,accion){	
	$("#conyuge").dialog('close');
	//ARRAY  de valores de la fila  
	arrayActual=[tabla,tipRel,conviven,tipDoc,cedula,pNombre,pApellido,sNombre,sApellido,sexo,direccion,barrio,telefono,celular,email,tipVivienda,deptRes,ciudadRes,zonaRes,estadoCivil,fecNac,deptNac,ciudadNac,capTrabajo,profesion,registroMatrimonio,fotocopiaCed,accion];
	arrayFinalC.push(arrayActual);
	}//end funcion


//---FUNCION PARA GENERAR ARRAY  BENEFICIARIOS
function generarArrayB(tabla,tipDoc,cedula,pNombre,pApellido,sNombre,sApellido,sexo,fecNac,parentesco,cedMama,tipoAfiliacion,capacidad,depNace,ciudadNace,estadoCivil,accion){
	
	$("#beneficiarios").dialog('close');
	//ARRAY  de valores de la fila
	        
	arrayActual=[tabla,tipDoc,cedula,pNombre,pApellido,sNombre,sApellido,sexo,fecNac,parentesco,cedMama,tipoAfiliacion,capacidad,depNace,ciudadNace,estadoCivil,accion];
	
	arrayFinalB.push(arrayActual);
		
}//end funcion
//---Funcion para generar array de certificados
function arrayCertificados(idBeneficiario,cEscolaridad,fEscolaridad,cUniversidad,fUniversidad,cSupervivencia,fSupervivencia,cDiscapacidad,fDiscapacidad,accion){
	
	arrayActual=[idBeneficiario,cEscolaridad,fEscolaridad,cUniversidad,fUniversidad,cSupervivencia,fSupervivencia,cDiscapacidad,fDiscapacidad,accion];
	arrayFinalCert.push(arrayActual);
	
	}
	
		  
//...ABRIR DIALOG DOCUMENTOS...//

//...ABRIR DOCUMENTOS...
function imagen(imgAlt){
	
   //Capturo el objeto como un ojeto jquery	
	var img=jQuery(imgAlt);
	//Borror en el div #imagen alguna imagen
	$("#imagen").find("img").remove();
	$("#imagen").dialog('open');
	
	var srcImage= img.attr("alt");
	$("#imagen").append("<img src='"+srcImage+"'");
}//end function imagen	


//... ABRIR VENTANA DIALOG PARA DIRECCIONES...//

function direccion(obj){
	     //$("#dialog-form").dialog("destroy"); //Destruir dialog para liberar recursos
		 $("#dialog-form").dialog({
			         height: 450,
			         width: 650,
			        modal: true,
			        open: function(){
					$('#dialog-form').html('');
					$.get('../../phpComunes/direcciones.html',function(data){
							$('#dialog-form').html(data);
					});
				       $('#tDirCompleta').val('');
				       $('#dialog-form select,#dialog-form input[type=text]').val('');
			           $('#uno').focus();
			},
			  buttons: {
			  
				                'Crear direcci\u00F3n': function() {
					            $("#dialog-form").dialog("destroy");
		                        $("#dialog-form").dialog('close');
							    obj.value=$('#tDirCompleta').val();
								$('#tDirCompleta').val('');
					            $('#dialog-form select,#dialog-form input[type=text]').val('');
					            
				}
			},
			close: function() {
				$("#dialog-form").dialog("destroy");
		        $("#dialog-form").dialog('close');
				$('#tDirCompleta').val('');
				$('#dialog-form select,#dialog-form input[type=text]').val('');
			}
	});
}//end function
//-- FIN DIRECCIONES



var nuevo=0;
var modificar=0;
var continuar=true;
var msg="";



$(function() {
		$("#ayuda").dialog({
		 	autoOpen: false,
			height: 450, 
			width: 700, 
			draggable:true,
			modal:false,
			open: function(evt, ui){
					$('#ayuda').html('');
					$.get('../../help/aportes/ayudaActualizacionTrabajador.html',function(data){
							$('#ayuda').html(data);
					})
			 }
		});
	});

$(function() {
	$("#dialog-form2").dialog("destroy");
	$("#dialog-form2").dialog({
		autoOpen: false,
		height: 400,
		width: 500,
		modal: true,
		buttons: {
			'Enviar': function() {
			var bValid = true;
			var campo=$('#notas').val();
			var campo0=$.trim(campo);
			if (campo0==""){
				$(this).dialog('close');
					return false;
				}
			var campo1=$('#usuario').val();
			var campo2="trabajadorNuevo.php";
			$.post('../../phpComunes/colaboracion.php',{v0:campo0,v1:campo1,v2:campo2}, 								
			function(datos){
				if(datos=='1'){
					alert("Su comentario fue enviado correctamente!. Gracias por participar en el mejoramiento de SIGAS");
				}else{
					alert(datos);
				}
		});
		$(this).dialog('close');
		},
		Cancelar: function() {
			$(this).dialog('close');
			$("#dialog-form2").dialog("destroy");
		}
	}		
	});
	/*
	$('#enviar-notas')
		.button()
		.click(function() {
		$('#dialogo-dir2').dialog('open');
	});
	*/
});





$(function(){
$("#deptRes2").change(function () {
   		$("#deptRes2 option:selected").each(function () {
				//alert($(this).val());
				elegido=$(this).val();
				$.post("../../phpComunes/ciudades.php", {elegido: elegido}, function(data){
				$("#ciudadRes2").html(data);
				$("#zonaRes2").html("");
				$("#ciudadRes2").trigger('change');
			});			
        });
   });

// Parametros para el combo2
$("#ciudadRes2").change(function () {
   		$("#ciudadRes2 option:selected").each(function () {
			//alert($(this).val());
				elegido=$(this).val();
				$.post("../../phpComunes/zonas.php", {elegido: elegido}, function(data){
				$("#zonaRes2").html(data);
			});			
        });
   });
   })
 
 $(function(){
$("#depNac2").change(function () {
   		$("#depNac2 option:selected").each(function () {
				//alert($(this).val());
				elegido=$(this).val();
				$.post("../../phpComunes/ciudades.php", {elegido: elegido}, function(data){
				$("#ciudadNac2").html(data);
			});			
        });
   });
 })

$(function(){
$("#depNac3").change(function () {
    $("#depNac3 option:selected").each(function () {
    elegido=$(this).val();
    $.post("../../phpComunes/ciudades.php", {elegido: elegido}, function(data){
    $("#ciudadNac3").html(data);
    });
    });
   });
 })

function calcularCategoria(sal){
    var salario=parseInt(sal);
    if(isNaN(salario)) return;
    var sm=parseInt(salario/ 515000);
    if(sm<2){
        $('#categoria').val('A');
    }

    if(sm>=2 && sm<4){
        $('#categoria').val('B');
    }
    if(sm>=4){
        $('#categoria').val('C');
    }

}

function guardarAfiliacion(){
var tipForm=        $("#tipForm").val();
var tipAfiliacion=  $("#tipAfiliacion").val();
var idEmpresa=      idempresa;
var idPersona=      idnp;
var fecIngreso=     $("#fecIngreso").val();
var horasDia=       $("#horasDia").val();
var horasMes=       $("#horasMes").val();
var salario=        $("#salario").val();
var agricola=       $("#agricola").val();
var cargo=          $("#cargo").val();
var categoria=      $("#categoria").val();
var traslado=       $("#traslado").val();
var codCaja=        $("#codCaja").val();
var tipoPago=       $("#tipoPago").val();

//tipoformulario,tipoafiliacion,idempresa, idPersona,fechaingreso,horasdia,horasmes,salario
//,agricola, cargo,categoria,traslado,codigocaja,tipopago

$.getJSON('guardarAfiliacion.php',{v0:tipForm,v1:tipAfiliacion,v2:idEmpresa,v3:idPersona,v4:fecIngreso,v5:horasDia,
    v6:horasMes,v7:salario,v8:agricola,v9:cargo,v10:categoria,v11:traslado,v12:codCaja,v13:tipoPago},function(datos){
    if(datos==0){
        alert("NO se pudo guardar la Afiliaci\u00F3n!");
        // borrar persona nueva  idnp
        return false;
    }
    else{
        alert("La afiliacion se guardo!");

    }
})
}

function guardarBeneficiario(arrayA){
    for(i=0;i < arrayA.length; i++){
        var datos=arrayA[i];
        var cadena = datos.toString();
        var campo0=datos[0];
        var campo1=datos[1];
        var campo2=datos[2];
        var campo3=datos[3];        //pn
        var campo4=datos[4];        //pa
        var campo5=datos[5];        //sn
        var campo6=datos[6];        //pa
        var campo7=datos[7];
        var campo8=datos[8];
        var campo9=datos[9];
        var campo10=datos[10];
        var campo11=datos[11];
        var campo12=datos[12];
        var campo13=datos[13];
        var campo14=datos[14];
        var campo15=datos[15];
        var campo="";
//  1	  2	 3	4	   5	    6	    7	  8	   9	     10     11  	   12		13	14 	15
//tipDoc,cedula,pNombre,pApellido,sNombre,sApellido,sexo,fecNac,parentesco,cedMama,tipoAfiliacion,capacidad,depNace,ciudadNace,estadoCivil

        if(existe==0){
            //noexiste guardar persona
            $.get('guardarPersonaCompleta2.php',{v0:campo0,v1:campo1,v2:campo2,v3:campo4,v4:campo6,v5:campo3,v6:campo5,v7:campo7,v8:campo,v9:campo,v10:campo,v11:campo,v12:campo,v13:campo,v14:campo,v15:campo,v16:campo,v17:campo15,v18:campo8,v19:campo14,v20:campo13,v21:campo12,v22:campo,v23:campo},function(data){
                if(data==0){
                    alert("NO se pudo guardar el beneficiario, revise los datos!");
                }
                else{
                    alert("Se guard\u00F3 los datos del beneficiario");
                    idnb=data;
                    guardarRelacion(campo9,campo10,campo11);
                }
            })
        }
        else{
            //existe actualizar persona
            $.get(URL+'phpComunes/actualizarPersona.php',{v0:idnb,v1:campo1,v2:campo2,v3:campo3,v4:campo4,v5:campo5,v6:campo6,v7:campo7,v8:campo15,v9:campo8,v10:campo14,v11:campo13,v12:campo12},function(data){
                if(data==0){
                    alert("No se pudo actualizar los datos del beneficiario!")
                }
            })
            guardarRelacion();
        }
    }
}

function guardarRelacion(paren,ccmm,afi){
var campo0 = idPersona;
var campo1 = idnb;
var campo2 = paren;
var campo3 = ccmm;
var campo4 = afi;

//idtrabajador idbeneficiario idparentesco idconyuge giro

$.getJSON(URL+'phpComunes/guardarBeneficiario.php',{v0:campo0,v1:campo1,v2:campo2,v3:campo3,v4:campo4},function(data){
    if(data==0)
        alert("La relaci\u00F3n beneficiario NO se guard\u00F3!, intente crearla por actulizar datos");
    else
        alert("La relaci\u00F3n beneficiario se guard\u00F3!, bajo el n\u00FAmero: "+data);
	//andres pintar tabla con los datos del nuevo beneficiario
    })
}

function buscarPersona(num){
if($("#tipoDoc2").val()==0){
    alert("Falta tipo documento");
    return false;
}
var idtd=$("#tipoDoc2").val();
$.getJSON(URL+'phpComunes/buscarPersona2.php',{v0:idtd,v1:num},function(data){
    if(data==0){
        existe=0;
	return false;
}
existe=1;
$.each(data,function(i,fila){
    idnb=fila.idpersona;
    $('#pNombre2').val(fila.pnombre);
    $('#sNombre2').val(fila.snombre);
    $('#pApellido2').val(fila.papellido);
    $('#sApellido2').val(fila.sapellido);
    $("#direccion2").val(fila.direccion);
    $("#barrio2").val(fila.idbarrio);
    $("#telefono2").val(fila.telefono);
    $("#celular2").val(fila.celular);
    $("#email2").val(fila.email);
    $("#tipoVivienda2").val(fila.idtipovivienda);
    $("#depRes2").val(fila.iddepresidencia);
    $("#ciudadRes2").val(fila.idciuresidencia);
    $("#zonaRes2").val(fila.idzona);
    $("#estadoCivil2").val(fila.idestadocivil);
    $("#depNac2").val(fila.iddepnace);
    $("#ciudadNac").val(fila.idciunace);
    $("#capTrabajo2").val(fila.capacidadtrabajo);
    $("#profesion2").val(fila.idprofesion);
    
    var fecha=fila.fechanacimiento;
    fecha=fecha.replace("/","");
    $('#fecNac2').val(fecha.replace("/",""));
    $('#fecNac2').click().blur();
    $('#sexo2').val(fila.sexo);
}); //each

// TABLA DINAMICA PARA DATOS EMPRESA TRABAJDOR ACTIVO
$("table[name='dinamicTable']").remove();
var tabla="<table class='tablero' width='100%' border='0' cellspacing='0' cellpadding='0'    name='dinamicTable' style='margin-first:8px;'>";
//----
$.getJSON(URL+'phpComunes/buscarAfiliacion.php',{v0:idnb},function(data){
    if(data != 0){
    alert("La persona que trata de Afiliar es un trabajador activo!");
    $.each(data,function(i,fila){
  
    $('#nitConyuge').val(fila.nit);
    $('#empCony').val(fila.razonsocial);
    $('#salCony').val(fila.salario);
    $('#subCony').val(fila.detalledefinicion);
		
    tabla+="<tr><th>Salario</th><th>Nit</th><th>Raz\u00F3n social</th><th>Tipo</th><th>Detalle</th></tr>"+"<tr><td>$"+ fila.salario +"</td><td>"+ fila.nit +"</td><td>"+fila.razonsocial+"</td><td>"+fila.tipoformulario+"</td><td>"+fila.detalledefinicion+"</td></tr>";
    //Agregar tabla despues de la tabla  beneficiarios
    $("#beneficiarios .tablero").after(tabla+="</table>");
    $("table[name='dinamicTable']").hide().fadeIn(800);
    });//end each
    $('#beneficiarios .tablero input:text').val("");
    $('##beneficiarios .tablero select').val("0");
    return false;
}
});
})
}

function buscarPersonaDos(num){
if($("#tipoDoc3").val()==0){
    alert("Falta tipo documento");
    return false;
}
var campo1=parseInt(num);
if(isNaN(campo1))return false;
var idtd=$("#tipoDoc3").val();
$.getJSON(URL+'phpComunes/buscarPersona2.php',{v0:idtd,v1:num},function(data){
    if(data==0){
        existe=0;
	return false;
}
existe=1;
$.each(data,function(i,fila){
    idnb=fila.idpersona;
    $('#pNombre3').val(fila.pnombre);
    $('#sNombre3').val(fila.snombre);
    $('#pApellido3').val(fila.papellido);
    $('#sApellido3').val(fila.sapellido);
    var fecha=fila.fechanacimiento;
    fecha=fecha.replace("/","");
    $('#fecNac3').val(fecha.replace("/",""));
    $('#fecNac3').click().blur();
    $('#sexo3').val(fila.sexo);
}); //each

// TABLA DINAMICA PARA DATOS EMPRESA TRABAJDOR ACTIVO
$("table[name='dinamicTable']").remove();

var tabla="<table class='tablero' width='100%' border='0' cellspacing='0' cellpadding='0'    name='dinamicTable' style='margin-first:8px;'>";
$.getJSON(URL+'phpComunes/buscarAfiliacion.php',{v0:idnb},function(data){
    if(data != 0){
    alert("La persona que trata de Afiliar es un trabajador activo!");
    $.each(data,function(i,fila){
    tabla+="<tr><th>Salario</th><th>Nit</th><th>Raz\u00F3n social</th><th>Tipo</th><th>Detalle</th></tr>"+"<tr><td>$"+ fila.salario +"</td><td>"+ fila.nit +"</td><td>"+fila.razonsocial+"</td><td>"+fila.tipoformulario+"</td><td>"+fila.detalledefinicion+"</td></tr>";
    //Agregar tabla despues de la tabla  beneficiarios
    $("#beneficiarios .tablero").after(tabla+="</table>");
    $("table[name='dinamicTable']").hide().fadeIn(800);
    });//end each
    $('#beneficiarios .tablero input:text').val("");
    $('##beneficiarios .tablero select').val("0");
    return false;
}
/*
$.getJSON(URL+'phpComunes/buscarRelacion.php',{v0:idnb,v1:idt},function(data){
    if(data!= 0){
        alert("La persona que trata de Afiliar ya es un Beneficiario!");
        $.each(data,function(i,fila){
            var nom = fila.pnomt+" "+fila.snomt+" "+fila.papet+" "+fila.sapet;
            tabla+="<tr><th>Nombres</th><th>Parentesco</th><th>Conv</th><th>F Afilia</th><th>Relaci&oacute;n</th><th> Giro</th><th>Estado</th></tr>"+"<tr><td>"+nom+"</td><td>"+fila.detalledefinicion+"</td><td>"+fila.conviven+"</td><td>"+fila.fechaafiliacion+"</td><td>"+fila.idrelacion+"</td><td>"+fila.giro+"</td><td>"+fila.estado+"</td></tr>";
            //Agregar tabla despues de la tabla  beneficiarios
            $("#beneficiarios .tablero").after(tabla+="</table>");
            $("table[name='dinamicTable']").hide().fadeIn(800);
       });//end each

   $('#nuevoB input:text').val("");
   $('#nuevoB select').val("0");
   return false;
    }
*/
$.getJSON(URL+'phpComunes/buscarRelaciones.php',{v0:idnb},function(data){
    if(data != 0){
        alert("La persona que trata de Afiliar tiene las siguientes relaciones!");
	$.each(data,function(i,fila){
            var nom = fila.pnomt+" "+fila.snomt+" "+fila.papet+" "+fila.sapet;
            tabla+="<tr><th>Nombres</th><th>Parentesco</th><th>Conv</th><th>F Afilia</th><th>Giro</th><th>Estado</th></tr>"
            +"<tr><td>"+nom+"</td><td>"+fila.detalledefinicion+ "</td><td>"+fila.conviven+"</td><td>"+fila.fechaafiliacion+"</td><td>"+fila.giro+"</td><td>"+fila.estado+"</td></tr>";
            //Agregar tabla despues de la tabla  beneficiarios
            $("#beneficiarios .tablero").after(tabla+="</table>");
            $("table[name='dinamicTable']").hide().fadeIn(800);
     });//end each

   $('#beneficiarios .tablero input:text').val("");
   $('#beneficiarios .tablero select').val("0");
    return false;
}
}); //getJSON relaciones
//}); //getJSON relacion
}); //getJSON afiliacion
}); //getJSON


}//end funcion
 /*
Ã� 	Ã� 	\u00C1
á 	á 	\u00E1
Ã‰ 	Ã‰ 	\u00C9
Ã© 	Ã© 	\u00E9
Ã� 	Ã� 	\u00CD
Ã­ 	Ã­ 	\u00ED
\u00F3 	\u00F3 	\u00D3
\u00F3 	\u00F3 	\u00F3
Ãš 	Ãš 	\u00DA
Ãº 	Ãº 	\u00FA
Ãœ 	Ãœ      \u00DC
Ã¼ 	Ã¼ 	    \u00FC
á¹„ 	Ã‘ 	\u00D1
Ã± 	Ã± 	\u00F1


*/

function llenarCampos(){
    $('#tipoDoc').val(1);
	$("#pNombre").val('orlando');
	$("#pApellido").val('puentes');
	$("#sNombre").val();//no obligado
	$("#sApellido").val('andrade');//no obligado
	$("#sexo").val('M');
	$("#tDireccion").val('calle 46 1a-53');
	$("#barrio").val(136);
	$("#telefono").val('8757575');
	$("#celular").val();
	$("#tipoVivienda").val('U');
	$("#combo1").val('41');//$("#deptRes").val();
	$("#combo2").val('41001');//$("#ciudadRes").val();
	$("#combo3").val('41001001');//$("#zonaRes").val();
	$("#estadoCivil").val(50);
	$("#combo4").val('41');
	$("#combo5").val('41001');
	$("#capTrabajo").val('N');
	$("#profesion").val(212);
    $('#fecNac').val('01011980');
    $('#nombreCorto').val('ORLANDO PUENTES')
	$("#fecNacHide").val('06/15/1980');	//rescato elv alor de la fecha del cmapo oculto
	$("#tipForm").val(48);
	$("#fecIngreso").val();
	$("#horasDia").val(8);
	$("#horasMes").val(240);
	$("#salario").val(1000000);
	$("#agricola").val('N');
	$("#cargo").val(160);
	$("#estado").val('A');
	$("#traslado").val('N');
	$("#tipAfiliacion").val(18);
	$("#fecIngreso").val('01/01/2010');
	$("#tipoPago").val('T');
}