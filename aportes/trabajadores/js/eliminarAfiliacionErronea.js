var arrIde=new Array();
var arrID=new Array();
var idEmpresa=0;
var idPersona=0;

$(document).ready(function(){
	var smlv=salarioMinimo();
	smlv=(isNaN(smlv) && smlv>0)?616000:smlv;
	$("#txtSMLV").val(smlv);
	

var URL=src();
$("#buscarPor").change(function(){
$("#idT").val('');
	if($(this).val()=='2'){
		$("#tipoDocumento").hide();
		$("#idT").hide();
	}else{
		$("#tipoDocumento").show();
		$("#idT").show();
		$("#idT").focus();
	}
});

//Buscar Trabajador
$("#buscarT").click(function(){
	eventClick();
});//fin click
});//fin ready

var nuevo=0;
var modificar=0;
var continuar=true;
var msg="";

function buscarGrupoAfiliacion(){
	
	var v0=$("#idT").val();
	var v1=$("#tipoDocumento").val();
	var v2=$("#buscarPor").val();

	
	
	//BUSCAMOS LOS POSIBLES RADICADOS A ELIMINAR 
	$.ajax({
		url:"buscarAfiliacionError.php",
		async: false,
		data :{v0:v0,v1:v1,v2:v2},		
		beforeSend: function(objeto){
        	dialogLoading('show');
        },        
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }            
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("En buscar Afiliacion Paso lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
		success:function(data){
			$("#tabTableAfiliacion tbody tr").remove();	
			if(data){
				var cadena="";

				$.each(data,function(i,fila){
					
					cadena+="<tr id='idtabla" + fila.idempresa +"'>" +
								"<td style='text-align:right'  >" + fila.nit + "</td>" +
								"<td style='text-align:center' >" + fila.razonsocial + "</td>" +
								"<td style='text-align:center' >" + fila.fechaingreso + "</td>" +
								"<td style='text-align:center' >" + fila.tipoformulario + "</td>" +
								"<td style='text-align:center' >" + fila.tipoafiliacion + "</td>" +
								"<td style='text-align:center' >" + fila.agricola + "</td>" +
							    "<td style='text-align:center' ><img style='cursor:pointer' src='"+URL+"imagenes/menu/ico_error.png' title='Eliminar Afiliacion' " +
							    "onclick=eliminarAfiliacion(" + fila.idempresa + "," + fila.idpersona +"," + fila.nit + ",'idtabla"+ fila.idempresa +"'); /></td>"
					    	"</tr>";					
				});//each
				con=data.length;
				$("#tabTableAfiliacion tbody").append(cadena);
			} else {
				msg='';
				msg+="No hay Afiliaciones para eliminar.";
				MENSAJE(msg);
				return false;
			}			
		},//succes
		type: "POST"
	});//ajax afiliacion		
}

function eliminarAfiliacion(idempresa,idpersona,nit,idTr){
	if(confirm('Realmente desea eliminar el Registro ?'))
	{   

	$.ajax({
		url:"eliminarAfiliacion.php",
		async: false,
		data :{v0:idempresa,v1:idpersona},		
		beforeSend: function(objeto){
        	dialogLoading('show');
        },        
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }            
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("En eliminar Afiliacion Paso lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
		type: "POST",
		success:function(datos){
			if(datos==1)
				{
				$("#"+idTr).remove();	
				alert("Afiliacion anulada");
				observacion(idempresa,idpersona,nit);
				}
			else
				{
				alert("No es posible anular la afiliacion");				
				}
		}
        
	});//$.ajax({	
	}
}

function observacion(idempresa,idpersona,nit){
	var URL=src();
	$("div[name='div-observaciones-tab']").dialog("destroy");
	$("textarea[name='observacionGrupo']").val("");
	$("div[name='div-observaciones-tab']").dialog({
		  modal:true,
		  width:520,
		  resizable:false,
		  draggable:false,
		  closeOnEscape:false,
		  open:function(){
			  $(this).prev().children().hide();//Oculto la barra de titulo
			  $(this).prev().html("OBSERVACIONES...");//Pongo un nuevo titulo
			  $("textarea[name='observacionGrupo']").focus();
			  },
		  buttons:{
		    'Guardar':function(){
		    	var comentario='Afiiacion anulada';
		    	var observacioncaptura=$("textarea[name='observacionGrupo']").val();
				var observacion=comentario+' - '+observacioncaptura + ' - '+'Nit.'+ nit;
				var observacionSinEspacios = observacion.replace(/^\s+|\s+$/g,"");
				
				if(observacioncaptura.length==0){
			    	alert("Escriba las observaciones!");
					return false;
					}
					if(observacioncaptura.length<10){
						alert("Las observaciones son muy cortas...!");
						return false;
					}
				
			    if(observacionSinEspacios!=""){
				 $.ajax({
					   url:URL+"phpComunes/pdo.insert.notas.php",
					   type:"POST",
					   data:{v0:idpersona,v1:observacion, v2:1},
					   success:function(data){
					    $("[name='rtaObservacion']").fadeIn();//el mensaje esta oculto en la capa div-observacion
						$("div[name='div-observaciones-tab']").dialog("close");
					   }
			     });//ajax
				 alert("Observacion guardada"); 
				}//if observacion
				else{
					alert("No se pudo guardar la observaci\xf3n");
				}
			 }//guardar
		  }//buttons
		  });   
}