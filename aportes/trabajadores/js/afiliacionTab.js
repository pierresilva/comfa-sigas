var URL=src();
var nit="";
var rz="";
var ide=0;
var idp=0;
var estadoActualAfiliacion = null;
sinActividadEconomicaIndependiente=false;

/* Nuevo 2016- Andrés Lara
 * 
 * Buscar Empresa Cuando es Agremiado o Dependiente
 *
 * */

function buscarEmpresa(nit){
	
	//Parametros de Busqueda
	var nitEmpresa=nit;
	var tipo=1;
	
	$("#idEmpresa,#tdEmpresa").val('');
	
	$.ajax({
		url: URL+'aportes/empresas/buscarTodas.php',
		type: "GET",
		data: {v0:nitEmpresa.toUpperCase(),v1:tipo},
		async: false,
		dataType: 'json',
		success: function(data){
			if(data==''){
				$("#tdEmpresa").html("NO EXISTE");
				$("#txtNit").val('').focus();
				return false;
			}
			
			
			var idemp,nitemp,razemp;
			
			$.each(data,function(i,fila){
			
				 idemp=fila.idempresa;
				 nitemp=fila.nit;
				 razemp=fila.razonsocial;
				
								
			});
			
			
			$("#idEmpresa").val(idemp);
			
			if(idemp=='0'){
				$("#tdEmpresa").html('INVALIDO PARA AGREMIACIÓN');
			}else{
				$("#tdEmpresa").html(razemp);
			}
			
			
		}
	});//fin ajax
	
}

function onChangeTipoAfiliacion(obj){
	
	/*------------------------------------
	 *  TIPOS DE AFILIACION TAB AFILIACION
	 * 
	 *  19 		IND FACULTATIVO 2%
	 *  20 		P. VOLUNTARIO 2%
	 *  21 		P. SIS. CAJAS 0%
	 *  23 		FIDELIDAD 0.6%  2%  0.0%
	 *  4209	P. REG. ESPECIAL 0%
	 *  4520	IND AGREMIADO
	 *  4521	IND EXTRANJERO
	 *  3320	EXCENTO
	 *  2938	DESEMPLEADO
	 *  
	 -----------------------------------*/
	
	$("#cboReside,#cboPais").attr("disabled","disabled");
	$("#txtNit,#idEmpresa").val('0');
	$("#tablaAfiTab  #cboPais").val('48');
	$("#txtNit").attr("disabled","disabled");
	
	
	//Habilitar los campos pais cuando es Ind. Extranjero
	if(obj.value==4521){
		$("#cboReside,#cboPais").removeAttr("disabled");
	}
	
	 //Buscar Empresa cuando es dependiente o agremiado	
	 if(obj.value==4520 || obj.value==18){
		
		$("#txtNit").removeAttr("disabled");
		
					
			setTimeout(function(){
						
				$.ajax({
					url: URL+"aportes/empresas/buscarEmpresa1.php",
					type: "POST",
					data: {v0:$("#idEmpresa").val()},
					dataType: "json",
					success: function(empresa){
						if(empresa==0&&obj.value){
							MENSAJE("No existe la empresa.");
							return false;
						}
						else{
							$("#tdEmpresa").html(empresa[0].razonsocial);
							//Mostrar datos de la empresa agremiada
							$("#txtNit").val(empresa[0].nit);
							$("#idEmpresa").val(empresa[0].idempresa);
						}
					}
				});
				
			},1000);
			
				
	}else{
		$("#txtNit").val("0");
		$("#tdEmpresa").html("INDEPENDIENTES Y PENSIONADOS.");
		
		
	}
	
	
	//Recalcular la categoria
	$("#salario").focus().trigger("blur");
}


function buscarAfiliaciones(idp){
	estadoActualAfiliacion = null;
	$("#div-mostrar").empty();
	$.getJSON('buscarAfiliaciones.php',{idpersona:idp},function(datos){
		if(datos == 0){
			MENSAJE("No hay afiliaciones!");
			limpiarCampos_otro2();
			return false;
		}
		$.each(datos,function(i,f){
			nit=f.nit;
			idf=f.idformulario;
			rz=f.razonsocial;
			estado=f.estado;
			var span = "";
			var clase = "";
			if(estado == 'A'){
				clase='ui-state-highlight';
				span='<span style="float: left; margin-right: 0.3em;" class="ui-icon ui-icon-circle-check" id="span_afiliacion_'+ idf +'"></span>';
			}
			$("#div-mostrar").append(span+'<p align="left" onclick="buscarAfiliacion('+idf+',\''+ estado +'\')" class="'+clase+'"  id="p_afiliacion_'+ idf +'">' + nit +" "+ rz +' (Estado: '+ estado +')</p>');
		});
	});
}

function buscarAfiliacion(idf,estado){
	
	//Limpiar y set datos
	estadoActualAfiliacion = estado;	
	$("#div-msje").remove();
	$("table.tablero :input.ui-state-error,select.ui-state-error").removeClass("ui-state-error");//Limpiar la clase de error
	$("#tipAfiliacion").attr("disabled",false);
	$("#txtNit").attr("disabled","disabled");
	$("#tipAfiliacion option[value=18]").show(); // mostrar dependiente en el tipo de afiliación
	$("#tablaAfiTab").show();//con evento one
	$("#div-afiliacion").show();
	$("#idRadicacion").val("");
	/*$("#fecIngreso").datepicker("refresh");
	$("#fecIngreso").trigger("focus");
	$("body").trigger("click");*/
	
	/*INDEPENDIENTES*/
	$("#rowIndependientes:visible").hide();
	$("#select7").val("0");
	$("#codigoCIU").val("");
	
	/************************
	 * AFILIACIONES ACTIVAS *
	 ************************/
	
	
	if(estado == 'A' || estado == 'P'){
		$.ajax({
			url: 'buscarAfiliacionIda.php',
			type: "POST",
			data: {v0:idf},
			async: false,
			dataType: "json",
			success: function(datos){
				if(datos==0){
					MENSAJE("Lo lamento no encontre su registro!");
					return false;
				}
				$("#actualizarAfiliacion").show();
				$.each(datos,function(i,f){
					
					$("#tipForm").val(f.tipoformulario);
					$("#tipAfiliacion").attr("disabled",false);
					$("#tipAfiliacion").val(f.tipoafiliacion);
					
					
					/*------------------------------------
					 * 
					 *  TIPOS DE AFILIACION
					 *  18		DEPENDIENTES
					 *  19 		IND FACULTATIVO 2%
					 *  20 		P. VOLUNTARIO 2%
					 *  21 		P. SIS. CAJAS 0%
					 *  23 		FIDELIDAD 0.6%  2%  0.0%
					 *  4209	P. REG. ESPECIAL 0%
					 *  4520	IND AGREMIADO
					 *  4521	IND EXTRANJERO
					 *  3320	EXCENTO
					 *  2938	DESEMPLEADO
					 *  
					 -----------------------------------*/
					
					$("#idRadicacion").val(f.idradicacion);
					
					//#######################################DEFINIR TAB DE MODIFICACION###################################################
					
					if(f.tipoafiliacion == 19 || f.tipoafiliacion == 20 || f.tipoafiliacion == 21 || f.tipoafiliacion == 23 || f.tipoafiliacion == 4209 || f.tipoafiliacion == 4520 || f.tipoafiliacion == 2938  ||f.tipoafiliacion == 4521 ){
						
						if(f.tipoafiliacion == 4520){
							$("#txtNit").removeAttr("disabled");
						}
						
						$("#tipAfiliacion").attr("disabled",false);
						$("#tipAfiliacion option[value=18]").hide(); // ocultar dependiente en el tipo de afiliaci�n
						
						//Mostrar Fila independientes
						$("#rowIndependientes").show();
						
						$.ajax({
							url: URL+'aportes/trabajadores/buscarActividadIndependiente.php', 
							type: "POST",
							data: {idpersona:f.idpersona,idradicacion:f.idradicacion},
							dataType: "json",
							success: function(actividad){
								
								if(actividad!=false){
									
									sinActividadEconomicaIndependiente=false;
									
									//Cargar Actividad economica, y 1 seg despues la categoria.
									$("#select7").val(actividad.idciiu);
								 	setTimeout(function(){
										$("#lisCodigo,#codigoCIU").val($.trim(actividad.idcategoria));
										
									},1000);
								
									
								}else{
									alert("No existen actividades economicas asociadas al trabajador independiente");
									sinActividadEconomicaIndependiente=true;
								}
							},
							error:function(xhr,obj,wtf){
								alert("Hubo un error cargar la actividad economica "+xhr+ " tipo "+obj+" - "+wtf);
							}
						});
						
						
					}else if(f.tipoafiliacion == 18){
						
						$("#tipAfiliacion").attr("disabled",true);
					}else{
						
						//Reg. Especial
						$("#tipAfiliacion").attr("disabled",false);
					}
					
					//##########################################################################################
					
					$("#fecIngreso").val(f.fechaingreso);
					$("#horasDia").val(f.horasdia);
					$("#horasMes").val(f.horasmes);
					$("#tipoPago").val(f.tipopago);
					$("#salario").val(f.salario);
					$("#categoria").val(f.categoria);
					$("#agricola").val(f.agricola);
					$("#cargo").val(f.cargo);
					$("#cboTipo").val(f.primaria);
					$("#estado").val(f.estado);
					$("#idafiliacion").val(f.idformulario);
					$("#idEmpresa,#idEmpresaTemporal").val(f.idempresa);
					
					idp=f.idpersona;
					ide=$("#idEmpresa").val();
					if(f.estado=='I'){
						$("#infoRetiro").show();
					}else{
						$("#infoRetiro").hide();
					}
					
					$("#cmbClaseAfiliacion").val(f.claseafiliacion);
				});//each
			},
			complete: function(){
				$.ajax({
					url: URL+'phpComunes/pdo.b.empresa.id.php', 
					type: "POST",
					data: {v0:ide,v1:1},
					dataType: "json",
					success: function(dato){
						if(dato==0){
							MENSAJE("No se encontro la Empresa!");
							$("#txtNit").val('');
							return false;
						}
						$.each(dato,function(i,f){
							$("#txtNit").val(f.nit);
							$("#tdEmpresa").html(f.razonsocial);
							return;
						});
						$.getJSON(URL+'phpComunes/pdo.buscar.persona.id.php',{v0:idp},function(data){
							nom = data[0].pnombre+" "+data[0].snombre+" "+data[0].papellido+" "+data[0].sapellido;
							$("#tdAfiliado").html(nom);
							
							//Pais de residencia
							if(data[0].idpaisreside==''||data[0].idpaisreside==null){
								$("#cboReside").val("N");
								$("#cboPais").val("48");
							}else{
								if(data[0].idpaisreside!='48'){
									$("#cboReside").val("S");
									$("#cboPais").val(data[0].idpaisreside);
								}
							}
							
						});
					}
				});
			}			
		});
		
	/**************************
	 * AFILIACIONES INACTIVAS *
	 **************************/
		
	} else if(estado == 'I') {
		
		$("#txtNit").attr("disabled","disabled");
		
		$.ajax({
			url: 'buscarAfiliacionInactivaPorFormulario.php',
			type: "POST",
			data: {idformulario:idf},
			async: false,
			dataType: "json",
			success: function(datos){
				if(datos==0){
					MENSAJE("Lo lamento no se encuentra registro!");
					return false;
				}
				$("#actualizarAfiliacion").show();
				$.each(datos,function(i,f){
					$("#tipForm").val(f.tipoformulario);
					$("#tipAfiliacion").attr("disabled",false);
					$("#tipAfiliacion").val(f.tipoafiliacion);
					if(f.tipoafiliacion == 19 || f.tipoafiliacion == 20 || f.tipoafiliacion == 21 || f.tipoafiliacion == 23 || f.tipoafiliacion == 2938){
						
						if(f.tipoafiliacion == 4520){
							$("#txtNit").removeAttr("disabled");
						}
						
						
						$("#tipAfiliacion").attr("disabled",false);
						$("#tipAfiliacion option[value=18]").hide(); // ocultar dependiente en el tipo de afiliación
					}else if(f.tipoafiliacion == 18){
						$("#tipAfiliacion").attr("disabled",true);
					}else{
						$("#tipAfiliacion").attr("disabled",false);
					}
					$("#fecIngreso").val(f.fechaingreso).trigger("change");
					$("#horasDia").val(f.horasdia);
					$("#horasMes").val(f.horasmes);
					$("#tipoPago").val(f.tipopago);
					$("#salario").val(f.salario);
					$("#categoria").val(f.categoria);
					$("#agricola").val(f.agricola);
					$("#cargo").val(f.cargo);
					$("#cboTipo").val(f.primaria);
					$("#estado").val(f.estado);
					$("#idafiliacion").val(f.idformulario);
					$("#datepicker").val(f.fecharetiro);
					$("#idEmpresa,#idEmpresaTemporal").val(f.idempresa);
					
					idp=f.idpersona;
					ide=$("#idEmpresa").val();
					$("#infoRetiro").show();
					$("#codigo").val(f.motivoretiro);
					
					$("#cmbClaseAfiliacion").val(f.claseafiliacion);
					
					//Añadir Idempresa de la afiliacion NUEVO 2016
					$("#idEmpresa,#idEmpresaTemporal").val(ide);
				});
			},
			complete: function(){
				$.ajax({
					url: URL+'phpComunes/pdo.b.empresa.id.php', 
					type: "POST",
					data: {v0:ide,v1:1},
					dataType: "json",
					success: function(dato){
						if(dato==0){
							MENSAJE("No se encuentra la Empresa!");
							$("#txtNit").val('');
							return false;
						}
						$.each(dato,function(i,f){
							$("#txtNit").val(f.nit);
							$("#tdEmpresa").html(f.razonsocial);
							return;
						});
						$.getJSON(URL+'phpComunes/pdo.buscar.persona.id.php',{v0:idp},function(data){
							nom = data[0].pnombre+" "+data[0].snombre+" "+data[0].papellido+" "+data[0].sapellido;
							$("#tdAfiliado").html(nom);
						});
					}
				});
			}
		});
	}
	
	$('#idPersona').val(idp);	
	
}
	
function validarSubsidio(){
	var afilia=$("#tipAfiliacion").val();
	var formul=$("#tipForm").val();
	if(afilia==0)return;
	if(formul==48 && afilia!=18){
		MENSAJE("Solo tienen derecho a subsidio familiar las afiliaciones de trabajadores DEPENDIENTES!, cambie el tipo de formulario o el tipo de afiliaci&oacute;n");
		$("#tipAfiliacion").val(0);
		$("#tipForm").val(0);
		$("#tipForm").focus();
	}
	
}
	

function actualizarAfiliacion(){
	
	var nit=				$("#txtNit").val();
	var idf=				$("#idafiliacion").val();
	var tipoformulario= 	$("#tipForm").val();
	var tipoafiliacion= 	$("#tipAfiliacion").val();
	var fechaingreso=   	$("#fecIngreso").val();
	var horasdia=	    	$("#horasDia").val();
	var horasmes=       	$("#horasMes").val();
	var tipopago=			$("#tipoPago").val();
	var salario=			$("#salario").val();
	var categoria=			$("#categoria").val();
	var agricola=			$("#agricola").val();
	var cargo=				$("#cargo").val();
	var primaria=			$("#cboTipo").val();
	var estado=				$("#estado").val();
	var idafiliacion=		$("#idafiliacion").val();
	var codigo=				$("#codigo").val();
	var fecharetiro=		$("#datepicker").val();
	var claseafiliacion=	$("#cmbClaseAfiliacion").val();
	var idradicacion=	    $("#idRadicacion").val();
	
	
		
	var error=0;
	$("table.tablero :input.ui-state-error,select.ui-state-error").removeClass("ui-state-error");//Limpiar la clase de error
	
	//------------------VALIDACIONES----------------------
   	if(tipoformulario=='0'){ 
   		$("#tipForm").addClass("ui-state-error");
   		error++;
   	}
	if(nit.length==0){ 
   		$("#txtNit").addClass("ui-state-error");
   		error++;
   	}
	
	if(tipoafiliacion=="4520"&&nit=="0"){ 
   		$("#txtNit").addClass("ui-state-error");
   		error++;
   	}
	
	if(tipoafiliacion=='0'){ 
   		$("#tipAfiliacion").addClass("ui-state-error");
   		error++;
   	}
	if(fechaingreso.length==0){ 
   		$("#fecIngreso").addClass("ui-state-error");
   		error++;
   	}
	if(horasdia.length==0){ 
   		$("#horasDia").addClass("ui-state-error");
   		error++;
   	}
	if(horasmes.length==0){ 
   		$("#horasMes").addClass("ui-state-error");
   		error++;
   	}
	if(tipopago=='0'){ 
   		$("#tipoPago").addClass("ui-state-error");
   		error++;
   	}
	if(salario=='' || isNaN(salario)){ 
   		$("#salario").addClass("ui-state-error");
   		error++;
   	}
	if(categoria.lengt==0){ 
   		$("#categoria").addClass("ui-state-error");
   		error++;
   	}
	if(agricola=='0'){ 
   		$("#agricola").addClass("ui-state-error");
   		error++;
   	}
	if(cargo=='0'){ 
   		$("#cargo").addClass("ui-state-error");
   		error++;
   	}
	if(primaria=='0'){ 
   		$("#cboTipo").addClass("ui-state-error");
   		error++;
   	}
	if(estado=='0'){ 
   		$("#estado").addClass("ui-state-error");
   		error++;
   	}
	if(estado=='I' && codigo=='0'){ 
   		$("#codigo").addClass("ui-state-error");
   		error++;
   	}
	if(estado=='I' && fecharetiro==''){ 
   		$("#datepicker").addClass("ui-state-error");
   		error++;
   	}
	
	//Validar Campos de Independientes
	
	if($("#rowIndependientes").is(":visible")){
		
		if($("#select7").val()=="0"){
			$("#select7").addClass("ui-state-error");
			error++
		}
		
		if($("#codigoCIU").val()==""){
			$("#codigoCIU").addClass("ui-state-error");
			error++
		}
		
	}
	
	if(error>0){
		MENSAJE("Llene los campos obligatorios!");
		return false; 
	}
	
	var mensajeSistema="";
	
	if($("#rowIndependientes").is(":visible")){
		
		var idPaisResidencia=	$("#cboPais").val();
		var idciiu=				$("#select7").val();
		var categoriaActividad=	$("#codigoCIU").val();
		
		/*
		 * ACTUALIZAR PAIS DE RESIDENCIA INDEPENDIENTES 2016
		 */
		
		if(idPaisResidencia!='48'){
			 $.ajax({
				   url: 'actualizarPais.php',
				   type:"POST",
				   async:false,
				   data:{idpersona:$("#idPersona").val(),idpais:$("#cboPais").val()},
				   success:function(data){
					   if(data==1){
						   mensajeSistema+=" ,Pais de residencia";
					   }
					   else{
						   alert("No se pudo actualizar el pais de residencia");
						   return false;
					   }
				   },
				   error:function(xhr,obj,quepaso){
					   alert("Error al actualizar el Pais.");
					   return false;
				   }
				
			});
		
		}
		/*
		 * ACTUALIZAR ACTIVIDAD ECONOMICA INDEPENDIENTES 2016
		 */
		 
		$ejecutarAccion=(	sinActividadEconomicaIndependiente	==	true	?	"I"	:	"A"	);
		
		 $.ajax({
			   url: 'actualizarActividadIndependiente.php',
			   type:"POST",
			   async:false,
			   data:{idciiu:idciiu,categoria:categoriaActividad,idpersona:$("#idPersona").val(),idradicacion:idradicacion,accion:$ejecutarAccion},
			   success:function(data){
				   if(data==1){
					   mensajeSistema+=" y Actividad Economica";
				   }
				   else{
					   alert("No se pudo actualizar La activ. Economica.");
					   return false;
				   }
			   },
			   error:function(xhr,obj,quepaso){
				   alert("Error al actualizar La actividad Economica.");
				   return false;
			   }
			
		});
	
	}
	/*
	 * ACTUALIZAR AFILIACION 
	 */
	
	$.getJSON('actualizarAfiliacion.php',{v0:idafiliacion,v1:tipoformulario,v2:tipoafiliacion,v3:ide,v4:fechaingreso,v5:horasdia,v6:horasmes,v7:salario,v8:agricola,v9:cargo,v10:primaria,v11:estado,v12:tipopago,v13:categoria,v14:codigo,v15:idp,v16:fecharetiro,v17:claseafiliacion,estadoActual: estadoActualAfiliacion},function(datos){
		if(datos==0){
			MENSAJE("El registro NO fue actualizado!");
		}else if (datos==2) {
			alert("La afiliacion fue inactivada, pas\xf3 al hist\xf3rico "+mensajeSistema);
			estadoActualAfiliacion = 'I';
			limpiarCampos_otro2();
			$("#div-msje").remove();
			buscarAfiliaciones(idp);
			observacionesTab(idp,1);
		}else{
		    alert("Afiliacion actualizada "+mensajeSistema);
			limpiarCampos_otro2();
			observacionesTab(idp,1);
			$("#div-msje").remove();
			buscarAfiliaciones(idp);
		}
	});
}	

function limpiarCampos_otro2(){
	$("#actualizarAfiliacion").hide();
	$("#tdAfiliado").html("");
	$("#datepicker").val("");
	$('#tdAfiliado').val('');
	$("#txtNit").val('');
	$('#tdEmpresa').html("");
	$("#tipForm").val(0);
	$("#tipAfiliacion").val(0);
	$("#fecIngreso").val('');
	$("#horasDia").val('');
	$("#horasMes").val('');
	$("#tipoPago").val(0);
	$("#salario").val('');
	$("#categoria").val('');
	$("#agricola").val(0);
	$("#cargo").val(0);
	$("#cboTipo").val(0);
	$("#estado").val(0).trigger("change");
}

/*
 * Calcular Categoria del Independiente Segun Honorarios y Tipo de Afiliaciï¿½n:
 * 
 * 	-Los independientes facultativos,extranjeros, e Independientes agremiados siempre la categorï¿½a serï¿½ B.
 * 	-Los pensionados de rï¿½gimen especial y sistemas de cajas deben ser A.
 * 	-Los pensionados voluntarios,  la categorï¿½a varï¿½a segï¿½n el valor de la mesada.
 * 
 * 	ANDRES LARA 2016
 */

function calcularCategoriaIndependienteTab(honorarios){
	
	var categoria='';
	
	var salario=parseInt(honorarios);
	
	//Campo Oculto Con el SMLV en el formulario
	var salarioMinimoLegalVigente=$('#txtSMLV').val();
	
	//Tipo de Afiliacion
	var tipoAfiliacion=$("#tipAfiliacion").val();
	
	if(tipoAfiliacion=='19'||tipoAfiliacion=='4520'||tipoAfiliacion=='4521'){
		categoria='B';
	}else{
		
		if(tipoAfiliacion=='21'||tipoAfiliacion=='4209'||tipoAfiliacion=='3320'||tipoAfiliacion=='2938'){
			categoria='A';
		}else{
			
			var totalSalario=parseFloat(salario/salarioMinimoLegalVigente);	
		   
		    if(totalSalario<=2){
		        categoria='A';
		    }
		    if(totalSalario>2 && totalSalario<=4){
		    	 categoria='B';
		    }
		    if(totalSalario>4){
		    	 categoria='C';
		    }
			
		}
		
	}
	
	 $('#categoria').val(categoria);
	
}

function calcularCategoria(sal){
	var salario=parseInt(sal);
	var smlv=$('#txtSMLV').val();
	smlv=(isNaN(smlv) && smlv>0)?616000:smlv;
	var sm=parseFloat(salario/smlv);
	
	if(sm<=2){
		return 'A';
	} else if(sm>2 && sm<=4){
		return 'B';
	} else if(sm>4){
		return 'C';
	} 
}	

function onChangeClaseAfiliacion(obj){
	var opt = obj.value;	
	if(opt=="0" || opt=="1"){
        $("#cargo").attr("disabled",false);    
    } else if(opt=="2") {
    	$("#cargo").val("3906"); 
    	$("#cargo").attr("disabled",true);
    } 
}