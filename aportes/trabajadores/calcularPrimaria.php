<?php

/* autor:       Orlando Puentes
 * fecha:       Julio 23 de 2010
 * objetivo:    Almacenar en la base de datos Aportes la información de los trabajadores que se van a afiliar a Comfamiliar Huila.
 */
 ini_set("display_errors",1);
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$idp=$_REQUEST['v0'];
$ide=$_REQUEST['v1'];
include_once $raiz . DIRECTORY_SEPARATOR . 'aportes' . DIRECTORY_SEPARATOR . 'trabajadores' . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'trabajadores.class.php' ;
/*include_once 'aportes/trabajadores/clases/trabajadores.class.php';*/
$objAfiliacion = new Trabajador();
$rs=$objAfiliacion->contar_afiliaciones_p($idp);
$row=mssql_fetch_array($rs);
if($row['cuenta']==0){
	echo json_encode('S');
	exit();
}
mssql_free_result($rs);
$rs=$objAfiliacion->contar_afi_pri_nit($idp, $ide);
$row=mssql_fetch_array($rs);
if($row['cuenta']==0){
	echo json_encode('N');
}
else{
	echo json_encode('S');
}

?>