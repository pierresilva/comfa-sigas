<?php
date_default_timezone_set('America/Bogota');
ini_set("display_errors",'1');
session_start();
$idp =$_REQUEST['v0'];

include_once '../../rsc/pdo/IFXDbManejador.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo $cadena;
	exit();
}
$filas = array();
$sql="SELECT a15.pnombre+' '+isnull(a15.snombre,'')+' '+a15.papellido+' '+isnull(a15.sapellido,'') as Beneficiario,
        b15.pnombre+' '+isnull(b15.snombre,'')+' '+b15.papellido+' '+isnull(b15.sapellido,'') as Muerto,
        c15.pnombre+' '+isnull(c15.snombre,'')+' '+c15.papellido+' '+isnull(c15.sapellido,'') as Tercero,
        a19.fechadefuncion,a91.detalledefinicion,a23.periodo
	  FROM aportes019 a19
      INNER JOIN aportes020 a20 ON a20.iddefuncion=a19.iddefuncion
      INNER JOIN aportes015 a15 ON a15.idpersona=a20.idbeneficiario
      LEFT  JOIN aportes023 a23 ON a23.iddetalle020=a20.iddefuncion
      INNER JOIN aportes015 b15 ON b15.idpersona=a19.idfallecido
      INNER JOIN aportes015 c15 ON c15.idpersona=a20.idtercero
      INNER JOIN aportes091 a91 ON a91.iddetalledef=a20.idpariente
      WHERE a20.idbeneficiario=$idp";
$rs=$db->querySimple($sql);
if(is_null($rs)){
	echo 0;
	exit();
}
while ($row=$rs->fetch()) {
		$filas[] = $row;
	}
if(count($filas)>0)
	echo json_encode($filas);
else 
	echo 0;
?>