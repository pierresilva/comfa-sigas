<?php
set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'p.planilla.unica.class.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';
$idp=$_REQUEST['v0']; 

$objPlanilla=new Planilla();
$result=$objPlanilla->buscar_planillas_consulta($idp);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>tPlanilla</title>
<link href="<?php echo URL_PORTAL; ?>css/sorterStyle.css" rel="stylesheet" type="text/css" media="screen" />
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/tableSorterDESC.js"></script>
<script type="text/javascript">		
	var sorterPlanillaTrabCon = new TINYDESC.tableDESC.sorter("sorterPlanillaTrabCon");
	sorterPlanillaTrabCon.head = "head";
	sorterPlanillaTrabCon.asc = "asc";
	sorterPlanillaTrabCon.desc = "desc";
	sorterPlanillaTrabCon.even = "evenrow";
	sorterPlanillaTrabCon.odd = "oddrow";
	sorterPlanillaTrabCon.evensel = "evenselected";
	sorterPlanillaTrabCon.oddsel = "oddselected";
	sorterPlanillaTrabCon.paginate = true;
	sorterPlanillaTrabCon.pagesize = (5);
	sorterPlanillaTrabCon.currentid = "currentpagePlanillaTrabCon";
	sorterPlanillaTrabCon.limitid = "pagelimitPlanillaTrabCon";
	sorterPlanillaTrabCon.init("tblPlanillaTrabCon",2);
</script>
</head>
<body>
	<table border="0">
		<tr>
			<td>
				<h4>Planilla Unica</h4>
			</td>
			<td class="t1">
				(Convensiones: I-Ingreso. R-Retiro. VS-Varianci&oacute;n Salarial. V-Vacaciones. M-Maternidad. TC-Tipo cotizante. P-Procesado. C-Correcci&oacute;n. ITE-Incapacidad Temporal de Enfermedad. ITAT-Incapacidad Temporal Acc Trabajo. STC-Supencion Temporal del Contrato. D-D&iacute;as. Pe-Periodo. A-Aporte. Dev-Devoluci&oacute;n)
			</td>
		</tr>
	</table>
	<table width="100%" border="0" cellspacing="0" class="sortable hover" id="tblPlanillaTrabCon" style="margin-left:-17px">
		<caption style="text-align:left; padding:5px; font-size:11px;"><span></span></caption>
		<thead>
			<tr>
				<th class="head" ><h3><strong>NIT</strong></h3></th>
				<th class="head" ><h3><strong>Planilla</strong></h3></th>
				<th class="head" ><h3><strong>Pe</strong></h3></th>
				<th class="head" ><h3><strong>Basico</strong></h3></th>
				<th class="head" ><h3><strong>Base</strong></h3></th>
				<th class="head" ><h3><strong>A</strong></h3></th>
				<th class="head" ><h3><strong>Dev</strong></h3></th>
				<th class="head" ><h3><strong>D</strong></h3></th>
				<th class="head" ><h3><strong>Fecha</strong></h3></th>
				<th class="head" width="10"><h3><strong>I</strong></h3></th>
				<th class="head" width="10"><h3><strong>R</strong></h3></th>
				<th class="head" width="10"><h3><strong>VS</strong></h3></th>
				<th class="head" width="10"><h3><strong>V</strong></h3></th>
				<th class="head" width="10"><h3><strong>M</strong></h3></th>
				<th class="head" ><h3><strong>TC</strong></h3></th>
				<th class="head" ><h3><strong>P</strong></h3></th>
				<th class="head" ><h3><strong>C</strong></h3></th>
				<th class="head" ><h3><strong>ITE</strong></h3></th>
				<th class="head" ><h3><strong>ITAT</strong></h3></th>
				<th class="head" ><h3><strong>STC</strong></h3></th>
			</tr>
		</thead>
		<tbody>
			<?php
				$cont=0;
				while ($consulta=mssql_fetch_array($result)){
					$cont++; 
			?>
			<tr>
				<td><a class='dialogoEmergente'><?php echo $consulta['nit'];  ?><span><?php echo $consulta['rsocial'];  ?></span></a></td>
				<td><?php echo $consulta['planilla']; ?></td>
				<td style=text-align:center><?php echo $consulta['periodo']; ?></td>
				<td style=text-align:right><?php echo number_format($consulta['salariobasico']); ?></td>
				<td style=text-align:right><?php echo number_format($consulta['ingresobase']); ?></td>
				<td style=text-align:right><?php echo number_format($consulta['valoraporte']); ?></td>
				<td style=text-align:right><?php echo number_format($consulta['devolucion']); ?></td>
				<td style=text-align:right><?php echo $consulta['diascotizados']; ?></td>
				<td style=text-align:center><?php echo $consulta['fechapago']; ?></td>
				<td><?php echo $consulta['ingreso']; ?></td>
				<td><?php echo $consulta['retiro']; ?></td>
				<td><?php echo $consulta['var_tra_salario']; ?></td>
				<td><?php echo $consulta['vacaciones']; ?></td>
				<td><?php echo $consulta['lic_maternidad']; ?></td>
				<td><?php echo $consulta['tipo_cotizante']; ?></td>
				<td><?php echo $consulta['procesado']; ?></td>
				<td><?php echo $consulta['correccion']; ?></td>
				<td><?php echo $consulta['inc_tem_emfermedad']; ?></td>
				<td><?php echo $consulta['inc_tem_acc_trabajo']; ?></td>
				<td><?php echo $consulta['sus_tem_contrato']; ?></td>
	    	</tr>
				<?php }
				if ($cont==0) { ?>
					<script type="text/javascript">	
						MENSAJE("NO existen aportes.");
					</script>
				<?php }
			?>
		</tbody>
	</table>
    
    <div id="controls">
		<div id="perpage">
			<select onChange="sorterPlanillaTrabCon.size(this.value)">
			<option value="5" selected="selected">5</option>
				<option value="10" >10</option>
				<option value="20">20</option>
				<option value="50">50</option>
				<option value="100">100</option>
			</select>
			<label>&nbsp;&nbsp;&nbsp;&nbsp;Registros Por P&aacute;gina</label>
		</div>
		<div id="navigation">
			<img src="<?php echo URL_PORTAL; ?>imagenes/imagesSorter/first.gif" width="16" height="16" alt="first Page" onClick="sorterPlanillaTrabCon.move(-1,true)" />
			<img src="<?php echo URL_PORTAL; ?>imagenes/imagesSorter/previous.gif" width="16" height="16" alt="first Page" onClick="sorterPlanillaTrabCon.move(-1)" />
			<img src="<?php echo URL_PORTAL; ?>imagenes/imagesSorter/next.gif" width="16" height="16" alt="first Page" onClick="sorterPlanillaTrabCon.move(1)" />
			<img src="<?php echo URL_PORTAL; ?>imagenes/imagesSorter/last.gif" width="16" height="16" alt="Last Page" onClick="sorterPlanillaTrabCon.move(1,true)" />
		</div>
		<div id="text">&nbsp;&nbsp;&nbsp;&nbsp;P&aacute;gina <label id="currentpagePlanillaTrabCon"></label> de <label id="pagelimitPlanillaTrabCon"></label></div>
	</div>    

	<input type="hidden" name="idPersonaCon" id="idPersonaCon" value="<?php echo $idp; ?>" />
</body>
</html>
