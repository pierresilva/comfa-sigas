<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';

include_once $raiz.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'observacion.class.php';

$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

$usuario = $_SESSION["USUARIO"];

$arrDatos = $_REQUEST["datos"];

$objObservacion = new Observacion();

$banderaError = 0;

$db->inicioTransaccion();
foreach($arrDatos["datos_afiliacion"] as $arrRow){
	
	//Actualizar informacion
	if($arrRow["observacion_modifica"]!=""){
		$query = "UPDATE aportes017 SET 
						tipoformulario={$arrRow["tipo_formulario"]}
						, tipoafiliacion={$arrRow["tipo_afiliacion"]}
						, fechaingreso= '{$arrRow["fecha_ingreso"]}'
						, fecharetiro= '{$arrRow["fecha_retiro"]}'
						, cargo={$arrRow["cargo"]}
						, usuario='$usuario'
						, fechasistema=getdate()
					WHERE idformulario={$arrRow["id_formulario"]}";
		
		$rs = $db->queryActualiza($query);
		if(intval($rs)==0){
			$banderaError++;
			break;
		}
		
		//Guardar observacion		
		$rsObservacion = $objObservacion->crearObservacion($arrDatos["id_persona"], 1	, $arrRow["observacion_modifica"], $usuario);
		
		if($rsObservacion==0){
			$banderaError++;
			break;
		}
	}
	
	if($arrRow["activar"]=="SI"){
		//Activar afiliacion
		$query = "INSERT INTO dbo.aportes016 (tipoformulario, tipoafiliacion, idempresa, idpersona, fechaingreso, horasdia, horasmes, salario, agricola, cargo, primaria, estado, fecharetiro, motivoretiro, fechanovedad, semanas, fechafidelidad, estadofidelidad, traslado, codigocaja, flag, tempo1, tempo2, fechasistema, usuario,    tipopago, categoria, auditado, idagencia, idradicacion, vendedor, codigosalario, flag2, porplanilla, madrecomunitaria, claseafiliacion)
									  SELECT  tipoformulario, tipoafiliacion, idempresa, idpersona, fechaingreso, horasdia, horasmes, salario, agricola, cargo, primaria, 'A'   , NULL       , NULL        , fechanovedad, semanas, NULL          , NULL           , traslado, codigocaja, flag, tempo1, tempo2, getdate()   , '$usuario', tipopago, categoria, auditado, idagencia, idradicacion, NULL    , NULL         , NULL , NULL       , NULL            , NULL
				 					  FROM aportes017
									  WHERE idformulario={$arrRow["id_formulario"]}";
		
		$rs = $db->queryInsert($query,"aportes016");
		if($rs===null){
			$banderaError++;
			break;
		}
		
		//Eliminar afiliacion
		$query = "DELETE FROM aportes017 WHERE idformulario={$arrRow["id_formulario"]}";
		
		$rs = $db->queryActualiza($query);
		if($rs===null){
			$banderaError++;
			break;
		}
		
		//Actualizar el estado de la empresa
		if(intval($arrRow["tipo_afiliacion"])!=18){ //[18][DEPENDIENTE]
			$query = "UPDATE aportes048 SET 
						estado='A', usuario='$usuario', fechaestado=getdate() 
					  WHERE idempresa=".$arrRow["id_empresa"];
			
			$rs = $db->queryActualiza($query);
			if($rs===null){
				$banderaError++;
				break;
			}
		}
		
		//Guardar observacion
		$rsObservacion = $objObservacion->crearObservacion($arrDatos["id_persona"], 1	, $arrRow["observacion_activa"], $usuario);
		
		if($rsObservacion==0){
			$banderaError++;
			break;
		}
	}
	
} 

if($banderaError==0){
	$db->confirmarTransaccion();
	echo 1;
}else{
	$db->cancelarTransaccion();
	echo 0;
}

?>