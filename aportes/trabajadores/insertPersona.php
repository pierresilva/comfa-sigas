<?php
// @autor:		Orlando Puentes A
// @fecha:		mayo 31/2010
// @formulario:	guardar datos persona - afiliacion nueva
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

$campos[0] = "0";
$campos[1] = (empty($_REQUEST['v1'])) ? NULL : $_REQUEST['v1'];
$campos[2] = (empty($_REQUEST['v2'])) ? NULL : $_REQUEST['v2'];
$campos[3] = strtoupper(trim((empty($_REQUEST['v3'])) ? NULL : $_REQUEST['v3']));
$campos[4] = strtoupper(trim((empty($_REQUEST['v4'])) ? NULL : $_REQUEST['v4']));
$campos[5] = strtoupper(trim((empty($_REQUEST['v5'])) ? NULL : $_REQUEST['v5']));
$campos[6] = strtoupper(trim((empty($_REQUEST['v6'])) ? NULL : $_REQUEST['v6']));
$campos[7] = (empty($_REQUEST['v7'])) ? NULL : $_REQUEST['v7'];
$campos[8] = (empty($_REQUEST['v8'])) ? NULL : $_REQUEST['v8'];
$campos[9]  = (empty($_REQUEST['v9'])) ? NULL : $_REQUEST['v9'];
$campos[10] = (empty($_REQUEST['v10'])) ? NULL : $_REQUEST['v10'];
$campos[11] = (empty($_REQUEST['v11'])) ? NULL : $_REQUEST['v11'];
$campos[12] = (empty($_REQUEST['v12'])) ? NULL : $_REQUEST['v12'];
$campos[13] = (empty($_REQUEST['v13'])) ? NULL : $_REQUEST['v13'];
$campos[14] = (empty($_REQUEST['v14'])) ? NULL : $_REQUEST['v14'];
$campos[15] = (empty($_REQUEST['v15'])) ? NULL : $_REQUEST['v15'];
$campos[16] = (empty($_REQUEST['v16'])) ? NULL : $_REQUEST['v16'];
$campos[17] = (empty($_REQUEST['v17'])) ? NULL : $_REQUEST['v17'];
$campos[18] = (empty($_REQUEST['v18'])) ? NULL : $_REQUEST['v18'];
$campos[19] = (empty($_REQUEST['v19'])) ? NULL : $_REQUEST['v19'];
$campos[20]= (empty($_REQUEST['v20'])) ? NULL : $_REQUEST['v20'];
$campos[21] = (empty($_REQUEST['v21'])) ? NULL : $_REQUEST['v21'];
$campos[22] = (empty($_REQUEST['v22'])) ? NULL : $_REQUEST['v22'];
$campos[23] = (empty($_REQUEST['v23'])) ? NULL : $_REQUEST['v23'];
$campos[24] = $_SESSION['USUARIO'];
$campos[25] = (empty($_REQUEST['v25'])) ? NULL : $_REQUEST['v25'];//IDTIPOPROPIEDAD
$campos[26] = (isset($_REQUEST['v26']))?$_REQUEST['v26']:48;

//1 idtipodocumento,2 identificacion,3 papellido,4 sapellido,5 pnombre,6 snombre,7 sexo,8 direccion,9 idbarrio,10 telefono,11 celular,12 email,13 idtipovivienda,14 iddepresidencia,15 idciuresidencia,16 idzona,17 idestadocivil,18 fechanacimiento, 19 idciunace,20 iddepnace,21 capacidadtrabajo,22 idprofesion,23 nombrecorto,24 usuario
$sql="insert into aportes015 (idtipodocumento, identificacion, papellido, sapellido, pnombre, snombre, sexo, direccion, idbarrio, telefono, celular, email, idpropiedadvivienda, idtipovivienda, iddepresidencia, idciuresidencia, idzona, idestadocivil, fechanacimiento, idciunace, iddepnace, capacidadtrabajo, idprofesion, nombrecorto,usuario, ruaf, biometria, fechaafiliacion, estado,validado,fechasistema,idpais) values('".$campos[1]."','".$campos[2]."','".$campos[3]."','".$campos[4]."','".$campos[5]."','".$campos[6]."','".$campos[7]."','".$campos[8]."','".$campos[9]."','".$campos[10]."','".$campos[11]."','".$campos[12]."','".$campos[25]."','".$campos[13]."','".$campos[14]."','".$campos[15]."','".$campos[16]."','".$campos[17]."','".$campos[18]."','".$campos[19]."','".$campos[20]."','".$campos[21]."','".$campos[22]."','".$campos[23]."','".$campos[24]."','N','N',cast(getdate() as date), 'N', 'N', cast(getdate() as date), $campos[26])";
$rs=$db->queryInsert($sql,'aportes015');
$error=$db->error;
if(is_array($error)){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}
echo $rs;
?>