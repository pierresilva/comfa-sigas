<?php
date_default_timezone_set('America/Bogota');
ini_set("display_errors",'1');
session_start();
$idp =$_REQUEST['v0'];

include_once '../../rsc/pdo/IFXDbManejador.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo $cadena;
	exit();
}
$filas = array();
$sql="SELECT a15.pnombre,a15.snombre,a15.papellido,a15.sapellido,
       a43.valorpignorado,a43.fechapignoracion,a43.saldo,a43.idpignoracion,a44.periodo
	  FROM aportes043 a43
      INNER JOIN aportes044 a44 ON a44.idpignoracion=a43.idpignoracion
      INNER JOIN aportes015 a15 ON a15.idpersona=a44.idbeneficiario
      WHERE a44.idbeneficiario=$idp";
$rs=$db->querySimple($sql);
if(is_null($rs)){
	echo 0;
	exit();
}
while ($row=$rs->fetch()) {
		$filas[] = $row;
	}
if(count($filas)>0)
	echo json_encode($filas);
else 
	echo 0;
?>