<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$c0 = $_REQUEST['v0'];		//id Beneficiario Eliminar
$c1 = $_REQUEST['v1'];		//id Beneficiario Trasladar
$c2 = $_REQUEST['v2'];		//Cantidad Relaciones a Modificar
$c3 = $_REQUEST['v3'];		//Cantidad Cuotas09 a Modificar
$c4 = $_REQUEST['v4'];		//Cantidad Cuotas14 a Modificar
$c5 = $_REQUEST['v5'];		//Cantidad Paquetes a Modificar
$c6 = $_REQUEST['v6'];		//Cantidad Certificados a Modificar
$c10 = $_REQUEST['v10'];	//Cantidad Causales a Modificar
$c11 = $_REQUEST['v11'];	//Cantidad Subsidio a Modificar
$c12 = $_REQUEST['v12'];	//Cantidad Embargos a Modificar
$c13 = $_REQUEST['v13'];	//Cantidad Pignoraciones a Modificar
$c14 = $_REQUEST['v14'];	//Cantidad Defunciones a Modificar
$c15 = $_REQUEST['v15'];	//Id certificados a eliminar
$c16 = $_REQUEST['v16'];	//Id certificados a unificar
$usuario=$_SESSION['USUARIO'];

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

$respuesta=9;

if(intval($c2)>0){
	$sql="SELECT a21.idtrabajador FROM aportes021 a21 WHERE a21.idbeneficiario=$c1";
	$rs=$db->querySimple($sql);
	if(!(is_null($rs))){
		while ($row=$rs->fetch()){
			$idtrabajador=$row['idtrabajador'];			
			$sqlEliminar="DELETE FROM aportes021 WHERE idbeneficiario=$c0 AND idtrabajador=$idtrabajador";
			$rsEliminar=$db->queryActualiza($sqlEliminar);
			if(!(is_null($rsEliminar))){ $c2 = intval($c2) - intval($rsEliminar); }
		}		
	}
	
	if(intval($c2)>0){
		$sql="UPDATE aportes021 SET idbeneficiario=$c1, usuario='$usuario' WHERE idbeneficiario=$c0";
		$rs=$db->queryActualiza($sql,'aportes021');
		if(is_null($rs)){
			$respuesta=1;			
		}
	}
}

if(intval($c3)>0 && $respuesta==9){
	$sql2="UPDATE aportes009 SET idbeneficiario=$c1, usuario='$usuario' WHERE idbeneficiario=$c0";
	$rs2=$db->queryActualiza($sql2,'aportes009');
	if(is_null($rs2)){
		$respuesta=2;		
	}
}

if(intval($c4)>0 && $respuesta==9){
	$sql3="UPDATE aportes014 SET idbeneficiario=$c1, usuario='$usuario' WHERE idbeneficiario=$c0";
	$rs3=$db->queryActualiza($sql3,'aportes014');
	if(is_null($rs3)){
		$respuesta=3;		
	}
}

if(intval($c5)>0 && $respuesta==9){
	$sql4="UPDATE aportes057 SET idbeneficiario=$c1, usuario='$usuario' WHERE idbeneficiario=$c0";
	$rs4=$db->queryActualiza($sql4,'aportes057');
	if(is_null($rs4)){
		$respuesta=4;		
	}
}

if(intval($c6)>0){
	$sql5="UPDATE aportes026 SET idbeneficiario=$c1, usuario='$usuario' WHERE 
	idbeneficiario=$c0 AND idcertificado IN($c16)";		
	$rs5=$db->queryActualiza($sql5,'aportes026');	
	if(is_null($rs5)){
		$respuesta=5;		
	} 
	
	$sqlCert="SELECT COUNT(idcertificado) cant, max(idcertificado) max FROM aportes026 WHERE idbeneficiario=$c1 AND estado='A'";
	$rsCert=$db->querySimple($sqlCert);
	
	while($row=$rsCert->fetch()){
		if(intval($row['cant'])>1){
			$maxCert=$row['max'];
			$sql6="UPDATE aportes026 SET estado='I' WHERE idbeneficiario=$c1 AND idcertificado<$maxCert AND idcertificado NOT IN($c16)";
			$rs6=$db->queryActualiza($sql6,'aportes026');
			if(is_null($rs6)){
				$respuesta=6;					
			}
		}

	}
		
}


//CAUSALES
if(intval($c10)>0 && $respuesta==9){
	$sql6="UPDATE aportes065 SET idbeneficiario=$c1 WHERE idbeneficiario=$c0";
	$rs6=$db->queryActualiza($sql6,'aportes065');
	if(is_null($rs6)){
		$respuesta=10;
	}
}
if(intval($c11)>0 && $respuesta==9){
	$sql7="UPDATE aportes099 SET idbeneficiario=$c1 WHERE idbeneficiario=$c0";
	$rs7=$db->queryActualiza($sql7,'aportes099');
	if(is_null($rs7)){
		$respuesta=11;
	}
}
if(intval($c12)>0 && $respuesta==9){
	$sql9="UPDATE aportes022 SET idbeneficiario=$c1 WHERE idbeneficiario=$c0";
	$rs9=$db->queryActualiza($sql9,'aportes022');
	if(is_null($rs9)){
		$respuesta=12;
	}
}
if(intval($c13)>0 && $respuesta==9){
	$sql10="UPDATE aportes044 SET idbeneficiario=$c1 WHERE idbeneficiario=$c0";
	$rs10=$db->queryActualiza($sql10,'aportes044');
	if(is_null($rs10)){
		$respuesta=13;
	}
}
if(intval($c14)>0 && $respuesta==9){
	$sql11="UPDATE aportes020 SET idbeneficiario=$c1, usuario='$usuario' WHERE idbeneficiario=$c0";
	$rs11=$db->queryActualiza($sql11,'aportes020');
	if(is_null($rs11)){
		$respuesta=14;
	}
}
if($respuesta==9){
		$sqlEliCert="DELETE FROM aportes026 WHERE idcertificado IN ($c15)";
		$rsEliCert=$db->querySimple($sqlEliCert);
		if(is_null($rsEliCert)){
			$respuesta=15;
		 }
}

if($respuesta==9){
	$sqlEliBene="DELETE FROM aportes015 WHERE idpersona=$c0";
	$rsEliBene=$db->queryActualiza($sqlEliBene);
	if(is_null($rsEliBene)){
		$respuesta=7;
	}
}

echo $respuesta;

die();
?>