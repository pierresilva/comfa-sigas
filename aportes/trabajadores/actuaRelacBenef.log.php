<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';

include_once $raiz.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'observacion.class.php';

$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

$usuario = $_SESSION["USUARIO"];

$arrDatos = $_REQUEST["datos"];

$objObservacion = new Observacion();

$banderaError = 0;

$db->inicioTransaccion();
foreach($arrDatos["datos_beneficiario"] as $arrRow){
	
	//Actualizar estado relacion
	$query = "UPDATE aportes021 SET 
					estado= '{$arrRow["estado_relacion"]}'
					, usuario = '$usuario'
					, fechaestado = getdate()
					, idmotivo = 0 
			WHERE idrelacion = {$arrRow["id_relacion"]}";
	
	$rs = $db->queryActualiza($query);
	if(intval($rs)==0){
		$banderaError++;
		break;
	}
	
	//Actualizar estado persona
	if($arrRow["estado_relacion"]=="A"){
		//Actualizar estado relacion
		$query = "UPDATE aportes015 SET
					estado= 'N'
					, usuario = '$usuario'
					, fechasistema = getdate()
				WHERE idpersona = {$arrRow["id_beneficiario"]}";
		
		$rs = $db->queryActualiza($query);
		if(intval($rs)==0){
			$banderaError++;
			break;
		}
	}
	
	//Guardar observacion		
	$rsObservacion = $objObservacion->crearObservacion($arrDatos["id_persona"], 1	, $arrRow["observacion"], $usuario);
	
	if($rsObservacion==0){
		$banderaError++;
		break;
	}
}

if($banderaError==0){
	$db->confirmarTransaccion();
	echo 1;
}else{
	$db->cancelarTransaccion();
	echo 0;
}

?>