<?php

/* autor:       Andres F. Lara
 * fecha:       Septiembre 14 de 2011
 * objetivo:    Almacenar los beneficiarios radicados. 
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'ciudades.class.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'funcionesComunes' . DIRECTORY_SEPARATOR . 'funcionesComunes.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'p.definiciones.class.php';

$objCiudad=new Ciudades;
$objClase= new Definiciones;

$consulta = $objClase->mostrar_datos(1,2);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::Nueva Persona::</title>
<link type="text/css" href="<?php echo URL_PORTAL; ?>css/Estilos.css" rel="stylesheet"/>
<link type="text/css" href="<?php echo URL_PORTAL; ?>css/formularios/base/ui.all.css" rel="stylesheet" />
<link type="text/css" href="<?php echo URL_PORTAL; ?>css/estilo_tablas.css" rel="stylesheet"/>

<link href="<?php echo URL_PORTAL; ?>css/marco.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/jquery-1.4.2.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/formularios/ui/ui.core.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/formularios/ui/ui.tabs.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/formularios/ui/ui.dialog.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/formularios/ui/ui.datepicker.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/comunes.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/jquery.combos.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/jquery.fechaLarga.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/jquery.personaSimple.js"></script>
<script language="javascript" type="text/javascript" src="<?php echo URL_PORTAL; ?>js/direccion.js"></script>
<script type="text/javascript" src="js/nuevaPersona.js" ></script>

<script type="text/javascript">
shortcut.add("Shift+F",function() {
		var URL=src();
		var url=URL+"aportes/trabajadores/consultaTrabajador.php";
    	window.open(url,"_blank");
    },{
		'propagate' : true,
		'target' : document 
	});        
</script>

<script type="text/javascript">
function notas(){
			$("#dialog-form2").dialog('open');
		}	
function mostrarAyuda(){
			$("#ayuda").dialog('open' );
		}
function validarEspacio(e){
	tecla=(document.all) ? e.keyCode : e.which;
	if(tecla==32){
		return false;
	}
}
</script>
</head>

<body>
<center>
<table width="80%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="13" height="29" class="arriba_iz">&nbsp;</td>
<td class="arriba_ce"><span class="letrablanca">::&nbsp;Inscripción Nueva Persona &nbsp;::</span></td>
<td width="13" class="arriba_de" align="right">&nbsp;</td>
</tr>
<tr>
<td class="cuerpo_iz">&nbsp;</td>                        
<td background="../../imagenes/tabla/centro.gif">
<img src="../../imagenes/tabla/spacer.gif" width="1" height="1"/> 
<img src="../../imagenes/spacer.gif" width="1" height="1"/> <img src="../../imagenes/menu/grabar.png" title="Guardar" width="16" height="16" onclick="verificarCampos()" style="cursor:pointer" id="bGuardar"/> 
<img src="../../imagenes/spacer.gif" width="1" height="1"/><img src="../../imagenes/spacer.gif" width="1" height="1"/>
<img src="../../imagenes/menu/refrescar.png" width="16" height="16" style="cursor:pointer" title="Limpiar campos" onclick="limpiarCampos();"/>
<img src="../../imagenes/spacer.gif" width="1" height="1"/><img src="../../imagenes/spacer.gif" width="1" height="1"/>
<a href="../../help/aportes/nuevosBeneficiarios.html" target="_blank" onclick="window.open(this.href, this.target, 'width=800,height=550,titlebar=0, resizable=no'); return false;" > 
<img src="../../imagenes/menu/informacion.png" width="16" height="16" style="border:none" title="Manual" />
</a> 
<img src="../../imagenes/spacer.gif" width="1" height="1"/> 
<img src="../../imagenes/menu/notas.png" width="16" height="16" style="cursor: pointer" title="ColaboraciÃ³n en LÃ­nea" onclick="notas();" /> 
</td>
<td class="cuerpo_de">&nbsp;</td>
</tr>
<tr>
<td class="cuerpo_iz">&nbsp;</td>
<td class="cuerpo_ce"><div id="resultado" style="color:#FF0000"></div></td>
<td class="cuerpo_de">&nbsp;</td>
</tr>
<tr>
<td class="cuerpo_iz">&nbsp;</td>
<td class="cuerpo_ce">

<table width="100%"  border="0" cellspacing="0" class="tablero">
<tr>
<td width="25%">Tipo Documento</td>
<td width="25%">
<select name="tipoDoc2" class="box1" id="tipoDoc2">
<option value="0">Seleccione..</option>
<?php
	$consulta=$objClase->mostrar_datos(1, 2);
	while($row=mssql_fetch_array($consulta)){
		echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
	}
?>
</select><img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
<td width="25%">Identificaci&oacute;n</td>
<td width="25%"><input name="identificacion2" type="text" class="box1" id="identificacion2" onblur="validarLongNumIdent(document.getElementById('tipoDoc2').value,this);buscarPersona(this.value);" onkeyup="validarCaracteresPermitidos(document.getElementById('tipoDoc2').value,this);"/>
<img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
</tr>         
<tr>
<td>Primer Nombre</td>
<td class="box1">
<input name="pNombre2" type="text" class="box1" id="pNombre2" onkeypress="return validarEspacio(event);" onkeydown='sololetras(this);' onkeyup='sololetras(this);'/>
<img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
<td>Segundo Nombre</td>
<td><input name="sNombre2" type="text" class="box1" id="sNombre2" onkeydown='sololetras(this);' onkeyup='sololetras(this);'/></td>
</tr>
<tr>
<td>Primer Apellido</td>
<td><input name="pApellido2" type="text" class="box1" id="pApellido2" onkeypress="return validarEspacio(event);" onkeydown='sololetras(this);' onkeyup='sololetras(this);'/>
<img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
<td>Segundo Apellido</td>
<td><input name="sApellido2" type="text" class="box1" id="sApellido2" onkeydown='sololetras(this);' onkeyup='sololetras(this);'/></td>
</tr>
<tr>
<td>Sexo</td>
<td><select name="sexo2" class="box1" id="sexo2">
<option selected="selected" value="0">Seleccione..</option>
<option value="M">Masculino</option>
<option value="F">Femenino</option>
</select>
<img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
<td>Ubicaci&oacute;n Vivienda</td>
<td><select name="tipoVivienda2" class="box1" id="tipoVivienda2">
  <option value="0">Seleccione..</option>
  <option value="U">Urbana</option>
  <option value="R">Rural</option>
</select><img src="../../imagenes/menu/obligado.png" width="12" height="12" /></td>
</tr>
<tr>
<td >Tipo Propiedad</td>
<td>
<select name="txtCasa" id="txtCasa" class="box1">
<option value="0" selected="selected">Seleccione...</option>
<?php
$defin = $objClase->mostrar_datos(42, 3);
while($r_defi=mssql_fetch_array($defin)){
	echo "<option value=".$r_defi['iddetalledef'].">".$r_defi['detalledefinicion']."</option>";
}
?>
</select><img src="../../imagenes/menu/obligado.png" width="12" height="12" /></td>
<td >Capacidad Trabajo</td>
<td ><select name="txtCapacidad" id="txtCapacidad" class="box1">
<option value="0" selected="selected">Seleccione...</option>
<option value="N">Normal</option>
<option value="I">Discapacitado</option>
</select><img src="../../imagenes/menu/obligado.png" width="12" height="12" /></td>
</tr>
<tr>
<td>Estado Civil</td>
<td><select name="estadoCivil2" class="box1" id="estadoCivil2">
  <?php
$consulta=$objClase->mostrar_datos(10, 3);
while($row=mssql_fetch_array($consulta)){
	echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
}
?>
</select>
<img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
<td>Profesi&oacute;n</td>
<td><select name="profesion2" class="box1" id="profesion2">
<option value="0">Seleccione..</option>
<?php

$consulta = $objClase->mostrar_datos(20,3);
 while( $row = mssql_fetch_array( $consulta ) ){
		echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
}
?>
<option>Otro..</option>
</select>
<img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
</tr>
<tr>
  <td>Departamento Residencia</td>
  <td><select name="cboDeptoC" class="box1" id="cboDeptoC">
    <option value="0">Seleccione..</option>
    <?php
	$consulta=$objCiudad->departamentos();
	while($row=mssql_fetch_array($consulta)){
	?>
    <option value="<?php echo $row["coddepartamento"]; ?>"><?php echo utf8_encode($row["departmento"]); ?></option>
    <?php
	}
?>
  </select><img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
  <td>Ciudad Residencia</td>
  <td><select name="cboCiudadC" class="box1" id="cboCiudadC">
  <option value="0">Seleccione..</option>
  </select><img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
</tr>
<tr>
  <td>Zona</td>
  <td><select name="cboZonaC" class="box1" id="cboZonaC">
  <option value="0">Seleccione..</option>
  </select><img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
  <td>Barrio</td>
  <td><select name="cboBarrioC" class="box1" id="cboBarrioC">
    <option value="0">Seleccione..</option>
  </select><img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
</tr>
<tr>
<td>Direcci&oacute;n </td>
<td><input name="direccion2" type="text" class="box1" id="direccion2" onfocus="direccion(this,document.getElementById('email2'));" /><img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
<td>E-mail</td>
<td><input name="email2" type="text" class="box1" id="email2" onblur="soloemail(this)"/></td>
</tr>
<tr>
<td>Tel&eacute;fono</td>
<td><input name="telefono2" type="text" class="box1" id="telefono2" maxlength="7" onkeydown="solonumeros(this);" onkeyup="solonumeros(this);"/>
</td>
<td>Celular</td>
<td><input name="celular2" type="text" class="box1" id="celular2" maxlength="10" onkeydown="solonumeros(this);" onkeyup="solonumeros(this);"/></td>
</tr>
<tr>
<td>Fecha De Nacimiento</td>
<td colspan="3"><input type="text" id="alternate2" readonly="readonly"/><img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></tr>
<tr>
<td>Departamento Nacimiento</td>
<td><select name="cboDeptoC2" class="box1" id="cboDeptoC2">
<option value="0">Seleccione..</option>
<?php
	$consulta=$objCiudad->departamentos();
	while($row=mssql_fetch_array($consulta)){
?>          
	<option value="<?php echo $row["coddepartamento"]; ?>"><?php echo utf8_encode($row["departmento"]); ?></option>
<?php
	}
?>
</select><img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
<td>Ciudad Nacimiento</td>
<td><select name="cboCiudadC2" class="box1" id="cboCiudadC2">
<option value="0">Seleccione..</option>
</select><img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
</tr>
</table>
</td>
<td class="cuerpo_de">&nbsp;</td>
</tr>
<tr>
<td class="abajo_iz" >&nbsp;</td>
<td class="abajo_ce" ></td>
<td class="abajo_de" >&nbsp;</td>
</tr>

</table>

<!-- formulario direcciones  --> 
<div id="dialog-form" title="Formulario de direcciones"></div>

</center>
		<!-- colaboracion en linea -->
		<div id="div-observaciones-tab"></div>
		<div id="dialog-form2" title="Colaboraci&oacute;n en lÃ­nea">
    		<p>Por favor diligencie este formulario para enviar comentarios, errores o falencias 
      		encontradas en el proceso. M&aacute;ximo 250 caracteres </p>
			<label>Tus comentarios:</label><br />
			<textarea name="notas" id="notas" cols="60" rows="10"></textarea>
		</div>
		 
		<!-- ayuda en linea -->
		<div id="ayuda" title="Manual .:. Nuevos Beneficiarios" style="background-image:url(../../imagenes/FondoGeneral0.png)">
        </div>
        
<div id="dialog-persona" title="Formulario Datos Basicos"></div>        

</body>
</html>