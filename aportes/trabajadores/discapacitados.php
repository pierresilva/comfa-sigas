<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'p.definiciones.class.php';
$objClase=new Definiciones();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>:: Discapacidad::</title>
<link rel="stylesheet" type="text/css" href="../../css/Estilos.css"  />
<link rel="stylesheet" type="text/css" href="../../css/estilo_tablas.css" />
<link type="text/css" href="../../css/formularios/base/ui.all.css" rel="stylesheet" />
<link href="../../css/marco.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../../js/jquery-1.4.2.js"></script>
<script type="text/javascript" src="../../js/formularios/ui/ui.core.js"></script>
<script type="text/javascript" src="../../js/formularios/ui/ui.dialog.js"></script>
<script type="text/javascript" src="../../js/formularios/ui/ui.datepicker.js"></script>
<script type="text/javascript" src="../../js/comunes.js"></script>
<script type="text/javascript" src="js/discapacitados.js"></script>

<script type="text/javascript">
shortcut.add("Shift+F",function() {
	var URL=src();
	var url=URL+"aportes/trabajadores/consultaTrabajador.php";
	window.open(url,"_blank");
},{
	'propagate' : true,
	'target' : document 
});        
</script>

</head>

<body>
<br /><br />
<table border="0" align="center" cellpadding="0" cellspacing="0" width="70%">
  <tbody>
 
  <!-- ESTILOS SUPERIOR TABLA-->
  <tr>
    <td width="13" height="29" class="arriba_iz">&nbsp;</td>
    <td class="arriba_ce"><span class="letrablanca">::&nbsp;Certificados Discapacitados&nbsp;::</span></td>
    <td width="13" class="arriba_de" align="right">&nbsp;</td>
  </tr>
  
  <!-- ESTILOS ICONOS TABLA-->
  <tr>
  <td class="cuerpo_iz">&nbsp;</td>
  <td class="cuerpo_ce">
  <img src="../../imagenes/menu/nuevo.png" alt="Nuevo" width="16" height="16" style="cursor:pointer" title="Nuevo" onclick="nuevoD()" />
  <img src="../../imagenes/spacer.gif" alt="" width="1" height="1" />
  <img src="../../imagenes/menu/grabar.png" alt="Guardar" width="16" height="16" style="cursor:pointer" title="Guardar" onclick="guardar();" id="bGuardar"/>  <img src="../../imagenes/spacer.gif" alt="" width="1" height="1" /> 
  <img src="../../imagenes/menu/imprimir.png" width="16" height="16" style="cursor:pointer" title="Imprimir" onclick="imprimir();" />
  <img src="../../imagenes/spacer.gif" alt="" width="1" height="1" />
  <img src="../../imagenes/menu/informacion.png" alt="Ayuda" width="16" height="16" style="border:none; cursor:pointer" title="Manual" onclick="mostrarAyuda();" />
  <img src="../../imagenes/menu/notas.png" alt="Soporte" width="16" height="16" style="cursor: pointer" title="Colaboraci&oacute;n en l&iacute;nea" onclick="notas();" />
       </td>
    <td class="cuerpo_de">&nbsp;</td>
  </tr>
  
  <!-- ESTILOS MEDIO TABLA-->
  <tr>
    <td class="cuerpo_iz">&nbsp;</td>
    <td class="cuerpo_ce">
   <table border="0" cellpadding="5" align="center" class="tablero" width="90%">
   <tr>
   	<td colspan="2">
   		<h3>Origen del certificado</h3>
   		<input type="radio" id="buscarPor_rad" name="buscarPor" value="radicacion" /> <label for="buscarPor_rad" style="font-size:10pt">Desde Radicaci&oacute;n</label><br/>
   		<input type="radio" id="buscarPor_form" name="buscarPor" value="formulario" /> <label for="buscarPor_form" style="font-size:10pt">Desde Formulario de trabajador</label>
   	</td>
   	<td id="td_radicaciones" colspan="2">
   		<label for="idradicacion" style="font-size:10pt">N&uacute;m. radidaci&oacute;n</label> 
   		<select name="idradicacion" id="idradicacion">
   			<option value="">Seleccione</option>
   		</select>
   	</td>
   </tr>
<tr>
<td>Tipo Documento</td>
<td>
	<select id="tipoDoc" class="box1">
    <?php
	$consulta= $objClase->mostrar_datos(1,1);
	while($row=mssql_fetch_array($consulta)){
	echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
	}
?>
    </select>
</td>
<td>N&uacute;mero</td>
<td><input id="identificacion" type="text" class="box1" onkeyup="validarCaracteresPermitidos(document.getElementById('tipoDoc').value,this);" onblur="validarLongNumIdent(document.getElementById('tipoDoc').value,this);buscarPersona(document.getElementById('tipoDoc').value,this.value)" /></td>
</tr>
<tr>
  <td>Nombre afiliado</td>
  <td colspan="3"><label for="nombreCompleto"></label>
    <input type="text" name="nombreCompleto" id="nombreCompleto"  class="boxlargo" disabled="disabled"/></td>
  </tr>
<tr>
  <td>Beneficiarios</td>
  <td colspan="3"><select id="beneficiarios" class="boxmediano"  onchange="buscarDiscapacidad();">
    	<option value="0" selected="selected">Seleccione...</option>
  	  </select>
  </td>
</tr>
<tr>
  <td>Fecha Cert. Discapacidad</td>
  <td colspan="3"><input id="fecha" type="text" class="boxfecha" /></td>
</tr>

</table>
    </td>
    <td class="cuerpo_de">&nbsp;</td>
  </tr>
  
  <!-- CONTENIDO TABLA-->
  <tr>
    <td class="cuerpo_iz">&nbsp;</td>
    <td class="cuerpo_ce">    
    </td>
    <td class="cuerpo_de">&nbsp;</td>
  </tr>
  
  <!-- ESTILOS PIE TABLA-->
  <tr>
    <td class="abajo_iz" >&nbsp;</td>
    <td class="abajo_ce" ></td>
    <td class="abajo_de" >&nbsp;</td>
  </tr>
  
</tbody>
</table>

<!-- colaboracion en linea -->
<div id="div-observaciones-tab"></div>
<div id="dialog-form2" title="Colaboraci&oacute;n en l&iacute;nea">
<p>Por favor diligencie este formulario para enviar comentarios, errores o falencias encontradas en el proceso. M&aacute;ximo 250 caracteres </p>
<label>Tus comentarios:</label><br />
<textarea name="notas" id="notas" cols="60", rows="10"></textarea>
</div>


<!-- Manual Ayuda -->
<div id="ayuda" title="::Manual Discapacitados::"></div>
<input id="idPersona" type="hidden"  />
</body>
</html>