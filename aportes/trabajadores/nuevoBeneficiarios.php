<?php

/* autor:       Andres F. Lara
 * fecha:       Septiembre 14 de 2011
 * objetivo:    Almacenar los beneficiarios radicados. 
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'p.definiciones.class.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'ciudades.class.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'funcionesComunes' . DIRECTORY_SEPARATOR . 'funcionesComunes.php';

$objCiudad =new Ciudades;
$objClase = new Definiciones;

$consulta = $objClase->mostrar_datos(1,2);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>:: Nuevo Beneficiarios ::</title>
<link type="text/css" rel="stylesheet" href="<?php echo URL_PORTAL; ?>newcss/marco.css" />
<link type="text/css" rel="stylesheet" href="<?php echo URL_PORTAL; ?>css/Estilos.css" />
<link type="text/css" rel="stylesheet" href="<?php echo URL_PORTAL; ?>css/css.1.8/jquery-ui-1.8.17.custom.css" />

<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/1.6/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/1.6/jquery-ui-1.8.16.custom.min.js"></script>

<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/comunes.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/direccion.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/llenarCampos.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/jquery.combos2.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/jquery.fechaLarga.js"></script>

<script type="text/javascript" src="js/nuevoBeneficiarios.js" ></script>

<script type="text/javascript">
shortcut.add("Shift+F",function() {
		var URL=src();
		var url=URL+"aportes/trabajadores/consultaTrabajador.php";
    	window.open(url,"_blank");
    },{
		'propagate' : true,
		'target' : document 
	});        
</script>

<script type="text/javascript">
function notas(){
			$("#dialog-form2").dialog('open');
		}	
function mostrarAyuda(){
			$("#ayuda").dialog('open' );
		}
function validarEspacio(e){
	tecla=(document.all) ? e.keyCode : e.which;
	if(tecla==32){
		return false;
	}
}
</script>
</head>

<body>
<center>
<table width="80%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="13" height="29" class="arriba_iz">&nbsp;</td>
<td class="arriba_ce"><span class="letrablanca">::&nbsp;Inscripción Nuevos Beneficiarios &nbsp;::</span></td>
<td width="13" class="arriba_de" align="right">&nbsp;</td>
</tr>
<tr>
<td class="cuerpo_iz">&nbsp;</td>                        
<td background="../../imagenes/tabla/centro.gif">
<img src="../../imagenes/tabla/spacer.gif" width="1" height="1"/> 
<img src="../../imagenes/menu/nuevo.png" title="Nuevo" width="16" height="16" style="cursor:pointer" onClick="nuevoB()"/> 
<img src="../../imagenes/spacer.gif" width="1" height="1"/> <img src="../../imagenes/menu/grabar.png" title="Guardar" width="16" height="16" onClick="guardarBeneficiario()" style="cursor:pointer" id="bGuardar"/> 
<img src="../../imagenes/spacer.gif" width="1" height="1"/><img src="../../imagenes/spacer.gif" width="1" height="1"/>
<a href="../../help/aportes/nuevosBeneficiarios.html" target="_blank" onClick="window.open(this.href, this.target, 'width=800,height=550,titlebar=0, resizable=no'); return false;" > 
<img src="../../imagenes/menu/informacion.png" width="16" height="16" style="border:none" title="Manual" />
</a> 
<img src="../../imagenes/spacer.gif" width="1" height="1"/> 
<img src="../../imagenes/menu/notas.png" width="16" height="16" style="cursor: pointer" title="ColaboraciÃ³n en LÃ­nea" onclick="notas();" /> 
</td>
<td class="cuerpo_de">&nbsp;</td>
</tr>
<tr>
<td class="cuerpo_iz">&nbsp;</td>
<td class="cuerpo_ce"><div id="resultado" style="color:#FF0000"></div></td>
<td class="cuerpo_de">&nbsp;</td>
</tr>
<tr>
<td class="cuerpo_iz">&nbsp;</td>
<td class="cuerpo_ce">
<table width="97%" border="0" align="center" cellspacing="0" class="tablero">
	<tr>
  		<td width="16%" align="left">Id Radicacion</td>
  		<td width="34%" align="left">
  			<select name="pendientes" id="pendientes" class="box1" onchange="buscarRadicacion();">
  				<option value="0" selected="selected">Seleccione</option>                                    
  			</select>
  		</td>
  		<td width="14%" align="left">Fecha </td>
  		<td width="36%" align="left"><input name="fecha" class="box1" id="fecha" readonly="readonly" /></td>
  	</tr>
  	<tr>
    	<th colspan="4" align="center">Información Afiliado</th>
    </tr>
  	<tr>
  		<td align="left">Tipo Identificaci&oacute;n</td>
  		<td  align="left">
  			<select name="tipoI" class="box1" id="tipoI" disabled="disabled" >
  			<?php
  				$consulta = $objClase->mostrar_datos(1,2);
  				while($row=mssql_fetch_array($consulta)){
    				echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
  				} 
  			?>
  			</select> <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
  		</td>
  		<td  align="left">N&uacute;mero</td>
  		<td  align="left">
  			<input name="numero" class="box1" id="numero" readonly="readonly" />
  			 <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
  		</td>
  	</tr>
  	<tr>
  		<td align="left">Afiliado <input type="hidden" id="idT" /></td>
  		<td colspan="3" align="left" id="nombreT"></td>
  	</tr>
  	<tr>
  		<td align="left">Fecha Afiliaci&oacute;n</td>
  		<td align="left" id="fafilia">&nbsp;</td>
  		<td align="left">Estado</td>
  		<td align="left" id="estado">&nbsp;</td>
  	</tr>
  	<tr>
  		<td align="left">Salario</td>
  		<td align="left" id="salario">&nbsp;</td>
  		<td align="left">Raz. Social</td>
  		<td align="left" id="rzsocial">&nbsp;</td>
  	</tr>
  	<tr>
  		<th colspan="4" align="center">Información Beneficiario</th>
  	</tr>
  	<tr>
	    <td>Parentesco</td>
	    <td>
	    	<select name="parentesco" class="box1" id="parentesco" onchange="validarComboParentesco();">
      			<option selected="selected" value="0">Seleccione..</option>
      			<option value="35">Hijo</option>
      			<option value="38">Hijastro</option>
      			<option value="36">Padre/Madre</option>
      			<option value="37">Hermano(a)</option>
    		</select> <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
    	</td>
    	<td>C&eacute;dula Padre &oacute; Madre</td>
    	<td>
    		<select name="cedMama" id="cedMama" class="box1" >
      			<option value="undefined" selected="selected">Seleccione.. </option>
      			<option value="0" name="0" >HIJO NATURAL </option>
      			<option value="otro" >Otro.. </option>	        
    		</select>&nbsp;<input id="nuevaRelacion" type="button" value="Crear Nueva Relaci&oacute;n" disabled="disabled"></input>
    	</td>
	</tr>
	<tr id="padreBiologico" style="display:none" >
    	<td>Tipo Id. Padre Biol&oacute;gico</td>
    	<td>
    		<select name="tipoIdPadreBiol" class="box1" id="tipoIdPadreBiol">
    			<option selected="selected" value="0">Seleccione..</option>
			<?php
				$consulta=$objClase->mostrar_datos(1, 2);
				while($row=mssql_fetch_array($consulta)){
					echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
				}
			?>
    		</select> <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
    	</td>
    	<td>N&uacute;mero Padre Biol&oacute;gico</td>
    	<td>
    		<input name="IdPadreBiol" type="text" class="box1" id="IdPadreBiol" onblur="buscarPersonaMostrar($('#tipoIdPadreBiol'),$('#IdPadreBiol'),$('#hideIdbiol'),$('#nomBiologico'));" />
    		 <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
    		 <input type="hidden" name="hideIdbiol" id="hideIdbiol" />
    	</td>    	
	</tr>
	<tr id="nombreBiologico" style="display:none" >
  		<td>Padre Biológico</td>
  		<td colspan="3" id="nomBiologico">&nbsp;</td>
	</tr>
	<tr>  
		<td>Tipo Afiliaci&oacute;n</td>
  		<td>
  			<select name="tipoAfiliacion3" class="box1" id="tipoAfiliacion3" onchange="validarTipoFormulario();" >
  				<option selected="selected" value="0">Seleccione..</option>
  				<option value="48">Subsidio</option>
  				<option value="49">Servicios</option>
  			</select> <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
  		</td>
  		<td>Capacidad de trabajo</td>
  		<td>
  			<select name="capTrabajo" class="box1" id="capTrabajo" onchange="validarEdad(0);" >
    			<option value="N">Normal</option>
    			<option value="I">Discapacitado</option>
  			</select> <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
  		</td>    	
	</tr>
	<tr>
    	<td>Tipo Documento</td>
      	<td>
      		<select name="tipoDoc3" class="box1" id="tipoDoc3" onchange="$('#identificacion3').val('').trigger('blur');" >
        		<option selected="selected" value="0">Seleccione..</option>
        	<?php
				$consulta=$objClase->mostrar_datos(1, 2);
				while($row=mssql_fetch_array($consulta)){
					echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
				}
			?>
      		</select> <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
      	</td>
      	<td>No. Identififcaci&oacute;n</td>
      	<td>
      		<input name="identificacion3" type="text" class="box1" id="identificacion3" onblur="buscarPersonaTres(this);" onkeyup="validarCaracteresPermitidos(document.getElementById('tipoDoc3').value,this);" />
      		 <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
      	</td>
    </tr>
	<tr>
  		<td>P. Nombre Beneficiario</td>
  		<td>
  			<input name="pNombre3" type="text" class="box1" id="pNombre3"  onkeyup="sololetras(this)" onkeypress="return validarEspacio(event)" onchange="buscarPersonaExistente($('#identificacion3').val(),$('#pNombre3').val(),$('#sNombre3').val(),$('#pApellido3').val(),$('#sApellido3').val(),true,existe)" />
  			 <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
  		</td>
  		<td>S. Nombre Beneficiario</td>
  		<td><input name="sNombre3" type="text" class="box1" id="sNombre3" onkeyup="sololetras(this)" onchange="buscarPersonaExistente($('#identificacion3').val(),$('#pNombre3').val(),$('#sNombre3').val(),$('#pApellido3').val(),$('#sApellido3').val(),true,existe)" /></td>
	</tr>
	<tr>
		<td>P. Apellido Beneficiario</td>
		<td>
			<input name="pApellido3" type="text" class="box1" id="pApellido3"  onkeyup="sololetras(this)" onkeypress="return validarEspacio(event)" onchange="buscarPersonaExistente($('#identificacion3').val(),$('#pNombre3').val(),$('#sNombre3').val(),$('#pApellido3').val(),$('#sApellido3').val(),true,existe)" />
			 <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
		</td>
		<td>S. Apellido Beneficiario</td>
		<td><input name="sApellido3" type="text" class="box1" id="sApellido3" onkeyup="sololetras(this)" onchange="buscarPersonaExistente($('#identificacion3').val(),$('#pNombre3').val(),$('#sNombre3').val(),$('#pApellido3').val(),$('#sApellido3').val(),true,existe)" /></td>
	</tr>
	<tr>
		<td>Fecha de Nacimiento</td>
		<td>
			<input name="fecNacHide3" type="text" class="box1" id="fecNacHide3" readonly="readonly" onchange="validarEdad(0);"/>
			 <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />			
		</td>
		<td>Pais Nace</td>
  		<td>
  			<select name="cboPaisB" class="box1" id="cboPaisB"  >
    			<option value="0">Seleccione..</option>
    		<?php
				$consulta=$objCiudad->paises();
				while($row=mssql_fetch_array($consulta)){
			?>
    			<option value="<?php echo $row["idpais"]; ?>"><?php echo $row["pais"]?></option>
    		<?php
				}
			?>
  			</select> <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
  		</td>
	</tr>
	<tr>
  		<td>Dep Nace</td>
  		<td>
  			<select name="cboDeptoB" class="box1" id="cboDeptoB"  >
  			</select> <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
  		</td>
  		<td>Ciudad Nace</td>
  		<td>
  			<select name="cboCiudadB" class="box1" id="cboCiudadB">
			</select> <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
		</td>
	</tr>	
	<tr>
		<td>Sexo</td>
		<td>
			<select name="sexo3" class="box1" id="sexo3">
				<option selected="selected" value="0">Seleccione..</option>
				<option value="M">Masculino</option>
				<option value="F">Femenino</option>
			</select> <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
		</td>
		<td><label id="lblAsignacion" style="display:none" >Fecha Asignación</label></td>
		<td>
			<div id="divAsignacion" style="display:none" >
				<input type="text" class="box1" name="fAsignacion" id="fAsignacion" readonly="readonly" onchange="$('#pInicialV3,#pFinalV3').val(''); $('#vigencia1,#vigencia2').attr('checked',false); " />
			 	 <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
			 </div>
		</td>
	</tr>
	<tr>
  		<td colspan="4" style="text-align:center">
  			<input type="hidden" name="estadoCivil3" id="estadoCivil3" value="50" />
  			<label style="font-weight:bold; color:#F00; text-align:center">Si anexa certificados Grabelos!</label>
  		</td>
  	</tr>	
	<tr id="filaCertificados" >
  		<td colspan="4">
	  		<table width="100%" border="0" >
	  			<tr id="filaEscolaridad" style="display:none">
					<td width="25%" >Certificado Escolaridad</td>
			  		<td width="25%" ><input type="radio" name="certificado" id="cEscolaridad" onchange="setFechaAsignaCert($('#cEscolaridad'),$('#fEscolaridad'))" /></td>
			  		<td width="25%" >Fecha aplica</td>
			  		<td width="25%" ><input type="text" class="box1" id="fEscolaridad" name="fCert" disabled="disabled" /></td>
				</tr>
				<tr id="filaUniversidad" style="display:none">
			  		<td>Certificado Universidad</td>
			  		<td><input type="radio" name="certificado" id="cUniversidad" onchange="setFechaAsignaCert($('#cUniversidad'),$('#fUniversidad'))" /></td>
			  		<td>fecha aplica</td>
			  		<td><input type="text" class="box1" id="fUniversidad" name="fCert" disabled="disabled"/></td>
				</tr>				
				<tr id="filaDiscapacidad" style="display:none">
			  		<td>Certficado Discapacidad</td>
			  		<td><input type="checkbox" name="cDiscapacidad" id="cDiscapacidad" onchange="setFechaAsignaCert($('#cDiscapacidad'),$('#fDiscapacidad'))" /></td>
			  		<td>Fecha aplica</td>
			  		<td><input name="fCert" type="text" class="box1" id="fDiscapacidad" disabled="disabled" /></td>
				</tr>
				<tr id="filaSupervivencia" style="display:none">
			  		<td>Certificado Supervivencia</td>
			  		<td><input type="checkbox" name="cSupervivencia" id="cSupervivencia" onchange="setFechaAsignaCert($('#cSupervivencia'),$('#fSupervivencia'))" /></td>
			  		<td>Fecha aplica</td>
			  		<td><input name="fCert" type="text" class="box1" id="fSupervivencia" disabled="disabled" /></td>
				</tr>	
	  		</table>
	  	</td>
  	</tr>
	<tr>
		<td colspan="4">
        	<div id="cerVigencia" style="display:none">
        		<table width="100%" border="0">
        			<tr>    
      					<td><center>A&ntilde;o Vigencia</center></td>
      					<td>
       						<input type="radio" name="vigencia" id="vigencia1" value="0" onClick="vVigencia(0)">
       						<label id="l1" for="vigencia1" >Periodo Actual</label><br/>
      						<input type="radio" name="vigencia" id="vigencia2" value="1" onClick="vVigencia(1)"> 
      						<label id="l2" for="vigencia2" >Periodo Anterior</label><br/>
      					</td>
      					<td colspan="2" >
      						<center>
	      						Periodo Inicial
	        					<input name="pInicialV3" type="text" class="box1 monthPicker" id="pInicialV3" value="" onBlur="validarVigencia(this.value,1); validarPeriodosDatePicker(this.id,'pFinalV3','errorVig'); " disabled="disabled" maxlength="6">
	        					Periodo Final
	        					<input name="pFinalV3" type="text" class="box1 monthPickerF" id="pFinalV3" value="" onBlur="validarVigencia(this.value,2); validarPeriodosDatePicker('pInicialV3',this.id,'errorVig');" disabled="disabled" maxlength="6">	        					
	        					<span class="Rojo" id="errorVig"></span>
        					</center>
        				</td>        
    				</tr>
        		</table>
       		</div>
       </td>	
	</tr>	
</table>
</td>
<td class="cuerpo_de">&nbsp;</td>
</tr>
<tr>
<td class="abajo_iz" >&nbsp;</td>
<td class="abajo_ce" ></td>
<td class="abajo_de" >&nbsp;</td>
</tr>

</table>

<input type="hidden" id="txtDiferenciaFechaIngreso" />
<input type="hidden" id="txtFechaIngresoM" />
<input type="hidden" id="txtHiddenSexo" />
<input type="hidden" id="idPersona" />
</center>

<!-- VENTANA CONYUGE DIALOG style="display:none;" -->
<div id="div-conyuge" title="::FORMULARIO NUEVO CONYUGE::" style="display:none">
	<h4 align="left"> DATOS DEL CONYUGE - Nueva relación</h4>
	<table width="100%"  border="0" cellspacing="0" class="tablero">		
		<tr>
			<td width="25%">Tipo Documento</td>
			<td width="25%">
				<select name="tipoDoc2" class="box1" id="tipoDoc2" onchange="$('#identificacion2').val('').trigger('blur');">
					<option value="0">Seleccione..</option>
				<?php
					$consulta=$objClase->mostrar_datos(1, 2);
					while($row=mssql_fetch_array($consulta)){
						echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
					}
				?>
				</select> <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
			</td>
			<td width="25%">N&uacute;mero Documento</td>
			<td width="25%">
				<input name="identificacion2" type="text" class="box1" id="identificacion2" onblur="buscarConyugeNuevo(this);" onkeyup="validarCaracteresPermitidos(document.getElementById('tipoDoc2').value,this);" 
					onkeydown="validarCaracteresPermitidos(document.getElementById('tipoDoc2').value,this);" onkeypress="validarCaracteresPermitidos(document.getElementById('tipoDoc2').value,this);" />
				 <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
			</td>
		</tr>         
		<tr>
			<td>Primer Nombre</td>
			<td class="box1">
				<input name="pNombre2" type="text" class="box1" id="pNombre2" onkeypress="return vpnombre(event)" onchange="buscarPersonaExistente($('#identificacion2').val(),$('#pNombre2').val(),$('#sNombre2').val(),$('#pApellido2').val(),$('#sApellido2').val(),true,idcony)" />
				 <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
			</td>
			<td>Segundo Nombre</td>
			<td>
				<input name="sNombre2" type="text" class="box1" id="sNombre2" onkeypress="return vsnombre(event)" onchange="buscarPersonaExistente($('#identificacion2').val(),$('#pNombre2').val(),$('#sNombre2').val(),$('#pApellido2').val(),$('#sApellido2').val(),true,idcony)" />
			</td>
		</tr>
		<tr>
			<td>Primer Apellido</td>
			<td>
				<input name="pApellido2" type="text" class="box1" id="pApellido2" onkeypress="return vpnombre(event)" onchange="buscarPersonaExistente($('#identificacion2').val(),$('#pNombre2').val(),$('#sNombre2').val(),$('#pApellido2').val(),$('#sApellido2').val(),true,idcony)" />
				 <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
			</td>
			<td>Segundo Apellido</td>
			<td><input name="sApellido2" type="text" class="box1" id="sApellido2" onkeypress="return vsnombre(event)" onchange="buscarPersonaExistente($('#identificacion2').val(),$('#pNombre2').val(),$('#sNombre2').val(),$('#pApellido2').val(),$('#sApellido2').val(),true,idcony)" /></td>
		</tr>
		<tr>
			<td width="25%">Tipo de relaci&oacute;n</td>
			<td width="25%">
				<select name="tipRel" class="box1" id="tipRel">
					<option selected="selected" value="0">Seleccione..</option>
					<option value="2147">CONYUGE</option>
					<option value="2148">COMPA&Ntilde;ERO(A)</option>
				</select> <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
			</td>
			<td width="25%">Conviven</td>
			<td width="25%">
				<select name="conviven" class="box1" id="conviven">
					<option value="0">Seleccione..</option>
					<option value="S">SI</option>
					<option value="N">NO</option>
				</select> <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
			</td>
		</tr>
		<tr>
			<td>G&eacute;nero</td>
			<td>
				<select name="sexo2" class="box1" id="sexo2">
					<option selected="selected" value="0">Seleccione..</option>
					<option value="M">Masculino</option>
					<option value="F">Femenino</option>
				</select> <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
			</td>
			<td>Tipo Vivienda</td>
			<td>
				<select name="tipoVivienda2" class="box1" id="tipoVivienda2">
  					<option value="0">Seleccione..</option>
  					<option value="U">Urbana</option>
  					<option value="R">Rural</option>
				</select>
			</td>
		</tr>		
		<tr>
  			<td>Departamento Residencia</td>
  			<td>
  				<select name="cboDeptoC" class="box1" id="cboDeptoC">
    				<option value="0">Seleccione..</option>
    			<?php
					$consulta=$objCiudad->departamentos();
					while($row=mssql_fetch_array($consulta)){
				?>
    				<option value="<?php echo $row["coddepartamento"]; ?>"><?php echo utf8_encode($row["departmento"]); ?></option>
    			<?php } ?>
  				</select>
  			</td>
  			<td>Ciudad Residencia</td>
  			<td>
  				<select name="cboCiudadC" class="box1" id="cboCiudadC">
  					<option value="0">Seleccione..</option>
  				</select>
  			</td>
		</tr>
		<tr>
  			<td>Zona</td>
  			<td>
  				<select name="cboZonaC" class="box1" id="cboZonaC">
  					<option value="0">Seleccione..</option>
  				</select>
  			</td>
  			<td>Barrio</td>
  			<td>
  				<select name="cboBarrioC" class="box1" id="cboBarrioC">
    				<option value="0">Seleccione..</option>
  				</select>
  			</td>
		</tr>
		<tr>
			<td>Direcci&oacute;n </td>
			<td>
				<input name="direccion2" type="text" class="box1" id="direccion2" onfocus="direccion(this,document.getElementById('cboBarrioC'));" />
			</td>
			<td>E-mail</td>
			<td><input name="email2" type="text" class="box1" id="email2" /></td>
		</tr>
		<tr>
			<td>Tel&eacute;fono</td>
			<td><input name="telefono2" type="text" class="box1" id="telefono2" maxlength="7" /></td>
			<td>Celular</td>
			<td><input name="celular2" type="text" class="box1" id="celular2" maxlength="10" /></td>
		</tr>
		<tr>
			<td>Fecha De Nacimiento</td>
			<td><input type="text" id="alternate2" readonly="readonly"/>
			<td>Pais Nacimiento</td>
			<td>
				<select name="cboPaisC2" class="box1" id="cboPaisC2">
					<option value="0">Seleccione</option>
				<?php
					$consulta=$objCiudad->paises();
					while($row=mssql_fetch_array($consulta)){
				?>          
					<option value="<?php echo $row["idpais"]; ?>"><?php echo utf8_encode($row["pais"]); ?></option>
				<?php } ?>
				</select>
			</td>
		</tr>
		<tr>
			<td>Departamento Nacimiento</td>
			<td>
				<select name="cboDeptoC2" class="box1" id="cboDeptoC2">
					<option value="0">Seleccione</option>
				</select>
			</td>
			<td>Ciudad Nacimiento</td>
			<td>
				<select name="cboCiudadC2" class="box1" id="cboCiudadC2">
					<option value="0">Seleccione</option>
				</select>
			</td>
		</tr>		
		<tr id="trInformacionAfiliacion" >
			<td>Información Afiliación</td>
			<td colspan="3">
				<select name="estadoCivil2" class="box1" id="estadoCivil2" style="display:none">
  				<?php
					$consulta=$objClase->mostrar_datos(10, 3);
					while($row=mssql_fetch_array($consulta)){
						echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
					}
				?>
				</select>
			</td>
		</tr>
		<tr id="trInformacionAfiliacion2">
  			<td>Nit</td>
  			<td><input name="nitConyuge" type="text" class="box1" id="nitConyuge" readonly="readonly" /></td>
  			<td>Empresa donde trabaja</td>
  			<td><input name="empCony" type="text" class="box1" id="empCony" readonly="readonly" /></td>
		</tr>
		<tr id="trInformacionAfiliacion3">
			<td>Salario </td>
			<td><input name="salCony" type="text" class="box1" id="salCony" readonly="readonly" /></td>
			<td>&iquest;Subsidio?</td>
			<td><input name="subCony"  type="text" class="box1" id="subCony" readonly="readonly" /></td>
		</tr>		
	</table>
	<div style="display:none">
		<input type="hidden" name="capTrabajo2" id="capTrabajo2" value="N" />
	</div>
	<div id="div-relacion"></div>
</div>

<!-- formulario direcciones  --> 
<div id="dialog-form" title="Formulario de direcciones"></div>

<!-- colaboracion en linea -->
		<div id="div-observaciones-tab"></div>
		<div id="dialog-form2" title="Colaboraci&oacute;n en lÃ­nea">
    		<p>Por favor diligencie este formulario para enviar comentarios, errores o falencias 
      		encontradas en el proceso. M&aacute;ximo 250 caracteres </p>
			<label>Tus comentarios:</label><br />
			<textarea name="notas" id="notas" cols="60" rows="10"></textarea>
		</div>
		 
		<!-- ayuda en linea -->
<div id="ayuda" title="Manual .:. Nuevos Beneficiarios" style="background-image:url(../../imagenes/FondoGeneral0.png)">
        </div>
        
<div id="dialog-persona" title="Formulario Datos Basicos" style="display:none">
<center>
<table width="578" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff" class="tablero">
	<tbody>
		<tr bgcolor="#EBEBEB">
		  <td colspan="4" style="text-align:center" >Datos B&aacute;sicos de la persona</td>
	  </tr>
		<tr>
		  <td >Tipo Documento</td>
		  <td ><select name="tDocumento" id="tDocumento" class="box1" onfocus="javascript:this.blur(); return false;">
          <option value="0" >Seleccione</option>
<?php
		$consulta=$objClase->mostrar_datos(1, 2);
		while($row=mssql_fetch_array($consulta)){
		echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
	}
?>
          </select>
          </td>
		  <td >N&uacute;mero</td>
		  <td ><input name="txtNumeroP" id="txtNumeroP" type="text" class="box1" readonly="readonly"></td>
	  </tr>
		<tr>
		  <td >Primer Nombre</td>
		  <td ><input name="pNombre" id="pNombre" type="text" class="box1"></td>
		  <td >Segundo Nombre</td>
		  <td ><input name="sNombre" id="sNombre" type="text" class="box1"></td>
	  </tr>
		<tr>
		  <td >Primer Apellido</td>
		  <td ><input name="pApellido" id="pApellido" type="text" class="box1"></td>
		  <td >Segundo Apellido</td>
		  <td ><input name="sApellido" id="sApellido" type="text" class="box1"></td>
	  </tr>
		<tr>
		  <td >&nbsp;</td>
		  <td >&nbsp;</td>
		  <td >&nbsp;</td>
		  <td >&nbsp;</td>
	  </tr>
		<tr>
			<td >&nbsp;</td>
			<td >&nbsp;</td>
			<td >&nbsp;</td>
			<td >&nbsp;</td>
		</tr>
</tbody>
</table>
</center>
</div>

<div id="divRelacionesActivas" title="Relaciones Activas" style="display:none; padding-first:40px" >
	<table width="578" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff" class="tablero">
		<tr>				
			<th class="head">TD</th>
			<th class="head">Doc</th>
			<th class="head">Nombre</th>
			<th class="head">Parentesco</th>				
			<th class="head">Conviven</th>
		</tr>
		<tbody id="tbRelacionesActivas" ></tbody>
	</table>	
</div>

<div id="divDocumentoDoble" title="Datos Persona" style="display:none; padding-first:40px" >
	<table width="460" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff" class="tablero">
		<tr>
			<th class="head">TD</th>				
			<th class="head">Doc</th>
			<th class="head">Nombre</th>
		</tr>
		<tbody id="tbDocumentoDoble" ></tbody>
	</table>	
</div>

</body>
</html>