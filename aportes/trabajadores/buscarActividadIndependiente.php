<?php
/* autor:       Andres Lara
 * fecha:       13/04/2016
* objetivo:     Buscar La actividad economica del independiente
*/

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

require_once('clases/trabajadores.class.php');
$objClase = new Trabajador();

$consulta=$objClase->buscar_actividad_independiente($_REQUEST['idpersona'],$_REQUEST['idradicacion']);


if ($consulta  == true){
	$row=mssql_fetch_array($consulta);
	
	echo json_encode($row);
}else{
	echo false;
}


?>