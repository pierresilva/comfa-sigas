<?php
set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'p.aportes.class.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';


//Clases para devoluciones 
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejaInforma.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';

$db = IFXDbManejaInforma::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}




$ide=(empty($_REQUEST['v0'])) ? 'NULL' : $_REQUEST['v0'];
$aportante=$_REQUEST['aportante'];

$objAportes=new Aportes();
$result=$objAportes->buscar_aportes($ide,$aportante);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Aportes</title>
<link href="<?php echo URL_PORTAL; ?>css/sorterStyle.css" rel="stylesheet" type="text/css" media="screen" />
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/tableSorterDESC.js"></script>
<script type="text/javascript">		
	var sorterAportesTrab = new TINYDESC.tableDESC.sorter("sorterAportesTrab");
	sorterAportesTrab.head = "head";
	sorterAportesTrab.asc = "asc";
	sorterAportesTrab.desc = "desc";
	sorterAportesTrab.even = "evenrow";
	sorterAportesTrab.odd = "oddrow";
	sorterAportesTrab.evensel = "evenselected";
	sorterAportesTrab.oddsel = "oddselected";
	sorterAportesTrab.paginate = true;
	sorterAportesTrab.pagesize = (5);
	sorterAportesTrab.currentid = "currentpageAportesTrab";
	sorterAportesTrab.limitid = "pagelimitAportesTrab";
	sorterAportesTrab.init("tblAportesTrab",2);

	//METODO PARA MOSTRAR LA OBSERVACION DEL APORTES
	function mostrarObservacion(id){
		
		$("#div-observaciones-aporte").dialog({
			autoOpen: true,
			width: 700,
			modal: false,
			title:"Observaci\u00F3n Aporte",
			open:function(evt, ui) {
				$( this ).html( $ ("#"+id ).html() );
			},
			buttons: {
				'Aceptar': function(){
					$( this ).dialog("destroy");
				}
			},
			close:function(){
				$( this ).dialog("destroy");
				$( this ).html("");
			}
		}); 
	}
</script>
</head>
<body >

	<h4>Aportes <?php echo $ide; //$idec ????? ?></h4>
		<table width="100%" border="0" cellspacing="0" class="sortable hover" id="tblAportesTrab">
			<caption style="text-align:left; padding:5px; font-size:11px;"><span></span></caption>
			<thead>
				<tr>
					<th class="head"><h3>NIT</h3></th>
					<th class="head"><h3>Comprobante</h3></th>
					<th class="head"><h3>No. Recibo</h3></th>
					<th class="head"><h3>Indice %</h3></th>
					<th class="head"><h3>Periodo</h3></th>
					<th class="head"><h3>Nómina</h3></th>
					<th class="head"><h3>Aporte</h3></th>
					<th class="head"><h3>Trabajadores</h3></th>
					<th class="head"><h3>Vr. Dev.</h3></th>
					<th class="head"><h3>Ajuste</h3></th>
					<th class="head"><h3>Fecha pago</h3></th>
					<th class="head"><h3>Tipo Aportante</h3></th>
					<th class="head"><h3>Observ.</h3></th>
				</tr>
			</thead>
			<tbody>
				<?php
					$cont=0;
					while($consulta=mssql_fetch_array($result)){
						$cont++;
						
						
						/**************************************
						 * 			 DEVOLUCIONES			  *
						 **************************************/
						
						$valorDev = 0;
						$sqlDev = "SELECT (aaportes-daportes) as valordevolucion, motivo, fechadevolucion FROM aportes058 WHERE idaporte=".$consulta["idaporte"];
						$rsDev = $db->querySimple($sqlDev);
						$b = 0;
						$motivo = "";
						while( ($rowDev = $rsDev->fetch() ) == true ){
							$b++;
							$valorDev += intval( $rowDev["valordevolucion"] );
							$motivo .= "<tr><td style='text-align:right'>$b</td><td>".$rowDev["motivo"]."</td><td>".$rowDev["fechadevolucion"]."</td></tr>";
						}
						
						/**************************************
						 * 		 TRASLADOS DE APORTES		  *
						***************************************/
						
						$rsNota = $objAportes->consultar_notas($consulta["idaporte"]);
						$b = 0;
						$notas = "";
						$observacionTraslado = "";
						while( $rsNot = $rsNota->fetch()){
							if($rsNot["tipo"]=="G" || $rsNot["tipo"]=="M"){
								$b++;
								$notas .= "<tr><td style='text-align:right'>$b</td><td>".$rsNot["notas"]."</td></tr>";
							}else if($rsNot["tipo"]=="T"){
								$observacionTraslado .= "<tr><td>{$rsNot["notas"]}</td><td>{$consulta["fechatraslado"]}</td><td>{$consulta["usuariotraslado"]}</td></tr>";
							}
						}
						
						/**************************************
						 * 	   INTERRUPCIONES DE APORTES	  *
						***************************************/
						
						$rsInterrupcion = $db->querySimple("SELECT a37.observaciones, a37.usuario, a37.fechasistema FROM aportes037 a37 INNER JOIN aportes038 a38 ON a38.idinterrupcion=a37.idinterrupcion
								WHERE a37.idempresa=$ide AND a38.periodo='{$consulta['periodo']}'");
						
								$observacionInterrupcion = "";
								while( $rowInterrupcion = $rsInterrupcion->fetch() ) {
									$observacionInterrupcion .= "<tr><td>".$rowInterrupcion["observaciones"]."</td><td>".$rowInterrupcion["fechasistema"]."</td></tr>";
								}
				?>	
					<tr>
						<td><?php 
								if($aportante=="E"):
									echo $consulta['nit'];
								else:
									echo $consulta['identificacion'];
								endif;
								  
								
							?></td>
					   	<td><?php echo $consulta['comprobante'];  ?></td>
					   	<td><?php echo $consulta['numerorecibo'];  ?></td>
					   	<td><?php echo $consulta['indicador'];  ?></td>
					    <td style='text-align:center'><?php echo $consulta['periodo'];  ?></td>
					    <td style='text-align:right'><?php echo number_format($consulta['valornomina']);  ?></td>
					    <td style='text-align:right'><?php echo number_format($consulta['valoraporte']);  ?></td>
					    <td style='text-align:right'><?php echo $consulta['trabajadores'];  ?></td>
					    <td style="text-align:right"><?php echo number_format($valorDev); ?>&nbsp;</td>
					    <td style='text-align:center'><?php echo $consulta['ajuste'];  ?></td>
					    <td><?php echo $consulta['fechapago'];  ?></td>
					    <td align="center">
					    	<?php 
					    		switch($consulta['aportante']){
					    			
									case "I": $tipoAportante ="Independiente"; break;
									case "P": $tipoAportante ="Pensionado"; break;
									default: $tipoAportante  ="Empresa";
									
					    		} 
					    		
					    		echo $tipoAportante;
					    	?>
					    </td>
					    <td style="text-align: center;">
				    	<?php 
				    		if( $notas != "" || $motivo != "" || $observacionInterrupcion != "" || $observacionTraslado != "")
				    			echo "<img src='../../imagenes/menu/informacion.png' width='14' height='14' style='cursor:pointer' onclick='mostrarObservacion(\"tdObservacion$cont\");' />";
				    	?>
    					</td>
    					
    					<!-- Modal de Observaciones Aportes -->
    					<td style="display: none; "id="tdObservacion<?php echo $cont?>">
				    	<center>
				    		
				    		<h3>Observaciones Registro de Aportes</h3>
				    		<table class="tablero" width="80%">
				    			<thead>
						    		<tr>
						    			<th width="5%">Cont</th><th width="90%">Observaci&oacute;n</th>
						    		</tr>
						    	</thead>
					    		<tbody><?php echo $notas; ?></tbody>
				    		</table>
				    		<!-- ### Motivo -->
				    		<h3>Motivo Devoluci&oacute;n de Aportes</h3>
				    		<table class="tablero" width="80%">
				    			<thead>
						    		<tr>
						    			<th width="5%">Cont</th><th width="90%">Motivo</th><th>Fecha</th>
						    		</tr>
						    	</thead>
					    		<tbody><?php echo $motivo; ?></tbody>
				    		</table>
				    		<!-- ### Observacion Traslados -->
				    		<h3>Observaciones de los Traslados</h3>
				    		<table class="tablero" width="80%">
				    			<thead>
						    		<tr>
						    			<th width="90%">Observaci&oacute;n</th><th>Fecha Traslado</th><th>Usuario Traslado</th>
						    		</tr>
						    	</thead>
					    		<tbody><?php echo $observacionTraslado; ?></tbody>
				    		</table>
				    		<!-- ### Observacion Interrupcion -->
				    		<h3>Observaciones de las Interrupciones</h3>
				    		<table class="tablero" width="80%">
				    			<thead>
						    		<tr>
						    			<th width="90%">Observaci&oacute;n</th><th>Fecha</th>
						    		</tr>
						    	</thead>
					    		<tbody><?php echo $observacionInterrupcion; ?></tbody>
				    		</table>
				    	</center>	
    </td>
    					
					</tr>
				<?php 	
					}
					if($cont==0){ ?>
						<script type="text/javascript">	
							MENSAJE("NO existen aportes.");
						</script>
					<?php }					
				?>
			</tbody>
		</table>
		<div id="controls">	
			<div id="perpage">
				<select onChange="sorterAportesTrab.size(this.value)">
					<option value="5" selected="selected">5</option>
					<option value="10" >10</option>
					<option value="20">20</option>
					<option value="50">50</option>
					<option value="100">100</option>
					<option value="500">500</option>
				</select>
				<label>&nbsp;&nbsp;&nbsp;&nbsp;Registros Por P&aacute;gina</label>
			</div>
			<div id="navigation">
				<img src="<?php echo URL_PORTAL; ?>imagenes/imagesSorter/first.gif" width="16" height="16" alt="first Page" onClick="sorterAportesTrab.move(-1,true)" />
				<img src="<?php echo URL_PORTAL; ?>imagenes/imagesSorter/previous.gif" width="16" height="16" alt="first Page" onClick="sorterAportesTrab.move(-1)" />
				<img src="<?php echo URL_PORTAL; ?>imagenes/imagesSorter/next.gif" width="16" height="16" alt="first Page" onClick="sorterAportesTrab.move(1)" />
				<img src="<?php echo URL_PORTAL; ?>imagenes/imagesSorter/last.gif" width="16" height="16" alt="Last Page" onClick="sorterAportesTrab.move(1,true)" />
			</div>
			<div id="text">&nbsp;&nbsp;&nbsp;&nbsp;P&aacute;gina <label id="currentpageAportesTrab"></label> de <label id="pagelimitAportesTrab"></label></div>	
		</div>
		       
		<input type="hidden" name="txtIde" id="txtIde" value="<?php echo $ide; ?>" />
		
		
		<div id="div-observaciones-aporte"></div>
</body>
</html>