<?php
set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'p.planilla.unica.class.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';
$idp=$_REQUEST['v0']; 

$objPlanilla=new Planilla();
$result=$objPlanilla->buscar_planillas_consulta($idp);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>tPlanilla</title>
<link href="<?php echo URL_PORTAL; ?>css/sorterStyle.css" rel="stylesheet" type="text/css" media="screen" />
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/tableSorterDESC.js"></script>

<script>
$("#div-planillacorreccion").dialog({
	autoOpen:false,
	width:1000,
	show: "drop",
	hide: "clip",	
	close: function(event,ui){
		$("#div-planillacorreccion table tbody").empty();
	}
});//fin dialog

function mostrarFichaCorreccion(idPlanilla){
	var idPersona=$("#idPersona").val();
	
	$("#div-planillacorreccion table tbody").empty();
	$.getJSON(URL+"aportes/trabajadores/buscarFichaCorreccion.php", {idPersona: idPersona, idPlanilla: idPlanilla}, function(data){
		if(data.length > 0){
			$("#div-planillacorreccion").dialog('open');
			$.each(data,function(i,fila){
				//var strTr = "<tr><td>"+ fila.nit +"</td><td>"+ fila.planilla +"</td><td>"+ fila.periodo +"</td><td>"+ fila.salariobasico +"</td><td>"+ fila.ingresobase +"</td><td>"+ fila.valoraporte +"</td><td>"+ fila.devolucion +"</td><td>"+ fila.diascotizados +"</td><td>"+ fila.fechapago +"</td><td>"+ fila.ingreso +"</td><td>"+ fila.retiro +"</td><td>"+ fila.var_tra_salario +"</td><td>"+ fila.vacaciones +"</td><td>"+ fila.lic_maternidad +"</td><td>"+ fila.tipo_cotizante +"</td><td>"+ fila.procesado +"</td><td>"+ fila.correccion +"</td><td>"+ fila.inc_tem_emfermedad +"</td><td>"+ fila.inc_tem_acc_trabajo +"</td><td>"+ fila.sus_tem_contrato +"</td></tr>";
				var strTr = "<tr><td>"+ fila.nit +"</td><td>"+ fila.planilla +"</td><td>"+ fila.periodo +"</td><td>"+ parseFloat(fila.salariobasico) +"</td><td>"+ parseFloat(fila.ingresobase) +"</td><td>"+ parseFloat(fila.valoraporte) +"</td><td>"+ fila.devolucion +"</td><td>"+ fila.diascotizados +"</td><td>"+ fila.fechapago +"</td><td>"+ fila.ingreso +"</td><td>"+ fila.retiro +"</td><td>"+ fila.var_tra_salario +"</td><td>"+ fila.vacaciones +"</td><td>"+ fila.lic_maternidad +"</td><td>"+ fila.tipo_cotizante +"</td><td>"+ fila.procesado +"</td><td>"+ fila.correccion +"</td><td>"+ fila.inc_tem_emfermedad +"</td><td>"+ fila.inc_tem_acc_trabajo +"</td><td>"+ fila.sus_tem_contrato +"</td></tr>";
				$("#div-planillacorreccion table tbody").append(strTr);
			});
		}
	});
}
</script>


<script type="text/javascript">		
	var sorterPlanillaTrab = new TINYDESC.tableDESC.sorter("sorterPlanillaTrab");
	sorterPlanillaTrab.head = "head";
	sorterPlanillaTrab.asc = "asc";
	sorterPlanillaTrab.desc = "desc";
	sorterPlanillaTrab.even = "evenrow";
	sorterPlanillaTrab.odd = "oddrow";
	sorterPlanillaTrab.evensel = "evenselected";
	sorterPlanillaTrab.oddsel = "oddselected";
	sorterPlanillaTrab.paginate = true;
	sorterPlanillaTrab.pagesize = (5);
	sorterPlanillaTrab.currentid = "currentpagePlanillaTrab";
	sorterPlanillaTrab.limitid = "pagelimitPlanillaTrab";
	sorterPlanillaTrab.init("tblPlanillaTrab",2);
</script>
</head>
<body>
	<table border="0">
		<tr>
			<td>
				<h4>Planilla Unica</h4>
			</td>
			<td class="t1">
				(Convensiones: I-Ingreso. R-Retiro. VS-Varianci&oacute;n Salarial. V-Vacaciones. M-Maternidad. TC-Tipo cotizante. P-Procesado. C-Correcci&oacute;n. ITE-Incapacidad Temporal de Enfermedad. ITAT-Incapacidad Temporal Acc Trabajo. STC-Suspensi&oacute;n Temporal del Contrato. SLN-Suspensi&oacute;n Laboral no remunerada. D-D&iacute;as. Pe-Periodo. A-Aporte. Ind-Indice de aportes. Dev-Devoluci&oacute;n)
			</td>
		</tr>
	</table>
	<table width="100%" border="0" cellspacing="0" class="sortable hover" id="tblPlanillaTrab" style="margin-left:-17px">
		<caption style="text-align:left; padding:5px; font-size:11px;"><span></span></caption>
		<thead>
			<tr>
				<th class="head" ><h3><strong>NIT</strong></h3></th>
				<th class="head" ><h3><strong>Planilla</strong></h3></th>
				<th class="head" ><h3><strong>Pe</strong></h3></th>
				<th class="head" ><h3><strong>Basico</strong></h3></th>
				<th class="head" ><h3><strong>Base</strong></h3></th>
				<th class="head" ><h3><strong>A</strong></h3></th>
				<th class="head" ><h3><strong>Ind</strong></h3></th>
				<th class="head" ><h3><strong>Dev</strong></h3></th>
				<th class="head" ><h3><strong>D</strong></h3></th>
				<th class="head" ><h3><strong>Fecha</strong></h3></th>
				<th class="head" width="10"><h3><strong>I</strong></h3></th>
				<th class="head" width="10"><h3><strong>R</strong></h3></th>
				<th class="head" width="10"><h3><strong>VS</strong></h3></th>
				<th class="head" width="10"><h3><strong>V</strong></h3></th>
				<th class="head" width="10"><h3><strong>M</strong></h3></th>
				<th class="head" ><h3><strong>TC</strong></h3></th>
				<th class="head" ><h3><strong>P</strong></h3></th>
				<th class="head" ><h3><strong>C</strong></h3></th>
				<th class="head" ><h3><strong>ITE</strong></h3></th>
				<th class="head" ><h3><strong>ITAT</strong></h3></th>
				<!-- <th class="head" ><h3><strong>STC</strong></h3></th>-->
				<th class="head" ><h3><strong>SLN</strong></h3></th>
			</tr>
		</thead>
		<tbody>
			<?php
				$cont=0;
				while ($consulta=mssql_fetch_array($result)){
					$cont++; 
			?>
			<tr>
				<td><a class='dialogoEmergente'><?php echo $consulta['nit'];  ?><span><?php echo $consulta['rsocial'];  ?></span></a></td>
				<td><?php echo $consulta['planilla']; ?></td>
				<td style=text-align:center><?php echo $consulta['periodo']; ?></td>
				<td style=text-align:right><?php echo number_format($consulta['salariobasico']); ?></td>
				<td style=text-align:right><?php echo number_format($consulta['ingresobase']); ?></td>
				<td style=text-align:right><?php echo number_format($consulta['valoraporte']); ?></td>
				<td style=text-align:right><?php echo number_format($consulta['tarifaaporte'],1); ?></td>
				<td style=text-align:right><?php echo number_format($consulta['devolucion']); ?></td>
				<td style=text-align:right><?php echo $consulta['diascotizados']; ?></td>
				<td style=text-align:center><?php echo $consulta['fechapago']; ?></td>
				<td><?php echo $consulta['ingreso']; ?></td>
				<td><?php echo $consulta['retiro']; ?></td>
				<td><?php echo $consulta['var_tra_salario']; ?></td>
				<td><?php echo $consulta['vacaciones']; ?></td>
				<td><?php echo $consulta['lic_maternidad']; ?></td>
				<td><?php echo $consulta['tipo_cot']; ?></td>
				<td><?php echo $consulta['procesado']; ?></td>
				<td><a href="#" onclick="mostrarFichaCorreccion('<?php echo $consulta['idplanilla']; ?>')"><?php echo $consulta['correccion']; ?></a></td>
				<td><?php echo $consulta['inc_tem_emfermedad']; ?></td>
				<td><?php echo $consulta['inc_tem_acc_trabajo']; ?></td>
				<!-- <td><?php echo $consulta['sus_tem_contrato']; ?></td>-->
				<td><?php echo $consulta['novedad_sln']."".($consulta['novedad_sln']=="X"?"-STC":""); ?></td>
	    	</tr>
				<?php }
				if ($cont==0) { ?>
					<script type="text/javascript">	
						MENSAJE("NO existen aportes.");
					</script>
				<?php }
			?>
		</tbody>
	</table>
    
    <div id="controls">
		<div id="perpage">
			<select onChange="sorterPlanillaTrab.size(this.value)">
			<option value="5" selected="selected">5</option>
				<option value="10" >10</option>
				<option value="20">20</option>
				<option value="50">50</option>
				<option value="100">100</option>
			</select>
			<label>&nbsp;&nbsp;&nbsp;&nbsp;Registros Por P&aacute;gina</label>
		</div>
		<div id="navigation">
			<img src="<?php echo URL_PORTAL; ?>imagenes/imagesSorter/first.gif" width="16" height="16" alt="first Page" onClick="sorterPlanillaTrab.move(-1,true)" />
			<img src="<?php echo URL_PORTAL; ?>imagenes/imagesSorter/previous.gif" width="16" height="16" alt="first Page" onClick="sorterPlanillaTrab.move(-1)" />
			<img src="<?php echo URL_PORTAL; ?>imagenes/imagesSorter/next.gif" width="16" height="16" alt="first Page" onClick="sorterPlanillaTrab.move(1)" />
			<img src="<?php echo URL_PORTAL; ?>imagenes/imagesSorter/last.gif" width="16" height="16" alt="Last Page" onClick="sorterPlanillaTrab.move(1,true)" />
		</div>
		<div id="text">&nbsp;&nbsp;&nbsp;&nbsp;P&aacute;gina <label id="currentpagePlanillaTrab"></label> de <label id="pagelimitPlanillaTrab"></label></div>
	</div>
	
	<div id="div-planillacorreccion" title="::PLANILLA CORRECCION::" style="display:none">
	<table width="100%" class="tablero" >
		<thead>
			<tr>
				<th>NIT</th>
				<th>Planilla</th>
				<th>Pe</th>
				<th>Basico</th>
				<th>Base</th>
				<th>A</th>
				<th>Dev</th>
				<th>D</th>
				<th>Fecha</th>
				<th>I</th>
				<th>R</th>
				<th>VS</th>
				<th>V</th>
				<th>M</th>
				<th>TC</th>
				<th>P</th>
				<th>C</th>
				<th>ITE</th>
				<th>ITAT</th>
				<th>STC</th>
			</tr>
		</thead>
		<tbody></tbody>
	</table>
</div> 

	<input type="hidden" name="idPersona" id="idPersona" value="<?php echo $idp; ?>" />
</body>
</html>
