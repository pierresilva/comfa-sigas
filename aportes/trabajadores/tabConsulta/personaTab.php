<?php
/* autor:       orlando puentes
 * fecha:       17/08/2010
 * objetivo:    Procesar los movimientos de los Bonos almacenados en la base de datos. 
 */
 
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$idp= $_REQUEST['v0'];
$ocultarBotonActualizar= isset($_REQUEST['botonActualizar'])? true:false;
include_once $raiz.DIRECTORY_SEPARATOR.'config.php';
include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'p.definiciones.class.php';
include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'ciudades.class.php';

$objDefiniciones=new Definiciones();
$objCiudad=new Ciudades();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"  />
<title>Documento sin t�tulo</title>
<script type="text/javascript" src="<?php echo $url; ?>js/llenarCampos.js"></script>
<script type="text/javascript">
//---------------------------------------------------------------------------------------+
function validarEspacio(e){
	tecla=(document.all) ? e.keyCode : e.which;
	if(tecla==32){
		return false;
	}
}
$(function() {
	$('#txtFechaN').datepicker({
		dateFormat: 'yy-mm-dd',
		changeMonth: true,
		changeYear: true,
		maxDate: "+0D",
	});
	var anoActual = new Date();
	var strYearRange = anoActual.getFullYear()-120 +":"+ anoActual.getFullYear();
	$("#txtFechaN").datepicker( "option", "yearRange", strYearRange );
});

/** DOCUMENT READY **/
$(document).ready(function(){		
	$("#cboCiudad").jCombo("2", { 
		parent: "#cboDepto",
		selected_value: '41001'
	});
	$("#cboZona").jCombo("3", {
		parent: "#cboCiudad"
	});	
	$("#cboBarrio").jCombo("4", {
		parent: "#cboZona"
	});	
	
	$("#cboDepto2").jCombo("1", {
		parent: "#cboPais2",
		selected_value: '41' 
	});	
	$("#cboCiudad2").jCombo("2", { 
		parent: "#cboDepto2",
		selected_value: '41001'
	});	

		
	nombreCorto();
	llenarCampos(<?php echo $idp; ?>);

	$(function(){		
		//----------------------VALIDAR QUE ACTUALICE ANTES DE PASAR DE TABS PERSONA-AFILIACION--+
		setTimeout((function(){
			<?php if($ocultarBotonActualizar==true): ?>
				$("#tablaPersonaTab input,#tablaPersonaTab select").attr("disabled","disabled");
			<?php endif; ?>				
			$("#tablaPersonaTab input,#tablaPersonaTab select").change(huboCambio);
			contarCambios=0;
		}),200);		
	});


	function huboCambio(){
		id=$(this).parents().parents().parent("table").attr("id");
		contarCambios=contarCambios+1;
		$("#div-msje").remove();
		div=$('<div id="div-msje" style="margin:9px auto;padding:6px;text-align:center;border:1px solid #FFFF00;width:300px" class="ui-state-highlight ui-corner-all"><span style="float: left; margin-right: 0.3em;" class="ui-icon ui-icon-info"></span><strong>Atenci\u00F3n</strong>, No olvidar actualizar tus datos.</div>')
		if(contarCambios>6&&id=="tablaPersonaTab"){
		div.appendTo("body").hide().show("pulsate");
		}
	}	
});
</script>
</head>

<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="tablaPersonaTab">
<tr>
  <td width="13" height="29" class="arriba_iz">&nbsp;</td>
  <td class="arriba_ce"><span class="letrablanca">::&nbsp;Actualizaci&oacute;n Datos Afiliado&nbsp;::</span></td>
  <td width="13" class="arriba_de">&nbsp;</td>
</tr>
  <tr>
	<td background="<?php echo $url; ?>imagenes/tabla/izquierda2.jpg">&nbsp;</td>
	<td background="<?php echo $url; ?>imagenes/tabla/centro.gif">
	<img src="<?php echo $url; ?>imagenes/tabla/spacer.gif" width="1" height="1">
	<img src="<?php echo $url; ?>imagenes/spacer.gif" width="1" height="1"/>
    <img src="<?php echo $url; ?>imagenes/spacer.gif" width="1" height="1"/>
    <img src="<?php echo $url; ?>imagenes/spacer.gif" width="1" height="1"/>
	
	</td>
    <td background="<?php echo $url; ?>imagenes/tabla/derecha.gif">&nbsp;</td>
  </tr>
  <tr>
    <td background="<?php echo $url; ?>imagenes/tabla/izquierda2.jpg">&nbsp;</td>
    <td align="left" background="<?php echo $url; ?>imagenes/tabla/centro.gif"><div id="error" style="color:#FF0000"></div></td>
    <td background="<?php echo $url; ?>imagenes/tabla/derecha.gif">&nbsp;</td>
  </tr>
  <tr>
    <td background="<?php echo $url; ?>imagenes/tabla/izquierda2.jpg">&nbsp;</td>
    <td align="center" background="<?php echo $url; ?>imagenes/tabla/centro.gif">
	<table width="100%" border="0" cellspacing="0" class="tablero">
      <tr>
        <td width="17%">Id Persona</td>
        <td width="32%"><input name="txtId" class="boxfecha" id="txtId" readonly="readonly" /></td>
        <td width="14%">Fecha</td>
        <td width="37%"><input name="txtId2" class="box1" id="txtId2" readonly="readonly" /></td>
        </tr>
      <tr>
      <td>Tipo Documento</td>
        <td><label>
          <select name="txtTipoD" id="txtTipoD" class="box1">
            <option value="0" selected="selected">Seleccione...</option>
            <?php
		$defin = $objDefiniciones->mostrar_datos(1,1);
		while($r_defi=mssql_fetch_array($defin)){
		echo "<option value=".$r_defi['iddetalledef'].">".$r_defi['detalledefinicion']."</option>";
			}
        ?>
          </select>
          <img src="<?php echo $url; ?>imagenes/menu/obligado.png" width="12" height="12" /></label></td>
        <td>N&uacute;mero</td>
        <td><input name="txtNumero" type="text" class="box1" id="txtNumero" onkeyup="validarCaracteresPermitidos(document.getElementById('txtTipoD').value,this);" onblur="validarLongNumIdent(document.getElementById('txtTipoD').value,this)"/>         
         <img src="<?php echo $url; ?>imagenes/menu/obligado.png" width="12" height="12"  /></td>
        </tr>
      <tr>
      <td>Primer Nombre</td>
        <td><input name="txtPNombre" type="text" class="box1" id="txtPNombre" onkeypress="return validarEspacio(event)" onkeydown="sololetras(this);" onkeyup="sololetras(this);"/>
          <img src="<?php echo $url; ?>imagenes/menu/obligado.png" width="12" height="12" /></td>
        <td>Segundo Nombre</td>
        <td><input name="textfield3" type="text" class="box1" id="txtSNombre" onkeydown="sololetras(this);" onkeyup="sololetras(this);"/></td>
        </tr>
	  <tr>
	    <td>Primer Apellido</td>
	    <td><input name="textfield2" type="text" class="box1" id="txtPApellido" onkeypress="return validarEspacio(event)" onkeydown="sololetras(this);" onkeyup="sololetras(this);"/>
	      <img src="<?php echo $url; ?>imagenes/menu/obligado.png" width="12" height="12" /></td>
	    <td>Segundo Apellido</td>
	    <td><input name="txtSApellido" type="text" class="box1" id="txtSApellido" onkeydown="sololetras(this);" onkeyup="sololetras(this);"/></td>
	    </tr>
	  <tr>
	    <td>Sexo</td>
	    <td>
	    	<select name="txtSexo" class="box1" id="txtSexo">
        		<option value="0" selected="selected">Seleccione...</option>
	      		<option value="M">Masculino</option>
	      		<option value="F">Femenino</option>
	      	</select> <img src="<?php echo $url; ?>imagenes/menu/obligado.png" width="12" height="12" />
	      	<input id="txtHiddenSexo" type="hidden" value="" />
	     </td>
	    <td>Estado Civil</td>
	    <td><select name="txtEstado2" id="txtEstado2" class="box1">
	      <option value="0" selected="selected">Seleccione...</option>
	      <?php
		$defin = $objDefiniciones->mostrar_datos(10,1);
		while($r_defi=mssql_fetch_array($defin)){
			if($r_defi['iddetalledef']!=54){
			echo "<option value=".$r_defi['iddetalledef'].">".$r_defi['detalledefinicion']."</option>";
			}
			}
        ?>
	      </select><img src="<?php echo $url; ?>imagenes/menu/obligado.png" width="12" height="12" /></td>
	    </tr>
	  <tr>
	    <td>Tel&eacute;fono</td>
	    <td><input name="txtTelefono" type="text" class="box1" id="txtTelefono" onkeydown="solonumeros(this);" onkeyup="solonumeros(this);"/>
	    	<img src="<?php echo $url; ?>imagenes/menu/obligado.png" width="12" height="12" />
	    </td>
	    
	    <td>Celular</td>
	    <td>
	    	<input name="txtCelular" type="text" class="box1" id="txtCelular" onkeydown="solonumeros(this);" onkeyup="solonumeros(this);"/>
	    </td>
	    </tr>
        <tr>
	    <td>E-mail</td>
	    <td><input name="txtEmail" type="text" class="box1" id="txtEmail" onblur="soloemail(this)"/>
	    <img src="<?php echo $url; ?>imagenes/menu/obligado.png" width="12" height="12" /></td>
	    <td>Pais de Residencia</td>
	    <td>
	    <select name="cboPaisReside" class="box1" style="width:240px" id="cboPaisReside">
									
			 <?php
				$consulta = $objCiudad->paises();
				while($r_defi=mssql_fetch_array($consulta)){
							
					echo "<option value=".$r_defi['idpais']." ".$selected.">".utf8_encode($r_defi['pais'])."</option>";
				}
			?>
		</select></td>
	    </tr>
	  <tr>
	    <td >Depto Reside</td>
	    <td ><select name="cboDepto" id="cboDepto" class="box1">
	      <option value="0" selected="selected">Seleccione</option>
	      <?php
		$consulta = $objCiudad->departamentos();
		while($r_defi=mssql_fetch_array($consulta)){
		echo "<option value=".$r_defi['coddepartamento'].">".utf8_encode($r_defi['departmento'])."</option>";
			}
        ?>
	      </select><img src="<?php echo $url; ?>imagenes/menu/obligado.png" width="12" height="12" /></td>
	    <td >Ciudad Reside</td>
	    <td ><select name="cboCiudad" id="cboCiudad" class="box1">
	      <option value="0" selected="selected">Seleccione</option>
	      </select><img src="<?php echo $url; ?>imagenes/menu/obligado.png" width="12" height="12" /></td>
	    </tr>
	  <tr>
	    <td >Zona Reside</td>
	    <td ><select name="cboZona" id="cboZona" class="box1">
	      <option value="0" selected="selected">Seleccione</option>
	      </select><img src="<?php echo $url; ?>imagenes/menu/obligado.png" width="12" height="12" /></td>
	    <td >Barrio</td>
	    <td ><select name="cboBarrio" id="cboBarrio" class="box1">
	      <option value="0" selected="selected">Seleccione</option>
	      </select>
          <img src="<?php echo $url; ?>imagenes/refrescar.png" alt="Refrescar" width="12" height="12" style="cursor:pointer" onclick="refrescarCombo(this)" />
          <img src="<?php echo $url; ?>imagenes/menu/obligado.png" alt="Obligado" width="12" height="12" /></td>
	    </tr>
	  <tr>
	    <td >Direcci&oacute;n</td>
	    <td ><input name="txtDireccion" type="text" class="boxmediano" id="txtDireccion" onfocus="direccion(this,document.getElementById('txtTipoV'));" /> <img src="<?php echo $url; ?>imagenes/menu/obligado.png" width="12" height="12" /></td>
	    <td >Ubicaci&oacute;n Vivienda</td>
	    <td ><select name="txtTipoV" id="txtTipoV" class="box1">
	      <option value="0" selected="selected">Seleccione...</option>
	      <option value="U">URBANA</option>
	      <option value="R">RURAL</option>
	      </select><img src="<?php echo $url; ?>imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
	    </tr>
	  <tr>
	    <td >Tipo Propiedad</td>
	    <td ><select name="txtCasa" id="txtCasa" class="box1">
	      <option value="0" selected="selected">Seleccione...</option>
	      <?php
		$defin = $objDefiniciones->mostrar_datos(42,1);
		while($r_defi=mssql_fetch_array($defin)){
		echo "<option value=".$r_defi['iddetalledef'].">".$r_defi['detalledefinicion']."</option>";
			}
        ?>
	      </select></td>
	    <td ></td>
	    <td ></td>
	    </tr>
	    <tr>
	    	<td >Fecha Nace</td>
	    	<td ><input name="textfield6" type="text" class="box1" id="txtFechaN" readonly="readonly" onchange="validarutadocumentos('txtFechaN','txtNumero','rutaDocP');"/><img src="<?php echo $url; ?>imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
	    	<td >Pais Nace</td>
	    	<td >
	    		<select name="cboPais2" id="cboPais2" class="box1">
	      			<option value="0" selected="selected">Seleccione</option>
				      <?php
						$consulta = $objCiudad->paises();
						while($r_defi=mssql_fetch_array($consulta)){
							echo "<option value=".$r_defi['idpais'].">".utf8_encode($r_defi['pais'])."</option>";
						}
			        ?>
	      		</select>
	      		<img src="<?php echo $url; ?>imagenes/menu/obligado.png" width="12" height="12" />
	    	</td>
	    </tr>
	  <tr>
	    <td >Depto Nace</td>
	    <td >
	    	<select name="cboDepto2" id="cboDepto2" class="box1">
	      		<option value="0" selected="selected">Seleccione</option>
	      	</select>	      <img src="<?php echo $url; ?>imagenes/menu/obligado.png" width="12" height="12" /></td>
	    <td >Ciudad Nace</td>
	    <td ><select name="cboCiudad2" id="cboCiudad2" class="box1">
	      <option value="0" selected="selected">Seleccione</option>
	      </select><img src="<?php echo $url; ?>imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
	    </tr>
	  <tr>
	    <td >Capacidad Trabajo</td>
	    <td ><select name="txtCapacidad" id="txtCapacidad" class="box1">
	      <option value="N">Normal</option>
	      <option value="I">Discapacitado</option>
	      </select></td>
	    <td >Profesion</td>
	    <td >
        <select name="profesion" class="boxmediano" id="profesion">
        <option value="0" selected="selected">Sin Profesi&oacute;n</option>
        <?php
	$consulta=$objDefiniciones->mostrar_datos(20, 4);
	while($row=mssql_fetch_array($consulta)){
		echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
	}
	?>        
        </select>
        </td>
	    </tr>
	  <tr>
	    <td >Nombre Corto</td>
	    <td ><input name="txtNombreC" type="text" class="boxlargo" id="txtNombreC" readonly="readonly" /></td>
	    <td >Ruta Documentos</td>
	    <td ><input  type="text" class="boxmediano" id="rutaDocP" readonly="readonly" /></td>
	    </tr>
	  
    </table>
    </td>
    <td background="<?php echo $url; ?>imagenes/tabla/derecha.gif">&nbsp;</td>
  </tr>
    <tr>
    <td height="38" background="<?php echo $url; ?>imagenes/tabla/abajo_izq2.gif">&nbsp;</td>
    <td background="<?php echo $url; ?>imagenes/tabla/abajo_central.gif">
    <?php if($ocultarBotonActualizar==false):?>
    <a id="actualizar" value="Actualizar datos" class="ui-state-default"  onClick="actualizar(<?php echo $idp; ?>);" style="text-decoration:none; padding:4px; cursor:pointer; margin-left:45%" >Actualizar datos</a>
    <?php endif;?>
    </td>
    <td background="<?php echo $url; ?>imagenes/tabla/abajo_der.gif">&nbsp;</td>
  </tr>
    </table>


</body>	
</html>