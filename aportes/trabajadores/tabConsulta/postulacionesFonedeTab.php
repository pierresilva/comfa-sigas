<?php
/* autor:       Oswaldo Gonz�lez
 * fecha:       12/08/2011
 * objetivo:    Mostrar lista de postulaciones a fonede seg�n la persona solicitada
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz. DIRECTORY_SEPARATOR . 'config.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Postulaciones FONEDE</title>
<link href="<?php echo URL_PORTAL; ?>css/sorterStyle.css" rel="stylesheet" type="text/css" media="screen" />
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/comunes.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/jquery.tablesorter.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/jquery.tablesorter.pager.js"></script>
<script type="text/javascript">
  $(document).ready(refrescar);
  
function refrescar(){
	$("#tPlanilla tbody tr").remove();
	var num=$("#nRegistros").val();
	var idp=$("#txtIde").val();
	$.ajax({
		url:URL+'phpComunes/buscarPostulacionesFonede.php',
		type:"POST",
		dataType:"json",
		data: {idpersona:idp},
		async: false,
		success: function(data){
			if(data !=0 ){
				var no = data.length;
				$("#tPlanilla caption span").html(no +" registros encontrados.");
				$.each(data, function(i,postulacion){
					var tr = "<tr>"+
								"<td style='text-align:right;'>"+ postulacion.seccional +"</td>"+
								"<td style='text-align:right;'>"+ postulacion.idradicacion +" ("+ postulacion.num_rad_rsn +")</td>"+
								//"<td style='text-align:center;'>"+ postulacion.procesado +"</td>"+
								"<td style='text-align:right;'>"+ postulacion.idpostulacion +"</td>"+
								"<td style='text-align:right;'>"+ postulacion.aficaja +"</td>"+
								"<td style='text-align:right;'>"+ postulacion.tiempo +"</td>"+
								"<td style='text-align:center;'> "+ postulacion.fecharadicacion.substr(0,16) +"</td>"+
								"<td style='text-align:center;'><span title='"+ postulacion.estado_postulacion +"'>"+ postulacion.codigo_estado_postulacion +"</span></td>"+
							  "</tr>";
					$("#tPlanilla tbody").append(tr);  
				});
			}
		},
		complete:function(){
			//Actualizar Tabla de sorter 
			$("#tPlanilla tbody tr:odd").addClass("evenrow");
			$("#tPlanilla").tablesorter();
			$("#tPlanilla").trigger("update");
		}
	}); 	
}
</script>

</head>

<body>
	<table border="0">
		<tr>
			<td><h4>Postulaciones a FONEDE</h4></td>
		</tr>
	</table>
	<table width="100%" border="0" cellspacing="0" class="sortable hover" id="tPlanilla">
		<caption style="text-align:left; padding:5px; font-size:11px;"><span></span></caption>
		<thead>
			<tr>
				<th class="head"><h3><strong>Seccional</strong></h3></th>
				<th class="head"><h3><strong>N&uacute;m. Rad.</strong></h3></th>
				<!-- <th class="head"><h3><strong>Rad. Procesada</strong></h3></th> -->
				<th class="head"><h3><strong>N&uacute;m. Postu.</strong></h3></th>
				<th class="head"><h3><strong>Vinculaci&oacute;n</strong></h3></th>
				<th class="head"><h3><strong>Tiempo</strong></h3></th>
				<th class="head"><h3><strong>Fecha solicitud</strong></h3></th>
				<th class="head"><h3><strong>Estado</strong></h3></th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
	<input type="hidden" name="txtIde" id="txtIde" value="<?php echo $_REQUEST['v0']; ?>" />
</body>
</html>