<?php
/* autor:       orlando puentes
 * fecha:       27/09/2010
 * objetivo:    
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';

$idtrabajador= $_REQUEST['v0'];
include_once $raiz . DIRECTORY_SEPARATOR . 'clases' .  DIRECTORY_SEPARATOR . 'p.embargos.class.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'clases' .  DIRECTORY_SEPARATOR . 'p.pignoracion.class.php';
$objClase=new Embargos();
$cuenta=$objClase->contar_embargos($idtrabajador);
$row=mssql_fetch_array($cuenta);
$cone=$row['cuenta']; 
$objPignoracion=new Pignoracion();
$cuenta=$objPignoracion->contar_pignoracion($idtrabajador);
$row=mssql_fetch_array($cuenta);
$conp=$row['cuenta'];
mssql_free_result($cuenta);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Descuentos</title>
<link href="<?php echo URL_PORTAL; ?>css/sorterStyle.css" rel="stylesheet" type="text/css" media="screen" />
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/comunes.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/jquery.tablesorter.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/jquery.tablesorter.pager.js"></script>

<script language="javascript">
var URL=src();

$(document).ready(function(){
	$("#tEmbargo").tablesorter();
	$("#tEmbargo").trigger("update");
	$("#tEmbargo tbody tr:odd").addClass("evenrow");
});

$("label[name='labelidpig']").unbind('click');
$("label[name='labelidpig']").bind("click",function(){
	var lidpig=$(this).text();
    buscarDetalle(lidpig);
});

function buscarDetalle(idpig){
	$("#dialog-pignoracion").dialog({
		title:"Detalle Pignoracion",
		width: 650,
		modal: true,
		draggable:true,
		zIndex:1000,
		resizable:false,
		closeOnEscape:true,
		open: function(){
			$(this).html('');
			$(this).load(URL+'phpComunes/detallePignoracion.php',{v0:idpig},function(data){
				$(this).html(data);
			});
		},
		close: function() {
			$(this).dialog("destroy");
		}
	});
}//end function
</script>
</head>

<body>
<h4>Embargos y Pignoraciones.</h4>
<h4>EMBARGOS</h4>
<br />
<?php 
if($cone>0){
	$cont=0;
	$consulta = $objClase->mostrar_registro($idtrabajador);
	while($row=mssql_fetch_array($consulta)){
		$tercero=$row['pnombre']." ".$row['snombre']." ".$row['papellido']." ".$row['sapellido'];
		$estado=$row['estado']=='A'?'Activo':'Inactivo';
		$idemb=$row['idembargo'];
		$cont++;
?>
<table width="100%" border="0" cellspacing="0" class="sortable hover" id="tEmbargo">
<thead>
<tr>
	<th class="head"><strong>Fecha</strong></th>
	<th class="head"><strong>N&uacute;mero</strong></th>
    <th class="head"><strong>Tercero</strong></th>
    <th class="head"><strong>Tipo</strong></th>
    <th class="head"><strong>Causal</strong></th>
    <th class="head"><strong>Estado</strong></th>
    <th class="head"><strong>Usuario Estado</strong></th>
 	<th class="head"><strong>Notas</strong></th>
  </tr>
  </thead>
  <tbody>
	<tr><td><?php echo $row['fechaembargo']; ?>&nbsp;</td>
		<td><?php echo number_format($row['identificacion']); ?>&nbsp;</td>
		<td><?php echo $tercero; ?>&nbsp;</td>
		<td><?php echo $row['codigopago']; ?>&nbsp;</td>
		<td><?php echo $row['juzgado']; ?>&nbsp;</td>
		<td><?php echo $row['estado']; ?>&nbsp;</td>
		<td><?php echo $row['usuarioestado']; ?>&nbsp;</td>
		<td><?php echo $row['notas']; ?>&nbsp;</td>
	</tr>
	<tr>
		<th><strong>Motivo</strong></th>
		<td colspan="7"><?php echo $row['motivoestado']; ?></td>
	</tr>
    </tbody>
</table>
<table width="80%" border="0" cellspacing="0" class="tablero" id="tEmbargo">
<tr>
	<th>Id</th>
	<th>Beneficiario</th>
    <th>Afiliaci&oacute;n</th>
    <th>Relaci&oacute;n</th>
    <th style="text-align: center">Subsidio</th>
 	<th style="text-align: center">Estado</th>
  </tr>
<?php 
	$beneficiarios=$objClase->beneficiarios_embargo($idemb);
	while($ben=mssql_fetch_array($beneficiarios)){
		$nom=$ben['pnombre']." ".$ben['snombre']." ".$ben['papellido']." ".$ben['sapellido'];
?>
	<tr><td><?php echo $ben['idbeneficiario']; ?>&nbsp;</td>
	<td><?php echo iconv('', 'UTF-8', $nom); ?>&nbsp;</td>
	<td><?php echo $ben['fechaafiliacion']; ?>&nbsp;</td>
	<td><?php echo $ben['detalledefinicion']; ?>&nbsp;</td>
	<td><?php echo $ben['giro']; ?>&nbsp;</td>
	<td><?php echo $ben['estado']; ?>&nbsp;</td>
	</tr>
<?php 	
		}
		echo "</table>";
	}
}
else{
	echo "<label class=Rojo>El trabajor no tiene Embargos!</label>";
}
?>
<br />
<h4>PIGNORACIONES</h4>
<?php 
if($conp>0){
?>
<table width="100%" border="0" cellspacing="0" class="sortable hover" id="tPignoracion">
<tr>
	<td class="head"><h3>ID.</h3></td>
    <td class="head"><h3>Fecha pignoraci&oacute;n</h3></td>
    <td class="head"><h3>Valor</h3></td>
    <td class="head"><h3>Saldo</h3></td>
    <td class="head"><h3>Inicio</h3></td>
 	<td class="head"><h3>Estado</h3></td>
 	<td class="head"><h3>Anulado</h3></td>
 	<td class="head"><h3>Pagare</h3></td>
 	<td class="head"><h3>Convenio</h3></td>
  </tr>
<?php 
$cont=0;
$pignoracion=$objPignoracion->buscar_pignoracion($idtrabajador);
while($row=mssql_fetch_array($pignoracion)){
	$cont++;
	$idpig=$row['idpignoracion'];
?>	
	<tr><td ><label style="cursor:pointer;text-decoration:none; font-weight: bold" name="labelidpig" id="lbl_pigno_<?php echo $idpig; ?>" title="Ver detalles de la pignoraci&oacute;n <?php echo $row['pagare']; ?>" > <?php echo $row['idpignoracion']; ?> </label></td>
	<td><?php echo $row['fechapignoracion']; ?>&nbsp;</td>
	<td><?php echo number_format($row['valorpignorado']); ?>&nbsp;</td>
	<td><?php echo number_format($row['saldo']); ?>&nbsp;</td>
	<td ><?php echo $row['periodoinicia']; ?>&nbsp;</td>
	<td><?php echo $row['estado']; ?>&nbsp;</td>
	<td ><?php echo $row['anulado']; ?>&nbsp;</td>
	<td ><?php echo $row['pagare']; ?>&nbsp;</td>
	<td ><?php echo $row['detalledefinicion']; ?>&nbsp;</td></tr>
<?php 
}
?>	
</table>
<?php 
}else {
	echo "<label class=Rojo><br>El trabajor no tiene Pignoraciones!</label>";
}
?>
<div id="dialog-pignoracion" style="display:none"></div>
</body>
</html>