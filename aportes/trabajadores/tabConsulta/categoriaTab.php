<?php
/* autor:       orlando puentes
 * fecha:       19/07/2010
 * objetivo:    
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR .'p.categorias.class.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';
$idp=$_REQUEST['v0'];
//$idp=85818;
//$ide=31368;
$objCategorias=new Categorias();
$result=$objCategorias->buscar_historico_categorias($idp);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Documento sin título</title>
<link href="<?php echo URL_PORTAL; ?>css/sorterStyle.css" rel="stylesheet" type="text/css" media="screen" />
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/jquery.tablesorter.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/jquery.tablesorter.pager.js"></script>
<script type="text/javascript">
$(document).ready(refrescar);
function refrescar(){	
  $("#table2").tablesorter();
  $("#table2").trigger("update");
  $("#table2 tbody tr:even").addClass("evenrow");
}
 
</script>
</head>
<body>
<h4>Categorias</h4>
<table width="70%" border="0" cellspacing="0" class="sortable hover" id="table2">
<thead>
<tr>
	<th width="30%" align="center" class="head"><h3><strong>Fecha Cambio</strong></h3></th>
	<th width="20%" class="head" style="text-align:center"><h3><strong>Salario</strong></h3></th>
	<th width="20%" class="head" style="text-align:center"><h3><strong>Categoria</strong></h3></th>
</tr>
</thead>
<tbody>
<?php 
while($consulta=mssql_fetch_array($result)){
?>
<tr>
         <td><?php echo $consulta['fechacambio'];  ?></td>
         <td style="text-align:center"><?php echo $consulta['salario'];  ?></td>
         <td style="text-align:center"><?php echo $consulta['categoria'];  ?></td>
     </tr>
  <?php }?>
  </tbody>
</table>
<div id="div-registros">N&uacute;mero de Registros: 
<select name="nRegistros" id="nRegistros" onchange="refrescar();">
	<option selected="selected"  value="10" >10</option>
	<option value="20">20</option>
	<option value="30">30</option>
	<option  value="40">40</option>
    <option  value="50">50</option>
</select>
</div>        
</body>
</html>
