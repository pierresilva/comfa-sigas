<?php
/* autor:       orlando puentes
 * fecha:       16/07/2010
 * objetivo:     
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Documentos Trabajador</title>
 <link type="text/css" href="../../css/Estilos.css" rel="stylesheet"/>
<script type="text/javascript">
 $(document).ready(function(){
	
$("#docs div:not(:first)").hide();
	
	 $("input[name=rconviven],input[name=rhijos],input[name=rpadres]").click(function(){
	       var name=$(this).attr("name");
	       
		   switch(name){
		           case 'rconviven': (   $(this).val()=='s'    ?   $("#docs div:eq(1)").show()  : $("#docs div:eq(1)").hide().find("input:check").attr("checked",false) );
		           break;
		           case 'rhijos':    (   $(this).val()=='s'    ?   $("#docs div:eq(2)").show()  : $("#docs div:eq(2)").hide().find("input:check").attr("checked",false) );
	 	           break;
		           case 'rpadres':   (  $(this).val()=='s'     ?   $("#docs div:eq(3)").show()  : $("#docs div:eq(3)").hide().find("input:check").attr("checked",false) );
   	               break;
		  }
	 });//end click
	
/*	$("#validar").click(function(event){
		event.preventDefault();
		if($("#formulario").attr("checked")==false){
			alert("Debe tener el formulario");
		}
		
		if($("input[name=rhijos]:checked").attr("value")=="s"){
			 if($("#check3").attr("checked")==false){
				 alert("Necesario el registro civil del HIJO");
			 }
		}
			
		var cadena="";	
		if($("input[name=rpadres]:checked").attr("value")=="s"){
			 if($("#check8").attr("checked")==false){
				 cadena+="Fotocopia de la cedula\n";
			 }
			
			if($("#check9").attr("checked")==false){
				 cadena+="Registro civil trabajador\n";
				 }
			if(cadena!=""){	 
			alert("Necesario "+cadena);
			}
		}	
	});//end click*/
	 
});//end ready
 </script>
<style type="text/css">
#pie li {
	font-size: 10px;
	color: #999;
}
</style>
</head>

<body>
<div id="docs">
<!-- DOCUMENTOS AFILIADO -->
<div>
  <h4>Documentos Afiliaci&oacute;n</h4>
  <p>
  <input type="checkbox" name="docTrabajador" id="formulario"/>
  Formulario afiliaci&oacute;n.
  <img src="../../imagenes/menu/obligado.png" width="12" height="12" />
  </p>
  <p>
  <input type="checkbox" name="docTrabajador" />
  Fotocopia de la c&eacute;dula.  </p>
  <p>�Conviven? 
    Si   
    <input type="radio" name="rconviven" id="si" value="s" />
    No    <input name="rconviven" type="radio" id="no" value="n" checked="checked" />
  </p>
  <p>�Hijos?&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    
    Si   
      <input type="radio" name="rhijos" id="si" value="s" />
    No    <input name="rhijos" type="radio" id="no" value="n" checked="checked" />
</p>
  <p>�Padres?&nbsp;&nbsp;&nbsp; 
    Si   
      <input type="radio" name="rpadres" id="si" value="s" />
    No    <input name="rpadres" type="radio" id="no" value="n" checked="checked" />
  </p>

</div>
<!-- DOCUMENTOS CONYUGE -->
<div>
<hr  />
 <h4>Documentos Conyuge</h4>
  <p>
  <input type="checkbox" name="docConyuge" id="check1" />
  Registro civil MATRIMONIO<sup>1</sup>.</p>
  <p>
  <input type="checkbox" name="docConyuge" id="check2" />
  Fotocopia AMPLIADA y LEGIBLE de la c&eacute;dula del c&oacute;nyuge.
  </p>
</div>
<!-- DOCUMENTOS HIJOS -->
<div>
<hr  />
<h4>Documentos  Hijos-Hijastros-Hermanos</h4>
<p>
  <input type="checkbox" name="docHijos" id="check3" />
  Fotocopia Registro civil.
  <img src="../../imagenes/menu/obligado.png" width="12" height="12" />
</p>
<p>
  <input type="checkbox" name="docHijos" id="check4" />
  Certificado de escolaridad<sup>2</sup>.
</p>
<p>
  <input type="checkbox" name="docHijos" id="check5" />
  Sentencia judicial de custodia.
</p>
<p>
  <input type="checkbox" name="docHijos" id="check6" />
  Sentencia de adopci&oacute;n.
</p>
<p>
  <input type="checkbox" name="docHijos" id="check7" />
  Certificado de discapacidad.
</p>

</div>
<!-- DOCUMENTOS PADRES -->
<div>
<hr  />
<h4>Documentos  Padres</h4>
<p>
  <input type="checkbox" name="docPadres" id="check8" />
  Fotocopia de la c&eacute;dula AMPLIADA.
  <img src="../../imagenes/menu/obligado.png" width="12" height="12" />
</p>
<p>
  <input type="checkbox" name="docPadres" id="check9" />
  Registro civil trabajador.
  <img src="../../imagenes/menu/obligado.png" width="12" height="12" />
</p>
<p>
  <input type="checkbox" name="docPadres" id="check10" />
  Certificado de supervivencia.
</p>
</div>
<!-- FIN DIV DOCS -->
</div>
<br />
<ul  id="pie" type="1" style="border-first:1px dashed #999">
<li>(1)Escritura P�blica &oacute; sentencia judicial para parejas del MISMO sexo. </li>
<li>(2)Hijos entre 12 y 18 años de edad, y aprobado por el plantel.</li>
<li><img src="../../imagenes/menu/obligado.png" width="12" height="12" />Documentos m&iacute;nimos requeridos.</li>
</ul>

</body>
</html>