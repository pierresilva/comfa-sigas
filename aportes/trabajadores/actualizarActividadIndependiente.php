<?php
/* autor:       Andres Lara
 * fecha:       14/03/2016
 * objetivo:    Insertar Las actividades economicas de independientes y pensionados
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'p.persona.class.php';
require_once('clases/trabajadores.class.php');

$objClase = new Trabajador();

$accion=$_REQUEST["accion"];

if($accion=='I'){
	$consulta=$objClase->insertar_actividad_independiente($_REQUEST['idpersona'],$_REQUEST['idciiu'],$_REQUEST['categoria'],$_REQUEST['idradicacion']);
}

if($accion=='A'){
	$consulta=$objClase->actualizar_actividad_independiente($_REQUEST['idpersona'],$_REQUEST['idciiu'],$_REQUEST['categoria'],$_REQUEST['idradicacion']);
}



if ($consulta  == true){
	echo 1;
}else{
	echo 0;
}

?>