<?php
date_default_timezone_set('America/Bogota');
ini_set("display_errors",'1');
session_start();
$idp =$_REQUEST['v0'];

include_once '../../rsc/pdo/IFXDbManejador.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo $cadena;
	exit();
}
$filas = array();
$sql="SELECT a91.detalledefinicion,a26.idcertificado,a26.estado,a26.periodoinicio,a26.periodofinal,a26.fechapresentacion
	  FROM aportes026 a26 INNER JOIN aportes091 a91 ON a91.iddetalledef=a26.idtipocertificado
	  WHERE a26.idbeneficiario=$idp ORDER BY a26.periodoinicio DESC";
$rs=$db->querySimple($sql);
if(is_null($rs)){
	echo 0;
	exit();
}
while ($row=$rs->fetch()) {
		$filas[] = $row;
	}
if(count($filas)>0)
	echo json_encode($filas);
else 
	echo 0;
?>