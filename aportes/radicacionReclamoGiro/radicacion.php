<?php
set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");

$root= $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';

$url=$_SERVER['PHP_SELF'];
include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes'.DIRECTORY_SEPARATOR.'auditoria.php';

include_once $raiz . DIRECTORY_SEPARATOR . 'rsc/pdo/IFXDbManejador.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}
$FrmActual=basename($_SERVER['PHP_SELF']);
$fecver = date('Ymd h:i:s A',filectime($FrmActual));
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="es"><head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Radicaci&oacute;n</title>
<link type="text/css" href="<?php echo URL_PORTAL; ?>newcss/Estilos.css" rel="stylesheet">
<link type="text/css" href="<?php echo URL_PORTAL; ?>newcss/css.1.8/jquery-ui-1.8.17.custom.css" rel="stylesheet">
<link type="text/css" href="<?php echo URL_PORTAL; ?>newcss/marco.css" rel="stylesheet">

<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/1.6/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/1.6/jquery-ui-1.8.16.custom.min.js"></script>

<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/comunes.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/jquery.utilitarios.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/clases/empresa.class.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/clases/persona.class.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/clases/tarjeta.class.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/clases/documento.class.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/clases/afiliacion.class.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/clases/radicacion.class.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/clases/certificado.class.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/clases/grupoFamiliar.class.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/clases/reclamoEmpresa.class.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/clases/reclamoTrabajador.class.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>aportes/radicacionnew/js/radicacion.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>aportes/radicacionReclamoGiro/js/tabs.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>aportes/radicacionnew/js/utilitarios.js"></script>
</head>

<body>
	<form name="forma">
		<br />
		<div class="demo">
			<div id="accordion">
				<h3 id="titRadicacion">
					<a href="#"> Datos B&aacute;sicos Radicaci&oacute;n </a>
				</h3>
				<div>
					<table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
						<tr>
							<td width="13" height="29" class="arriba_iz">&nbsp;</td>
							<td class="arriba_ce">
								<span class="letrablanca">:: Datos B&aacute;sicos ::</span>
								<div style="text-align:right; height:20px; top:22px; width:221px; position:absolute; right: 100px;"><?php echo 'Versi&oacute;n: ' . $fecver; ?></div>
							</td>
							<td width="13" class="arriba_de" align="right">&nbsp;</td>
						</tr>
						<tr height="35" >
							<td class="cuerpo_iz">&nbsp;</td>
							<td class="cuerpo_ce">
								<table width="90%" cellspacing="0" cellpadding="0" align="center">
									<tr>
										<td><img src="../../imagenes/nuevoRegistro.png" title="Nuevo" style="cursor: pointer" onclick="nuevoR();"></td>
									</tr>
								</table></td>
							<td class="cuerpo_de">&nbsp;</td>
						</tr>
						<tr>
							<td class="cuerpo_iz">&nbsp;</td>
							<td class="cuerpo_ce">
								<table width="90%" border="0" cellspacing="0" cellpadding="0" align="center" class="tablero">
									<tr>
										<td width="17%">Radicaci&oacute;n Nro</td>
										<td width="29%"><input name="txtIdRadicacion" class="boxfecha" id="txtIdRadicacion" disabled="disabled" /></td>
										<td width="17%">Fecha</td>
										<td width="37%"><input name="txtFecha" class="box1" id="txtFecha" disabled="disabled" /></td>
									</tr>
									<tr>
										<td>Hora Inicio</td>
										<td><input name="txtHoraIni" type="text" class="box1" style="width: 100px" id="txtHoraIni" disabled="disabled">&nbsp;<span id="horaDia" style="color: #FF0000"></span></td>
										<td>Hora Final</td>
										<td><input name="txtHoraFin" type="text" class="box1" style="width: 100px" id="txtHoraFin" disabled="disabled"></td>
									</tr>
									<tr>
										<td>Tipo Documento</td>
										<td><label> <select name="cmbIdTipoDocumento" id="cmbIdTipoDocumento" class="box1" style="width: 250px" onchange="$('#txtIdentificacion').val('').trigger('blur');" >
													<option value="0" selected="selected">Seleccione...</option>
						            		<?php
												$rs = $db->Definiciones ( 1, 1 );
												while ( $row = $rs->fetch() ) {
													echo "<option value=" . $row ['iddetalledef'] . ">" . $row ['detalledefinicion'] . "</option>";
												} 
											?>
						          	</select> <img src="../../imagenes/menu/obligado.png" width="12" height="12"></label></td>
										<td>N&uacute;mero</td>
										<td><input name="txtIdentificacion" type="text" class="box1" id="txtIdentificacion" onkeyup="validarIdentificacion($('#cmbIdTipoDocumento'),this);" onkeydown="validarIdentificacion($('#cmbIdTipoDocumento'),this);" 
											onkeypress="validarIdentificacion($('#cmbIdTipoDocumento'),this);" onblur="buscarPersona($('#cmbIdTipoDocumento'), $('#txtIdentificacion'), $('#tdNombreCompletoTrae'), true);" maxlength="12">											
											<img src="../../imagenes/menu/obligado.png" width="12" height="12"></td>
									</tr>
									<tr>
										<td>Nombre Completo</td>
										<td id="tdNombreCompletoTrae" colspan="3">&nbsp;</td>
									</tr>
									<tr>										
										<td>Tipo Radicaci&oacute;n</td>
										<td><select name="cmbIdTipoRadicacion" id="cmbIdTipoRadicacion" class="boxmediano" style="width: 250px" onChange="initRad(this.value);">
												<option value="0" selected="selected">Seleccione</option>
							      		<?php
							      			$rs = $db->Definiciones ( 6, 3 );											
											while ( $row = $rs->fetch() ) {
												if($row ['iddetalledef']==32 || $row ['iddetalledef']==169 || $row ['iddetalledef']==170 || $row ['iddetalledef']==4230 || $row ['iddetalledef']==4231){
													echo "<option value=" . $row ['iddetalledef'] . ">" . $row ['detalledefinicion'] . "</option>";
												}
											}
										?>
							      	</select> <img src="../../imagenes/menu/obligado.png" width="12" height="12"></td>
							      		<td>Presentaci&oacute;n</td>
										<td><select name="cmbIdTipoPresentacion" id="cmbIdTipoPresentacion" class="box1" style="width: 250px">
												<option value="0" selected="selected">Seleccione</option>
							      		<?php
							      			$rs = $db->Definiciones ( 13, 3 );										 	
											while ( $row = $rs->fetch() ) {
												echo "<option value=" . $row ['iddetalledef'] . ">" . $row ['detalledefinicion'] . "</option>";
											} 
										?>
							      	</select> <img src="../../imagenes/menu/obligado.png" width="12" height="12"></td>
									</tr>
								</table>
							</td>
							<td class="cuerpo_de">&nbsp;</td>
						</tr>
						<tr>
							<td class="abajo_iz">&nbsp;</td>
							<td class="abajo_ce"></td>
							<td class="abajo_de">&nbsp;</td>
						</tr>
					</table>
				</div>
				<!-- Div A01 Tabla Basicas Radicacion -->

				<h3 id="titDetRadicacion" >
					<a href="#">Detalle de la Radicaci&oacute;n</a>
				</h3>
				<div id="detRadicacion">
					<form name="formaTabs">
						<div id="tabs">
							<ul>
								<li><a href="#">&nbsp;</a></li>
							</ul>							
						</div>
					</form>
				</div>				
				<!-- Div A02 Tablas por radicacion -->
				
				<h3 id="titDocRequerido" >
					<a href="#">Documentos Requeridos</a>
				</h3>
				<div id="docRequerido">					
				</div>				
				<!-- Div A03 Tablas por documentos solicitados -->
			</div>
			<!-- Div Acordeon -->
		</div>
		<!-- Div Global -->
	</form>
	
	<!-- DIALOGO PERSONA SIMPLE -->
	<div id="dialog-persona" title="Formulario Datos Basicos" style="display:none">
	<center>
		<table width="578" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff" class="tablero">
			<tbody>
				<tr bgcolor="#EBEBEB">
				  <td colspan="4" style="text-align:center" >Datos B&aacute;sicos de la persona</td>
			  </tr>
				<tr>
				  <td >Tipo Documento</td>
				  <td ><select name="tDocumento" id="tDocumento" class="box1" disabled>
		          	<option value="1">CEDULA DE CIUDADANIA</option>
					<option value="2">TARJETA DE IDENTIDAD</option>
					<option value="3">PASAPORTE</option>
					<option value="4">CEDULA DE EXTRANJERIA</option>
					<option value="5">NIT</option>
					<option value="6">REGISTRO CIVIL</option>
					<option value="7">MENOR SIN IDENTIFICACION</option>
					<option value="8">ADULTO SIN IDENTIFICACION</option>
		          </select>
		          </td>
				  <td >N&uacute;mero</td>
				  <td ><input name="txtNumeroP" id="txtNumeroP" type="text" class="box1" onchange="buscarPersonaExistente($('#txtNumeroP').val(),$('#spNombre').val(),$('#ssNombre').val(),$('#spApellido').val(),$('#ssApellido').val(),true,0)" readonly></td>
			  </tr>
				<tr>
				  <td >Primer Nombre</td>
				  <td ><input name="spNombre" id="spNombre" type="text" class="box1" onkeydown='sololetras(this);' onkeyup='sololetras(this);' onchange="buscarPersonaExistente($('#txtNumeroP').val(),$('#spNombre').val(),$('#ssNombre').val(),$('#spApellido').val(),$('#ssApellido').val(),true,0)" onkeypress="return validarEspacio(event)" ></td>
				  <td >Segundo Nombre</td>
				  <td ><input name="ssNombre" id="ssNombre" type="text" class="box1" onkeydown='sololetras(this);' onkeyup='sololetras(this);' onchange="buscarPersonaExistente($('#txtNumeroP').val(),$('#spNombre').val(),$('#ssNombre').val(),$('#spApellido').val(),$('#ssApellido').val(),true,0)" ></td>
			  </tr>
				<tr>
				  <td >Primer Apellido</td>
				  <td ><input name="spApellido" id="spApellido" type="text" class="box1" onkeydown='sololetras(this);' onkeyup='sololetras(this);' onchange="buscarPersonaExistente($('#txtNumeroP').val(),$('#spNombre').val(),$('#ssNombre').val(),$('#spApellido').val(),$('#ssApellido').val(),true,0)" onkeypress="return validarEspacio(event)"></td>
				  <td >Segundo Apellido</td>
				  <td ><input name="ssApellido" id="ssApellido" type="text" class="box1" onkeydown='sololetras(this);' onkeyup='sololetras(this);' onchange="buscarPersonaExistente($('#txtNumeroP').val(),$('#spNombre').val(),$('#ssNombre').val(),$('#spApellido').val(),$('#ssApellido').val(),true,0)" ></td>
			  </tr>
			  <tr>
			  	<td >Sexo</td>
				<td ><select name="tSexo" id="tSexo" class="box1" >
					<option value="0">Seleccione</option>
		        	<option value="M">MASCULINO</option>
					<option value="F">FEMENINO</option>
		          </select>
		        </td>
		    	<td><label for="fechaNaceDialogPersona">Fecha de nacimiento</label></td>
		    	<td><input type="text" name="fechaNaceDialogPersona" id="fechaNaceDialogPersona" readonly="true" /></td>		    	
			  </tr>
				<tr>
					<td >&nbsp;</td>
					<td >&nbsp;</td>
					<td >&nbsp;</td>
					<td >&nbsp;</td>
				</tr>
		</tbody>
		</table>
	</center>
	</div>
	
	<!-- DIALOGO PERSONA DUPLICADA -->
	<div id="divDocumentoDoble" title="Datos Persona" style="display:none; padding-first:40px" >
		<table width="460" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff" class="tablero">
			<tr>
				<th class="head">TD</th>				
				<th class="head">Doc</th>
				<th class="head">Nombre</th>
			</tr>
			<tbody id="tbDocumentoDoble" ></tbody>
		</table>	
	</div>
	
	<!-- DIALOGO MOTIVOS DEVOLUCION -->
	<div id="divMotivosDevolucion" title="Motivos de Devolucion" style="display:none; padding-first:40px" >
		<p>
			<label>Motivo</label>
			<select name="cmbMotivosDevolucion" id="cmbMotivosDevolucion" class="boxlargo">
		      	<option value="0"> Seleccione...</option>
				<?php
					$rs = $db->Definiciones ( 54, 1 );
					while ( $row = $rs->fetch() ) {
						echo "<option value=" . $row ['iddetalledef'] . ">" . $row ['detalledefinicion'] . "</option>";
					}
				?>
		    </select>
		</p>
		<button id="btnMotivosDevolucion" class="ui-state-default" onclick="saveDevolucion(2);">Aceptar</button>	
	</div>
	
	<input id="txtSMLV" type="hidden" value="" />
</body>

</html>