<?php
/* autor:       orlando puentes
 * fecha:       octubre 4 de2010
 * objetivo:    Procesar los movimientos de los Bonos almacenados en la base de datos. 
 */
 set_time_limit(0);
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$usuario= $_SESSION["USUARIO"];

include_once $raiz.DIRECTORY_SEPARATOR.'funcionesComunes'.DIRECTORY_SEPARATOR.'funcionesComunes.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
	$db = IFXDbManejador::conectarDB();
 	if($db->conexionID==null){
 		$cadena = $db->error;
 		echo msg_error($cadena);
		exit();
 	}
 	
$v0=$_REQUEST['v0'];
$sql="SELECT top 1 idtipodocumento, identificacion, papellido, sapellido, pnombre, snombre, fechanacimiento,idciunace,cn.municipio as cnace,sexo,idciuresidencia,cr.municipio,idzona,z.zona,telefono FROM aportes015 LEFT JOIN aportes089 cn ON aportes015.idciunace=cn.codmunicipio LEFT JOIN aportes089 cr ON aportes015.idciuresidencia=cr.codmunicipio LEFT JOIN aportes089 z ON aportes015.idzona=z.codzona WHERE idpersona=$v0";
$rs=$db->querySimple($sql);
$con=0;
$filas=array();
while($w=$rs->fetch()){
	$filas[]=$w;
	$con++;
}
if($con==0){
	echo 0;
}else{
	echo json_encode($filas);
}
?>