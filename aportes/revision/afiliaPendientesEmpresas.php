<?php
/**
 * @author Oswaldo Gonzalez
 * @date 01-jun-2012
 */
set_time_limit(0);
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$usuario= $_SESSION["USUARIO"];

include_once $raiz.DIRECTORY_SEPARATOR.'funcionesComunes'.DIRECTORY_SEPARATOR.'funcionesComunes.php';

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

$pagina = $_REQUEST["pagina"];
$numRegistros = $_REQUEST["numRegistros"];
$razonSocial = (isset($_REQUEST["razonSocial"]) && $_REQUEST["razonSocial"] != "")?$_REQUEST["razonSocial"]:null;
$nit = (isset($_REQUEST["nit"]) && $_REQUEST["nit"] != "")?$_REQUEST["nit"]:null;
$strCondicionBusqueda = "";

if($razonSocial != null)
	$strCondicionBusqueda = " AND razonsocial like '%{$razonSocial}%' ";
else if($nit != null)
	$strCondicionBusqueda = " AND nit like '%{$nit}%' ";

$sql="select * from (
		SELECT row_number() over(order by aportes048.idempresa asc) as contador, aportes048.idempresa, 
			aportes048.idtipodocumento,
			td.codigo, 
			aportes048.nit as nit_empresa,
			digito,
			razonsocial, 
			aportes048.direccion,
			seccional
		FROM 
			aportes048 
			INNER JOIN aportes091 td ON aportes048.idtipodocumento = td.iddetalledef 
		WHERE
			legalizada='S' AND 
			(auditado = 'N' OR auditado IS NULL) AND 
			seccional IS NOT NULL {$strCondicionBusqueda}) as c where c.contador between ($pagina-1) * $numRegistros + 1 AND $pagina * $numRegistros";

$rs = $db->querySimple($sql);
$filas=array();
while($w = $rs->fetch())
	$filas[] = $w;

if(count($filas) == 0)
	echo 0;
else
	echo json_encode($filas);
?>