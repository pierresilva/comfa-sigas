<?php
set_time_limit ( 0 );
ini_set ( "display_errors", '1' );
date_default_timezone_set ( "America/Bogota" );

$root = $_SERVER ['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once $root;

$v0 = $_REQUEST ['v0'];
$idp = $_REQUEST ['v1'];
$idbenef = $_REQUEST ['v2'];

include_once $raiz . DIRECTORY_SEPARATOR . 'pdo/persona.class.php';
include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'grupo.familiar.class.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'p.afiliacion.class.php';
include_once $raiz.DIRECTORY_SEPARATOR.'aportes'.DIRECTORY_SEPARATOR.'radicacion'.DIRECTORY_SEPARATOR.'clases'. DIRECTORY_SEPARATOR.'radicacion.class.php';

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

if(!isset($_SESSION['SMLV'])){
	$sqlSalario="select top 1 smlv from aportes012 where procesado='S' order by periodo desc";
	$rsSalario=$db->querySimple($sqlSalario);
	$rowSalario=$rsSalario->fetch();
	$smlv=intval($rowSalario['smlv']);
} else { $smlv=$_SESSION['SMLV']; }

$objPersona=new Persona();
$rowPersona = $objPersona->buscarPersona2(0, $idp, 10);
$rowPersona = $rowPersona[0];

$nomT = trim($rowPersona['pnombre']) . " " . trim($rowPersona['snombre']) . " ".trim($rowPersona['papellido'])." ".trim($rowPersona['sapellido']);
$estadoTrabajador=$rowPersona['estado'];
if ($estadoTrabajador=='M' && $rowPersona['fechadefuncion'].lenght>0) {
	$estadoTrabajador ="<a class='dialogoEmergente' >".$rowPersona['estado']."<span>".$rowPersona['fechadefuncion']."</span></a>";
}
?>

<html>
	<h5>Datos del Afiliado</h5>
	<table width="100%" border="0" cellspacing="0" class="tablero"
		id="tblDatosAfiliadoConvivencia">
		<tr>
			<th width="4%">TD</th>
			<th width="8%">N&uacute;mero</th>
			<th width="30%">Afiliado</th>
			<th width="4%">Estado</th>
			<th width="10%">Zona</th>
			<th width="18%">Direccion</th>
			<th width="8%">Tel</th>
			<th width="6%">F Nace</th>
			<th width="3%">Edad</th>
			<th width="3%">RUAF</th>
		</tr>
		<tr>
			<td style="text-align: center"><?php echo $rowPersona['tipodocumento']; ?></td>
			<td style="text-align: center"><?php echo $rowPersona['identificacion'];  ?></td>
			<td style="text-align: left"><?php echo $idp . " - " . $nomT; ?></td>
			<td style="text-align: center"><?php echo $estadoTrabajador;  ?></td>
			<td style="text-align: center"><?php echo $rowPersona['zona'];  ?></td>
			<td style="text-align: center"><?php echo $rowPersona['direccion'];  ?></td>
			<td style="text-align: center"><?php echo $rowPersona['telefono'];  ?></td>
			<td style="text-align: center"><?php echo $rowPersona['fechanacimiento'];  ?></td>
			<td style="text-align: center"><?php echo intval($rowPersona['dias']);  ?></td>
			<td style="text-align: center"><?php echo $rowPersona['ruaf'];  ?></td>
		</tr>
	</table>
	<br />
	
	<?php
		unset($rowPersona);
		
		$objAfiliacion=new Afiliacion();
		$conAfiliacion=$objAfiliacion->buscarFichaAfiliacionesActivasAfiliado($idp);
		
		$cont=mssql_num_rows($conAfiliacion);
		if(is_numeric($cont) && $cont>0){
			$salarioAcumulado=0;
	?>	
	
	<h5>Afiliaci&oacute;n Activa</h5>
	<table width="100%" border="0" cellspacing="0" class="tablero"
		id="tblDatosBeneficiarioConvivencia">
		<tr>
			<th width="6%">Rad.</th>
			<th width="6%">Grab.</th>
			<th width="2%">Nit</th>
			<th width="26%">Empresa</th>
			<th width="4%">F. Ingre</th>
			<th width="5%">Tipo Form.</th>
			<th width="5%">Tipo Afi.</th>
			<th width="3%">Horas</th>
			<th width="6%">Salario</th>
			<th width="14%">Cargo</th>
			<th width="2%">Ctr&iacute;a</th>
			<th width="4%">Est.</th>
			<th width="2%">Pri.</th>
			<th width="2%">Agr.</th>
		</tr>
		
		<?php				
			while($rowAfiliacion=mssql_fetch_array($conAfiliacion)){
				$radicado = $rowAfiliacion ["usuarioRadica"] . "<br/>" . $rowAfiliacion ["fecharadicacion"];
				$grabado = $rowAfiliacion ["usuarioGraba"] . "<br/>" . $rowAfiliacion ["fechaproceso"];
				$salarioAcumulado += $rowAfiliacion ['salario'];
				$obsPU = $rowAfiliacion ["estado"];
				if ($rowAfiliacion ["estado"] == 'PU') {
					$obsPU = "<center>
								<table border='0' cellspacing='0'>
									<tr>
										<td style='border: 0px; padding-right: 0px; padding-left: 0px;'> P </td>
										<td style='border: 0px; padding-right: 0px; padding-left: 0px;'>
										<img src='" . URL_PORTAL . "imagenes/messagebox_warning.png' style='height: 16px; width: 16px;' title='No se ha radicado, entr&oacute; por Planilla &Uacute;nica.' />
										</td>
									</tr>
								</table>
							</center>";
				}
				
				if ($rowAfiliacion ["idradicacion"] > 0) {
					$radicado = "<a style='cursor: pointer;' onclick='mostrarDocumentosRadicacion(" . $rowAfiliacion ['idradicacion'] . ")'>" . $radicado . "</a>";
				}				
			?>
		<tr style="<?php echo $estiloColorFondo; ?>">
			<td style="text-align: right"><?php echo $radicado ?></td>
			<td style="text-align: right"><?php echo $grabado ?></td>
			<td><a style="cursor: pointer;" onclick="mostrarDatosEmpresa(this.text)"><?php echo $rowAfiliacion['nit'];?></a></td>
			<td><?php echo $rowAfiliacion['razonsocial'];  ?></td>
			<td><?php echo $rowAfiliacion['fechaingreso'];  ?></td>
			<td><?php echo $rowAfiliacion['tipoformulario'];  ?></td>
			<td><?php echo $rowAfiliacion['clasef'];  ?></td>
			<td><?php echo $rowAfiliacion['horasmes'];  ?></td>
			<td style="text-align: center"><?php echo number_format($rowAfiliacion['salario']);  ?></td>
			<td style="text-align: center"><span title="<?php echo $rowAfiliacion['cargo'];?>"><?php echo $rowAfiliacion['nomcargo']; ?></span></td>
			<td style="text-align: center"><?php echo $rowAfiliacion['categoria'];  ?></td>
			<td style="text-align: center"><?php echo $obsPU; ?></td>
			<td style="text-align: center"><?php echo $rowAfiliacion['primaria']; ?></td>
			<td style="text-align: center"><?php echo $rowAfiliacion['agricola']; ?></td>
		</tr>
		<?php 			
			}
			
			$cat = (($salarioAcumulado) / $smlv);
			if ($cat <= 0) {
				$c = '';
			} else {
				if ($cat <= 2) {
					$c = 'A';
				} elseif ($cat <= 4) {
					$c = 'B';
				} else {
					$c = 'C';
				}
			}
			?> 
		<tr>
			<th colspan="6"></th>
			<th colspan="2">Salario Acumulado</th>
			<th><?php echo number_format($salarioAcumulado); ?></th>
			<th>Categoria</th>
			<th><?php echo $c; ?></th>
			<th colspan="6">&nbsp;</th>
		</tr>		
	</table>
	<br />
		
	<?php }		
		unset($rowAfiliacion);
		
		$objGrupo=new GrupoFamiliar();
		$grupo = $objGrupo->buscarBasicoConyuges($idp);
		
		$cont=mssql_num_rows($grupo);
		if(is_numeric($cont) && $cont>0) { 
	?>	
		
	<h5>Relaci&oacute;n de Convivencia</h5>
	<table width="100%" border="0" cellspacing="0" class="tablero"
		id="tblDatosConvivenciaConvivencia">
		<tr>
			<th width="4%">TD</th>
			<th width="8%">N&uacute;mero</th>
			<th width="39%">Conyuge</th>
			<th width="5%">Convive</th>
			<th width="7%">F Nace</th>
			<th width="12%">Ult. Cambio Conv.</th>
			<th width="5%">Labora</th>
			<th width="7%">Salario</th>
			<th width="7%">F Ingreso</th>
		</tr>
		
		<?php 
			while($rowGrupo=mssql_fetch_array($grupo)){
				$nom = utf8_encode($rowGrupo['pnombre']." ".$rowGrupo['snombre']." ".$rowGrupo['papellido']." ".$rowGrupo['sapellido']);
				$idc = $rowGrupo['idbeneficiario'];				
				$conv = $rowGrupo['conviven'];
				$fechaNacimiento = $rowGrupo['fechanacimiento'];
				$fechaUltimoCambioConv = $rowGrupo['fechasistema'];
				$labora=$rowGrupo['labora'];
				$salario=($rowGrupo['salario']==0?'':number_format($rowGrupo['salario']));
				$fechaAfi=$rowGrupo['fechaingreso'];
			?>       
       	<tr>       					
       		<td style="text-align:center"><?php echo $rowGrupo['codigo'];  ?></td>
			<td style="text-align:right" ><?php echo $rowGrupo['identificacion'];  ?></td>
			<td style="text-align:left" ><?php echo $rowGrupo['idbeneficiario']." - ".$nom; ?></td>
			<td style="text-align:center" ><?php echo $conv; ?></td>
			<td style="text-align:center" ><?php echo $fechaNacimiento; ?></td>
			<td style="text-align:center" ><?php echo $fechaUltimoCambioConv; ?></td>
			<td style="text-align:center" ><?php echo $labora;  ?></td>
			<td style="text-align:center" ><?php echo $salario;  ?></td>
			<td style="text-align:center" ><?php echo $fechaAfi; ?></td>
		</tr>
		<?php } ?> 
		
	</table><br />
	
	<?php 
		}
		 
		unset($row);
		
		$objRadicacion = new Radicacion();
		$consulta = $objRadicacion->buscarDocumentosRadicacion($v0);		
		
		$cont=mssql_num_rows($consulta);
		if(is_numeric($cont) && $cont>0) { 
	?>	
	
	<h5>Documentos Presentados</h5>
	<table width="100%" class="tablero" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff">
		<thead>
			<tr bgcolor="#EBEBEB">
				<td colspan="2" style="text-align: left">
					<strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Radicaci&oacute;n:&nbsp;<label id="lblIdDocumentoRadicacion"></label></strong>
				</td>
			</tr>
			<tr bgcolor="#DFE3EA" style="color: #566C7C; font-size: 10px; font-weight: bold;">
				<td style="text-align: center" width="90%">Documento.</td>
				<td style="text-align: center" width="10%">Cantidad</td>
			</tr>
		</thead>		
		
			<?php while($row=mssql_fetch_array($consulta)){ ?>
		<tr>
			<td style="text-align:left" ><?php echo $row['detalledefinicion']; ?></td>
			<td style="text-align:center" ><?php echo $row['cantidad']; ?></td>
  		</tr>
  			<?php } ?>
		
	</table> <br />
		
	<?php 
		}
		
		unset($row);
	?>
		
</html>