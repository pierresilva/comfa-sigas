/*
* @autor:      Oswaldo González
* @fecha:      15/08/2012
* objetivo:
*/

parametros="";
paginaG=1;
filas=10;

$(document).ready(function(){
	var URL=src();
	$("#buscarPor").change(function(){
		$("#pn,#pa,#identificacion").val('');
		if($(this).val()=='n'){
			$("#tipoDocumento").hide();
			$("#agencia1").hide();
			$("#agencia1").val('s')
			$("#pn,#pa").show();
			$("#identificacion").hide();
		}
		if($(this).val()=='i'){
			$("#tipoDocumento").show();
			$("#agencia1").hide();
			$("#agencia1").val('s')
			$("#pn,#pa").hide();
			$("#identificacion").show();
			$("#identificacion").focus();
		}
		if($(this).val()=='a'){
			$("#tipoDocumento").hide();
			$("#agencia1").show();
			$("#agencia1").val('s')
			$("#pn,#pa").hide();
			$("#identificacion").hide();
			$("#agencia1").focus();
		}
	});
	
	$("#buscarT").click(function(){
		if($("#identificacion").is(":visible") && $("#identificacion").val()==''){
			$(this).next("span.Rojo").html("Ingrese un valor a buscar.").hide().fadeIn("slow");
			$("#identificacion").focus();
			return false;
		}
		if($("#pn").is(":visible")&&$("#pn").val()==''&&$("#pa").val()==''){
			$(this).next("span.Rojo").html("Ingrese un valor a buscar.").hide().fadeIn("slow");
			$("#pn").focus();
			return false;
		}

		cargarAfiliacionesPendientes();
		
	});
		
	//carga los trabajdores de la BD y los muestra en la tabla
	cargarAfiliacionesPendientes();

	//Dialog ayuda
	$("#ayuda").dialog({
	 	autoOpen: false,
		height: 450,
		width: 600,
		draggable:true,
		modal:false,
		open: function(evt, ui){
				$('#ayuda').html('');
				$.get('../../help/aportes/ayudaRevision.html',function(data){
						$('#ayuda').html(data);
				});
		 }
	});
	
	//Dialog Colaboracion en linea
	$("#dialog-form2").dialog({
		autoOpen: false,
		height: 400,
		width: 500,
		modal: true,
		buttons: {
			'Enviar': function() {
			var bValid = true;
			var campo=$('#notas').val();
			var campo0=$.trim(campo);
			if (campo0==""){
				$(this).dialog('close');
					return false;
				}
			var campo1=$('#usuario').val();
			var campo2="radicacion.php";
			$.post(URL+'phpComunes/colaboracion.php',{v0:campo0,v1:campo1,v2:campo2},
			function(datos){
				if(datos=='1'){
					alert("Su comentario fue enviado correctamente!. Gracias por participar en el mejoramiento de SIGAS");
				}else{
					alert(datos);
				}
		});
		$(this).dialog('close');
		},
		Cancelar: function() {
			$(this).dialog('close');
			$("#dialog-form2").dialog("destroy");
		}
	}
	});

	//DIALOG DETALLLES TRABAJADOR
	$("#detalles").dialog({
	 	autoOpen: false,
		width: 750,
		show:"slow",
		hide:"slow",
		resizable:false,
		draggable:false,
		modal:true,
		open:function(){
		  
		}
		});
});//fin ready


//Dialog Ayuda
function mostrarAyuda(){
	$("#ayuda").dialog('open' );
	}

function notas(){
	$("#dialog-form2").dialog('open');
	}	

function detalles(event,obj){
	event.preventDefault();
	$(" #info").removeData("id");//Elimina el valor ID
	$(" #info").data("id",obj.innerHTML);//Almacena un valor del ID clickeado
	$("#p_nombre_afiliado").empty();
	$("#tGrupo tbody").empty();
	$("#detalles").dialog('open');
	var v0=$("#info").data('id');
	$.ajax({
		url: "masPersona.php",
		type: "POST",
		data: {v0:v0},
		async: false,
		dataType: "json",
		success: function(data){
			$("#p_nombre_afiliado").html(data[0].identificacion +" - "+ data[0].pnombre +" "+ data[0].snombre +" "+ data[0].papellido +" "+ data[0].sapellido);
			$("#tDatos2 tr:not(:first)").remove();
			$("#tDatos2").append("<tr><td>"+data[0].fechanacimiento+"</td><td>"+data[0].cnace+"</td><td>"+data[0].sexo+"</td><td>"+data[0].municipio+"</td><td>"+data[0].zona+"</td><td>"+data[0].telefono+"</td></tr>");
		},
		complete: function(){
			$.ajax({
				url:"masAfiliacion.php",
				type: "POST",
				data: {v0:v0},
				async: false,
				dataType: "json",
				success: function(datos){
					switch(datos[0].idagencia){
						case '01': var agencia='Neiva'; break;
						case '02': var agencia='Garzon'; break;
						case '03': var agencia='Pitalito'; break;
						case '04': var agencia='La Plata'; break;
					}
					$("#tAfiliacion tr:not(:first)").remove();
					$("#tAfiliacion").append("<tr><td title='"+datos[0].razonsocial+"'>"+datos[0].nit+"</td><td>"+datos[0].fechaingreso+"</td><td>"+datos[0].salario+"</td><td>"+datos[0].tfor+"</td><td>"+datos[0].tafi+"</td><td>"+agencia+"</td></tr>");
					
					$.ajax({
						url: URL+"aportes/trabajadores/buscarGruposFamiliares.php",
						type: "POST",
						data: {v0:v0},
						async: false,
						dataType: "json",
						success: function(datosGrupo){
							if(datosGrupo != 0){
								$.each(datosGrupo,function(c,beneficiario){
									beneficiario.conviven = (beneficiario.conviven == null)?'N/A':beneficiario.conviven;
									var strBenef = "<tr><td>"+ beneficiario.idpersona +"</td><td>"+ beneficiario.identificacion +"</td><td>"+ beneficiario.pnombre +" "+ beneficiario.snombre +" "+ beneficiario.papellido +" "+ beneficiario.sapellido +"</td><td>"+ beneficiario.parentesco +"</td><td>"+ beneficiario.fechanacimiento +" ("+ beneficiario.edad +")</td><td>"+ beneficiario.conviven +"</td><td>"+ beneficiario.parentesco +"</td><td>"+ beneficiario.estado +"</td></tr>";
									$("#tGrupo tbody").append(strBenef);
								});
							}
						}
					});
				}
			});
		}
	});
	
}	

function guardarRevision(){
	var seleccionadas = [];
	var auditadas = [];
	var contador = 0;
	 $("table.tablaR td input:checked").each(function(i){
	 	var ide = $(this).val();//extraigo el valor del value del check cargado dinamicamente
	 	seleccionadas[i] = ide;
	 	if(ide > 0){
	 		auditadas[contador] = ide;
	 		contador++;
	 	}
	 });
	 
 
 	if(auditadas.length > 0){
 		$.getJSON('marcarAuditado.php',{v0:auditadas.join()},function(data){
			if(data == 0)
				alert("Hubo problemas al actualizar los registros");
			else{
				if(data == auditadas.length && auditadas.length == seleccionadas.length)
					alert("Todas las radicaciones fueron auditadas.");
				else
					alert("Se Marcaron como Auditadas las Radicaciones: \n"+ auditadas.join() +". \n"+ (seleccionadas.length-auditadas.length) +" Registros no pudieron ser modificados.");
			}

			$("#tDatos tr:not(:first)").remove(); 
			cargarAfiliacionesPendientes();
 		});
 	}
}


function cargarAfiliacionesPendientes(){
	//$("#tDatos tbody").empty();
	var identificacion = $("#identificacion").val();
	var buscarPor = $("#buscarPor").val();
	var tipoDocumento = $("#tipoDocumento").val();
	var primerNombre = $("#pn").val();//campo nombre
	var primerApellido = $("#pa").val();//campo nombre
	var agencia = $("#agencia1").val();
	
	var strParametros = "";
	parametros="";

	if(buscarPor == 'i' && identificacion != '') // por identificación
		strParametros += "buscarPor=identificacion&tipoDocumento="+ tipoDocumento +"&numDocumento="+ identificacion;

	if(buscarPor == 'n') // por nombre
		strParametros += "buscarPor=nombre&nombre="+ primerNombre +"&apellido="+ primerApellido;
	
	if(buscarPor == 'a') // por nombre
		strParametros += "agencia="+ agencia;	
	
	parametros=strParametros;
	
	listarAfiliaP(parametros, paginaG, filas);	
}

function nuevoR(){
	$("input:text, select").not('button').val('');
	cargarAfiliacionesPendientes();
}

function listarAfiliaP(str, pag, rows){
	var cadena="";
	
	if(str.length==0)
		cadena="pagina="+pag+"&rows="+rows;
	else
		cadena=str+"&pagina="+pag+"&rows="+rows;
	
	$.ajax({
		type:"GET",
		url:"listar_afiliaPendientes.php",
		data:cadena,
		success:function(data)
		{
			$("#contenido").fadeIn(1000).html(data);
			$("table.tablaR tr:even").addClass("zebra");
			$("#cmbFilas").val(rows);

		  	//Seleccionar el check de la tabla
		  	$(".tablaR td img").toggle(function(){
		  		$(this).attr("src",URL+"imagenes/chk1.png");
		  		$(this).next("input:checkbox").attr("checked",true);
		  	},function(){
		  		$(this).attr("src",URL+"imagenes/chk0.png");
		  		$(this).next("input:checkbox").attr("checked",false);
		  	});
		}
	});
}

function cambiarCantRegistros(cant){
	filas=cant;
	
	listarAfiliaP(parametros, paginaG, filas);
}