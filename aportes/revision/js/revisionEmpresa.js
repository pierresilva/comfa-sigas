var paginaActual=null;
$(document).ready(function(){
	var URL = src();	
	
	//DIALOG DETALLES EMPRESA
	$("#div-datosEmpresa").dialog({
		autoOpen:false,
		width:740,
		hide: "clip",
		open:function(event,ui){
		},//fin funcion open
		close:function(event,ui){
			$("#idEmp,#sector,#clasesoci,#nit1,#digito1,#tipo_doc,#razonsocial,#codigosucursal,#sigla,#direccion,#departmento,#municipio,#telefono,#fax,#url").html("");
			$("#repLeg,#contacto,#contratista,#actividad,#actDane,#indicador,#idasesor,#seccional,#estado,#clase_apo,#tipo_apo,#fechaafiliacion,#fechaestado").html("");
			$("#fechamatricula,#fechaaportes,#usuario,#fechasistema").html("");					
		}
	});//fin dialog
	
	$("#ayuda").dialog({
		autoOpen: false,
		height: 450,
		width: 600,
		draggable:true,
		modal:false,
		open: function(evt, ui){
				$('#ayuda').html('');
			$.get('../../help/aportes/ayudaRevision.html',function(data){
				$('#ayuda').html(data);
			});
		}
	});
	
	$("#dialog-form2").dialog({
		autoOpen: false,
		height: 400,
		width: 500,
		modal: true,
		buttons: {
			'Enviar': function() {
				var bValid = true;
				var campo=$('#notas').val();
				var campo0=$.trim(campo);
				if (campo0 == ""){
					$(this).dialog('close');
						return false;
				}
				var campo1=$('#usuario').val();
				var campo2="radicacion.php";
				$.post(URL+'phpComunes/colaboracion.php',{v0:campo0,v1:campo1,v2:campo2},
				function(datos){
					if(datos=='1'){
						alert("Su comentario fue enviado correctamente!. Gracias por participar en el mejoramiento de SIGAS");
					}else{
						alert(datos);
					}
				});
				$(this).dialog('close');
			},
			Cancelar: function() {
				$(this).dialog('close');
				$("#dialog-form2").dialog("destroy");
			}
		}
	});
	
	$("#buscarEmpresa").click(function(){
		$("#tDatos tbody,tfoot").empty();
		//VALIDAR QUE EL NIT SEA NUMEROS
		if( $("#buscarPor").val()=="1"&& isNaN($("#idEmpresa").val()) ){
			$(this).next("span.Rojo").html("El NIT debe ser num\u00E9rico.").hide().fadeIn("slow");
				return false;
		}
		
		if($("#buscarPor").val()=='Seleccione..'){
			$(this).next("span.Rojo").html("Seleccione el criterio de b\u00FAsqueda.").hide().fadeIn("slow");
			return false;
		}

		if($("#idEmpresa").val()==''){
			$(this).next("span.Rojo").html("Ingrese el NIT \u00F3 RAZON SOCIAL").hide().fadeIn("slow");
			$("#idEmpresa").focus();
			return false;
		}

		cargarAfiliacionesPendientes(1,[$("#idEmpresa").val().toUpperCase(),$("#buscarPor").val()]);
	});//fin click
	
	
	cargarAfiliacionesPendientes(1,[$("#idEmpresa").val().toUpperCase(),$("#buscarPor").val()]);
	
}); // dom ready

function cargarAfiliacionesPendientes(pagina,parametros){
	paginaActual = pagina;
	$("#tDatos tbody").empty();
	var razonSocial = "";
	var nit = "";
	if(parametros.length == 2){
		// buscar por
		if(parametros[1] == "nit")
			nit = parametros[0];
		else if(parametros[1] == "razonsocial"){
			razonSocial = parametros[0];
		}
	}
	$.getJSON('contarAfiliaPendientesEmpresas.php?nit='+ nit +"&razonSocial="+ razonSocial,function(totalRegistros){
		var regsPorPagina = 20;
		var numPaginas = Math.ceil(totalRegistros/regsPorPagina);
		var strEnlacesPag = "";
		$("#span_num_empresas").html(totalRegistros);
		c=0;
		for(var i = pagina; i<=numPaginas; i++){
			if(c <= 10){
				strEnlacesPag += "<a href='#' id='pagina_"+ i +"' class='link_paginacion' >"+ i +"</a> ";
			}
			c++;	
		}
		
		var strEnlaceAnterior = "";
		if(pagina > 1)
			strEnlaceAnterior = "<a href='#' id='pagina_anterior'> << </a>";
		
		var strEnlaceSiguiente = "";
		if(pagina < numPaginas)
			strEnlaceSiguiente = "<a href='#' id='pagina_siguiente'> >> </a>";
		
		strEnlacesPag = strEnlaceAnterior + strEnlacesPag + strEnlaceSiguiente; 
		//carga las empresas de la BD
		$.getJSON('afiliaPendientesEmpresas.php?pagina='+ pagina +'&numRegistros='+regsPorPagina +'&nit='+ nit +"&razonSocial="+ razonSocial,function(data){
		  	if(data == 0){
		  		alert("no hay datos");
		  		return false;
		  	}
		  	$.each(data,function(i,fila){
				var agencia = 'N/A';
				switch(fila.seccional){
					case '01': agencia='Neiva'; break;
					case '02': agencia='Garzon'; break;
					case '03': agencia='Pitalito'; break;
					case '04': agencia='La Plata'; break;
				}

				var direccion = (fila.direccion == 'null' || fila.direccion == null)?'':fila.direccion;
				
				$("#tDatos").append("<tr><td><a href='#' onClick='detalles(event,this);'>"+fila.idempresa+"</a></td><td style=text-align:right>"+fila.nit_empresa+"</td><td style=text-align:left>"+fila.razonsocial+"</td><td style=text-align:left>"+direccion+"</td><td style=text-align:left>"+agencia+"</td><td><img src='"+ URL+"imagenes/chk0.png' /><input type='checkbox' value='"+fila.idempresa+"' style='display:none' /></td></tr>");
		  	});
		  	
		  	$("table.tablaR tr:even").addClass("zebra");
			//Dar un ID dinamico a los check box de la tabla de empresas
			$("table.tablaR td input:checkbox").each(function(index){
				$(this).attr({"id":"chk"+index,"value": $(this).parents("tr").children("td:first").find("a").html()}).hide();//id de los check
			});
			
			$("table.tablaR td img").each(function(index,imagen){
				$(imagen).attr({"id":"img"+index,"value": $(imagen).parents("tr").children("td:first").find("a").html()});//id de los check
				$(imagen).bind('click',function(){
					if($(imagen).next("input:checkbox").is(":checked")){
						$(imagen).attr("src",URL+"imagenes/chk0.png");
						$(imagen).next("input:checkbox").attr("checked",false);
					}else{
						$(imagen).attr("src",URL+"imagenes/chk1.png");
						$(imagen).next("input:checkbox").attr("checked",true);
					}
				});
			});
		});//end post
		
		$("#tDatos tfoot").html("<tr><td colspan='6'>"+ strEnlacesPag +"</td></tr>");
		$(".link_paginacion").each(function(ind,enlace){
			$(enlace).bind('click',function(){
				var pag = $(this).attr("id").replace("pagina_","");
				cargarAfiliacionesPendientes(pag,[]);
			});
		});
		$("#pagina_anterior").bind("click",function(){
			if(pagina > 1)
				cargarAfiliacionesPendientes(parseInt(pagina)-1,[$("#idEmpresa").val().toUpperCase(),$("#buscarPor").val()]);
		});
		
		$("#pagina_siguiente").bind("click",function(){
			if(pagina < numPaginas)
				cargarAfiliacionesPendientes(parseInt(pagina)+1,[$("#idEmpresa").val().toUpperCase(),$("#buscarPor").val()]);
		});
		
		$("#pagina_"+pagina).addClass("pagina_actual");
	});	
}

function detalles(event,obj){		
	var v0=obj.innerHTML;
	
	$.getJSON(URL+"phpComunes/buscarDatosEmpresaID.php", {v0:v0}, function(data){
		if(data){
			var idempresa1='';
			$.each(data,function(i,fila){				
				idempresa1=fila.idempresa;
				$("#sector").html(fila.sector);
				$("#clasesoci").html(fila.clasesoci);
				$("#nit1").html(idempresa1 + " - " + $.trim(fila.nit));
				$("#digito1").html($.trim(fila.digito));
				$("#tipo_doc").html($.trim(fila.tipo_doc)); 
				$("#razonsocial").html($.trim(fila.razonsocial));
				$("#codigosucursal").html(fila.codigosucursal);
				$("#sigla").html(fila.sigla);
				$("#direccion").html($.trim(fila.direccion));
				$("#departmento").html(fila.departmento);
				$("#municipio").html(fila.municipio);
				$("#telefono").html(fila.telefono);
				$("#fax").html(fila.fax);
				$("#url").html(fila.url);
				$("#repLeg").html($.trim(fila.par)+" "+$.trim(fila.sar)+" "+$.trim(fila.pnr)+" "+$.trim(fila.snr));
				$("#contacto").html($.trim(fila.pac)+" "+$.trim(fila.sac)+" "+$.trim(fila.pnc)+" "+$.trim(fila.snc));
				$("#contratista").html($.trim(fila.contratista));
				$("#actividad").html($.trim(fila.actividad));
				$("#actDane").html($.trim(fila.codact+" - "+fila.dane));
				$("#indicador").html(fila.indicador);
				$("#idasesor").html(fila.idasesor);
				$("#seccional").html(fila.seccional);
				$("#estado").html(fila.estado);
			    $("#clase_apo").html(fila.clase_apo);
				$("#tipo_apo").html(fila.tipo_apo);
				$("#fechaafiliacion").html(fila.fechaafiliacion);
				$("#fechaestado").html(fila.fechaestado);
				$("#fechamatricula").html(fila.fechamatricula);
				$("#fechaaportes").html(fila.fechaaportes);
				$("#usuario").html(fila.usuario);
				$("#fechasistema").html(fila.fechasistema);			
			});
			$.getJSON(URL+"phpComunes/buscarObservacion.php", {v0:idempresa1,v1:2}, function(data){
				if(data){
					$.each(data,function(j,fila1){
						$("#observas").html(fila1);
					});
				}
			});

			$("#div-datosEmpresa").dialog('open');
		}
	});
}

function guardarRevision(){
	var seleccionadas = [];
	var auditadas = [];
	var contador = 0;
	$("table.tablaR td input:checked").each(function(i){
		var ide = $(this).val();//extraigo el valor del value del check cargado dinamicamente
		seleccionadas[i] = ide;
		if(ide > 0){
			auditadas[contador] = ide;
			contador++;
		}
	});
	 
 
 	if(auditadas.length > 0){
 		$.getJSON('marcarAuditadoEmpresa.php',{v0:auditadas.join()},function(data){
			if(data == 0)
				alert("Hubo problemas al actualizar los registros");
			else{
				if(data == auditadas.length && auditadas.length == seleccionadas.length)
					alert("Todas las empresas seleccionadas fueron auditadas.");
				else
					alert("Se Marcaron como Auditadas las empresas: \n"+ auditadas.join() +". \n"+ (seleccionadas.length-auditadas.length) +" Registros no pudieron ser modificados.");
			}
			cargarAfiliacionesPendientes(paginaActual,[$("#idEmpresa").val().toUpperCase(),$("#buscarPor").val()]);
 		});
 	}else
 		alert("Debe seleccionar al menos una empresa a auditar.");
}

function mostrarAyuda(){
	$("#ayuda").dialog('open' );
}

function notas(){
	$("#dialog-form2").dialog('open');
}