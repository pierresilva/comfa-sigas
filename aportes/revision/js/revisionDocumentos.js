parametros="";
paginaG=1;
filas=10;

$(document).ready(function(){
	var URL=src();
	
	$("#paginar").live("click", function(){
		$("#contenido").html("<br/><div align='center'><img src='../../imagenes/ajax-loader-fon.gif'/></div>");
			var pagina=$(this).attr("data");
			paginaG=pagina;

			listarDocumentosP(parametros, paginaG, filas);
    });
	
	$("#buscarPor").change(function(){
		$("#pn,#pa,#identificacion").val('');
		if($(this).val()=='n'){
			$("#tipoDocumento, #agencia1, #identificacion").hide();
			$("#agencia1").val('s');
			$("#pn,#pa").show();
		} else if($(this).val()=='i'){
			$("#tipoDocumento, #identificacion").show();
			$("#agencia1,#pn,#pa").hide();
			$("#agencia1").val('s');
			$("#identificacion").focus();
		} else if($(this).val()=='a'){
			$("#tipoDocumento, #identificacion, #pn, #pa").hide();
			$("#agencia1").show();
			$("#agencia1").val('s');
			$("#agencia1").focus();
		}
	});
	
	$("#buscarT").click(function(){
		if($("#identificacion").is(":visible") && $("#identificacion").val()==''){
			$(this).next("span.Rojo").html("Ingrese un valor a buscar.").hide().fadeIn("slow");
			$("#identificacion").focus();
			return false;
		}
		if($("#pn").is(":visible")&&$("#pn").val()==''&&$("#pa").val()==''){
			$(this).next("span.Rojo").html("Ingrese un valor a buscar.").hide().fadeIn("slow");
			$("#pn").focus();
			return false;
		}

		cargarDocumentosPendientes();
		
	});
		
	cargarDocumentosPendientes();
	activeDialogs();
});


function cargarDocumentosPendientes(){
	var identificacion = $("#identificacion").val();
	var buscarPor = $("#buscarPor").val();
	var tipoDocumento = $("#tipoDocumento").val();
	var primerNombre = $("#pn").val();
	var primerApellido = $("#pa").val();
	var agencia = $("#agencia1").val();
	
	var strParametros = "";
	parametros="";

	if(buscarPor == 'i' && identificacion != '') {
		strParametros += "buscarPor=identificacion&tipoDocumento="+ tipoDocumento +"&numDocumento="+ identificacion;
	} else if(buscarPor == 'n') {
		strParametros += "buscarPor=nombre&nombre="+ primerNombre +"&apellido="+ primerApellido;
	} else if(buscarPor == 'a') {
		strParametros += "agencia="+ agencia;
	}
	
	parametros=strParametros;
	
	listarDocumentosP(parametros, paginaG, filas);	
}


function listarDocumentosP(str, pag, rows){
	var cadena="";
	
	if(str.length==0) {
		cadena = "pagina=" + pag + "&rows=" + rows;
	} else {
		cadena = str + "&pagina=" + pag + "&rows=" + rows;
	}
	
	$.ajax({
		type:"GET",
		url:"listar_documentosRevisionPendientes.php",
		data:cadena,
		success:function(data)
		{
			$("#contenido").fadeIn(1000).html(data);
			$("table.tablaR tr:even").addClass("zebra");
			$("#cmbFilas").val(rows);

		  	//Seleccionar el check de la tabla
		  	$(".tablaR td img").toggle(function(){
		  		$(this).attr("src",URL+"imagenes/chk1.png");
		  		$(this).next("input:checkbox").attr("checked",true);
		  	},function(){
		  		$(this).attr("src",URL+"imagenes/chk0.png");
		  		$(this).next("input:checkbox").attr("checked",false);
		  	});
		}
	});
}

/***** DETALLES DEL DOCUMENTO *****/

function detalles ( event, obj, idradicacion, idtiporadicacion, idpersona, idbeneficiario ) {
	
	switch (idtiporadicacion) {
		case 32:
			traerDatosDocsCertificado( $("#divDocumentos"), idradicacion, idpersona, idbeneficiario );
			break;
		case 70: 
			traerDatosDocsGrupoFamiliar( $("#divDocumentos"), idradicacion, idpersona, idbeneficiario );
			
			break;
		case 191: 
		case 211:
			traerDatosDocsConvivencia( $("#divDocumentos"), idradicacion, idpersona, idbeneficiario );
			break;
	}
}

/***** LLENAR DIALOGO *****/

function traerDatosDocsCertificado ( div, idradicacion, idpersona, idbeneficiario ) {
	div.empty();	
	if(idpersona>0){
		div.dialog({
			title:"Detalle Documentos Certificado",
			width: 920,
			modal: true,
			resizable:true,
			position: [ "top", 20 ],
			closeOnEscape:true,
			open: function(){
				$(this).html('');
				$(this).load( 'php/detalleDocsCertificado.php',
						{ v0: idradicacion, v1:idpersona, v2:idbeneficiario },
						function(data){
							$(this).html(data);
				});
			},
			close: function() {
				$(this).dialog("destroy");
			}
		});
	}
}

function traerDatosDocsConvivencia ( div, idradicacion, idpersona, idbeneficiario ) {
	div.empty();	
	if(idpersona>0){
		div.dialog({
			title:"Detalle Documentos Convivencia",
			width: 920,
			modal: true,
			resizable:true,
			position: [ "top", 20 ],
			closeOnEscape:true,
			open: function(){
				$(this).html('');
				$(this).load( 'php/detalleDocsConvivencia.php',
						{ v0: idradicacion, v1:idpersona, v2:idbeneficiario },
						function(data){
							$(this).html(data);
				});
			},
			close: function() {
				$(this).dialog("destroy");
			}
		});
	}
}

function traerDatosDocsGrupoFamiliar ( div, idradicacion, idpersona, idbeneficiario ) {
	div.empty();	
	if(idpersona>0){
		div.dialog({
			title:"Detalle Documentos Convivencia",
			width: 920,
			modal: true,
			resizable:true,
			position: [ "top", 20 ],
			closeOnEscape:true,
			open: function(){
				$(this).html('');
				$(this).load( 'php/detalleDocsGrupoFamiliar.php',
						{ v0: idradicacion, v1:idpersona, v2:idbeneficiario },
						function(data){
							$(this).html(data);
				});
			},
			close: function() {
				$(this).dialog("destroy");
			}
		});
	}
}

/***** MARCAR AUDITADO *****/

function guardarRevision(){
	var seleccionadas = [];
	var auditadas = [];
	var contador = 0;
	 $("table.tablaR td input:checked").each(function(i){
	 	var ide = $(this).val();//extraigo el valor del value del check cargado dinamicamente
	 	seleccionadas[i] = ide;
	 	if(ide > 0){
	 		auditadas[contador] = ide;
	 		contador++;
	 	}
	 });
	 
 
 	if(auditadas.length > 0){
 		$.getJSON('marcarAuditadoDocumento.php',{v0:auditadas.join()},function(data){
			if(data == 0)
				alert("Hubo problemas al actualizar los registros");
			else{
				if(data == auditadas.length && auditadas.length == seleccionadas.length)
					alert("Todas las radicaciones fueron auditadas.");
				else
					alert("Se Marcaron como Auditadas las Radicaciones: \n"+ auditadas.join() +". \n"+ (seleccionadas.length-auditadas.length) +" Registros no pudieron ser modificados.");
			}

			$("#tDatos tr:not(:first)").remove(); 
			cargarDocumentosPendientes();
 		});
 	}
}

/***** DIALOGOS ******/

function activeDialogs(){
	$("#div-datosEmpresa").dialog({
		autoOpen:false,
		width:740,
		hide: "clip",	
		open:function(event,ui){
		},//fin funcion open
		
		close:function(event,ui){
			$("#idEmp").html("");
			$("#sector").html("");
			$("#clasesoci").html("");
			$("#nit1").html("");
			$("#digito1").html("");
			$("#tipo_doc").html(""); 
			$("#razonsocial").html("");
			$("#codigosucursal").html("");
			$("#sigla").html("");
			$("#direccion").html("");
			$("#departmento").html("");
			$("#municipio").html("");
			$("#telefono").html("");
			$("#fax").html("");
			$("#url").html("");
			$("#repLeg").html("");
			$("#contacto").html("");
			$("#contratista").html("");
			$("#actividad").html("");
			$("#actDane").html("");
			$("#indicador").html("");
			$("#idasesor").html("");
			$("#seccional").html("");
			$("#estado").html("");
		    $("#clase_apo").html("");
			$("#tipo_apo").html("");
			$("#fechaafiliacion").html("");
			$("#fechaestado").html("");
			$("#fechamatricula").html("");
			$("#fechaaportes").html("");
			$("#usuario").html("");
			$("#fechasistema").html("");			
		}
	});
	
	$("#div-datosDocumentosRadicacion").dialog({
		autoOpen:false,
		width:640,
		show: "drop",
		hide: "clip",
		close: function(event,ui){
			$("#lblIdDocumentoRadicacion").html("");
			$("#div-datosDocumentosRadicacion table tbody").empty();
		}
	});
	
	$("#div-datosCertificados").dialog({
		autoOpen:false,
		width:640,
		show: "drop",
		hide: "clip",	
		open:function(event,ui){
		},//fin funcion open
		close: function(event,ui){
			$("#div-datosCertificados table tbody").empty();
		}
	});	
}


/***** UTILES *****/

function nuevoR(){
	$("input:text, select").not('button').val('');
	cargarDocumentosPendientes();
}

function cambiarCantRegistros(cant){
	filas=cant;
	
	listarDocumentosP(parametros, paginaG, filas);
}

/***** DATOS DIALOGO *****/

function mostrarDatosEmpresa(nit){
	$.getJSON( URL + "phpComunes/buscarDatosEmpresa.php", { v0:nit }, function( data ){
		if(data){
			var idempresa1='';
			$.each(data,function(i,fila){				
				idempresa1=fila.idempresa;
				$("#sector").html(fila.sector);
				$("#clasesoci").html(fila.clasesoci);
				$("#nit1").html(idempresa1 + " - " + $.trim(fila.nit));
				$("#digito1").html($.trim(fila.digito));
				$("#tipo_doc").html($.trim(fila.tipo_doc)); 
				$("#razonsocial").html($.trim(fila.razonsocial));
				$("#codigosucursal").html(fila.codigosucursal);
				$("#sigla").html(fila.sigla);
				$("#direccion").html($.trim(fila.direccion));
				$("#departmento").html(fila.departmento);
				$("#municipio").html(fila.municipio);
				$("#telefono").html(fila.telefono);
				$("#fax").html(fila.fax);
				$("#url").html(fila.url);
				$("#repLeg").html($.trim(fila.par)+" "+$.trim(fila.sar)+" "+$.trim(fila.pnr)+" "+$.trim(fila.snr));
				$("#contacto").html($.trim(fila.pac)+" "+$.trim(fila.sac)+" "+$.trim(fila.pnc)+" "+$.trim(fila.snc));
				$("#contratista").html($.trim(fila.contratista));
				$("#actividad").html($.trim(fila.actividad));
				$("#actDane").html($.trim(fila.codact+" - "+fila.dane));
				$("#indicador").html(fila.indicador);
				$("#idasesor").html(fila.idasesor);
				$("#seccional").html(fila.seccional);
				$("#estado").html(fila.estado);
			    $("#clase_apo").html(fila.clase_apo);
				$("#tipo_apo").html(fila.tipo_apo);
				$("#fechaafiliacion").html(fila.fechaafiliacion);
				$("#fechaestado").html(fila.fechaestado);
				$("#fechamatricula").html(fila.fechamatricula);
				$("#fechaaportes").html(fila.fechaaportes);
				$("#usuario").html(fila.usuario);
				$("#fechasistema").html(fila.fechasistema);			
			});
			$.getJSON(URL+"phpComunes/buscarObservacion.php", {v0:idempresa1,v1:2}, function(data){
				if(data){
					$.each(data,function(j,fila1){
						$("#observas").html(fila1);
					});
				}
			});

			$("#div-datosEmpresa").dialog('open');
		}
	});	
}

function mostrarDocumentosRadicacion(idRadicacion){
	$("#lblIdDocumentoRadicacion").html(idRadicacion);
	$("#div-datosDocumentosRadicacion table tbody").empty();

	$.ajax({
		url: URL+"phpComunes/buscarDocumentosRadicacion.php",
		async: false,
		dataType: 'json',
		data: ({v0: idRadicacion}),
		success: function(data){
			if(data.length > 0){
				$("#div-datosDocumentosRadicacion").dialog('open');
				$.each(data,function(i,fila){
					var strTr = "<tr><td>"+ fila.detalledefinicion +"</td><td style='text-align: center;'>"+ fila.cantidad +"</td></tr>";
					$("#div-datosDocumentosRadicacion table tbody").append(strTr);
				});
			} else {
				$("#div-datosDocumentosRadicacion").dialog('open');
				$("#div-datosDocumentosRadicacion table tbody").append("<tr><td colspan='2'><strong class='Rojo'>&nbsp;&nbsp;&nbsp;NO SE ENCONTR&Oacute; DOCUMENTOS PRESENTADOS</strong></td></tr>");
			}
		}
	});
}

function mostrarDatosCertificados(idBeneficiario){
	$("#div-datosCertificados table tbody").empty();

	$.ajax({
		url: URL + "phpComunes/buscarCertificados.php",
		async: false,
		dataType: 'json',
		data: ({idBeneficiario: idBeneficiario}),
		success: function(data){
			if(data.length > 0){
				$("#div-datosCertificados").dialog('open');
				$.each(data,function(i,fila){
					var strTr = "<tr><td>"+ fila.idcertificado +"</td><td>"+ fila.tipo_certificado +"</td><td>"+ fila.periodoinicio +"</td><td>"+ fila.periodofinal +"</td><td>"+ fila.fechapresentacion +"</td></tr>";
					$("#div-datosCertificados table tbody").append(strTr);
				});
			}
		}
	});
}