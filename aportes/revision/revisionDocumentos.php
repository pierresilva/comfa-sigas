<?php
set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<HTML lang="es">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<title> :: Revisi&oacute;n de Documentos :: </title>
		<link type="text/css" href="<?php echo URL_PORTAL; ?>newcss/Estilos.css" rel="stylesheet">
		<link type="text/css" href="<?php echo URL_PORTAL; ?>newcss/css.1.8/jquery-ui-1.8.17.custom.css" rel="stylesheet">
		<link type="text/css" href="<?php echo URL_PORTAL; ?>newcss/marco.css" rel="stylesheet">
		
		<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/1.6/jquery-1.6.2.min.js"></script>
		<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/1.6/jquery-ui-1.8.16.custom.min.js"></script>

		<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/comunes.js"></script>
		<script type="text/javascript" src="js/revisionDocumentos.js"></script>
		
	
		<style type="text/css">
			#controls { margin:0 auto; height:20px; }
			#perpage { float:left; width:175px; margin-left:40px; }
			#perpage select { float:left; font-size:11px; }
			#perpage label { margin:2px 0 0 5px; font-size:10px }
			#navigation { float:left;/* width:600px;*/ text-align:center }
			#navigation img { cursor:pointer; }
			#text {float:left; width:100px; text-align:left; margin-first:2px; font-size:10px }
		</style>
	

	</head>

	<body>
		<center>
			<br><br>
			<table width="90%" border="0" cellspacing="0" cellpadding="0" class="ui-corner-all">
				<tr>
					<td width="13" height="29" background="<?php echo URL_PORTAL; ?>imagenes/arriba_izq.gif">&nbsp;</td>
					<td background="<?php echo URL_PORTAL; ?>imagenes/arriba_central2.gif">
						<span class="letrablanca">::&nbsp;Revisi&oacute;n de Documentos&nbsp;::</span>
					</td>
					<td width="13" background="<?php echo URL_PORTAL; ?>imagenes/arriba_der.gif" align="right">&nbsp;</td>
				</tr>
				<tr>
					<td background="<?php echo URL_PORTAL; ?>imagenes/tabla/izquierda2.jpg">&nbsp;</td>
					<td background="<?php echo URL_PORTAL; ?>imagenes/centro.gif">
						<img src="<?php echo URL_PORTAL; ?>imagenes/spacer.gif" width="1" height="1"/>
						<img src="<?php echo URL_PORTAL; ?>imagenes/menu/nuevo.png" width="16" height="16" style="cursor:pointer" title="Nuevo" onclick="nuevoR();">
						<img src="<?php echo URL_PORTAL; ?>imagenes/spacer.gif" width="1" height="1">
						<img src="<?php echo URL_PORTAL; ?>imagenes/menu/grabar.png" width:16 height="16" style="cursor:pointer" title="Guardar" onClick="guardarRevision();"/>						
					</td>
					<td background="<?php echo URL_PORTAL; ?>imagenes/tabla/derecha.gif">&nbsp;</td>
				</tr>
				<tr>
					<td background="<?php echo URL_PORTAL; ?>imagenes/tabla/izquierda2.jpg">&nbsp;</td>
					<td align="center" background="<?php echo URL_PORTAL; ?>imagenes/centro.gif">
						<div id="resultado" style="font-weight: bold;font-size: 14px;color:#FF0000"></div>
					</td>
					<td background="<?php echo URL_PORTAL; ?>imagenes/tabla/derecha.gif">&nbsp;</td>
				</tr>
				<tr>
					<td background="<?php echo URL_PORTAL; ?>imagenes/tabla/izquierda2.jpg">&nbsp;</td>
					<td  background="<?php echo URL_PORTAL; ?>imagenes/centro.gif" >
						<h5>DOCUMENTOS A AUDITAR</h5>
						<table width="100%" border="0" cellspacing="0" class="tablero">
							<tr>
								<td width="40">Buscar Por:</td>
								<td width="50">
									<select name="buscarPor" class="box1" id="buscarPor">
										<option value="i" selected="selected">IDENTIFICACION</option>
										<option value="n">NOMBRE COMPLETO</option>
										<option value="a">AGENCIA</option>
									</select>
								</td>
								<td width="169">
									<select id="tipoDocumento" name="tipoDocumento" class="box1">
										<option value="1" selected="selected">C&eacute;dula ciudadan�a</option>
										<option value="3">Pasaporte</option>
										<option value="4">C&eacute;dula extranjer&iacute;a</option>
									</select>				
									<input name="identificacion" type="text" class="box" id="identificacion" onblur="validarLongNumIdent(document.getElementById('tipoDocumento').value,this)" onkeyup="validarCaracteresPermitidos(document.getElementById('tipoDocumento').value,this);"/> 
									<input type="text" class="box" id="pn" style="display:none" /> -
									<input type="text" class="box" id="pa" style="display:none" />
				
									<select id="agencia1" name="agencia1" class="box1" hidden="">
										<option value="s" selected="selected">Seleccione Agencia</option>
										<option value="01">Neiva</option>
										<option value="02">Garz&oacute;n</option>
										<option value="03">Pitalito</option>
										<option value="04">La Plata</option>
									</select>
								</td>
								<td width="116">
									<input name="buscarT" type="button" class="ui-state-default" id="buscarT" value="Buscar" />
									<span class="Rojo"></span>
								</td>
							</tr>
						</table>	
						<div id="contenido"></div>     
					</td>
					<td background="<?php echo URL_PORTAL; ?>imagenes/tabla/derecha.gif">&nbsp;</td>
				</tr>
				<tr>
					<td height="38" background="<?php echo URL_PORTAL; ?>imagenes/abajo_izq2.gif">&nbsp;</td>
					<td background="<?php echo URL_PORTAL; ?>imagenes/abajo_central.gif"></td>
					<td background="<?php echo URL_PORTAL; ?>imagenes/abajo_der.gif">&nbsp;</td>
				</tr>
			</table>
		</center>
		<input type="hidden" name="pageNum" value="0">
		<input type="hidden" name="valorp" id="valorp" >
		<input type="hidden" name="txtIdCliente" value="">
 
<!-- DIALOGOS -->
		
		<!-- DIALOGO CERTIFICADOS -->
		<div id="divDocumentos" style="display:none;"></div>
		
		<div id="div-datosEmpresa" title="::DATOS DE EMPRESA::" style="display:none">	
			<table width="100%" class="tablero" >
				<tr>
					<td>Sector</td>
					<td id="sector" colspan="2"></td>
					<td>Clase de Sociedad</td>
					<td id="clasesoci"></td>
				</tr>
				<tr>
					<td width="15%"><label for="iden">NIT</label></td>
					<td width="15%" id="nit1"><label class="box1" ></label></td>
					<td width="14%" id="digito1">D&iacute;gito <label ></label></td>
					<td width="12%">Tipo Documento</td>
					<td width="30%" id="tipo_doc"><label></label></td>			
				</tr>
				<tr>
					<td>Raz&oacute;n Social</td>
					<td id="razonsocial" colspan="3"><label ></label></td>
					<td id="codigosucursal">C&oacute;digo Sucursal - <label ></label></td>
				</tr>
				<tr>
					<td>Nombre Comercial</td>
					<td id="sigla" colspan="4"><label ></label></td>
				</tr>
				<tr>
					<td>Direcci&oacute;n</td>
					<td id="direccion" colspan="4"><label ></label></td>
				</tr>
				<tr>
					<td>Departamento</td>
					<td id="departmento" colspan="2"></td>
					<td>Ciudad</td>
					<td id="municipio"><label ></label></td>
				</tr>
				<tr>
					<td>Tel&eacute;fono</td>
					<td id="telefono" colspan="2"><label ></label></td>
					<td>Fax</td>
					<td id="fax"><label ></label></td>
				</tr>
				<tr>
					<td>URL</td>
					<td id="url" colspan="2"><label ></label></td>
					<td>Email</td>
					<td id="email"><label ></label></td>
				</tr>
				<tr>
					<td> Representante Legal</td>
					<td id="repLeg" colspan="4"></td>
				</tr>
				<tr>
					<td>Contacto</td>
					<td id="contacto" colspan="4"></td>
				</tr>
				<tr>
		  			<td>Contratista</td>
		  			<td id="contratista" colspan="2"><label ></label></td>
		  			<td>Actividad</td>
		  			<td id="actividad"></td>
				</tr>
				<tr>
		  			<td>Actividad Econ&oacute;mica DANE</td>
		  			<td id="actDane" colspan="4"></td>
				</tr>
				<tr>
		  			<td>&Iacute;ndice Aporte</td>
		  			<td id="indicador" colspan="2"><label></label></td>
		  			<td>Asesor</td>
		  			<td id="idasesor"><label></label></td>
				</tr>
				<tr>
					<td>Seccional</td>
					<td id="seccional" colspan="2"><label ></label></td>
					<td>Estado</td>
					<td id="estado"><label ></label></td>
				</tr>
				<tr>
					<td>Clase Aportante</td>
					<td id="clase_apo" colspan="2"></td>
					<td>Tipo Aportante</td>
					<td id="tipo_apo"></td>
				</tr>
				<tr>
					<td>Fecha Afiliaci&oacute;n</td>
					<td id="fechaafiliacion" colspan="2"><label ></label></td>
					<td>Fecha Estado</td>
					<td id="fechaestado"><label ></label></td>
				</tr>
				<tr>
		  			<td>Fecha Constituci&oacute;n</td>
		  			<td id="fechamatricula" colspan="2"></td>
		  			<td>Fecha Inicio Aportes</td>
		  			<td id="fechaaportes"><label ></label></td>
				</tr>
				<tr>
					<td>Usuario</td>
					<td id="usuario" colspan="2"><label ></label></td>
					<td>Fecha Grabación</td>
					<td id="fechasistema"></td>
				</tr>
				<tr>
					<td>Observaciones</td>
					<td id="observas" colspan="4"></td>
				</tr>
			</table>
		</div>
		
		<div id="div-datosCertificados" title="::HISTORIAL DE CERTIFICADOS::" style="display:none">
			<table width="100%" class="tablero" >
				<thead>
					<tr>
						<th>Id Cert.</th>
						<th>Tipo</th>
						<th>Peri&oacute;do Inicial</th>
						<th>Peri&oacute;do Final</th>
						<th>Fecha Presentaci&oacute;n</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
		
		<div id="div-datosDocumentosRadicacion" title="::DOCUMENTOS PRESENTADOS::" style="display:none">
			<table width="100%" class="tablero" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff">
				<thead>
					<tr bgcolor="#EBEBEB">
						<td colspan="2" style="text-align: left">
							<strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Radicaci&oacute;n:&nbsp;<label id="lblIdDocumentoRadicacion"></label></strong>
						</td>
					</tr>
					<tr bgcolor="#DFE3EA" style="color: #566C7C; font-size: 10px; font-weight: bold;">
						<td style="text-align: center" width="90%">Documento.</td>
						<td style="text-align: center" width="10%">Cantidad</td>
					</tr>
				</thead>		
				<tbody></tbody>
			</table>
		</div>
		
	</body>
</html>
