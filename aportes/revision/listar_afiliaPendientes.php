<?php

set_time_limit(0);
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'funcionesComunes'.DIRECTORY_SEPARATOR.'funcionesComunes.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
include_once $raiz .DIRECTORY_SEPARATOR. 'clases' .DIRECTORY_SEPARATOR. 'p.afiliacion.class.php';

$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

$objClase = new Afiliacion();

$usuario = $_SESSION["USUARIO"];
$buscarPor = isset($_REQUEST["buscarPor"])?$_REQUEST["buscarPor"]:null;
$agencia = isset($_REQUEST["agencia"])?$_REQUEST["agencia"]:"s";;
$tipoIdentificacion = null;
$identificacion = null;
$nombre = null;
$apellido = null;
$nombre = null;
$strCondicionBusqueda = "";

if($buscarPor == 'identificacion'){
	$tipoIdentificacion = $_REQUEST["tipoDocumento"];
	$identificacion = $_REQUEST["numDocumento"];
	$strCondicionBusqueda = " AND aportes015.idtipodocumento = ". $tipoIdentificacion ." AND aportes015.identificacion='". $identificacion ."' ";
}

if($buscarPor == 'nombre'){
	$nombre = $_REQUEST["nombre"];
	$apellido = $_REQUEST["apellido"];

	$arrCondNombre = array();

	if($nombre != "")
		$arrCondNombre["nom"] = "aportes015.pnombre LIKE '%{$nombre}%'";

	if($apellido != "")
		$arrCondNombre["ape"] = "aportes015.papellido LIKE '%{$apellido}%'";

	if(count($arrCondNombre)>0)
		$strCondicionBusqueda = " AND (".implode(" AND ",$arrCondNombre) .")";
}

if($agencia!='s')
	$str=" AND idagencia='$agencia'";
else 
	$str=" $strCondicionBusqueda ";

$consulta_cantidad=$objClase->Count_Afil_Auditar($str);
$resultados_cantidad2=mssql_fetch_array($consulta_cantidad);
$resultados_cantidad=$resultados_cantidad2["cant"];

 if ($resultados_cantidad>0)
 {
    $filas_pagina=10;
    $numero_pagina=1;

    if(isset($_GET["pagina"]))
    {
        sleep(1);
        $numero_pagina=$_GET["pagina"];
    }
    
    if(isset($_GET["rows"]))
    	$filas_pagina=$_GET["rows"];
	
    $total_registros=ceil($resultados_cantidad/$filas_pagina);
    
    if($numero_pagina>$total_registros)
    	$numero_pagina=$total_registros;
    
    $campo_de_inicio=($numero_pagina-1)*$filas_pagina;
    $campo_fin=$campo_de_inicio+$filas_pagina;    
    
    echo "<div align='center'>";
    
    echo "<table width='100%' border='0' cellpadding='0' cellspacing='0' class='tablaR hover' id='tDatos'  >";
    echo "<thead><tr>";    
    echo "<th width='5%' height='20' align='center' > <strong>ID</strong></th>";
    echo "<th width='5%'  align='center' ><strong>Rad</strong></th>";
    echo "<th width='8%' align='center'><strong>T.D.</strong></th>";
    echo "<th width='10%' align='center'><strong>N&uacute;mero</strong></th>";
    echo "<th width='26%' align='center'><strong>Nombres</strong></th>";
    echo "<th width='22%'><strong>Direcci&oacute;n</strong></th>";
    echo "<th width='9%'><strong>Agencia</strong></th>";
    echo "<th width='8%'><strong>Empleador</strong></th>";
    echo "<th width='7%'><strong>Aud</strong></th>";
    echo "</tr></thead>";   
    
    $consulta=$objClase->Select_Afil_Auditar($str, $campo_de_inicio, $campo_fin);
    while ($resultados=mssql_fetch_array($consulta))
    {    	
    	$nom=$resultados["pnombre"]." ".$resultados["snombre"]." ".$resultados["papellido"]." ".$resultados["sapellido"];
    	$agencia = 'N/A';
    	switch($resultados["idagencia"]){
    		case '01': $agencia='Neiva'; break;
    		case '02': $agencia='Garzon'; break;
    		case '03': $agencia='Pitalito'; break;
    		case '04': $agencia='La Plata'; break;
    	}
    	
    	$idRadicacion = (intVal($resultados["idradicacion"]) > 0)?$resultados["idradicacion"]:'N/A';
    	$direccion = ($resultados["direccion"] == 'null' || $resultados["direccion"] == null)?'':$resultados["direccion"];   	
    	
    	echo "<tr>";
    	echo "<td><a href='#' onClick='detalles(event,this);'>".$resultados["idpersona"]."</a></td>";
    	echo "<td>".$idRadicacion."</td>";
    	echo "<td>".$resultados["codigo"]."</td>";
    	echo "<td style=text-align:right>".$resultados["identificacion"]."</td>";
    	echo "<td style=text-align:left>".$nom."</td>";
    	echo "<td style=text-align:left>".$direccion."</td>";
    	echo "<td style=text-align:left>".$agencia."</td>";
    	echo "<td style=text-align:right>".$resultados["nit"]."</td>";
    	echo "<td><img src='../../imagenes/chk0.png' /><input type='checkbox' value='".$idRadicacion."' style='display:none;'></td>";
    	echo "</tr>";
    }

    echo "</table>";
    echo "</div>";

    echo "<br/>";
	
    if ($total_registros>1)
    {
    	echo "<div id='controls'>";
    		echo "<div id='perpage'>";
    			echo "<select id='cmbFilas' name='cmbFilas' onChange='cambiarCantRegistros(this.value)'>";
    				echo "<option value='5'>5</option>";
    				echo "<option value='10' selected='selected' >10</option>";
    				echo "<option value='20'>20</option>";
    				echo "<option value='50'>50</option>";
    				echo "<option value='100'>100</option>";
    			echo "</select>";
    			echo "<label>Registros Por P&aacute;gina</label>";
    		echo "</div>";    	
        	echo "<div id='navigation'>";
        		echo "<a id='paginar' data='".(1)."'><img src='../../imagenes/imagesSorter/first.gif' width='16' height='16' alt='Primera Pagina' /></a>";
        		echo "<a id='paginar' data='".($numero_pagina-1)."'><img src='../../imagenes/imagesSorter/previous.gif' width='16' height='16' alt='Pagina Anterior' /></a>";
        		echo "<a id='paginar' data='".($numero_pagina+1)."'><img src='../../imagenes/imagesSorter/next.gif' width='16' height='16' alt='Pagina Siguiente' /></a>";
        		echo "<a id='paginar' data='".($total_registros)."'><img src='../../imagenes/imagesSorter/last.gif' width='16' height='16' alt='Ultima Pagina' /></a>";       
		
        	echo "</div>";
        	echo "<div id='text'>";
        	echo "P&aacute;gina ".$numero_pagina." de ".$total_registros;
        	echo "</div>";
        echo "</div>";
        
    }
}

?>