<?php
/**
 * @author Oswaldo Gonz�lez
 * @date 01-jun-2012
 * @objetivo Verificar documentos radicados por empresas
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

?>
<HTML xmlns="http://www.w3.org/1999/xhtml"><HEAD>
<TITLE>::REVISION::</TITLE>
<META http-equiv=Content-Type content="text/html; charset=iso-8859-1">
<meta http-equiv="Content-Script-Type" content="text/javascript; charset=iso-8859-1">
<META content="MSHTML 6.00.2900.2180" name=GENERATOR>

<link type="text/css" href="../../css/css.1.8/jquery-ui-1.8.17.custom.css" rel="stylesheet">
<link type="text/css" href="../../css/marco.css" rel="stylesheet">
<link type="text/css" href="../../css/Estilos.css" rel="stylesheet">


<script type="text/javascript" src="../../js/1.6/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="../../js/1.6/jquery-ui-1.8.16.custom.min.js"></script>

<script type="text/javascript" src="../../js/comunes.js"></script>
<script type="text/javascript" src="js/revisionEmpresa.js"></script>
<script type="text/javascript">
	shortcut.add("Shift+F",function() {
		var URL=src();
		var url=URL+"aportes/trabajadores/consultaTrabajador.php";
	    window.open(url,"_blank");
	},{
		'propagate' : true,
		'target' : document 
	});
</script>
<style>
.pagina_actual{
	font-size: 12pt;
	font-weight: bold;
}
</style>
</head>
<body>
<center>
<br><br>
<table width="90%" border="0" cellspacing="0" cellpadding="0" class="ui-corner-all">
  <tr>
    <td width="13" height="29" background="../../imagenes/arriba_izq.gif">&nbsp;</td>
    <td background="../../imagenes/arriba_central2.gif"><span class="letrablanca">::&nbsp;Revisi&oacute;n de empresas ::</span></td>
    <td width="13" background="../../imagenes/arriba_der.gif" align="right">&nbsp;</td>
  </tr>
  <tr>
    <td background="../../imagenes/tabla/izquierda2.jpg">&nbsp;</td>
    <td background="../../imagenes/centro.gif">
	 <img src="../../imagenes/spacer.gif" width="1" height="1"/><img src="../../imagenes/spacer.gif" width="1" height="1"/>
<img src="../../imagenes/menu/grabar.png" width:16 height=16 style="cursor:pointer" title="Guardar" onClick="guardarRevision();" id="bGuardar"/> 
<img src="../../imagenes/spacer.gif" width="1" height="1"/><img src="../../imagenes/spacer.gif" width="1" height="1"/><img src="../../imagenes/spacer.gif" width="1" height="1"/>
<img src="../../imagenes/menu/informacion.png" width="16" height="16" style="border:none; cursor:pointer" title="Manual" onClick="mostrarAyuda();" />
  
<img src="../../imagenes/menu/notas.png" width="16" height="16" style="cursor: pointer" title="Colaboraci�nn en l�nea" onClick="notas();" />
	  </td>
    <td background="../../imagenes/tabla/derecha.gif">&nbsp;</td>
  </tr>
  <tr>
    <td background="../../imagenes/tabla/izquierda2.jpg">&nbsp;</td>
    <td align="center" background="../../imagenes/centro.gif"><div id="resultado" style="font-weight: bold;font-size: 14px;color:#FF0000"></div></td>
    <td background="../../imagenes/tabla/derecha.gif">&nbsp;</td>
  </tr>
  <tr>
    <td background="../../imagenes/tabla/izquierda2.jpg">&nbsp;</td>
    <td  background="../../imagenes/centro.gif" >
    <h5>EMPRESAS PENDIENTES POR AUDITAR</h5>
    <p>Tiene <span id="span_num_empresas" class="font-weigth:bold;"></span> empresas por auditar.</p>
    <br/>
    	<table width="90%" border="0" cellspacing="0" class="tablero">
          <tr>
             <td width="186">Buscar Por:</td>
             <td width="153"><select name="buscarPor" class="box1" id="buscarPor">
               <option selected="selected">Seleccione..</option>
               <option value="nit" selected="selected">NIT o CC</option>
               <option value="razonsocial">RAZON SOCIAL</option>
             </select></td>
             <td width="127"><input name="idEmpresa" type="text" class="box1" id="idEmpresa"  onblur="validarLongNumIdent(5,this)" /></td>
             <td width="641"><input name="buscarEmpresa" type="button" class="ui-state-default" id="buscarEmpresa" value="Buscar" />
              <span class="Rojo"></span>
             
             </td>
          </tr>
         
        </table>
	<p></p>
	<table width="100%" border="0" cellpadding="0" cellspacing="0" class="tablaR hover" id="tDatos"  >
		<thead>
			<tr>
		        <th width="5%" height="20" align="center" >ID</th>
		        <th width="10%" align="center">N&uacute;mero doc.</th>
		        <th width="26%" align="center">Raz&oacute;n social</th>
		        <th width="22%">Direcci&oacute;n</th>
		        <th width="9%">Agencia</th>
		        <th width="7%">Auditar</th>
	        </tr>
		</thead>
		<tbody>
		</tbody>
		<tfoot>
        </tfoot>
	</table>
     
    </td>
    <td background="../../imagenes/tabla/derecha.gif">&nbsp;</td>
  </tr>
  <tr>
    <td height="38" background="../../imagenes/abajo_izq2.gif">&nbsp;</td>
    <td background="../../imagenes/abajo_central.gif"></td>
    <td background="../../imagenes/abajo_der.gif">&nbsp;</td>
  </tr>
</table>

</center>
<input type="hidden" name="pageNum" value="0">

<input type="hidden" name="valorp" id="valorp" >
<input type="hidden" name="txtIdCliente" value="">
 
 <!-- VENTANA DIALOG VER MAS-->
<div id="verMas" title="INFORMACI�N ADICIONAL" style="display:none;">
  <h4>INFORMACION DE TRAYECTORIAS</h4>
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="box-table-a" style="margin:0" id="trayectoria">
  <tr>
    <th>Nit</th>
    <th>Empresa</th>
    <th>Fecha de Ingreso</th>
    <th>Fecha de retiro</th>
  </tr>
</table>

  <h4>INFORMACION PLANILLA &Uacute;NICA</h4>
    
    <table width="100%" border="0" cellspacing="0" cellpadding="0"  id="planilla" class="box-table-a" style="margin:0"  >
	  <tr>
	    <th>Nit</th>
	    <th>Periodo</th>
	    <th>Fecha de pago</th>
	    <th>Ingreso</th>
	    <th>Retiro</th>
	  </tr>
	</table>
    
  <h4>INFORMACION BENEFICIARIOS</h4>
  <table width="100%" border="0" cellspacing="0" cellpadding="0"  class="box-table-a" style="margin:0" id="beneficiarios">
  <tr>
    <th>Documento</th>
    <th>Nombres</th>
    <th>Parentesco</th>
    <th>Estado;</th>
    <th>Giro</th>
  </tr>
</table>

</div>

<!-- colaboracion en linea -->
<div id="div-observaciones-tab"></div>
<div id="dialog-form2" title="Colaboraci&oacute;n en l&iacute;nea">
<p>Por favor diligencie este formulario para enviar comentarios, errores o falencias encontradas en el proceso. M�ximo 250 caracteres </p>
<label>Tus comentarios:</label><br />
<textarea name="notas" id="notas" cols="60" rows="10"></textarea>
</div>

<!-- Manual Ayuda -->
<div id="ayuda" title="Manual Revisi�n"></div>

<!-- DIV EMPRESA -->
<div id="div-datosEmpresa" title="::DATOS DE EMPRESA::" style="display:none">	
	<table width="100%" class="tablero" >
		<tr>
			<td>Sector</td>
			<td id="sector" colspan="2"></td>
			<td>Clase de Sociedad</td>
			<td id="clasesoci"></td>
		</tr>
		<tr>
			<td width="15%"><label for="iden">NIT</label></td>
			<td width="15%" id="nit1"><label class="box1" ></label></td>
			<td width="14%" id="digito1">D&iacute;gito <label ></label></td>
			<td width="12%">Tipo Documento</td>
			<td width="30%" id="tipo_doc"><label></label></td>			
		</tr>
		<tr>
			<td>Raz&oacute;n Social</td>
			<td id="razonsocial" colspan="3"><label ></label></td>
			<td id="codigosucursal">C&oacute;digo Sucursal - <label ></label></td>
		</tr>
		<tr>
			<td>Nombre Comercial</td>
			<td id="sigla" colspan="4"><label ></label></td>
		</tr>
		<tr>
			<td>Direcci&oacute;n</td>
			<td id="direccion" colspan="4"><label ></label></td>
		</tr>
		<tr>
			<td>Departamento</td>
			<td id="departmento" colspan="2"></td>
			<td>Ciudad</td>
			<td id="municipio"><label ></label></td>
		</tr>
		<tr>
			<td>Tel&eacute;fono</td>
			<td id="telefono" colspan="2"><label ></label></td>
			<td>Fax</td>
			<td id="fax"><label ></label></td>
		</tr>
		<tr>
			<td>URL</td>
			<td id="url" colspan="2"><label ></label></td>
			<td>Email</td>
			<td id="email"><label ></label></td>
		</tr>
		<tr>
			<td> Representante Legal</td>
			<td id="repLeg" colspan="4"></td>
		</tr>
		<tr>
			<td>Contacto</td>
			<td id="contacto" colspan="4"></td>
		</tr>
		<tr>
  			<td>Contratista</td>
  			<td id="contratista" colspan="2"><label ></label></td>
  			<td>Actividad</td>
  			<td id="actividad"></td>
		</tr>
		<tr>
  			<td>Actividad Econ&oacute;mica DANE</td>
  			<td id="actDane" colspan="4"></td>
		</tr>
		<tr>
  			<td>&Iacute;ndice Aporte</td>
  			<td id="indicador" colspan="2"><label></label></td>
  			<td>Asesor</td>
  			<td id="idasesor"><label></label></td>
		</tr>
		<tr>
			<td>Seccional</td>
			<td id="seccional" colspan="2"><label ></label></td>
			<td>Estado</td>
			<td id="estado"><label ></label></td>
		</tr>
		<tr>
			<td>Clase Aportante</td>
			<td id="clase_apo" colspan="2"></td>
			<td>Tipo Aportante</td>
			<td id="tipo_apo"></td>
		</tr>
		<tr>
			<td>Fecha Afiliaci&oacute;n</td>
			<td id="fechaafiliacion" colspan="2"><label ></label></td>
			<td>Fecha Estado</td>
			<td id="fechaestado"><label ></label></td>
		</tr>
		<tr>
  			<td>Fecha Constituci&oacute;n</td>
  			<td id="fechamatricula" colspan="2"></td>
  			<td>Fecha Inicio Aportes</td>
  			<td id="fechaaportes"><label ></label></td>
		</tr>
		<tr>
			<td>Usuario</td>
			<td id="usuario" colspan="2"><label ></label></td>
			<td>Fecha Grabaci&oacute;n</td>
			<td id="fechasistema"></td>
		</tr>
		<tr>
			<td>Observaciones</td>
			<td id="observas" colspan="4"></td>
		</tr>
	</table>
</div>

</body>
</html>