<?php
/* autor:       orlando puentes
 * fecha:       29/09/2010
 * objetivo:    Procesar los movimientos de los Bonos almacenados en la base de datos. 
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

?>
<HTML xmlns="http://www.w3.org/1999/xhtml"><HEAD>
<TITLE>::REVISION::</TITLE>
<META http-equiv=Content-Type content="text/html; charset=iso-8859-1">
<meta http-equiv="Content-Script-Type" content="text/javascript; charset=iso-8859-1">
<META content="MSHTML 6.00.2900.2180" name=GENERATOR>
<link href="../../css/Estilos.css" rel="stylesheet" type="text/css">
<link type="text/css" href="../../css/Estilos.css" rel="stylesheet"/>
<link type="text/css" href="../../css/formularios/base/ui.all.css" rel="stylesheet" />
<!--<script type="text/javascript" src="../../js/jquery-1.4.2.js"></script> -->
<script type="text/javascript" src="../../js/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="../../js/formularios/ui/ui.core.js"></script>
<script type="text/javascript" src="../../js/formularios/ui/ui.tabs.js"></script>
<script type="text/javascript" src="../../js/formularios/ui/ui.draggable.js"></script>
<script type="text/javascript" src="../../js/formularios/ui/ui.resizable.js"></script>
<script type="text/javascript" src="../../js/formularios/ui/ui.dialog.js"></script>
<script type="text/javascript" src="../../js/formularios/ui/ui.datepicker.js"></script>
<script type="text/javascript" src="../../js/comunes.js"></script>
<script type="text/javascript" src="js/revision.js"></script>
<script type="text/javascript">
shortcut.add("Shift+F",function() {
		                                var URL=src();
		var url=URL+"aportes/trabajadores/consultaTrabajador.php";
    	window.open(url,"_blank");
    },{
	'propagate' : true,
	'target' : document 
        });        
</script>

<script type="text/javascript">
	$(document).ready(function() {    
    		$("#paginar").live("click", function(){
		$("#contenido").html("<br/><div align='center'><img src='../../imagenes/ajax-loader-fon.gif'/></div>");
			var pagina=$(this).attr("data");
			paginaG=pagina;

			listarAfiliaP(parametros, paginaG, filas);
    	});
	});  
</script>

<style type="text/css">
#empresas p{ cursor:pointer; color:#333; width:600px;}
#empresas p:hover{color:#000}
#accordion h3,div{ padding:6px;}
#accordion span.plus{ background:url(../../imagenes/plus.png) no-repeat right center; margin-left:95%;}
#accordion span.minus{background:url(../../imagenes/minus.png) no-repeat right center;  margin-left:95%}
div#wrapTable{padding:0px; margin-bottom:-2px}
div#icon{width:50px; margin:auto;margin-first:5px;padding:10px; text-align:center;display:none; cursor:pointer;}
div#icon span{background:url("../../imagenes/show.png") no-repeat; padding:12px;}
div#icon span.toggleIcon{background:url("../../imagenes/hide.png") no-repeat; padding:12px;}

#controls { margin:0 auto; height:20px;}
#perpage {float:left; width:200px; margin-left:40px;}
#perpage select {float:left; font-size:11px}
#perpage label {float:left; margin:2px 0 0 5px; font-size:10px}
#navigation {float:left;/* width:600px;*/ text-align:center}
#navigation img {cursor:pointer}
#text {float:left; width:200px; text-align:left; margin-first:2px; font-size:10px}

#periodo p {
	text-align: center;
	font-style: italic;
	color: #CCC;
}


</style>

</head>
<body>
<center>
<br><br>
<table width="90%" border="0" cellspacing="0" cellpadding="0" class="ui-corner-all">
  <tr>
    <td width="13" height="29" background="../../imagenes/arriba_izq.gif">&nbsp;</td>
    <td background="../../imagenes/arriba_central2.gif"><span class="letrablanca">::&nbsp;Revisi&oacute;n de trabajadores&nbsp;::</span></td>
    <td width="13" background="../../imagenes/arriba_der.gif" align="right">&nbsp;</td>
  </tr>
  <tr>
    <td background="../../imagenes/tabla/izquierda2.jpg">&nbsp;</td>
    <td background="../../imagenes/centro.gif">
	 <img src="../../imagenes/spacer.gif" width="1" height="1"/>
	<img src="../../imagenes/menu/nuevo.png" title="Nuevo" width="16" height="16" style="cursor:pointer" onClick="nuevoR();">
	<img src="../../imagenes/spacer.gif" width="1" height="1">
<img src="../../imagenes/menu/grabar.png" width:16 height=16 style="cursor:pointer" title="Guardar" onClick="guardarRevision();" id="bGuardar"/> 
<img src="../../imagenes/spacer.gif" width="1" height="1"/><img src="../../imagenes/spacer.gif" width="1" height="1"/><img src="../../imagenes/spacer.gif" width="1" height="1"/>
<img src="../../imagenes/menu/informacion.png" width="16" height="16" style="border:none; cursor:pointer" title="Manual" onClick="mostrarAyuda();" />
  
<img src="../../imagenes/menu/notas.png" width="16" height="16" style="cursor: pointer" title="Colaboraci�nn en l�nea" onClick="notas();" />
	  </td>
    <td background="../../imagenes/tabla/derecha.gif">&nbsp;</td>
  </tr>
  <tr>
    <td background="../../imagenes/tabla/izquierda2.jpg">&nbsp;</td>
    <td align="center" background="../../imagenes/centro.gif"><div id="resultado" style="font-weight: bold;font-size: 14px;color:#FF0000"></div></td>
    <td background="../../imagenes/tabla/derecha.gif">&nbsp;</td>
  </tr>
  <tr>
    <td background="../../imagenes/tabla/izquierda2.jpg">&nbsp;</td>
    <td  background="../../imagenes/centro.gif" >
    <h5>TRABAJADOR A AUDITAR</h5>
    <table width="100%" border="0" cellspacing="0" class="tablero">
		<tr>
			<td width="40">Buscar Por:</td>
			<td width="50">
				<select name="buscarPor" class="box1" id="buscarPor">
					<option value="i" selected="selected">IDENTIFICACION</option>
					<option value="n">NOMBRE COMPLETO</option>
					<option value="a">AGENCIA</option>
				</select>
			</td>
			<td width="169">
				<select id="tipoDocumento" name="tipoDocumento" class="box1">
				<option value="1" selected="selected">C&eacute;dula ciudadan�a</option>
				<option value="3">Pasaporte</option>
				<option value="4">C&eacute;dula extranjer&iacute;a</option>
				</select>
				
				<input name="identificacion" type="text" class="box" id="identificacion" onBlur="validarLongNumIdent(document.getElementById('tipoDocumento').value,this)" onKeyUp="validarCaracteresPermitidos(document.getElementById('tipoDocumento').value,this);"/> 
				<input type="text" class="box" id="pn" style="display:none" /> -
				<input type="text" class="box" id="pa" style="display:none" />
				
				<select id="agencia1" name="agencia1" class="box1" hidden="">
				<option value="s" selected="selected">Seleccione Agencia</option>
				<option value="01">Neiva</option>
				<option value="02">Garz&oacute;n</option>
				<option value="03">Pitalito</option>
				<option value="04">La Plata</option>
				</select>
			</td>
			<td width="116">
				<input name="buscarT" type="button" class="ui-state-default" id="buscarT" value="Buscar" />
				<span class="Rojo"></span>
			</td>
		</tr>
	</table>
	
	<div id="contenido"></div>
     
    </td>
    <td background="../../imagenes/tabla/derecha.gif">&nbsp;</td>
  </tr>
  <tr>
    <td height="38" background="../../imagenes/abajo_izq2.gif">&nbsp;</td>
    <td background="../../imagenes/abajo_central.gif"></td>
    <td background="../../imagenes/abajo_der.gif">&nbsp;</td>
  </tr>
</table>

</center>
<input type="hidden" name="pageNum" value="0">

<input type="hidden" name="valorp" id="valorp" >
<input type="hidden" name="txtIdCliente" value="">
 
 <!-- VENTANA DIALOGVER MAS-->
<div id="verMas" title="INFORMACI�N ADICIONAL" style="display:none;">
  <h4>INFORMACION DE TRAYECTORIAS</h4>
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="box-table-a" style="margin:0" id="trayectoria">
  <tr>
    <th>Nit</th>
    <th>Empresa</th>
    <th>Fecha de Ingreso</th>
    <th>Fecha de retiro</th>
  </tr>
</table>

  <h4>INFORMACION PLANILLA &Uacute;NICA</h4>
    
    <table width="100%" border="0" cellspacing="0" cellpadding="0"  id="planilla" class="box-table-a" style="margin:0"  >
  <tr>
    <th>Nit</th>
    <th>Periodo</th>
    <th>Fecha de pago</th>
    <th>Ingreso</th>
    <th>Retiro</th>
  </tr>
</table>
    
  <h4>INFORMACION BENEFICIARIOS</h4>
  <table width="100%" border="0" cellspacing="0" cellpadding="0"  class="box-table-a" style="margin:0" id="beneficiarios">
  <tr>
    <th>Documento</th>
    <th>Nombres</th>
    <th>Parentesco</th>
    <th>Estado;</th>
    <th>Giro</th>
  </tr>
</table>

</div>

<!-- colaboracion en linea -->
<div id="div-observaciones-tab"></div>
<div id="dialog-form2" title="Colaboraci&oacute;n en l&iacute;nea">
<p>Por favor diligencie este formulario para enviar comentarios, errores o falencias encontradas en el proceso. M�ximo 250 caracteres </p>
<label>Tus comentarios:</label><br />
<textarea name="notas" id="notas" cols="60", rows="10"></textarea>
</div>

<!-- Manual Ayuda -->
<div id="ayuda" title="Manual Revisi�n">
</div>

<div title="::Detalles Afiliado::" id="detalles">
<p align="left" id="info"></p>
  <h5>Datos Personales</h5>
  <p id="p_nombre_afiliado"></p>
  <table width="100%" border="0" cellspacing="0" class="tablero" id="tDatos2">
    <tr>
      <th width="10%" height="19">Fecha Nace</th>
      <th width="16%" bgcolor="#ECFFB0">Ciudad Nace</th>
      <th width="19%" bgcolor="#ECFFB0"><strong>Sexo</strong></th>
      <th width="17%" bgcolor="#ECFFB0"><strong>Ciudad Rescidencia</strong></th>
      <th width="25%" bgcolor="#ECFFB0"><strong>Zona</strong></th>
      <th width="13%" bgcolor="#ECFFB0"><strong>Tel&eacute;fono</strong></th>
    </tr>
  </table>
  <h5>DATOS AFILIACI&Oacute;N</h5>
  <table width="100%" border="0" cellspacing="0" class="tablero" id="tAfiliacion">
    <tr>
      <th width="10%" height="19"><strong>Empleador</strong></th>
      <th width="16%" bgcolor="#ECFFB0"><strong>Fecha Ingreso</strong></th>
      <th width="19%" bgcolor="#ECFFB0"><strong>Salario</strong></th>
      <th width="17%" bgcolor="#ECFFB0"><strong>T. Formulario</strong></th>
      <th width="25%" bgcolor="#ECFFB0"><strong>T. Afiliaci&oacute;n</strong></th>
      <th width="13%" bgcolor="#ECFFB0"><strong>Agencia</strong></th>
    </tr>
  </table>
  
   <h5>GRUPO FAMILIAR</h5>
<table width="100%" border="0" cellspacing="0" class="tablero" id="tGrupo">
	<thead>
		<tr>
		    <th bgcolor="#ECFFB0"><strong>id</strong></th>
		    <th bgcolor="#ECFFB0"><strong>Identif.</strong></th>
		    <th bgcolor="#ECFFB0"><strong>Nombres</strong></th>
		    <th bgcolor="#ECFFB0"><strong>Parentesco</strong></th>
		    <th bgcolor="#ECFFB0"><strong>Fec. Nac.</strong></th>
		    <th bgcolor="#ECFFB0"><strong>Conviven</strong></th>
		    <th bgcolor="#ECFFB0"><strong>Relaci&oacute;n</strong></th>
			<th bgcolor="#ECFFB0"><strong>Estado</strong></th>
		</tr>
	</thead>
	<tbody>
	</tbody>
</table>
 
</div>

</body>

</html>
