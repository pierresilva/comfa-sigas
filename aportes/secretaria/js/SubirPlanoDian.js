function procesar(archivo,tipoarchivo){
	$("#ajax").html('<img src=../../imagenes/ajax-loader.gif />');
	$("#boton").html(' ');
	$.ajax({
		url: 'procesarPlanoDian.php',
		data: {v0:archivo,v1:tipoarchivo},
		type: "POST",
		async:false,
		success: function(datos){
			if(isNaN(datos)){
				$("#ajax").html("Ocurrio un error con el procesamiento de su solicitud.");
			} else if(parseInt(datos)==-2){
				$("#ajax").html("Ocurrio un error con la conexion a la base de datos.");
			} else if(parseInt(datos)==-1){
				$("#ajax").html("No se encontraron cedulas que procesar.");
			} else if(parseInt(datos)==0) {
				$("#ajax").html("Las cedulas procesadas no arrojaron ningun resultado que mostrar.");
			}else {
				$("#ajax").html("<small>Informacion procesada satisfactoriamente.</b></small><br><br><label style='cursor:pointer' onClick='descargar();'><img src=../../imagenes/descargarArchivo.png width=143 height=30 /></label>");
			} 
		}
	});	
}

function validarTipoArchivo(){
	if(!$("#tipoarchivo").val()){
		alert("Seleccione el Tipo de Archivo.");
		return false;
	}
}
	
function descargar(){
	var url='../../phpComunes/descargaPlanos.php';
	window.open(url);
}