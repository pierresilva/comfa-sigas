<?php
set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';

include_once $_SESSION['RAIZ'] . DIRECTORY_SEPARATOR . 'rsc/pdo/IFXDbManejador.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

$FrmActual=basename($_SERVER['PHP_SELF']);
$fecver = date('Ymd h:i:s A',filectime($FrmActual));
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="es"><head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>:: Certificados ::</title>
<link type="text/css" href="<?php echo URL_PORTAL; ?>newcss/Estilos.css" rel="stylesheet">
<link type="text/css" href="<?php echo URL_PORTAL; ?>newcss/css.1.8/jquery-ui-1.8.17.custom.css" rel="stylesheet">
<link type="text/css" href="<?php echo URL_PORTAL; ?>newcss/marco.css" rel="stylesheet">

<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/1.6/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/1.6/jquery-ui-1.8.16.custom.min.js"></script>

<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/comunes.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/jquery.utilitarios.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/clases/certificado.class.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>aportes/radicacionnew/js/utilitarios.js"></script>
<script type="text/javascript" src="js/certificados.js"></script>
</head>

<body>
	<table width="90%" border="0" cellspacing="0" cellpadding="0"
		align="center">
		<tr>
			<td width="13" height="29" class="arriba_iz">&nbsp;</td>
			<td class="arriba_ce">
				<span class="letrablanca">:: Informaci&oacute;n Certificado Beneficiario ::</span>
				<div style="text-align:right; height:20px; top:8px; width:221px; position:absolute; right: 100px;"><?php echo 'Versi&oacute;n: ' . $fecver; ?></div>
			</td>
			<td width="13" class="arriba_de" align="right">&nbsp;</td>
		</tr>
		<tr>
			<td class="cuerpo_iz">&nbsp;</td>
			<td class="cuerpo_ce"><br />
				<table width="90%" cellspacing="0" cellpadding="0" align="center">
					<tr>
						<td>
							<img src="<?php echo URL_PORTAL; ?>imagenes/menu/nuevo.png" alt="Nuevo" width="16" height="16" style="cursor:pointer" title="Nuevo" onclick="newCertificado()" />
							<img src="<?php echo URL_PORTAL; ?>imagenes/spacer.gif" alt="" width="1" height="1" />
							<img src="<?php echo URL_PORTAL; ?>imagenes/menu/grabar.png" alt="Guardar" width="16" height="16" style="cursor:pointer" title="Guardar" onclick="saveCertificado();" id="bGuardar"/>
							<img src="<?php echo URL_PORTAL; ?>imagenes/spacer.gif" alt="" width="1" height="1" /> 
						</td>
					</tr>
				</table>
			</td>
			<td class="cuerpo_de">&nbsp;</td>
		</tr>
		<tr>
			<td class="cuerpo_iz">&nbsp;</td>
			<td class="cuerpo_ce">
				<table width="90%" border="0" cellspacing="0" class="tablero" align="center">
					<tr>
						<td align="left" width="17%">Tipo Documento Afiliado</td>
						<td width="38%">
							<select name="cmbIdTipoDocumentoAfiliado" id="cmbIdTipoDocumentoAfiliado" class="box1" style="width: 250px" onchange="$('#txtNumero').val('').trigger('blur');" >
								<option value="0" selected="selected">Seleccione...</option>
		            			<?php
		            				$rs = $db->Definiciones ( 1, 1 );															
									while ( $row = $rs->fetch() ) {
										echo "<option value=" . $row ['iddetalledef'] . ">" . $row ['detalledefinicion'] . "</option>";
									}
								?>
							</select> <img src="../../imagenes/menu/obligado.png" width="12" height="12">
						</td>
						<td width="22.5%">N&uacute;mero Documento Afiliado</td>
						<td width="22.5%">
							<input type="text" class="box1" name="txtNumero" maxlength="12" id="txtNumero" onkeyup="validarIdentificacion($('#cmbIdTipoDocumentoAfiliado'),this);"
								onkeydown="validarIdentificacion($('#cmbIdTipoDocumentoAfiliado'),this);" onkeypress="validarIdentificacion($('#cmbIdTipoDocumentoAfiliado'),this);"
								onblur="busquedaPersona($('#cmbIdTipoDocumentoAfiliado'), $('#txtNumero'), $('#tdNombreCompletoAfiliado'));">
							<img src="../../imagenes/menu/obligado.png" width="12" height="12">
						</td>
					</tr>
					<tr>
						<td>Nombre Completo</td>
						<td id="tdNombreCompletoAfiliado">&nbsp;</td>
						<td>Fecha Nacimiento Afiliado</td>
						<td><input type="text" class="box1" name="txtFechaNacimientoAfiliado" id="txtFechaNacimientoAfiliado" disabled /></td>
					</tr>
					<tr>
						<td>Beneficiario</td>
						<td>
							<select name="cmbIdBeneficiario" id="cmbIdBeneficiario" class="box1" style="width: 300px" onchange="onChangeBeneficiario();" >
								<option value="0" selected="selected">Seleccione...</option>
							</select> <img src="../../imagenes/menu/obligado.png" width="12" height="12">&nbsp;
							<img src="../../imagenes/menu/reporte.png" width="14" height="12" style="cursor:pointer" onclick="mostrarDatosCertificados();" id="imgCertificado" >
						</td>
						<td>Discapacidad</td>
						<td><select name="cmbDiscapacidad" id="cmbDiscapacidad" class="box1" onchange="onChangeDiscapacidad(this);" disabled="disabled" >								
								<option value="N" selected="selected">NO</option>
								<option value="I" selected="selected">SI</option>
							</select> <img src="../../imagenes/menu/obligado.png" width="12" height="12">
						</td>
					</tr>					
					<tr id="filaFechas" >
						<td>Fecha Nacimiento</td>
						<td><input type="text" class="box1" name="txtFechaNacimiento" id="txtFechaNacimiento" disabled="disabled" /></td>
						<td>Fecha Asignaci&oacute;n</td>
						<td><input type="text" class="box1" name="txtFechaAsignacion" id="txtFechaAsignacion" disabled="disabled" /></td>
						<input id="txtIdParentesco" type="hidden" value="" />
					</tr>
					<tr id="filaCertificados" >
				  		<td colspan="4">
					  		<table width="100%" border="0" >
					  			<tr id="filaEscolaridad" style="display:none">
									<td width="25%" >Certificado Escolaridad</td>
							  		<td width="25%" ><input type="radio" name="certificado" id="cEscolaridad" onchange="setFechaAsignaCert($('#cEscolaridad'),$('#fEscolaridad'))" /></td>
							  		<td width="25%" >Fecha aplica</td>
							  		<td width="25%" ><input type="text" class="box1" id="fEscolaridad" name="fCert" disabled="disabled" /></td>
								</tr>
								<tr id="filaUniversidad" style="display:none">
							  		<td>Certificado Universidad</td>
							  		<td><input type="radio" name="certificado" id="cUniversidad" onchange="setFechaAsignaCert($('#cUniversidad'),$('#fUniversidad'))" /></td>
							  		<td>fecha aplica</td>
							  		<td><input type="text" class="box1" id="fUniversidad" name="fCert" disabled="disabled"/></td>
								</tr>				
								<tr id="filaDiscapacidad" style="display:none">
							  		<td>Certficado Discapacidad</td>
							  		<td><input type="radio" name="certificado" id="cDiscapacidad" onchange="setFechaAsignaCert($('#cDiscapacidad'),$('#fDiscapacidad'))" /></td>
							  		<td>Fecha aplica</td>
							  		<td><input name="fCert" type="text" class="box1" id="fDiscapacidad" disabled="disabled" /></td>
								</tr>
								<tr id="filaSupervivencia" style="display:none">
							  		<td>Certificado Supervivencia</td>
							  		<td><input type="radio" name="certificado" id="cSupervivencia" onchange="setFechaAsignaCert($('#cSupervivencia'),$('#fSupervivencia'))" /></td>
							  		<td>Fecha aplica</td>
							  		<td><input name="fCert" type="text" class="box1" id="fSupervivencia" disabled="disabled" /></td>
								</tr>	
					  		</table>
					  	</td>
				  	</tr>
					<tr id="filaVigencias" >
						<td colspan="4">
				        	<div id="cerVigencia" style="display:none">
				        		<table width="100%" border="0">
				        			<tr>    
				      					<td><center>A&ntilde;o Vigencia</center></td>
				      					<td>
				       						<input type="radio" name="vigencia" id="vigencia1" value="0" onClick="vVigencia(0)">
				       						<label id="l1" for="vigencia1" >Periodo Actual</label><br/>
				      						<input type="radio" name="vigencia" id="vigencia2" value="1" onClick="vVigencia(1)"> 
				      						<label id="l2" for="vigencia2" >Periodo Anterior</label><br/>
				      					</td>
				      					<td colspan="2" >
				      						<center>
					      						Periodo Inicial
					        					<input name="pInicialV3" type="text" class="box1 monthPicker" id="pInicialV3" value="" onBlur="validarVigencia(this.value,1); validarPeriodosDatePicker(this.id,'pFinalV3','errorVig'); " disabled="disabled" maxlength="6">
					        					Periodo Final
					        					<input name="pFinalV3" type="text" class="box1 monthPickerF" id="pFinalV3" value="" onBlur="validarVigencia(this.value,2); validarPeriodosDatePicker('pInicialV3',this.id,'errorVig');" disabled="disabled" maxlength="6">	        					
					        					<span class="Rojo" id="errorVig"></span>
				        					</center>
				        				</td>        
				    				</tr>
				        		</table>
				       		</div>
				       </td>	
					</tr>
				</table>
			</td>
			<td class="cuerpo_de">&nbsp;</td>
		</tr>
		<tr>
			<td class="abajo_iz" >&nbsp;</td>
    		<td class="abajo_ce" >&nbsp;</td>
    		<td class="abajo_de" >&nbsp;</td>
		</tr>		
	</table>
	
	<div id="div-datosCertificados" title="::HISTORIAL DE CERTIFICADOS::" style="display:none">
	<table width="100%" class="tablero" >
		<thead>
			<tr>
				<th>Id Cert.</th>
				<th>Tipo</th>
				<th>Peri&oacute;do Inicial</th>
				<th>Peri&oacute;do Final</th>
				<th>Fecha Presentaci&oacute;n</th>
			</tr>
		</thead>
		<tbody></tbody>
	</table>
	</div>
</body>
</html>