<?php
/* autor:       orlando puentes
 * fecha:       octubre 14 de 2010
 * objetivo:    Almacenar en la base de datos todas las definiciones que se trabajan en Aportes y Subsidio 
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'rsc' . DIRECTORY_SEPARATOR .'conexion.class.php';

class Definiciones{
	//constructor	
	var $con;
	var $fechaSistema;
	function Definiciones(){
		$this->con = new DBManager();
	}
	
	function mostrar_registro($id){
		if($this->con->conectar() == true){
			$sql = "SELECT * FROM aportes091 WHERE iddetalledef='$id'";
			return mssql_query($sql, $this->con->conect);
		}
	}
	
	/**
	 * Lista por codigo y id de definicion el detalle solicitado
	 * 
	 * @param string $cod
	 * @param integer $id
	 * 
	 * @return resource Recurso de resultados de la consulta ifx
	 */
	function mostrar_registroXcodigo($cod, $id){
		if($this->con->conectar() == true){
			$sql = "SELECT * FROM aportes091 WHERE codigo='$cod' AND iddefinicion=$id";
			return mssql_query($sql, $this->con->conect);
		}
	}
	
	/**
	 * Obtiene una lista de definiciones seg�n el id del tipo recibido  
	 * @param int $id tipo de definici�n
	 * @param string $col nombre de columna para ordenar
	 */
	function mostrar_datos($id, $col = 4){
		if($this->con->conectar() == true){
			return mssql_query("SELECT * FROM aportes091 where iddefinicion=$id order by $col", $this->con->conect);
		}
	}
	
	/**
	 * Obtiene los datos del valor del salario m�nimo del a�o actual
	 * @return int de la consulta de informix
	 */
	function obtener_salariomin_actual(){
		if($this->con->conectar()){
			$sql = "SELECT top 1 smlv FROM aportes012 WHERE year(fechainicio) = ". date("Y") ." AND year(fechafin) = ". date("Y");
			$result = mssql_query($sql,$this->con->conect);
			$arrResult = mssql_fetch_row($result);
			return $arrResult["smlv"];
		}
	}
	
	/**
	 * 
	 * Obtiene el valor del salario minimo seg�n el a�o recibido
	 * @param int Representa al valor del smmlv
	 */
	function obtener_salariomin_por_agno($agno){
		if($this->con->conectar()){
			$sql = "SELECT top 1 smlv FROM aportes012 WHERE year(fechainicio) = ". $agno ." AND year(fechafin) = ". $agno;
			$result = mssql_query($sql,$this->con->conect);
			$arrResult = mssql_fetch_row($result);
			return $arrResult["smlv"];
		}
	}
}
?>