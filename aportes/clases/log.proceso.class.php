<?php

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';

include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'utilitario.class.php';

class LogProceso{
	
	private $usuario;
	
	private static $con = null;
	
	function __construct(){
		//Conexion
		try{
			self::$con = IFXDbManejador::conectarDB();
			if( self::$con->conexionID==null ){
				throw new Exception(self::$con->error);
			}
		}catch(Exception $e){
			echo $e->getMessage();
			exit();
		}	
	}
	
	
	public function setUsuario($usuario){$this->usuario = $usuario;}
	public function getUsuario(){return $this->usuario;}
	
	/**
	 * Metodo encargado de guardar el log en la base de datos
	 * @param unknown_type $idTrazaProceso
	 * @param unknown_type $codigo [EMPRESA, AFILIADO]
	 * @param unknown_type $descripcion
	 * @return number 0: error, 1: exitoso
	 */
	public function guardar_log_db($idTrazaProceso,$codigo,$tipoLog,$descripcion,$data="", $estado, $mensaErrorSiste = ""){
		$query = "INSERT INTO aportes423
					(id_traza_proceso, codigo, tipo_log, descripcion, data, estado, mensa_error_siste ,usuario)
					VALUES($idTrazaProceso,'$codigo','$tipoLog',:descripcion,:data,'$estado', :mensa_error_siste , '$this->usuario')";
				
		$statement = self::$con->conexionID->prepare($query);
		$statement->bindValue(":descripcion", $descripcion,  PDO::PARAM_STR);
		$statement->bindValue(":data", $data,  PDO::PARAM_STR);
		$statement->bindValue(":mensa_error_siste", $mensaErrorSiste,  PDO::PARAM_STR);
		$guardada = $statement->execute();
		return $guardada == false ? 0 : 1;
	}
	
	/**
	 * 
	 * @param unknown_type $arrFiltro
	 */
	public function fetch_log($arrAtributoValor){
		$attrEntidad = array(
				array("nombre"=>"id_traza_proceso","tipo"=>"NUMBER","entidad"=>"a423")
				,array("nombre"=>"id_proceso","tipo"=>"NUMBER","entidad"=>"a420")
				,array("nombre"=>"id_etapa_proceso","NUMBER"=>"TEXT","entidad"=>"a421"));
		
		$filtroSql = Utilitario::obtenerFiltro($attrEntidad,$arrAtributoValor);
		
		$query = "SELECT
					a423.id_log, a423.id_traza_proceso, a423.codigo, a423.tipo_log, a423.descripcion, a423.data,a423.estado, a423.usuario, a423.fecha_creacion 
					, a422.procesado
					, a420.id_proceso, a420.tipo AS tipo_proceso, a420.fecha_inicio AS fecha_inici_proce, a420.fecha_fin AS fecha_fin_proce, a420.estado AS estado_proceso
					, a421.id_etapa_proceso, a421.etapa_proceso, a421.descripcion AS descr_etapa_proce
				FROM aportes423 a423
					INNER JOIN aportes422 a422 ON a422.id_traza_proceso=a423.id_traza_proceso
					INNER JOIN aportes420 a420 ON a420.id_proceso=a422.id_proceso
					INNER JOIN aportes421 a421 ON a421.id_etapa_proceso=a422.id_etapa_proceso
				$filtroSql
				ORDER BY a423.tipo_log, a423.fecha_creacion DESC, a421.orden";
		
		$arrResultado = $this->fetchConsulta($query);
		
		//Convertir a objeto la descripcion
		foreach($arrResultado as &$row){
			$row["data"] = json_decode($row["data"]);
		}
		unset($row);
		
		return $arrResultado;
	}
	
	/**
	 * Retorna los valores obtenidos en la consulta
	 * @return multitype:array
	 */
	private function fetchConsulta($querySql){
		$resultado = array();
		$rs = self::$con->querySimple($querySql);
		while($row = $rs->fetch())
			$resultado[] = array_map("trim",$row);
		//$resultado[] = array_map("utf8_encode",$row);
		return $resultado;
	}
}

?>