<?php
/* autor:       Orlando Puentes
 * fecha:       Julio 26 de 2010
 * objetivo:    Almacenar en la base de datos todas las profesiones. 
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'rsc' . DIRECTORY_SEPARATOR .'conexion.class.php';

class Profesiones {
	
	var $con;
	
	function __construct() {
		$this->con = new DBManager ();
	}
	
	function listar_todas() {
		if ($this->con->conectar () == true) {
			$sql = "SELECT iddetalledef, detalledefinicion FROM aportes091 WHERE iddefinicion=20";
			return mssql_query ( $sql, $this->con->conect );
		}
	}
	
	function buscar_por($criterio) {
		$criterio = strtoupper(trim($criterio));
		if ($this->con->conectar () == true) {
			$sql = "SELECT iddetalledef, detalledefinicion FROM aportes091 WHERE iddefinicion=20 AND detalledefinicion LIKE '%$criterio%'";
			return mssql_query ( $sql, $this->con->conect );
		}
	}
}

?>