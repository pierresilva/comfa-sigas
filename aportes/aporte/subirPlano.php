<?php
// @autor:		Orlando Puentes A
// @fecha:		mayo 31/2010
// @formulario:	guardar datos persona - modulo empresa
set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz . DIRECTORY_SEPARATOR .'config.php'; 
include_once $raiz . DIRECTORY_SEPARATOR .'funcionesComunes'.DIRECTORY_SEPARATOR.'funcionesComunes.php';
$anno=date('Y');
$mes=date('m');
$dia=date('d');
?>
<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es">
<head> 
<title>Validar Plano Banco</title>
<link href="../../css/Estilos.css" rel="stylesheet" type="text/css" />
<link type="text/css" href="../../css/marco.css" rel="stylesheet" />
<script language="javascript" src="../../js/jquery-1.4.2.js"></script>
<script language="javascript" src="js/subirPlano.js"></script>
</head>
<body>

 	<center>
		<table width="80%" height="200" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td class="arriba_iz" >&nbsp;</td>
				<td class="arriba_ce" ><span class="letrablanca" id="encabezado">::Validar Plano Banco::</span></td>
				<td class="arriba_de" >&nbsp;</td>
			</tr>
			<tr>
				<td class="cuerpo_iz">&nbsp;</td>
				<td class="cuerpo_ce" >
					<label id="lblBoton">
					<img src="../../imagenes/spacer.gif" alt="" width="2" height="1" />
					<a href="validarPlanoBanco.html"><img src="../../imagenes/menu/nuevo.png" alt="" width="20" height="20" style="cursor:pointer" title="Nuevo"  /> </a>
					</label>
				</td>
				<td class="cuerpo_de">&nbsp;</td>
			</tr>
			<tr>
			<td class="cuerpo_iz">&nbsp;</td>
			<td class="cuerpo_ce">
				<div id="ajax" align="center">
				<?php 
				$dir1 = $ruta_cargados.'tesoreria'. DIRECTORY_SEPARATOR .'auditoria'. DIRECTORY_SEPARATOR;
				$ok=verificaDirectorio($dir1);
				if($ok==0){
					echo 2;		//no se pudo crear el directorio destino
					exit();
				} 
				
                                $dir = opendir($dir1);
                                while ($elemento = readdir($dir)){      
                                    if(strlen($elemento)>2){
                                        unlink($dir1.$elemento);
                                    }
                                }
                                closedir($dir);
                                
				$directorio=$dir1;
				echo "<br><br><br><br>";
				if(isset($_FILES['archivo'])){
					foreach ($_FILES['archivo']['error'] as $key => $error) {
					   if ($error == UPLOAD_ERR_OK) {
						   $archivos[]= $_FILES["archivo"]["name"][$key];
						   move_uploaded_file($_FILES["archivo"]["tmp_name"][$key],$directorio.$_FILES["archivo"]["name"][$key]) 
						   or die("Ocurrio un problema al intentar validar el archivo.");
					   }
					   else{
					   		echo "<center><p class=Rojo>Error: $error</p></center>";
					   		exit();
					   }
					}
				 }
				 else{
				 	echo "<center><p class=Rojo>No se pudo validar el archivo!</p></center>";
				 	exit();
				 }
				 
				for($i=0; $i<count($archivos); $i++){
					echo "<p align=center>Archivo listo para validar: <strong>".$archivos[$i]."</strong></p>"; 
				}
				if($i>0){
				?>
				<center>
				    <div id="boton">    
				    <p style="text-decoration:none; font-size:12px; color:#F00; font-weight:bold; cursor:pointer" onclick="procesar();">Validar plano Banco</p>
				</div>
				</center>
				<?php
					}
				 ?> 
			 	</div>
			 
				<div id="mensaje" align="center">..:..</div>
		 	</td>
			<td class="cuerpo_de">&nbsp;</td>
			</tr>
			<tr>
			<td class="abajo_iz" >&nbsp;</td>
			<td class="abajo_ce" ></td>
			<td class="abajo_de" >&nbsp;</td>
			</tr>
		</table>
	</center>

 </body>
 </html>
 
