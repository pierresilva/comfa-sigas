
// funcion para obtener cookie en javascript
	function getCookie(c_name)
	{
		var i,x,y,ARRcookies=document.cookie.split(";");
		for (i=0;i<ARRcookies.length;i++)
		{
  			x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
  			y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
  			x=x.replace(/^\s+|\s+$/g,"");
  			if (x==c_name)
    			{
    				return unescape(y);
    			}// fin if
		}// fin for
  	}// fin getCookie
//funcion para obtener cookie en javascript
	
$(function(){
	//crear el datepicker pera el primer periodo
	datepickerC("PERIODO","#txtPeriodo");
	
	//direccion raiz del proyecto
	var URL=src();
	
	$("#txtNit").blur(function(){
		$("#tdMensajeError,#tBody,#txtPeriodo").html('');
		$("#tdPeriodo,#tabAportes").css('display','none');
		var nit = $("#txtNit").val().trim();
		if(nit=="")
			return false;
			
		$.ajax({
			url:"procesoModificarIndiceAportes.php",	
			type:"POST",
			data:{v0:"CONSULTAR_EMPRESA",v1:nit},
			dataType:"json",
			async:false,
			success:function(datos){
				if(datos['error']==1){
					$("#lblRazonSocial").html(datos['descripcion']);
					return false;
				}
				$("#lblRazonSocial").html(datos['razonSocial']);
				$("#hidIdEmpresa").val(datos['idEmpresa']);
				$("#tdPeriodo").css('display','block');
			}
		});
	});	
	$("#btnBuscarAportes").click(function(){
		$("#tdMensajeError").html('');
		var periodo   = $("#txtPeriodo").val();
		var idEmpresa = $("#hidIdEmpresa").val(); 
		$("#tabAportes").css('display','none');
		if(periodo=="" || idEmpresa=="")
			return  false;
		
		$.ajax({
			url:"procesoModificarIndiceAportes.php",
			type:"POST",
			data:{v0:"BUSCAR_APOSTES",v1:idEmpresa,v2:periodo},
			dataType:"json",
			async:false,
			success:function(datos){
				if(datos['error']==1){
					$("#tdMensajeError").html(datos['descripcion']);
					$("#tBody").html('');
					return false;
				}
				var tr = "";
				$.each(datos['aportes'],function(i,row){
					indice = row['indicador'];
					//alert(" este es el indice  =" + indice );
					if(indice > 1){
						procesar01();					
					}else if (indice = 1) {
						$.ajax({
							url:"procesoModificarIndiceAportes.php",
							type:"POST",
							data:{v0:"BUSCAR_INDICE",v1:idEmpresa,v2:periodo},
							dataType:"json",
							async:false,
							success:function(datos){
								if(datos['error']==1){
									$("#tdMensajeError").html(datos['descripcion']);
									$("#tBody").html('');
									return false;
								}
								var tr = "";
								$.each(datos['aportes'],function(i,row){
									IdEmpresa = row['idempresa'];
									Periodo = row['periodo'];
									Indicador   = parseFloat( row['tarifaaporte'] );
									ValorAporte = row['valoraporte'];
									
									nomina = Math.round((ValorAporte * 100) / Indicador);
									//Manda a Modificar el indice a aportes011 y aportes048
									$.ajax({
										url:'procesoModificarIndiceAportes.php',
										type:'POST',
										data:{v0:'MODIFICAR',v1:IdEmpresa,v2:Periodo,v3:Indicador,v4:nomina},
										dataType:'json',
										async:true,
										success:function(datos){
											if(datos == 0){
												alert("La empresa no tiene aportes");
												$("#txtNit").val("");
												$("#txtPeriodo").val("");
												$("#lblRazonSocial").text("");
												return false;
											}
											alert("Datos Actualizados");
											$("#txtNit").val("");
											$("#txtPeriodo").val("");
											$("#lblRazonSocial").text("");
											observacionesTab(IdEmpresa);
										}	
									});
										
									
								});
							}
						});
						
					}
				});
			}
		});
	});
});

//asignar datepicker de periodo al id
function datepickerC(caso,id){
	switch(caso){
		case "PERIODO":
			$(id).datepicker({
		        dateFormat: 'yymm',
		        changeMonth: true,
		        changeYear: true,
		        showButtonPanel: true,
		        onClose: function(dateText, inst) {
		        	var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
		            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
		            $(this).datepicker('setDate', new Date(year, month, 1));
		        }
		    });
		    $(id).focus(function () {
				$(".ui-datepicker-calendar").hide();
				$("#ui-datepicker-div").position({
					my: "center top",
					at: "center bottom",
					of: $(this)
				});
			});
			break;
		case "FECHA":
			$(id).datepicker({
				changeMonth: true,
				changeYear: true,
				maxDate: "+0D"
			});
			break;
	}
	
}

/**MANDA A EXEL**/
function procesar01(){
	var periodo   = $("#txtPeriodo").val().trim();
	var IdEmpresa = $("#hidIdEmpresa").val(); 
	var usuario=$("#txtusuario").val();
	url="http://"+getCookie("URL_Reportes")+"/aportes/aportes/rptExcel023.jsp?v0="+IdEmpresa+"&v1="+periodo+"&v2="+usuario;
	window.open(url,"_NEW");
}

function observacionesTab(IdEmpresa){
	var URL=src();
	$("div[name='div-observaciones-tab']").dialog("destroy");
	$("textarea[name='observacionGrupo']").val("");
	$("div[name='div-observaciones-tab']").dialog({
		  modal:true,
		  width:520,
		  resizable:false,
		  draggable:false,
		  closeOnEscape:false,
		  open:function(){
			  $(this).prev().children().hide();//Oculto la barra de titulo
			  $(this).prev().html("OBSERVACIONES...");//Pongo un nuevo titulo
			  $("textarea[name='observacionGrupo']").focus();
			  },
		  buttons:{
		    'Guardar':function(){

		    	var comentario='Se modifico el &iacute;ndice de aportes empresas Ley 1429';
		    	var observacioncaptura=$("textarea[name='observacionGrupo']").val();
				var observacion=comentario+' - '+observacioncaptura;
				var observacionSinEspacios = observacion.replace(/^\s+|\s+$/g,"");
				
				if(observacioncaptura.length==0){
			    	alert("Escriba las observaciones!");
					return false;
					}
					if(observacioncaptura.length<10){
						alert("Las observaciones son muy cortas...!");
						return false;
					}
					
				
			    if(observacionSinEspacios!=""){
				 $.ajax({
					   url:URL+"phpComunes/pdo.insert.notas.php",
					   type:"POST",
					   data:{v0:IdEmpresa,v1:observacion, v2:2},
					   success:function(data){
					    $("[name='rtaObservacion']").fadeIn();//el mensaje esta oculto en la capa div-observacion
						$("div[name='div-observaciones-tab']").dialog("close");
					   }
			     });//ajax
				 alert("Observacion guardada"); 
				}//if observacion
				else{
					alert("No se pudo guardar la observaci\xf3n");
				}
			 }//guardar
		  }//buttons
		  });   
}
	