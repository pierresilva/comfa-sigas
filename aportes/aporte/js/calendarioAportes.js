$(function(){
	/*CONTROLES
	 * **/
	$("#btnGuardar,#btnActualizar").button();
	
	/*EVENTOS 
	 * **/
	datepickerC("PERIODO","#txtPeriodo");
	datepickerC("FECHA_BETWEEN_LIBRE","#txtFechaInicial,#txtFechaFinal");
	$("#btnGuardar").bind("click",guardarCalendario);
	$("#btnActualizar").bind("click",actualizarCalendario);
	
	$("#cmbClaseAportante,#txtPeriodo").change(function(){
		inabilitarActualizar();
		if(!$("#cmbClaseAportante").val() || !$("#txtPeriodo").val())return false;
		
		var resultado = buscarCalendario();
		//Verificar que existan datos
		if(typeof resultado == "object" && resultado.length>0){
			
			//Verificar si el periodo ya esta procesado
			if(resultado[0].procesado=="S"){
				alert("El periodo ya existe y esta procesado!!");
				nuevo();
				return false;
			}
				
			if(confirm("El periodo ya existe, desea modificarlo!!")){
				llenarCampos(resultado[0]);
				habilitarActualizar();
			}
		}
	});
});

function habilitarActualizar(){
	$("#btnGuardar").hide();
	$("#btnActualizar").show();
}
function inabilitarActualizar(){
	$("#btnGuardar").show()
	$("#btnActualizar").hide()
}

function nuevo(){
	$("#txtPeriodo,#txtFechaInicial,#txtFechaFinal,#hidIdFechaVencimiento").val("");
	$("#cmbClaseAportante").val(0);
	inabilitarActualizar();
}

function llenarCampos(datos){
	$("#hidIdFechaVencimiento").val(datos.id_fecha_vencimiento);
	$("#txtPeriodo").val(datos.periodo_aporte);
	$("#cmbClaseAportante").val(datos.clase_aportante);
	$("#txtFechaInicial").val(datos.fecha_inicio);
	$("#txtFechaFinal").val(datos.fecha_final);
}

/*
 *FUNCIONES DE PETICIONES 
 * **/

function buscarCalendario(){
	var result = "",
		objDatos = {
			periodo_aporte: $("#txtPeriodo").val().trim(),
			clase_aportante: $("#cmbClaseAportante").val().trim()
		};
	
	$.ajax({
		url:"buscarCalendario.php",
		type:"POST",
		data:{objDatos:objDatos},
		dataType:"json",
		async:false,
		success:function(datos){
			result = datos;
		}
	});
	return result;
}
function guardarCalendario(){
	//Validar los datos requeridos del formulario
	if(validaCampoFormu())
		return false;
	
	var objDatos = {
			periodo_aporte: $("#txtPeriodo").val().trim(),
			clase_aportante: $("#cmbClaseAportante").val().trim(),
			fecha_inicio: $("#txtFechaInicial").val(),
			fecha_final: $("#txtFechaFinal").val()
		}
	
	$.ajax({
		url:"guardarCalendario.php",
		type:"POST",
		data:{objDatos:objDatos},
		dataType:"json",
		async:false,
		success:function(datos){
			if(datos==1){
				alert("El calendario se guardo correctamente!!.");
				nuevo();			
			}
			else
				alert("Error. El calendario no se guardo!!.");	
		}
	});
}

function actualizarCalendario(){
	//Validar los datos requeridos del formulario
	if(validaCampoFormu())
		return false;
	
	var objDatos = {
			id_fecha_vencimiento: $("#hidIdFechaVencimiento").val().trim(),
			periodo_aporte: $("#txtPeriodo").val().trim(),
			clase_aportante: $("#cmbClaseAportante").val().trim(),
			fecha_inicio: $("#txtFechaInicial").val(),
			fecha_final: $("#txtFechaFinal").val()
		}
	
	$.ajax({
		url:"actualizarCalendario.php",
		type:"POST",
		data:{objDatos:objDatos},
		dataType:"json",
		async:false,
		success:function(datos){
			if(datos==1){
				alert("El calendario se actualizo correctamente!!.");
				nuevo();			
			}
			else
				alert("Error. El calendario no se actualizo!!.");	
		}
	});
}

/**
 * Validar los elementos requeridos del formulario
 * @returns {Number}
 */
function validaCampoFormu(){
	var banderaError = 0;
	$( ".ui-state-error" ).removeClass( "ui-state-error" );
	$(".element-required").each(function(i,row){
		if( $( this ).val() == 0 ){
			$( this ).addClass( "ui-state-error" );
			banderaError++;
		}
	});
	return banderaError;
}