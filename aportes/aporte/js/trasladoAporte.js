$(function(){
	//direccion raiz del proyecto
	var URL=src();
	
	//crear el datepicker pera los campos de periodo
	datepickerC("PERIODO","#txtPeriodo");
	datepickerC("PERIODO","#txtPeriodoTraslado");
	//Crear el botton
	$("#btnTrasladar").button();
	nuevo();
	//Crear dialogo para visualizar las devoluciones
	$("#divDevoluciones").dialog({
		modal:true,
		autoOpen:false,
		width:700
	});
	//Evento para buscar los datos de la empresa
	$("#txtNit").blur(function(){
		nuevo();
		
		var nit = $("#txtNit").val().trim();
		if(nit=="")
			return false;
		
		var datosEmpresa = buscarDatosEmpresa(nit);				
		if(typeof datosEmpresa != "object"){
			$("#lblRazonSocial").addClass("classTextError").html(
					"No existe empresa con el nit " + nit);
			desabilitarCampoPeriodo();
			return false;
		}
		$("#lblRazonSocial").html(datosEmpresa.razonsocial);
		$("#hidIdEmpresa").val(datosEmpresa.idempresa);
		abilitarCampoPeriodo();
	});	
	
	//Metodo para buscar los aportes de la empresa
	$("#btnBuscarAportes").click(function(){
		limpiarCampos2("#tdMensajeError");
		desabilitarCamposTraslado();
		var periodo   = $("#txtPeriodo").val();
		var idEmpresa = $("#hidIdEmpresa").val(); 
		$("#tabAportes").css('display','none');
		if(periodo==0 || idEmpresa==0)
			return  false;
		
		var banderaInterrupcion = 0;
		$.ajax({
			url:"buscarInterrupcion.php",
			type:"POST",
			data:{idEmpresa:idEmpresa,periodo:periodo},
			dataType:"json",
			async:false,
			success:function(datos){
				if(datos==0)
					banderaInterrupcion = 1;
			}
		});
		
		if(banderaInterrupcion==0){
			$("#tdMensajeError").html("El periodo "+periodo+" tiene interrupcion");
			return false;
		}
		
		//Peticion para obtener los aportes
		$.ajax({
			url:URL+"aportes/empresas/buscarAportesDePeriodoPorNit.php",
			type:"POST",
			data:{idEmpresa:idEmpresa,periodo:periodo},
			dataType:"json",
			async:false,
			success:function(datos){
				if(datos==0){
					$("#tdMensajeError").html("No existen aportes para el periodo "+periodo);
					limpiarCampos2("#tBody");
					return false;
				}
				var tr = "";
				$.each(datos,function(i,row){
					tr += "<tr id='trAporte"+i+"' >" +
							"<td><input type='checkbox' name='chIdRegistro' id='chIdRegistro' " +
									"value='"+row.idaporte+"' onclick='activarDesactivarMod(\"trAporte"+i+"\");'/></td>" +
							"<td id='tdIdAporte'>"+row.idaporte+"</td>" + 
							"<td>"+row.numerorecibo+"</td>" +
							"<td>"+row.comprobante+"</td>" +
							"<td style='text-align:right'>$"+formatNumber(row.valoraporte)+"</td>" +
							"<td style='text-align:right'>$"+formatNumber(row.valornomina)+"</td>" +
							"<td>"+row.indicador+"</td>" +
							"<td>"+row.fechapago+"</td></tr>";
				});
				$("#tabAportes").css("display","block");
				$("#tBody").html(tr);
			}
		});
	});
	
	//Evento para buscar los datos de la empresa que va ha trasladar los aportes
	$("#txtNitTraslado").blur(function(){
		$("#lblRazonSocialTraslado").removeClass("classTextError");
		var nit = $("#txtNitTraslado").val().trim();
		if(nit=="")
			return false;
		datosEmpresaTraslado = buscarDatosEmpresa(nit);
		if(typeof datosEmpresaTraslado != "object"){
			$("#lblRazonSocialTraslado").addClass("classTextError").html(
					"No existe empresa con el nit " + nit);
			limpiarCampos2("#hidIdEmpresaTraslado");
			return false;
		}
		$("#lblRazonSocialTraslado").html(datosEmpresaTraslado.razonsocial);
		$("#hidIdEmpresaTraslado").val(datosEmpresaTraslado.idempresa);
	});	
	
	//Evento para manipular los campos del traslado de acuerdo al tipo
	$("#cmbTipoTraslado").change(function(){
		var tipo = $(this).val();
		limpiarCampos2("#txtNitTraslado,#hidIdEmpresaTraslado,#lblRazonSocialTraslado,#txtPeriodoTraslado");
		$("#txtNitTraslado,#txtPeriodoTraslado").removeClass("ui-state-error");
		if(tipo=="Periodo") {
			$("#trEmpresaTraslado").hide();
			$("#trPeriodoTraslado").show();
		}else if(tipo=="Empresa") {
			$("#trEmpresaTraslado").show();
			$("#trPeriodoTraslado").hide();
		}else if(tipo=="EmpresaPeriodo") {
			$("#trEmpresaTraslado,#trPeriodoTraslado").show();
		}
	});
});

/**
 * Busca los datos de la empresa de acuerdo al nit
 * @param nit
 * @returns Datos de la empresa
 */
function buscarDatosEmpresa(nit) {
	var data;
	$.ajax({
		url:URL+"phpComunes/buscarEmpresaNit.php",	
		type:"POST",
		data:{v0:nit},
		dataType:"json",
		async:false,
		success:function(datos){
			if(typeof datos == "object")
				data = datos[0];
		}
	});
	return data;
}

function abilitarCampoPeriodo(){
	$("#btnBuscarAportes,#txtPeriodo").attr("disabled",false);
}

function desabilitarCampoPeriodo(){
	$("#btnBuscarAportes,#txtPeriodo").attr("disabled",true);
	limpiarCampos2("#txtPeriodo");
}

function abilitarCamposTraslado(){
	$("#tblTraslado").show();
}

function desabilitarCamposTraslado(){
	$("#tblTraslado").hide();
	$("#cmbTipoTraslado").val("Periodo").trigger("change");
	limpiarCampos2("#txtNitTraslado,#lblRazonSocialTraslado,#hidIdEmpresaTraslado,#txtPeriodoTraslado");
}

//activar o desactivar los campos para modificar datos del aporte
function activarDesactivarMod(id){
	var numChecked = $("#tabAportes :checkbox:checked").length;
	var checked = $("#"+id+" #chIdRegistro").is(":checked");
	//Habilitar los campos del traslado
	if(numChecked>0)
		abilitarCamposTraslado();
	else
		desabilitarCamposTraslado();
	//Cambiar apariencia la fila del aporte
	if(checked==true)
		$("#"+id).addClass("ui-state-default");
	else
		$("#"+id).removeClass("ui-state-default");
			
}

//limpiar los campos
function nuevo() {
	desabilitarCampoPeriodo();
	desabilitarCamposTraslado();
	limpiarCampos2("#tdMensajeError,#tBody,#txtPeriodo,#hidIdEmpresa,#lblRazonSocial,#txaObservacion");
	$("#tabAportes").hide();
	$("#lblRazonSocial").removeClass("classTextError");
}

//modificar el aporte
function trasladar(){
	//Datos de la empresa antigua
	var idEmpresa = parseInt($("#hidIdEmpresa").val());
	var objNit = $("#txtNit");
	var objRazonSocial = $("#lblRazonSocial");
	var objPeriodo = $("#txtPeriodo");
	
	var tipoTraslado = $("#cmbTipoTraslado").val();
	//Datos de la empresa nueva
	var idEmpresaTraslado = parseInt($("#hidIdEmpresaTraslado").val());
	var objNitTraslado = $("#txtNitTraslado");
	var objRazonSocialTrasl = $("#lblRazonSocialTraslado");
	var objPeriodoTraslado = $("#txtPeriodoTraslado");
	var objObservacion = $("#txaObservacion");
	var numAportes = $("#tabAportes :checkbox:checked").length;
	
	$("#txtNit,#txtPeriodo,#txtNitTraslado,#txtPeriodoTraslado,#txaObservacion" +
			",#lblRazonSocialTraslado,#lblRazonSocial").removeClass("ui-state-error");

	//validacion campos vacios
	if(idEmpresa==0){objRazonSocial.addClass("ui-state-error");return false;}
	if(objNit.val()==0){objNit.addClass("ui-state-error");return false;}
	if(objPeriodo.val()==0){objPeriodo.addClass("ui-state-error");return false;}
	if(objObservacion.val()==0){objObservacion.addClass("ui-state-error");return false;}
	if(numAportes==0) {
		alert("Debe chequear los aportes a trasladar");
		return false;
	}
	if(tipoTraslado=="Periodo") {
		if(objPeriodoTraslado.val()==0){objPeriodoTraslado.addClass("ui-state-error");return false;}
		if(objPeriodo.val()==objPeriodoTraslado.val()){
			alert("Los periodos deben ser diferentes");
			return false;
		}
	}else if(tipoTraslado=="Empresa") {
		if(idEmpresaTraslado==0){objRazonSocialTrasl.addClass("ui-state-error");return false;}
		if(objNitTraslado.val()==0){objNitTraslado.addClass("ui-state-error");return false;}
		if(idEmpresa==idEmpresaTraslado){
			alert("La empresas deben ser diferentes");
			return false;
		}
	}else {
		if(idEmpresaTraslado==0){objRazonSocialTrasl.addClass("ui-state-error");return false;}
		if(objNitTraslado.val()==0){objNitTraslado.addClass("ui-state-error");return false;}
		if(objPeriodoTraslado.val()==0){objPeriodoTraslado.addClass("ui-state-error");return false;}
		if(idEmpresa==idEmpresaTraslado){
			alert("La empresas deben ser diferentes");
			return false;
		}
		if(objPeriodo.val()==objPeriodoTraslado.val()){
			alert("Los periodos deben ser diferentes");
			return false;
		}
	}
	
	var arrIdAportes = new Array();
	//------------------------
	//Obtenemos los id de los aportes a trasladar
	$("#tabAportes :checkbox:checked").each(function(i,row){
		arrIdAportes[arrIdAportes.length] = parseInt($(this).val());
	});
	
	
	//----------------------------------
	var objData = {
			tipoTraslado:tipoTraslado
			, arrIdAportes:arrIdAportes
			, idEmpresaTraslado:idEmpresaTraslado
			, periodoTraslado:objPeriodoTraslado.val()
			, observacion:objObservacion.val().trim()}
	//peticion para guardar el traslado
	$.ajax({
		url:"guardarTraslado.php",
		type:"POST",
		data:objData,
		dataType:"json",
		async:false,
		success:function(datos){
			if(typeof datos == "object"){
				preparaDialogDevolucion(datos);
				$("#divDevoluciones").dialog("open");
				return false;
			}else if(datos == 0){
				alert("El traslado del aporte no fue posible");
				return false;
			}else if(datos==2){
				alert("La nota no se guardo, pero el traslado se realizo con exito");
				nuevo();
				limpiarCampos2("#txtNit");
			}else{
				alert("El traslado del aporte se realizo correctamente");
				nuevo();
				limpiarCampos2("#txtNit");
			}
			
		}	
	});
}

function preparaDialogDevolucion(datos) {
	limpiarCampos2("#divDevoluciones tbody");
	var htmlDevolucion = "";
	console.log(datos);
	$.each(datos,function(i,row){
		htmlDevolucion += "<tr><td>"+row.idaporte+"</td>" +
				"<td style='text-align:left'>$"+formatNumber(parseInt(row.aaportes)-parseInt(row.daportes))+"</td>" +
				"<td>"+row.motivo+"</td></tr>";
	});
	$("#divDevoluciones tbody").html(htmlDevolucion);
}
	
	