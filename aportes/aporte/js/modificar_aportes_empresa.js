$(function(){
	//crear el datepicker pera el primer periodo
	datepickerC("PERIODO","#txtPeriodo");
	
	//direccion raiz del proyecto
	var URL=src();
	
	$("#txtNit").blur(function(){
		$("#tdMensajeError,#tBody,#txtPeriodo").html('');
		$("#tdPeriodo,#tabAportes").css('display','none');
		var nit = $("#txtNit").val().trim();
		if(nit=="")
			return false;
			
		$.ajax({
			url:"modificar_aportes_empresa.log.php",	
			type:"POST",
			data:{v0:"CONSULTAR_EMPRESA",v1:nit},
			dataType:"json",
			async:false,
			success:function(datos){
				if(datos['error']==1){
					$("#lblRazonSocial").html(datos['descripcion']);
					return false;
				}
				$("#lblRazonSocial").html(datos['razonSocial']);
				$("#hidIdEmpresa").val(datos['idEmpresa']);
				$("#tdPeriodo").css('display','block');
			}
		});
	});	
	$("#btnBuscarAportes").click(function(){
		$("#tdMensajeError").html('');
		var periodo   = $("#txtPeriodo").val();
		var idEmpresa = $("#hidIdEmpresa").val(); 
		$("#tabAportes").css('display','none');
		if(periodo=="" || idEmpresa=="")
			return  false;
		
		$.ajax({
			url:"modificar_aportes_empresa.log.php",
			type:"POST",
			data:{v0:"BUSCAR_APOSTES",v1:idEmpresa,v2:periodo},
			dataType:"json",
			async:false,
			success:function(datos){
				if(datos['error']==1){
					$("#tdMensajeError").html(datos['descripcion']);
					$("#tBody").html('');
					return false;
				}
				var tr = "";
				var array = indicesAporte(1,"I");
				$.each(datos['aportes'],function(i,row){
					tr += "<tr id='trAporte"+i+"' ><td><input type='checkbox' name='chIdRegistro' id='chIdRegistro' onclick='activarDesactivarMod(\"trAporte"+i+"\",\"TR\",\""+i+"\");'/></td><td id='tdIdAporte'>"+row['idaporte']+"</td>";
					tr += "<td><select id='cmbIndice' class='box1'  onchange='calcular(\"trAporte"+i+"\");' name='cmbIndice'><option value='0'>Seleccione..</option>"+indicesAporte(row["indicador"],array)+"</select></td>";
					tr += "<td><input type='text' style='text-align:right;' class='box1' onkeyup='formatMoneda(this,\"F\");' id='txtValorAporte' name='txtValorAporte' value='"+formatMoneda(row['valoraporte'],"A")+"' onblur='calcular(\"trAporte"+i+"\");'/></td>";
					tr += "<td><input type='text' style='text-align:right;' class='box1' onkeyup='formatMoneda(this,\"F\");' id='txtValorNomina' name='txtValorNomina' value='"+formatMoneda(row['valornomina'],"A")+"'/></td>"; 
					tr += "<td><input type='text' style='text-align:center; width: 80px;' class='classCampoCorto' id='txtFechaPago"+i+"' name='txtFechaPago"+i+"' value='"+row['fechapago']+"'/></td>";
					tr += "<td><input type='text' style='text-align:center; width: 50px;' class='classCampoCorto' id='txtPeriodo' name='txtPeriodo' value='"+row['periodo']+"'/></td>";
					tr += "<td><textarea style='text-align:left; width: 300px; height:20px;' class='classCampoCorto' id='txaObservacion' ></textarea> </td>";
					tr += "<td ><img src='"+URL+"imagenes/menu/modificar.png' id='idImagen' width='16' height='16' style='cursor:pointer'  title='Modificar' onclick='modificarR(\"trAporte"+i+"\",\""+i+"\");' /></td></tr>";
					//formatNumber(row['valoraporte'],'$')formatNumber(row['valornomina'],'$')
				});
				$("#tabAportes").css("display","block");
				$("#tBody").html(tr);
				activarDesactivarMod("tBody","TABLA",0);
			}
		});
	});
});

//asignar datepicker de periodo al id
function datepickerC(caso,id){
	switch(caso){
		case "PERIODO":
			$(id).datepicker({
		        dateFormat: 'yymm',
		        changeMonth: true,
		        changeYear: true,
		        showButtonPanel: true,
		        onClose: function(dateText, inst) {
		        	var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
		            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
		            $(this).datepicker('setDate', new Date(year, month, 1));
		        }
		    });
		    $(id).focus(function () {
				$(".ui-datepicker-calendar").hide();
				$("#ui-datepicker-div").position({
					my: "center top",
					at: "center bottom",
					of: $(this)
				});
			});
			break;
		case "FECHA":
			$(id).datepicker({
				changeMonth: true,
				changeYear: true,
				maxDate: "+0D"
			});
			break;
	}
	
}

//obtener los indices
function indicesAporte(id,array){
	var resultado = "";
	if(array=="I"){
		$.ajax({
			url:URL+"phpComunes/buscarIndicador.php",
			type:"POST",
			data:{},
			dataType:"json",
			async:false,
			success:function(datos){
				resultado = datos;
			}
		});
	}else{
		id=(isNaN(parseFloat(id)))?0:id;
		$.each(array,function(i,row){
			resultado +="<option "+((parseFloat(id)==parseFloat(row.codigo))?"selected='selected'":"")+" value='"+row.codigo+"'>"+row.detalledefinicion+"</option>";
		});
	}	
	return resultado;
}

//activar o desactivar los campos para modificar datos del aporte
function activarDesactivarMod(id,tipo,cont){
	if(tipo=="TABLA"){
		$("#"+id+" :input").attr("readonly",true);
		$("#"+id+" #idImagen").css("display","none");
	}else if(tipo=="TR"){
		var checked = $("#"+id+" #chIdRegistro").is(":checked");
		if(checked==true){
			$("#"+id+" :input").removeAttr("readonly");
			$("#"+id+" #idImagen").css("display","block");
			datepickerC("FECHA",("#"+id+" #txtFechaPago"+cont));
			datepickerC("PERIODO",("#"+id+" #txtPeriodo"));
			$("#"+id).addClass("ui-state-default");
		}else{
			$("#"+id+" :input").attr("readonly",true);
			$("#"+id+" #idImagen").css("display","none");
			$("#"+id+" #txtFechaPago"+cont).datepicker("destroy");
			$("#"+id+" #txtPeriodo").datepicker("destroy");
			$("#"+id).removeClass("ui-state-default");
		}		
	}	
}

//calcular el valor nomina dependiendo del indice seleccionado
function calcular(id){
	var aportes = $("#"+id+" #txtValorAporte").val().trim();
	var indice = parseFloat($("#"+id+" #cmbIndice").val());
	
	if(indice==0)
		return false;
	
	aportes = aportes== "" ? 0 : parseInt(formatNormal(aportes));	
	var nomina = Math.round((aportes * 100) / indice);
	$("#"+id+" #txtValorNomina").val(nomina);
} 

//modificar el aporte
function modificarR(id,cont){
	
	//recoger los datos a modificar
	var idAporte    = $("#"+id+" #tdIdAporte").text();
	var indicador   = $("#"+id+" #cmbIndice");
	var valorAporte = $("#"+id+" #txtValorAporte");
	var valorNomina = $("#"+id+" #txtValorNomina");
	var fechaPago   = $("#"+id+" #txtFechaPago"+cont);
	var periodo     = $("#"+id+" #txtPeriodo");
	var observacion = $("#"+id+" #txaObservacion");
	//-----------------------------
	$("#trAporte1 input,#trAporte1 select, #trAporte1 textarea").removeClass("ui-state-error");

	//validacion campos vacios
	if(indicador.val()==""){indicador.addClass("ui-state-error");return false;}
	if(valorAporte.val().trim()==""){valorAporte.addClass("ui-state-error");return false;}
	if(valorNomina.val().trim()==""){valorNomina.addClass("ui-state-error");return false;}
	if(fechaPago.val().trim()==""){fechaPago.addClass("ui-state-error");return false;}
	if(periodo.val().trim()==""){periodo.addClass("ui-state-error");return false;}
	if(observacion.val().trim()==""){observacion.addClass("ui-state-error");return false;}
	//------------------------
	
	valorAporte = formatNormal(valorAporte.val().trim());
	valorNomina = formatNormal(valorNomina.val().trim());
	indicador   = parseFloat( indicador.val() );
	
	//calcular la distribucion del aporte
	var caja = icbf = sena = 0;
	switch ( indicador ){
		case 0.6:
			caja=valorAporte;
			break;
		case 2:
			caja=valorAporte;
			break;
		case 4:
			caja=valorAporte;
			break;
		case 9:
			caja=parseInt(valorNomina*0.04);
			icbf=parseInt(valorNomina*0.03);
			sena=valorAporte-caja-icbf;
			break;
	}
	//----------------------------------
	
	//peticion para modificar el aporte
	$.ajax({
		url:'modificar_aportes_empresa.log.php',
		type:'POST',
		data:{v0:'MODIFICAR',v1:idAporte,v2:indicador,v3:valorAporte,v4:valorNomina,v5:fechaPago.val().trim(),v6:periodo.val().trim(),v7:caja,v8:icbf,v9:sena,v10:observacion.val().trim()},
		dataType:'json',
		async:true,
		success:function(datos){
			if(datos == 0){
				alert("La Actualizaci\xF3n no fu\xE9 posible");
				return false;
			}
			alert("El aporte fu\xE9 modificado");
			//validar si cambio el periodo de aporte
			if(periodo.val()==$("#txtPeriodo").val()){
				$("#"+id+" #chIdRegistro").attr("checked",false);
				activarDesactivarMod(id,"TR",0);
			}else{
				//remover el tr si el periodo del aportes fue cambiado
				$("#"+id).remove();
			}	
		}	
	});
}

//formatear numeros
function formatMoneda(input,tipo){
	var num = 0;
	
	if(tipo=="F")
		num = input.value;
	else 
		num = input;
	
	num = num.replace(/\./g,'');
	
	if(!isNaN(num)){
		num = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1.');
		num = num.split('').reverse().join('').replace(/^[\.]/,'');
		if(tipo=="F")
			input.value = num;
		else
			return num;
	}
	else{ 
		return false;
	}
}

function formatNormal(input){
	var txt=input.replace(/\./g,'');
	////////////////////////////////////////// Validaciones
	if(txt.length > 0){
		if(isNaN(txt))
			return 0;
		else{
			if(txt>0)
				return parseInt(txt);
			else
				return 0;
		}
	} else {
		return 0;
	}
}
	
	