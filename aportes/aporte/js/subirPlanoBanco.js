/*
* @autor:      Ing. Orlando Puentes
* @fecha:      24/09/2010
* objetivo:
*/
/* var URL=src();*/


function procesar(){
	$("#ajax").html('<img src=../../imagenes/ajax-loader.gif />');
	$("#boton").html(' ');
	
	$.ajax({
		url: 'procesarPlanoBanco.php',
		type: "POST",
		async:false,
		success: function(datos){
			if(datos==1){alert('No se proceso el archivo la FECHA DE PAGO corresponde a un CIERRE CONTABLE')}
		    document.getElementById("msjCopia").style.display="block";
		    //document.getElementById("listaError").style.display="block";
		}
	});
	
	var bandera = true;
	while (bandera){
		$.ajax({
			url: 'leerLogNoProc.php',
			dataType: 'json',
			async: false,
			/*complete: function(objeto, exito){
				console.log(exito);
				if(exito=="success"){
					bandera = false;
					$("#ajax").html('&nbsp;');					
				}
					
			},*/
			success: function(datosErrores){
				if(datosErrores instanceof Array){
					bandera = false;
					$("#ajax").html('&nbsp;');
					document.getElementById("listaError").style.display="block";
				}
				
			   if (datosErrores instanceof Array && datosErrores.length>0){
				   $("#trErrores").show();
			       $.each(datosErrores,function(i,fila){
		    	       $("#tbErrores").append("<tr>" +
		        	           "<td style='text-align: center' >" + fila.error + "</td>" +
		        	           "<td>&nbsp;" + fila.Planilla + "</td>" +
		        	           "<td style='text-align: center'>" + fila.Nit + "</td>" +
		        	           "<td style='text-align: right'>" + fila.ValorPagado + "&nbsp;&nbsp;</td>" +
		        	           "<td style='text-align: right'>" + fila.Periodo + "&nbsp;&nbsp;</td>" +
		                    "</tr>");
					});
				}
		    }	
		});
	}

}

function mensaje(){
	document.getElementById('boton').innerHTML="";
	document.getElementById('mensaje').innerHTML="Procesando por favor espere!";
}

function validarArchivoProcesar(){
	if($("#archivo").val()==0){
		alert("Primero debe examinar el archivo a procesar");
		return false;
	}
	return true;
}