/*
* @autor:      Ing. Orlando Puentes
* @fecha:      06/10/2010
* objetivo:
*/
// JavaScript Document
//var URL=src();
var idempresa;
var fechaafiliacion;
var rutaDocsAnt;
$(document).ready(function(){	
	limpiar();
	$("#txtId").focus();
	$("#periodos").periodos({claseMes:'ui-state-default',idDestino:'etiquetas',anioCompleto:true});
});
/*$(function() {
		$("#ayuda").dialog({
		 	autoOpen: false,
			height: 450, 
			width: 700, 
			open: function(evt, ui){
					$('#ayuda').html('');
					$.get('../../help/aportes/ayudaActualizarNIT.html',function(data){
							$('#ayuda').html(data);
					});
			 }
		});
	});*/

function buscarEmpresa(nit){
	if(nit.value==""){
		alert("Escriba el NIT de la Empresa!");
		$("#txtId2").html('');
			return false;
		}
	else{
		$.getJSON('../../aportes/empresas/buscarEmpresaNIT.php',{v0:nit.value},function(datos){
			if(datos!=0){
				$.each(datos, function(i, fila){
					$("#txtId2").html(fila.razonsocial);
					idempresa=fila.idempresa;
				});
			}else{
				alert("No existe empresa con ese numero de NIT!");
				$("#txtId2").html('');
			}
		});
	}
}
	
function guardarInterrupcion(){
	var nitEmpresa=$("#txtId").val();//nit
	var observaciones=$("#areaObserv").val();//observaciones
	if(nitEmpresa.value==""){
			alert("Escriba el NIT de la Empresa!");
			return false;
		}
	//PERIODOS
	periodos=$("#array").val();
	if(periodos.length==0){
			alert("Debe seleccionar por lo menos un periodo!");
			return false;
		}
		arrPeriodos=periodos.split(",");
		$.getJSON('guardarInterrupcion.php',{idempresa:idempresa,observaciones:observaciones,periodos:arrPeriodos,nitempresa:nitEmpresa},function(data){
			//alert(data);
			if(data==0)
			{
				alert("Fallo al guardar la interrupcion en la 37!!");
				return false;
			}else if(data==1)
				{
					alert("Fallo al guardar la interrupcion en la 38!!");
					return false;
				}else if(data==2)
					{
						alert("Fallo al guardar la interrupcion en la 97!!");
						return false;
					}
					else if(data==97){
						alert("Se guardo la interrupcion!");
					}
		});
}	
	
function limpiar(){
	limpiarCampos();
	$("#etiquetas").empty();
	}	
	

