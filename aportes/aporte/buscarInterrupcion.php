<?php
	ini_set("display_errors", 1);
	$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
	include_once  $root;
	
	include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
	include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
	
	$db = IFXDbManejador::conectarDB();
	if($db->conexionID==null){
		$cadena = $db->error;
		echo msg_error($cadena);
		exit();
	}
	
	$periodo = $_REQUEST["periodo"];
	$idEmpresa = $_REQUEST["idEmpresa"];
	
	$sql = "SELECT * 
			FROM aportes037 a37
				INNER JOIN aportes038 a38 ON a38.idinterrupcion=a37.idinterrupcion
			WHERE a37.idempresa=$idEmpresa AND a38.periodo='$periodo'";
	$rs = $db->querySimple($sql);
	if($rs)
		echo json_encode($rs->fetch());
	else
		echo 0;
	
?>