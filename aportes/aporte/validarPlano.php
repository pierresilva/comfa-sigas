<?php
set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
include_once $raiz.DIRECTORY_SEPARATOR. 'funcionesComunes'.DIRECTORY_SEPARATOR.'funcionesComunes.php';
include_once $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas'.DIRECTORY_SEPARATOR. 'config.php';
include_once $raiz.DIRECTORY_SEPARATOR.'phpComunes'.DIRECTORY_SEPARATOR.'wssdk'.DIRECTORY_SEPARATOR.'ClientWSInfWeb.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
include_once $raiz.DIRECTORY_SEPARATOR. 'clases'.DIRECTORY_SEPARATOR.'Logger.php';
$log = new Logger(3);

$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

$usuario=$_SESSION['USUARIO'];
$directorio =$ruta_cargados."tesoreria".DIRECTORY_SEPARATOR.'auditoria'. DIRECTORY_SEPARATOR;

$dir = opendir($directorio);
$flag=true;
while ($elemento = readdir($dir))
{      
    if(strlen($elemento)>2){
        $archivos[]=$elemento;
        $flag=true;
    }
}
closedir($dir);
$arcProc=$archivos[0];


if(!$flag){
    echo "NO hay archivos de pagos de aportes!!";
    exit();
}


$ws = new ClientWSInfWeb(USUARIO_WEB_SIGAS, CONTRASENA_WEB_SIGAS);

$tabla = "<br/><table border='1' class='tablero'><tr><td><b>Nit</b></td><td><b>Fecha pago</b></td><td><b>Valor</b></td><td><b>periodo</b></td></td><td><b>Planilla</b></td><td><b>Procesado...</b></td></tr>";
$tablaResulMovi = "";
$tablaResulSigas = "";
for($i=0; $i<count($archivos); $i++){
    $arcProc=$archivos[$i];
    $directorio.=$archivos[$i]; 
    $archivoS = file($directorio);
    $lineas = count($archivoS);
    $fechaPago=trim(substr($archivoS[0],1,8));
    for($c=0; $c < $lineas; $c++){
        if($c > 1){
            $tipo=trim(substr($archivoS[$c],0,1));
            $tipo=intval($tipo);
            
            if($tipo != 6) continue;
            
            $nit=trim(substr($archivoS[$c],1,16)); //nit
            $planilla=trim(substr($archivoS[$c],41,15));
            $periodo=trim(substr($archivoS[$c],56,6));
            $valor=intval(trim(substr($archivoS[$c],72,16)));
            //$cadena="Aportes ".$periodo." ".$nit." ".$fechaPago;
            //$tablaResulSigas.= verificarSigas($db, $planilla, $fechaPago, $periodo, $valor, $nit);
            
            if(verificarExistencia($db,$planilla,$fechaPago,$periodo,$valor)==0){
				//no fue grabado en SIGAS
				$tablaResulSigas.= "<tr ><td>$nit</td><td>$fechaPago</td><td>$valor</td><td>$periodo</td><td>$planilla</td><td>No existe el pago</td>";
            }
            
            $exMovi = verificarMovi($nit,$fechaPago,$valor,$ws);

            if($exMovi==0){
                $nit=substr($nit, 0,-1);
                $exMovi =verificarMovi($nit,$fechaPago,$valor,$ws);
                if($exMovi==-1){
                    $tablaResulMovi .= "<tr ><td>$nit</td><td>$fechaPago</td><td>$valor</td><td>$periodo</td><td>$planilla</td><td>Error en los datos</td>";
                }
                elseif ($exMovi==0){
                	$tablaResulMovi .= "<tr ><td>$nit</td><td>$fechaPago</td><td>$valor</td><td>$periodo</td><td>$planilla</td><td>No existe el pago</td>";
                }/*elseif ($exMovi>0){
                    
                }*/
                // -1 error en los datos
                // 0 no existe el pago
                // > 0 es el num del recibo
            }
        }
    } 
}
$mensaje="<br/>Todos los pagos del plano validado estan bien.";
if($tablaResulMovi!=""){
    echo "<h2>NO EXISTE EL PAGO EN LA MOVI</h2>".$tabla.$tablaResulMovi."</table>";
}else{
    echo "<h2>NO EXISTE EL PAGO EN LA MOVI</h2>".$mensaje;
}

if($tablaResulSigas!=""){
    echo "<h2>NO EXISTE EL PAGO EN SIGAS</h2>".$tabla.$tablaResulSigas."</table>";
}else{
    echo "<h2>NO EXISTE EL PAGO EN SIGAS</h2>".$mensaje;
}
unlink($directorio);

function verificarSigas($db, $planilla, $fechaPago, $periodo, $valor, $nit){
	$tr = "";
	//$estadoProceso = "";
	
//	$sql="select top 1 nit, idempresa from aportes010 where planilla='$planilla'";
//	$rse=$db->querySimple($sql);
//	$cont=0;
//	while ($row=$rse->fetch()){
//		$idempresa=$row['idempresa'];
//		$nit2=$row['nit'];
//		$cont++;
//	}
//	if($cont==0){
//		//no se encontro la planilla
//		return;
//	}
	if(verificarExistencia($db,$planilla/*,$idempresa*/,$fechaPago,$periodo,$valor)==0){
		//no fue grabado en SIGAS
		$tr = "<tr ><td>$nit</td><td>$fechaPago</td><td>$valor</td><td>$periodo</td><td>$planilla</td><td>No existe el pago</td>";
	}
	else{
		//se grabo
		$tr = "";
	}
	
	return $tr;
	
}

function verificarExistencia($db,$planilla/*,$idempresa*/,$fechaPago,$periodo,$nomina){
	//verificar q no se aya cargado ese registro aportes011
	//$sql="select count(*) as cuenta from aportes011 where planilla='$planilla' and idempresa='$idempresa' and fechapago='$fechaPago' and periodo='$periodo' and valornomina='$nomina'";
	$sql="select count(*) as cuenta from aportes011 where planilla='$planilla'  and fechapago='$fechaPago' and periodo='$periodo'";
	$rsv=$db->querySimple($sql);
	$row=$rsv->fetch();
	$num=$row['cuenta'];
	return $num;
}

function verificarMovi($nit,$fecha,$valor,$ws){
	$anno=substr($fecha,0, 4);
	$mes=substr($fecha,4,2);
	$dia=substr($fecha, -2);
	$f=new Fecha($anno,$mes,$dia);
	$comprob='CPU';
	//$ws = new ClientWSInfWeb(USUARIO_WEB_SIGAS, CONTRASENA_WEB_SIGAS);
	return  $ws->movimientoNumero($comprob, $nit, $f, $valor);
}

?>

 

