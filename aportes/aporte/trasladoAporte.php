<?php
	set_time_limit(0);
	ini_set("display_errors",'1');
	date_default_timezone_set("America/Bogota");
	$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
	include_once  $root;
	$usuario=$_SESSION['USUARIO'];
	$fecver = date('Ymd h:i:s A',filectime('trasladoAporte.php'));
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<title>Traslado Aportes</title>
		<link type="text/css" href="../../css/Estilos.css" rel="stylesheet"/>
		<link type="text/css" href="../../css/marco.css" rel="stylesheet"/>
		<link type="text/css" rel="stylesheet" href="../../css/css.1.8/jquery-ui-1.8.17.custom.css" />
		<script language="javascript" src="../../js/1.6/jquery-1.6.2.min.js"></script>
		<script language="javascript" src="../../js/1.6/jquery-ui-1.8.16.custom.min.js"></script>
		<script language="javascript" src="../../js/comunes.js"></script>
		<script language="javascript" src="js/trasladoAporte.js"></script>
		<style type="text/css">
			.classCampoCorto {background: none repeat scroll 0 0 #F7F7F7; border-style: hidden; border-width: 0; font-size: 11px; padding: 0;}
			.classTextError {color:#ff0000} 
		</style>
	</head>
	<body>
		<center>
			<br /><br />
			<table width="60%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="arriba_iz" >&nbsp;</td>
					<td class="arriba_ce" ><span class="letrablanca">::Traslado de Aportes::</span>
						<div style="text-align:right;float:right;">
							<?php echo 'Versi&oacute;n: ' . $fecver; ?>
						</div>
					</td>
					<td class="arriba_de" >&nbsp;</td>
				</tr>
				<tr>
					<td class="cuerpo_iz">&nbsp;</td>
					<td class="cuerpo_ce"><img src="../../imagenes/spacer.gif" alt="" width="2" height="1" /></td>
					<td class="cuerpo_de">&nbsp;</td>
				</tr>
				<tr>
					<td class="cuerpo_iz" >&nbsp;</td>
					<td class="cuerpo_ce" >
						<table border="0" class="tablero" cellspacing="0" width="100%" >
							<tr>
						    	<td width="100" style="text-align:right;">
						    		Nit:
						    		<input type="hidden" name="hidIdEmpresa" id="hidIdEmpresa"/>
						    	</td>
						    	<td >
						    		<input type="text" name="txtNit" id="txtNit" />
						    		&nbsp; <label id="lblRazonSocial" >&nbsp;</label>
						    	</td>
						  	</tr>
						  	<tr>
							    <td style="text-align:right;">Periodo:</td>
							    <td >
							    		<input  type="text" readonly="readonly"size="10" name="txtPeriodo" id="txtPeriodo" disabled="disabled"/> 
							    		&nbsp;&nbsp;<input type="button" value="Buscar" id="btnBuscarAportes" class="ui-state-default" disabled="disabled"/>
							    </td>
							</tr>
						  	<tr>
						    	<td colspan="2" id="tdMensajeError" class="classTextError"></td>
						  	</tr>
						  	<tr>
						    	<td colspan="2"><b>Aportes de la Empresa</b></td>
						  	</tr>
					  	</table>
					  	
						<table border="0" class="tablero" cellspacing="0" width="100%" id="tabAportes" style="display: none;"> 
							<thead width="100%">
						  		<tr>
						  			<th width="5%"></th>
						  			<th width="5%">Id</th>
						  			<th width="15%">Recibo</th>
						  			<th width="5%">Comprobante</th>
						  			<th width="20%">Aporte</th>
						  			<th width="20%">Valor Nomina</th>
						  			<th width="5%">Indice</th>
						  			<th width="5%">Fecha Pago</th>
						  		</tr>
						  	</thead>
						  	<tbody id="tBody" width="100%">		
						  	</tbody>
						  	<tfoot><tr><th colspan="8">&nbsp;</th></tr></tfoot>
						</table>
						<table border="0" class="tablero" cellspacing="0" width="100%" id="tblTraslado" >
							<tr><td colspan="2"><b>Datos del Traslado</b></td></tr>
							<tr>
						    	<td width="100" style="text-align:right;">
						    		Tipo Traslado:
						    	</td>
						    	<td>
						    		<select name="cmbTipoTraslado" id="cmbTipoTraslado">
						    			<option value="Periodo">Periodo</option>
						    			<option value="Empresa">Empresa</option>
						    			<option value="EmpresaPeriodo">Empresa y Periodo</option>
						    		</select>
						    	</td>
						  	</tr>
						  	<tr>
							    <td colspan="2" ></td>
							</tr>
							<!-- TIPO TRASLADO EMPRESA -->
							<tr id="trEmpresaTraslado">
								<td style="text-align:right;">Nit:</td>
								<td>
									<input type="text" name="txtNitTraslado" id="txtNitTraslado" />
									<input type="hidden" name="hidIdEmpresaTraslado" id="hidIdEmpresaTraslado" />
						    		&nbsp; <label id="lblRazonSocialTraslado" >&nbsp;</label>
						    	</td>
							</tr>
							<!-- TIPO TRASLADO PERIODO -->
							<tr id="trPeriodoTraslado">
								<td style="text-align:right;">Periodo:</td>
								<td>
									<input  type="text" readonly="readonly"size="10" name="txtPeriodoTraslado" id="txtPeriodoTraslado"/>
						    	</td>
							</tr>
							<tr >
								<td style="text-align:right;">Observaci&oacute;n:</td>
								<td>
									<textarea name="txaObservacion" id="txaObservacion" cols="80" rows="3" maxlength="250"></textarea>
						    	</td>
							</tr>
							<tr>
								<td style="text-align:center;" colspan="2">
									<input type="button" name="btnTrasladar" id="btnTrasladar" value="Trasladar" onclick="trasladar();"/>
								</td>
							</tr>
					  	</table>
					</td>
					<td class="cuerpo_de" >&nbsp;</td>
				</tr>
				<tr>
					<td class="cuerpo_iz" >&nbsp;</td>
					<td class="cuerpo_ce" >&nbsp;</td>
					<td class="cuerpo_de" >&nbsp;</td>
				</tr>
				<tr>
					<td class="abajo_iz" >&nbsp;</td>
					<td class="abajo_ce" ></td>
					<td class="abajo_de" >&nbsp;</td>
				</tr>
			</table>
		</center>
		<div id="divDevoluciones">
			<b>Los siguientes aportes tiene devolucion, por tanto no es posible realizar el traslado</b>
			<br/>
			<br/>
			<table border="0" class="tablero" id="tblDevolucion" width="80%" align="center" >
				<thead>
					<tr>
						<th>Id Aporte</th>
						<th>Valor</th>
						<th>Motivo</th>
					</tr>
				</thead>
				<tbody></tbody>				
			</table>
			<br/>
			<br/>
		</div>
	</body>
</html>
