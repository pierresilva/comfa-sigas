<?php
	date_default_timezone_set('America/Bogota');
	$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
	include_once  $root;
	include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
	include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
	
	include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'p.aportes.class.php';
	include_once $raiz.DIRECTORY_SEPARATOR.'aportes'.DIRECTORY_SEPARATOR.'empresas'.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'empresas.class.php';
	
	$db = IFXDbManejador::conectarDB();
	if($db->conexionID==null){
		$cadena = $db->error;
		echo msg_error($cadena);
		exit();
	}
	
	//Parametros
	$tipoTraslado = $_REQUEST["tipoTraslado"];
	$idEmpresaTraslado = $_REQUEST["idEmpresaTraslado"];
	$periodoTraslado = $_REQUEST["periodoTraslado"];
	$arrIdAportes = $_REQUEST["arrIdAportes"];
	$observacion = $_REQUEST["observacion"];
	
	$idAportes = join($arrIdAportes,',');
	
	$sqlDevolucion = "SELECT * FROM aportes058 WHERE idaporte IN($idAportes)";
	$rsDevolucion = $db->querySimple($sqlDevolucion);
	$arrDatosDevolucion = array();
	while($row=$rsDevolucion->fetch()){
		$arrDatosDevolucion[] = $row;
	}
	
	if(count($arrDatosDevolucion)>0)
		echo json_encode($arrDatosDevolucion);
	else {
		$objAportes = new Aportes();
		$objEmpresa = new Empresa();
		
		$objAportes->inicioTransaccion();		
		
		if($tipoTraslado=="Periodo") {
			$idEmpresaTraslado = null;
		}else if($tipoTraslado=="Empresa") {
			$periodoTraslado = null;
		}
		
		$arrDatosAporte = $objAportes->buscar_aporte_traslado($idAportes);
		$rs = $objAportes->actualizar_traslado_aporte($idAportes,$idEmpresaTraslado,$periodoTraslado,$_SESSION["USUARIO"]);
				
		if($rs>0 && count($arrDatosAporte)>0){
			$banderaError = 0; 
			
			//Preparar los datos del update
			$datosUpdate = array();
			$datosUpdate["periodo"] = ($periodoTraslado==null)?$arrDatosAporte[0]["periodo"]:$periodoTraslado;
			$datosUpdate["idempresa"] = ($idEmpresaTraslado==null)?$arrDatosAporte[0]["idempresa"]:$idEmpresaTraslado;
			
			//Buscar datos de la empresa
			$datosEmpresa = $objEmpresa->buscar_empresa_por_idempresa($datosUpdate["idempresa"]);
			while($row=mssql_fetch_array($datosEmpresa)){
				$datosUpdate["nit"] = $row["nit"];
				$datosUpdate["razonsocial"] = $row["razonsocial"];
			}
			
			//Actualizar el encabezado de la planilla, donde se encuentra la mora
			foreach($arrDatosAporte as $datoAporte){
				//Preparar los datos del filtro
				$datosFiltro = array();
				$datosFiltro["periodo"] = $datoAporte["periodo"];
				$datosFiltro["fechapago"] = array("I"=>$datoAporte["fechapago"],"F"=>$datoAporte["fechapago"]);
				$datosFiltro["planilla"] = $datoAporte["planilla"];
				$datosFiltro["idempresa"] = $datoAporte["idempresa"];
				
				//Buscar el encabezado empresa 
				$datoEncabAport = $objAportes->buscar_encabezado_aporte($datosFiltro);
				if(count($datoEncabAport)>0){
					$rsAporte = $objAportes->actualizar_encabezado_aporte($datosUpdate,$datosFiltro);
					if($rsAporte==0){
						$banderaError++;
						break;
					}
				}
			}
			
			//Debug
			//$objAportes->cancelarTransaccion();
			//var_dump($banderaError);
			//exit();
			
			if($banderaError==0){
				//Guardar las notas para los aportes
				$contador = 0;
				foreach($arrIdAportes as $idAporte){
					$rsGuardarNota = $objAportes->guardarNota($idAporte, $observacion, "T");
					$contador += $rsGuardarNota;
				}
				
				$objAportes->confirmarTransaccion();
				if($contador==count($arrIdAportes))
					echo 1;
				else
					echo 2;
			}else{
				$objAportes->cancelarTransaccion();
				echo 0;
			}
					
		}else{
			$objAportes->cancelarTransaccion();
			echo 0;
		}
	}
?>