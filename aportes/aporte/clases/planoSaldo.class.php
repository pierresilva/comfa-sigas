<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';

include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'utilitario.class.php';

class PlanoSaldo{
	
	private $idSaldo;
	private $idProceso;
	private $idEtapaProceso;
	private $idArchivo;
	private $error;
	private $estado;
	private $usuario;
	
	private $bin;
	private $nit;
	private $numeroTarjeta;
	private $miembro;
	private $nombreTitular;
	private $saldoDisponible;	
	private $estadoSaldo;
	private $descripcionEstado;
	private $subtipo;
	private $codigoCuenta;
	private $fechaArchivo;
	
	private $saldoDisponibleDB;
	
	private static $con = null;
	
	function __construct(){
		//Conexion
		try{
			self::$con = IFXDbManejador::conectarDB();
			if( self::$con->conexionID==null ){
				throw new Exception(self::$con->error);
			}
		}catch(Exception $e){
			echo $e->getMessage();
			exit();
		}
	}
	
	/*
	 * GETTER
	*/
	
	public function getIdSaldo( ){ return $this->idSaldo;}
	public function getIdProceso( ){ return $this->idProceso;}
	public function getIdEtapaProceso( ){ return $this->idEtapaProceso;}
	public function getIdArchivo( ){ return $this->idArchivo;}
	public function getError( ){ return $this->error;}
	public function getEstado( ){ return $this->estado;}
	public function getUsuario( ){ return $this->usuario;}
	
	public function getBin(){return $this->bin; }
	public function getNit(){return $this->nit; }
	public function getNumeroTarjeta(){return $this->numeroTarjeta; }
	public function getMiembro(){return $this->miembro; }
	public function getNombreTitular(){return $this->nombreTitular; }
	public function getSaldoDisponible(){return $this->saldoDisponible; }
	public function getEstadoSaldo(){return $this->estadoSaldo; }
	public function getDescripcionEstado(){return $this->descripcionEstado; }
	public function getSubtipo(){return $this->subtipo; }
	public function getCodigoCuenta(){return $this->codigoCuenta; }
	public function getFechaArchivo(){return $this->fechaArchivo; }
	
	public function getSaldoDisponibleDB(){return $this->saldoDisponibleDB; }
					
	/*
	 * SETTER
	 */

	public function setIdSaldo( $idSaldo ){ $this->idSaldo = $idSaldo;}
	public function setIdProceso( $idProceso ){ $this->idProceso = $idProceso;}
	public function setIdEtapaProceso( $idEtapaProceso ){ $this->idEtapaProceso = $idEtapaProceso;}
	public function setIdArchivo( $idArchivo ){ $this->idArchivo = $idArchivo;}
	public function setError( $error ){ $this->error = $error;}
	public function setEstado( $estado ){ $this->estado = $estado;}
	public function setUsuario( $usuario ){ $this->usuario = $usuario;}
	
	public function setBin( $bin ){ $this->bin = $bin; }
	public function setNit( $nit ){ $this->nit = $nit; }
	public function setNumeroTarjeta( $numeroTarjeta ){ $this->numeroTarjeta = $numeroTarjeta; }
	public function setMiembro( $miembro ){ $this->miembro = $miembro; }
	public function setNombreTitular( $nombreTitular ){ $this->nombreTitular = $nombreTitular; }
	public function setSaldoDisponible( $saldoDisponible ){ $this->saldoDisponible = $saldoDisponible; }
	public function setEstadoSaldo( $estadoSaldo ){ $this->estadoSaldo = $estadoSaldo; }
	public function setDescripcionEstado( $descripcionEstado ){ $this->descripcionEstado = $descripcionEstado; }
	public function setSubtipo( $subtipo ){ $this->subtipo = $subtipo; }
	public function setCodigoCuenta( $codigoCuenta ){ $this->codigoCuenta = $codigoCuenta; }
	public function setFechaArchivo( $fechaArchivo ){ $this->fechaArchivo = $fechaArchivo; }
	
	public function setSaldoDisponibleDB( $saldoDisponibleDB ){$this->saldoDisponibleDB = $saldoDisponibleDB;}
	
	public function guardar(){
		$query = "INSERT INTO aportes164 (
					id_proceso
					, id_etapa_proceso
					, id_archivo
					, error
					, estado
					, usuario
										
					, bin
					, nit
					, numero_tarjeta
					, miembro
					, nombre_titular
					, saldo_disponible
					, estado_saldo
					, descripcion_estado
					, subtipo
					, codigo_cuenta
					, fecha_archivo
					
					, saldo_disponible_db
										
				) VALUES(
					$this->idProceso
					, $this->idEtapaProceso
					, $this->idArchivo
					, '$this->error'
					, '$this->estado'
					, '$this->usuario'
				
					, '$this->bin'
					, '$this->nit'
					, '$this->numeroTarjeta'
					, '$this->miembro'
					, '$this->nombreTitular'
					, $this->saldoDisponible
					, '$this->estadoSaldo'
					, '$this->descripcionEstado'
					, '$this->subtipo'
					, '$this->codigoCuenta'
					, '$this->fechaArchivo'
					
					, $this->saldoDisponibleDB
					
				)";
		$resultado = (self::$con->queryInsert($query,"aportes164")) === null ? 0 : 1;
		return $resultado;
	}

	/**
	 *
	 * @return number [PROCESO:La etapa esta en proceso,TERMINO: La etapa termino, null: Error]
	 */
	public function procesar_datos(){
		$resultado = "";
		$sentencia = self::$con->conexionID->prepare ( "EXEC [asopagos].[sp_Maestro_Saldo]
				@id_proceso = $this->idProceso
				, @select_top = 500
				, @usuario = '$this->usuario'
				, @resultado = :resultado" );
		$sentencia->bindParam ( ":resultado", $resultado, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000 );
		$sentencia->execute ();

		$resultado = $resultado=='PROCESO' || $resultado=='FIN_EXITOSO'|| $resultado=='FIN_ERROR' ? $resultado: null;

		return $resultado;
	}
	
	/*public function estado_interfaze(){
		$estado = "PROCESO";
		$query = "-- [11][procesar_dato_sigas]
					-- [12][procesar_dato_service]
					SELECT count(*) AS contador , 'CONTADOR' AS bandera
					FROM aportes426
					WHERE id_proceso=$this->idProceso
						AND id_etapa_proceso=11
						AND ISNULL(estado,'') = 'PROCESADO'
						AND error='N'
					UNION
					SELECT COUNT(*) , 'ERROR' AS bandera
					FROM aportes426
					WHERE id_proceso=$this->idProceso
						AND id_etapa_proceso=12
						AND ISNULL(error,'')='S'
					ORDER BY bandera";
		$arrResultado = $this->fetchConsulta($query);
	
		if(intval($arrResultado[0]["contador"])==0){
			if(intval($arrResultado[1]["contador"])==0){
				$estado = "FIN_EXITOSO";
			}else{
				$estado = "FIN_ERROR";
			}
		}
		return $estado;
	}*/
	

	/**
	 * Metodo encargado de obtener el estado del proceso
	 *
	 * @return String [TERMINO: El proceso termino, PROCESO: El proceso no termino]
	 */
	/*public function fetch_datos_interfaz($topSelect){
	
		$query = "-- [4][procesar_dato_sigas]
					SELECT TOP $topSelect a426.*
						,a30.nombrearchivo
					FROM aportes426 a426
					LEFT JOIN aportes030 a30 ON a30.idarchivopu=a426.id_archivo
					WHERE id_proceso=$this->idProceso
						AND id_etapa_proceso=11
						AND ISNULL(estado,'') = 'PROCESADO'
						AND error='N'";
	
		$arrResultado = $this->fetchConsulta($query);
	
		return $arrResultado;
	}*/

	/*public function update_estado($idPlanoTesoreria,$error = 'N'){
		$query = "-- [12][procesar_dato_service]
					UPDATE aportes426 SET
						estado='PROCESADO', error='$error', id_etapa_proceso=12, fecha_modificacion=GETDATE()
					WHERE id_plano_tesoreria=$idPlanoTesoreria";
			
		return (self::$con->queryActualiza($query)>0)?1:0;
	}*/
	
	/**
	 * Retorna los valores obtenidos en la consulta
	 * @return multitype:array
	 */
	/*private function fetchConsulta($querySql){
		$resultado = array();
		$rs = self::$con->querySimple($querySql);
		while($row = $rs->fetch())
			$resultado[] = array_map("trim",$row);
		//$resultado[] = array_map("utf8_encode",$row);
		return $resultado;
	}*/
	
}