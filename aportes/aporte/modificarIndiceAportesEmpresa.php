<?php
set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
$usuario=$_SESSION['USUARIO'];
include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'p.definiciones.class.php';
$objClase=new Definiciones();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Modificar Indice de Aportes</title>
<link type="text/css" href="../../css/Estilos.css" rel="stylesheet"/>
<link type="text/css" href="../../css/marco.css" rel="stylesheet"/>
<link type="text/css" rel="stylesheet" href="../../css/css.1.8/jquery-ui-1.8.17.custom.css" />
<script language="javascript" src="../../js/1.6/jquery-1.6.2.min.js"></script>
<script language="javascript" src="../../js/1.6/jquery-ui-1.8.16.custom.min.js"></script>
<script language="javascript" src="../../js/comunes.js"></script>
<script language="javascript" src="js/modificarIndiceAportesEmpresa.js"></script>
<style type="text/css">
.classCampoCorto {background: none repeat scroll 0 0 #F7F7F7; border-style: hidden; border-width: 0; font-size: 11px; padding: 0;} 
</style>
</head>

<body>
<center>
<br /><br />
<table width="990" border="0" cellspacing="0" cellpadding="0">
<tr>
<td class="arriba_iz" >&nbsp;</td>
<td class="arriba_ce" ><span class="letrablanca">::Modificar Indice de Aportes Empresas ley 1429::</span></td>
<td class="arriba_de" >&nbsp;</td>
</tr>
<tr>
<td class="cuerpo_iz">&nbsp;</td>
<td class="cuerpo_ce">

<img src="../../imagenes/spacer.gif" alt="" width="2" height="1" /></td>
<td class="cuerpo_de">&nbsp;</td>
</tr>
<tr>
<td class="cuerpo_iz" >&nbsp;</td>
<td class="cuerpo_ce" >
	<table border="0" class="tablero" cellspacing="0" width="100%" >
	  <tr>
	    <td width="100" style="text-align:right;">Nit:<input type="hidden" name="hidIdEmpresa" id="hidIdEmpresa"/></td>
	    <td ><input type="text" name="txtNit" id="txtNit" />&nbsp; <label id="lblRazonSocial" >&nbsp;</label></td>
	  </tr>
	  <tr>
	    <td style="text-align:right;">Periodo:</td>
	    <td colspan="5" style="display:none;"	id="tdPeriodo"><input  type="text" readonly="readonly"size="10" name="txtPeriodo" id="txtPeriodo"/> &nbsp;&nbsp;<input type="button" value="Actualizar" id="btnBuscarAportes" class="ui-state-default"/></td>
	  </tr>
	  <tr>
	      <td colspan="6" id="tdMensajeError" style="color:#FF0000"></td>
	  </tr>
	 <!--<tr>
	      <td colspan="6">Trabajadores con Indice diferentes</td>
	  </tr>-->
	  </table>
	<input type="hidden" id="txtusuario" name="txtusuario" value="<?php echo $usuario; ?>" /> 
</td>
<td class="cuerpo_de" >&nbsp;</td>
</tr>
<tr>
<td class="cuerpo_iz" >&nbsp;</td>
<td class="cuerpo_ce" >&nbsp;</td>
<td class="cuerpo_de" >&nbsp;</td>
</tr>
<tr>
<td class="abajo_iz" >&nbsp;</td>
<td class="abajo_ce" ></td>
<td class="abajo_de" >&nbsp;</td>
</tr>
</table>
</center>

<!-- FORMULARIO OBSERVACIONES-->
<div name="div-observaciones-tab" style="display:none" title="Observaciones empresas ley 1429">
<table class="tablero">
 <tr>
   <td>Usuario</td>
   <td colspan="3" >
   <input name="usuarioObs" id="usuarioObs" class="box1" disabled="disabled" value="<?php echo $_SESSION['USUARIO']?>" /></td>
   </tr>
 <tr>
   <td>Observaciones</td>
   <td colspan="3" >
   <textarea name="observacionGrupo" id="observacionGrupo" cols="45" rows="5" class="boxlargo"></textarea></td>
   </tr>
</table>
<div class="ui-state-highlight ui-corner-all" style="margin: 9px auto; display:none;padding: 6px; text-align: center; border: 1px solid rgb(255, 255, 0); width:300px" id="rtaObservacion"><span class="ui-icon ui-icon-info" style="float: left; margin-right: 0.3em;"></span>Observaci&oacute;n guardada con &eacute;xito.</div>
</div>

</body>
</html>
