<?php
// @autor:		Orlando Puentes A
// @fecha:		mayo 31/2010
// @formulario:	guardar datos persona - modulo empresa
set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz . DIRECTORY_SEPARATOR .'config.php'; 
include_once $raiz . DIRECTORY_SEPARATOR .'funcionesComunes'.DIRECTORY_SEPARATOR.'funcionesComunes.php';
$anno=date('Y');
$mes=date('m');
$dia=date('d');
?>
<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es">
<head> 
 <title>Procesar archivos</title>
 <link href="../../css/Estilos.css" rel="stylesheet" type="text/css">
 <script language="javascript" src="../../js/jquery-1.4.2.js"></script>
 <script language="javascript" src="js/subirPlanobanco.js"></script>
 </head>
 <body>
 <br />
 <p align="center" class="Rojo">Resultado de la copia de archivos</p>
 <br />
<?php 
$dir1 = $ruta_cargados.'tesoreria'. DIRECTORY_SEPARATOR .'cargados'. DIRECTORY_SEPARATOR;
$ok=verificaDirectorio($dir1);
if($ok==0){
	echo 2;		//no se pudo crear el directorio destino
	exit();
} 
$dir2=$ruta_cargados.'tesoreria'. DIRECTORY_SEPARATOR. 'procesados'. DIRECTORY_SEPARATOR. $anno. DIRECTORY_SEPARATOR. $mes. DIRECTORY_SEPARATOR. $dia. DIRECTORY_SEPARATOR;
$ok=verificaDirectorio($dir2);
if($ok==0){
	echo 2;		//no se pudo crear el directorio copia
	exit();
}

$directorio=$dir1;
$_SESSION['DIRECTORIO']=$dir2;
echo "<br><br><br><br>";
if(isset($_FILES['archivo'])){
	foreach ($_FILES['archivo']['error'] as $key => $error) {
	   if ($error == UPLOAD_ERR_OK) {
		   $archivos[]= $_FILES["archivo"]["name"][$key];
		   move_uploaded_file($_FILES["archivo"]["tmp_name"][$key],$directorio.$_FILES["archivo"]["name"][$key]) 
		   or die("Ocurrio un problema al intentar subir el archivo.");
	   }
	   else{
	   		echo "<center><p class=Rojo>Error: $error</p></center>";
	   		exit();
	   }
	}
 }
 else{
 	echo "<center><p class=Rojo>No se pudo cargar el archivo!</p></center>";
 	exit();
 }
 
for($i=0; $i<count($archivos); $i++){
	
	$total = count(glob($directorio."{*}",GLOB_BRACE));
	if ($total>1){echo "<script language='JavaScript'>alert('Existe mas de un archivo para procesar por favor comunicarse con T.I');</script>"; exit();}
	
	echo "<p align=center id='msjCopia' style='display:none'>Archivo copiado en el servidor: <strong>".$archivos[$i]."</strong></p>"; 
}
if($i>0){
?>
<div id="listaError" style="display:none" align="center">
<small><strong>Datos no procesados</strong></small><br />
<table width="50%" border="0" cellspacing="0" class="tablero" id="tbErrores" align="center">
	<thead>
		<tr>
			<th>Error</th>
			<th>Planilla</th>
			<th>Nit</th>
			<th>Valor Pago</th>
			<th>Periodo</th>		
		</tr>
	</thead>
	<tbody>
	
	</tbody>
	</table> 
	</div>
<center>
    <div id="boton">    
    <p style="text-decoration:none; font-size:12px; color:#F00; font-weight:bold; cursor:pointer" onclick="procesar();">Procesar estos archivos</p>
</div>
</center>
<?php
	}
 ?> 
 <div id="ajax" align="center"> </div>
<div id="mensajes" align="center">..:..</div>
 </body>
 </html>
 
