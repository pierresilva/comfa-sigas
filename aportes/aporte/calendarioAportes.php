<?php
	set_time_limit(0);
	ini_set("display_errors",'1');
	date_default_timezone_set("America/Bogota");
	$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
	include_once  $root;
	include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';
	
	include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
	include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
	
	$db = IFXDbManejador::conectarDB();
	if($db->conexionID==null){
		$cadena = $db->error;
		echo msg_error($cadena);
		exit();
	}
	$usuario=$_SESSION['USUARIO'];
	$fecver = date('Ymd h:i:s A',filectime('calendarioAportes.php'));
?>

<html >
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<title>Calendario de Aportes</title>
		
		<link type="text/css" href="<?php echo URL_PORTAL; ?>newcss/Estilos.css" rel="stylesheet">
		<link type="text/css" href="<?php echo URL_PORTAL; ?>newcss/css.1.8/jquery-ui-1.8.17.custom.css" rel="stylesheet">
		<link type="text/css" href="<?php echo URL_PORTAL; ?>newcss/marco.css" rel="stylesheet">
		
		<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/1.6/jquery-1.6.2.min.js"></script>
		<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/1.6/jquery-ui-1.8.16.custom.min.js"></script>
		<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/comunes.js"></script>
		<script type="text/javascript" src="<?php echo URL_PORTAL; ?>aportes/aporte/js/calendarioAportes.js"></script>
	</head>
	<body>
		<table width="60%" border="0" cellspacing="0" cellpadding="0" style="margin: 20px auto 0 auto;">
			<tr>
				<td class="arriba_iz" >&nbsp;</td>
				<td class="arriba_ce" ><span class="letrablanca">::Calendario del pago de portes::</span>
					<div style="text-align:right;float:right;">
						<?php echo 'Versi&oacute;n: ' . $fecver; ?>
						<input type="hidden" name="hidUsuario" id="hidUsuario" value="<?php echo $_SESSION["USUARIO"];?>"/>
					</div>
				</td>
				<td class="arriba_de" >&nbsp;</td>
			</tr>
			<tr>
				<td class="cuerpo_iz">&nbsp;</td>
				<td class="cuerpo_ce"><img src="../../imagenes/spacer.gif" alt="" width="2" height="1" /></td>
				<td class="cuerpo_de">&nbsp;</td>
			</tr>
			<tr>
				<td class="cuerpo_iz" >&nbsp;</td>
				<td class="cuerpo_ce" >								
					<table border="0" class="tablero" cellspacing="0" width="95%" align="center">									
						<tbody>
							<tr>
								<td width="20%">Periodo:</td>
								<td colspan="3">
									<input type="text" name="txtPeriodo" id="txtPeriodo" class="box1 element-required"/>
									<input type="hidden" name="hidIdFechaVencimiento" id="hidIdFechaVencimiento"/>
								</td>
							</tr>
							<tr>
								<td>Clase aportante:</td>
								<td colspan="3">
									<select name="cmbClaseAportante" id="cmbClaseAportante" class="boxlargo element-required">
										<option value="0" selected="selected">Seleccione</option>
							      		<?php
							      			$rs = $db->Definiciones ( 33, 1);										 	
											while ( $row = $rs->fetch() ) {
												if($row ['iddetalledef']==2653 || $row ['iddetalledef']==2654 || $row ['iddetalledef']==2655 )
												echo "<option value=" . $row ['iddetalledef'] . ">" . $row ['detalledefinicion'] . "</option>";
											} 
										?>
									</select>	
								</td>
							</tr>
							<tr>
								<td>
									Fecha Inicial
								</td>
								<td colspan="">
									<input type='text' name='txtFechaInicial' id='txtFechaInicial' readonly="readonly" class="box1 element-required"/>
								</td>
								<td>
									Fecha Final
								</td>
								<td colspan="">
									<input type='text' name='txtFechaFinal' id='txtFechaFinal' readonly="readonly" class="box1 element-required"/>
								</td>
							</tr>
							<tr>
								<td colspan="4" id="tdMensaje"></td>
							</tr>
						</tbody>
						<tfoot>
							<tr>
								<td colspan="9" style="text-align: center;">
									<input type="button" name="btnGuardar" id="btnGuardar" value="Guardar"/>
									<input type="button" name="btnActualizar" id="btnActualizar" value="Actualizar" style="display:none;"/>
								</td>
							</tr>
						</tfoot>
			  		</table>
				</td>
				<td class="cuerpo_de" >&nbsp;</td>
			</tr>
			<tr>
				<td class="cuerpo_iz" >&nbsp;</td>
				<td class="cuerpo_ce" >&nbsp;</td>
				<td class="cuerpo_de" >&nbsp;</td>
			</tr>
			<tr>
				<td class="abajo_iz" >&nbsp;</td>
				<td class="abajo_ce" ></td>
				<td class="abajo_de" >&nbsp;</td>
			</tr>
		</table>
	</body>
</html>
