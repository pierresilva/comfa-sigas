<?php
	date_default_timezone_set('America/Bogota');
	$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
	include_once  $root;
	$fecver = date('Ymd h:i:s A',filectime('generarMorosas.php'));
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<title>Generar Morosas</title>
		<link type="text/css" href="../../css/Estilos.css" rel="stylesheet"/>
		<link type="text/css" href="../../css/marco.css" rel="stylesheet"/>
		<link type="text/css" rel="stylesheet" href="../../css/css.1.8/jquery-ui-1.8.17.custom.css" />
		<script language="javascript" src="../../js/1.6/jquery-1.6.2.min.js"></script>
		<script language="javascript" src="../../js/1.6/jquery-ui-1.8.16.custom.min.js"></script>
		<script language="javascript" src="../../js/comunes.js"></script>
		<script language="javascript" src="js/generarMorosas.js"></script>
		<style type="text/css">
			.classCampoCorto {background: none repeat scroll 0 0 #F7F7F7; border-style: hidden; border-width: 0; font-size: 11px; padding: 0;}
			.classTextError {color:#ff0000} 
		</style>
		<script language="javascript">
			function img(){
				
			}
		</script>
	</head>
	<body>
		<center>
			<br /><br />
			<table width="60%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="arriba_iz" >&nbsp;</td>
					<td class="arriba_ce" ><span class="letrablanca">::Generar Morosas::</span>
						<div style="text-align:right;float:right;">
							<?php echo 'Versi&oacute;n: ' . $fecver; ?>
						</div>
					</td>
					<td class="arriba_de" >&nbsp;</td>
				</tr>
				<tr>
					<td class="cuerpo_iz">&nbsp;</td>
					<td class="cuerpo_ce"><img src="../../imagenes/spacer.gif" alt="" width="2" height="1" /></td>
					<td class="cuerpo_de">&nbsp;</td>
				</tr>
				<tr>
					<td class="cuerpo_iz" >&nbsp;</td>
					<td class="cuerpo_ce" >
						<table border="0" class="tablero" cellspacing="0" width="100%" >
							<tr>
								<td style="text-align:center;">&nbsp;</td>
							</tr>
							<tr>
						    	<td style="text-align:center;">
						    		<b>Generar empresas morosas</b>	
						    	</td>
						    </tr>
						    <tr>
						    	<td width="100" style="text-align:center;">
						    			<input type="button" name="btnGenerar" id="btnGenerar" value="Generar"/>
						    	</td>
						  	</tr>
					  	</table>
					</td>
					<td class="cuerpo_de" >&nbsp;</td>
				</tr>
				<tr>
					<td class="cuerpo_iz" >&nbsp;</td>
					<td class="cuerpo_ce" >&nbsp;</td>
					<td class="cuerpo_de" >&nbsp;</td>
				</tr>
				<tr>
					<td class="abajo_iz" >&nbsp;</td>
					<td class="abajo_ce" ></td>
					<td class="abajo_de" >&nbsp;</td>
				</tr>
			</table>
		</center>
	</body>
</html>