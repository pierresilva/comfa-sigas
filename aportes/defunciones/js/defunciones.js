/*
* @autor:      Ing. Orlando Puentes
* @fecha:      Agosto 25 de 2010
* objetivo:
*/

// JavaScript Document
var URL = src();
var idPersona=0;

var arrayB = [];


var diasMaxFechaDefuncion = 1080;
$(document).ready(function(){
	$("#fechaDefuncion").datepicker({
		changeMonth: true,
		changeYear: true,
		maxDate: "+0D",
		minDate: "-"+ (diasMaxFechaDefuncion/30) +"M",
		onClose:function(dateText,elm){
			if(comparaFecha())
				verificarCuotaMonetaria($(this).val());
		}
	});
});
$(function() {
	$("#dialog-form2").dialog("destroy");
	$("#dialog-form2").dialog({
		autoOpen: false,
		height: 400,
		width: 500,
		modal: true,
		buttons: {
			'Enviar': function() {
			var bValid = true;
			var campo=$('#notas').val();
			var campo0=$.trim(campo);
			if (campo0==""){
				$(this).dialog('close');
					return false;
				}
			var campo1=$('#usuario').val();
			var campo2="radicacion.php";
			$.post(URL+'phpComunes/colaboracion.php',{v0:campo0,v1:campo1,v2:campo2},
			function(datos){
				if(datos=='1'){
					alert("Su comentario fue enviado correctamente!. Gracias por participar en el mejoramiento de SIGAS");
				}else{
					alert(datos);
				}
		});
		$(this).dialog('close');
		},
		Cancelar: function() {
			$(this).dialog('close');
			$("#dialog-form2").dialog("destroy");
		}
	}
	});
	/*
	$('#enviar-notas')
		.button()
		.click(function() {
		$('#dialog-form2').dialog('open');
	});*/
	
});

$(function() {
		$("#ayuda").dialog({
		 	autoOpen: false,
			height: 450,
			width: 600,
			draggable:true,
			modal:false,
			open: function(evt, ui){
					$('#ayuda').html('');
					$.get('../../help/aportes/ayudaRadicacion.html',function(data){
							$('#ayuda').html(data);
					});
			 }
		});
	});

function nuevoR(){
	nuevo=1;
	$("#bGuardar").show();
	limpiarCampos();
	$("#errores").empty();
    $('#pendientes option').remove();
	$.post(URL+'phpComunes/buscarRadicaTipoID.php',{v0:183},function(datos){
    	var cmbRadicacion = $("#pendientes");
	   	$(cmbRadicacion).html(datos);
	   	if($(cmbRadicacion).children().length==1){
			alert('Lo lamento NO hay radicaciones pendientes de grabar!');
			return false;
         }
	});
}

//comparar las fecha defuncion con la fecha de la afiliacion
function comparaFecha(){
	var estadoAfiliacion = $("#estado").text().trim();
	if(estadoAfiliacion == 'I'){
		var fechaRetiro = $("#fretiro").text().trim();
		var fechaDefuncion = $("#fechaDefuncion").val().trim();
		
		if(fechaDefuncion!="" && fechaRetiro!=""){
			fechaRetiro = new Date(fechaRetiro);
			fechaDefuncion = new Date(fechaDefuncion);
			if(fechaDefuncion>fechaRetiro){
				alert("Fecha Retiro es menor a la fecha Defuncion");
				$("#fechaDefuncion").val('');
				return false;
			}
		}
	}
	return true;
		
}

function buscarRadicacion(){
	$("#tdTercero").html("");
	$("#beneficiarios tr:not(:first)").remove();
	$("#tipoPago,#hdnBeneficiarios,#hdnIdTercero").val(0);
	$("#fechaDefuncion").val("");
	$("#trOtro,#trNombreOtro").hide();
	var idRadicacion = $('#pendientes').val();
	if(idRadicacion==0){
		nuevoR();
		return 
	}
	$("#fecha").val($("#pendientes option:selected").attr("id") );
	//peticion para traer los datos de la radicacion defuncion
	$.ajax({
		url:"defunciones.log.php",
		type:"POST",
		data:{accion:"CONSULTAR",v0:idRadicacion},
		dataType:"json",
		async:false,
		success:function(datos){
			//los datos en la logica estan listos para ser distribuidos en los diferentes campos
			if(datos.error==1){
				alert(datos.descripcion);
				return false;
			}
			
			//datos afiliado---------------
			$("#tipoI").val(datos.afiliado.tipoIdentificacion);
			$("#numero").val(datos.afiliado.numero);
			$("#nombreT").html(datos.afiliado.id+" - "+datos.afiliado.nombres);
			idPersona = datos.afiliado.id;
			//-----------------------------
			
			if(datos.error==2){
				alert(datos.descripcion);
				return false;
			}
			
			//datos afiliacion-------------
			$("#fafilia").html(datos.afiliado.fechaAfiliacion);
			$("#estado").html(datos.afiliado.estado);
			$("#salario").html(datos.afiliado.salario);
			$("#hdnIdEmpresa").val(datos.afiliado.idEmpresa);
			$("#fretiro").html(datos.afiliado.fechaRetiro);
			//-----------------------------
			
			//datos fallecido--------------
			$("#tipoFallecido").html(datos.fallecido.tipoFallecido);
			$("#txtFallecido").html(datos.fallecido.numero+" - "+datos.fallecido.nombres);
			$("#hdnIdParentesco").val(datos.fallecido.idTipoFallecido);
			$("#hdnFallecido").val(datos.fallecido.id);
			//-----------------------------
			
			//se quitara
			if(datos.fallecido.giro=="NO" && datos.fallecido.idTipoFallecido!=0){
				alert("El fallecido no tiene Giro el ultimo Periodo "+datos.fallecido.periodo);
				$("#fechaDefuncion").val('');
				return false;
			}//------------
			
			//datos cuota monetaria--------
			$("#hdnPeriodo").val(datos.fallecido.periodo);
			$("#hdnCuotaMonetaria").val(datos.fallecido.cuotaMonetaria);
			$("#textcuota").html(datos.fallecido.cuotaMonetaria);
			//-----------------------------
			
			if(datos.fallecido.idTipoFallecido!=0){
				//datos Tercero----------------------
				$("#tdTercero").html(datos.subsidio.numero+" - "+datos.subsidio.nombres);		
				$("#hdnIdTercero").val(datos.subsidio.id);
				//-----------------------------------
				
				//datos Beneficiario-----------------
				//$("#tdBeneficiario").html($("#txtFallecido").text());
				var bene = ($("#txtFallecido").text()).split('-');
				$("#beneficiarios").append("<tr><td align=left style='border:0px' ><input type=checkbox id=check"+0+" value="+datos.fallecido.id+" checked disabled /></td><td align=left style='border:0px' >"+bene[0]+"</td><td align=left style='border:0px' >"+bene[1]+"</td><td align=left style='border:0px' >"+$("#tipoFallecido").text()+"</td></tr>");
				$("#hdnBeneficiarios").val(datos.fallecido.id);
				//-----------------------------------
			}
		}
	});
}

function verificarCuotaMonetaria(fechaDefuncion){
	arrayB=[];
	if(fechaDefuncion=="")
		return false;
	
	var select = "<select name='cmbTercero' class='boxlargo' id='cmbTercero' onchange='buscarBeneficiario();'><option value='0'>Seleccione...</option>";
	var bandera = 0;
	var beneSinTercero = "";
	var beneSinGiro = 0;
	var periodoNoGiro="";
	var tipoFallecido=$("#hdnIdParentesco").val();
	var idAfiliado = idPersona;
	var fechaDefuncion = fechaDefuncion;
	var idFallecido = $("#hdnFallecido").val();
	var cont = 0;
	$.ajax({
		url:"defunciones.log.php",
		type:"POST",
		data:{accion:"BENE",tipoFallecido:tipoFallecido,idAfiliado:idAfiliado,fechaDefuncion:fechaDefuncion,idFallecido:idFallecido},
		dataType:"json",
		async:false,
		success:function(datos){
			
			if(parseInt(tipoFallecido)==0){
				//El Fallecido es el Afiliado				
				$.each(datos,function(i,row){
					//beneficiario con Cuota Monetaria el ultimos periodos
					if(row.giro=='SI'){
						if(bandera!=row.idTercero){
							select +="<option value='"+row.idTercero+"'>"+row.numTercero+" - "+row.nomTercero+"</option>";
							bandera=row.idTercero;						
						}
						
						var arrayRow = new Array(row.id,row.numero,row.nombres,row.idTercero,row.parentesco);
						arrayB.push(arrayRow);
						cont++;
					}else if(row.giro=='NO'){
						beneSinGiro++;
						periodoNoGiro=row.periodo;
					}										
				});
				if(cont>0){
					select +="</select>";
					//datos Tercero---------------------
					$("#tdTercero").html(select);
					//----------------------------------
				} else {					
					if(beneSinGiro!=""){
						alert('No hay beneficiarios con giro en el periodo ' + periodoNoGiro);
						$("#fechaDefuncion").val('');
					} else {
						alert('No hay beneficiarios');
						$("#fechaDefuncion").val('');
					}
				}
			}else if(datos.giro=="NO"){
				//El Fallecido es el Beneficiario
				alert("No hay Cuota Monetaria");
				$("#fechaDefuncion").val('');
			}
		}
	});
	
}

/**
 * 
 * @param id           id div 
 * @param contenido    contenido del mensaje
 */
function dialogMensaje(id,contenido){
	$("#"+id).html(contenido);
	$("#"+id).dialog({
		modal:true,
		width:500,
		height:400,
		title:"Sigas | Mensaje",
		buttons:{
			Aceptar:function(){
				$(this).dialog("close");
				$(this).dialog("destroy");
			}
		}
	});
}

function buscarBeneficiario(){
	var idTercero = $("#cmbTercero").val();
	$("#beneficiarios tr:not(:first)").remove();
	$("#hdnIdTercero").val('');
	$("#txtIdentificacion").val('').trigger('onblur');
	$("#hdnBeneficiarios").val("");
	$("#trOtro,#trNombreOtro").hide();
	if(idTercero==0)return false;	
		
	var contador = 0;
	var atributos="";
	if(idTercero>0) { atributos = "checked "; }
	else { $("#trOtro,#trNombreOtro").show(); $('#cmbIdTipoDocumento').val('1'); }
	
	$.each(arrayB,function(i,row){
		if(idTercero==row[3]){			
			$("#beneficiarios").append("<tr><td align=left style='border:0px' ><input type=checkbox id=check"+contador+" value="+row[0]+" "+atributos+" /></td><td align=left style='border:0px' >"+row[1]+"</td><td align=left style='border:0px' >"+row[2]+"</td><td align=left style='border:0px' >"+row[4]+"</td></tr>");
			contador++;
		}
	});
	if(contador>0){
		$("#hdnIdTercero").val(idTercero);		
	}else{
		alert("No hay beneficiarios");		
	}
}

function buscarTercero(){
	$('#tdNombreOtro').html('');
	var tipDoc=$("#cmbIdTipoDocumento").val();
	var numero=parseInt($("#txtIdentificacion").val());
	
	if(isNaN(numero)){ $("#txtIdentificacion").val(''); return false; }
		
	$.ajax({
		url: URL+'phpComunes/buscarPersona2.php',
		type:"POST",
		data:{v0:tipDoc,v1:numero},
		dataType:"json",
		async:false,
		success:function(data){
			if(data){
				$.each(data,function(i,fila){					
					var nombre=$.trim(fila.pnombre)+' '+$.trim(fila.snombre)+' '+$.trim(fila.papellido)+' '+$.trim(fila.sapellido);
					$("#hdnIdTercero").val(fila.idpersona);
					$('#tdNombreOtro').html(nombre);
				});
			} else {
				alert("El tercero no existe en el sistema.\n Sera creado.");
				newPersonaSimple($('#cmbIdTipoDocumento'),$('#txtIdentificacion'));
				$('#txtIdentificacion').val('');
			}
		}
	});
}

function guardarRegistro(){
	$("#bGuardar").hide();
	var idRadicacion = $("#pendientes").val();
	var idTrabajador = idPersona;
	var idFallecido = $("#hdnFallecido").val();
	var fechaDefuncion = $("#fechaDefuncion").val();
	var formaPago = $("#tipoPago").val();
	var idTercero = $("#hdnIdTercero").val();
	var idParentesco = $("#hdnIdParentesco").val();
	var cuotas = $("#numCuotas").val();
	var fechaRadicacion = $("#fecha").val();
	var idBeneficiarios = "";
	var idEmpresa = $("#hdnIdEmpresa").val();
	var cuotaMonetaria = $("#hdnCuotaMonetaria").val();
	var periodo = $("#hdnPeriodo").val();
	/////////beneficiarios
	var contador=0;
	$("#beneficiarios tr:not(':first')").each(function(index){
	 if($("#check"+index).is(':checked')==true){
		 idBeneficiarios += $("#check"+index).val() +",";
		 contador++;
	 }
	});
	if(contador<=0){
		alert("No hay beneficiarios seleccionados");
		$("#bGuardar").show();
		return false;
	} else {
		//peticion para guardar el registro de la defuncion
		$.ajax({
			url:"guardarRegistros.php",
			type:"POST",
			data:{v1:idRadicacion,v2:idTrabajador,v3:idFallecido,v4:fechaDefuncion,v5:formaPago,v6:idTercero,v7:idParentesco,v8:cuotas,v9:fechaRadicacion,v10:idBeneficiarios,v11:idEmpresa,v12:cuotaMonetaria,v13:periodo},
			dataType:"json",
			async:false,
			success:function(datos){
				if(datos.error==1){
					alert(datos.descripcion);
					return false;
				}
				alert("La defuncion se guardo correctamente");
				cerrarRadicacion(parseInt($("#pendientes").val()));
				
				if(datos.error==2){
					alert(datos.descripcion);
					return false;
				}
				alert("El proceso termino con exito");
				nuevoR();
			}
		});
		//
	}
}

function limpiarCampos(){
	$("#trOtro,#trNombreOtro").hide();
	$("table.tablero input:text").val("");
	$(":hidden").val("");
	$("table.tablero select").val(0);
	$("#pendientes").focus();
	$('#numCuotas').val(12);
	$("#tipoFallecido, #txtFallecido, #tdTercero, #salario, #estado, #fafilia, #nombreT,#fretiro,#textcuota").html("");
	$("#beneficiarios tr:not(:first)").remove();
	$('#cmbIdTipoDocumento,#tDocumento').html($('#tipoI').html());	
	arrayB=[];
}

function validarCampos(x){
	$("#bGuardar").hide();

	var error=0;
	$(".tablero input.ui-state-error,.tablero select.ui-state-error").removeClass("ui-state-error");
	if($("#comboFallecido").val()==0){
		$("#comboFallecido").addClass("ui-state-error");
		error++;
	}
		
	if($("#fechaDefuncion").val()==''){
		$("#fechaDefuncion").addClass("ui-state-error");
		error++;
	}
	
	if($("#tipoPago").val()==0){
		$("#tipoPago").addClass("ui-state-error");
		error++;
	}
	var numc=parseInt($("#numCuotas").val());
	
	if(numc > 12){
		$("#numCuotas").addClass("ui-state-error");
		error++;
		alert("El numero de cuotas no puede ser mayor a 12!");
		}
	
	if($("#hdnFallecido").val()==0 || $("#hdnIdTercero").val()=='' || $("#hdnIdTercero").val()<=0 || $("#hdnIdParentesco").val()=="" || $("#hdnIdEmpresa").val()==0 || $("#hdnCuotaMonetaria").val()==0 || $("#hdnPeriodo").val()==0){
		alert("Los Datos No se cargaron completamente, favor realizar el proceso de nuevo");
		error++;
	}
	if(error>0){
		$("#bGuardar").show();
		return false;
	}
	
	guardarRegistro();
}	

/**
 * 
 * @param idradicacion    radicacion a cancelar
 * @returns {Boolean}
 */

function cerrarRadicacion(idradicacion){
	$.ajax({
		url: URL+'phpComunes/cerrarRadicacion.php',
		type: "POST",
		data: "submit=&v0="+idradicacion,
		success: function(datos){
			limpiarCampos();
			alert(datos);
		}
	});
	return false;
}

function newPersonaSimple(cmbtd,txtd){
	var tipodoc=cmbtd.val();
	var doc =txtd.val();
	
	$("#dialog-persona").dialog({
		height: 228,
		width: 650,
		draggable:false,
		modal: true,
		open: function(){			
			actDatepicker($("#fechaNaceDialogPersona"));			
			$('#dialog-persona select,#dialog-persona input[type=text]').val('');
			$("#tDocumento").val(tipodoc);
			$("#txtNumeroP").val(doc);
		},
		buttons: {
			'Guardar datos': function() {
				var error=0;
				var tipodocumento=$("#tDocumento").val();
				var documento=$.trim($("#txtNumeroP").val());
				var prNombre=$.trim($("#spNombre").val());
				var seNombre=$.trim($("#ssNombre").val());
				var prApellido=$.trim($("#spApellido").val());
				var seApellido=$.trim($("#ssApellido").val());
				var noCorto=nombreCorto(prNombre, seNombre, prApellido, seApellido);
				var fechaNace = $("#fechaNaceDialogPersona").val();
				
				$("table.tablero :input.ui-state-error,select.ui-state-error").removeClass("ui-state-error");
				
				if(documento.length==0){
					$("#txtNumeroP").addClass("ui-state-error");
					error++;
				}
				if(prNombre.length==0){
					$("#spNombre").addClass("ui-state-error");
					error++;
				}
				if(prApellido.length==0){
					$("#spApellido").addClass("ui-state-error");
					error++;
				}
				var regexpFechas = /^((\d{4}))[\/|\-](|(0[1-9])|(1[0-2]))[\/|\-]((0[1-9])|(1\d)|(2\d)|(3[0-1]))$/;
				if(!fechaNace.match(regexpFechas)){
					$("#fechaNaceDialogPersona").addClass("ui-state-error");
					error++;
				}
				
				if(error>0){
					alert("Llene los campos obligatorios!");
					return false;
				}
				
				$.ajax({
					url: URL+'phpComunes/pdo.insert.persona.php',
					type: "POST",
					data: {v0:tipodocumento,v1:documento,v2:prApellido,v3:seApellido,v4:prNombre,v5:seNombre,v19:noCorto,v8:fechaNace},
					async:false,
					success: function(datos){
						var e = parseInt(datos);
						if(e>0){
							cmbtd.val(tipodocumento);
							var doc =txtd.val(documento);							
							$('#dialog-persona select,#dialog-persona input[type=text]').val('');
							alert("La persona fue creada satisfactoriamente. (ID."+ datos +")");							
							$("#dialog-persona").dialog('close');							
						}else{
							alert("NO se guardo el registro!");
							txtd.val('');
						}
					}
				});
			}
		},
		close: function() {
			$('#dialog-persona select,#dialog-persona input[type=text]').val('');
			txtd.focus();
			$(this).dialog("destroy");
		}
	});
}

function nombreCorto(pn,sn,pa,sa){
	var patron=new RegExp('[\u00F1|\u00D1]');
	for(i=1;i<=pn.length;i++){
		pn=pn.replace(patron,"&");
	}
	for(i=1;i<=sn.length;i++){
		sn=sn.replace(patron,"&");
	}
	for(i=1;i<=pa.length;i++){
		pa=pa.replace(patron,"&");
	}
	for(i=1;i<=sa.length;i++){
		sa=sa.replace(patron,"&");
	}
	txt=pn+" "+sn+" "+pa+" "+sa;
	txt=txt.toUpperCase();
	num=parseInt(txt.length);
	if(num>26){
		if(sn.length>0){
			sn=sn.slice(0,1);
			txt=pn+" "+sn+" "+pa+" "+sa;
			txt=txt.toUpperCase();
			num=parseInt(txt.length);
		}
		if(num>26)
		if(sa.length>0){
			sa=sa.slice(0,1);
			txt=pn+" "+sn+" "+pa+" "+sa;
			txt=txt.toUpperCase();
			num=parseInt(txt.length);
			}
		}
	if(num>26){
		txt=txt.substring(0, 26);
	}
	$("#tncorto").val(txt);
	return txt;
}

function actDatepicker(objeto){
	objeto.datepicker({
		changeMonth: true,
		changeYear: true,
		maxDate: "+0D"
	});
}