<?php 
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';


/**
 * 
 * @param int          $idBeneficiario benficiario del periodo
 * @param String       $periodo        periodo del giro
 * @param int          $idTrabajador   afiliado
 * @param unknown_type $db             conexion bd
 */

function giroPorPeriodo($idBeneficiario,$periodo,$idTrabajador,$db){
	$ultimaCuota = 0;
	//berificar si el beneficiario los dos ultimos periodos recibio subsidio---
	$sqlCuota9 = "SELECT count(*)cantidad FROM aportes009 WHERE /*procesado='S' and*/ idbeneficiario=$idBeneficiario and periodo ='$periodo' and idtrabajador=".$idTrabajador;
	$rsCuota9 = $db->querySimple($sqlCuota9);
	$rowGiro9 = $rsCuota9->fetchObject();
	$ultimaCuota = $rowGiro9->cantidad;
	if($ultimaCuota ==0){
		$sqlCuota14 = "SELECT count(*)cantidad FROM aportes014 WHERE /*procesado='S' and*/ idbeneficiario=$idBeneficiario and periodo ='$periodo' and idtrabajador=".$idTrabajador;
		$rsCuota14 =  $db->querySimple($sqlCuota14);
		$rowGiro14 = $rsCuota14->fetchObject();
		$ultimaCuota = $rowGiro14->cantidad;
	}
	if($ultimaCuota == 0){
		
		// "SELECT count(*)cantidad FROM aportes099 WHERE idbeneficiario= and periodo ='' and idtrabajador=".;
		$sqlCuota14 ="SELECT count(*)cantidad FROM aportes099 WHERE idbeneficiario=$idBeneficiario  and periodo ='$periodo' and idpersona=$idTrabajador AND flag1='S'";
		$rsCuota14 =  $db->querySimple($sqlCuota14);
		$rowGiro14 = $rsCuota14->fetchObject();
		$ultimaCuota = $rowGiro14->cantidad;
	}
		
	return $ultimaCuota;
}
//funcion para manipular el periodo
function increPeriodo($periodo,$ban){
	$mes  = intval(substr($periodo, 4,2));
	$anno = intval(substr($periodo, 0,4));
	switch ($ban){
		case "S":
			if($mes==12){
				$anno = $anno+1;
				$mes  = 1;
			}else{
				$mes = $mes+1;
			}
			break;
		case "R":
			if($mes==1){
				$mes = 12;
				$anno = $anno-1;
			}else{
				$mes = $mes-1;
			}
			break;
	}

	$periodo = ($mes<10)?$anno."0".$mes:$anno."".$mes;
	return $periodo;
}
?>