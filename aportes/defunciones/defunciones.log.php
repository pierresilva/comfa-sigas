<?php
/** Autor: Oscar Uriel Rodriguez  Tovar
 *  formulario: L O G I C A  D E F U N C I O N E S
 * */
$raiz="";
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
require_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'p.persona.class.php';
include_once $raiz.DIRECTORY_SEPARATOR.'aportes'.DIRECTORY_SEPARATOR.'radicacion'.DIRECTORY_SEPARATOR.'clases'. DIRECTORY_SEPARATOR.'radicacion.class.php';
include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR . 'p.afiliacion.class.php';
include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR . 'grupo.familiar.class.php';
include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR . 'p.definiciones.class.php';
include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR . 'p.empresas.class.php';
include_once $raiz.DIRECTORY_SEPARATOR.'aportes'.DIRECTORY_SEPARATOR.'defunciones'.DIRECTORY_SEPARATOR.'funciones.php';

$objEmpresa = new Empresa;
$objDefinicion = new Definiciones;
$objGrupoFamiliar = new GrupoFamiliar;
$objAfiliacion = new Afiliacion;
$objPersona = new Persona;
$objRadicacion = new Radicacion;

$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}


switch($_REQUEST["accion"]){
	case "CONSULTAR":

		//array resultados
		$datos = array("afiliado"=>"","fallecido"=>"","subsidio"=>"","error"=>0,"descripcion"=>"");
		
		//datos de la cuota moneta�a
		$sql = "SELECT TOP 1 periodo, cuotamonetaria FROM aportes012 where procesado='S' ORDER BY periodo DESC";
		$rs5 = $db->querySimple($sql);
		$w5 = $rs5->fetchObject();
		$periodo = $w5->periodo;
		//$cuotaMonetaria = $w5->cuotamonetaria;
		//$arrayPeriodo[1] = increPeriodo($arrayPeriodo[0],"R");
		//-------------------------------------------------------------
		
		$idRadicacion = $_REQUEST['v0'];
		$rsRadicacion = $objRadicacion->buscarRadicacion($idRadicacion);
		if(($rowRadicacion = mssql_fetch_object($rsRadicacion))==true){
		
			$rsPersona    = $objPersona->mostrar_registro2($rowRadicacion->idtipodocumentoafiliado,$rowRadicacion->numero);
			if(($rowPersona = mssql_fetch_object($rsPersona))==true){
					
				//datos del afiliado-----------------------------------
				$datos["afiliado"]["tipoIdentificacion"] = $rowRadicacion->idtipodocumentoafiliado;
				$datos["afiliado"]["id"] = $rowPersona->idpersona;
				$datos["afiliado"]["numero"] = $rowPersona->identificacion;
				$datos["afiliado"]["nombres"] = utf8_encode($rowPersona->pnombre." ".$rowPersona->snombre." ".$rowPersona->papellido." ".$rowPersona->sapellido);
				//-----------------------------------------------------
				/**
				 * Buscar Afiliacion Primaria
				 */
				$contador = 0;
				$rsAfiliacion = $objAfiliacion->buscar_afiliacion_p_a($rowPersona->idpersona);
				$sql = "select  top 1 fecharetiro,a48.nit,a17.fechaingreso,'I' as estado, a17.salario from aportes017 a17 inner join aportes048 a48 on a48.idempresa=a17.idempresa
				 where idpersona=".$rowPersona->idpersona." order by a17.fecharetiro desc";
				//select aportes016.estado,aportes016.salario,aportes016.fechaingreso, aportes048.nit, aportes048.razonsocial, aportes016.tipoformulario, aportes091.detalledefinicion FROM aportes016  INNER JOIN aportes048 ON aportes016.idempresa=aportes048.idempresa LEFT JOIN aportes091 ON aportes091.iddetalledef=aportes016.tipoformulario WHERE idpersona=$idp and aportes016.estado='A' and primaria='S'
				$rsAfiliacionI = $db->querySimple($sql);
				if(($rowAfiliacion = mssql_fetch_object($rsAfiliacion))==true)
					$contador=1;
				else if(($rowAfiliacion = $rsAfiliacionI->fetchObject())==true)
					$contador=1;
				
				if($contador==1){
					//datos empresa en la cual labora el afiliado---------------
					$rsEmpresa = $objEmpresa->buscar_sucursales($rowAfiliacion->nit);
					$idEmpresa = ($rowEmpresa = mssql_fetch_object($rsEmpresa))?$rowEmpresa->idempresa:0;
					//----------------------------------------------------------
					
					//datos de la afiliacion------------------------------------
					$datos["afiliado"]["fechaAfiliacion"] = $rowAfiliacion->fechaingreso;
					$datos["afiliado"]["estado"] = $rowAfiliacion->estado;
					$datos["afiliado"]["salario"] = $rowAfiliacion->salario;
					$datos["afiliado"]["idEmpresa"] = $idEmpresa;
					$datos["afiliado"]["fechaRetiro"] = ($rowAfiliacion->estado=='I')?str_replace('-', '/', $rowAfiliacion->fecharetiro):"";
					//-----------------------------------------------------------
					
					$identificacionBeneficiario = 0;
					$rsBeneficiario = $objPersona->mostrar_persona_id($rowRadicacion->idbeneficiario);
					if(($rowBeneficiario = mssql_fetch_object($rsBeneficiario))==true){
						$identificacionBeneficiario = $rowBeneficiario->identificacion;
						$idBeneficiario = $rowBeneficiario->idpersona;
						//Datos Cuota Monetar�a------------------------------
						if ($datos["afiliado"]["id"] == $idBeneficiario)
						{		
								
						$sqla9 = "SELECT top 1 a9.valor FROM aportes009 a9 where a9.idtrabajador='".$datos["afiliado"]["id"]."'
							and a9.periodoproceso = (select max(b9.periodoproceso) from aportes009 b9 where b9.idtrabajador=a9.idtrabajador)";
							$rsa9 = $db->querySimple($sqla9);
							$wa9 = $rsa9->fetchObject();
							$cuotaMonetaria = $wa9->valor;
							
						}
							else
						{
							
						$sqla9 = "SELECT top 1 a9.valor FROM aportes009 a9 where a9.idtrabajador='".$datos["afiliado"]["id"]."' and a9.idbeneficiario=$idBeneficiario
							and a9.periodoproceso = (select max(b9.periodoproceso) from aportes009 b9 where b9.idtrabajador=a9.idtrabajador and b9.idbeneficiario=a9.idbeneficiario)";
							$rsa9 = $db->querySimple($sqla9);
							$wa9 = $rsa9->fetchObject();
							$cuotaMonetaria = $wa9->valor;
							
						}						
						
						$datos["fallecido"]["periodo"] = $periodo;
						$datos["fallecido"]["cuotaMonetaria"] = $cuotaMonetaria;
						//----------------------------------------------------
						
						if($rowPersona->idpersona!=$rowRadicacion->idbeneficiario){
							//Tipo Fallecido Beneficiario
							$rsGrupoFamiliar = $objGrupoFamiliar->buscar_beneficiario($identificacionBeneficiario , $rowPersona->idpersona);
							if(($rowGrupoFamiliar = mssql_fetch_object($rsGrupoFamiliar))==true){
								
								//datos del fallecido---------------------------------
								$rsParentesco = $objDefinicion->mostrar_registro($rowGrupoFamiliar->idparentesco);
								$datos["fallecido"]["tipoFallecido"]= ($rowParentesco = mssql_fetch_object($rsParentesco))? $rowParentesco->detalledefinicion:"No Existe";
								$datos["fallecido"]["idTipoFallecido"]=$rowGrupoFamiliar->idparentesco;
								$datos["fallecido"]["id"] = $rowGrupoFamiliar->idpersona;
								$datos["fallecido"]["numero"] = $rowGrupoFamiliar->identificacion;
								$datos["fallecido"]["nombres"] = utf8_encode($rowGrupoFamiliar->pnombre." ".$rowGrupoFamiliar->snombre." ".$rowGrupoFamiliar->papellido." ".$rowGrupoFamiliar->sapellido);
								//----------------------------------------------------
								
								//datos del beneficiario del subsidio-----------------
								$datos["subsidio"]["id"] = $datos["afiliado"]["id"];
								$datos["subsidio"]["numero"] = $datos["afiliado"]["numero"];
								$datos["subsidio"]["nombres"] = $datos["afiliado"]["nombres"];
								//----------------------------------------------------
								
							}else{
								//beneficiario no encontrado
								$datos["error"]=2;
								$datos["descripcion"] = "El beneficiario Fallecido no existe";
							}
						}else{
							//Tipo Fallecido afiliado
							
							//datos del fallecido---------------------------------
							$datos["fallecido"]["tipoFallecido"] = "Trabajador";
							$datos["fallecido"]["idTipoFallecido"] = 0;
							$datos["fallecido"]["id"] = $rowPersona->idpersona;
							$datos["fallecido"]["numero"] = $rowPersona->identificacion;
							$datos["fallecido"]["nombres"] = utf8_encode($rowPersona->pnombre." ".$rowPersona->snombre." ".$rowPersona->papellido." ".$rowPersona->sapellido);
							//----------------------------------------------------
							
						}
					}else{
						//el beneficiario no existe
						$datos["error"]=2;
						$datos["descripcion"] = "El beneficiario Fallecido no existe";
					}
				}else{
					//no hay Afiliacion primaria
					$datos["error"]=2;
					$datos["descripcion"] = "El trabajador no tiene una Afiliacion ";
				}
			}else{
				//no hay Afiliado
				$datos["error"]=1;
				$datos["descripcion"] = "El afiliado no existe";
			}
		}else{
			//no hay radicacion
			$datos["error"]=1;
			$datos["descripcion"] = "La radicacion no existe";
		}
		
		echo json_encode($datos);
		break;
	case "BENE":
		$datos = array();
		/**
		 * 
		 * @POST tipoFallecido
		 * @POST idAfiliado
		 * @POST periodo
		 * @POST idFallecido
		 * 
		 */
		$tipoFallecido = $_REQUEST["tipoFallecido"];
		$idAfiliado = $_REQUEST["idAfiliado"];
		list($anno,$mes,$dia) = explode("/",$_REQUEST["fechaDefuncion"]);
		$periodo = increPeriodo($anno.$mes,"R");
		if($tipoFallecido==0){
			
			//beneficiario del subsidio los grupo familiar
			
			$sql = "SELECT a.idbeneficiario,a.idparentesco,a.giro,a.embarga,a91.detalledefinicion parentesco,b15.idpersona,b15.identificacion,b15.papellido,b15.sapellido,b15.pnombre,b15.snombre,a.idc,isnull(c15.identificacion,'') as idenc,c15.papellido as pac,c15.sapellido as sac,isnull(c15.pnombre,'OTRO') as pnc,c15.snombre as snc 
					FROM (
						SELECT a21.idtrabajador,a21.idbeneficiario,a21.idparentesco,a21.giro,a21.embarga,CASE WHEN a21.idparentesco=36 THEN a21.idbeneficiario ELSE a21.idconyuge END idc
						FROM aportes021 a21 WHERE a21.idparentesco IN(35,36,37,38) AND a21.giro='S' AND a21.estado='A' AND (a21.idparentesco=36 OR (a21.idparentesco!=36 AND a21.idconyuge>0)) AND a21.idtrabajador=".$idAfiliado."  
						UNION
						SELECT a21.idtrabajador,a21.idbeneficiario,a21.idparentesco,a21.giro,a21.embarga,-1 idc
						FROM aportes021 a21 WHERE a21.idparentesco IN(35,36,37,38) AND a21.giro='S' AND a21.estado='A' AND a21.idtrabajador=".$idAfiliado."  
					) a 
					INNER JOIN aportes015 b15 ON b15.idpersona=a.idbeneficiario
					LEFT JOIN aportes015 c15 ON c15.idpersona=a.idc
					INNER JOIN aportes091 a91 ON a91.iddetalledef=a.idparentesco
					WHERE 0=isnull((SELECT count(*) FROM aportes019 a19 INNER JOIN aportes020 a20 ON a20.iddefuncion=a19.iddefuncion AND a19.idtrabajador=a.idtrabajador AND a20.idbeneficiario=a.idbeneficiario),0)
					order by a.idc desc";
			$rsGrupoFamiliar = $db->querySimple($sql);
			$cont = 0;
			while(($rowGrupoFamiliar = $rsGrupoFamiliar->fetchObject())==true){
			
				$ultimaCuota = 0;
				$ultimaCuota = giroPorPeriodo($rowGrupoFamiliar->idbeneficiario,$periodo,$idAfiliado,$db);
			
				$datos[$cont]["giro"] = ($ultimaCuota==1)?'SI':'NO';
				$datos[$cont]["periodo"] = $periodo;
				//datos de los beneficiario del subsidio-----------------
				$datos[$cont]["id"]         = $rowGrupoFamiliar->idbeneficiario;
				$datos[$cont]["numero"]     = $rowGrupoFamiliar->identificacion;
				$datos[$cont]["nombres"]    = utf8_encode($rowGrupoFamiliar->pnombre." ".$rowGrupoFamiliar->snombre." ".$rowGrupoFamiliar->papellido." ".$rowGrupoFamiliar->sapellido);
				$datos[$cont]["idTercero"]  = $rowGrupoFamiliar->idc;
				$datos[$cont]["numTercero"] = $rowGrupoFamiliar->idenc;
				$datos[$cont]["nomTercero"] = utf8_encode($rowGrupoFamiliar->pnc." ".$rowGrupoFamiliar->snc." ".$rowGrupoFamiliar->pac." ".$rowGrupoFamiliar->sac);
				$datos[$cont]["parentesco"] = $rowGrupoFamiliar->parentesco;
				//-------------------------------------------------------
				//El tercero debe estar activo. Si el estado de esta persona es M o I
				//Debe grabar un nuevo tercero �No se como encontrar los terceros?
				$cont++;
			}
			
		}else if($tipoFallecido!=0){
			
			//Validar beneficiario con giro en el periodo anterior de la defuncion----
			$idFallecido = $_REQUEST["idFallecido"];
			$ultimaCuota = 0;
			$ultimaCuota = giroPorPeriodo($idFallecido,$periodo,$idAfiliado,$db);
			$datos["giro"] = ($ultimaCuota==1)?"SI":"NO";
			$datos["periodo"] = $periodo;
			//------------------------------------------------------------------------
		}
		echo json_encode($datos);
		break;
}
?>