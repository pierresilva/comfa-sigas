<?php
/* autor:       Orlando Puentes
 * fecha:       Agosto 2 de 2010
 * objetivo:    Procesar los movimientos de los Bonos almacenados en la base de datos. 
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once  $raiz . DIRECTORY_SEPARATOR . 'rsc' . DIRECTORY_SEPARATOR . 'conexion.class.php';
class EContrato{
 //constructor	
var $con;
function EContrato(){
 		$this->con=new DBManager;
 	}

function insert_liquidacion($campo){
		if($this->con->conectar()==true){
			$sql="insert into aportes002(idcontratante, idcontratista, contrato, objeto, valorinicial, valortotal, aiu,totalneto, baseaporte, aportes, observaciones, fechasistema, usuario) values(".$campo[2].",".$campo[3].",'".$campo[4]."','".$campo[5]."',".$campo[6].",".$campo[7].",".$campo[8].",".$campo[9].",".$campo[10].",".$campo[11].",'".$campo[12]."',cast(getdate() as date),'".$campo[13]."')";
			//echo $sql;
			return mssql_query($sql,$this->con->conect);
		}
	}


function insert_detalle($campo){
		if($this->con->conectar()==true){
			$sql="insert into aportes003(idliquidacion, valoradicional) values(".$campo[0].",".$campo[1].")";
			//echo $sql;
			return mssql_query($sql,$this->con->conect);
		}
	}
	
	function buscar_contrato($contra){
		if($this->con->conectar()==true){
			$sql="Select contrato from aportes002 where contrato='$contra'";
			return mssql_query($sql,$this->con->conect);
		}
	}
	
	function buscar_contrato2($contra,$ide){
		if($this->con->conectar()==true){
			$sql="Select contrato from aportes002 where contrato='$contra' and idcontratante=$ide";
			return mssql_query($sql,$this->con->conect);
		}
	}
	
	function buscar_contrato_todo($nitContratante = '', $nitContratista = '', $numContrato = '', $idContrato = null){
		if($this->con->conectar()==true){
			$arrCondiciones = array();
			$strCondiciones = "";
			
			if($idContrato != null)
				$arrCondiciones[] = " idliquidacion = $idContrato ";
			else{
				if($nitContratante != "")
					$arrCondiciones[] = " c1.nit = '$nitContratante' ";
				if($nitContratista != "")
					$arrCondiciones[] = " c2.nit = '$nitContratista' ";
				if($numContrato != "")
					$arrCondiciones[] = " contrato = '$numContrato' ";
			}

			if(count($arrCondiciones)>0)
				$strCondiciones = " WHERE " . implode(" AND ", $arrCondiciones);
			
			$sql="SELECT aportes002.*, c1.nit as nit_contratante, c1.razonsocial as razonsocial_contratante, c2.nit as nit_contratista, c2.razonsocial as razonsocial_contratista FROM aportes002 INNER JOIN aportes048 c1 ON c1.idempresa = aportes002.idcontratante INNER JOIN aportes048 c2 ON c2.idempresa = aportes002.idcontratista ". $strCondiciones ." ORDER BY idliquidacion desc";
			return mssql_query($sql,$this->con->conect);
		}
	}
		
function buscar_historico($ide){
	if($this->con->conectar()==true){
		$sql="Select * from aportes002 where idcontratista=$ide";
		return mssql_query($sql,$this->con->conect);
	}	
}	
		
	function guardar_contratista($campos){
		if($this->con->conectar()==true){
			$idTipoDoc = 5;
			$sql="insert into aportes048 (idtipodocumento, nit, digito, razonsocial, sigla, direccion, iddepartamento, idciudad, telefono, url, email, idrepresentante, idcodigoactividad, idsector, seccional, estado, fechaaportes, fechaafiliacion, usuario, fechasistema, idclasesociedad, legalizada, nitrsn,codigosucursal,principal,contratista,exento ) values($idTipoDoc, '".$campos[0]."',".$campos[1].",'".$campos[2]."','".$campos[3]."','".$campos[4]."','".$campos[5]."','".$campos[6]."','".$campos[7]."','".$campos[8]."','".$campos[9]."',".$campos[13].",".$campos[10].",94,'".$campos[12]."','N',cast(getdate() as date),cast(getdate() as date),'". $campos[14]."',cast(getdate() as date),".$campos[11].",'N',".$campos[0].",'000','S','S','N')";
			return mssql_query($sql,$this->con->conect);
		}
	}
	
	function buscar_detalles_liquidacion($idLiquidacion){
		if($this->con->conectar()==true){
			$sql = "SELECT * FROM aportes003 where idliquidacion=$idLiquidacion";
			return mssql_query($sql,$this->con->conect);
		}
	}
	
	function limpiar_detalles_liquidacion($idLiquidacion){
		if($this->con->conectar()==true){
			$sql = "DELETE FROM aportes003 where idliquidacion=$idLiquidacion";
			return mssql_query($sql,$this->con->conect);
		}
	}
}
?>
