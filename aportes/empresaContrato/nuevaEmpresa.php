<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';

$url=$_SERVER['PHP_SELF'];
include_once $raiz. DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'auditoria.php';
auditar($url);

include_once $raiz . DIRECTORY_SEPARATOR . 'funcionesComunes' . DIRECTORY_SEPARATOR .'funcionesComunes.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'p.definiciones.class.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'ciudades.class.php';
$objClase=new Definiciones();
$objCiudad=new Ciudades();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="es">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Empresa Contratista</title>
<link type="text/css" rel="stylesheet" href="<?php echo URL_PORTAL; ?>newcss/marco.css" />
<link type="text/css" rel="stylesheet" href="<?php echo URL_PORTAL; ?>css/Estilos.css" />
<link type="text/css" rel="stylesheet" href="<?php echo URL_PORTAL; ?>css/css.1.8/jquery-ui-1.8.17.custom.css" />

<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/1.6/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/1.6/jquery-ui-1.8.16.custom.min.js"></script>

<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/comunes.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/direccion.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/llenarCampos.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/jquery.combos2.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/jquery.personaSimple.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/jquery.utilitarios.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/clases/empresa.class.js"></script>

<script type="text/javascript" src="js/nuevaEmpresa.js"></script>

<script type="text/javascript">
	shortcut.add("Shift+F",function() {
		var URL=src();
		var url=URL+"aportes/trabajadores/consultaTrabajador.php";
	   	window.open(url,"_blank");
	},{
		'propagate' : true,
		'target' : document 
	});        
</script>

<script type="text/javascript">
	fechaActual = new Date();
	$(function() { $('#fecAfiliacion').val(fechaActual.toLocaleDateString()); });
</script>

<script language="javascript">
	$(function() {
		$("#ayuda").dialog({
			 autoOpen: false,
			 height: 550, 
			 width: 800, 
			 open: function(evt, ui){
					$('#ayuda').html('');
					$.get('../../help/aportes/ayudaEmpresaNuevaC.htm',function(data){
							$('#ayuda').html(data);
					});
			 }
		});
		$("#dialog-form2").dialog({ autoOpen: false });
	});  
	
	$(document).ready(function(){		
		$("#cboCiudad2").jCombo("2", { 
			parent: "#cboDepto2",
			selected_value: '41001'
		});

		//Evento onchage del estado
		$("#estado").change(function () {
				var filtro = $("#estado").val();
				if(filtro=='I'){
					$("#rowInactivo").css('display','');
				} else {
					$("#rowInactivo").css('display','none');
				}
			});
	});
</script>

<!-- colaboracion en linea  -->
<script type="text/javascript">
	$(function() {
		$("#dialog-form2").dialog("destroy");		
		$("#dialog-form2").dialog({
			autoOpen: false,
			height: 400,
			width: 500,
			modal: true,
			buttons: {
				'Enviar': function() {
					var bValid = true;
					var campo=$('#notas').val();
					var campo0=$.trim(campo);
					if (campo0==""){
						$(this).dialog('close');
						return false;
						}
					var campo1=$('#usuarioEmpresa').val();
					var campo2="nuevaEmpresa.php";
					$.post('../../phpComunes/colaboracion.php',{v0:campo0,v1:campo1,v2:campo2}, function(datos){
						if(datos=='1'){
							alert("Su comentario fue enviado correctamente!. Gracias por participar en el mejoramiento de SIGAS");
						}else{
							alert(datos);
						}
					});
				$(this).dialog('close');
				},
				Cancelar: function() {
					$(this).dialog('close');
					$("#dialog-form2").dialog("destroy");
				}
			}
			
		});
		
		$('#enviar-notas')
			.button()
			.click(function() {
				$('#dialog-form2').dialog('open');
			});
	});
</script>
<style type="text/css">
<!--
.Estilo3 {font-family: Arial, Helvetica, sans-serif}
-->
</style>
</head>

<body>
<table width="98%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="13" height="29" class="arriba_iz">&nbsp;</td>
	    <td class="arriba_ce"><span class="letrablanca">::&nbsp;Nueva Empresa Contratista&nbsp;::</span></td>
	    <td width="13" class="arriba_de" align="right">&nbsp;</td>
 	</tr>
	<tr>
		<td class="cuerpo_iz">&nbsp;</td>
		<td class="cuerpo_ce">
			<img src="../../imagenes/tabla/spacer.gif" width="1" height="1"/>
			<img src="../../imagenes/spacer.gif" width="2" height="1"/>
			<img src="../../imagenes/menu/nuevo.png" title="Nuevo" width="16" height="16" style="cursor: pointer" onclick="nuevoREmpresa();"/>
			<img src="../../imagenes/spacer.gif" width="2" height="1"/>
			<img src="../../imagenes/menu/grabar.png" width="16" height=16 style="cursor: pointer" title="Guardar" onclick="validarCamposEmpresa(1);" id="bGuardar"/>			
			<img src="../../imagenes/spacer.gif" width="2" height="1"/>
			<img src="../../imagenes/menu/imprimir.png" width="16" height="16" style="cursor: pointer" title="Imprimir" onclick="window.print()"/>
			<img src="../../imagenes/spacer.gif" width="2" height="1"/>			
			<img src="../../imagenes/menu/informacion.png" width="16" height="16" style="border: none; cursor: pointer" title="Manual" onclick="mostrarAyuda();" />
		</td>
    	<td class="cuerpo_de">&nbsp;</td>
	</tr>
	<tr>
		<td class="cuerpo_iz">&nbsp;</td>
		<td align="left" class="cuerpo_ce">
			<div  id="error" class="Rojo" align="left"></div>
			<table width="100%" border="0" cellspacing="0" class="tablero">
				<tr>
					<td width="17%">Sector</td>
					<td width="29%">
						<select name="sector" class="box1" id="sector">
							<option value="0" selected="selected">Seleccione...</option>
						<?php
							$consulta=$objClase->mostrar_datos(16, 2);
							while($row=mssql_fetch_array($consulta)){
								echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
							}
						?>
   						</select>
   					</td>
					<td width="17%">Clase de Sociedad</td>
					<td width="37%">
						<select name="sociedad" class="box1" id="sociedad">
							<option value="0" selected="selected">Seleccione...</option>
						<?php
							$consulta=$objClase->mostrar_datos(17, 4);
							while($row=mssql_fetch_array($consulta)){
								echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
							}
						?>
   						</select>
   					</td>
				</tr>
				<tr>
					<td><label for="iden">NIT</label></td>
					<td>
						<label><input name="tNit" type="text" class="box1" id="tNit" value="" onblur="validarLongNumIdent(5,this); busquedaEmpresa($('#tNit'), $('#razonsocial'));"  onkeyup="solonumeros(this);"/> <img src="../../imagenes/menu/obligado.png" width="12" height="12" />
						 - </label> D&iacute;gito <label for="textfield3"></label> 
   						<input name="tDigito" type="text" class="boxcorto" id="tDigito" readonly="readonly" /><label>   						 
   					</td>
					<td>Tipo Documento</td>
					<td>
						<select name="tipoDoc" id="tipoDoc" class="box1">
							<option value="0" selected="selected">Seleccione...</option>
						<?php
							$consulta=$objClase->mostrar_datos(1, 4);
							while($row=mssql_fetch_array($consulta)){
								echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
							}
						?>
   						</select>
   					</td>
				</tr>
				<tr>
					<td>Raz&oacute;n Social</td>
					<td colspan="2"><input name="razonsocial" type="text" class="boxlargo" id="razonsocial" onkeypress="copiar(this);"> <img src="../../imagenes/menu/obligado.png" width="12" height="12" /></td>
					<td>C&oacute;digo Sucursal - 000</td>
				</tr>
				<tr>
					<td>Nombre Comercial</td>
					<td colspan="3"><input name="nombreComercial" type="text" class="boxlargo" id="nombreComercial" onkeypress="tabular (this, event);"/> <img src="../../imagenes/menu/obligado.png" width="12" height="12" /></td>
				</tr>
				<tr>
					<td>Direcci&oacute;n</td>
					<td colspan="3"><input name="tDireccion" type="text" class="boxlargo" id="tDireccion" onfocus="direccion(this,document.getElementById('cboDepto2'));"  /></td>
				</tr>
				<tr>
					<td>Departamento</td>
					<td> 
   						<select name="cboDepto2" class="box1" id="cboDepto2">
							<option value="0" selected="selected">-- Seleccione --</option>
						<?php
							$consulta=$objCiudad->departamentos();
							while($row=mssql_fetch_array($consulta)){        
								echo "<option value=".$row["coddepartamento"].">".$row["departmento"]."</option>";
							}
						?>
						</select>
					</td>
					<td>Ciudad</td>
					<td>
						<select name="cboCiudad2" class="box1" id="cboCiudad2">
							<option value="0" selected="selected">-- Seleccione --</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>Tel&eacute;fono</td>
					<td><input name="telefono" type="text" class="box1"	id="telefono"  maxlength="7"/></td>
					<td>Fax</td>
					<td><input name="fax" type="text" class="box1"	id="fax" /></td>
				</tr>
				<tr>
					<td>URL</td>
					<td><input name="url" type="text" class="box1"	id="url" /></td>
					<td>Email</td>
					<td><input name="email" type="text" class="box1" id="email" onblur="soloemail(this)" /></td>
				</tr>
				<tr>
		  			<td>C.C. Representante Legal</td>
		  			<td>
		 				<input name="ccRepresentante" type="text" class="box1" id="ccRepresentante" onblur="buscarPersona(this);" onkeypress="tabular(this,event);" />
		 				<input type="hidden" name="idRepresentante" id="idRepresentante" />
           			</td>
		 			<td colspan="2" class="Rojo" id="tNombres">&nbsp;</td>
	  			</tr>
				<tr>
					<td>Seccional</td>
					<td>
						<select name="seccional" class="box1" id="seccional">
							<option value="0" selected="selected">Seleccione</option>
							<option value="01">Neiva</option>
							<option value="02">Garz&oacute;n</option>
							<option value="03">Pitalito</option>
							<option value="04">La Plata</option>
						</select> <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
					</td>
					<td>Estado</td>
					<td>
						<select name="estado" class="box1" id="estado">
							<option value="0" selected="selected">Seleccione</option>
							<option value="A">Activo</option>
							<option value="I">Inactivo</option>
						</select> <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
					</td>
				</tr>
				<tr id="rowInactivo" style="display:none">
			    	<td>Causal Inactivaci&oacute;n</td>
			    	<td>
			    		<select name="cmbCausalInactivacion" id="cmbCausalInactivacion" class="boxmediano">
			    			<option value="0" selected="selected">Seleccione...</option>
			    			<?php
								$rsCausalInact=$objClase->mostrar_datos(65);
								while( ( $rowCausalInact=mssql_fetch_array($rsCausalInact) ) == true){
									echo "<option value=".$rowCausalInact['iddetalledef'].">".$rowCausalInact['detalledefinicion']."</option>";
								}
							?>
			    		</select>
			    		<img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
			    	</td>
			    	<td>Fecha estado</td>
			   		<td>
			   			<input type="text" id="fechaEstado" name="fechaEstado" class="box1" readonly="readonly" />
			   			<img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
			   		</td>
			    </tr>
				<tr>
					<td>Fecha Afiliaci&oacute;n</td>
					<td><input name="fecAfiliacion" type="text" class="box1" id="fecAfiliacion" readonly="readonly" value="" style="width: 170px;" /></td>
					<td>usuario</td>
					<td><input name="usuarioEmpresa" id="usuarioEmpresa" type="text" class="box1" readonly="readonly" value="<?php echo $_SESSION["USUARIO"]; ?>" /></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td colspan="3">&nbsp;</td>
				</tr>
			</table>
			<table width="100%" border="0" cellspacing="0">
  				<tr>
					<td align="left">&nbsp;</td>
	  			</tr>
				<tr>
					<td align="left" class="Rojo"><img src="../../imagenes/menu/obligado.png" width="16" height="12" align="left" /> Campo Obligado</td>
				</tr>
			</table>
  		</td>
		<td class="cuerpo_de">&nbsp;</td>
	</tr>
	<tr>
       	<td class="abajo_iz" >&nbsp;</td>
		<td class="abajo_ce" ></td>
		<td class="abajo_de" >&nbsp;</td>
	</tr>
</table>

<!-- formulario direcciones  --> 
<div id="dialog-form" title="Formulario de direcciones"></div>
<div id="div-observaciones-tab"></div>


<!-- colaboracion en linea -->
<!-- FORMULARIO OBSERVACIONES-->
<div name="div-observaciones-tab" style="display:none" title="Observaciones de modificaci&oacute;n">
	<table class="tablero">
		<tr>
			<td>Usuario</td>
			<td colspan="3" ><input name="usuarioObs" id="usuarioObs" class="box1" disabled="disabled" value="<?php echo $_SESSION['USUARIO']?>" /></td>
	   	</tr>
		<tr>
			<td>Observaciones</td>
			<td colspan="3" ><textarea name="observacionGrupo" id="observacionGrupo" cols="45" rows="5" class="boxlargo"></textarea></td>
		</tr>
	</table>
	<div class="ui-state-highlight ui-corner-all" style="margin: 9px auto; display:none;padding: 6px; text-align: center; border: 1px solid rgb(255, 255, 0); width:300px" id="rtaObservacion"><span class="ui-icon ui-icon-info" style="float: left; margin-right: 0.3em;"></span>Observaci&oacute;n guardada con &eacute;xito.</div>
</div>
<!-- fin colaboracion -->
<div id="dialog-persona" title="Formulario Datos Basicos"></div>
<!-- ayuda en linea -->
<div id="ayuda" title="Manual .:. Nueva Empresa Contrato" style="background-image:url('../../imagenes/FondoGeneral0.png')"></div>
<!-- fin ayuda en linea -->
</body>

<script language="javascript">
	function mostrarAyuda(){
		$("#ayuda").dialog('open' );
	}
	
	function notas(){
		$("#dialog-form2").dialog('open');
	}	
</script>
</html>
