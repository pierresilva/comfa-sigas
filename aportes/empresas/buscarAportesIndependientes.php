<?php
/**
 * @autor Andres Lara
* @fecha 28-Jun-2016
*/
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$url=$_SERVER['PHP_SELF'];
include_once $raiz.DIRECTORY_SEPARATOR ."config.php";
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejaInforma.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';

$db = IFXDbManejaInforma::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

$nit 	 = $_REQUEST['nit'];
$periodo = $_REQUEST['periodo'];

include_once $_SESSION["RAIZ"] . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'p.aportes.class.php';
$objClase=new Aportes();


/* ************************************************************** *
 * Antes de Buscar los aportes se debe buscar la planilla (A10)   *
* *************************************************************** */
	
	$consultaIndpte=$objClase->verificar_planillas_independientes($nit,$periodo);

	if($consultaIndpte=='N'){
		echo -1;
	}else{
		
		$rowIndpte=mssql_fetch_array($consultaIndpte);
		$numeroPlanilla=$rowIndpte["planilla"];
		
		//-----------------------------------------------------------------------------------------------------------------------------//
		$consulta = $objClase->obtener_aportes_indpte_por_periodo($numeroPlanilla,$periodo);
		
		$aportes = array();
		while($aporte = mssql_fetch_array($consulta)){
			$sql = "SELECT isnull(sum(aaportes-daportes),0) AS valordevolucion FROM aportes058 WHERE idaporte=".$aporte["idaporte"];
			$row = $db->querySimple( $sql )->fetch();
			$aportes[] = array_merge( $aporte, $row );
		}
		
		
		if(count($aportes) == 0)
			echo 0;
		else
			echo json_encode($aportes);
		//-----------------------------------------------------------------------------------------------------------------------------//
	}



