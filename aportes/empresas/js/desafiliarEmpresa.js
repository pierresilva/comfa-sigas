/*Validar que no desafilie la empresa si 
 * ya esta desafiliada y con el mismo estado
 * 
 */
$(function(){
	
	$("#btnDesafiliar").button();
	
	$("#txtNit").blur(function(){
		nuevo();
		
		var nit = $("#txtNit").val().trim();
		if(!nit)
			return false;
		
		var datosEmpresa = buscarDatosEmpresa(nit);				
		if(typeof datosEmpresa != "object"){
			$("#lblRazonSocial").addClass("classTextError").html(
					"No existe empresa con el nit " + nit);
			return false;
		}
		$("#lblRazonSocial").html(datosEmpresa.razonsocial);
		$("#hidIdEmpresa").val(datosEmpresa.idempresa);
	});
	
	$("#cmbCausalDesafiliacion").change(function(){
		/*	Nota: En un futuro se puede compara la causal para 
		 * 		buscar o no las suspensiones, por que las desafiliaciones 
		 * 		tambien pueden ser voluntarias, etc.
		 * 		*/
		
		$("#txtNit").removeClass("ui-state-error");
		
		var idEmpresa = $("#hidIdEmpresa").val();
		var causal = $(this).val();
		if(!idEmpresa){
			$("#txtNit").addClass("ui-state-error");
			return false;
		}
		
		enabledButton();
		
		limpiarCampos2("#tBody,#tdMensajeError");
		if(!causal){
			return false;
		}
		if($("#cmbCausalDesafiliacion").find(":selected").attr("title")!='EMR' &&  $("#cmbCausalDesafiliacion").find(":selected").attr("title")!='EMP'){
			datosSuspension(idEmpresa);
		}
		
		
	});
	
	//Control para el elemento
	$("#cmbEstadoEmpresa").change(function(){
		var mensaje="";
		if($(this).val()=="I"){
			mensaje = "Las afiliaciones de los empleados tambi\xE9n se inactivan";	
		}
		$("#lblMensajeEstado").text(mensaje);
	});
});

function datosSuspension(idEmpresa){
	$("#tdMensajeError").text("");
	limpiarCampos2("#tBody");
	//Buscar los periodos Suspendidos
	var datosSuspension = buscarSuspension(idEmpresa);
	
	if(typeof datosSuspension != "object"){
		$("#tdMensajeError").text("La empresa no tiene periodos suspendidos");
		//Desabilitar el boton para la desafiliacion
		disableButton();
	}else{
		var htmlTr = "";
		var contadorLiq = 0;
		$.each(datosSuspension,function(i,row){
			
			htmlTr += "<tr><td class='clsIdSuspension'>"+row.id_suspension+"</td>" +
					"<td style='text-align:center;'>"+row.periodo+"</td>" +
					"<td style='text-align:center;'>"+row.liquidacion+"</td>" +
					"<td style='text-align:center;'>"+row.fecha_sistema+"</td></tr>";
			
			//Validar que el periodo este liquidado
			if(row.liquidacion=="N")
				contadorLiq++;
		});
		
		//Adicionar el contenido de las suspensiones al formulario
		$("#tBody").html(htmlTr);
		
		if(datosSuspension.length<2){
			$("#tdMensajeError").text("La empresa debe tener dos o mas periodos en suspensi\xF3n");
			
			//Desabilitar el boton para la desafiliacion
			disableButton();
		}else if(contadorLiq>0){
			$("#tdMensajeError").text("Los periodos suspendidos deben estar liquidados");
			
			//Desabilitar el boton para la desafiliacion
			disableButton();
		}
	}
}

/**
 * Busca los datos de la empresa de acuerdo al nit
 * @param nit
 * @returns Datos de la empresa
 */
function buscarDatosEmpresa(nit) {
	var data;
	$.ajax({
		url:URL+"phpComunes/buscarEmpresaCompleta.php",	
		type:"POST",
		data:{v0:nit},
		dataType:"json",
		async:false,
		success:function(datos){
			if(typeof datos == "object")
				data = datos[0];
		}
	});
	return data;
}

/**
 * Busca los datos de la empresa de acuerdo al id empresa
 * @param idEmpresa
 */
function buscarSuspension(idEmpresa,nit){
	var data;
	$.ajax({
		url:"buscarEmpresaSuspension.php",	
		type:"POST",
		data:{objDatos:{id_empresa:idEmpresa,nit:nit,estado:"A"}},
		dataType:"json",
		async:false,
		success:function(datos){
			if(typeof datos == "object")
				data = datos;
		}
	});
	return data;
}

function disableButton(){
	$("#btnDesafiliar").attr("disabled",true);
}
function enabledButton(){
	$("#btnDesafiliar").attr("disabled",false);
}

function nuevo(){
	limpiarCampos2("#tdMensajeError,#tBody,#hidIdEmpresa,#lblRazonSocial,#txaObservacion," +
			"#cmbEstadoEmpresa,#cmbCausalDesafiliacion,#lblMensajeEstado,#txtFecha");
	$("#lblRazonSocial").removeClass("classTextError");
	
	//Remover class de error
	$(".element-required").removeClass("ui-state-error");
	
	//Habilitar boton
	enabledButton();
}

/**
 * Metodo para desafiliar la empresa
 */
function desafiliar(){
	
	//Validar los datos requeridos del formulario
	if(validaCampoFormu())
		return false;
	
	var idEmpresa = $("#hidIdEmpresa").val();
	var estadoEmpresa = $("#cmbEstadoEmpresa").val();
	var fecha=$("#txtFecha").val();	
	var observacion = $("#txaObservacion").val();
	
	var causal = $("#cmbCausalDesafiliacion").val();
	
	var objDatosEncabezado = {id_empresa:idEmpresa
			,id_causal:causal
			,estado_empresa:estadoEmpresa
			,observacion:observacion
			,fechaDesafiliacion:fecha};
	
	/*	Nota: En un futuro se puede compara la causal para 
	 * 		buscar o no las suspensiones, por que las desafiliaciones 
	 * 		tambien pueden ser voluntarias, etc.
	 * 		*/
	var arrDatosDetalle = [];
	//Obtener los datos de la suspension
	$("#tBody tr td.clsIdSuspension").each(function(){
		arrDatosDetalle[arrDatosDetalle.length] = {id_suspension:$(this).text().trim()};
	});
	
	$.ajax({
		url:"guardarDesafiliacionEmpresa.php",
		type:"POST",
		data:{datosEncabezado:objDatosEncabezado,datosDetalle:arrDatosDetalle},
		dataType:"json",
		async:false,
		beforeSend: function(objeto){
			dialogLoading('show');
		},
		complete: function(objeto, exito){
			dialogLoading('close');
		}, 
		success:function(datos){
			if(datos==0){
				alert("Error al desafiliar la empresa");
			}else{
				alert("La desafiliacion se realizo correctamente");
				nuevo();
				limpiarCampos2("#txtNit");
			}
		}
	});
}

/**
 * Validar los elementos requeridos del formulario
 * @returns {Number}
 */
function validaCampoFormu(){
	var banderaError = 0;
	$( ".ui-state-error" ).removeClass( "ui-state-error" );
	$(".element-required").each(function(i,row){
		if( $( this ).val() == 0 ){
			$( this ).addClass( "ui-state-error" );
			banderaError++;
		}
	});
	return banderaError;
}
