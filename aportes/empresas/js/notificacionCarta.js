var uri = "",
	uriBuscarControlNotificacion = "",
	uriBuscarEmpresa = "",
	uriNotificacionCartaLog = "";
$(function(){
	uri = src();
	uriBuscarControlNotificacion = uri+"aportes/empresas/buscarControlNotificacion.php";
	uriBuscarEmpresa = uri+"aportes/empresas/buscarEmpresaNIT.php";
	uriNotificacionCartaLog = uri+"aportes/empresas/notificacionCarta.log.php";
		
		
	$("#btnNuevo").click(nuevo);
	$("#btnGuardar").click(guardar);
	$("#btnListar").click(ctrListarNotificacion);
	
	$("#txtNit").blur(ctrEmpresa);
	
	$("#divListaNotificacion").dialog({
		modal:true,
		autoOpen:false,
		width: 600,
		heigth:200
	});
});

function fetchEmpresa(nit){
	var resultado = null;
	
	$.ajax({
		type:"POST",
		url: uriBuscarEmpresa,
		data: {v0:nit},
		dataType: "json",
		async: false,
		success: function(data){
			resultado = data;
		}
	});
	
	return resultado;
}

function fetchControlNotificacion(objFiltro){
	var resultado = null;
	
	$.ajax({
		type:"POST",
		url: uriBuscarControlNotificacion,
		data: {datos:objFiltro},
		dataType: "json",
		async: false,
		success: function(data){
			resultado = data;
		}
	});
	
	return resultado;
}

function ctrEmpresa(){
	var nit = $("#txtNit").val().trim();
	nuevo();
	if(!nit) return false;
	
	$("#txtNit").val(nit);
	
	var objDataEmpresa = fetchEmpresa(nit);
	var razonSocial = "";
	var idEmpresa = "";
	
	if(objDataEmpresa && typeof objDataEmpresa=="object" && objDataEmpresa.length>0){
		idEmpresa = objDataEmpresa[0].idempresa;
		razonSocial = objDataEmpresa[0].razonsocial;
	}else{
		razonSocial = "<b>La empresa no existe</b>";
	}
	
	$("#tdRazonSocial").html(razonSocial);
	$("#hidIdEmpresa").val(idEmpresa);
}

function ctrNotificacion(idNotificacion){
	var objData = fetchControlNotificacion({id_notificacion:idNotificacion});
	
	var htmlNotificacion = "";
	if(objData && typeof objData=="object" && objData.length>0){
		objData = objData[0];
		$("#hidIdNotificacion").val(objData.id_notificacion);
		$("#cmbCartaNotificacion").val(objData.id_carta_notificacion);
		$("#txaInformacion").val(objData.informacion);
		
	}
	
	$("#divListaNotificacion").dialog("close");
}

function ctrListarNotificacion(){
	var idEmpresa = $("#hidIdEmpresa").val().trim();
	if(idEmpresa==0)return false;
	
	var objData = fetchControlNotificacion({id_empresa:idEmpresa});
	
	var htmlNotificacion = "";
	if(objData && typeof objData=="object" && objData.length>0){
		
		$.each(objData,function(i,row){
			htmlNotificacion += "<tr><td><a style='cursor:pointer;' onclick='ctrNotificacion("+row.id_notificacion+");'>"+row.carta_notificacion+"</a></td>" +
					"<td>"+row.informacion+"</td>" +
					"<td>"+row.fecha_sistema+"</td></tr>";
		});	
	}
	
	if(htmlNotificacion==""){
		alert("La empresa no tiene notificaciones");
	}else{
		$("#tblListaNotificacion tbody").html(htmlNotificacion);
		$("#divListaNotificacion").dialog("open");
	}
}

function fetchDataForm(){
	var objData = {
			id_empresa: $("#hidIdEmpresa").val().trim()
			, id_notificacion: $("#hidIdNotificacion").val().trim()
			, id_carta_notificacion: $("#cmbCartaNotificacion").val().trim()
			, informacion: $("#txaInformacion").val().trim()
			, id_estado:0
			, fecha_estado: ""
			, contenido_carta: ""
			, accion: ""};
	
	return objData;
}

function nuevo(){
	limpiarCampos2("input:text,input:hidden,select,textarea,#tblListaNotificacion tbody,#tdRazonSocial");
}

function guardar(){
	
	//Validar los elementos requeridos del formulario
	if(validateElementsRequired()>0) return false; 
	
	var objData = fetchDataForm();
	if(objData.id_notificacion>0) objData.accion = "U";
	else objData.accion = "I";
	
	$.ajax({
		type:"POST",
		url: uriNotificacionCartaLog,
		data: {datos:objData},
		dataType: "json",
		async: false,
		success: function(data){
			if(data.error==0){
				alert("Los datos se guardaron correctamente");
				nuevo();
			}else{
				alert("Error al guardar los datos");
			}
		}
	});
	
}