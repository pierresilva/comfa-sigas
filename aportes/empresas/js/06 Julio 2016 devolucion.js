var URL=src();
var nomina=0;
var aportes=0;
var ide=0;
var nuevo=0;
var fechapago=0;
var comprobante = null;
var documento = null;
var numTrabActivos;
var idEmpresa = null;
var arregloTrabajadores = [];
var persona = null;

var valorPagado = 0;
var valorCaja = null;
var valorNomina = null;
var indicador = null;
var sena = null;
var icbf = null;
var planilla = null;

var idApoDev = null;
var idTdDevolucion = null;

$(function(){
	$("#btn_add_persona").button();
	$("#periodo").datepicker({
		dateFormat: 'mmyy',
		changeMonth: true,
		changeYear: true,
		showButtonPanel: true,
		//maxDate: '+0D',
		onClose: function(dateText, inst) {
			var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
			var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
			$(this).val($.datepicker.formatDate('yymm', new Date(year, month, 1)));
			buscarAportesPeriodo();		
		}
	});
	$("#periodo").focus(function () {
		$(".ui-datepicker-calendar").hide();
		$("#ui-datepicker-div").position({
			my: "center top",
			at: "center bottom",
			of: $(this)
		});
	});
	
	$("#txtFechaDevolucion").datepicker({
		changeMonth: true,
		changeYear: true,
		maxDate: '+0D'
	});
	
	$("#periodo").bind("blur",limpiarxPeriodo);
	
	$("#txtNumero").bind("blur",buscarEmpresa);
	
	$("#numDoc").bind("focus",function(){
		if(idEmpresa==null){
			alert("Digite primero el Nit de la empresa");			
			$("#txtNumero").focus();
			return false;
		}
		if($("#periodo").val()==""){
			alert("Digite primero el Periodo del aporte");			
			$("#periodo").focus();
			return false;
		}
		var idAporte = $("[name=opAporte]:checked").val();
		if(!idAporte > 0){
			alert("Debe seleccionar un aporte");
			return false;
		}
	});
	
	$("#numDoc").bind("blur",function(){
		persona = null;
		$("#tdTrabajador").empty();		
		
		if($(this).val() != ''){
			controlTrabajador();
			
			/*buscarPlanilla();
			
			if(persona==null)
				return false;
						
			if(!persona.idpersona>0 || isNaN(persona.idpersona) || persona.idpersona==""){
				$.ajax({
					url: URL+'phpComunes/buscarPersona2.php',
					type: "POST",
					data: {v1:$(this).val(),v0:$("#tipoDoc").val()},
					async: false,
					dataType: 'json',
					success: function(datos){
						if(datos == 0){											
							persona_simple2(document.getElementById('numDoc'));	//Crear persona						
							return false;										
						}
					}
				});
			}*/
		}
	});
	
	$("#valortrabajador").bind("blur",agregarTrabajador);
	$("#btn_add_persona").bind("click",agregarTrabajador2);
});

function controlTrabajador(){
	var numeroIdentificacion = $("#numDoc").val();
	var tipoIdentificacion = $("#tipoDoc").val();
	var nit = $("#txtNumero").val();
	var periodo = $("#periodo").val();
	var pu = planilla;
	var idAporte = $("[name=opAporte]:checked").val();
	var valorAporte = 0;
	var idPersonaPU = 0;
	var idEmpresa = $("#hidIdEmpresa").val();
	
	var objPersona = null;
	
	//Buscar PU
	var objPU = fetchPU(nit, numeroIdentificacion, tipoIdentificacion, periodo, pu);
	
	if(objPU==0){
		//Buscar Persona
		var objFiltro = {idempresa:idEmpresa,identificacion:numeroIdentificacion, tipodocumento:tipoIdentificacion};
		objPersona = fetchAfiliActivInact(objFiltro);
		objPersona = objPersona[0];
		
		var idTipoAfiliacion = 0;
		if(typeof objPersona == "object"){
			//idTipoAfiliacion = isNaN(parseInt(objPersona[0].tipoafiliacion))?0:parseInt(objPersona[0].tipoafiliacion);
			idTipoAfiliacion = isNaN(parseInt(objPersona.tipoafiliacion))?0:parseInt(objPersona.tipoafiliacion);
		}
		
		// [18][DEPENDIENTE]
		if(idTipoAfiliacion==18){
			$("#numDoc").val('');
			$("#tdTrabajador").empty();
			alert("No se encontro planilla para modificar");
			return false;
		}
		
	}else{
		
		var arrPersona = fetchPersona(objPU[0].idpersona);
		arrPersona = arrPersona[0];
		
		arrPersona.trabajador = arrPersona.pnombre + " " + arrPersona.snombre + " " + arrPersona.papellido + " " + arrPersona.sapellido;
		
		objPersona = arrPersona;
		//var objFiltro = {idpersona:objPU[0].idpersona};
		//objPersona = fetchAfiliActivInact(objFiltro);
		
		valorAporte = objPU[0].valoraporte;
		idPersonaPU = isNaN(parseInt(objPU[0].idpersona2))?0:parseInt(objPU[0].idpersona2);
		
		//Variable global 
		persona = objPU[0];
	}
	
	//Verificar las devoluciones
	if(typeof objPersona == "object"){
		//objPersona = objPersona[0];
		var objDevolucion = fetchDevolucion(idAporte, objPersona.idpersona);
		
		if(typeof objDevolucion == "object"){
			objDevolucion = objDevolucion[0];
			alert("La persona tiene una devolucion de $"+objDevolucion.valor+" realizada el "+objDevolucion.fechasistema);
			$("#tdTrabajador").empty();
			$("#numDoc").val('');
			$("#numDoc").focus();
			return false;
		}
	}else if(objPU == 0){
		alert("La persona no exite");
		return false;
	}
	
	//Crear la persona si no exite paro debe estar en la PU
	if(typeof objPersona != "object" && objPU != 0){
		persona_simple2(document.getElementById('numDoc'));	//Crear persona	
		return false;
	}
	
	//Verificar si la persona aun no esta agregada a la devolucion actual
	var banderaAgregado = false;
	$.each(arregloTrabajadores,function(ind,trabajador){
		if(trabajador.id == idPersona){
			banderaAgregado = true;
		}
	});
	if(banderaAgregado==true){
		alert("Ya ha agregado a esa persona en la devoluci&oacute;n.");
		$("#numDoc").val('');
		$("#tdTrabajador").empty();
		return false;
	}
	
	//Adicionar los datos del trabajador al formulario
	var idPersona = objPersona.idpersona;
	
	var existe = idPersonaPU==0?'N':'S';
	
	var str = idPersona +" - "+ numeroIdentificacion +" "+ objPersona.trabajador +"<input type='hidden' id='idtrabajador' name='idtrabajador' value='"+ idPersona +"' />" +
			"<input type='hidden' id='valorAporteTrab' name='valorAporteTrab' value='"+ valorAporte +"' />" +
			"<input type='hidden' id='TrabExiste' name='TrabExiste' value='"+ existe +"' />" +
			"<input type='hidden' id='nombretrabajador' name='nombretrabajador' value='"+ objPersona.trabajador +"' />" +
			"<input type='hidden' id='identificacion' name='identificacion' value='"+ numeroIdentificacion +"' />";
	
	$("#tdTrabajador").html(str);	
	
}

function fetchPersona(idPersona){
	var resultado = null;
	
	$.ajax({
		url: URL+'phpComunes/pdo.buscar.persona.id.php',
		type: "POST",
		data: {v0:idPersona},
		async: false,
		dataType: 'json',
		success: function(data){
			resultado = data;
		}
	});
	return resultado;
}

function fetchPU(nit, numeroIdentificacion, tipoIdentificacion, periodo, pu){
	var resultado = null;
	
	$.ajax({
		url: 'buscarPlanilla.php',
		type: "POST",
		data: {v0:nit,v1:periodo,v2:tipoIdentificacion,v3:numeroIdentificacion,v4:pu},
		async: false,
		dataType: 'json',
		success: function(data){
			resultado = data;
		}
	});
	return resultado;
}

function fetchDevolucion(idAporte, idPersona){
	var resultado = null;
	$.ajax({
		url: URL+'phpComunes/buscarDevolucion.php',
		type: "POST",
		data: {v0:idAporte,v1:idPersona},
		async: false,
		dataType: 'json',
		success: function(data){
			resultado = data;
		}
	});
	return resultado;
}

function fetchAfiliActiv(objFiltro){
	var resultado = null;
	
	$.ajax({
		url: URL+'phpComunes/buscarAfiliActiv.php',
		type: "POST",
		data: {objDatosFiltro:objFiltro},
		async: false,
		dataType: 'json',
		success: function(data){
			resultado = data;
		}
	});
	return resultado;
}

function fetchAfiliActivInact(objFiltro){
	var resultado = null;
	
	$.ajax({
		url: URL+'phpComunes/buscaAfiliActivInact.php',
		type: "POST",
		data: {objDatosFiltro:objFiltro},
		async: false,
		dataType: 'json',
		success: function(data){
			resultado = data;
		}
	});
	return resultado;
}

function buscarEmpresa(){
	if(nuevo != 0){
		MENSAJE("Seleccione primero el bot&oacute;n NUEVO.");
		return false;
	}
	
	//Limpiar campos 
	arregloTrabajadores = [];
	valorPagado=0;
	valorCaja=null;
	valorNomina = null;
	indicador = null;
	sena = null;
	icbf = null;
	planilla = null;
	idApoDev = null;
	idTdDevolucion = null;
	persona=null;	
	$("#caja").find("option[value='"+idEmpresa+"']").remove();	
	$("#periodo").val('');
	$("#numDoc").val('');
	$("#tdTrabajador").empty();
	$("#TrabExisten").val('S');
	$("#tblTrabajadores tbody").empty();
	$("#tblAportes tbody").empty();
	$("#td_total_devolver_trab").empty();
	
	var nit = $("#txtNumero").val();
	$("#tdEmpresa").empty();
	$("#hidIdEmpresa").val();
	idEmpresa = null;
	if(nit != ''){
		$.ajax({
			url: URL+'phpComunes/pdo.b.empresa.nit.php',
			type: "POST",
			data: {v0:nit},
			async: false,
			dataType: 'json',
			success: function(datos){
				if(datos==0){
					MENSAJE("No existe la empresa o no hay una afiliacion Principal, contacte al administrador del sistema!");				
					//camposLimpiar();	//Ing pide solo limpiar el error
					$("#txtNumero").val('');
					$("#txtNumero").focus();
				}
				$.each(datos,function(i,fila){
					// Empresas Ley 1429
					claseaportante = fila.claseaportante;
					// ------------------
					rz=fila.razonsocial;
					t= fila.idempresa +" - "+nit+" - "+rz;
					idEmpresa = fila.idempresa;
					$("#tdEmpresa").html(t);
					$("#hidIdEmpresa").val(idEmpresa);
					
					
					if(claseaportante==2875){
						$("#TrIndicador").css('display','block');
					} else {
						$("#TrIndicador").css('display','none');
					}
				    $('#caja').append('<option value="'+idEmpresa+'" selected="selected" >'+rz+'</option>');  
				});
			}
		});
	}
}


function nuevoR(){
	$("#bGuardar").show();
	camposLimpiar();
}

function camposLimpiar(){
	$("#caja").find("option[value='"+idEmpresa+"']").remove();
	idEmpresa = null;
	valorPagado = 0;
	valorCaja=null;
	valorNomina = null;
	indicador = null;
	sena = null;
	icbf = null;
	planilla = null;
	idApoDev = null;
	idTdDevolucion = null;
	arregloTrabajadores = [];
	persona=null;
	$("#TrabExisten").val('S');
	$("input,select,textarea").not("#btn_add_persona").val('');
	//$("#nit").focus();	//campo no encontrado
	$("#txtNumero").focus();
	$("table.tablero input:text").val('');
	$("table.tablero select").val(0);
	$("#tdEmpresa").html("");
	$("#hidIdEmpresa").val("");
	$("#tblTrabajadores tbody").empty();
	$("#tblAportes tbody").empty();
	$("#td_total_devolver_trab").empty();
	nuevo=0;
}

function quitarPersona(idPersona){
	var id = idPersona;
	var cont = 0;
	
	for ( i = 0; i < arregloTrabajadores.length; i++ ){
		if( arregloTrabajadores[ i ].id == id ){
			calcularAporteDevolucion( "S", parseInt( arregloTrabajadores[ i ].valor ) );
			arregloTrabajadores.splice( i ,1 );
		} else {
			if(arregloTrabajadores[ i ].existe=='N'){
				cont++;
			}
		}	
	}
	
	if(cont==0) $("#TrabExisten").val('S');
	
	$("#tr_trabajador_"+ id).remove();
	$("#td_total_devolver_trab").empty().append(calcularTotalDevolver());
}

function agregarTrabajador2(){
	if($("#numDoc").val() == '')
		return false;
	
	agregarTrabajador();
}

function agregarTrabajador(){
	/*if(  parseInt( $( "#valor" ).val().trim() ) <  parseInt( $("#valortrabajador").val().trim() ) ){
		alert("El valor a devolver por el trabajador es mayor que el valor devoluci&oacute;n.");
		$("#valor").focus();
		return false;
	} */ 
	if( $("#valortrabajador").val().trim()=="" )
		return false;
	
	/*if( parseInt( $( idApoDev ).text().trim() ) < parseInt ( $("#valortrabajador").val() ) ) {
		alert("El valor a devolver por el trabajador es mayor que el valor pagado.");
		$("#valortrabajador").focus();
		return false;
	}*/		
	
	var idPersona = $("#idtrabajador").val();
	var agregado = false;
	$.each(arregloTrabajadores,function(ind,trabajador){
		if(trabajador.id == idPersona){
			MENSAJE("Ya ha agregado a esa persona en la devoluci&oacute;n.");
			agregado = true;
		}
	});
	
	if(agregado == false){
		var valorAporteTrab = $("#valorAporteTrab").val();
		var nomTipoDoc = $("#tipoDoc option:selected").text();
		var numDoc = $("#numDoc").val();
		var nomPersona = $("#nombretrabajador").val();
		var idAdmin = $("#caja").val();
		var nomAdmin = $("#caja option:selected").text();
		var valorDevolver = $("#valortrabajador").val();
		var idBanco = $("#banco").val();
		var nomBanco = $("#banco option:selected").text();
		var idTipoCuenta = $("#idtipocuenta").val();
		var tipoCuenta = $("#idtipocuenta option:selected").text();
		var cuenta = $("#numerocuenta").val();
		var existe = $("#TrabExiste").val();		
		var msjError = "";
		if($("#numDoc").val() == '')
			msjError += "Verifique la identificaci&oacute;n.<br/>";
		if(idPersona == '' || idPersona == 0)
			msjError += "Verifique el trabajador a agregar.<br/>";
		if(cuenta == '')
			msjError += "Verifique el No. de cuenta bancaria.<br/>";
		if(valorDevolver == '' || valorDevolver == 0)
			msjError += "Verifique el valor a devolver del trabajador.<br/>";
		
		if(msjError != ''){
			MENSAJE(msjError);
			return false;
		}
		
		if(existe=='N') $("#TrabExisten").val('N');
		
		calcularAporteDevolucion( "R", valorDevolver );
		
		arregloTrabajadores[arregloTrabajadores.length] = {'id': idPersona, 'valorAporteTrab': valorAporteTrab, 'nomtipodoc': nomTipoDoc, 'numdoc': numDoc, 'nombre': nomPersona, 'idcaja': idAdmin, 'nomcaja': nomAdmin, 'idbanco': idBanco, 'nombanco': nomBanco, 'idtipocuenta': idTipoCuenta, 'tipocuenta':  tipoCuenta, 'numcuenta': cuenta,'valor': valorDevolver, 'existe': existe};
		var strTr = "<tr id='tr_trabajador_"+ idPersona +"'>" +
					"<td>"+ idPersona +"</td>" +
					"<td>"+ $("#identificacion").val() +"</td>" +
					"<td>"+ $("#nombretrabajador").val() +"</td>" +
					"<td>"+ nomAdmin +"</td>" +
					"<td>"+ nomBanco +"</td>" +
					"<td>"+ tipoCuenta +"</td>" +
					"<td>"+ cuenta +"</td>" +
					"<td style='font-weight:bold;'>$ "+ valorDevolver +"</td>"+
					"<td><input type='button' value='Quitar' id='btn_quitar_"+ idPersona +"' onclick='quitarPersona("+ idPersona +")' /></td></tr>";
		$("#tblTrabajadores tbody").append(strTr);
		$("#tdTrabajador").empty();
		//$("#numDoc,#tipoDoc,#caja,#banco,#idtipocuenta,#numerocuenta,#valortrabajador").val('');
		$("#numDoc,#valortrabajador").val('');
		$("#td_total_devolver_trab").empty().append(calcularTotalDevolver());
		$("#numDoc").focus();
	}
}

function calcularAporteDevolucion( op, valor ){
	valor = parseInt( valor );
	var valorDev = 0;
	var aporte = parseInt( formatNormal( $( idApoDev ).text().replace(/\$/g,'').trim() ) );
	var devolucion = parseInt( formatNormal( $( idTdDevolucion ).text().replace(/\$/g,'').trim() ) );
    if ( op == "S" ){ 
    	valorDev = ( aporte + valor );
    	devolucion = devolucion - valor;
	}else if( op == "R" ) {
		valorDev = ( aporte - valor );
		devolucion = devolucion + valor;
	}
    valorDev = "$ " + formatMoneda( valorDev.toString() );
    devolucion = "$ " + formatMoneda( devolucion.toString() );
    $( idApoDev ).html( valorDev );
    $( idTdDevolucion ).html( devolucion ); 
}
function calcularTotalDevolver(){
	var acumulado = 0;
	$.each(arregloTrabajadores,function(ind,trabajador){
		acumulado += parseInt(trabajador.valor);
	});
	return acumulado;
}

function validarCampos(){
	if(nuevo != 0){
		MENSAJE("Seleccione primero el bot&oacute;n NUEVO.");
		return false;
	}else{
		var valorDevolver = $("#valor").val();
		var totalDevolverTrabs = calcularTotalDevolver();
		var errores = "";
		var periodo = $("#periodo").val();
		var idAporte = $("[name=opAporte]:checked").val();
		//Empresas Ley 1429
		if (claseaportante==2875){
			var indicadorCombo = $("#Combindicador").val();
			
			if(indicadorCombo == 4268 ){
				indicadorC = 0;
			} else if (indicadorCombo == 4267){
				indicadorC = 1;
			} else if (indicadorCombo== 4266){
				indicadorC = 2;
			} else if (indicadorCombo == 4265){
				indicadorC = 3;
			} else if (indicadorCombo == 108){
				indicadorC = 4;
			}
	
			$.ajax({
				url: "buscarAportesDePeriodoPorNit.php",
				type: "POST",
				data: {idEmpresa: idEmpresa, periodo: $("#periodo").val()},
				async: false,
				dataType: 'json',
				success: function(datos){
					$.each(datos,function(i,fila){
						indicadorA = fila.indicador;
					});
				}
			});
			
		var comentario='Se realizo el cambio del indice de aportes';
    	var motivoText=$("#motivo").val();
		var motivo = comentario+' - '+periodo+' - Indice actual - '+indicadorC+' - Indice anterior - '+indicadorA+' - '+motivoText;
		}else{
			var motivo = $("#motivo").val();
		}
		//-----------------
		var fechaDevolucion = $( "#txtFechaDevolucion").val().trim();
		var nombreActo = $( "#txtNombreActo" ).val().trim();
		var numeroActo = $( "#txtNumeroActo" ).val().trim();
		
		if(!idAporte > 0)
			errores += "No hay aporte seleccionado.<br/>";
		if(valorDevolver != totalDevolverTrabs)
			errores += "El valor total no corresponde con los detalles de la devoluci&oacute;n.<br/>";
		if(idEmpresa == null || parseInt(idEmpresa) == 0)
			errores += "No ha digitado los datos de la empresa.<br/>";
		if(periodo == '' || periodo.length != 6)
			errores += "Digite un per&iacute;odo v&aacute;lido.<br/>";
		if($("#valor").val() == '' || parseInt($("#valor").val())==0)
			errores += "Digite el valor total a devolver.<br/>";
		if($("tr[id^=tr_trabajador_]").length == 0)
			errores += "No hay trabajadores digitados.<br/>";
		if(valorPagado < valorDevolver)
			errores += "El valor a devolver es mayor que el valor pagado.<br/>";
		////////////VALIDAR OBSERVACION
		var mtv=$("#motivo").val().replace(/^\s*|\s*$/g,"");
		if(mtv=="")
			errores += "Digite el motivo de la devolucion.<br/>";
		if(nombreActo=="")
			errores += "Digite el nombre del acto.<br/>";
		if(numeroActo=="")
			errores += "Digite el numero del acto.<br/>";
		if(isNaN(parseInt(numeroActo)))
			errores += "El numero del Acto debe ser numerico.<br/>";
		if(fechaDevolucion=="")
			errores += "Digite la fecha de la devolucion.<br/>";
		if(errores != ''){
			MENSAJE(errores);
			return false;
		}else{
			if(isNaN(valorCaja)) valorCaja=null;
			if(isNaN(valorNomina)){ alert("Valor de la Nomina de este Aporte no es valido"); return false; };
			if(isNaN(indicador)) indicador=null;
			if(isNaN(sena)) sena=null;
			if(isNaN(icbf)) icbf=null;
						
			var naporte=valorPagado-valorDevolver;
			var nnomina=0;
				if (claseaportante==2875){
					var indicadorCombo = $("#Combindicador").val();
					
					if(indicadorCombo == 4268 ){
						indicadorC = 0;
					} else if (indicadorCombo == 4267){
						indicadorC = 1;
					} else if (indicadorCombo== 4266){
						indicadorC = 2;
					} else if (indicadorCombo == 4265){
						indicadorC = 3;
					} else if (indicadorCombo == 108){
						indicadorC = 4;
					}
			var nindicador=indicadorC;
				}else{
			var nindicador=indicador;
				}
			var ncaja=0;
			var nsena=0;
			var nicbf=0;
			
			if(nindicador==null){
				nindicador=valorPagado*100/valorNomina;
			}
			
			nnomina=naporte*100/nindicador;
			//alert("nindicador  = "+nindicador);
			switch (nindicador){
			case 0:
				ncaja=naporte;
				break;
			case 0.6:
				ncaja=naporte;
				break;
			case 1:
				ncaja=naporte;
				break;
			case 2:
				ncaja=naporte;
				break;
			case 3:
				ncaja=naporte;
				break;
			case 4:
				ncaja=naporte;
				break;
			case 7:
				ncaja=parseInt(nnomina*0.04);
				nicbf=naporte-ncaja;
				break;
			case 9:
				ncaja=parseInt(nnomina*0.04);
				nicbf=parseInt(nnomina*0.03);
				nsena=naporte-ncaja-nicbf;
				break;
			default:
				alert("Indicador no corresponde a ninguno de los valores aceptados(0.6, 1, 2, 3, 4, 7, 9)");					
//				alert("nIndicador="+nindicador+"\nnnomina="+nnomina+"\naporte="+naporte+"\nncaja="+ncaja+"\nnsena="+nsena+"\nnicbf="+nicbf);
				return false;
				break;
		}
			
			//alert("idAporte="+idAporte+"\nvalorPagado="+valorPagado+"\nvalorCaja="+valorCaja+"\nsena="+sena+"\nicbf="+icbf+"\nNsena="+nsena+"\nNicbf="+nicbf+"\nmotivo="+motivo+"\nfecha="+fechaDevolucion);
			
			$.ajax({
				type:"POST", 
				url: "guardarDevolucion.php",
				data: {v0:idAporte,v1:valorPagado,v2:valorCaja,v3:sena,v4:icbf,v5:naporte,v6:ncaja,v7:nsena,v8:nicbf,v9:motivo,v10:fechaDevolucion,v11:nombreActo,v12:numeroActo},
				async:false,
				dataType: "json",
				success:function(idDevolucion){
					if(idDevolucion > 0){						
						alert("Se guardo Devolucion "+idDevolucion);
						var TrabExisten=$("#TrabExisten").val();
						// Empresas Ley 1429
						//alert("Entrar a modificar"+ nindicador);
						if(claseaportante==2875){
						//no se debe actualizar el aporte ni la nomina okr
						$.getJSON('modificarIndiceAporte.php',{idAporte:idAporte,indicador:nindicador},function(resActAporte){
							if(resActAporte==0){
								alert("No se pudo Actualizar el Indice de Aporte!");
								return false;
							}else{
								alert("Se actualizo el Indice de Aporte!");
							}
						});
						}
						// --------------------
						
						var arregloDetallesDevolucion = [];
						var arregloErroresDetalles = [];
						$.each(arregloTrabajadores,function(posic,trabajador){
							$.ajax({
								type:"POST", 
								url: "guardarDetalleDevolucion.php",
								data: {idDevolucion: idDevolucion, idPersona: trabajador.id, idCaja: trabajador.idcaja, idBanco: trabajador.idbanco, idTipoCuenta: trabajador.idtipocuenta, numeroCuenta: trabajador.numcuenta, valor: trabajador.valor},
								async:false,
								dataType: "json",
								success:function(idDetalleGrabado){
									if(idDetalleGrabado > 0){
										var valorAporteTrab = 0;
										if(trabajador.valorAporteTrab>0 && (trabajador.valorAporteTrab-trabajador.valor)>0){
											valorAporteTrab=trabajador.valorAporteTrab-trabajador.valor;
										}
										//El valor de la planilla no se debe modificar okr
										/*if(trabajador.existe=="S"){
											$.getJSON('modificarPlanilla.php',{idempresa: idEmpresa, idtrabajador: trabajador.id, periodo: periodo, valor: valorAporteTrab},function(resActPlanilla){
												if(resActPlanilla==0){
													alert("No se pudo Actualizar la planilla del trabajador "+trabajador.id);												
												}							
								 			});
										}*/										
										
										arregloDetallesDevolucion[arregloDetallesDevolucion.length] = {'id': idDetalleGrabado, 'idpersona': trabajador.id, 'documento': trabajador.numdoc, 'nombre': trabajador.nombre, 'valor': trabajador.valor};
									}else{
										arregloErroresDetalles[arregloErroresDetalles.length] = {'idpersona': trabajador.id, 'documento': trabajador.numdoc, 'nombre': trabajador.nombre, 'valor': trabajador.valor};
									}
									if((arregloDetallesDevolucion.length + arregloErroresDetalles.length) == arregloTrabajadores.length)
										mostrarMensajeFinal(idDevolucion,valorDevolver,arregloDetallesDevolucion,arregloErroresDetalles);
								}
							});
						});
						nuevo=1;
					}else{
						MENSAJE("No se pudo grabar la devoluci&oacute;n. Verifique los datos.");
						return false;
					}
				}
			});
		}
		
		
	}
}

function limpiarxPeriodo(){
	valorPagado = 0;
	valorCaja=null;
	valorNomina = null;
	indicador = null;
	sena = null;
	icbf = null;
	planilla = null;
	idApoDev = null;
	idTdDevolucion = null;
	arregloTrabajadores = [];
	persona=null;
	
	$("#TrabExisten").val('S');	
	$("#numDoc").val('');
	$("#tblTrabajadores tbody").empty();
	$("#tblAportes tbody").empty();
	$("#td_total_devolver_trab").empty();
	$("#tdTrabajador").empty();	
}

function buscarAportesPeriodo(){
	if(idEmpresa==null){
		alert("Digite primero el Nit de la empresa");
		$("#periodo").val('');
		$("#txtNumero").focus();
		return false;
	}
	
	//Empresas Ley 1429
	if(claseaportante==2875){
		if($("#Combindicador").val()==0){
			alert("Seleccione el indicador del aporte");			
			$("#Combindicador").focus();
			return false;
		}
	}
	//------------------
	
	$("#tblAportes tbody").empty();
	$.ajax({
		type:"POST", 
		url: "buscarAportesDePeriodoPorNit.php",
		data: {idEmpresa: idEmpresa, periodo: $("#periodo").val()},
		async:false,
		dataType: "json",
		success:function(aportes){
			if(aportes.length > 0){
				$.each(aportes,function(indData,aporte){
					
					var strTr = "<tr>" +
							"<td><input type='radio' name='opAporte' value='"+ aporte.idaporte +"' id='opAporte"+ indData +"' /></td>" +
							"<td style='text-align:center' id='td_planilla" + indData + "'>"+ aporte.planilla +"</td>" +
							"<td style='text-align:center'>"+ aporte.numerorecibo +"</td>" +
							"<td style='text-align:center'>"+ $("#periodo").val() +"</td><td style='text-align:center'>"+ aporte.fechapago +"</td>" +
							"<td style='text-align:right' id='tdValorAporte"+ indData +"'>$ "+ formatMoneda(aporte.valoraporte) +"</td>" +
							"<td style='text-align:right' id='td_dev_"+ indData +"'>$ "+ formatMoneda( aporte.valordevolucion ) + " </td>" +
							"<td style='text-align:right' id='td_apor_dev_"+ indData +"'>$ "+ formatMoneda( (parseInt(aporte.valoraporte) - parseInt(aporte.valordevolucion) ).toString() ) +" </td>" +
							"<td style='text-align:right'>$ "+ formatMoneda(aporte.valornomina) +"</td>" +
							"<td style='text-align:right'>$ <span id='span_valor_pagado_"+ indData +"'>"+ formatMoneda(aporte.valorpagado) +"</span></td>" +
							"<td style='text-align:right'>$ "+ formatMoneda(aporte.valortrabajador) +"" +
									"<span id='span_valor_caja_"+ indData +"' style='display:none'>"+ aporte.caja +"</span>" +
									"<span id='span_valor_nomina_"+ indData +"' style='display:none'>"+ aporte.valornomina +"</span>" +
									"<span id='span_indicador_"+ indData +"' style='display:none'>"+ aporte.indicador +"</span>" +
									"<span id='span_sena_"+ indData +"' style='display:none'>"+ aporte.sena +"</span>" +
									"<span id='span_icbf_"+ indData +"' style='display:none'>"+ aporte.icbf +"</span>" +
							"</td>" +
							"<td style='text-align:center'>"+ aporte.ajuste +"</td><td style='text-align:center'>"+ aporte.trabajadores +"</td></tr>";
					$("#tblAportes tbody").append(strTr);
					$("#opAporte"+indData).bind('click',function(){
						var acumula = 0;
						$.each( arregloTrabajadores, function(i,row){
							acumula += parseInt( row.valor );
						});
						idApoDev = "#td_apor_dev_" + $(this).get(0).id.substring(8);
						idTdDevolucion = "#td_dev_" + $(this).get(0).id.substring(8);
						
						calcularAporteDevolucion( "S", acumula );
						valorPagado = 0;
						valorCaja = null;
						valorNomina = null;
						indicador = null;
						sena = null;
						icbf = null;
						planilla = null;
						arregloTrabajadores = [];
						persona = null;

						$("#TrabExisten").val('S');	
						$("#numDoc").val('');
						$("#tdTrabajador").empty();
						$("#tblTrabajadores tbody").empty();
						$("#td_total_devolver_trab").empty();
						
						valorPagado = parseInt(formatNormal($("#span_valor_pagado_"+parseInt($(this).attr('id').replace('opAporte',''))).html()));
						valorCaja = parseInt($("#span_valor_caja_"+parseInt($(this).attr('id').replace('opAporte',''))).html());						
						valorNomina = parseInt($("#span_valor_nomina_"+parseInt($(this).attr('id').replace('opAporte',''))).html());
						/*
						// Empresas Ley 1429
						if(claseaportante==2875){
							var indicadorCombo = $("#Combindicador").val();
							
							if(indicadorCombo == 4268 ){
								indicador = 0;
							} else if (indicadorCombo == 4267){
								indicador = 1;
							} else if (indicadorCombo== 4266){
								indicador = 2;
							} else if (indicadorCombo == 4265){
								indicador = 3;
							} else if (indicadorCombo == 108){
								indicador = 4;
							}
							
							//alert("Indicador =" +indicador + "El indicador combo =" +indicadorCombo);
						// ------------------
						}else{}*/
						indicador = parseFloat($("#span_indicador_"+parseInt($(this).attr('id').replace('opAporte',''))).html());
						sena = parseInt($("#span_sena_"+parseInt($(this).attr('id').replace('opAporte',''))).html());
						icbf = parseInt($("#span_icbf_"+parseInt($(this).attr('id').replace('opAporte',''))).html());
						planilla = parseInt($("#td_planilla"+parseInt($(this).attr('id').replace('opAporte',''))).html());
					});
				});
				if(aportes.length == 1)
					$("input[id^=opAporte]").trigger("click");
				
				$("#valor").focus();
			}else{				
				$("#valor").focus();
				alert("No se encontraron aportes del periodo digitado.");
				$("#periodo").val('');
				return false;
			}
		}
	});
}

function mostrarMensajeFinal(idDevolucion,valor,devolucionesOk,devolucionesFail){
	var mensaje = "";
	var errores = "";
	
	if(devolucionesOk.length > 0)
		mensaje += "La devoluci&oacute;n fue grabada correctamente (<strong>"+ idDevolucion +"</strong>) por valor de $ <strong>"+ valor +"</strong>.<br/>" +
				   "<h3>Detalles</h3> Se hizo devoluci&oacute;n para <strong>"+ devolucionesOk.length +"</strong> trabajadores:<ul>";
	if(devolucionesFail.length > 0)
		errores += "<h3>Errores</h3> No se agregaron a la devoluci&oacute;n <strong>"+ devolucionesOk.length +"</strong> trabajadores:<ul>";
	
	$.each(devolucionesOk,function(i,devolucion){
		mensaje += "<li>" + devolucion.documento +" - "+ devolucion.nombre +" -- $ "+ devolucion.valor +"</li>";
	});
	
	$.each(devolucionesFail,function(i,devolucion){
		errores += "<li>" + devolucion.documento +" - "+ devolucion.nombre +" -- $ "+ devolucion.valor +"</li>";
	});
	
	if(errores != "")
		errores += "</ul>";
	
	if(devolucionesOk.length > 0)
		mensaje += "</ul>";
	
	MENSAJElocal(mensaje +"<br/>"+ errores);

	return true;
}

/*function buscarPlanilla(){
	var v0=$("#txtNumero").val();
	var v1=$("#periodo").val();
	var v2=$("#tipoDoc").val();
	var v3=$("#numDoc").val();
	
	$.ajax({
		url: 'buscarPlanilla.php',
		type: "POST",
		data: {v0:v0,v1:v1,v2:v2,v3:v3,v4:planilla},
		async: false,
		dataType: 'json',
		success: function(data){
			if(data==0){
				alert("No se encontro planilla para modificar");
				$("#numDoc").val('');
				$("#tdTrabajador").empty();
				return false;
			}

			$.each(data,function(i,fila){
				if(!fila.idpersona>0){
					persona=fila;
					return false;
				}
				
				var idAporte = $("[name=opAporte]:checked").val();
				var seguir=true;
				
				$.ajax({
					url: URL+'phpComunes/buscarDevolucion.php',
					type: "POST",
					data: {v0:idAporte,v1:fila.idpersona},
					async: false,
					dataType: 'json',
					success: function(devoluciones){
						if(!devoluciones == 0){											
							alert("La persona tiene una devolucion de $"+devoluciones[0].valor+" realizada el "+devoluciones[0].fechasistema)
							$("#tdTrabajador").empty();
							$("#numDoc").val('');
							$("#numDoc").focus();
							seguir=false;										
						}
					}
				});
				
				if(seguir==false)
					return false;
					
				var nombre = fila.papellido +" "+ fila.sapellido +" "+ fila.pnombre +" "+ fila.snombre;
				var idPersona = fila.idpersona;
				var valorAporteTrab = fila.valoraporte;
				var identificacion = $("#numDoc").val();
				var existe='S';
				
				if(!(parseInt(fila.idpersona2)>0))
					existe='N';
				
				var str = idPersona +" - "+ identificacion +" "+ nombre +"<input type='hidden' id='idtrabajador' name='idtrabajador' value='"+ idPersona +"' /> <input type='hidden' id='valorAporteTrab' name='valorAporteTrab' value='"+ valorAporteTrab +"' /> <input type='hidden' id='TrabExiste' name='TrabExiste' value='"+ existe +"' /> <input type='hidden' id='nombretrabajador' name='nombretrabajador' value='"+ nombre +"' /> <input type='hidden' id='identificacion' name='identificacion' value='"+ identificacion +"' />";
				
				var agregado=false;
				
				$.each(arregloTrabajadores,function(ind,trabajador){
					if(trabajador.id == idPersona){
						MENSAJE("Ya ha agregado a esa persona en la devoluci&oacute;n.");
						agregado = true;
					}
				});
				
				if(!agregado){					
					$("#tdTrabajador").html(str);
				} else {
					$("#numDoc").val('');
					$("#tdTrabajador").empty();
					return false;
				}				
			});//fin each	for
			return true;
		}//success
	});
}*/
	
function persona_simple2(obj){		
	var num=obj.value;	
	
	$("#dialog-persona").dialog({
		height: 225,
		width: 650,
		draggable:false,
		modal: true,
		open: function(){
			$('#dialog-persona').html('');
			$.ajax({
				url: URL+'phpComunes/personaSimple.php',
				async: false,
				success: function(data){
					$('#dialog-persona').html(data);
					$("#fechaNaceDialogPersona").datepicker({
						changeMonth: true,
						changeYear: true,
						dateFormat: 'yy-mm-dd',
						maxDate: '+0D',
						minDate: '-120Y'
					});
					var anoActual = new Date();
					var strYearRange = anoActual.getFullYear()-120 +":"+ anoActual.getFullYear();
					$("#fechaNaceDialogPersona").datepicker("option", "yearRange", strYearRange);
				}
			});
			$('#dialog-persona select,#dialog-persona input[type=text]').val('');
			$("#txtNumeroP").val(num);
			$('#pNombre').focus();
		},
		buttons: {
			'Guardar datos': function() {
				var campo0=$("#tDocumento").val();
				var campo1=$.trim($("#txtNumeroP").val());
				var campo4=$.trim($("#spNombre").val());
				var campo5=$.trim($("#ssNombre").val());
				var campo2=$.trim($("#spApellido").val());
				var campo3=$.trim($("#ssApellido").val());
				var campo6=new_nombreCorto(campo4, campo5, campo2, campo3);
				var fechaNace = $("#fechaNaceDialogPersona").val();
				
				if(campo1==""){
					alert("Falta numero de identificaci\u00F3n");
					return false;
				}
				if(campo4==""){
					alert("Falta primer nombre");
					return false;
				}
				if(campo2==""){
					alert("Falta primer apellido");
					return false;
				}
						
				var regexpFechas = /^((\d{4}))[\/|\-](|(0[1-9])|(1[0-2]))[\/|\-]((0[1-9])|(1\d)|(2\d)|(3[0-1]))$/;
				if(!fechaNace.match(regexpFechas)){
					//alert("Verifique la fecha de nacimiento. (AAAA-MM-DD).");
					//return false;
					fechaNace='1990-01-01';
				}
						
				$.ajax({
					url: URL+'phpComunes/pdo.insert.persona.php',
					type: "POST",
					data: {v0:campo0,v2:campo2,v3:campo3,v4:campo4,v5:campo5,v1:campo1,v19:campo6,v8:fechaNace},
					async: false,
					success: function(datos){
						var e = parseInt(datos);
						if(e>0){							
							$('#dialog-persona select,#dialog-persona input[type=text]').val('');							
							
							alert("La persona fue creada satisfactoriamente. (ID."+ datos +")");
							
							var str = e +" - "+ num +" "+ campo6 +"<input type='hidden' id='idtrabajador' name='idtrabajador' value='"+ e +"' /> <input type='hidden' id='valorAporteTrab' name='valorAporteTrab' value='"+ persona.valoraporte +"' /> <input type='hidden' id='TrabExiste' name='TrabExiste' value='N' /> <input type='hidden' id='nombretrabajador' name='nombretrabajador' value='"+ campo6 +"' /> <input type='hidden' id='identificacion' name='identificacion' value='"+ num +"' />";
							
							var agregado=false;
							
							$.each(arregloTrabajadores,function(ind,trabajador){
								if(trabajador.id == e){
									MENSAJE("Ya ha agregado a esa persona en la devoluci&oacute;n.");
									agregado = true;
								}
							});
							
							if(!agregado){					
								$("#tdTrabajador").html(str);
							} else {
								$("#numDoc").val('');
								$("#tdTrabajador").empty();
								return false;
							}
							
							$(this).dialog('close');
						}
					}
				});
			}
		},
		close: function() {
				$(this).dialog("destroy");
				$("#caja").focus();
				 
		}
	});
}

function new_nombreCorto(pn,sn,pa,sa){
	var patron=new RegExp('[\u00F1|\u00D1]');
	for(i=1;i<=pn.length;i++){
		pn=pn.replace(patron,"&");
	}
	for(i=1;i<=sn.length;i++){
		sn=sn.replace(patron,"&");
	}
	for(i=1;i<=pa.length;i++){
		pa=pa.replace(patron,"&");
	}
	for(i=1;i<=sa.length;i++){
		sa=sa.replace(patron,"&");
	}
	txt=pn+" "+sn+" "+pa+" "+sa;
	txt=txt.toUpperCase();
	num=parseInt(txt.length);
	if(num>26){
		if(sn.length>0){
			sn=sn.slice(0,1);
			txt=pn+" "+sn+" "+pa+" "+sa;
			txt=txt.toUpperCase();
			num=parseInt(txt.length);
		}
		if(num>26)
		if(sa.length>0){
			sa=sa.slice(0,1);
			txt=pn+" "+sn+" "+pa+" "+sa;
			txt=txt.toUpperCase();
			num=parseInt(txt.length);
			}
		}
	if(num>26){
		txt=txt.substring(0, 26);
	}
	$("#tncorto").val(txt);
	return txt;
}

function MENSAJElocal(str){
	var dialog="<div title='MENSAJE SIGAS' id='info'></div>";
	$("#info").dialog("destroy").remove();
	$(dialog).appendTo("body");
	$("#info").append("<p>"+str+"</p>");
	$("#info").dialog({
		modal:true,
		resizable:false,
		width: 460,
		closeOnEscape:true,
		close: function(){
			if(confirm("Desea grabar otra devolucion \ncon la misma empresa?")==true){
				$("#periodo").val("").trigger("blur");
				nuevo=0;
			} else {
				camposLimpiar();
			}
		}
	});
} 

function formatMoneda(input){
	var num = input.replace(/\./g,'');
	if(!isNaN(num)){
		num = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1.');
		num = num.split('').reverse().join('').replace(/^[\.]/,'');
		return num;
	} else{ alert('Solo se permiten numeros');
		return 0;
	}
}

function formatNormal(input){
	var txt=input.replace(/\./g,'');
	
	//////////////////////////////////////////		Validaciones
	
	if(txt.length > 0){
		if(isNaN(txt))
			return 0;
		else{
			if(txt>0)
				return parseInt(txt);
			else
				return 0;
		}
	} else {
		return 0;
	}	
}