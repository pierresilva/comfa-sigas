// JavaScript Document
// autor Orlando Puentes A
// fecha mayo 20 de 2010
// objeto administraci\xf3n de los eventos del formulario de empresa.php
var nuevo=0;
var modificar=0;
var continuar=true;
var ccgerente=0;
var ccjefe=0;
var msg="";
var URL=src();
$(function(){

});
function validarCampos(op){
	var cadena="<ul>";

	if(document.forms[0].elements[2].value==0){
		cadena+="<li>Falta sector</li>";
	}
	/****************************************/
	if(document.forms[0].elements[3].value==0){
		cadena+="<li>Falta clase de sociedad</li>";
	}
	/*************************************************/
	if(document.forms[0].elements[4].value.length==0){
		cadena+="<li>Falta NIT</li>";
	}
	else{
		if(isNaN(document.forms[0].elements[4].value))
		cadena+="<li>El NIT debe ser num\u00E9rico!</li>";
		var long=document.forms[0].elements[4].value.length;
		if(long<5 || long>11)
		cadena+="<li>el NIT es de 5 a 11 d\xedgitos</li>";
	}
	/************************************************/
	if(document.forms[0].elements[5].value.length==0){
		cadena+="<li>Falta d\u00EDgito de verificaci\u00FAn</li>";
	}
	/************************************************/
	if(document.forms[0].elements[6].value==0){
		cadena+="<li>Falta tipo de documento</li>";
	}
	/***************************************************/
	if(document.forms[0].elements[7].value.length==0){
		cadena+="<li>Falta raz\u00F3n social</li>";
	}
	else{
		if(document.forms[0].elements[7].value.length<3)
		cadena+="<li>La raz\u00F3n social es muy corta!</li>";
	}
	/***************************************************/
	if(document.forms[0].elements[8].value.length==0){
		cadena+="<li>Falta nombre comercial</li>";
	}
	else{
		if(document.forms[0].elements[8].value.length<3)
		cadena+="<li>La raz\u00FAn social es muy corta!</li>";
	}
	/***************************************************/
	if(document.forms[0].elements[11].value.length==0){
		cadena+="<li>Falta direcci\u00F3n</li>";
	}
	else{
		if(document.forms[0].elements[11].value.length<10)
		cadena+="<li>La direccion es muy corta!</li>";
	}
	/***************************************************/
	if(document.forms[0].elements[9].value==0){
		cadena+="<li>Falta departamento</li>";
	}
	/***************************************************/
	if(document.forms[0].elements[10].value==0){
		cadena+="<li>Falta ciudad</li>";
	}
	/*************************************************/
	if(document.forms[0].elements[12].value.length==0){
		cadena+="<li>Falta el tel\u00E9fono</li>";
	}
	else{
		if(isNaN(document.forms[0].elements[12].value))
		cadena+="<li>El tel\u00E9fono debe ser num\u00E9rico!</li>";
		var long=document.forms[0].elements[12].value.length;
		if(long<7 || long>10)
		cadena+="<li>el tel\u00E9fono es de 7 a 10 d\u00EDgitos</li>";
	}
	/*************************************************/
	if(document.forms[0].elements[13].value.length>0){
		if(isNaN(document.forms[0].elements[13].value))
		cadena+="<li>El fax debe ser num\u00E9rico!</li>";
		var long=document.forms[0].elements[13].value.length;
		if(long<7)
		cadena+="<li>el fax es de 7 d\xedgitos</li>";
	}
	/************************************************
	
	14 www y 15 email
	
	*/
	/*************************************************/
	if(document.forms[0].elements[16].value.length==0){
		cadena+="<li>Falta identificaci\u00F3n del representante legal</li>";
	}
	else{
		if(isNaN(document.forms[0].elements[16].value))
		cadena+="<li>La identificaci\u00F3n debe ser num\u00E9rico!</li>";
		var long=document.forms[0].elements[16].value.length;
		if(long<5 || long>11)
		cadena+="<li>La identificaci\u00F3n es de 5 a 11 d\u00EDgitos</li>";
	}
	if(ccgerente==0){
		if(document.forms[0].elements[17].value.length==0){
			cadena+="<li>El primer nombre del representante es obligatorio</li>";
			}
		if(document.forms[0].elements[19].value.length==0){
			cadena+="<li>El primer apellido del representante es obligatorio</li>";
			}
		}
	/****************************************/
	if(document.forms[0].elements[26].value==0){
		cadena+="<li>Falta contratista</li>";
	}
	/****************************************/
	if(document.forms[0].elements[27].value==0){
		cadena+="<li>Falta colegio</li>";
	}
	/****************************************/
	if(document.forms[0].elements[28].value==0){
		cadena+="<li>Falta excento</li>";
	}
	/****************************************/
	if(document.forms[0].elements[29].value==0){
		cadena+="<li>Falta actividad</li>";
	}
	/****************************************/
	if(document.forms[0].elements[30].value==0){
		cadena+="<li>Falta indice de aportes</li>";
	}
	/****************************************/
	if(document.forms[0].elements[31].value==0){
		cadena+="<li>Falta asesor</li>";
	}
	/****************************************/
	if(document.forms[0].elements[32].value==0){
		cadena+="<li>Falta seccional</li>";
	}
	/****************************************/
	if(document.forms[0].elements[33].value==0){
		cadena+="<li>Falta estado</li>";
	}
/*	if(document.forms[0].elements[35].value==0){
		cadena+="<li>Falta fecha inicio aportes</li>";
	}*/
	
	/******************** FIN ****************/
	if(cadena.length>10){
		cadena+="</ul>";
		cadena="Hay errores en los datos, por favor revisar:"+cadena;
		document.getElementById('error').innerHTML=cadena;
		return;
	}
	else{
		cadena="Datos completos!";
		document.getElementById('error').innerHTML=cadena;
	}
	if(ccgerente==0){
		var l=document.forms[0].elements[16].value.length;
		if(l>3)
			guardarPersona(1);
		}
	if(ccjefe==0){
		var l=document.forms[0].elements[21].value.length;
		if(l>3)
			guardarPersona(2);
		}	
	buscarNitInforma();
}

function nuevoR(){
	nuevo=1;
	limpiarCampos();
	document.getElementById('observaciones').value="";
	msg="";
	document.forms[0].elements[1].value = fechaHoy();
	document.getElementById('error').innerHTML="";
	document.forms[0].elements[36].value=document.getElementById('usuario').value;
	var campo0=31;
	$('#lisRadicacion option').remove();
	$.post(URL+'phpComunes/buscarRadicaTipoID.php',{v0:campo0},function(datos){
    	var cmbRadicacion = $("#lisRadicacion");
	   	$(cmbRadicacion).html(datos);
	   	if($(cmbRadicacion).children().length==1){
			alert('Lo lamento NO hay radicaciones pendientes de grabar!');
			return false;
         }
	});
}

function buscarRadicacion(obj){
	$.getJSON('buscarRadicacion.php', {v0:obj.value}, function(datos){
		$.each(datos,function(i,fila){
			$("#tNit").val(fila.nit);
			$("#tNit2").val(fila.nit);
			$("#tIdRadicacion").val(fila.idtiporadicacion);
			$("#tFechaRadicacion").val(fila.fechasistema);
			$("#textfield18").val(fila.fecharadicacion);
			document.forms[0].tNit.onblur();
			//$("#tNit").onblur();
			buscarEmpresa();
		});
	})
}

function buscarEmpresa(){
	var v0=$("#tNit").val();
	
	}

function copiar(obj){
	
	$("#textfield2").val(obj.value); //nombre comercial
	}
	
function buscarNitInforma(){
	var campo0=document.forms[0].elements[4].value;
	$.ajax({
		url: 'buscarInforma.php',
		type: "POST",
		data: "submit=&v0="+campo0,
		success: function(datos){
			if(datos=='1'){
				msg+="El NIT ya existe en INFORMA WEB \r\n";
				guardarEmpresa();
				}
			else{
				guardarEmpresaInforma();
				}
		}
	});
	return false;
	}
		
function buscarPersona(objeto,op){
	if(nuevo==0){
		alert("Si es un registro nuevo haga click en NUEVO!!");
		return;
	}
	var numero=parseInt(objeto.value);
	if(isNaN(numero)) return;
	$.ajax({
		url: '../phpComunes/buscarPersona.php',
		cache: false,
		type: "GET",
		data: "submit=&v0="+numero,
		success: function(datos){
			if(op==0){
			if(datos==0){
				alert("No existe en nuestra base, registre los datos completos!");	
				document.getElementById('representante').innerHTML="No existe en nuestra base, registre los datos completos!";
				ccgerente=0;
				document.forms[0].elements[17].value="";
				document.forms[0].elements[18].value="";
				document.forms[0].elements[19].value="";
				document.forms[0].elements[20].value="";
				return false;
			}
				else{
				var result=datos.split(",");
				ccgerente=1;
				document.forms[0].elements[17].value=result[5];
				document.forms[0].elements[18].value=result[6];
				document.forms[0].elements[19].value=result[3];
				document.forms[0].elements[20].value=result[4];
			}
			}
			else{
			if(datos==0){
				alert("No existe en nuestra base, registre los datos completos!");	
				document.getElementById('personal').innerHTML="No existe en nuestra base, registre los datos completos!";
				ccjefe=0;
				document.forms[0].elements[22].value="";
				document.forms[0].elements[23].value="";
				document.forms[0].elements[24].value="";
				document.forms[0].elements[25].value="";
				return false;
			}
				else{
				var result=datos.split(",");
				ccjefe=1;
				document.forms[0].elements[22].value=result[5];
				document.forms[0].elements[23].value=result[6];
				document.forms[0].elements[24].value=result[3];
				document.forms[0].elements[25].value=result[4];
			}
				
				}
		}
	});
}

function guardarEmpresa(){
	var campo0=document.forms[0].elements[0].value;
	var campo1=document.forms[0].elements[1].value;
	var campo2=document.forms[0].elements[2].value;//sector
	var campo3=document.forms[0].elements[3].value;
	var campo4=document.forms[0].elements[4].value;
	var campo5=document.forms[0].elements[5].value;
	var campo6=document.forms[0].elements[6].value;
	var campo7=document.forms[0].elements[7].value;
	var campo8=document.forms[0].elements[8].value;
	var campo9=document.forms[0].elements[11].value;//direccion
	var campo10=document.forms[0].elements[9].value;//depto
	var campo11=document.forms[0].elements[10].value;//ciudad
	var campo12=document.forms[0].elements[12].value;
	var campo13=document.forms[0].elements[13].value;
	var campo14=document.forms[0].elements[14].value;
	var campo15=document.forms[0].elements[15].value;
	var campo16=document.forms[0].elements[16].value;
	var campo17=document.forms[0].elements[17].value;
	var campo18=document.forms[0].elements[18].value;
	var campo19=document.forms[0].elements[19].value;
	var campo20=document.forms[0].elements[20].value;
	var campo21=document.forms[0].elements[21].value;//cc idjefepersonal
	var campo22=document.forms[0].elements[22].value;
	var campo23=document.forms[0].elements[23].value;
	var campo24=document.forms[0].elements[24].value;
	var campo25=document.forms[0].elements[25].value;
	var campo26=document.forms[0].elements[26].value;
	var campo27=document.forms[0].elements[27].value;
	var campo28=document.forms[0].elements[28].value;
	var campo29=document.forms[0].elements[29].value;
	var campo30=document.forms[0].elements[30].value;
	var campo31=document.forms[0].elements[31].value;
	var campo32=document.forms[0].elements[32].value;
	var campo33=document.forms[0].elements[33].value;
	var campo34=document.forms[0].elements[34].value;
	var campo35=document.forms[0].elements[35].value;
	var campo36=document.forms[0].elements[36].value;
	var campo37=document.forms[0].elements[37].value;
	
	$.ajax({
		url: 'empresasG.php',
		type: "POST",
		data: "submit=&v0="+campo0+"&v1="+campo1+"&v2="+campo2+"&v3="+campo3+"&v4="+campo4+"&v5="+campo5+"&v6="+campo6+"&v7="+campo7+"&v8="+campo8+
		"&v9="+campo9+"&v10="+campo10+"&v11="+campo11+"&v12="+campo12+"&v13="+campo13+"&v14="+campo14+"&v15="+campo15+"&v16="+campo16+"&v17="+campo17+"&v18="+campo18+"&v19="+campo19+"&v20="+campo20+"&v21="+campo21+"&v22="+campo22+"&v23="+campo23+"&v24="+campo24+"&v25="+campo25+"&v26="+campo26+"&v27="+campo27+"&v28="+campo28+"&v29="+campo29+"&v30="+campo30+"&v31="+campo31+"&v32="+campo32+"&v33="+campo33+"&v34="+campo34+"&v35="+campo35+"&v36="+campo36+"&v37="+campo37,
		success: function(datos){
			if(datos=='1'){
				msg+="Se guard\u00F3 la empresa principal \r\n";
				guardarObservaciones();
				}
			else{
			alert(datos);
			}
		}
	});
	return false;
	}
	
function guardarEmpresaInforma(){
	var campo0 =document.forms[0].elements[4].value;
	var campo1 =document.forms[0].elements[7].value;
	var campo2 =document.forms[0].elements[9].value;
	var campo3 =document.forms[0].elements[12].value;
	var campo4 =document.forms[0].elements[11].value;
	var campo5 =document.forms[0].elements[16].value;
	var campo6 =document.forms[0].elements[15].value;
	var campo7 =document.forms[0].elements[13].value;
	var campo8 =document.forms[0].elements[10].value;
	$.ajax({
		url: 'empresasGInforma.php',
		type: "POST",
		data: "submit=&v0="+campo0+"&v1="+campo1+"&v2="+campo2+"&v3="+campo3+"&v4="+campo4+"&v5="+campo5+"&v6="+campo6+"&v7="+campo7+"&v8="+campo8,
		success: function(datos){
			if(datos==1){
				msg+="Se guard\u00F3 la Empresa en Informa Web \r\n";
				}
			else{
				msg+="NO se pudo guardar en Informa Web! \r\n";
				alert("No se pudo guardar la empresa en Informa WEB!, intente de nuevo");
				return false;
				}
			guardarEmpresa();
		}
	});
	return false;
	}
		
function guardarObservaciones(){
	var campo0=document.forms[0].elements[37].value;
	var campo1=document.forms[0].elements[4].value;
	var campo2=2;
	$.ajax({
		url: '../../clases/observaciones.php',
		type: "POST",
		data: "submit=&v0="+campo0+"&v1="+campo1+"&v2="+campo2,
		success: function(datos){
				if(datos==1){
				msg+="Se guardaron las observaciones \r\n";
				alert(msg)
				msg="";
				cerrarRadicacion();
				}
			else{
				msg+="NO se pudo guardar las Observaciones! \r\n";
				alert(msg)
				msg="";
				cerrarRadicacion();
				}
		}
	});
	return false;
	}
	
function guardarPersona(op){
	if(op==1){
		//gerente
		var campo0=document.forms[0].elements[16].value;
		var campo2=document.forms[0].elements[17].value;
		var campo3=document.forms[0].elements[18].value;
		var campo4=document.forms[0].elements[19].value;
		var campo5=document.forms[0].elements[20].value;
		var campo6=document.getElementById('usuario').value;
		}
	else{
		var campo0=document.forms[0].elements[21].value;
		var campo2=document.forms[0].elements[22].value;
		var campo3=document.forms[0].elements[23].value;
		var campo4=document.forms[0].elements[24].value;
		var campo5=document.forms[0].elements[25].value;
		var campo6=document.getElementById('usuario').value;
		}
	$.ajax({
		url: 'personaG.php',
		type: "POST",
		data: "submit=&v0="+campo0+"&v2="+campo2+"&v3="+campo3+"&v4="+campo4+"&v5="+campo5+"&v6="+campo6,
		success: function(datos){
				if(datos=='1'){
				msg+="Se guardaron datos persona \r\n";
				}
			else{
				alert(datos);
				return false;
				}
		}
	});
	return false;
	}	
	
function digito(obj)
{
	var vpri, x, y, z, i, nit1, dv1;
 	nit1=obj.value;
 	if (isNaN(nit1))
 	{
 	alert('El valor digitado no es un n\u00FAmero v\u00E1lido'+nit1);
 	} 
 	else {
 	vpri = new Array(16);
 	x=0 ; y=0 ; z=nit1.length ;
	 vpri[1]=3;
	 vpri[2]=7;
	 vpri[3]=13;
	 vpri[4]=17;
	 vpri[5]=19;
	 vpri[6]=23;
	 vpri[7]=29;
	 vpri[8]=37;
	 vpri[9]=41;
	 vpri[10]=43;
	 vpri[11]=47;
	 vpri[12]=53;
	 vpri[13]=59;
	 vpri[14]=67;
	 vpri[15]=71;
 	for(i=0 ; i<z ; i++) {
 	y=(nit1.substr(i,1));
 	x+=(y*vpri[z-i]);
 	}
 	y=x%11
 	if (y > 1)
 	{
 		dv1=11-y;
 	} else {
 		dv1=y;
 	}
 	document.forma.tDigito.value=dv1;
   }
  } 
function cerrarRadicacion(){
    var campo0=document.forms[0].elements[0].value;
	$.ajax({
		url: 'cerrarRadicacion.php',
		type: "POST",
		data: "submit=&v0="+campo0,
		success: function(datos){
			limpiarCampos();
			document.getElementById('observaciones').value="";
			alert(datos);
		}
	});
	return false;
	  }
/*
Ã� 	&Aacute; 	\u00C1
á 	&aacute; 	\u00E1
Ã‰ 	&Eacute; 	\u00C9
Ã© 	&eacute; 	\u00E9
Ã� 	&Iacute; 	\u00CD
Ã­ 	&iacute; 	\u00ED
Ó 	&Oacute; 	\u00D3
Ã³ 	&oacute; 	\u00F3
Ãš 	&Uacute; 	\u00DA
Ãº 	&uacute; 	\u00FA
Ãœ 	&Uuml; 	\u00DC
Ã¼ 	&uuml; 	\u00FC
á¹„ 	&Ntilde; 	\u00D1
Ã± 	&ntilde; 	\u00F1
*/  