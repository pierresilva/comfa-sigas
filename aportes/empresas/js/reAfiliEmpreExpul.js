$(function(){
	//Asignar dise�o al boton para guardar
	$("#btnGuardar").button();
	
	//Asignacion de las radicaciones al elemento radicacion
	$("#cmbIdRadicacion").html(comboRadicacion());
	
	//Asignar evento al elemento radicacion
	$("#cmbIdRadicacion").change(cargarDatosEmpresa);
	
	//Asignacion del datepicker al elemento de fecha afiliacion empresa
	datepickerC("FECHA","#txtFechaAfiliEmpre");
	
});

/********|t|i|**********
 * METODOS DE PETICION *
 * *********************/

/**
 * Metodo: Obtiene las radicaciones sin procesar
 * @author tiour
 * @returns HTML: Option para el combo
 */
function comboRadicacion(){
	var comboHtml = "";
	var idTipoRadicacion = 4258; //[4258][RE AFILIACION EMPRESA EXPULSADA]
	$.ajax({
		url:URL+'phpComunes/buscarRadicaTipoID.php',
		type:"POST",
		data:{v0:idTipoRadicacion},
		async:false,
		success:function(datos){
			comboHtml = datos;
		}
	});
	return comboHtml
}

/**
 * Metodo: Obtiene los datos de la radicacion
 * @author tiour
 * @param idRadicacion
 * @returns Object: Datos del radicado
 */
function datosRadicacion(idRadicacion){
	var resultado = null;
	$.ajax({
		url:'buscarRadicacion.php',
		type:"POST",
		data:{v0:idRadicacion},
		dataType:"json",
		async:false,
		success:function(datos){
			if(typeof datos == "object")
				resultado = datos[0];
		}
	});
	return resultado;
}

/**
 * Metodo: Obtiene los datos de la empresa y las desafiliaciones
 * @author tiour
 * @param nit
 * @returns Object: Datos de la empresa y las desafiliaciones
 */
function datosEmprDesaf(nit){
	var resultado = null;
	$.ajax({
		url:'buscarEmpreDesaf.php',
		type:"POST",
		data:{objDatos:{nit:nit}},
		dataType:"json",
		async:false,
		success:function(datos){
			resultado = datos;
		}
	});
	return resultado;
}

/**
 * @author tiour
 * @param idRadicacion
 */
function cerrarRadicacion(idRadicacion){
	$.ajax({
		url:URL+'phpComunes/cerrarRadicacion.php',
		type:"POST",
		data:{v0:idRadicacion},
		async:false,
		success: function(datos){
			alert(datos);	
		}
	});
}

/********|t|i|************
 * METODOS DE VALIDACION *
 * ***********************/

/**
 * Validar los elementos requeridos del formulario
 * @returns {Number}
 */
function validaCampoFormu(){
	var banderaError = 0;
	$( ".ui-state-error" ).removeClass( "ui-state-error" );
	$(".element-required").each(function(i,row){
		if( $( this ).val() == 0 ){
			$( this ).addClass( "ui-state-error" );
			banderaError++;
		}
	});
	return banderaError;
}


/********|t|i|**********
 * METODOS DE CONTROL  *
 * *********************/


function cargarDatosEmpresa(){
	var idRadicacion = parseInt($("#cmbIdRadicacion").val()),
		objDatosRadicacion = null,
		objDatosEmpresa = null;
	
	//Limpiar los campos de formulario
	nuevo();
	
	if(!idRadicacion)return false;
	
	//Obtener los datos de la radicacion
	objDatosRadicacion = datosRadicacion(idRadicacion);
	if(typeof objDatosRadicacion == "object"){
		//Obtener los datos de la empresa
		objDatosEmpresa = datosEmprDesaf(objDatosRadicacion.nit);
		if(typeof objDatosEmpresa == "object" && objDatosEmpresa.length>0){
			
			//Asignar los datos a los elementos del formulario
			$("#hidIdEmpresa").val(objDatosEmpresa[0].id_empresa);
			$("#lblNit").text(objDatosEmpresa[0].nit);
			$("#lblRazonSocial").text(objDatosEmpresa[0].razonsocial);
			$("#lblCausal").text(objDatosEmpresa[0].causa_estad_empre);
			$("#lblFechaEstado").text(objDatosEmpresa[0].fecha_estad_empre);
			$("#lblEstado").text(objDatosEmpresa[0].estado_empresa);
			
			var htmlFila = "",
				arrBandera = [],
				contDesafiliacionActiva = 0;
			
			//Formar las filas con la informacion de las desafiliaciones
			$.each(objDatosEmpresa,function(i,row){
				
				//Validar para obtener solo los datos de la desafiliacion
				if(arrBandera.indexOf(row.id_desafiliacion)<0){
					//Verificar si existen desafiliaciones activas
					if(row.estado_desafiliacion=="A" && row.cod_causal != "EMR" && row.cod_causal != "EMP")
						contDesafiliacionActiva++;
					
					htmlFila = "<tr><td><input type='radio' name='radDesafiliacion' value='"+row.id_desafiliacion+"'/></td>" +
							"<td class='clsIdDesafiliacion'>"+row.id_desafiliacion+"</td>" +
							"<td >"+row.causal+"</td>" +
							"<td >"+row.estado_desafiliacion+"</td>" +
							"<td>"+row.usuario+"</td>" +
							"<td>"+row.fecha_desafiliacion+"</td>" +
							"<td>"+row.fecha_estado+"</td></tr>";
				}
				arrBandera[arrBandera.length] = row.id_desafiliacion;
			});
			
			//Asignar las filas a la tabla de las desafiliaciones
			$("#tblDesafiliacion tbody").html(htmlFila);
			
			if(contDesafiliacionActiva>0){
				mostrarMensaje("La EMPRESA NO debe tener desafiliaciones ACTIVAS!!");
				desabBotonGuard();
			}
			
		}else{
			//Ocurrio un error al obtener los datos de la empresa
			alert("Ocurrio un ERROR al obtener los datos de la empresa y las desafiliaciones!!");
		}
	}else{
		//Ocurrio un error al obtener los datos de la radicacion
		alert("Ocurrio un ERROR al obtener los datos de la radicaci\xF3n");
	}
}

/**
 * Metodo: Guardar la re afiliacion de la empresa
 */
function guardar(){
	//Validar los datos del formulario
	if(validaCampoFormu()>0)return false;
	
	//Validar el datos de la desafiliacion
	var idDesafiliacion = $(":radio[name='radDesafiliacion']:checked").val();
	if(!idDesafiliacion){
		alert("Debe seleccionar la desafiliaci\xF3n!!");
		return false;
	}
	
	//Obtener los datos
	var objDatos = {
			id_desafiliacion:idDesafiliacion
			,id_empresa:$("#hidIdEmpresa").val()
			,fecha_afili_empre:$("#txtFechaAfiliEmpre").val()
			,observacion:$("#txaObservacion").val().trim()
		};
	var idRadicado = $("#cmbIdRadicacion").val();
	
	$.ajax({
		url:"guardarReAfiliEmpreExpul.php",
		type:"POST",
		data:{objDatos:objDatos},
		dataType:"json",
		async:false,
		beforeSend: function(objeto){
			dialogLoading('show');
		},
		complete: function(objeto, exito){
			dialogLoading('close');
		}, 
		success:function(datos){
			if(datos==0){
				alert("Error al desafiliar la empresa");
			}else{
				alert("La re afiliaci\xF3n se realizo correctamente");
				nuevo();
				//Cerrar la radicacion
				cerrarRadicacion(idRadicado);
				//Eliminar la radicacion del combo
				$("#cmbIdRadicacion :selected").remove();
			}
		}
	});
}

/**
 * Metodo: Asignar texto de mensaje al elemento del formulario
 * @author tiour
 * @param mensaje
 */
function mostrarMensaje(mensaje){
	$("#tdMensajeError").text(mensaje);
}

/**
 * Metodo: Habilita el elemento Type Button para guardar los datos
 * @author tiour
 */
function habilBotonGuard(){
	$("#btnGuardar").attr("disabled",false);
}

/**
 * Metodo: inabilita el elemento Type Button para guardar los datos
 * @author tiour
 */
function desabBotonGuard(){
	$("#btnGuardar").attr("disabled",true);
}

function nuevo(){
	limpiarCampos2("#tdMensajeError,#hidIdEmpresa,#lblNit,#lblRazonSocial,#lblCausal" +
			",#lblFechaEstado,#lblEstado,#tblDesafiliacion tbody,#txtFechaAfiliEmpre" +
			",#txaObservacion");
	habilBotonGuard();
}






