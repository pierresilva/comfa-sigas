<?php
	set_time_limit(0);
	ini_set("display_errors",'1');
	date_default_timezone_set("America/Bogota");
	$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
	include_once  $root;
	include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
	include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
	
	$db = IFXDbManejador::conectarDB();
	if($db->conexionID==null){
		$cadena = $db->error;
		echo msg_error($cadena);
		exit();
	}
	
	$sql = "SELECT
			a48.idempresa, a48.nit, rtrim(a48.razonsocial) as razonsocial,a48.direcorresp,a48.telefono,a48.email,a48.fechaafiliacion,a48.fechaaportes,a48.fechamatricula,a48.estado
			,a15.identificacion AS identificacionrepresentante,a15.pnombre+' '+ISNULL(a15.snombre,'')+' '+a15.papellido+' '+ISNULL(a15.sapellido,'') AS representante
			,a500.agencia
			,a91.detalledefinicion AS claseaportante
			,a91.detalledefinicion AS tipoaportante
			,(SELECT max(a11.periodo) FROM aportes011 a11 WHERE a11.idempresa=a48.idempresa) AS UltPeriodo
		FROM aportes048 a48
			LEFT JOIN aportes015 a15 ON a15.idpersona=a48.idrepresentante
			LEFT JOIN aportes500 a500 ON a500.codigo=a48.seccional
			LEFT JOIN aportes091 a91 ON a91.iddetalledef=a48.claseaportante
			LEFT JOIN aportes091 b91 ON b91.iddetalledef=a48.tipoaportante
		WHERE
			0=(SELECT count(*) FROM aportes016 a16 WHERE a16.idempresa=a48.idempresa)
			AND a48.estado<>'I'
		ORDER BY a48.nit";

	$rsDatos = $db->querySimple($sql);
	if($rsDatos){
		$arrDatos = array();
		while ($row = $rsDatos->fetch()){
			$arrDatos[] = $row;
		}
		echo json_encode($arrDatos);
	}else 
		echo 0;
?>