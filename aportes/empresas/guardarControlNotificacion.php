<?php 
	set_time_limit(0);
	ini_set("display_errors",'1');
	date_default_timezone_set("America/Bogota");
	$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
	include_once  $root;
	include_once $raiz.DIRECTORY_SEPARATOR.'aportes'.DIRECTORY_SEPARATOR.'empresas'.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'empresas.class.php';
	
	$usuario = $_SESSION["USUARIO"];
	$datos = $_POST["datos"];
	$objEmpresa = new Empresa();
	
	$datoEstadoNotif = array("id_estado"=>0,"id_notificacion"=>0,"fecha_estado"=>null);
	$datoEstadoNotif["id_estado"] = $datos["id_estado"];
	$datoEstadoNotif["fecha_estado"] = $datos["fecha_estado"];
	
	$banderaError = 0;
	
	$objEmpresa->inicioTransaccion();
	//Actualizar el estado de la notificacion
	foreach ($datos["arrDatosNotificacion"] as $rowDatos){
		$datoEstadoNotif["id_notificacion"] = $rowDatos["id_notificacion"];
		if($objEmpresa->actualizar_estado_notificacion($datoEstadoNotif)==0){
			$banderaError++;
			break;
		}
	}
	
	//Guardar Observacion Empresa
	if($banderaError==0){
		$rsObservacion = $objEmpresa->guardar_observacion(array("id_empresa"=>$datos["id_empresa"]
				,"observacion"=>$datos["observacion"]),$usuario);
		if($rsObservacion==0)
			$banderaError++;
	}
	
	if($banderaError==0){
		$objEmpresa->confirmarTransaccion();
		echo 1;
	}else{
		//Error
		$objEmpresa->cancelarTransaccion();
		echo 0;
	}
?>