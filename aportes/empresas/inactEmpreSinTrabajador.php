<?php
	set_time_limit(0);
	ini_set("display_errors",'1');
	date_default_timezone_set("America/Bogota");
	$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
	include_once  $root;
	include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
	include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
	include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR .'p.definiciones.class.php';
	
	$usuario=$_SESSION['USUARIO'];
	$fecver = date('Ymd h:i:s A',filectime('inactEmpreSinTrabajador.php'));
	
	$objClase=new Definiciones();
	$causal = "";
	$rsCausalInact=$objClase->mostrar_datos(65);
	while( ( $rowCausalInact=mssql_fetch_array($rsCausalInact) ) == true){
		if($rowCausalInact['iddetalledef']==4103)
			$causal = "<option value=".$rowCausalInact['iddetalledef'].">".$rowCausalInact['detalledefinicion']."</option>";
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<title>Inactivar Empresa</title>
		<link type="text/css" href="../../css/Estilos.css" rel="stylesheet"/>
		<link type="text/css" href="../../css/marco.css" rel="stylesheet"/>
		<link type="text/css" rel="stylesheet" href="../../css/css.1.8/jquery-ui-1.8.17.custom.css" />
		<script language="javascript" src="../../js/1.6/jquery-1.6.2.min.js"></script>
		<script language="javascript" src="../../js/1.6/jquery-ui-1.8.16.custom.min.js"></script>
		<script language="javascript" src="../../js/comunes.js"></script>
		<script language="javascript" src="js/inactEmpreSinTrabajador.js"></script>
		<style type="text/css">
			.classCampoCorto {background: none repeat scroll 0 0 #F7F7F7; border-style: hidden; border-width: 0; font-size: 11px; padding: 0;}
			.classTextError {color:#ff0000} 
		</style>
	</head>
	<body>
		<center>
			<br /><br />
			<table  border="0" cellspacing="0" width="90%" cellpadding="0">
				<tr>
					<td class="arriba_iz" >&nbsp;</td>
					<td class="arriba_ce" ><span class="letrablanca">::Inactivar empresas sin trabajadores::</span>
						<div style="text-align:right;float:right;">
							<?php echo 'Versi&oacute;n: ' . $fecver; ?>
						</div>
					</td>
					<td class="arriba_de" >&nbsp;</td>
				</tr>
				<tr>
					<td class="cuerpo_iz">&nbsp;</td>
					<td class="cuerpo_ce"><img src="../../imagenes/spacer.gif" alt="" width="2" height="1" /></td>
					<td class="cuerpo_de">&nbsp;</td>
				</tr>
				<tr>
					<td class="cuerpo_iz" >&nbsp;</td>
					<td class="cuerpo_ce" >								
						<table border="0" class="tablero" cellspacing="0" width="90%" align="center">									
							<thead>
								<tr><td colspan="15">&nbsp;</td></tr>
								<tr>			
									<td colspan="15">
										Nit:									
										<input type="text" name="txtNitBuscar" id="txtNitBuscar" class="box1"/>
									</td>
								</tr>
								<tr>			
									<td colspan="15">
										Periodo:
										<input type="text" name="txtPeriodoInicial" id="txtPeriodoInicial" class="box1"/>										
										<input type="text" name="txtPeriodoFinal" id="txtPeriodoFinal" class="box1"/>
									</td>
								</tr>
								<tr>			
									<td colspan="15">
										Estado:
										<select name="cmbEstado" id="cmbEstado" class="box1">
											<option value="">Seleccione</option>
											<option value="A">A</option>
											<option value="P">P</option>
											<option value="I">I</option>
										</select>
									</td>
								</tr>
								<tr>			
									<td colspan="15">
										<input type="button" name="btnBuscar" id="btnBuscar" value="Buscar"/>
										<input type="button" name="btnLimpiar" id="btnLimpiar" value="Limpiar"/>
									</td>
								</tr>
								<tr>
									<td id="tdMensaje" colspan="15" style="color: red; text-decoration: blink;"></td>
								</tr>
								<tr>
									<th>
										<img src="../../imagenes/chk1.png" id="imgChequear" style="cursor:pointer;"/>
						  				<input type="hidden" name="hidChequearTodo" id="hidChequearTodo" value=""/>
						  			</th>
									<th>Id Empresa</th>
						  			<th>Nit</th>
						  			<th>Raz&oacute;n Social</th>
						  			<th>Dir.Correspon</th>
						  			<th>Telefono</th>
						  			<th>Correo</th>
						  			<th>Representante - C.C</th>
						  			<th>Agencia</th> 
						  			<th>Estado</th> 
						  			<th>Fecha Afiliaci&oacute;n</th>
						  			<th>Fecha Inicio Aportes</th>
						  			<th>Ultimo Periodo Aportes</th>
						  			<th>Fecha Constituci&oacute;n</th>
						  			<th></th> 	
						  		</tr>
							</thead>
							<tbody id="tbodyDatosEmpresas">
							</tbody>
							<tfoot>
								<tr>
									<td colspan="15">&nbsp;</td>
								</tr>
								<tr>
									<td colspan="15" style="text-align: center;">
										Fecha Estado 
										<input type="text" name="txtFechaEstadoMasiva" id="txtFechaEstadoMasiva" readonly="readonly"/>
										Causal inactivacion
										<select name="cmbCausalInactivacionMasiva" id="cmbCausalInactivacionMasiva" class="boxmediano" disabled="disabled">
							    			<?php echo $causal;?>
							    		</select>
									</td>
								</tr>
								<tr>
									<td colspan="15" style="text-align: center;">
										<input type="button" name="btnInactivar" id="btnInactivar" value="Inactivar"/>
									</td>
								</tr>
							</tfoot>
				  		</table>
					</td>
					<td class="cuerpo_de" >&nbsp;</td>
				</tr>
				<tr>
					<td class="cuerpo_iz" >&nbsp;</td>
					<td class="cuerpo_ce" >&nbsp;</td>
					<td class="cuerpo_de" >&nbsp;</td>
				</tr>
				<tr>
					<td class="abajo_iz" >&nbsp;</td>
					<td class="abajo_ce" ></td>
					<td class="abajo_de" >&nbsp;</td>
				</tr>
			</table>
		</center>
		<div id="divInactivarEmpresa" title=":Inactivar Empresa:">
			<br/><br/><br/>
			<table border="0" class="tablero" width="90%" align="center" >
				<thead>
					<tr>
						<td colspan="4"><b>Datos de la empresa</b></td>
					</tr>
					<tr>
						<td width="15%">Id Empresa</td>
						<td id="tdIdEmpresa"></td>
						<td width="20%">Raz&oacute;n social</td>
						<td id="tdRazonSocial" width="70%"></td>
					</tr>
					<tr>
						<td>Nit</td>
						<td id="tdNit"></td>
						<td>Fecha estado</td>
						<td>
							<input type="text" name="txtFechaEstado" id="txtFechaEstado" readonly="readonly"/>
						</td>
					</tr>
					<tr>
						<td colspan="2">Causal Inactivaci&oacute;n</td>
						<td colspan="2">
							<select name="cmbCausalInactivacion" id="cmbCausalInactivacion" class="boxmediano" disabled="disabled">
				    			<?php echo $causal;?>
				    		</select>
						</td>
					</tr>
				</thead>			
			</table>
			<br/>
			<br/>
		</div>
		<!-- Div Mostras empresas no inactivas -->
		<div id="divEmpresasNoInactivas">
			<table border="0" class="tablero" width="90%" align="center" >
				<b>Empresas que no fueron inactivas</b>
				<thead>
					<tr>
						<th>Nit</th>
						<th>Raz&oacute;n social</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
		<!-- FORMULARIO OBSERVACIONES-->
		<div name="div-observaciones-tab" style="display:none" title="Observaciones de modificaci&oacute;n">
			<table class="tablero">
		 		<tr>
		   			<td>Usuario</td>
		   			<td colspan="3" >
		   				<input type="text" name="usuarioObs" id="usuarioObs" class="box1" disabled="disabled" value="<?php echo $_SESSION['USUARIO']?>" />
		   			</td>
		   		</tr>
		 		<tr>
		   			<td>Observaciones</td>
		   			<td colspan="3" >
		   				<textarea name="observacionGrupo" id="observacionGrupo" cols="45" rows="5" class="boxlargo"></textarea>
		   			</td>
		   		</tr>
			</table>
			<div class="ui-state-highlight ui-corner-all" style="margin: 9px auto; display:none;padding: 6px; text-align: center; border: 1px solid rgb(255, 255, 0); width:300px" id="rtaObservacion">
				<span class="ui-icon ui-icon-info" style="float: left; margin-right: 0.3em;"></span>Observaci&oacute;n guardada con &eacute;xito.
			</div>
		</div>
	</body>
</html>
