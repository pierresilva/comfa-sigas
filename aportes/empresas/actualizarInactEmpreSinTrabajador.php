<?php
	set_time_limit(0);
	ini_set("display_errors",'1');
	date_default_timezone_set("America/Bogota");
	$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
	include_once  $root;
	include_once $raiz.DIRECTORY_SEPARATOR.'aportes'.DIRECTORY_SEPARATOR.'empresas'.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'empresas.class.php';
	
	/*Los parametros deben ser un array con objetos {idEmpresa,fechaEstado,causal}*/
	$objDatos = $_REQUEST["objeto"];
	
	$objEmpresa = new Empresa();
	for($i=0,$hasta=count($objDatos);$i<$hasta;$i++){
		if($objEmpresa->inactivar_empresa($objDatos[$i]["idEmpresa"],$objDatos[$i]["fechaEstado"],$objDatos[$i]["causal"])==true){
			unset($objDatos[$i]);
		}
	}
	echo json_encode($objDatos);