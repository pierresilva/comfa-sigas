<!-- 
	Nota: Este formulario esta dise�ado para desafiliar
		las empresas por recidencia en el pago de los aportes
		parafiscales.
-->
<?php
	set_time_limit(0);
	ini_set("display_errors",'1');
	date_default_timezone_set("America/Bogota");
	$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
	include_once  $root;
	include_once $raiz. DIRECTORY_SEPARATOR .'clases'. DIRECTORY_SEPARATOR . 'p.definiciones.class.php';
	
	$usuario=$_SESSION['USUARIO'];
	$fecver = date('Ymd h:i:s A',filectime('desafiliarEmpresa.php'));
	
	//Objeto de la definicion
	$objDefiniciones = new Definiciones();
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<title>Desafiliar Empresa</title>
		<link type="text/css" href="../../css/Estilos.css" rel="stylesheet"/>
		<link type="text/css" href="../../css/marco.css" rel="stylesheet"/>
		<link type="text/css" rel="stylesheet" href="../../css/css.1.8/jquery-ui-1.8.17.custom.css" />
		<script language="javascript" src="../../js/1.6/jquery-1.6.2.min.js"></script>
		<script language="javascript" src="../../js/1.6/jquery-ui-1.8.16.custom.min.js"></script>
		<script language="javascript" src="../../js/comunes.js"></script>
		<script language="javascript" src="js/desafiliarEmpresa.js"></script>
		<script type="text/javascript">
	$(function() {
		$('#txtFecha').datepicker({
			changeMonth: true,
			changeYear: true,
			maxDate: "+0D",
			onClose: function(dateText, inst) {
				$("#txaObservacion").focus();
			}
		});
		var anoActual = new Date();
		var strYearRange = anoActual.getFullYear()-120 +":"+ anoActual.getFullYear();
		$("#txtFecha").datepicker( "option", "yearRange", strYearRange );

	});
</script>
		<style type="text/css">
			.classCampoCorto {background: none repeat scroll 0 0 #F7F7F7; border-style: hidden; border-width: 0; font-size: 11px; padding: 0;}
			.classTextError {color:#ff0000}
		</style>
	</head>
	<body>
		<center>
			<br /><br />
			<table width="70%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="arriba_iz" >&nbsp;</td>
					<td class="arriba_ce" ><span class="letrablanca">::Desafiliar Empresa::</span>
						<div style="text-align:right;float:right;">
							<?php echo 'Versi&oacute;n: ' . $fecver; ?>
						</div>
					</td>
					<td class="arriba_de" >&nbsp;</td>
				</tr>
				<tr>
					<td class="cuerpo_iz">&nbsp;</td>
					<td class="cuerpo_ce"><img src="../../imagenes/spacer.gif" alt="" width="2" height="1" /></td>
					<td class="cuerpo_de">&nbsp;</td>
				</tr>
				<tr>
					<td class="cuerpo_iz" >&nbsp;</td>
					<td class="cuerpo_ce" >
						<table border="0" class="tablero" cellspacing="0" width="90%" style="margin:0 auto;">
							<tr>
						    	<td width="20%" style="text-align:right;">
						    		Nit:
						    		<input type="hidden" name="hidIdEmpresa" id="hidIdEmpresa" class="element-required"/>
						    	</td>
						    	<td >
						    		<input type="text" name="txtNit" id="txtNit" class="box1 element-required"/>
						    		<span class="clsObligatorio">&nbsp;&nbsp;&nbsp;</span>
						    		&nbsp; <label id="lblRazonSocial" >&nbsp;</label>
						    	</td>
						  	</tr>
						  	<tr>
							    <td width="100" style="text-align:right;">
						    		Causal desafiliaci&oacute;n:
						    	</td>
						    	<td>
						    		<select name="cmbCausalDesafiliacion" id="cmbCausalDesafiliacion" class="box1 element-required">
						    			<option value="">Seleccione</option>
						    			<?php
								        	$consulta=$objDefiniciones->mostrar_datos(70);
								        	while($row=mssql_fetch_array($consulta)){
								       			echo "<option value=".$row['iddetalledef']." title=".$row['codigo'].">".$row['detalledefinicion']."</option>";
								           	}
						    			?>
						    		</select>
						    		<span class="clsObligatorio">&nbsp;&nbsp;&nbsp;</span>
						    	</td>
							</tr>
						  	<tr>
						    	<td colspan="2" id="tdMensajeError" class="classTextError"></td>
						  	</tr>
						  	<tr>
						    	<td colspan="2"><b>Periodos Suspendidos</b></td>
						  	</tr>
					  		<tr>
					  			<td colspan="2">
									<table border="0" class="tablero" cellspacing="0" width="40%" id="tabPeriodos" style="display: block; margin: 0 auto;"> 
										<thead width="100%">
									  		<tr>
									  			<th width="10%">Id Suspensi&oacute;n</th>
									  			<th width="10%">Periodo</th>
									  			<th width="8%">Liquidado</th>
									  			<th width="10%">Fecha Suspensi&oacute;n</th>
									  		</tr>
									  	</thead>
									  	<tbody id="tBody" width="100%">		
									  	</tbody>
									</table>
								</td>
							</tr>
							<tr><td colspan="2"><!--  <b>Datos de la desafiliaci&oacute;n</b>--></td></tr>
							<tr>
						    	<td width="20%" style="text-align:right;">
						    		Estado Empresa:
						    	</td>
						    	<td>
						    		<select name="cmbEstadoEmpresa" id="cmbEstadoEmpresa" class="box1 element-required">
						    			<option value="">Seleccione</option>
						    			<option value="A">Activa</option>
						    			<option value="I">Inactiva</option>
						    		</select>
						    		<span class="clsObligatorio">&nbsp;&nbsp;&nbsp;</span>
						    		<label id="lblMensajeEstado">&nbsp;</label>
						    	</td>
						  	</tr>
						  	<tr>
								<td style="text-align:right;">Fecha Desafiliaci&oacute;n:</td>
								<td><input name="txtFecha" type="text" class="box1" id="txtFecha" value=""/>
								  <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
								</tr>
							<tr >
								<td style="text-align:right;">Observaci&oacute;n:</td>
								<td>
									<textarea name="txaObservacion" id="txaObservacion" cols="80" rows="3" maxlength="250" class="boxlargo element-required"></textarea>
									<span class="clsObligatorio">&nbsp;&nbsp;&nbsp;</span>
						    	</td>
							</tr>
							<tr>
								<td style="text-align:center;" colspan="2">
									<input type="button" name="btnDesafiliar" id="btnDesafiliar" value="Desafiliar" onclick="desafiliar();"/>
								</td>
							</tr>
					  	</table>
					</td>
					<td class="cuerpo_de" >&nbsp;</td>
				</tr>
				<tr>
					<td class="cuerpo_iz" >&nbsp;</td>
					<td class="cuerpo_ce" >&nbsp;</td>
					<td class="cuerpo_de" >&nbsp;</td>
				</tr>
				<tr>
					<td class="abajo_iz" >&nbsp;</td>
					<td class="abajo_ce" ></td>
					<td class="abajo_de" >&nbsp;</td>
				</tr>
			</table>
		</center>
	</body>
</html>
