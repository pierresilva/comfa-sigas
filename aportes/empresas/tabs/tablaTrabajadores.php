<?php
/* autor:       orlando puentes
 * fecha:       06/07/2010
 * objetivo:     
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;



$idemp=$_POST['v0'];
include_once $raiz . DIRECTORY_SEPARATOR.'aportes'.DIRECTORY_SEPARATOR.'trabajadores'.DIRECTORY_SEPARATOR.'clases'. DIRECTORY_SEPARATOR.'trabajadores.class.php';
$objTrabajador = new Trabajador;
$consulta = $objTrabajador->buscar_trabajadores_sucursal($idemp);
?>
<head>
  <title>TRABAJADORES EMPRESA</title>
<script type="text/javascript" src="../../js/script.js"></script>
<script type="text/javascript">
  var sorter = new TINY.table.sorter("sorter");
	sorter.head = "head";
	sorter.asc = "asc";
	sorter.desc = "desc";
	sorter.even = "evenrow";
	sorter.odd = "oddrow";
	sorter.evensel = "evenselected";
	sorter.oddsel = "oddselected";
	sorter.paginate = true;
	sorter.pagesize = (5);
	sorter.currentid = "currentpage2";
	sorter.limitid = "pagelimit2";
	sorter.init("table2",1);
  </script>
 
 </head>
<body>
<h4>Trabajadores de la empresa</h4>
<table  cellpadding="0" cellspacing="0" border="0" id="table2" class="sortable" width="90%">
  <thead>
  <tr>
    <th><h3>Num</h3></th>
    <th><h3>Identi</h3></th>
    <th><h3>Tipo doc</h3></th>
    <th><h3>Nombres</h3></th>
    <th><h3>fec Ingreso</h3></th>
    <th><h3>Salario</h3></th>
    <th><h3>Estado Afiliaci&oacute;n</h3></th>
  </tr>
  </thead>
  <tbody>
<?php 
$cont=1;
while($row=mssql_fetch_array($consulta)){
	$nom=$row['pnombre']." ".$row['snombre']." ".$row['papellido']." ".$row['sapellido'];
?>	
	<tr>
    <td><?php echo $cont; ?>&nbsp;</td>
    <td style="text-align:right;"><?php echo $row['identificacion']; ?>&nbsp;</td>
    <td ><?php echo $row['detalledefinicion'] ?>&nbsp;</td>
    <td><?php echo $nom; ?>&nbsp;</td>
    <td><?php echo $row['fechaingreso']; ?>&nbsp;</td>
    <td style="text-align:right;"><label style="text-align:right"> <?php echo number_format($row['salario']); ?></label></td>
    <td style="text-align:center;"><?php echo $row['estado_afiliacion']; ?>&nbsp;</td>
  </tr>
<?php $cont++;} ?> 
</tbody>
</table>
<div id="controls">
		<div id="perpage">
			<select onChange="sorter.size(this.value)">
			<option value="5" selected="selected">5</option>
				<option value="10" >10</option>
				<option value="20">20</option>
				<option value="50">50</option>
				<option value="100">100</option>
			</select>
			<label>Registros Por P&aacute;gina</label>
		</div>
		<div id="navigation">
			<img src="../../imagenes/imagesSorter/first.gif" width="16" height="16" alt="first Page" onClick="sorter.move(-1,true)" />
			<img src="../../imagenes/imagesSorter/previous.gif" width="16" height="16" alt="first Page" onClick="sorter.move(-1)" />
			<img src="../../imagenes/imagesSorter/next.gif" width="16" height="16" alt="first Page" onClick="sorter.move(1)" />
			<img src="../../imagenes/imagesSorter/last.gif" width="16" height="16" alt="Last Page" onClick="sorter.move(1,true)" />
		</div>
		<div id="text">P&aacute;gina <label id="currentpage2"></label> de <label id="pagelimit2"></label></div>
	</div>
    

</body>
