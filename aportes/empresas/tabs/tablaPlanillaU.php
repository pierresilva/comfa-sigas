<?php
/* autor:       orlando puentes
 * fecha:       06/07/2010
 * objetivo:    
 */
 
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;


$idemp=$_POST['v0'];
$per=$_POST['v1'];
include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'p.planilla.unica.class.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';
$objPlanilla = new Planilla;
$consulta = $objPlanilla->buscar_planillas($idemp,$per);
?>
<head>
  <title>PLANILLA UNICA</title>
  <link href="<?php echo URL_PORTAL; ?>css/sorterStyle.css" rel="stylesheet" type="text/css" media="screen" />
<script type="text/javascript" src="../../js/script.js"></script>
<script type="text/javascript">
  var sorter = new TINY.table.sorter("sorter");
	sorter.head = "head";
	sorter.asc = "asc";
	sorter.desc = "desc";
	sorter.even = "evenrow";
	sorter.odd = "oddrow";
	sorter.evensel = "evenselected";
	sorter.oddsel = "oddselected";
	sorter.paginate = true;
	sorter.pagesize = (5);
	sorter.currentid = "currentpagePU";
	sorter.limitid = "pagelimitPU";
	sorter.init("tablePU",1);
  </script>
 
 </head>
<body>

<h4>Planilla &Uacute;nica</h4>
<table  cellpadding="0" cellspacing="0" border="0" id="tablePU" class="sortable" width="100%"  >
  <thead>
  <tr >
    <th width="3"><h4><strong>Plani</strong></h4></th>
    <th width="3"><h4><strong>N°</strong></h4></th>
    <th width="3"><h4><strong>Trbj</strong></h4></th>
    <th width="3"><h4><strong>Apor</strong></h4></th>
    <th width="3"><h4><strong>Dev</strong></h4></th>
    <th width="3"><h4><strong>Bas</strong></h4></th>
    <th width="3"><h4><strong>IngBas</strong></h4></th>
    <th width="3"><h4><strong>Días</strong></h4></th>
    <th width="3"><h4><strong>I</strong></h4></th>
    <th width="3"><h4><strong>R</strong></h4></th>
    <th width="3"><h4><strong>VT</strong></h4></th>
    <th width="3"><h4><strong>VP</strong></h4></th>
    <th width="3"><h4><strong>ST</strong></h4></th>
    <th width="3"><h4><strong>IT</strong></h4></th>
    <th width="3"><h4><strong>LM</strong></h4></th>
    <th width="3"><h4><strong>V</strong></h4></th>
    <th width="3"><h4><strong>ITA</strong></h4></th>
    <th width="3"><h4><strong>TC</strong></h4></th>
    <th width="3"><h4><strong>Per</strong></h4></th>
    <th width="3"><h4><strong>C</strong></h4></th>
    <th width="3"><h4><strong>Hor</strong></h4></th>
    <th width="3"><h4><strong>Proc</strong></h4></th>
    <th width="3"><h4><strong>Usu</strong></h4></th>
    <th width="3"><h4><strong>FSis</strong></h4></th>
    <th width="3"><h4><strong>Modi</strong></h4></th>
    <th width="3"><h4><strong>FecMod</strong></h4></th>
    </tr>
  </thead>
  <tbody>
<?php 
$cont=0;
$valoraporte = 0;
$devolucion = 0;
$salariobasico = 0;
$ingresobase = 0;
while($row=mssql_fetch_array($consulta)){
	$nom=$row['pnombre']." ".$row['papellido'];
?>	
	<tr>
     <td align="right" style="font-size:9px"><?php echo $row['planilla']; ?>&nbsp;</td>
    <td style="font-size:9px"><?php echo $row['identificacion'] ?></td>
    <td style="font-size:9px"><?php echo $nom; ?>&nbsp;</td>
    <td align="right" style="font-size:9px"><?php echo number_format($row['valoraporte']); ?></td>
    <td align="right" style="font-size:9px"><?php echo number_format($row['devolucion']); ?></td>
    <td align="right" style="font-size:9px"><?php echo number_format($row['salariobasico']); ?></td>
    <td align="right" style="font-size:9px"><?php echo number_format($row['ingresobase']); ?></td>
    <td align="right" style="font-size:9px"><?php echo $row['diascotizados']; ?></td>
    <td align="right" style="font-size:9px"><?php echo $row['ingreso']; ?></td>
    <td align="right" style="font-size:9px"><?php echo $row['retiro']; ?></td>
    <td align="right" style="font-size:9px"><?php echo $row['var_tra_salario']; ?></td>
    <td align="right" style="font-size:9px"><?php echo $row['var_per_salario']; ?></td>
    <td align="right" style="font-size:9px"><?php echo $row['sus_tem_contrato']; ?></td>
    <td align="right" style="font-size:9px"><?php echo $row['inc_tem_emfermedad']; ?></td>
    <td align="right" style="font-size:9px"><?php echo $row['lic_maternidad']; ?></td>
    <td align="right" style="font-size:9px"><?php echo $row['vacaciones']; ?></td>
    <td align="right" style="font-size:9px"><?php echo number_format($row['inc_tem_acc_trabajo']); ?></td>
    <td align="right" style="font-size:9px"><?php echo $row['tipo_cotizante']; ?>&nbsp;</td>
    <td align="right" style="font-size:9px"><?php echo $row['periodo']; ?>&nbsp;</td>
    <td align="right" style="font-size:9px"><?php echo $row['correccion']; ?></td>
    <td align="right" style="font-size:9px"><?php echo $row['horascotizadas']; ?>&nbsp;</td>
    <td align="right" style="font-size:9px"><?php echo $row['procesado']; ?>&nbsp;</td>
    <td align="right" style="font-size:9px"><?php echo $row['usuario']; ?>&nbsp;</td>
    <td align="right" style="font-size:9px"><?php echo $row['fechasistema']; ?>&nbsp;</td>
    <td align="right" style="font-size:9px"><?php echo $row['usuariomodifica']; ?>&nbsp;</td>
     <td align="right" style="font-size:9px"><?php echo $row['fechamodifica']; ?>&nbsp;</td>
    </tr>
<?php 
	$cont++;
	$valoraporte += intval($row['valoraporte']);
	$devolucion += intval($row['devolucion']);
	$salariobasico += intval($row['salariobasico']);
	$ingresobase += intval($row['ingresobase']);
}
if($cont==0){
	echo "<tr><td class='Rojo' colspan='26' align='center'>No hay informaci&oacute;n del periodo</td></tr>";
	exit();
}else{
	?>
	<tr>
		<td align="right" style="font-size:9px"><h3>Total</h3></td>
	    <td style="font-size:9px">&nbsp;</td>
	    <td style="font-size:9px">&nbsp;</td>
	    <td align="right" style="font-size:9px"><h3><?php echo number_format($valoraporte); ?></h3></td>
	    <td align="right" style="font-size:9px"><h3><?php echo number_format($devolucion); ?></h3></td>
	    <td align="right" style="font-size:9px"><h3><?php echo number_format($salariobasico); ?></h3></td>
	    <td align="right" style="font-size:9px"><h3><?php echo number_format($ingresobase); ?></h3></td>
	    <td align="right" style="font-size:9px">&nbsp;</td>
	    <td align="right" style="font-size:9px">&nbsp;</td>
	    <td align="right" style="font-size:9px">&nbsp;</td>
	    <td align="right" style="font-size:9px">&nbsp;</td>
	    <td align="right" style="font-size:9px">&nbsp;</td>
	    <td align="right" style="font-size:9px">&nbsp;</td>
	    <td align="right" style="font-size:9px">&nbsp;</td>
	    <td align="right" style="font-size:9px">&nbsp;</td>
	    <td align="right" style="font-size:9px">&nbsp;</td>
	    <td align="right" style="font-size:9px">&nbsp;</td>
	    <td align="right" style="font-size:9px">&nbsp;</td>
	    <td align="right" style="font-size:9px">&nbsp;</td>
	    <td align="right" style="font-size:9px">&nbsp;</td>
	    <td align="right" style="font-size:9px">&nbsp;</td>
	    <td align="right" style="font-size:9px">&nbsp;</td>
	    <td align="right" style="font-size:9px">&nbsp;</td>
	    <td align="right" style="font-size:9px">&nbsp;</td>
	    <td align="right" style="font-size:9px">&nbsp;</td>
	    <td align="right" style="font-size:9px">&nbsp;</td>
	</tr>
<?php 
}
 ?> 
</tbody>
</table>
<div id="controls">
<div id="perpage">
			<select onChange="sorter.size(this.value)">
			<option value="5" selected="selected">5</option>
				<option value="10" >10</option>
				<option value="20">20</option>
				<option value="50">50</option>
				<option value="100">100</option>
			</select>
			<label>Registros Por P&aacute;gina</label>
  </div>
		<div id="navigation">
			<img src="../../imagenes/imagesSorter/first.gif" width="16" height="16" alt="first Page" onClick="sorter.move(-1,true)" />
			<img src="../../imagenes/imagesSorter/previous.gif" width="16" height="16" alt="first Page" onClick="sorter.move(-1)" />
			<img src="../../imagenes/imagesSorter/next.gif" width="16" height="16" alt="first Page" onClick="sorter.move(1)" />
			<img src="../../imagenes/imagesSorter/last.gif" width="16" height="16" alt="Last Page" onClick="sorter.move(1,true)" />
		</div>
		<div id="text">P&aacute;gina <label id="currentpagePU"></label> de <label id="pagelimitPU"></label></div>
</div>
</body>
