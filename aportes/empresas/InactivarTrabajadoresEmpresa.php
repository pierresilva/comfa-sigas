<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'p.afiliacion.class.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
include_once $raiz.DIRECTORY_SEPARATOR.'aportes'.DIRECTORY_SEPARATOR.'empresas'.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'empresas.class.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}
$objAfiliacion = new Afiliacion();
$objAfiEmpresa = new Empresa();
$idsPersonas = $_REQUEST["idspersonas"];
$arrIdsPersonas = explode(",",$idsPersonas);
$idEmpresa = $_REQUEST["idempresa"];
$fechaFidelidad = $_REQUEST["fechafidelidad"];
$fechaInactivacion = $_REQUEST["fechaestado"];
$efectuados = 0;
$noEfectuados = 0;
$tipoAfiliacion = 0;
// tipos de afiliación para independientes
$arrIdsTiposAfiliacionIndependiente = array(19,20,21,23,3320,2938);
$arrResultado = array("noefectuados" => array(), "numefectuados" => 0, "numnoefectuados" => 0, "operacion" => '');
	foreach($arrIdsPersonas as $idPersona){
		
			$sql = "select
				aportes016.idpersona,
				aportes016.idformulario,
				aportes016.tipoformulario,
				tipoafiliacion,
				horasdia,
				horasmes,
				agricola,
				cargo,
				categoria,
				aportes016.fechaingreso,
				aportes016.estado,
				aportes016.salario,
				aportes016.traslado,
				aportes016.codigocaja,
				aportes016.tipopago,
				aportes016.fechanovedad,
				aportes016.semanas,
				aportes016.fechafidelidad,
                aportes016.estadofidelidad,
                aportes016.flag,
                aportes016.tempo1,
                aportes016.tempo2,
                aportes016.auditado,
                aportes016.idagencia,
                aportes016.idradicacion,
				aportes015.idtipodocumento,
				aportes015.identificacion,
				aportes015.papellido,
				aportes015.sapellido,
				aportes015.pnombre,
				aportes015.snombre,
				aportes091.codigo
				from
				aportes016
				inner join aportes015 on aportes016.idpersona = aportes015.idpersona
				INNER JOIN aportes091 ON aportes015.idtipodocumento=aportes091.iddetalledef
				where
				aportes016.idempresa=$idEmpresa and
				aportes016.estado='A' and
				aportes016.idpersona=$idPersona";
			$result = $db->querySimple($sql);
			$afiliacionInicial = $result->fetch();
		
			// ejecutar procedimiento almacenado para inactivar la afiliación
			//$sql = "execute proc_inactivar_afiliacion 'I', {$afiliacionInicial["idformulario"]}, {$afiliacionInicial["idpersona"]}, $idCausalInactivacion, '$fechaRetiro', '{$_SESSION["USUARIO"]}', 0";
			$sql = "INSERT INTO aportes017( 
                        tipoformulario, 
                        tipoafiliacion, 
                        idempresa, 
                        idpersona, 
                        fechaingreso, 
                        horasdia, 
                        horasmes, 
                        salario, 
                        agricola, 
                        cargo, 
                        primaria, 
                        estado, 
                        fecharetiro, 
                        motivoretiro, 
                        fechanovedad, 
                        semanas, 
                        fechafidelidad, 
                        estadofidelidad, 
                        traslado, 
                        codigocaja, 
                        flag, 
                        tempo1, 
                        tempo2, 
                        fechasistema, 
                        usuario, 
                        tipopago, 
                        categoria, 
                        auditado, 
                        idagencia, 
                        idradicacion) 
	VALUES('{$afiliacionInicial["tipoformulario"]}',
			 '{$afiliacionInicial["tipoafiliacion"]}', 
			 '$idEmpresa', 
			 '{$afiliacionInicial["idpersona"]}', 
			 '{$afiliacionInicial["fechaingreso"]}', 
			 '{$afiliacionInicial["horasdia"]}', 
			 '{$afiliacionInicial["horasmes"]}', 
			 '{$afiliacionInicial["salario"]}', 
			 '{$afiliacionInicial["agricola"]}', 
			 '{$afiliacionInicial["cargo"]}', 
			 'N', 
			 'I', 
			 '$fechaInactivacion', 
			 '2858', 
			 '{$afiliacionInicial["fechanovedad"]}', 
			 '{$afiliacionInicial["semanas"]}', 
			 '$fechaFidelidad', 
			 'A', 
			 '{$afiliacionInicial["traslado"]}', 
			 '{$afiliacionInicial["codigocaja"]}', 
			 '{$afiliacionInicial["flag"]}', 
			 '{$afiliacionInicial["tempo1"]}',
			 '{$afiliacionInicial["tempo2"]}',
			 cast(getdate() as date), 
			 '{$_SESSION["USUARIO"]}', 
			 '{$afiliacionInicial["tipopago"]}', 
			 '{$afiliacionInicial["categoria"]}', 
			 '{$afiliacionInicial["auditado"]}', 
			 '{$afiliacionInicial["idagencia"]}', 
			 '{$afiliacionInicial["idradicacion"]}')";
			$resultInactivar = $db->queryActualiza($sql);
			if($resultInactivar == 1){
				// el insert se ejecutó sin problemas				
				$efectuados++;
				$arrResultado["numefectuados"] = $efectuados;
				$arrResultado["operacion"] = 'INACTIVO';
				$sql = "DELETE FROM aportes016 WHERE idformulario = {$afiliacionInicial["idformulario"]}";
				$resultBorrar = $db->queryActualiza($sql);
			}else{
				if($resultInactivar == 0){
					// hubo problemas insertando
					$noEfectuados++;
					$arrResultado["noefectuados"][] = $idPersona;
					$arrResultado["numnoefectuados"] = $noEfectuados;
					$arrResultado["operacion"] = 'INACTIVO';
				}
			}	
	}
print_r(json_encode($arrResultado));
die();
?>
