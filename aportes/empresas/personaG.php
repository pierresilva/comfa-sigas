<?php
// @autor:		Orlando Puentes A
// @fecha:		mayo 31/2010
// @formulario:	guardar datos persona - modulo empresa

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once 'clases' . DIRECTORY_SEPARATOR . 'persona.class.php';
$campo0 = trim($_POST['v0']);
$campo1 = 1;
$campo2 = strtoupper(htmlspecialchars(trim($_POST['v2'])));
$campo3 = strtoupper(htmlspecialchars(trim($_POST['v3'])));
$campo4 = strtoupper(htmlspecialchars(trim($_POST['v4'])));
$campo5 = strtoupper(htmlspecialchars(trim($_POST['v5'])));
$campo6 = trim($_POST['v6']);

$objClase = new Persona;
	if ( $objClase->insertSimple(array($campo0,$campo1,$campo2,$campo3,$campo4,$campo5,$campo6)) == true){
		echo '1';
	}else{
		echo "Se produjo un error guardando datos persona $campo0";
	} 

?>