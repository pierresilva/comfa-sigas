<?php
/* autor:       orlando puentes
 * fecha:       02/06/2010
 * objetivo:    
 */
 
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$usuario=$_SESSION['USUARIO'];
$idpersona= $_REQUEST['v0'];
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

include_once 'clases'.DIRECTORY_SEPARATOR.'empresas.class.php';

$campo0 = empty($_REQUEST['v0']) ? NULL : trim($_REQUEST['v0']);//idradicacion
$campo1 = empty($_REQUEST['v1']) ? NULL : trim($_REQUEST['v1']);//fecha
$campo2 = empty($_REQUEST['v2']) ? NULL : $_REQUEST['v2'];		//sector
$campo3 = empty($_REQUEST['v3']) ? NULL : trim($_REQUEST['v3']);//clase de sociedad
$campo4 = empty($_REQUEST['v4']) ? NULL : trim($_REQUEST['v4']);//Nit
$campo5 = empty($_REQUEST['v5']) ? NULL : trim($_REQUEST['v5']);//Digito
$campo6 = empty($_REQUEST['v6']) ? NULL : trim($_REQUEST['v6']);//Tipo Documento
$campo7 = empty($_REQUEST['v7']) ? NULL : strtoupper(trim($_REQUEST['v7']));//Razon Social
$campo8 = empty($_REQUEST['v8']) ? NULL : strtoupper(trim($_REQUEST['v8']));//Sigla
$campo9 = empty($_REQUEST['v9']) ? NULL : trim($_REQUEST['v9']);//departamento
$campo10 = empty($_REQUEST['v10']) ? NULL : trim($_REQUEST['v10']);//ciudad
$campo11 = empty($_REQUEST['v11']) ? NULL : trim($_REQUEST['v11']);//direccion
$campo12 = empty($_REQUEST['v12']) ? NULL : trim($_REQUEST['v12']);//telefono
$campo13 = empty($_REQUEST['v13']) ? NULL : trim($_REQUEST['v13']);//fax
$campo14 = empty($_REQUEST['v14']) ? NULL : trim($_REQUEST['v14']);//URL
$campo15 = empty($_REQUEST['v15']) ? NULL : trim($_REQUEST['v15']);//Email
$campo16 = empty($_REQUEST['v16']) ? NULL : trim($_REQUEST['v16']);//ID REPRESENTANTE
$campo17 = empty($_REQUEST['v17']) ? NULL : trim($_REQUEST['v17']);//ID CONTACTI
$campo18 = empty($_REQUEST['v18']) ? NULL : trim($_REQUEST['v18']);//contratista
$campo19 = empty($_REQUEST['v19']) ? NULL : trim($_REQUEST['v19']);//colegio
$campo20 = empty($_REQUEST['v20']) ? NULL : trim($_REQUEST['v20']);//excento
$campo21 = empty($_REQUEST['v21']) ? NULL : trim($_REQUEST['v21']);//Actividad Economica
$campo22 = empty($_REQUEST['v22']) ? NULL : $_REQUEST['v22'];//Indice
$campo23 = empty($_REQUEST['v23']) ? NULL : trim($_REQUEST['v23']);//Asesor
$campo24 = empty($_REQUEST['v24']) ? NULL : $_REQUEST['v24'];//Seccional
$campo25 = empty($_REQUEST['v25']) ? NULL : $_REQUEST['v25'];//Estado
$campo26 = empty($_REQUEST['v26']) ? NULL : trim($_REQUEST['v26']);//Fecha Afiliacion
$campo27 = empty($_REQUEST['v27']) ? NULL : trim($_REQUEST['v27']);//Fecha Aportes
$campo28 = empty($_REQUEST['v28']) ? NULL : trim($_REQUEST['v28']);//usuario
$campo29 = empty($_REQUEST['v29']) ? NULL : trim($_REQUEST['v29']);//codigo sucursal
$campo30 = empty($_REQUEST['v30']) ? NULL : trim($_REQUEST['v30']);//Fecha constitucion=>fechamatricula

$principal = 'N';

$guardada = false;

$sql="INSERT INTO aportes048 (nit, digito, nitrsn, codigosucursal, principal, razonsocial, sigla, direccion, iddepartamento, idciudad, idzona, telefono, fax, url, email, idrepresentante, idjefepersonal, contratista, colegio, exento, idcodigoactividad, indicador, idasesor, idsector, seccional, estado, fechaaportes, fechaafiliacion, trabajadores, aportantes, conyuges, hijos, hermanos, padres, usuario, fechasistema, idtipodocumento, idclasesociedad, fechamatricula) VALUES (:campo4, :campo5, :campo4, :campo29, :principal, :campo7, :campo8, :campo11, :campo9, :campo10, :campo10, :campo12, :campo13, :campo14, :campo15, :campo16, :campo17, :campo18, :campo19, :campo20, :campo21, :campo22, :campo23, :campo2, :campo24, :campo25, :campo27, :campo26, 0, 0, 0, 0, 0, 0, :campo28, cast(getdate() as date), :campo6, :campo3, :campo30)";
//$sql="INSERT INTO aportes048 (nit, digito, nitrsn, codigosucursal, principal, razonsocial, sigla, direccion, iddepartamento, idciudad, idzona, telefono, fax, url, email, idrepresentante, idjefepersonal, contratista, colegio, exento, idcodigoactividad, indicador, idasesor, idsector, seccional, estado, fechaaportes, fechaafiliacion, trabajadores, aportantes, conyuges, hijos, hermanos, padres, usuario, fechasistema, idtipodocumento, idclasesociedad, fechamatricula) VALUES ($campo4, $campo5, $campo4, $campo29, $principal, $campo7, $campo8, $campo11, $campo9, $campo10, $campo10, $campo12, $campo13, $campo14, $campo15, $campo16, $campo17, $campo18, $campo19, $campo20, $campo21, $campo22, $campo23, $campo2, $campo24, $campo25, $campo27, $campo26, 0, 0, 0, 0, 0, 0, $campo28, cast(getdate() as date), $campo6, $campo3, $campo30)";

$statement = $db->conexionID->prepare($sql);

$statement->bindParam(":campo4", $campo4, PDO::PARAM_STR);
$statement->bindParam(":campo5", $campo5, PDO::PARAM_STR);
$statement->bindParam(":campo4", $campo4, PDO::PARAM_STR);
$statement->bindParam(":campo29", $campo29, PDO::PARAM_STR);
$statement->bindParam(":campo7", $campo7, PDO::PARAM_STR);
$statement->bindParam(":campo8", $campo8, PDO::PARAM_STR);
$statement->bindParam(":campo11", $campo11, PDO::PARAM_STR);
$statement->bindParam(":campo9", $campo9, PDO::PARAM_STR);
$statement->bindParam(":campo10", $campo10, PDO::PARAM_STR);
$statement->bindParam(":campo10", $campo10, PDO::PARAM_STR);
$statement->bindParam(":campo12", $campo12, PDO::PARAM_STR);
$statement->bindParam(":campo13", $campo13, PDO::PARAM_STR);
$statement->bindParam(":campo14", $campo14, PDO::PARAM_STR);
$statement->bindParam(":campo15", $campo15, PDO::PARAM_STR);
$statement->bindParam(":campo16", $campo16, PDO::PARAM_STR);
$statement->bindParam(":campo17", $campo17, PDO::PARAM_STR);
$statement->bindParam(":campo18", $campo18, PDO::PARAM_STR);
$statement->bindParam(":campo19", $campo19, PDO::PARAM_STR);
$statement->bindParam(":campo20", $campo20, PDO::PARAM_STR);
$statement->bindParam(":campo21", $campo21, PDO::PARAM_INT);
$statement->bindParam(":campo22", $campo22, PDO::PARAM_STR);
$statement->bindParam(":campo23", $campo23, PDO::PARAM_INT);
$statement->bindParam(":campo2", $campo2, PDO::PARAM_INT);
$statement->bindParam(":campo24", $campo24, PDO::PARAM_STR);
$statement->bindParam(":campo25", $campo25, PDO::PARAM_STR);
$statement->bindParam(":campo27", $campo27, PDO::PARAM_STR);
$statement->bindParam(":campo26", $campo26, PDO::PARAM_STR);
$statement->bindParam(":campo28", $campo28, PDO::PARAM_STR);
$statement->bindParam(":campo6", $campo6, PDO::PARAM_INT);
$statement->bindParam(":campo3", $campo3, PDO::PARAM_INT);
$statement->bindParam(":campo30", $campo30, PDO::PARAM_STR);
$statement->bindParam(":principal", $principal, PDO::PARAM_STR);

$guardada = $statement->execute();
if($guardada){
	// buscar id de la empresa creada
	$rs = $db->conexionID->lastInsertId();
	echo $rs;
}else{
	// errores
	$error = $statement->errorInfo();
	echo "Error codigo {$error[0]} con el mensaje >>> {$error[2]}";
}
die();
?>