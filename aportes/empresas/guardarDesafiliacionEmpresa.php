<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'aportes'.DIRECTORY_SEPARATOR.'empresas'.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'empresas.class.php';
include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'p.afiliacion.class.php';

//Recibir datos del js
$datosEncabezado=$_REQUEST['datosEncabezado'];
$datosDetalle = empty($_REQUEST['datosDetalle']) ? 'NULL' : $_REQUEST['datosDetalle'];
//$datosDetalle=$_REQUEST['datosDetalle'];

$usuario=$_SESSION['USUARIO'];
$banderaError = 0;
$idDesafiliacion = 0;
$codigoEstadoEmpresa = 4118;//[4118][EXPULSION]
$fechaSistema = Date("ymd");
$objEmpresa = new Empresa();
$objAfiliacion = new Afiliacion();

$objEmpresa->inicioTransaccion();

//Validar si ya esta desafiliada
$datoDesafiliacion = $objEmpresa->buscar_empresa_desafiliada(
		array("id_empresa"=>$datosEncabezado["id_empresa"],"estado"=>"A"));
if(count($datoDesafiliacion)>0){
	$idDesafiliacion = $datoDesafiliacion[0]["id_desafiliacion"];
}

//Guardar la desafiliacion
if($idDesafiliacion==0){
	$idDesafiliacion = $objEmpresa->guardar_desafiliacion($datosEncabezado,$usuario);
	if($idDesafiliacion>0){
		if($datosDetalle!='NULL'){
			if(count($datosDetalle)>0){
				echo count($datosDetalle);
				foreach ($datosDetalle as $rowDato){
					$rowDato["id_desafiliacion"] = $idDesafiliacion;
				 
					//Guardar el detalle desafiliacion
					if($objEmpresa->guardar_detalle_desafiliacion($rowDato)==0){
						$banderaError++;
						break;
					}
				}
			}else{
				$banderaError++;
			}
		}	
	}else{
		$banderaError++;
	}
}

//Inactivar Empleados
if($banderaError==0 && $datosEncabezado["estado_empresa"]=="I"){
	if(($objAfiliacion->spProcesoInactAfiliExpul($datosEncabezado["id_empresa"],$usuario))==0){
		$banderaError++;
	}	
}

//Inactivar Empresa
if($banderaError==0 && $datosEncabezado["estado_empresa"]=="I"){
	$rsInactivarEmpresa = $objEmpresa->inactivar_empresa($datosEncabezado["id_empresa"],$datosEncabezado["fechaDesafiliacion"],$codigoEstadoEmpresa); 
	if($rsInactivarEmpresa==false){
		$banderaError++;
	}
}

//Actualizar codigo estado de la empresa
if($banderaError==0 && $datosEncabezado["estado_empresa"]!="I"){
	$rsCodigoEstado = $objEmpresa->actualizar_codigo_estado($datosEncabezado["id_empresa"],$codigoEstadoEmpresa);
	if($rsCodigoEstado==0){
		$banderaError++;
	}
}

//Guardar Observacion Empresa
if($banderaError==0){
	$rsObservacion = $objEmpresa->guardar_observacion(array("id_empresa"=>$datosEncabezado["id_empresa"]
			,"observacion"=>$datosEncabezado["observacion"]),$usuario);
	if($rsObservacion==0)
		$banderaError++;
}

//Fin
if($banderaError>0){
	$objEmpresa->cancelarTransaccion();
	echo 0;
}else{
	$objEmpresa->confirmarTransaccion();
	echo 1;
}
?>