<?php
/* autor:       orlando puentes
 * fecha:       07/06/2010
 * objetivo:    
 */
 $root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8" />
	<title>jQuery UI Dialog - Modal form</title>
    
    <link type="text/css" href="../../css/formularios/base/ui.all.css" rel="stylesheet" />
<link type="text/css" href="../../css/Estilos.css" rel="stylesheet"/>
<link type="text/css" href="../../css/formularios/demos.css" />
<script type="text/javascript" src="../../js/jquery-1.4.2.js"></script>
<script type="text/javascript" src="../../js/formularios/ui/ui.core.js"></script>
<script type="text/javascript" src="../../js/formularios/ui/ui.draggable.js"></script>
<script type="text/javascript" src="../../js/formularios/ui/ui.resizable.js"></script>
<script type="text/javascript" src="../../js/formularios/ui/ui.dialog.js"></script>
<script type="text/javascript" src="../../js/formularios/ui/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../../js/formularios/ui/ui.datepicker.js"></script>
<script type="text/javascript" src="../../js/comunes.js"></script>
<script type="text/javascript" src="js/empresas.js"></script>

<script type="text/javascript" src="../../js/formularios/ui/effects.core.js"></script> 
<script type="text/javascript" src="../../js/formularios/ui/ui.button.js"></script>    
	<link type="text/css" href="../../themes/base/jquery.ui.all.css" rel="stylesheet" />
	
	<script type="text/javascript" src="../../external/jquery.bgiframe-2.1.1.js"></script>
	
	
	<script type="text/javascript" src="../../ui/jquery.ui.mouse.js"></script>
	
	
	<script type="text/javascript" src="../../ui/jquery.ui.position.js"></script>
	
	
	<script type="text/javascript" src="../../ui/jquery.effects.core.js"></script>
	<link type="text/css" href="../demos.css" rel="stylesheet" />
	<style type="text/css">
		body { font-size: 62.5%; }
		label, input { display:block; }
		input.text { margin-bottom:12px; width:95%; padding: .4em; }
		fieldset { padding:0; border:0; margin-first:25px; }
		h1 { font-size: 1.2em; margin: .6em 0; }
		div#users-contain { width: 350px; margin: 20px 0; }
		div#users-contain table { margin: 1em 0; border-collapse: collapse; width: 100%; }
		div#users-contain table td, div#users-contain table th { border: 1px solid #eee; padding: .6em 10px; text-align: left; }
		.ui-dialog .ui-state-error { padding: .3em; }
		.validateTips { border: 1px solid transparent; padding: 0.3em; }
		
	</style>
	<script type="text/javascript">
	$(function() {
		$("#dialog").dialog("destroy");
		var notas = $("#notas"),
			allFields = $([]).add(name),
			tips = $(".validateTips");
		
		$("#dialog-form").dialog({
			autoOpen: false,
			height: 300,
			width: 350,
			modal: true,
			buttons: {
				'Enviar': function() {
					var bValid = true;
					allFields.removeClass('ui-state-error');
					var campo0=notas.val();
					$.ajax({
					url: 'colaboracion.php',
					type: "POST",
					data: "submit=&v0="+campo0,
					success: function(datos){
						if(datos=='1'){
							alert("Su comentario fue enviado correctamente!. Gracias por paticipar en el mejoramiento de SIGAS");
							}
						else{
							alert(datos);
							}
					}
				});
				$('#notas').val($('#notas').val());
				$('#notas').val('');
				$('#dialog-form select,#dialog-form input[type=text]').val('');
				$(this).dialog('close');
				},
				Cancelar: function() {
					$(this).dialog('close');
				}
			},
			close: function() {
				allFields.val('').removeClass('ui-state-error');
			}
		});
		
		
		
		$('#create-user')
			.button()
			.click(function() {
				$('#dialog-form').dialog('open');
			});

	});
	</script>
</head>
<body>

<div class="demo">

<div id="dialog-form" title="Colaboración en línea">
	<p class="validateTips">Por favor diligencie este formulario para enviar comentarios, errores o falencias encontradas en el proceso. Máximo 250 caracteres</p>

	<form>
	<fieldset>
		<label for="notas">Comentario</label>
        <textarea name="notas" id="notas" cols="40" class="text ui-widget-content ui-corner-all"></textarea>
	</fieldset>
	</form>
</div>


<div id="users-contain" class="ui-widget">

		<h1>Existing Users:</h1>


	
</div>
<button id="create-user">Create new user</button>

</div><!-- End demo -->

<div class="demo-description">


</div><!-- End demo-description -->

</body>
</html>
