<?php
	/**
	 * Busca las empresas ley 1429 con inconsistencias y las actualiza a No giro y guarda la observacion.
	 * 
	 * */
	set_time_limit(0);
	ini_set("display_errors",'1');
	date_default_timezone_set("America/Bogota");
	$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
	include_once  $root;
	
	$usuario=$_SESSION['USUARIO'];
	$fecha=date("m/d/Y");
	$campo0 = $_REQUEST['v0']; //Fecha Inicial
	$campo1 = $_REQUEST['v1']; //Fecha Final
	
	include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
	include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
	include_once $raiz.DIRECTORY_SEPARATOR.'aportes'.DIRECTORY_SEPARATOR.'empresas'.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'empresas.class.php';
	
	$db = IFXDbManejador::conectarDB();
	if($db->conexionID==null){
		$cadena = $db->error;
		echo msg_error($cadena);
		exit();
	}
	
	$objEmpresa = new Empresa(); 
	$sql = "SELECT
			a48.idempresa,
			a48.nit
			FROM aportes010 a10
			INNER JOIN aportes048 a48 ON a48.nit=a10.nit
			WHERE
			a48.claseaportante=2875
			AND 
				((CASE DATEDIFF(yyyy,a48.fechamatricula,GETDATE())
				WHEN 0 THEN '0'
				WHEN 1 THEN '0'
				WHEN 2 THEN '0'
				WHEN 3 THEN '1'
				WHEN 4 THEN '2'
				WHEN 5 THEN '3'
				ELSE
					(CASE WHEN DATEDIFF(yyyy,a48.fechamatricula, GETDATE()) >= 6 THEN '4' END) END)!=(SELECT TOP 1 substring(a91.codigo,1,1) FROM aportes091 a91 WHERE a91.iddetalledef=a48.indicador)
			OR a10.tarifaaporte IS NULL 
			OR (SELECT TOP 1 substring(a91.codigo,1,1) FROM aportes091 a91 WHERE a91.iddetalledef=a48.indicador AND a91.codigo!='0.6') IS NULL)
			AND 
			(CASE DATEDIFF(yyyy,a48.fechamatricula,convert(NVARCHAR(30),a10.periodo + '01',111))+1
			WHEN 1 THEN '0'
			WHEN 2 THEN '0'
			WHEN 3 THEN '1'
			WHEN 4 THEN '2'
			WHEN 5 THEN '3'
			ELSE
			(CASE WHEN DATEDIFF(yyyy,a48.fechamatricula,convert(NVARCHAR(30),a10.periodo + '01',111))+1 >= 6 THEN '4' END) END) <> a10.tarifaaporte
			AND a48.estado='A'
			AND a10.fechasistema BETWEEN '$campo0' AND '$campo1'
			GROUP BY a48.idempresa,a48.nit";

	$idEmpresaFiltro="";
	$rsDatos = $db->querySimple($sql);
	while ($row=$rsDatos->fetch()){
		$idEmpresaFiltro .=	$row["idempresa"].",";
	}
	$idEmpresaFiltro=trim($idEmpresaFiltro,",");
	if ($idEmpresaFiltro != ''){
	//-------------------------------------------------GUARDA LA OBSERVACION------------------------------------------------------------------
	$arridEmpresas = explode(",",$idEmpresaFiltro);
	$obs = 'Se marca la empresa para NO giro, proceso de inconsistencias ley 1429';	//observacion
	$flag = 2;	//  2 empresa	
	
	foreach($arridEmpresas as $idEmpresa){
		$sql="Select observaciones from aportes088 where idregistro=$idEmpresa and identidad=$flag";
		$rs=$db->querySimple($sql);
		$x=$rs->fetch();
		$not = $x['observaciones'];
		
		$ob=$fecha." - ".$usuario." - ".$obs."<br>".$not;
		
		if($not==''){
			
		$sql1="Select count(*) as cuenta from aportes088 where idregistro=$idEmpresa and identidad=$flag";
		$rs1=$db->querySimple($sql1);
		$w=$rs1->fetch();
		
		if($w['cuenta']>0){
			$sql="delete from aportes088 where idregistro=$idEmpresa and identidad=$flag";
			$rs1=$db->queryActualiza($sql);
		}
			$sql1="Insert into aportes088 (idregistro,identidad,observaciones) values($idEmpresa,$flag,'$ob')";
			$rs1=$db->queryInsert($sql1, 'aportes088');
		}//Fin del if($not==''){
	}
	//------------------------------------------------------FIN----------------------------------------------------------------
	}
	$empresas=$objEmpresa->empresasNOgiro_ley1429($idEmpresaFiltro);
	echo (count($empresas)>0) ? json_encode($empresas) : 0;
	
?>