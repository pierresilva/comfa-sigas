URL=src();
nuevo=0;
var idPersona=0;
var valorTotalPignorado = 0;
$(document).ready(function(){
	
	
	//Dialog Colaboracion en linea
	$("#dialog-form2").dialog({
		autoOpen: false,
		height: 400,
		width: 500,
		draggable:false,
		modal: true,
		buttons: {
		    'Enviar': function() {
			var bValid = true;
			var campo=$('#notas').val();
			var campo0=$.trim(campo);
			if (campo0==""){
				$(this).dialog('close');
					return false;
				}
			var campo1=$('#usuario').val();
			var campo2="empresaSinConv.php";
			$.post(URL+'phpComunes/colaboracion.php',{v0:campo0,v1:campo1,v2:campo2},
			function(datos){
				if(datos=='1'){
					alert("Su comentario fue enviado correctamente!. Gracias por participar en el mejoramiento de SIGAS");
				}else{
					alert(datos);
				}
		});
		$(this).dialog('close');
		},
		Cancelar: function() {
			$(this).dialog('close');
			$("#dialog-form2").dialog("close");
		}
	}
  });
  
  //Dialog ayuda
  $("#ayuda").dialog({
      autoOpen: false,
      height: 450,
      width: 700,
      draggable:true,
      modal:false,
      open: function(evt, ui){
		$('#ayuda').html('');
		$.get(URL +'help/aportes/ayudaCargaManualPignoracion.html',function(data){
			$('#ayuda').html(data);
		});
      }
  });
	
	/*$("#valor").bind('blur',function(){
		if($(this).val() > parseInt($("#total_saldo").html())){
			alert("El valor a pagar no debe ser mayor al saldo.");
		}
	});*/
  
	$("#pagar_total").bind('click',function(){
		if($(this).is(":checked")){
			$("#valor").val(parseInt($("#total_saldo").html()));
			$("#valor").attr('disabled',true);
		}else{
			$("#valor").attr('disabled',false);
		}
	});
	
});

function nuevoD(){
	nuevo=1;
	$("#bGuardar").show();
	limpiarCampos();
}

function limpiarCampos(){
	$("table.tablero input:text").val('');
	$("table.tablero select").val(0);
	$("#notas").val('');
	$("#tipoDoc").focus();
	$("#idPersona").val('');
	$("#lista_pignoraciones").hide();
	$("#lista_pignoraciones tbody").empty();
	$("#lista_abonos").hide();
	$("#lista_abonos tbody").empty();
	$("#total_saldo").empty();
	$("#total_abonos").empty();
	$("#periodo").val(periodoActual);
}

function buscarPersona(td,event){
	var numero = event.value;
	if(numero != ''){
		//advertencia si el numero de indentificacion esta incorrecto
		validarLongNumIdent(td,event);
		if($("#identificacion").val().trim()==''){
			nuevoD();
			return false;
		}
			
		if(nuevo == 0){
			MENSAJE("Si es un registro nuevo haga click en NUEVO.");
			return false;
		}
		$("#lista_pignoraciones tbody").empty();
		$.ajax({
			url:URL+'phpComunes/buscarPersona2.php',
			type:"POST",
			dataType:"json",
			data:{v1:numero,v0:td},
			success:function(datos){
				if(datos==0){
					MENSAJE("No existe el afiliado.");
					return false;
				}else{
					$.each(datos,function(i,fila){
						nom = $.trim(fila.pnombre)+" "+$.trim(fila.snombre)+" "+$.trim(fila.papellido)+" "+$.trim(fila.sapellido);
						idPersona=fila.idpersona;
					});//each
					$("#nombreCompleto").val(nom);
					$("#idPersona").val(idPersona);
					// buscar las pignoraciones del afiliado
					$.ajax({
						url:URL+'phpComunes/buscarPignoracion.php',
						type:"POST",
						dataType:"json",
						data:{v0:idPersona, estado: 'A'},
						success:function(datos){
							if(datos != 0){
								$.each(datos,function(i,pignoracion){
									var idInput = 'pignoracion_'+ pignoracion.idpignoracion;
									var strTr = "<tr>" +
													"<td align='center'><input type='radio' name='pignoracion' id='"+idInput+"'></td>"+
													"<td align='center'><label for='"+idInput+"'>"+ pignoracion.idpignoracion +"</label></td>" +
													"<td align='center'><label for='"+idInput+"'>"+ pignoracion.fechapignoracion +"</label></td>" +
													"<td align='center'><label for='"+idInput+"'>"+ pignoracion.valorpignorado +"</label></td>" +
													"<td align='center'><label for='"+idInput+"'>"+ pignoracion.saldo +"</label></td>" +
													"<td align='center'><label for='"+idInput+"'>"+ pignoracion.estado +"</label></td>" +
													"<td align='center'><label for='"+idInput+"'>"+ pignoracion.observaciones +"</label></td>" +
												"</tr>";
									$("#lista_pignoraciones tbody").append(strTr);
									$("#"+ idInput).bind("click",function(){
										valorTotalPignorado = pignoracion.valorpignorado;
										cargarAbonos($("#"+ idInput).attr('id').replace("pignoracion_",""));
									});
									
									if(datos.length == 1)
										$("#"+ idInput).trigger('click');
								});
								$("#lista_pignoraciones").show();
							}else{
								alert("La persona solicitada no tiene pignoraciones activas.");
								nuevo=0;
							}
						}
					});
				}
				
			}
		});//ajax
	}
}

function cargarAbonos(idPignoracion){
	$("#lista_abonos tbody").empty();
	$("#total_abonos,#total_saldo").empty();
	$("#lista_abonos").show();
	$("#valor").val('');
	$.ajax({
		url:URL+'aportes/procesoPignoracion/cargarAbonos.php',
		type:"POST",
		dataType:"json",
		data:{idPignoracion: idPignoracion},
		success:function(datos){
			if(datos != 0){
				var totalAbonos = 0;
				var saldo = 0;
				var valorPignorado = 0;
				$.each(datos,function(i,abono){
					var strTr = "<tr>" +
									"<td>"+ abono.iddetalle43 +"</td>"+
									"<td>"+ abono.fechasistema +"</td>"+
									"<td>"+ abono.periodo +"</td>"+
									"<td>"+ abono.valor +"</td>"+
								"</tr>";
					$("#lista_abonos tbody").append(strTr);
					totalAbonos += abono.valor;
					valorPignorado = abono.valorpignorado;
				});
				saldo = valorPignorado-totalAbonos;
				$("#total_abonos").html(totalAbonos);
				$("#total_saldo").html(saldo);
			}else{
				$("#total_abonos").html(0);
				$("#total_saldo").html(valorTotalPignorado);
			}
		}
	});
}

function guardar(){
	var errores = "";
	if(nuevo==0){
		MENSAJE("Si es un registro nuevo haga click en NUEVO.");
	   $("#bGuardar").show();
	   return false;
	}
	var valor = $("#valor").val();
	var periodo = $("#periodo").val();
	var idPignoracion = 0;
	var pagoTotal = 0;
	var saldoTotal = parseInt($("#total_saldo").html());
	
	if($("input[name=pignoracion]").length == 0)
		errores += "Debe seleccionar una pignoraci\xf3n.\n";
	else
		idPignoracion = $("input[name=pignoracion]:checked").attr('id').replace('pignoracion_','');
	
	if($("#pagar_total").is(":checked") || $("#valor").val() == saldoTotal)
		pagoTotal = 1;
	
	idPersona = $("#idPersona").val();
	if(idPersona == 0 || idPersona == '')
		errores += "Verifique el afiliado!\n";
	
	if(isNaN(parseInt(periodo)))
		errores += "Verifique el periodo a abonar.\n";
	
	if($("#valor").val() == '' || isNaN(parseInt($("#valor").val())))
		errores += "Verifique el valor a pagar.\n";
	/*else
		if($("#valor").val() > parseInt($("#total_saldo").html()))
			errores += "El valor a pagar no debe ser mayor al saldo.\n";*/
	
	if(errores == ""){
		$.ajax({
			url: 'guardarAbono.php',
			type:"POST",
			dataType:"json",
			data:{idPersona: idPersona, valor: valor, periodo: periodo, idBeneficiario: idPersona, idPignoracion: idPignoracion, pagoTotal: pagoTotal, saldoTotal: saldoTotal},
			success:function(datos){
				var msg = "";
				if(datos.errores == null){
					msg = "El abono fue grabado satisfactoriamente!";
					//guardar observacion
					observacionesTab(idPersona,1);
				}else
					msg = "No se pudo grabar el abono.\nVerifique los errores: \n"+ datos.errores.join("\n");
				MENSAJE(msg);	
			}
		});
		nuevo=0;
	}else
		alert("Hay errores en los datos: \n"+ errores);
}

function mostrarAyuda(){
	$("#ayuda").dialog('open');
}

