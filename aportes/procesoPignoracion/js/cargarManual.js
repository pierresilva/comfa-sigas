var URL=src()
	//, idPersona=0
	, uriBuscarPignoracion = ""
	, objDataPignoracion = {};

$(document).ready(function(){
	uriBuscarPignoracion = URL+"aportes/procesoPignoracion/buscarPignoracion.php";
	
	datepickerC("FECHA","#txtFechaPignoracion");
	
	$("#divPignoracion").dialog({
		autoOpen: false,
		modal: true,
		width: 500,
		buttons: {
			"Cerrar": function(){
				$(this).dialog("close");
			}
		}
	});
});

function nuevo(){
	//nuevo=1;
	$("#bGuardar").show();
	//limpiarCampos();
	objDataPignoracion = {};
	limpiarCampos2("#tipoDoc,#identificacion,#lblNombreCompleto,#valor,#txtFechaPignoracion,#motivo,#notas,#idPersona,#tblPignoracion tbody,#hidIdPignoracion");
}

/*function limpiarCampos(){
	$("table.tablero input:text").val('');
	$("table.tablero select").val(0);
	$("#notas").val('');
	$("#tipoDoc").focus();
	$("#idPersona").val('');
}*/

function buscarPersona(td,numero){
	if(numero != ''){
		/*if(nuevo == 0){
			MENSAJE("Si es un registro nuevo haga click en NUEVO.");
			return false;
		}*/
		
		$.ajax({
			url:URL+'phpComunes/buscarPersona2.php',
			type:"POST",
			dataType:"json",
			data:{v1:numero,v0:td},
			success:function(datos){
				if(datos==0){
					MENSAJE("No existe el afiliado.");
					return false;
				}else{
					var idPersona = "";
					$.each(datos,function(i,fila){
						nom = $.trim(fila.pnombre)+" "+$.trim(fila.snombre)+" "+$.trim(fila.papellido)+" "+$.trim(fila.sapellido);
						idPersona=fila.idpersona;
					});//each
					$("#lblNombreCompleto").text(nom);
					$("#idPersona").val(idPersona);
				}
				
			}
		});//ajax
	}
}

/**
 * METODO: Busca las pignoraciones
 * 
 * @param Json objDatosFiltro
 * @returns Array  
 * */
function fetchPignoracion(idPersona,estado){
	var datosRetorno = "";
	$.ajax({
		url:uriBuscarPignoracion,
		type:"POST",
		data:{id_persona:idPersona,estado:estado},
		dataType:"json",
		async:false,
		success:function(datos){
			//************************************
			datosRetorno = datos;
		}
	});
	return datosRetorno;
}

function listar(){
	var idPersona = $("#idPersona").val();
	objDataPignoracion = {};
	
	if(!idPersona) {
		alert("Falta cargar los datos de la persona!!");
		return false;
	}
	
	var objData = fetchPignoracion(idPersona,"A");
	var htmlDataPignoracion = "";
	
	if(objData && typeof objData=="object" && objData.length>0){
		
		$.each(objData,function(i,row){
			htmlDataPignoracion += "<tr><td><a onclick='ctrPignoracion(\""+row.idpignoracion+"\")' style='cursor:pointer;'>"+row.idpignoracion+"</a></td>" 
										+ "<td>"+row.valorpignorado+"</td>"
										+ "<td>"+row.fechapignoracion+"</td>"
										+ "<td>"+row.convenio+"</td>"
										+ "<td>"+row.estado+"</td>"
									+"</tr>";
			objDataPignoracion["id"+row.idpignoracion] = row;
		});
	}
	
	$("#tblPignoracion tbody").html(htmlDataPignoracion);
	$("#divPignoracion").dialog("open");
}


function ctrPignoracion(idPignoracion){
	var objPignoracion = objDataPignoracion["id"+idPignoracion];
	
	$("#hidIdPignoracion").val(objPignoracion.idpignoracion)
	$("#valor").val(objPignoracion.valorpignorado);
	$("#txtFechaPignoracion").val(objPignoracion.fechapignoracion);
	$("#motivo").val(objPignoracion.idconvenio);
	$("#notas").val(objPignoracion.observaciones);	
	
	$("#divPignoracion").dialog("close");
}

function guardar(){
	var errores = "";
	/*if(nuevo==0){
		MENSAJE("Si es un registro nuevo haga click en NUEVO.");
	   $("#bGuardar").show();
	   return false;
	}*/

	idPersona = $("#idPersona").val();
	if(idPersona == 0 || idPersona == '')
		errores += "Verifique el afiliado!\n";
	
	var idPignoracion = parseInt($("#hidIdPignoracion").val());
	var valor = $("#valor").val();
	var fechaPignoracion = $("#txtFechaPignoracion").val();
	var motivo = $("#motivo").val();
	var notas = $("#notas").val();
	
	
	if(isNaN(parseInt(valor)))
		errores += "Verifique el valor a pignorar.\n";
	
	if(!fechaPignoracion)
		errores += "Verifique la fecha de la pignoracion.\n";
	
	if(isNaN(parseInt(motivo)))
		errores += "Verifique el motivo.";
	
	if(notas.length > 250)
		errores += "Las notas no deben superar los 250 caracteres.\n";
	
	if(errores == ""){
		
		//Verificar si es para actualizar o guardar
		if(isNaN(idPignoracion) || idPignoracion==0){
			
			//Guardar la pignoracion
			$.ajax({
				url: 'guardarPignoracion.php',
				type:"POST",
				dataType:"json",
				data:{idPersona: idPersona, valor: valor, fecha_pignoracion:fechaPignoracion, motivo: motivo, notas: notas},
				success:function(datos){
					var msg = "";
					if(datos.errores == null){
						msg = "La pignoraci&oacute;n fue grabada satisfactoriamente!";
						nuevo();
					}else
						msg = "No se pudo grabar la pignoraci&oacute;n.\nVerifique los errores: \n"+ datos.errores.join("\n");
					MENSAJE(msg);	
				}
			});
			
		}else{
			
			//Actualizar la pignoracion
			$.ajax({
				url: 'actualizarPignoracion.php',
				type:"POST",
				dataType:"json",
				data:{id_pignoracion: idPignoracion, valor: valor, fecha_pignoracion:fechaPignoracion, motivo: motivo, notas: notas},
				success:function(datos){
					var msg = "";
					if(datos == 1){
						msg = "La pignoraci&oacute;n fue actualizada satisfactoriamente!";
						nuevo();
					}else{
						msg = "No se pudo actualizar la pignoraci&oacute;n.";
					}
					MENSAJE(msg);
				}
			});
		}
	}else
		alert("Hay errores en los datos: \n"+ errores);
}

