<?php
/**
 * @author Oswaldo Gonzalez
 * @date 6-jun-2012
 */
set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz. DIRECTORY_SEPARATOR. 'config.php';
include_once $raiz. DIRECTORY_SEPARATOR. 'phpComunes' . DIRECTORY_SEPARATOR . 'session.php';
include_once $raiz. DIRECTORY_SEPARATOR. 'rsc'. DIRECTORY_SEPARATOR. 'pdo' .DIRECTORY_SEPARATOR. 'IFXDbManejador.php';
include_once $raiz. DIRECTORY_SEPARATOR. 'rsc'. DIRECTORY_SEPARATOR. 'pdo' .DIRECTORY_SEPARATOR. 'IFXerror.php';
include_once $raiz. DIRECTORY_SEPARATOR. 'clases' . DIRECTORY_SEPARATOR . 'p.persona.class.php';
session();

$db = IFXDbManejador::conectarDB();
if($db->conexionID == null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

$idPignoracion = $_REQUEST["id_pignoracion"];
$valor = $_REQUEST["valor"];
$fechaPignoracion = $_REQUEST["fecha_pignoracion"];
$convenio = $_REQUEST["motivo"];
$notas = $_REQUEST["notas"];

$usuario = $_SESSION['USUARIO'];

$sql = "UPDATE aportes043 SET 
			valorpignorado = $valor
			, saldo = $valor
			, idconvenio = $convenio
			, fechapignoracion = '$fechaPignoracion'
			, observaciones = '$notas'
			, usuario = '$usuario'
			, fechasistema = cast(getdate() as date)
		WHERE idpignoracion=$idPignoracion";
$resultado = $db->queryInsert($sql,"aportes043");

if($resultado==true){
	echo 1;
}else{
	echo 0;
}
   
?>