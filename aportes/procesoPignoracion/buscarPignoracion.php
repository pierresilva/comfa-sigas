<?php
set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz. DIRECTORY_SEPARATOR. 'config.php';
include_once $raiz. DIRECTORY_SEPARATOR. 'phpComunes' . DIRECTORY_SEPARATOR . 'session.php';
include_once $raiz. DIRECTORY_SEPARATOR. 'rsc'. DIRECTORY_SEPARATOR. 'pdo' .DIRECTORY_SEPARATOR. 'IFXDbManejador.php';
include_once $raiz. DIRECTORY_SEPARATOR. 'rsc'. DIRECTORY_SEPARATOR. 'pdo' .DIRECTORY_SEPARATOR. 'IFXerror.php';
include_once $raiz. DIRECTORY_SEPARATOR. 'clases' . DIRECTORY_SEPARATOR . 'p.persona.class.php';

$idPersona = $_REQUEST["id_persona"];
$estado = $_REQUEST["estado"];

$usuario = $_SESSION['USUARIO'];

$db = IFXDbManejador::conectarDB();
if($db->conexionID == null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

$query = "SELECT a43.idpignoracion, a43.idtrabajador, a43.tipopago, a43.valorpignorado, a43.idconvenio, a43.fechapignoracion, a43.estado, a43.saldo, a43.observaciones, a43.pagare, a43.facturanumero, a43.anulado, a43.fechaanula, a43.motivo, a43.usuario, a43.fechasistema, a43.fechacancelacion, a43.flag, a43.tempo1, a43.periodoinicia, a43.saldoantes, a43.totalabono
				, a91.detalledefinicion AS convenio
			FROM dbo.aportes043 a43
				LEFT JOIN aportes091 a91 ON a91.iddetalledef=a43.idconvenio
			WHERE idtrabajador=".$idPersona." AND estado='".$estado."'";

$arrResultado = array();
$resultado = $db->querySimple($query);

while($row = $resultado->fetch()){
	$arrResultado[] = $row;
}

echo json_encode($arrResultado);
?>