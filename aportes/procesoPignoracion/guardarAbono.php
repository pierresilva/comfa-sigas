<?php
/**
 * @author Oswaldo Gonzalez
 * @date 6-jun-2012
 */
set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz. DIRECTORY_SEPARATOR. 'config.php';
include_once $raiz. DIRECTORY_SEPARATOR. 'rsc'. DIRECTORY_SEPARATOR. 'pdo' .DIRECTORY_SEPARATOR. 'IFXDbManejador.php';
include_once $raiz. DIRECTORY_SEPARATOR. 'rsc'. DIRECTORY_SEPARATOR. 'pdo' .DIRECTORY_SEPARATOR. 'IFXerror.php';
include_once $raiz. DIRECTORY_SEPARATOR. 'clases' . DIRECTORY_SEPARATOR . 'p.persona.class.php';
include_once $raiz. DIRECTORY_SEPARATOR. 'clases' . DIRECTORY_SEPARATOR . 'p.pignoracion.class.php';
session();
$idPersona = $_REQUEST["idPersona"];
$valor = intval($_REQUEST["valor"]);
$periodo = $_REQUEST["periodo"];
$idBeneficiario = $_REQUEST["idBeneficiario"];
$idPignoracion = $_REQUEST["idPignoracion"];
$pagoTotal = $_REQUEST["pagoTotal"];
$saldoTotal = $_REQUEST["saldoTotal"];

$usuario = $_SESSION['USUARIO'];

$db = IFXDbManejador::conectarDB();
if($db->conexionID == null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

$resultado = array("idabono" => 0, "errores" => null);
$arrErroresTmp = array();
$persona = new Persona();
$objPignoracion = new Pignoracion();
$rsAfiliado = $persona->mostrar_persona_id($idPersona);
$afiliado = mssql_fetch_array($rsAfiliado);
$rsPignoracion = $objPignoracion->buscar_pignoracion_por_id($idPignoracion);
$pignoracion = mssql_fetch_array($rsPignoracion);

if(is_array($afiliado)){
	if(is_array($afiliado)){
		$afiliado = array_map("utf8_encode",$afiliado);
	}else
		$arrErroresTmp["afiliado"] = "No se encuentra la persona.";
}else
	$arrErroresTmp["afiliado"] = "No se encuentra la persona.";

if(!is_array($pignoracion)){
	$arrErroresTmp["pignoracion"] = "No se encuentra la pignoraci&oacute;n.";
}else{
	//if($pignoracion["estado"] == 'I')
		//$arrErroresTmp["estadoPignoracion"] = "La pignoraci&oacute;n est&aacute; inactiva.";
}

if(!is_numeric($valor))
	$arrErroresTmp["valor"] = "El valor del abono debe ser un n&uacute;mero.";

if(count($arrErroresTmp) == 0){
	$sql = "INSERT INTO aportes044 (idpignoracion, idbeneficiario, periodo, valor, usuario, fechasistema) values ($idPignoracion, {$afiliado["idpersona"]}, '$periodo', $valor, '{$_SESSION["USUARIO"]}', cast(getdate() as date))";
	$rs1 = $db->queryInsert($sql,"aportes044");
    if(intval($rs1) > 0){
    	$resultado["idabono"] = intval($rs1);
    	if($pagoTotal){
    		$sql = "UPDATE aportes043 SET estado='I', saldo=0 WHERE idpignoracion=$idPignoracion";
    		$rs2 = $db->queryActualiza($sql);
    		if(is_array($rs2)){
    			$cadena=$db->error;
    			$resultado["errores"] = array("No se pudo inactivar la pignoraci&oacute;n: ". msg_error($cadena));
    		}
    	}else{
    		//actualizar saldo de la pignoracion
    		$saldoTotal = $saldoTotal-$valor;
    		$sql = "UPDATE aportes043 SET estado='A', saldo=$saldoTotal WHERE idpignoracion=$idPignoracion";
    		$rs3 = $db->queryActualiza($sql);
	    	if(is_array($rs3)){
    			$cadena=$db->error;
    			$resultado["errores"] = array("No se pudo actualizar el saldo de la pignoraci&oacute;n: ". msg_error($cadena));
    		}
    	}
    }else
    	$resultado["errores"] = array("No se pudo grabar el abono.");
}else
	$resultado["errores"] = array_values($arrErroresTmp);
print_r(json_encode($resultado));

?>