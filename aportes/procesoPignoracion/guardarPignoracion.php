<?php
/**
 * @author Oswaldo Gonzalez
 * @date 6-jun-2012
 */
set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz. DIRECTORY_SEPARATOR. 'config.php';
include_once $raiz. DIRECTORY_SEPARATOR. 'phpComunes' . DIRECTORY_SEPARATOR . 'session.php';
include_once $raiz. DIRECTORY_SEPARATOR. 'rsc'. DIRECTORY_SEPARATOR. 'pdo' .DIRECTORY_SEPARATOR. 'IFXDbManejador.php';
include_once $raiz. DIRECTORY_SEPARATOR. 'rsc'. DIRECTORY_SEPARATOR. 'pdo' .DIRECTORY_SEPARATOR. 'IFXerror.php';
//include_once $raiz. DIRECTORY_SEPARATOR. 'clases' . DIRECTORY_SEPARATOR . 'p.persona.class.php';
session();
$idPersona = $_REQUEST["idPersona"];
$valor = $_REQUEST["valor"];
$fechaPignoracion = $_REQUEST["fecha_pignoracion"];
$convenio = $_REQUEST["motivo"];
$notas = $_REQUEST["notas"];

$usuario = $_SESSION['USUARIO'];

$db = IFXDbManejador::conectarDB();
if($db->conexionID == null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

$resultado = array("idpignoracion" => 0, "errores" => null);
$arrErroresTmp = array();

/*$persona = new Persona();
$afiliado = $persona->obtener_datos_trabajador_idpersona($idPersona);
if(is_array($afiliado)){
	if(is_array($afiliado)){
		$afiliado = array_map("utf8_encode",$afiliado);
	}else
		$arrErroresTmp["afiliado"] = "No se encuentra el afiliado.";
}else
	$arrErroresTmp["afiliado"] = "No se encuentra el afiliado.";
*/


if(!is_numeric($valor))
	$arrErroresTmp["total"] = "El total de la pignoraci&oacute;n debe ser un n&uacute;mero.";

if(!is_numeric($convenio))
	$arrErroresTmp["idconvenio"] = "El ID del convenio debe ser un n&uacute;mero.";

if(strlen($notas)>250)
	$arrErroresTmp["notas"] = "Las notas/observaciones no pueden superar los 250 caracteres.";

$resultPeriodo = $db->ultimo_periodo();
$arrPeriodo = $resultPeriodo->fetch();
$periodo = $arrPeriodo["periodo"];

if(count($arrErroresTmp) == 0){
	$tipoPago = 'T';
	$estado = 'A';
		
	$sql = "INSERT INTO aportes043 (idtrabajador, tipopago, valorpignorado, idconvenio, fechapignoracion, estado, saldo, observaciones, usuario, fechasistema, periodoinicia) values ($idPersona, '$tipoPago', $valor, $convenio, '$fechaPignoracion', '$estado', $valor, '$notas', '{$_SESSION["USUARIO"]}', cast(getdate() as date), '$periodo')";
	$rs1 = $db->queryInsert($sql,"aportes043");

    if(intval($rs1) > 0)
		$resultado["idpignoracion"] = intval($rs1);
    else
    	$resultado["errores"] = array("pignoracion" => "No se pudo grabar la pignoracion.");
}else
	$resultado["errores"] = array_values($arrErroresTmp);
print_r(json_encode($resultado));

?>