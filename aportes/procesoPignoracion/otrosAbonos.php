<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz .DIRECTORY_SEPARATOR. 'config.php';
include_once $raiz .DIRECTORY_SEPARATOR. 'clases' .DIRECTORY_SEPARATOR. 'p.definiciones.class.php';
include_once $raiz. DIRECTORY_SEPARATOR. 'rsc'. DIRECTORY_SEPARATOR. 'pdo' .DIRECTORY_SEPARATOR. 'IFXDbManejador.php';
include_once $raiz. DIRECTORY_SEPARATOR. 'rsc'. DIRECTORY_SEPARATOR. 'pdo' .DIRECTORY_SEPARATOR. 'IFXerror.php';
$fecver = date('Ymd h:i:s A',filectime('distribucion.php'));
$db = IFXDbManejador::conectarDB();
if($db->conexionID == null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

$objDefiniciones = new Definiciones();
$sql = "SELECT MIN(periodo) AS periodo FROM aportes012 WHERE procesado='N'";
$rsPeriodoActual = $db->querySimple($sql);
$arrPeriodoActual = $rsPeriodoActual->fetch();
$periodoActual = $arrPeriodoActual["periodo"];
$usuario = $_SESSION["USUARIO"];

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>:: Registro de abonos de Pignoraci&oacute;n ::</title>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_PORTAL; ?>css/Estilos.css"  />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_PORTAL; ?>css/estilo_tablas.css" />
	<link type="text/css" href="<?php echo URL_PORTAL; ?>css/formularios/base/ui.all.css" rel="stylesheet" />
	<link href="<?php echo URL_PORTAL; ?>css/marco.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/jquery-1.4.2.js"></script>
	<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/formularios/ui/ui.core.js"></script>
	<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/formularios/ui/ui.dialog.js"></script>
	<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/formularios/ui/ui.datepicker.js"></script>
	<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/comunes.js"></script>
	<script type="text/javascript" src="js/otrosAbonos.js"></script>
	<script type="text/javascript">
		var periodoActual = <?php echo $periodoActual; ?>;
	</script>
</head>

<body>
<br /><br />
<table border="0" align="center" cellpadding="0" cellspacing="0" width="70%">
  <tbody>
 
  <!-- ESTILOS SUPERIOR TABLA-->
  <tr>
    <td width="13" height="29" class="arriba_iz">&nbsp;</td>
    <td class="arriba_ce"><span class="letrablanca">:: Registro de abonos de Pignoraci&oacute;n ::</span>
    <div style="text-align:right;float:right;">
					<?php echo 'Versi&oacute;n: ' . $fecver; ?>
				</div>
    </td>
    <td width="13" class="arriba_de" align="right">&nbsp;</td>
  </tr>
  
  <!-- ESTILOS ICONOS TABLA-->
  <tr>
	<td class="cuerpo_iz">&nbsp;</td>
	<td class="cuerpo_ce">
	  <img src="<?php echo URL_PORTAL; ?>imagenes/menu/nuevo.png" alt="Nuevo" width="16" height="16" style="cursor:pointer" title="Nuevo" onclick="nuevoD()" />
	  <img src="<?php echo URL_PORTAL; ?>imagenes/spacer.gif" alt="" width="1" height="1" />
	  <img src="<?php echo URL_PORTAL; ?>imagenes/menu/grabar.png" alt="Guardar" width="16" height="16" style="cursor:pointer" title="Guardar" onclick="guardar();" id="bGuardar"/>  <img src="../../imagenes/spacer.gif" alt="" width="1" height="1" /> 
	  <img src="<?php echo URL_PORTAL; ?>imagenes/spacer.gif" alt="" width="1" height="1" />
	  <img src="<?php echo URL_PORTAL; ?>imagenes/menu/informacion.png" alt="Ayuda" width="16" height="16" style="border:none; cursor:pointer" title="Manual" onclick="mostrarAyuda();" />
	  <img src="<?php echo URL_PORTAL; ?>imagenes/menu/notas.png" alt="Soporte" width="16" height="16" style="cursor: pointer" title="Colaboraci&oacute;n en l&iacute;nea" onclick="notas();" />
	</td>
	<td class="cuerpo_de">&nbsp;</td>
  </tr>
  <!-- ESTILOS MEDIO TABLA-->
  <tr>
    <td class="cuerpo_iz">&nbsp;</td>
    <td class="cuerpo_ce">
   <table border="0" cellpadding="5" align="center" class="tablero" width="90%">
	<tr>
		<td>Tipo Documento</td>
		<td>
			<select id="tipoDoc" class="box1">
		    <?php
				$consulta = $objDefiniciones->mostrar_datos(1,1);
				while($row = mssql_fetch_array($consulta))
					echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
			?>
		    </select>
		</td>
		<td>N&uacute;mero</td>
		<td><input id="identificacion" type="text" class="box1" onblur="buscarPersona(document.getElementById('tipoDoc').value,this)" onkeyup="validarCaracteresPermitidos(document.getElementById('tipoDoc').value,this);"/></td>
	</tr>
	<tr>
	  <td>Nombre afiliado</td>
	  <td colspan="3"><label for="nombreCompleto"></label>
	    <input type="text" name="nombreCompleto" id="nombreCompleto"  class="boxlargo" disabled="disabled"/>
	  </td>
	</tr>
	<tr>
		<td>Pignoraciones</td>
		<td colspan="3">
			<table id="lista_pignoraciones" style="display: none;">
				<thead>
					<tr>
						<th colspan="2">Id. Pigno.</th>
						<th>Fecha</th>
						<th>Valor</th>
						<th>Saldo</th>
						<th>Estado</th>
						<th>Concepto</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</td>
	</tr>
	<tr>
		<td>Abonos realizados</td>
		<td colspan="3">
			<table id="lista_abonos" style="display: none;">
				<thead>
					<tr>
						<th>Id. Abono.</th>
						<th>Fecha</th>
						<th>Periodo</th>
						<th>Valor</th>
					</tr>
				</thead>
				<tbody></tbody>
				<tfoot>
					<tr>
						<td colspan="3">Total:</td><td><span id="total_abonos"></span></td>
					</tr>
					<tr>
						<th colspan="3"><h2>Saldo:</h2></th><th><h2><span id="total_saldo"></span></h2></th>
					</tr>
				</tfoot>
			</table>
		</td>
	</tr>
	<tr>
		<td><label for="periodo">Periodo</label></td>
		<td><input type="text" name="periodo" id="periodo" size="6" maxlength="6" readonly="" onkeydown="solonumeros(this);" onkeyup="solonumeros(this);" maxlength="10" value="<?php echo $periodoActual; ?>" /></td>
		<td><label for="valor">Valor a abonar</label></td>
		<td><input type="text" name="valor" id="valor" size="10" onkeydown="solonumeros(this);" onkeyup="solonumeros(this);" maxlength="10" /> <input type="checkbox" name="pagar_total" id="pagar_total" /> <label for="pagar_total">Saldo total</label></td>
	</tr>
	</table>
    </td>
    <td class="cuerpo_de">&nbsp;</td>
  </tr>
  
  <!-- CONTENIDO TABLA-->
  <tr>
    <td class="cuerpo_iz">&nbsp;</td>
    <td class="cuerpo_ce">    
    </td>
    <td class="cuerpo_de">&nbsp;</td>
  </tr>
  
  <!-- ESTILOS PIE TABLA-->
  <tr>
    <td class="abajo_iz" >&nbsp;</td>
    <td class="abajo_ce" ></td>
    <td class="abajo_de" >&nbsp;</td>
  </tr>
  
</tbody>

</table>
<!-- FORMULARIO OBSERVACIONES-->
<div name="div-observaciones-tab" style="display:none" title="Observaciones de modificaci&oacute;n">
<table class="tablero">
 <tr>
   <td>Usuario</td>
   <td colspan="3" >
   <input type="text" name="usuarioObs" id="usuarioObs" class="box1" disabled="disabled" value="<?php echo $usuario?>" /> </td>
   </tr>
 <tr>
   <td>Observaciones</td>
   <td colspan="3" >
   <textarea name="observacionGrupo" id="observacionGrupo" cols="45" rows="5" class="boxlargo"></textarea></td>
   </tr>
</table>
<div class="ui-state-highlight ui-corner-all" style="margin: 9px auto; display:none;padding: 6px; text-align: center; border: 1px solid rgb(255, 255, 0); width:300px" id="rtaObservacion"><span class="ui-icon ui-icon-info" style="float: left; margin-right: 0.3em;"></span>Observaci&oacute;n guardada con &eacute;xito.</div>
</div>

<!-- colaboracion en linea -->
<div id="div-observaciones-tab"></div>
<div id="dialog-form2" title="Colaboraci&oacute;n en l&iacute;nea">
<p>Por favor diligencie este formulario para enviar comentarios, errores o falencias encontradas en el proceso. M&aacute;ximo 250 caracteres </p>
<label>Tus comentarios:</label><br />
<textarea name="notas" id="notas" cols="60", rows="10"></textarea>
</div>


<!-- Manual Ayuda -->
<div id="ayuda" title=":: Manual ::"></div>
<input id="idPersona" type="hidden" />
</body>
</html>