<?php
include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'session.php';
session();

require_once ('clases/Consultor.php');
$con = new Consultor();
$sql = "SELECT TOP 1 periodo, fechalimite, controlproceso, estadogiro FROM aportes012 WHERE procesado = 'N' ORDER BY periodo ASC";
$dper1 = $con->enArray($sql);
$sql = "SELECT TOP 1 fechalimite FROM aportes012 WHERE procesado = 'S' ORDER BY periodo DESC";
$dper2 = $con->enArray($sql);

include_once ("clases/p.definiciones.class.php");
$objClase = new Definiciones();
?>
<script type="text/javascript" src="js/adc-empresa.js" language="javascript"></script>
<script type="text/javascript" src="js/js-giro-adc-empresa.js" language="javascript"></script>

<table width="100%" border="0" cellspacing="0" class="tablero">
	<tr>
		<td>
		<table id="tblArchivosCargados" border="0" cellspacing="0"
			class="box-table-a" style="margin: 1em auto;">
			<tbody>
				<tr>
					<td style="width: 40%">
					<div id="divFormGiro">
					<div><label>Periodo actual</label><input type="text"
						id="txtAEPeriodoActual" name="txtAEPeriodoActual" readonly="readonly"
						value="<?php
						echo $dper1[0]['periodo']?>" /></div>
							<div><label>Periodo a girar</label><input type="text"
								id="txtAEPeriodo" name="txtAEPeriodo" /></div>
							<!-- <div><label>Tipo identificaci&oacute;n</label><select
								name="sltTipIdEmpresa" id="sltTipIdEmpresa">
								<option value="0" selected="selected">Seleccione...</option>
            					<?php
										$consulta = $objClase->mostrar_datos(1, 1);
										while($row = mssql_fetch_array($consulta)){
											echo "<option value=" . $row['iddetalledef'] . ">" . $row['detalledefinicion'] . "</option>";
										}
										?>
          						</select></div> -->
							<div><label>N&uacute;mero identificaci&oacute;n</label><input
								type="text" id="txtNoIdEmpresa" name="txtNoIdEmpresa" /></div>
							<!--<div><label>Id. Empresa</label><input
								type="text" id="txtIdEmpresa" name="txtIdEmpresa" readonly="readonly" /></div>
							 <div><label>Salario B&aacute;sico</label><input type="radio"
						name="radAdcTipSal" id="radSalBas" checked="checked" /></div>
					<div><label>Ingreso B&aacute;se</label><input type="radio"
						name="radAdcTipSal" id="radIngBas" /></div>  -->
					</div>
					</td>
					<td>
					<div id="giro-pros-adcemp">
						<div id="progreso-adcemp"></div>
					</div>
					</td>
				</tr>
			</tbody>
			<thead>
				<tr>
					<th><strong>Giro</strong></th>
					<th><strong>Resumen</strong></th>
				</tr>
			</thead>
		</table>
		</td>
	</tr>
	<tr>
		<td>
		<p align="center">
			<input type="button" id="btnGiroAdicEmpresa" name="btnGiroAdicEmpresa" value="Procesar Giro" <?php echo $dper1[0]['estadogiro']=='C'?'Disabled="Disabled"':'' ?>  />
			<input type="button" id="btnDeshGiroAdicEmpresa" name="btnDeshGiroAdicEmpresa" value="Deshacer Giro" <?php echo $dper1[0]['estadogiro']=='C'?'Disabled="Disabled"':'' ?>  />
		</p>
		</td>
	</tr>
</table>
