<?php 
//ini_set("display_errors",'1');
session_start ();
include_once($_SESSION["RAIZ"] . "config.php");
global $arregloAgencias;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Reporte de afiliados DIAN</title>
	<link type="text/css" href="<?php echo URL_PORTAL.DIRECTORIO_ESTILOS_CSS_FORMULARIOS; ?>base/ui.all.css" rel="stylesheet" />
	<link type="text/css" href="<?php echo URL_PORTAL.DIRECTORIO_ESTILOS_CSS; ?>Estilos.css" rel="stylesheet"/>
	<link type="text/css" href="<?php echo URL_PORTAL.DIRECTORIO_ESTILOS_CSS; ?>estilo_tablas.css" rel="stylesheet"/>
	<link type="text/css" href="<?php echo URL_PORTAL.DIRECTORIO_ESTILOS_CSS_FORMULARIOS; ?>demos.css" />
	<link type="text/css" href="<?php echo URL_PORTAL.DIRECTORIO_ESTILOS_CSS; ?>estiloReporte.css" rel="stylesheet"/>
	<script type="text/javascript" src="<?php echo URL_PORTAL.DIRECTORIO_SCRIPTS_JS; ?>jquery-1.3.2.min.js"></script>
	<script type="text/javascript" src="<?php echo URL_PORTAL.DIRECTORIO_SCRIPTS_JS_FORMULARIOS; ?>ui/ui.datepicker.js"></script>
</head>
<script>
	$(document).ready(function() {
		$( "#fecha" ).datepicker({dateFormat: 'mm-dd-yy'});
	});
</script>
<table width="100%" border="0">
	<tr>
		<td align="center" ><img src="<?php echo URL_PORTAL . DIRECTORIO_IMAGENES;?>logo_reporte.png" width="32" height="32" /></td>
	</tr>
	<tr>
		<td align="center" ><strong class="titulo">Reporte de beneficiarios que cumplieron 12 a&ntilde;os y que no tienen certificados de estudios.</strong></td>
	</tr>
	<tr>
		<td align="center">
			<form name="frm1" id="frm1" action="<?php echo URL_PORTAL.DIRECTORIO_CENTRO_REPORTES ."aportes/giro/reporte013.php"; ?>" method="post">
				<table class='tablaR hover'>
					<tr>
						<th>Edad l&iacute;mite:</th>
						<td><input type="text" name="edad" id="edad" size="3" />
						</td>
					</tr>
					<tr>
						<th>Fecha de corte:</th>
						<td><input type="text" name="fecha" id="fecha" />
						</td>
					</tr>
					<tr>
						<th colspan="2">Formato:</th>
					</tr>
					<tr>
						<td><input type="radio" name="formato" id="opt_formato_texto" value="texto" checked /> <label for="opt_formato_texto">Archivo plano</label></td>
						<td><input type="radio" name="formato" id="opt_formato_pdf" value="pdf" /> <label for="opt_formato_pdf">PDF</label></td>
					</tr>
					<tr>
						<td colspan="2"><input type="submit" value="Enviar" /></td>
					</tr>
				</table>
			</form>	
		</td>
	</tr>
</table>