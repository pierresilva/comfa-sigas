<?php
/* autor:       orlando puentes
 * fecha:       28/09/2010
 * objetivo:
 */
//ini_set("display_errors",'1');
include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'session.php';
session();


?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Men&uacute; Reportes de subsidio cuota monetaria</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="<?php echo URL_PORTAL.DIRECTORIO_ESTILOS_CSS; ?>estiloReporte.css" rel="stylesheet" type="text/css">
<script language="javascript" src="<?php echo URL_PORTAL.DIRECTORIO_SCRIPTS_JS; ?>jquery-1.3.2.min.js"></script>
<script language="javascript" src="<?php echo URL_PORTAL.DIRECTORIO_SCRIPTS_JS; ?>comunes.js"></script>
<script language="javascript">
	var dirCentroReportes = '<?php echo DIRECTORIO_CENTRO_REPORTES; ?>';
</script>
</head>
<body>
<center>
  <table width="100%" border="0">
  <tr>
    <td align="center" ><img src="<?php echo URL_PORTAL.DIRECTORIO_CENTRO_REPORTES; ?>imagenes/razonSocialRpt.png" width="518" height="91" /></td>
  </tr>
  <tr>
    <td align="center" ><b>Listado de Reportes de subsidio cuota monetaria</b></td>
  </tr>
  <tr>
    <td class="fecha" ></td>
  </tr>
</table>
<table width="60%" border="0" cellspacing="1" class="tablero">
	<tr><th>Reporte</th>
	<th>Descripci&oacute;n</th></tr>
	  <!--<tr>
	    <td scope="row" align="left">Reporte001</td>
	    <td align="left"><label style="cursor:hand" onclick="imprimir(1)">Trabajadores retirados al 11 del mes de giro actual.</label></td>
	  </tr> -->
	 <tr>
	  <td scope="row" align="left" width="20%">Reporte002</td>
	  <td align="left"><label style="cursor:hand" onClick="imprimir(2)">Empresas que pagaron aportes el mismo mes de giro actual, pero que no tienen giro(NO contratistas, ni independientes, ni pensionados).</label></td>
	</tr>
	<tr>
	  <td scope="row">Reporte003</td>
	  <td><label style="cursor:hand" onClick="imprimir(3)">Novedades de Retiro a último día del mes sin Subsidio Cuota Monetaria</label></td>
	</tr>
	<tr>
	  <td scope="row">Reporte004</td>
	  <td>
	  <label style="cursor:pointer" onClick="imprimir(4);">Trabajadores que se retiraron e ingresaron dentro del mismo mes de giro y que no tienen giro actual.</td>
	</tr>
	<tr>
	  <td scope="row">Reporte005</td>
	  <td><label style="cursor:pointer" onClick="imprimir(5);">Empresas empleadoras que pagaron aportes fuera de la fecha l&iacute;mite de pago y que no tienen giro actual</label></td>
	</tr>
	<tr>
	  <td scope="row">Reporte006</td>
	  <td><label style="cursor:pointer" onClick="imprimir(6);">Afiliado con giro actual y que trabajan con empresas contratistas, pensionados o independientes.</label></td>
	</tr>
	<tr>
	  <td scope="row">Reporte007</td>
	  <td><label style="cursor:hand" onClick="imprimir(7)">Reporte de lo que se gira por embargo con forma de pago C.</label></td>
	</tr>
	<tr>
	  <td scope="row">Reporte008</td>
	  <td><label style="cursor:hand" onClick="imprimir(8)">Beneficiarios que han presentado certificados con respecto al ao anterior (supervivencia)</label></td>
	</tr>
	<tr>
	  <td scope="row">Reporte009</td>
	  <td><label style="cursor:hand" onClick="imprimir(9)">Beneficiarios que han presentado certificados con respecto al ao anterior (escolaridad)</label></td>
	</tr>
	<tr>
	  <td scope="row">Reporte010</td>
	  <td><label style="cursor:hand" onClick="imprimir(10)">Giro del mes actual para auditoria</label></td>
	</tr>
	<tr>
	  <td scope="row">Reporte012</td>
	  <td><label style="cursor:hand" onClick="configurar(1)">Beneficiarios activos por agencia</label></td>
	</tr>
	<tr>
	  <td scope="row">Reporte013</td>
	  <td><label style="cursor:hand" onClick="imprimir(11)">Beneficiarios que cumplieron 12 a&ntilde;os y que no tienen certificados de estudios.</label></td>
	</tr>
	<tr>
	  <td scope="row">Reporte014</td>
	  <td><label style="cursor:hand" onClick="imprimir(12)">Reporte de pignoraciones activas.</label></td>
	</tr>
	<tr>
	  <td scope="row">Reporte015</td>
	  <td><label style="cursor:hand" onClick="imprimir(13)">Reporte planilla de cr&eacute;dito resumido.</label></td>
	</tr>
	<tr>
	  <td scope="row">Reporte016</td>
	  <td><label style="cursor:hand" onClick="imprimir(14)">Planilla de cr&eacute;ditos no cancelados (Volados).</label></td>
	</tr>
	<tr>
	  <td scope="row">Reporte017</td>
	  <td><label style="cursor:hand" onClick="imprimir(15)">Afiliados con novedad de retiro en planilla &uacute;nica.</label></td>
	</tr>
	<tr>
	  <td scope="row">Reporte018</td>
	  <td><label style="cursor:hand" onClick="configurar(2)">Giro de meses anteriores para auditoria.</label></td>
	</tr>
	<tr>
	  <td scope="row">Reporte019</td>
	  <td><label style="cursor:hand" onClick="configurar(3)">Pignoraciones por a&ntilde;o.</label></td>
	</tr>
	<tr>
	  <td scope="row">Reporte020</td>
	  <td><label style="cursor:hand" onClick="configurar(4)">Afiliados DIAN.</label></td>
	</tr>
	<tr>
	  <td scope="row">Reporte021</td>
	  <td><label style="cursor:hand" onClick="configurar(5)">Beneficiarios mayores de 19 a&ntilde;os para inactivar.</label></td>
	</tr>
	<tr>
	  <td scope="row">Reporte022</td>
	  <td><label style="cursor:hand" onClick="configurar(6)">Beneficiarios sin certificados (Escolaridad, discapacidad, supervivencia, universidad).</label></td>
	</tr>
	<tr>
	  <td scope="row">Reporte023</td>
	  <td><label style="cursor:hand" onClick="configurar(7)">Afiliados por seccional.</label></td>
	</tr>
	<tr>
	  <td scope="row">Reporte024</td>
	  <td><label style="cursor:hand" onClick="imprimir(16)">Afiliados no afiliados y con giro actual.</label></td>
	</tr>
	<tr>
	  <td scope="row">Reporte025</td>
	  <td><label style="cursor:hand" onClick="imprimir(17)">Empresas programadas para giro pero que no fueron procesadas.</label></td>
	</tr>
	<tr>
	  <td scope="row">Reporte026</td>
	  <td><label style="cursor:hand" onClick="imprimir(18)">Empresas contratistas.</label></td>
	</tr>
	<tr>
	  <td scope="row">Reporte027</td>
	  <td><label style="cursor:hand" onClick="imprimir(19)">Pignoraciones con saldo negativo (menor a $0).</label></td>
	</tr>
	<tr>
	  <td scope="row">Reporte028</td>
	  <td><label style="cursor:hand" onClick="configurar(8)">Planilla corporaci&oacute;n.</label></td>
	</tr>
</table>
</center>
</body>
<script language="javascript">
var URL=src();

function configurar(nu){
	switch (nu){
		case 1 : url=URL+"aportes/giro/reportes/configReporte012.php";
		break;
		case 2 : url=URL+"aportes/giro/reportes/configReporte018.php";
		break;
		case 3 : url=URL+"aportes/giro/reportes/configReporte019.php";
		break;
		case 4 : url=URL+"aportes/giro/reportes/configReporte020.php";
		break;
		case 5 : url=URL+"aportes/giro/reportes/configReporte021.php";
		break;
		case 6 : url=URL+"aportes/giro/reportes/configReporte022.php";
		break;
		case 7 : url=URL+"aportes/giro/reportes/configReporte023.php";
		break;
		case 8 : url=URL+"aportes/giro/reportes/configReporte028.php";
		break;
	}
	window.open(url,"myFrame" );
}

function imprimir(nu){
    switch(nu){
        case 1 : url=URL+dirCentroReportes+"aportes/giro/reporte001.php";
        break;
        case 2 : url=URL+dirCentroReportes+"aportes/giro/reporte002.php";
		break;
		case 3 : url=URL+dirCentroReportes+"aportes/giro/reporte003.php";
		break;
		case 4 : url=URL+dirCentroReportes+"aportes/giro/reporte004.php";
		break;
		case 5 : url=URL+dirCentroReportes+"aportes/giro/reporte005.php";
		break;
		case 6 : url=URL+dirCentroReportes+"aportes/giro/reporte006.php";
		break;
		case 7 : url=URL+dirCentroReportes+"aportes/giro/reporte007.php";
		break;
		case 8 : url=URL+dirCentroReportes+"aportes/giro/reporte008.php";
		break;
		case 9 : url=URL+dirCentroReportes+"aportes/giro/reporte009.php";
		break;
		case 10 : url=URL+dirCentroReportes+"aportes/giro/reporte010.php";
		break;
		case 11 : url=URL+dirCentroReportes+"aportes/giro/reporte013.php";
		break;
		case 12 : url=URL+dirCentroReportes+"aportes/giro/reporte014.php";
		break;
		case 13 : url=URL+dirCentroReportes+"aportes/giro/reporte015.php";
		break;
		case 14 : url=URL+dirCentroReportes+"aportes/giro/reporte016.php";
		break;
		case 15 : url=URL+dirCentroReportes+"aportes/giro/reporte017.php";
		break;
		case 16 : url=URL+dirCentroReportes+"aportes/giro/reporte024.php";
		break;
		case 17 : url=URL+dirCentroReportes+"aportes/giro/reporte025.php";
		break;
		case 18 : url=URL+dirCentroReportes+"aportes/giro/reporte026.php";
		break;
		case 19 : url=URL+dirCentroReportes+"aportes/giro/reporte027.php";
		break;
    }
    window.open(url, '_blank')
}
</script>
</html>