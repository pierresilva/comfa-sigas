<?php 
ini_set("display_errors", '1');
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;


include_once("config.php");
include($raiz."centroReportes/aportes/giro/ReporteGiroController.class.php");
include_once(RUTA_FISICA_DIRECTORIO_CLASES_RSC."pdo/IFXDbManejador.php");
$objReporte = new ReporteGiroController();
$agencias=$objReporte->obtener_Agencias();
$giros= $objReporte->obtener_Giro();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>REPORTE RESUMEN DE GIRO</title>
<link type="text/css" href="../../../css/Estilos.css" rel="stylesheet"/>
</head>
<body>
<center>
<p><br />
  <br />
  <img src="../../../centroReportes/imagenes/logo_reporte.png" width="362" height="70" /></p>
<p><strong>RESUMEN DEL SUBSIDIO PAGADO EN EL PERIODO</strong> </p>
<form action="<?php echo $_SESSION["URL"]; ?>centroReportes/aportes/giro/reporte030.php" method="post">
  <br />
<table width="30%" border="0" class="tablaR" >
<tr>
  <th colspan="2">Resumen Cuota Monetaria</th></tr>
<tr>
<td width="61%" style="text-align:left"><strong>Fuente:</strong></td>
<td width="39%" style="text-align:left"><select name="giro" class="box1" id="giro">
  <option value="0">Seleccione</option>
  <?php
	 foreach ($giros as $key => $giro) {
		 echo "<option value='". strtolower($giro["detalledefinicion"]) ."'>". $giro["detalledefinicion"] ."</option>";
	}
  ?>
</select></td>
</tr>
<tr>
  <td style="text-align:left"><strong>Periodo:</strong></td>
  <td style="text-align:left"><input name="periodo" type="text" class="box1" id="periodo" /></td>
</tr>
<tr>
  <td><div align="left"><strong>Agencias:</strong></div></td>
  <td><span style="text-align:left">
    <select name="agencia" class="box1" id="agencia">
      <option value="0">Seleccione</option>
      <?php
		foreach($agencias as $key => $agencia){
			echo "<option value='". $agencia["iddetalledef"] ."'>". $agencia["detalledefinicion"] ."</option>";
		}
		?>
    </select>
  </span>
    <td style="text-align:left">&nbsp;</td>
</tr>
<tr>
  <td colspan="2" style="text-align:center"><input name="buscar" type="submit" class="ui-state-default" id="buscar" value="Generar" /></td>
  </tr>
</table>

</form>
</center>
</body>
</html>
