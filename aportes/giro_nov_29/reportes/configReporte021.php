<?php 
ini_set("display_errors",1);
include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'session.php';
session();

include_once($_SESSION["RAIZ"] . "config.php");
$fechaHoy = date("Y-m-d");
$fechaHoyCompleta = date("Y-m-d h:i:s");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Reporte de afiliados DIAN</title>
	<link type="text/css" href="<?php echo URL_PORTAL.DIRECTORIO_ESTILOS_CSS_FORMULARIOS; ?>base/ui.all.css" rel="stylesheet" />
	<link type="text/css" href="<?php echo URL_PORTAL.DIRECTORIO_ESTILOS_CSS; ?>Estilos.css" rel="stylesheet"/>
	<link type="text/css" href="<?php echo URL_PORTAL.DIRECTORIO_ESTILOS_CSS; ?>estilo_tablas.css" rel="stylesheet"/>
	<link type="text/css" href="<?php echo URL_PORTAL.DIRECTORIO_ESTILOS_CSS_FORMULARIOS; ?>demos.css" />
	<link type="text/css" href="<?php echo URL_PORTAL.DIRECTORIO_ESTILOS_CSS; ?>estiloReporte.css" rel="stylesheet"/>
	<script type="text/javascript" src="<?php echo URL_PORTAL.DIRECTORIO_SCRIPTS_JS; ?>jquery-1.3.2.min.js"></script>
	<script type="text/javascript" src="<?php echo URL_PORTAL.DIRECTORIO_SCRIPTS_JS_FORMULARIOS; ?>ui/ui.datepicker.js"></script>
</head>
<script>
	$(document).ready(function() {
		$( "#fecha" ).datepicker({dateFormat: 'mm-dd-yy'});
	});
</script>
<body>
	<table width="100%" border="0">
		<tr>
			<td align="center" ><img src="<?php echo URL_PORTAL.DIRECTORIO_IMAGENES; ?>logo_reporte.png" width="707" height="86" /></td>
		</tr>
		<tr>
			<td align="center" >
				<p><strong class="titulo">Reporte de beneficiarios Activos mayores de 19 a&ntilde;os (para inactivar).</strong></p>
				<p><strong>Parentescos: Hijos,Hermanos con capacidad de trabajo normal.</strong></p>
			</td>
		</tr>
		<tr>
			<td align="center">
				<form name="frmConsultar" id="frmConsultar" action="<?php echo URL_PORTAL.DIRECTORIO_CENTRO_REPORTES; ?>aportes/giro/reporte021.php" method="post" >
					<table>
						<tr>
							<th><label for='fecha'>Fecha de corte:</label></th>
							<td><input type='text' name='fecha' id='fecha' />
                            <input type="submit" value="Consultar" /><input type="hidden" name="consultar" id="consultar" value="1"/>
                            </td>
						</tr>
					</table>
				</form>
			</td>
		</tr>
	</table>
</body>