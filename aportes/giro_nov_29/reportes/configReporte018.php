<?php
session_start ();
include_once($_SESSION["RAIZ"] . "config.php");
include(DIRECTORIO_CLASES."p.aportes.class.php");
$objAportes = new Aportes();
$resultPeriodos = $objAportes->obtener_periodos_antiguos(12);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>::Reporte de giros por meses para auditor&iacute;a::</title>
	<link type="text/css" href="<?php echo URL_PORTAL . DIRECTORIO_ESTILOS_CSS; ?>Estilos.css" rel="stylesheet"/>
	<link type="text/css" href="<?php echo URL_PORTAL . DIRECTORIO_ESTILOS_CSS; ?>estilo_tablas.css" rel="stylesheet"/>
	<link type="text/css" href="<?php echo URL_PORTAL . DIRECTORIO_ESTILOS_CSS_FORMULARIOS; ?>formularios/demos.css" />
<link type="text/css" href="<?php echo URL_PORTAL . DIRECTORIO_ESTILOS_CSS; ?>estiloReporte.css" rel="stylesheet"/>
</head>
<table width="100%" border="0">
	<tr>
		<td align="center"><img src="<?php echo URL_PORTAL . DIRECTORIO_IMAGENES;?>logo_reporte.png" width="707" height="86" /></td>
	</tr>
	<tr>
		<td align="center" ><strong class="titulo">Reporte de giros por meses para auditor&iacute;a</strong></td>
	</tr>
	<tr>
		<td align="center">
			<form name="frm1" id="frm1" action="<?php echo URL_PORTAL.DIRECTORIO_CENTRO_REPORTES ."aportes/giro/reporte018.php"; ?>" method="post">
				<table class='tablaR hover'>
					<tr>
						<th>Per&iacute;odo:</th>
						<td>
							<select name="periodo" value="periodo">
								<?php while($periodo = mssql_fetch_array($resultPeriodos)):?>
								
								<option value="<?php echo $periodo["periodo"]; ?>"><?php echo $periodo["periodo"]; ?></option>
								<?php endwhile; ?>
							</select>
						</td>
					</tr>
					<tr>
						<td colspan="2"><input type="submit" value="Enviar" /></td>
					</tr>
				</table>
			</form>
		</td>
	</tr>
</table>