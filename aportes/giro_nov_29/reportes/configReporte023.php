<?php 
include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'session.php';
session();

include_once(RUTA_FISICA_DIRECTORIO_CLASES . "p.certificados.class.php");
$fechaHoy = date("Y-m-d");
$fechaHoyCompleta = date("Y-m-d h:i:s");
$objCertificados = new Certificados();
$resultCertificados = $objCertificados->obtener_tipos_certificados_por_codigos(array(55,56,57,58));
global $arregloAgencias;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Reporte de afiliados por seccional</title>
	<link type="text/css" href="<?php echo URL_PORTAL.DIRECTORIO_ESTILOS_CSS_FORMULARIOS; ?>base/ui.all.css" rel="stylesheet" />
	<link type="text/css" href="<?php echo URL_PORTAL.DIRECTORIO_ESTILOS_CSS; ?>Estilos.css" rel="stylesheet"/>
	<link type="text/css" href="<?php echo URL_PORTAL.DIRECTORIO_ESTILOS_CSS; ?>estilo_tablas.css" rel="stylesheet"/>
	<link type="text/css" href="<?php echo URL_PORTAL.DIRECTORIO_ESTILOS_CSS_FORMULARIOS; ?>demos.css" />
</head>
<body>
	<table border="0"  class='tablaR hover' align="center">
		<tr>
			<td align="center" ><img src="<?php echo URL_PORTAL.DIRECTORIO_IMAGENES; ?>logo_reporte.png" width="707" height="86" /></td>
		</tr>
		<tr>
			<td align="center" ><strong style="font-weigth:bold;font-size:10pt">Reporte de afiliados por seccional</strong></td>
		</tr>
		<tr>
			<td align="center">
				<form name="frmConsultar" id="frmConsultar" action="<?php echo URL_PORTAL.DIRECTORIO_CENTRO_REPORTES; ?>aportes/giro/reporte023.php" method="post" >
					<table align="center">
						<tr>
							<th><label for="idagencia">Seccional:</label></th>
							<td>
								<select name="idagencia" id="idagencia" >
									<?php foreach ($arregloAgencias as $codigo => $nomAgencia): ?>
									<option value="<?php echo $codigo ?>"><?php echo $nomAgencia ?></option>
									<?php endforeach; ?>
								</select> 
                                <input type="submit" value="Consultar" /><input type="hidden" name="consultar" id="consultar" value="1"/>
							</td>
						</tr>
					</table>
				</form>
			</td>
		</tr>
	</table>
</body>