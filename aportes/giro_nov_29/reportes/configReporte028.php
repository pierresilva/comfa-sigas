<?php
include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'session.php';
session();

$fechaHoy = date("Y-m-d");
$fechaHoyCompleta = date("Y-m-d h:i:s");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>::Reporte de novedades por empresa::</title>
	<link type="text/css" href="<?php echo URL_PORTAL.DIRECTORIO_ESTILOS_CSS_FORMULARIOS ?>base/ui.all.css" rel="stylesheet" />
	<link type="text/css" href="<?php echo URL_PORTAL.DIRECTORIO_ESTILOS_CSS; ?>Estilos.css" rel="stylesheet"/>
	<link type="text/css" href="<?php echo URL_PORTAL.DIRECTORIO_ESTILOS_CSS; ?>estilo_tablas.css" rel="stylesheet"/>
	<link type="text/css" href="<?php echo URL_PORTAL.DIRECTORIO_ESTILOS_CSS; ?>formularios/demos.css" />
	<link type="text/css" href="<?php echo URL_PORTAL.DIRECTORIO_ESTILOS_CSS; ?>estiloReporte.css" rel="stylesheet"/>
	<script type="text/javascript" src="<?php echo URL_PORTAL.DIRECTORIO_SCRIPTS_JS; ?>jquery-1.3.2.min.js"></script>
	<script type="text/javascript" src="<?php echo URL_PORTAL.DIRECTORIO_SCRIPTS_JS_FORMULARIOS; ?>ui/ui.core.js"></script>
	<script type="text/javascript" src="<?php echo URL_PORTAL.DIRECTORIO_SCRIPTS_JS; ?>comunes.js"></script>
</head>
<script>
var urlPortal = src();
$(document).ready(function(){
	
	$("#nitBuscar").keyup(function(){
		$("#empresas").html("");
	});
	
	$("#nitBuscar").blur(function(){
		buscarEmpresa();
	});
	
	$("#buscar").click(function(){
		buscarEmpresa();
	});
	
	$("#frm1").submit(function(){
		if( isNaN($("#nitBuscar").val()) ){
			alert("El NIT debe ser num\u00E9rico.");
			return false;
		}
		
	});
	
	var buscarEmpresa = function (){
		if( isNaN($("#nitBuscar").val()) ){
			alert("El NIT debe ser num\u00E9rico.");
			return false;
		}
		
		$("#empresas").html("Buscando...");
		var nit=$("#nitBuscar").val();
		$.getJSON(urlPortal+'aportes/empresas/buscarTodas.php',{v0:nit.toUpperCase(),v1:1},function(data){
			$("#empresas").html("");
			if(data!=''){
				$.each(data,function(i,fila){
					$("#empresas").append('<input type="radio" name="idEmpresa" id="idEnviar_'+ fila.idempresa +'" value="'+ fila.idempresa +'" > <label for="idEnviar_'+ fila.idempresa +'" >'+fila.nit+" - "+fila.razonsocial+'</label><br/>');
				});
			}else{
				$("#empresas").html("No se encontraron empresas");
			}
		});
	};
	
});//Fin ready
</script>
<table width="100%" border="0">
	<tr>
        <td align="left" ><img src="<?php echo URL_PORTAL.DIRECTORIO_IMAGENES; ?>logo_comfamiliar.jpg" width="75%"/></td>
        <td align="center" ><img src="<?php echo URL_PORTAL.DIRECTORIO_IMAGENES; ?>logo_reporte.png" width="32" height="32" /></td>
	</tr>
	<tr>
		<td align="center" colspan="2"><strong style="font-weight:bold;">Reporte de giro 028 - Reporte planilla corporaci&oacute;n</strong></td>
	</tr>
	<tr>
		<td align="center" colspan="2">
			<form name="frm1" id="frm1" action="<?php echo URL_PORTAL.DIRECTORIO_CENTRO_REPORTES; ?>aportes/giro/reporte028.php" method="post" >
				<table class="tablaR hover">
					<tr>
						<th>Opci&oacute;n del reporte</th>
						<td align="left" colspan="2" style="text-align:left">
							<input type="radio" name="opcionReporte" id="opcionReporte_todos" value="todos"/> <label for="opcionReporte_todos">Todos</label><br>
							<input type="radio" name="opcionReporte" id="opcionReporte_nit" value="nit"/> <label for="opcionReporte_nit">Nit.</label>
						</td>
					</tr>
					<tr>
						<th>Nit</th>
						<td><input type="text" name="nitBuscar" id="nitBuscar" /></td>
						<td><input type="button" name="buscar" id="buscar" value="Buscar" /></td>
					</tr>
					<tr>
						<th colspan="3">Empresas</th>
					</tr>
					<tr>
						<td colspan="3" id="empresas" style="text-align:left">
						</td>
					</tr>
					<tr>
						<td align="center" colspan="3"><input type="submit" value="Consultar" /></td>
					</tr>
				</table>
			</form>
		</td>
	</tr>
</table>