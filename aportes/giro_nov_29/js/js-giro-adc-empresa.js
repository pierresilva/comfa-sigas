var pb = null;
var pg = 0;

var giroAdicionalEmpresa = function() {
	pb = $('#progreso-adcind').progressbar( {
		value : 0
	});
	var par = $('.ide:checked').serialize()+'&acc=2&'+$('#txtAEPeriodo').serialize();
	$.ajax({
		url : "giroAdicionalEmpresa.php",
		async : true,
		type : 'POST',
		data: par,
		success : function(data) {
			$('#giro-pros-adcind').append(data);
			$(pb).progressbar("option", "value", 100);
		}
	});

	/*pg = $(pb).progressbar("option", "value");
	while (pg != 100) {
		$.ajax({
			url : "progBarGiroNormal.php",
			async : false,
			type : 'GET',
			context: $(pb),
			data : ( {
				control : $('#txtCodControl').val(),
				periodo : $('#txtPeriodo').val()
			}),
			success : function(da) {
				da *= 1;
				pg = $(pb).progressbar("option", "value");
				if(pg < 100){
					$(this).progressbar("option", "value", da);
				}
			}
		});
	}*/
};

var anularGiroAdicionalIndividual = function(){
	pb = $('#progreso-adcind').progressbar({
		value : 0
	});
	$.ajax({
		url : "giroAdicionalIndividual.php",
		async : true,
		type : 'POST',
		data: ({acc:'2'}),
		context : $('#giro-pros-adcind'),
		success : function(data) {
			$(this).append(data);
			$(pb).progressbar("option", "value", 100);
		}
	});
};