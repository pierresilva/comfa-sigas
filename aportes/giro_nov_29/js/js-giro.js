var pb = null;
var pg = 0;
var tid = null;

var giroNormal = function() {
	
	pb = $('#progreso').progressbar( {
		value : 0
	});
	
	$.ajax({
		url : "basicaGiroNormal.php",
		async : true,
		type : 'POST',
		data : ({
			acc : '1'
		}),
		context : $('#giro-pros'),
		success : function(data) {
			$('#txtResumen').html(data);
			clearInterval(tid);
			$(pb).progressbar("option", "value", 100);
			// console.log(data);
		}
	});
	
	pg = $(pb).progressbar("option", "value");
	/*while (pg != 100) {
		$.ajax({
			url : "progBarGiroNormal.php",
			async : false,
			type : 'GET',
			context: $(pb),
			data : ( {
				control : $('#txtCodControl').val(),
				periodo : $('#txtPeriodo').val()
			}),
			success : function(da) {
				da *= 1;
				pg = $(pb).progressbar("option", "value");
				if(pg < 100){
					$(pb).progressbar("option", "value", da);
				}
			}
		});
	}*/
	
	/*tid = setInterval(function() {
		$.ajax({
			url : "progBarGiroNormal.php",
			async : false,
			type : 'GET',
			context: $(pb),
			data : ( {
				control : $('#txtCodControl').val(),
				periodo : $('#txtPeriodo').val()
			}),
			success : function(da) {
				da *= 1;
				pg = $(pb).progressbar("option", "value");
				if(pg < 100){
					$(pb).progressbar("option", "value", da);
				}
			}
		});
	}, 5000);*/

};

var anularGiroActual = function() {
	pb = $('#progreso').progressbar({
		value : 0
	});
	$.ajax({
		url : "basicaGiroNormal.php",
		async : true,
		type : 'POST',
		data : ({
			acc : '2'
		}),
		context : $('#giro-pros'),
		success : function(data) {
			$(this).append(data);
			$(pb).progressbar("option", "value", 100);
		}
	});
};

var pasarHistorico = function() {
	pb = $('#progreso').progressbar({
		value : 0
	});
	$.ajax({
		url : "basicaGiroNormal.php",
		async : true,
		type : 'POST',
		data : ({
			acc : '3'
		}),
		context : $('#giro-pros'),
		success : function(data) {
			$(this).append(data);
			$(pb).progressbar("option", "value", 100);
		}
	});
};