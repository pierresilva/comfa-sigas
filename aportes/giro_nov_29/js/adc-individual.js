/**
 * 
 */
$('#btnGiroAdicIndividual').bind('click', function() {
	var peract = $.trim($('#txtAIPeriodoActual').val()) * 1;
	var pergir = $.trim($('#txtAIPeriodo').val()) * 1;
	var slttipid = $('#sltTipIdentificacion').val();
	var noid = $.trim($('#txtNoIdentificacion').val());
	if (pergir == 0) {
		alert('El periodo a girar es obligatorio');
	}else if (pergir > peract) {
		alert('No se puede girar un periodo posterior al actual');
	}else if(slttipid == 0){
		alert('El tipo de documento es obligatorio');
	}else if(noid.length == 0){
		alert('El n�mero de documento es obligatorio');
	}else{
		giroAdicionalIndividual();
	}
});
$('#btnDeshGiroAdicIndividual').bind('click', function() {
	alert("Esta acci�n no se puede deshacer");
	if (confirm("Esta seguro de deshacer el giro actual?")) {
		// anularGiroActual();
	}
});
$('#txtNoIdentificacion').bind('blur', function(){
	$('#div-info').remove();
	$('#progreso-adcind').progressbar('destroy');
	var noid = $.trim($(this).val());
	if(noid.length == 0){
		alert('Debe digitar el n�mero de indentificaci�n');
	}else{
		$.getJSON('giroAdicionalIndividual.php', ({acc:'1', id:$('#txtNoIdentificacion').val(), tipid: $('#sltTipIdentificacion').val()}),function(data){
			if(data != 0){
				$('#txtIdPersona').val(data[0].idpersona);
				$('#giro-pros-adcind').append('<div class="ui-widget" id="div-info" style="margin: 1em 0"><div class="ui-widget-header"><center>Giro adicional trabajador</center></div><div class="ui-widget-content">'+data[0].pnombre+' '+data[0].snombre+' '+data[0].papellido+' '+data[0].sapellido+'</div></div>');
			}else{
				alert('No se encuentra el trabajador');
			}
		});
	}
});
