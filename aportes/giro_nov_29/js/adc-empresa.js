/**
 * 
 */
$('#btnGiroAdicEmpresa').bind('click', function() {
	var peract = $.trim($('#txtAEPeriodoActual').val()) * 1;
	var pergir = $.trim($('#txtAEPeriodo').val()) * 1;
	var noid = $.trim($('#txtNoIdEmpresa').val());
	if (pergir == 0) {
		alert('El periodo a girar es obligatorio');
	}else if (pergir > peract) {
		alert('No se puede girar un periodo posterior al actual');
	}else if(noid.length == 0){
		alert('El n�mero de documento es obligatorio');
	}else{
		giroAdicionalEmpresa();
	}
});
$('#btnDeshGiroAdicEmpresa').bind('click', function() {
	alert("Esta acci�n no se puede deshacer");
	if (confirm("Esta seguro de deshacer el giro actual?")) {
		// anularGiroActual();
	}
});
$('#txtNoIdEmpresa').bind('blur', function(){
	$('#div-info').remove();
	$('#progreso-adcemp').progressbar('destroy');
	var noid = $.trim($(this).val());
	if(noid.length == 0){
		alert('Debe digitar el n�mero de identificaci�n de la empresa');
	}else{
		$.getJSON('giroAdicionalEmpresa.php', ({acc:'1', id:$('#txtNoIdEmpresa').val()}),function(data){
			if(data != 0){
				//$('#txtIdEmpresa').val(data[0].idempresa);
				$('#giro-pros-adcemp').append('<div class="ui-widget" id="div-info" style="margin: 1em 0"><div class="ui-widget-header"><input type="checkbox" name="chkideall" id="chkideall" /><center>Giro adicional empresa</center></div></div>');
				$.each(data, function(i, dat){
					$('#div-info').append('<div class="ui-widget-content" style="clear:both"><input type="checkbox" id="chk'+dat.idempresa+'" name="ide'+dat.idempresa+'" class="ide" value="ide'+dat.idempresa+'" />'+dat.idempresa+' - '+dat.razonsocial+' - '+dat.estado+'</div>');	
				});
				$('#chkideall').bind('click', function(){
					if($(this).is(':checked')){
						$('.ide').attr('checked','checked');
					}else{
						$('.ide').attr('checked','');
					}
				})
			}else{
				alert('No se encuentra la empresa');
			}
		});
	}
});

