/**
 * Acciones iniciales del formulario de nominas por planilla unica
 *
 * @author Jose Luis Rojas
 * @version 1.0.0
 * @since 24 Agosto 2010
 */
$(document).ready(function(){
	$('#btnGiroNormal').bind('click',giroNormal);
	$('#btnDeshGiro').bind('click',function(){
		alert("Esta acci�n no se puede deshacer");
		if(confirm("Esta seguro de deshacer el giro actual?")){
			anularGiroActual();
		}
	});
	$('#btnHistGiro').bind('click',function(){
		alert("Esta acci�n no se puede deshacer");
		if(confirm("Esta seguro de pasar el giro actual a historico?")){
			pasarHistorico();
		}
	});
	$('#tabs').tabs({ cache: true });
});