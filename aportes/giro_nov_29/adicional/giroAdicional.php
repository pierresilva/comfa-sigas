<?php

include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'session.php';
session();
require_once ('clases/Consultor.php');
$con = new Consultor();
$sql = "SELECT TOP 1 periodo, fechalimite, controlproceso, estadogiro FROM aportes012 WHERE procesado = 'N' ORDER BY periodo ASC";
$dper1 = $con->enArray($sql);
$sql = "SELECT TOP 1 fechalimite FROM aportes012 WHERE procesado = 'S' ORDER BY periodo DESC";
$dper2 = $con->enArray($sql);

?>
<script type="text/javascript" src="adicional/js/ui-adicional.js"
	language="javascript"></script>
<script type="text/javascript" src="adicional/js/js-adicional.js"
	language="javascript"></script>
<script>
$("#tabs-reclamos").tabs();
</script>
<div id="tabs-reclamos">
<ul>
	<li><a href="#div-rec-tra">Reclamos Trabajador</a></li>
	<li><a href="#div-rec-emp">Reclamos Empresas</a></li>
</ul>
<div id="div-rec-tra">
<table id="tblReclamosTrabajador" border="0" cellspacing="0"
	class="box-table-a" style="margin: 1em auto;">
	<tbody>
		<tr></tr>
	</tbody>
	<thead>
		<tr>
			<th><strong>id</strong></th>
			<th><strong>No.Ident</strong></th>
			<th><strong>Nombre</strong></th>
			<th><strong>Per.Inicial</strong></th>
			<th><strong>Per.Final</strong></th>
			<th><strong>Cod.Causal</strong></th>
			<th><strong>Fec.Reclamo</strong></th>
			<th><strong>Usu.Reclamo</strong></th>
			<th><input type="checkbox" id="chk-all-tra" name="chk-all"
				class="tra" /></th>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<td colspan="9">
			<p align="center">
				<input type="button" id="btnGiroReclamosTrabajador" name="btnGiroReclamosTrabajador" value="Procesar Giro" <?php echo $dper1[0]['estadogiro']=='C'?'Disabled="Disabled"':'' ?> />
				<input type="button" id="btnDeshGiroAdicIndividual" name="btnDeshGiroAdicIndividual" value="Deshacer Giro" <?php echo $dper1[0]['estadogiro']=='C'?'Disabled="Disabled"':'' ?> />
			</p>
			</td>
		</tr>
	</tfoot>
</table>
</div>
<div id="div-rec-emp">
<table id="tblReclamosEmpresas" border="0" cellspacing="0"
	class="box-table-a" style="margin: 1em auto;">
	<tbody>
		<tr></tr>
	</tbody>
	<thead>
		<tr>
			<th><strong>id</strong></th>
			<th><strong>Nit</strong></th>
			<th><strong>Raz&oacute;n social</strong></th>
			<th><strong>Per.Inicial</strong></th>
			<th><strong>Per.Final</strong></th>
			<th><strong>Cod.Causal</strong></th>
			<th><strong>Fec.Reclamo</strong></th>
			<th><strong>Usu.Reclamo</strong></th>
			<th><input type="checkbox" id="chk-all-emp" name="chk-all"
				class="emp" /></th>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<td colspan="9">
			<p align="center"><input type="button" id="btnGiroReclamosEmpresa"
				name="btnGiroReclamosEmpresa" value="Procesar Giro" /><input
				type="button" id="btnDeshGiroAdicIndividual"
				name="btnDeshGiroAdicIndividual" value="Deshacer Giro" /></p>
			</td>
		</tr>
	</tfoot>
</table>
</div>
</div>