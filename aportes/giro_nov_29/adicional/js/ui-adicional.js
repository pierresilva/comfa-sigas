/**
 * 
 */
$(function(){
	$('#tabs-reclamos').bind( "tabsshow", function(event, ui) {
		var tabSel = $('#tabs-reclamos').tabs( "option", "selected" );
		if(tabSel == 0){
			reclamosTrabajadorPendientes();
		}else if(tabSel == 1){
			reclamosEmpresaPendientes();
		}
	});
	
	$('#chk-all-tra, #chk-all-emp').bind('click',function(){
		var id = $(this).attr('id');
		var clase = $(this).attr('class');
		
		if($(this).is(':checked')){
			$(this).attr('checked','checked');
			$(':checkbox[id!="'+id+'"]').filter('.'+clase).attr('checked','checked');
		}else{
			$(this).attr('checked','');
			$(':checkbox[id!="'+id+'"]').filter('.'+clase).attr('checked','');
		}
	});
	
	$('#btnGiroReclamosTrabajador').bind('click', function(){
		var par = $('.tra:checked').serialize();
		par += '&acc=3';
		$.post('adicional/prc-giroAdicional.php', par, function(data){
			alert(data);
		});
	});
	
	$('#btnGiroReclamosEmpresa').bind('click', function(){
		var par = $('.emp:checked').serialize();
		par += '&acc=4';
		$.post('adicional/prc-giroAdicional.php', par, function(data){
			alert(data);
		});
	});
});