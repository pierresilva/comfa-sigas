/**
 * 
 */
var reclamosTrabajadorPendientes = function(){
	$('#tblReclamosTrabajador tbody').html('<tr><td colspan="9"><img src="../../imagenes/ajax-loader-min.gif" alt=""/></td></tr>');
	$.getJSON('adicional/prc-giroAdicional.php',({acc:0}), function(data){
		$('#tblReclamosTrabajador tbody').html('');
		$(data).each(function(i){
			var nombre = data[i].pnombre+' '+data[i].snombre+' '+data[i].papellido+' '+data[i].sapellido; 
			$('#tblReclamosTrabajador tbody').append('<tr><td>'+data[i].idreclamo+'</td><td>'+data[i].identificacion+'</td><td>'+nombre+'</td><td>'+data[i].periodoinicial+'</td><td>'+data[i].periodofinal+'</td><td><a href="#" class="notTra" alt="'+data[i].detalledefinicion+ ' - '+data[i].notas +'">'+data[i].idcausal+'</a></td><td>'+data[i].fechareclamo+'</td><td>'+data[i].usuario+'</td><td><input type="checkbox" name="tra_'+data[i].idreclamo+'" class="tra" value="'+data[i].idreclamo+'" /></td></tr>')
		});
		$('.notTra').bind('click', function(){
			alert($(this).attr('alt'));
		});
	});
}

var reclamosEmpresaPendientes = function(){
	$('#tblReclamosEmpresas tbody').html('<tr><td colspan="9"><img src="../../imagenes/ajax-loader-min.gif" alt=""/></td></tr>');
	$.getJSON('adicional/prc-giroAdicional.php',({acc:1}), function(data){
		$('#tblReclamosEmpresas tbody').html('');
		$(data).each(function(i){
			$('#tblReclamosEmpresas tbody').append('<tr><td>'+data[i].idreclamo+'</td><td>'+data[i].nit+'</td><td>'+data[i].razonsocial+'</td><td>'+data[i].periodoinicial+'</td><td>'+data[i].periodofinal+'</td><td><a href="#" class="notEmp" alt="'+data[i].detalledefinicion+ ' - '+data[i].notas +'">'+data[i].idcausal+'</a></td><td>'+data[i].fechareclamo+'</td><td>'+data[i].usuario+'</td><td><input type="checkbox" name="emp_'+data[i].idreclamo+'" class="emp" value="'+data[i].idreclamo+'" /></td></tr>')
		});
		$('.notEmp').bind('click', function(){
			alert($(this).attr('alt'));
		});
	});
}