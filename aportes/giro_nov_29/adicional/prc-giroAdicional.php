<?php
include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'session.php';
session();

require_once ('clases/Consultor.php');
require_once ('../clases/Giro.php');

$acc = $_REQUEST['acc'];
$con = new Consultor();
if($acc == 0){
	//listar reclamos por trabajador	
	//$sql = "SELECT a55.idreclamo, a15.identificacion, a15.pnombre, a15.snombre, a15.papellido, a15.sapellido, a55.periodoinicial, a55.periodofinal, a55.idcausal, a55.notas, a55.fechareclamo, a55.usuario FROM aportes055 a55 INNER JOIN aportes015 a15 ON a55.idtrabajador=a15.idpersona WHERE a55.estado='A' ORDER BY fechareclamo";
	$sql = "SELECT a55.idreclamo, a15.identificacion, a15.pnombre, a15.snombre, a15.papellido, a15.sapellido, a55.periodoinicial, a55.periodofinal, a55.idcausal, a55.notas, a55.fechareclamo, a55.usuario, a91.detalledefinicion FROM aportes055 a55 INNER JOIN aportes015 a15 ON a55.idtrabajador=a15.idpersona INNER JOIN aportes091 a91 ON a91.iddetalledef=a55.idcausal WHERE a55.estado='A' ORDER BY fechareclamo";
	$dat = $con->enJSON($sql);
	//$dat['notas'] = $dat['notas'].' - '.$dat['detalledefinicion'];
	echo $dat;
}elseif($acc == 1){
	//listar reclamos por empresa
	//$sql = "SELECT a60.idreclamo, a48.nit, a48.razonsocial, a60.periodoinicial, a60.periodofinal, a60.idcausal, a60.notas, a60.fechareclamo, a60.usuario FROM aportes060 a60 INNER JOIN aportes048 a48 ON a60.idempresa=a48.idempresa WHERE a60.estado='A' ORDER BY fechareclamo";
	$sql = "SELECT a60.idreclamo, a48.nit, a48.razonsocial, a60.periodoinicial, a60.periodofinal, a60.idcausal, a60.notas, a60.fechareclamo, a60.usuario, a91.detalledefinicion FROM aportes060 a60 INNER JOIN aportes048 a48 ON a60.idempresa=a48.idempresa INNER JOIN aportes091 a91 ON a91.iddetalledef=a60.idcausal WHERE a60.estado='A' ORDER BY fechareclamo";
	$dat = $con->enJSON($sql);
	echo $dat;
}elseif($acc == 3){
	//Girar reclamos por trabajador
	$giro = new Giro('A');
	//$giro->definirTablaGiro('aportes014_p', 'aportes054_p');
	foreach($_REQUEST as $ind=>$idreclamo){
		$tip = explode('_', $ind);
		if($tip[0] == 'tra'){
			$giro->giroReclamoXTrabajador($idreclamo);
		}
	}
}elseif($acc == 4){
	//Girar reclamos por empresa
	$giro = new Giro('A');
	//$giro->definirTablaGiro('aportes014_p', 'aportes054_p');
	foreach($_REQUEST as $ind=>$idreclamo){
		$tip = explode('_', $ind);
		if($tip[0] == 'emp'){
			$giro->giroReclamoXEmpresa($idreclamo);
		}
	}
}
?>