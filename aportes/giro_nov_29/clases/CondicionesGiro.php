<?php
include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'session.php';
session ();

require_once ('Logs.php');

class CondicionesGiro {

	public static function validar(Afiliado &$afiliado, Parametros &$param) {

		$periodoGirar = $param->periodoGirar;
		$periodoActual = $param->periodoActual;
		$usuario = $param->usuario;
		$fechagiro = $param->fechaGiro;
		$tipoGiro = $param->tipoGiro;
		$fechaInicialPeriodo = $param->{$param->periodoGirar} ['fechaInicialPeriodo'];
		$fechaFinalPeriodo = $param->{$param->periodoGirar} ['fechaFinalPeriodo'];
		$tope4 = $param->{$param->periodoGirar} ['tope4'];
		$smlv = $param->{$param->periodoGirar} ['valorSMLV'];
		$minpago = (($smlv / 240) * 96);

		$remun = $param->tipoRemuneracion;
		$remun2 = ($param->tipoRemuneracion=='ingbase')?'totalIngBase':'totalSalBasico';
		$log = new Logs ();

		//$log->parametros(2, array('nombre'=>'errorescondicionesgiro.txt', 'ruta'=>'/var/www/html/logs_sigas/giro'));
		$parFile = array ('nombre' => 'errores.txt', 'ruta' => '..\..\..\planos_generados\giro' );
		$parBD = array ('periodoproceso' => 'TEXT', 'periodo' => 'TEXT', 'tipogiro' => 'TEXT', 'idempresa' => 'INT', 'idtrabajador' => 'INT', 'idconyuge' => 'INT', 'idbeneficiario' => 'INT', 'idcodigocausal' => 'INT', 'usuario' => 'TEXT', 'fechasistema' => 'TEXT' );
		$log->parametros ( 4, $parFile, 'aportes065', $parBD );

		$afiliado->obtenerAfiliacion ( $fechaInicialPeriodo, $fechaFinalPeriodo, $periodoGirar );

		if ($afiliado->condicionesXPeriodo [$periodoGirar] ['afiliacion'] != false) {

			$afiliado->obtenerNominas ( $periodoGirar );
			$afiliado->obtenerTopeIndividual ( $tope4, $smlv, $remun, $periodoGirar );

			if ($afiliado->giro == 48) {

				$afiliado->condicionesXPeriodo [$periodoGirar] ['edad'] = $afiliado->edad ( $fechaFinalPeriodo );

				if ($afiliado->estado == 'M') {
					//Los muertos se estan haciendo por beneficiarios al momento de girar... tener en cuenta revision, En caso de que el muerto sea el afiliado, se procesa de manera independiente
					return false;
				} else {

					if($afiliado->condicionesXPeriodo [$periodoGirar] ['nominas']->cantidadNominas != 0){

					//Banderas Nominas
					if ($afiliado->condicionesXPeriodo [$periodoGirar] ['nominas']->bndNovedades == true) {
						if (Utiles::fechaMayor ( $afiliado->condicionesXPeriodo [$periodoGirar] ['afiliacion'] ['fechaingreso'], $fechaFinalPeriodo ) == 1) {
							//Ingreso despues de la fecha final del periodo
							$ln = "El trabajador " . $afiliado->numeroDocumento . " ingreso despues de la fecha final del periodo";
							$datBD = array($periodoActual, $periodoGirar, $tipoGiro, $afiliado->idempresa, $afiliado->idpersona, 0, 0, 2897, $usuario, $fechagiro);
							$log->grabar($ln, $datBD);
							return false;
						} else {
							return true;
						}
					} else {
						//Verificacion de las 96 horas
						$fecing = (isset($afiliado->condicionesXPeriodo [$periodoGirar]['afiliacion']['fechaingreso']))?$afiliado->condicionesXPeriodo[$periodoGirar]['afiliacion']['fechaingreso']:'00/00/0000';
						$dias = Utiles::diasEntre ( $fechaFinalPeriodo,  $fecing);
						if ($afiliado->condicionesXPeriodo [$periodoGirar] ['nominas']->{$remun2} >= $minpago) {
							if ($dias >= 12) {
								if ($afiliado->condicionesXPeriodo [$periodoGirar] ['nominas']->totalHoras >= 96) {
									if (Utiles::fechaMayor ( $afiliado->condicionesXPeriodo [$periodoGirar] ['afiliacion'] ['fechaingreso'], $fechaFinalPeriodo ) == 1) {
										//Ingreso despues de la fecha final del periodo
										$ln = "El trabajador " . $afiliado->numeroDocumento . " ingreso despues de la fecha final del periodo";
										$datBD = array($periodoActual, $periodoGirar, $tipoGiro, $afiliado->idempresa, $afiliado->idpersona, 0, 0, 2897, $usuario, $fechagiro);
										$log->grabar($ln, $datBD);
										return false;
									} else {
										return true;
									}
								} else {
									//No trabajo las 96 horas
									$ln = "El trabajador " . $afiliado->numeroDocumento . " no le reportaron las 96 horas minimas";
									$datBD = array($periodoActual, $periodoGirar, $tipoGiro, $afiliado->idempresa, $afiliado->idpersona, 0, 0, 2901, $usuario, $fechagiro);
									$log->grabar($ln, $datBD);
									return false;
								}
							} else {
								//No trabajo por lo menos 12 dias antes de la fecha final del periodo
								$ln = "El trabajador " . $afiliado->numeroDocumento . " no trabajo por lo menos 12 dias (96 horas) antes de la fecha final del periodo";
								$datBD = array($periodoActual, $periodoGirar, $tipoGiro, $afiliado->idempresa, $afiliado->idpersona, 0, 0, 2900, $usuario, $fechagiro);
								$log->grabar($ln, $datBD);
								return false;
							}
						}else{
							//NO PAGO LO MINIMO POR LAS 96 HORAS
							$ln = "El trabajador " . $afiliado->numeroDocumento . " no aporto lo minimo por las 96 horas";
							$datBD = array($periodoActual, $periodoGirar, $tipoGiro, $afiliado->idempresa, $afiliado->idpersona, 0, 0, 2899, $usuario, $fechagiro);
							$log->grabar($ln, $datBD);
							return false;
						}
					}
				}else{
							$ln = "El trabajador " . $afiliado->numeroDocumento . " no tiene nomina para el periodo ".$periodoGirar;
							$datBD = array($periodoActual, $periodoGirar, $tipoGiro, $afiliado->idempresa, $afiliado->idpersona, 0, 0, 2924, $usuario, $fechagiro);
							$log->grabar($ln, $datBD);
							return false;
				}

			}

			} elseif(!isset($afiliado->giro)){
				//NO TIENE AFILIACION
				$ln = "El trabajador " . $afiliado->numeroDocumento . " no tiene afiliacion";
				$datBD = array($periodoActual, $periodoGirar, $tipoGiro, $afiliado->idempresa, $afiliado->idpersona, 0, 0, 2896, $usuario, $fechagiro);
				$log->grabar($ln, $datBD);
				return false;
			}else {
				//Afiliacion para servicios
				$ln = "El trabajador " . $afiliado->numeroDocumento . " afiliado para servicios";
				$datBD = array($periodoActual, $periodoGirar, $tipoGiro, $afiliado->idempresa, $afiliado->idpersona, 0, 0, 2898, $usuario, $fechagiro);
				$log->grabar($ln, $datBD);
				return false;
			}
		} else {
			//NO TIENE AFILIACION
			$ln = "El trabajador " . $afiliado->numeroDocumento . " no tiene afiliacion";
			$datBD = array($periodoActual, $periodoGirar, $tipoGiro, $afiliado->idempresa, $afiliado->idpersona, 0, 0, 2896, $usuario, $fechagiro);
			$log->grabar($ln, $datBD);
			return false;
		}
	}
}

?>
