<?php
include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'session.php';
session();

require_once ('clases/Consultor.php');


class Logs{
	
	private $modLog = 0;
	/**
	 * 
	 * @var unknown_type
	 */
	private $archivo = '';
	private $tabla = '';
	private $camposTabla = array();
	
	/**
	 * Parametros
	 * 
	 * Llama de acuerdo al numero de parametros el metodo necesario
	 * si 1 parametro -> Log visual, modlog
	 * si 2 parametros -> Log a archivo, modolog y array de archivo (nombre, ruta)
	 * si 3 parametros -> Log a base de datos, modlog, nombreTabla, array(campo=>tipodato) con el campo siendo la llave del array
	 * si 4 parametros -> Todos los tipos de logs
	 * si 5+ error
	 * 
	 * Grabar
	 * 
	 * @param unknown_type $nom
	 * @param unknown_type $args
	 */
	public function __call($nom, $args){
		if($nom == 'parametros'){
			if(count($args) == 1){
				$this->modLog = 1;
			}elseif(count($args) == 2){
				$this->modLog = 2;
				$this->paramArchivo($args[1]);
			}elseif(count($args) == 3){
				$this->modLog = 3;
				$this->paramBD($args[1], $args[2]);
			}elseif(count($args) == 4){
				$this->modLog = 4;
				$this->paramArchivo($args[1]);
				$this->paramBD($args[2], $args[3]);
			}else{
				//@todo Error
			}
		}elseif($nom == 'grabar'){
			if($this->modLog == 1){
				if(count($args) == 1){
					$this->mostrarLog($args[0]);
				}
			}elseif($this->modLog == 2){
				if(count($args) == 1){
					$this->escribirArchivo($args[0]);
				}
			}elseif($this->modLog == 3){
				if(count($args) == 1 && is_array($args[0])){
					$this->grabarBD($args[0]);
				}
			}elseif($this->modLog == 4){
				if(count($args) == 1){
					if (is_array($args[0])) {
						$txt = implode(',', $args[0]);
						$this->mostrarLog($txt);
						$this->escribirArchivo($txt);
						$this->grabarBD($args[0]);	
					}
				}elseif (count($args) == 2){
					if (is_string($args[0]) && is_array($args[1])){
						$this->mostrarLog($args[0]);
						$this->escribirArchivo($args[0]);
						$this->grabarBD($args[1]);	
					}
				}
			}else{
			
			}
		}else{
			//@todo error no se encuentra el metodo invocado
		}
	}
	
	private function paramArchivo($espcArchivo){
		$ds = DIRECTORY_SEPARATOR;
		$nombre = $espcArchivo['nombre'];
		$ruta = $espcArchivo['ruta'];
		$rutaArchivo = $ruta . $ds . $nombre;
		if(! is_dir($ruta)){
			if(! mkdir($ruta, 0777, true)){
				//@todo error no se pudo crear la ruta del archivo
			}
		}
		if(! is_file($rutaArchivo)){
			if(! touch($rutaArchivo)){
				//@todo error no se pudo crear el archivo
			}else{
				$this->archivo = $rutaArchivo;
			}
		}else{
			$this->archivo = $rutaArchivo;
		}
	}
	
	private function paramBD($tabla, $campos){
		$this->tabla = $tabla;
		$this->camposTabla = $campos;
	}
	
	private function mostrarLog($txt){
		echo date('m/d/Y h:i:s')." - $txt". '&#013;&#010;';
		ob_flush();
		flush();
	}
	
	private function escribirArchivo($txt){
		$fh = fopen($this->archivo, 'a');
		if($fh){
			$txt = date('m/d/Y h:i:s')." - $txt". "\r\n";
			fwrite($fh, $txt);
			fclose($fh);
		}else{
			//@todo no se pudo abrir el archivo
		}
	}
	
	private function grabarBD($campos = array()){
		if(count($campos) == count($this->camposTabla)){
			$con = new Consultor();
			$val = "'";
			$val .= implode("','", $campos);
			$val .= "'";
			
			$cmp = array_keys($this->camposTabla);
			$cmp = implode(',', $cmp);
			$sql = "INSERT INTO " . $this->tabla . "($cmp) VALUES($val)";
			return $con->insertId($sql); 
		}
	}

}

?>