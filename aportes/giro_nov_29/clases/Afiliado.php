<?php
/**
 * @author JOSE LUIS ROJAS
 * @version 1.0.0
 * @since 26 Octubre 2010
 */
include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'session.php';
session ();

require_once ('clases/Consultor.php');
require_once ('aportes/giro/clases/Persona.php');
require_once ('Relaciones.php');
require_once ('Afiliacion.php');
require_once ('Nominas.php');
require_once ('CondicionesGiro.php');
require_once ('Pignoracion.php');
require_once ('Embargo.php');

class Afiliado extends Persona {

	public $con = null;

	/*public $afiliacion;
	public $relaciones;
	public $nominas;
	public $idempresa;
	public $topeIndividual;
	public $cantSMLV;
	public $bndGiro = false;*/

	public $condicionesXPeriodo = array ();

	function __construct($idpersona = 0, $idempresa = 0) {
		$this->con = new Consultor ();
		$this->idpersona = $idpersona;
		$this->idempresa = $idempresa;
		$this->obtenerDatosPersona ();
	}

	/**
	 * Obtiene los datos basicos de la persona
	 *
	 * @param integer $idpersona
	 */
	public function obtenerDatosPersona() {
		$sql = "SELECT idtipodocumento, identificacion, sexo, fechanacimiento, discapacitado, estado FROM aportes015 WHERE idpersona=$this->idpersona";
		$dat = $this->con->enArray ( $sql );

		if ($dat != false && count ( $dat ) != 0) {
			//$this->idpersona = $idpersona;
			$this->discapacitado = $dat [0] ['discapacitado'];
			$this->estado = $dat [0] ['estado'];
			$this->fechaNacimiento = $dat [0] ['fechanacimiento'];
			$this->sexo = $dat [0] ['sexo'];
			$this->tipoDocumento = $dat [0] ['idtipodocumento'];
			$this->numeroDocumento = $dat [0] ['identificacion'];
		} else {
			return false;
		}
	}

	/**
	 * Obtiene los datos basicos de las personas por tipo y numero de identificacion
	 *
	 * @param integer $tipdoc
	 * @param string $numdoc
	 */
	public function obtenerDatosPersonaXDoc($tipdoc, $numdoc) {
		$sql = "SELECT idpersona, sexo, fechanacimiento, discapacitado, estado FROM aportes015 WHERE idtipodocumento=$tipdoc AND identificacion='$numdoc'";
		$dat = $this->con->enArray ( $sql );

		if ($dat != false && count ( $dat ) != 0) {
			$this->idpersona = $dat [0] ['idpersona'];
			$this->discapacitado = $dat [0] ['discapacitado'];
			$this->estado = $dat [0] ['estado'];
			$this->fechaNacimiento = $dat [0] ['fechanacimiento'];
			$this->sexo = $dat [0] ['sexo'];
			$this->tipoDocumento = $dat [0] ['idtipodocumento'];
			$this->numeroDocumento = $dat [0] ['identificacion'];
		} else {
			return false;
		}
	}

	public function obtenerRelaciones() {
		$this->relaciones = new Relaciones ( $this->idpersona );
		$this->relaciones->obtenerRelaciones ();
	}

	public function obtenerAfiliacion($fecIniper, $fecFinper, $periodo) {
		$afil = new Afiliacion ();
		$this->condicionesXPeriodo [$periodo] ['afiliacion'] = $afil->obtenerAfiliacion ( $this->idempresa, $this->idpersona, $periodo );
		$this->giro = ($this->condicionesXPeriodo [$periodo] ['afiliacion'] != false) ? $this->condicionesXPeriodo [$periodo] ['afiliacion'] ['tipoformulario'] : false;
		if ($this->idempresa == 0 && $this->condicionesXPeriodo [$periodo] ['afiliacion'] != false) {
			$this->idempresa = $this->condicionesXPeriodo [$periodo] ['afiliacion'] ['idempresa'];
		}else {
			//@todo error No se encontro afiliacion para el periodo a girar
			return false;
		}
		$this->condicionesXPeriodo [$periodo] ['afiliacion'] ['tope'] = true;
	}

	public function obtenerNominas($periodo) {
		$this->condicionesXPeriodo [$periodo] ['nominas'] = new Nominas ();
		$this->condicionesXPeriodo [$periodo] ['nominas']->obtenerBanderasNomina ( $this->idempresa, $this->idpersona, $periodo );
		$this->condicionesXPeriodo [$periodo] ['nominas']->obtenerValoresNominas ( $this->idpersona, $periodo );
	}

	public function obtenerTopeIndividual($tope4, $smlv, $tipSalario, $periodo) {
		//echo ($tipSalario == null)?$this->idpersona.'<br/>':'';
		$this->condicionesXPeriodo [$periodo] ['cantSMLV'] = 0;
		switch ($tipSalario) {
			case 'ingbase' :
				$tipSalario = 'totalIngBase';
				break;
			case 'salbasico' :
				$tipSalario = 'totalSalBasico';
				break;
			default :
				return false;
				break;
		}
		//$this->condicionesXPeriodo[$periodo]['nominas']->cantidadNominas
		/*if ($this->condicionesXPeriodo [$periodo] ['nominas']->bndNovedades == true) {
			$this->condicionesXPeriodo [$periodo] ['afiliacion'] ['tope'] = false;
			$this->condicionesXPeriodo [$periodo] ['cantSMLV'] = 1;
		} else {*/
		if ($this->condicionesXPeriodo [$periodo] ['nominas']->{$tipSalario} > 0) {
			if ($this->condicionesXPeriodo [$periodo] ['nominas']->{$tipSalario} > $tope4) {
				$this->condicionesXPeriodo [$periodo] ['afiliacion'] ['tope'] = true;
			} else {
				$this->condicionesXPeriodo [$periodo] ['afiliacion'] ['tope'] = false;
			}
			$this->condicionesXPeriodo [$periodo] ['cantSMLV'] = $this->condicionesXPeriodo [$periodo] ['nominas']->{$tipSalario} / $smlv;
		}else{
			$this->condicionesXPeriodo [$periodo] ['afiliacion'] ['tope'] = true;
		}
		//}
	}

	public function condicionesGiroIndividual(Parametros &$param) {
		$this->bndGiro = CondicionesGiro::validar ( $this, $param );
	}

	public function obtenerPignoracion() {
		$this->pignoracion = new Pignoracion ( $this->idpersona );
	}

	public function obtenerEmbargo() {
		$this->embargo = new Embargo ( $this->idpersona );
	}

	public function obtenerAportesIndividual($aportes = array()) {
		if (count ( $aportes ) > 0) {
			foreach ( $aportes as $ap ) {
				if (array_search ( $this->idempresa, $ap )) {
					return true;
				}
			}
			return false;
		} else {
			//@todo Error, no se encontraron los aportes de la empresa
			return false;
		}
	}
}

?>