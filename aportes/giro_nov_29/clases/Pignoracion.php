<?php
/**
 *
 *
 * @author admin
 * @todo Parametrizacion del tipo de cobro de acuerdo al convenio, se cobra a algunos parentescos
 */
include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'session.php';
session();

class Pignoracion{

	private $estado = null;
	private $valor = 0;
	private $saldo = 0;
	private $periodoInicial = null;
	private $anulado = null;
	public $pagare = null;
	public $con = null;
	private $convenio = null;
	public $valorCobrar = 0;
	public $idpignoracion = 0;

	function __construct($idpersona){
		$this->con = new Consultor();
		$sql = "SELECT idpignoracion, valorpignorado, idconvenio, estado, saldo, pagare, anulado, periodoinicia FROM aportes043 WHERE idtrabajador=$idpersona AND estado = 'A'";

		$dat = $this->con->enArray($sql);

		if($dat != false && count($dat) != 0){
			$this->idpignoracion = $dat[0]['idpignoracion'];
			$this->estado = $dat[0]['estado'];
			$this->valor = $dat[0]['valorpignorado'];
			$this->saldo = $dat[0]['saldo'];
			$this->periodoInicial = $dat[0]['periodoinicia'];
			$this->anulado = $dat[0]['anulado'];
			$this->pagare = $dat[0]['pagare'];
			$this->convenio = $dat[0]['idconvenio'];
		}
	}

	/**
	 *
	 *
	 * @param integer $valSubsidio
	 */
	public function cuotaMes($valSubsidio){
		if($this->saldo > 0){
			if($this->saldo >= $valSubsidio){
				$this->valorCobrar = $valSubsidio;
			}else{
				$this->valorCobrar = $this->saldo;
			}
		}else{
			//no tiene saldo por cobrar, si la pignoracion esta activa se debe inactivar
			$sql = "UPDATE aportes043 SET estado = 'I', saldo=0 WHERE idpignoracion=$this->idpignoracion";
			$this->con->noRetorno($sql);
		}
	}

	/**
	 *
	 */
	public function confirmarCobro($idbeneficiario, $periodo, $periodoproceso, $usuario){
		if(intval($periodo) >= intval($this->periodoInicial) && $this->saldo > 0){
			$sql = "INSERT INTO aportes044(idpignoracion,idbeneficiario,periodo,periodoproceso,valor,usuario,fechasistema) ";
			$sql .= "VALUES($this->idpignoracion, $idbeneficiario, '$periodo', '$periodoproceso', $this->valorCobrar, '$usuario', cast(getdate() as date))";
			$estCobPig = $this->con->noRetorno($sql);
			if(! $estCobPig){
				$this->saldo =+ $this->valorCobrar;
				$this->con->noRetorno('ROLLBACK TRANSACTION');
				return false;
			}else{
				$this->saldo =- $this->valorCobrar;
				if($this->saldo == 0){
					$sql = "UPDATE aportes043 SET estado = 'I', saldo='" . $this->saldo . "' WHERE idpignoracion=$this->idpignoracion";
					$estPig = $this->con->noRetorno($sql);
					if(! $estPig){
						$this->saldo =+ $this->valorCobrar;
						$this->con->noRetorno('ROLLBACK TRANSACTION');
						return false;
					}else{
						return true;
					}
				}
				return true;
			}
		}else{
			return true;
		}
	}
}

?>