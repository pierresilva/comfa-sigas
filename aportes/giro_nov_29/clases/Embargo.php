<?php
include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'session.php';
session();

class Embargo{
	
	private $con = null;
	public $embargantes = array();
	
	function __construct($idpersona){
		$this->con = new Consultor();
		$sql = "SELECT a18.idembargo, a18.idtercero, a18.idconyuge, a18.periodoinicio, a18.tipopago, a18.codigopago, a18.estado, a22.idbeneficiario FROM aportes018 a18 LEFT JOIN aportes022 a22 ON a18.idembargo=a22.idembargo WHERE a18.idtrabajador=$idpersona AND a18.estado='A'";
		
		$dat = $this->con->enArray($sql);
		
		if($dat != false && count($dat) != 0){
			foreach($dat as $emb){
				if(strlen($emb['idbeneficiario']) > 0){
					$this->embargantes[$emb['idbeneficiario']] = array('idembargo'=>$emb['idembargo'], 'idconyuge'=>$emb['idconyuge'], 'idtercero'=>$emb['idtercero'], 'periodoinicio'=>$emb['periodoinicio'], 'tipopago'=>$emb['tipopago'], 'codigopago'=>$emb['codigopago']);
				}
			}
		}
	}
	
	/**
	 * Busca si el beneficiario a girar tiene embargo por parte de la conyuge, 
	 * devuelve los datos del embargo en caso que si, sino, devuelve falso 
	 * 
	 * @param integer $idbeneficiario
	 */
	public function buscarEmbargoBeneficiario($idbeneficiario){
		if(array_key_exists($idbeneficiario, $this->embargantes)){
			return $this->embargantes[$idbeneficiario];
		}else{
			return false;
		}
	}
}

?>