<?php
include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'session.php';
session();

abstract class Persona{

	public $idpersona;
	public $tipoDocumento;
	public $numeroDocumento;
	public $estado;
	public $fechaNacimiento;
	public $edad;
	public $sexo;
	public $discapacitado;
	public $giro;

	/**
	 * Se obtienen los datos basicos de la persona
	 *
	 * @param integer $idpersona
	 */
	public abstract function obtenerDatosPersona();

	public function obtenerDatosBeneficiario($idtrabajador){}

	/**
	 * Genera la edad  para el periodo dado
	 *
	 * @param string $fecFinperiodo Fecha final del periodo, usualmente el ultimo dia del mes
	 */
	public function edad($fecFinperiodo){
		$f1bnd = preg_match('%\A(?:(19|20)[0-9]{2}[-](0[1-9]|1[012])[-](0[1-9]|[12][0-9]|3[01]))\Z%', $fecFinperiodo);
		$f2bnd = preg_match('%\A(?:(19|20)[0-9]{2}[-](0[1-9]|1[012])[-](0[1-9]|[12][0-9]|3[01]))\Z%', $this->fechaNacimiento);

		if($f1bnd && $f2bnd){
			$ff = explode('-', $fecFinperiodo);
			$ffpu = mktime(0, 0, 0, $ff[1], $ff[2], $ff[0]);
			$fn = explode('-', $this->fechaNacimiento);
			$fnu = mktime(0, 0, 0, $fn[1], $fn[2], $fn[0]);

			$this->edad = ($ffpu - $fnu) / (60 * 60 * 24 * 365.25);
		}else{
			return 0;
		}

	}

	//public abstract function defuncion(){}
}

?>