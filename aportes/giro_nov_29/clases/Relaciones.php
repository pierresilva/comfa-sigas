<?php

/**
 * @author JOSE LUIS ROJAS
 * @version 1.0.0
 * @since 27 Octubre 2010
 *
 */

include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'session.php';
session();

//$raiz = $_SESSION ['RAIZ'];
//chdir ( $raiz );

require_once ('clases/Consultor.php');

class Relaciones{

	private $con = null;
	private $idpersona;

	public $idconyuge;
	public $cantConyuges;
	public $conyuges;
	public $cantHijos;
	public $cantHermanos;
	public $cantPadres;
	public $grupoFamiliar = array();

	function __construct($idpersona){
		$this->con = new Consultor();
		$this->idpersona = $idpersona;
	}

	public function obtenerRelaciones(){
		//$sql = "SELECT idbeneficiario, idparentesco, idconyuge, conviven, giro FROM aportes021 WHERE idtrabajador=$this->idpersona";
		$sql = "SELECT a21.idbeneficiario, idparentesco, idconyuge, conviven, giro, a20.iddefuncion, a20.idtercero, a20.tipopago, a20.codigopago, LEFT(convert(varchar, a19.fechadefuncion, 112),6) AS perdef, 'N' AS procesado FROM aportes021 a21 LEFT JOIN aportes020 a20 ON a21.idbeneficiario=a20.idbeneficiario AND a20.procesado='N' LEFT JOIN aportes019 a19 ON a19.iddefuncion = a20.iddefuncion WHERE a21.idtrabajador=$this->idpersona";
		$g = $this->con->enArray($sql);
		if($g != false && count($g) > 0){
			foreach ($g as $gr) {
				if($gr['idparentesco'] == '34' && $gr['conviven'] == 'S'){
					$this->idconyuge = $gr['idbeneficiario'];
				}
				$this->grupoFamiliar[] = $gr;
			}
			/**
			 * Existe grupo familiar se busca cuantos tiene de cada tipo de parentesco
			 */
			//$this->cantConyugesPermanentes();
			$this->cantConyuges();
			$this->cantHermanos();
			$this->cantHijos();
			$this->cantPadres();
		}
	}

	private function cantConyuges(){
		$sql = "SELECT DISTINCT idconyuge FROM aportes021 WHERE idtrabajador=$this->idpersona AND idparentesco=34";
		$c = $this->con->enArray($sql);
		foreach ($c as $con) {
			$this->conyuges[] = $con['idconyuge'];
		}
		$this->cantConyuges = count($this->conyuges);
	}


	private function cantConyugesPermanentes(){
		$sql = "SELECT COUNT(idconyuge) AS cant FROM aportes021 WHERE idtrabajador=$this->idpersona AND idparentesco=34 AND conviven='S'";
		$c = $this->con->enArray($sql);
		$this->cantConyuges = intval($c[0]['cant']);
	}

	private function cantHijos(){
		$sql = "SELECT COUNT(idbeneficiario) AS cant FROM aportes021 WHERE idtrabajador=$this->idpersona AND idparentesco in (35,38)";
		$c = $this->con->enArray($sql);
		$this->cantHijos = intval($c[0]['cant']);
	}

	private function cantHermanos(){
		$sql = "SELECT COUNT(idbeneficiario) AS cant FROM aportes021 WHERE idtrabajador=$this->idpersona AND idparentesco=37";
		$c = $this->con->enArray($sql);
		$this->cantHermanos = intval($c[0]['cant']);
	}

	private function cantPadres(){
		$sql = "SELECT COUNT(idbeneficiario) AS cant FROM aportes021 WHERE idtrabajador=$this->idpersona AND idparentesco=36";
		$c = $this->con->enArray($sql);
		$this->cantPadres = intval($c[0]['cant']);
	}
}

?>
