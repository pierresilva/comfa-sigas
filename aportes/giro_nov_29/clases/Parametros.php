<?php
/**
 * @author JOSE LUIS ROJAS
 * @version 1.0.0
 * @since 26 Octubre 2010
 */
include_once ('phpComunes' . DIRECTORY_SEPARATOR . 'session.php');
session();

//$raiz = $_SESSION ['RAIZ'];
//chdir ( $raiz );

require_once ('clases/Consultor.php');
date_default_timezone_set('America/Bogota');

class Parametros{

	private $con;

	private $param = array();

	/**
	 *
	 * Variables usadas para los parametros
	 *
	 * private periodoActual;
	 * private fechaInicialPeriodo;
	 * private fechaFinalPeriodo;
	 * private fechaInicialPago;
	 * private fechaFinalPago;
	 * private valorSMLV;
	 * private valorCuota;
	 * private valorCuotaCampo;
	 * private tokenControlGiro;
	 */

	function __construct(){
		$this->con = new Consultor();
	}

	public function __get($prm){
		if(array_key_exists($prm, $this->param)){
			return $this->param[$prm];
		}
		return null;
	}

	public function __set($prm, $valor){
		$this->param[$prm] = $valor;
	}

	/**
	 * Obtiene los parametros del periodo actual no girado
	 *
	 */
	public function obtenerParametrosBD(){
		$sql = "SELECT MIN(periodo) AS periodo FROM aportes012 WHERE procesado='N'";
		$rPerAct = $this->con->enArray($sql);
		if($rPerAct != false && count($rPerAct) > 0){
			$peract = $rPerAct[0]['periodo'];
			$this->periodoActual = $peract;
		}else{
			trigger_error("No se pudo consultar el periodo actual para el giro", E_USER_ERROR);
			return false;
		}

		$sql = "SELECT TOP 1 MAX(periodo) AS periodo, fechalimite FROM aportes012 WHERE procesado='S' GROUP BY fechalimite ORDER BY periodo DESC";
		$rPerAnt = $this->con->enArray($sql);
		if($rPerAnt != false && count($rPerAnt) > 0){
			$perant = $rPerAnt[0]['periodo'];
			$this->fechaInicialPago = $rPerAnt[0]['fechalimite'];
		}else{
			trigger_error("No se pudo consultar el periodo anterior para el giro", E_USER_ERROR);
			return false;
		}

		$sql = "SELECT fechainicio, fechafin, fechalimite, cuotamonetaria, cuotacampo, smlv, procesado, controlproceso, estadogiro FROM aportes012 WHERE periodo = '$this->periodoActual'";
		$rDatAct = $this->con->enArray($sql);
		if($rDatAct != false && count($rDatAct) > 0){
			$this->fechaInicialPeriodo = $rDatAct[0]['fechainicio'];
			$this->fechaFinalPeriodo = $rDatAct[0]['fechafin'];
			$this->fechaFinalPago = $rDatAct[0]['fechalimite'];
			$this->valorSMLV = $rDatAct[0]['smlv'];
			$this->valorCuota = $rDatAct[0]['cuotamonetaria'];
			$this->valorCuotaCampo = $rDatAct[0]['cuotacampo'];
			$this->tokenControlGiro = $rDatAct[0]['controlproceso'];
			$this->estadoGiro = $rDatAct[0]['estadogiro'];
		}else{
			trigger_error("No se pudo consultar los datos del periodo a girar", E_USER_ERROR);
			return false;
		}
	}

	/**
	 * Obtiene los parametros del periodo que se solicita
	 *
	 * @param string $periodo numero del periodo 'aaaamm'
	 */
	public function obtenerParametrosBDXPeriodo($periodo){
		if(! array_key_exists($periodo, $this->param)){

			$perAnt = date('Ym',mktime(0, 0, 0, (intval(substr($periodo, 4, 2))-1), 1, substr($periodo, 0, 4)));

			$sql = "SELECT fechalimite FROM aportes012 WHERE periodo='$perAnt'";
			$rPerAnt = $this->con->enArray($sql);
			if($rPerAnt != false && count($rPerAnt) > 0){
				$tmp['fechaInicialPago'] = $rPerAnt[0]['fechalimite'];
			}else{
				trigger_error("No se pudo consultar el periodo anterior de $periodo para el giro", E_USER_ERROR);
				return false;
			}

			$sql = "SELECT fechainicio, fechafin, fechalimite, cuotamonetaria, cuotacampo, smlv, procesado, controlproceso FROM aportes012 WHERE periodo = '$periodo'";
			$rDatAct = $this->con->enArray($sql);
			if($rDatAct != false && count($rDatAct) > 0){
				$tmp['fechaInicialPeriodo'] = $rDatAct[0]['fechainicio'];
				$tmp['fechaFinalPeriodo'] = $rDatAct[0]['fechafin'];
				$tmp['fechaFinalPago'] = $rDatAct[0]['fechalimite'];
				$tmp['valorSMLV'] = intval($rDatAct[0]['smlv']);
				$tmp['tope4'] = $tmp['valorSMLV']*4;
				$tmp['tope6'] = $tmp['valorSMLV']*6;
				$tmp['valorCuota'] = $rDatAct[0]['cuotamonetaria'];
				$tmp['valorCuotaCampo'] = $rDatAct[0]['cuotacampo'];
				$tmp['tokenControlGiro'] = $rDatAct[0]['controlproceso'];
			}else{
				trigger_error("No se pudo consultar los datos del periodo $periodo a girar", E_USER_ERROR);
				return false;
			}
			$this->$periodo = $tmp;
			$this->periodoGirar = $periodo;
			return true;
		}else{
			return true;
		}
	}
}
/*
$parametros = new Parametros();
$parametros->tipoGiro = 'N';
$parametros->obtenerParametrosBD();
$periodo = '201001';
$parametros->obtenerParametrosBDXPeriodo($periodo);
echo $parametros->{$periodo}['fechaInicialPeriodo'];
echo $parametros->periodoActual;
*/
?>