<?php
/**
 * @author JOSE LUIS ROJAS
 * @version 1.0.0
 * @since 26 Octubre 2010
 */

include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'session.php';
session();


require_once ('clases/Consultor.php');
require_once ('Afiliado.php');
require_once ('Afiliacion.php');

class Conyuge extends Afiliado{

	function __construct($idpersona = 0){
		parent::__construct($idpersona);
	}

	/*public function obtenerNominas($periodo){
		$this->condicionesXPeriodo[$periodo]['nominas'] = new Nominas();
		$this->condicionesXPeriodo[$periodo]['nominas']->obtenerValoresNominas($this->idpersona, $periodo);
	}*/

	public function obtenerAfiliacion($fecIniper, $fecFinper, $periodo){
		$afil = new Afiliacion();
		$this->condicionesXPeriodo[$periodo]['afiliacion'] = $afil->obtenerAfiliacionSinEmpresa($this->idpersona, $periodo);
		if($this->condicionesXPeriodo[$periodo]['afiliacion'] != false){
			//$this->giro = $this->condicionesXPeriodo[$periodo]['afiliacion']['tipoformulario'];
			$this->giro = ($this->condicionesXPeriodo[$periodo]['afiliacion'] != false)?$this->condicionesXPeriodo[$periodo]['afiliacion']['tipoformulario']:false;
			$this->idempresa = $this->condicionesXPeriodo[$periodo]['afiliacion']['idempresa'];
		}
		$this->condicionesXPeriodo[$periodo]['afiliacion']['tope'] = true;
	}
}

?>