<?php
/**
 * @author JOSE LUIS ROJAS
 * @version 1.0.0
 * @since 26 octubre 2010
 *
 * Proceso de giro para sigas, se reevaluaron los procesos y se genero un nuevo proceso de giro basado en los requerimientos del negocio
 */

//chdir ( $raiz );


require_once ('Parametros.php');
require_once ('Aportes.php');
require_once ('Afiliado.php');
require_once ('Conyuge.php');
require_once ('Beneficiario.php');
require_once ('Utiles.php');
require_once ('Logs.php');

class Giro{

	/**
	 * @var Parametros Objeto que maneja todos los parametros del periodo y el giro en general
	 */
	private $prm;
	/**
	 *
	 * @var array Listado de los aportes de las empresas a procesar
	 */
	private $aportes;
	/**
	 *
	 * @var Afiliado Objeto que maneja todos los atributos del afiliado a procesar
	 */
	private $afiliado;
	/**
	 *
	 * @var Conyuge Objeto que maneja todos los atributos del conyuge afiliado o no a procesar
	 */
	private $conyuge;

	private $con;

	private $stackAfiliados = array();

	private $tabla = null;

	private $tablaAud = null;

	private $log;

	/*function __destruct(){
		$sql = "DROP TABLE tmp_serialized";
		$this->con->noRetorno($sql);
	}*/

	/**
	 * Inicializa los parametros para girar el periodo actual por procesar, tomando en base el ingreso base de cotizacion por defecto y tipo de giro
	 * normal
	 *
	 *
	 * @param $tipoGiro Tipo de giro a realizar A(dicional) o N(ormal)
	 * @param $tipoRemuneracion Tipo de remuneracion a utilizar ingbase o salbasico
	 */
	function __construct($tipoGiro = 'N', $tipoRemuneracion = 'salbasico'){
		set_time_limit(0);
		$this->prm = new Parametros();
		$this->prm->tipoGiro = $tipoGiro;
		$this->prm->tipoRemuneracion = $tipoRemuneracion;
		$this->prm->usuario = $_SESSION['USUARIO'];
		$this->prm->fechaGiro = date('Y-m-d');
		$this->prm->obtenerParametrosBD();



		$this->con = new Consultor();

		$this->log = new Logs();
		//$this->log->parametros(2, array('nombre'=>'errores.txt', 'ruta'=>'/var/www/html/logs_sigas/giro'));
		if(!is_dir('..\..\..\planos_generados\giro\serialized')){
			$d = mkdir('..\..\..\planos_generados\giro\serialized');
		}
		$parFile = array('nombre'=>'errores.txt', 'ruta'=>'..\..\..\planos_generados\giro');
		$parBD = array('periodoproceso'=>'TEXT', 'periodo'=>'TEXT', 'tipogiro'=>'TEXT', 'idempresa'=>'INT', 'idtrabajador'=>'INT', 'idconyuge'=>'INT', 'idbeneficiario'=>'INT', 'idcodigocausal'=>'INT', 'usuario'=>'TEXT', 'fechasistema'=>'TEXT');
		$this->log->parametros(4, $parFile, 'aportes065', $parBD);

		$this->tabla = 'aportes014';
		$this->tablaAud = 'aportes054';

		if($this->prm->estadoGiro == 'C'){
			//Giro Cerrado no girar
			//exit('ESTADO DEL GIRO '.$this->prm->periodoActual.' CERRADO');
			$ln = "El periodo a procesar se encuentra cerrado";
			$datBD = array();
			$this->log->grabar($ln, $datBD);
			exit();
		}

		/*$sql = "SELECT COUNT(*) AS cant FROM sys.Tables WHERE name='tmp_serialized'";
		$ctmp = $this->con->enArray($sql);

		if(is_array($ctmp) && intval($ctmp[0]['cant']) > 0){
			$sql = "DROP TABLE tmp_serialized";
			$this->con->noRetorno($sql);
		}

		$sql = "CREATE TABLE [dbo].[tmp_serialized]([idpersona] [int] NOT NULL,[objeto] [varchar](4000) NOT NULL,CONSTRAINT [PK_tmp_serialized] PRIMARY KEY CLUSTERED ([idpersona] ASC)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]) ON [PRIMARY]";
		$this->con->noRetorno($sql);*/
	}

	public function pasarHistorico(){
		$sql = "INSERT INTO aportes009 SELECT * FROM $this->tabla";
		if($this->con->noRetorno($sql) === true){
			$sql = "UPDATE aportes012 SET procesado='S', estadogiro='C' WHERE periodo='$this->prm->periodoActual'";
			$this->con->noRetorno($sql);

			$sql = "TRUNCATE aportes014";
			$this->con->noRetorno($sql);
		}
	}

	public function anularGiroActual(){
		//Borra pignoracion primero
		$sql = "SELECT idcuota, idbeneficiario, periodo, periodoproceso, idpignoracion FROM $this->tabla WHERE tipogiro='N'";
		$datgir = $this->con->enArray($sql);
		foreach($datgir as $gir){
			if($gir['idpignoracion'] != 0){
				$sql = "DELETE FROM aportes044 WHERE idpignoracion=$gir[idpignoracion] AND periodo=$gir[periodo] AND periodoproceso=$gir[periodoproceso] AND idbeneficiario=$gir[idbeneficiario]";
				$this->con->noRetorno($sql);
			}
			$sql = "DELETE FROM $this->tabla WHERE idcuota=$gir[idcuota]";
			$this->con->noRetorno($sql);
		}
		//Actualiza las nominas marcadas que fueron procesadas en el giro
		$sql = "UPDATE aportes010 SET control=NULL, procesado='N' WHERE control='" . trim($this->prm->tokenControlGiro) . "' AND procesado = 'S'";
		$this->con->noRetorno($sql);

		//Borra los giros que se encuentran en el peridoo actual
	//$sql = "TRUNCATE aportes014";
	//$this->con->noRetorno($sql);
	}

	public function general(){

		$ln = "inicio giro------------------------------------------------------------------------------------------------------" . date('Y-m-d h:i:s');
		$this->log->grabar($ln);

		$this->aportes = Aportes::obtenerAportesEmpresasGiroGral($this->prm->fechaInicialPago, $this->prm->fechaFinalPago, $this->prm->fechaFinalPeriodo, $this->prm->periodoActual);
		//Si se encuentran aportes de las empresas se continua el procesamiento
		if($this->aportes != false && count($this->aportes) > 0){
			foreach($this->aportes as $emp){
				$mayorAportes = Utiles::fechaMayor($emp['fechaaportes'], $this->prm->fechaFinalPeriodo);
				if($mayorAportes != 1 && $mayorAportes != false){

					$aportantes = Aportes::obtenerTrabajadoresXEmpresaGiroGral($emp['idempresa'], $emp['periodo'], $this->prm->fechaInicialPago, $this->prm->fechaFinalPago);
					if($aportantes != false){
						$this->prm->obtenerParametrosBDXPeriodo($emp['periodo']);
						$this->prm->periodoGirar = $emp['periodo'];
						foreach($aportantes as $afil){
							//Valida cada uno de los afiliados a esa empresa reportados por nomina
							$cumpleCondiciones = $this->preGiro($emp['idempresa'], $emp['claseaportante'], $afil['idtrabajador']);

							if(isset($this->afiliado)){
								//Utiles::serializa($this->afiliado, $this->stackAfiliados[$afil['idtrabajador']]);
								Utiles::serializa($this->afiliado, $afil['idtrabajador']);
							}
							/*if (isset ( $this->conyuge )) {
								Utiles::serializa ( $this->conyuge, $this->stackAfiliados [$this->afiliado->relaciones->idconyuge] );
							}*/
							$this->marcarNominaProcesada($this->afiliado->idpersona, $emp['idempresa'], $this->prm->periodoGirar);
							unset($this->afiliado);
							unset($this->conyuge);

						}
					}else{
						//Empresa sin nominas de trabajadores
						$ln = "empresa con id " . $emp['idempresa'] . " y No.Planilla " . $emp['planilla'] . " sin nominas de trabajadores para el periodo " . $emp['periodo'];
						$datBD = array($this->prm->periodoActual, $this->prm->periodoGirar, $this->prm->tipoGiro, $emp['idempresa'], 0, 0, 0, 2892, $this->prm->usuario, $this->prm->fechaGiro);
						$this->log->grabar($ln, $datBD);
						continue;
					}

				}else{
					//Fecha de aportes superior al final del periodo
					$ln = "Fecha de aportes de la empresa con id " . $emp['idempresa'] . " y No.Planilla " . $emp['planilla'] . " con fecha de aportes superior al final del periodo  " . $emp['periodo'];
					$datBD = array($this->prm->periodoActual, $this->prm->periodoGirar, $this->prm->tipoGiro, $emp['idempresa'], 0, 0, 0, 2891, $this->prm->usuario, $this->prm->fechaGiro);
					$this->log->grabar($ln, $datBD);
					continue;
				}
			}
		}else{
			//No se encontraron aportes de empresas para procesar
			$ln = 'No se encontraron aportes de empresas para procesar en el periodo ' . $emp['periodo'];
			$datBD = array($this->prm->periodoActual, $this->prm->periodoGirar, $this->prm->tipoGiro, $this->afiliado->idempresa, $this->afiliado->idpersona, $this->afiliado->relaciones->idconyuge, 0, 9999, 'YOP', '05/03/2011');
			$this->log->grabar($ln, $datBD);
		}

		$this->_girarAfiliadosMuertos();

		$ln = "fin giro------------------------------------------------------------------------------------------------------" . date('Y-m-d h:i:s');
		$this->log->grabar($ln);
	}

	public function giroReclamoXTrabajador($idreclamo){
		$sql = "SELECT idtrabajador, periodoinicial, periodofinal FROM aportes055 WHERE idreclamo=$idreclamo AND estado='A'";
		$datRec = $this->con->enArray($sql);
		if(is_array($datRec) && count($datRec[0]) > 0){
			$perini = str_split($datRec[0]['periodoinicial'], 4);
			$periniU = mktime(0, 0, 0, $perini[1], 1, $perini[0]);
			$perfin = str_split($datRec[0]['periodofinal'], 4);
			$perfinU = mktime(0, 0, 0, $perfin[1], 1, $perfin[0]);
			$idpersona = $datRec[0]['idtrabajador'];
			$periodo = $datRec[0]['periodoinicial'];

			$gir = 0;
			$per = 0;

			while($periniU <= $perfinU){
				$bnd = $this->giroIndividualXPeriodo($idpersona, $periodo);
				if(isset($this->afiliado)){
					Utiles::serializa($this->afiliado, $idpersona);
				}
				if($bnd == true){
					$gir++;
				}
				$per++;
				$perini[1] += 1;
				$periniU = mktime(0, 0, 0, $perini[1], 1, $perini[0]);
				$periodo = date('Ym', $periniU);
			}
		}
	}

	public function giroReclamoXEmpresa($idreclamo){
		$sql = "SELECT idempresa, periodoinicial, periodofinal FROM aportes060 WHERE idreclamo=$idreclamo AND estado='A'";
		$datRec = $this->con->enArray($sql);
		if(is_array($datRec) && count($datRec[0]) > 0){
			$perini = str_split($datRec[0]['periodoinicial'], 4);
			$periniU = mktime(0, 0, 0, $perini[1], 1, $perini[0]);
			$perfin = str_split($datRec[0]['periodofinal'], 4);
			$perfinU = mktime(0, 0, 0, $perfin[1], 1, $perfin[0]);
			$idempresa = $datRec[0]['idempresa'];
			$periodo = $datRec[0]['periodoinicial'];

			$gir = 0;
			$per = 0;

			while($periniU <= $perfinU){
				$bnd = $this->giroEmpresaXPeriodo($idempresa, $periodo);
				if($bnd == true){
					$gir++;
				}
				$per++;
				$perini[1] += 1;
				$periniU = mktime(0, 0, 0, $perini[1], 1, $perini[0]);
				$periodo = date('Ym', $periniU);
			}
		}
	}

	public function giroEmpresaXPeriodo($idempresa, $periodo){
		$this->prm->obtenerParametrosBDXPeriodo($periodo);
		$this->prm->periodoGirar = $periodo;

		$ap = Aportes::obtenerAportesEmpresasGiroXNit($this->prm->fechaInicialPago, $this->prm->fechaFinalPago, $this->prm->fechaFinalPeriodo, $periodo, $idempresa);

		if($ap != false && count($ap) > 0){
			$aportantes = Aportes::obtenerTrabajadoresXEmpresaGiroGral($idempresa, $periodo);
			if($aportantes != false){
				foreach($aportantes as $afil){
					//Valida cada uno de los afiliados a esa empresa reportados por nomina
					if($this->preGiro($idempresa, 0, $afil['idtrabajador'])){
						$this->marcarNominaProcesada($this->afiliado->idpersona, $idempresa, $this->prm->periodoGirar);
							return true;
					}elseif($this->afiliado->estado == 'M'){
						$this->_girarAfiliadosMuertos($this->afiliado->idpersona);
					}else {
						return false;
					}

					if(isset($this->afiliado)){
						Utiles::serializa($this->afiliado, $afil['idtrabajador']);
					}

					unset($this->afiliado);
					unset($this->conyuge);
				}
			}
		}
	}

	public function giroIndividualXPeriodo($idpersona, $periodo){
		$this->prm->obtenerParametrosBDXPeriodo($periodo);
		$this->prm->periodoGirar = $periodo;

		/*$this->aportes = Aportes::obtenerAportesEmpresasGiroXAfiliado ( $this->prm->{$this->prm->periodoGirar} ['fechaInicialPago'], $this->prm->{$this->prm->periodoGirar} ['fechaFinalPago'], $this->prm->{$this->prm->periodoGirar} ['fechaFinalPeriodo'], $this->prm->periodoGirar, $idpersona );
		if ($this->aportes != false) {*/

		if($this->preGiro(0, 0, $idpersona)){
			$this->marcarNominaProcesada($this->afiliado->idpersona, $this->prm->periodoGirar);
			return true;
		}elseif($this->afiliado->estado == 'M'){
			$this->_girarAfiliadosMuertos($idpersona);
		}else{
			return false;
		}
		/*} else {
			return false;
		}*/
	}

	//
	private function preGiro($idempresa, $claseaportante, $idpersona){
		$eAfil = false;
		$eCony = false;

		if(array_key_exists($idpersona, $this->stackAfiliados)){
			//$this->afiliado = Utiles::deserializa($this->stackAfiliados[$idpersona]);
			$this->afiliado = Utiles::deserializa($idpersona);

			if(get_class($this->afiliado) == 'Afiliado'){
				$eAfil = true;

		//$this->afiliado = $this->stackAfiliados[$idpersona];
			}
		}

		if($eAfil == false){
			$this->afiliado = new Afiliado($idpersona, $idempresa);
			$this->afiliado->obtenerRelaciones();
			$this->stackAfiliados[$idpersona] = '/tmp/' . $idpersona . '_' . microtime();
		}
		//$this->afiliado->claseAportante = $claseaportante;


		if(! array_key_exists($this->prm->periodoGirar, $this->afiliado->condicionesXPeriodo)){
			$this->afiliado->obtenerPignoracion();
			$this->afiliado->obtenerEmbargo();
			$this->afiliado->condicionesGiroIndividual($this->prm);

			//------------------
			if($this->afiliado->idempresa == 0 && $this->prm->tipoGiro == 'A'){
				if(isset($this->afiliado->condicionesXPeriodo[$this->prm->periodoGirar]['afiliacion']['idempresa']) && $this->afiliado->condicionesXPeriodo[$this->prm->periodoGirar]['afiliacion']['idempresa'] != 0){
					$this->afiliado->idempresa = $this->afiliado->condicionesXPeriodo[$this->prm->periodoGirar]['afiliacion']['idempresa'];
					if(! $this->afiliado->obtenerAportesIndividual($this->aportes)){
						//@todo No tiene aportes con la empresa
						return false;
					}
				}else{
					$ln = "No existe afiliacion con ninguna empresa en el periodo " . $this->prm->periodoGirar . " para el trabajador " . $this->afiliado->numeroDocumento;
					$this->log->grabar($ln);
					return false;
				}
			}elseif($this->prm->tipoGiro == 'A'){
				//$this->aportes = Aportes::obtenerAportesEmpresasGiroXAfiliado($this->prm->{$this->prm->periodoGirar}['fechaInicialPago'], $this->prm->{$this->prm->periodoGirar}['fechaFinalPago'], $this->prm->{$this->prm->periodoGirar}['fechaFinalPeriodo'], $this->prm->periodoGirar, $idpersona);
				$this->aportes = Aportes::obtenerAportesEmpresasGiroXAfiliado($this->prm->fechaInicialPago, $this->prm->fechaFinalPago, $this->prm->fechaFinalPeriodo, $this->prm->periodoGirar, $idpersona);
				if(! $this->afiliado->obtenerAportesIndividual($this->aportes)){
					return false;
				}
			}
		}

		if($this->afiliado->relaciones->cantConyuges < 1){
			//Afiliado Soltero, si las condiciones para giro ya fueron evaluadas se prosigue a girar

			if($this->afiliado->condicionesXPeriodo[$this->prm->periodoGirar]['afiliacion']['tope'] == true){
				//hace tope salarial de 4, trabajador soltero
				$ln = 'Hace tope salarial individual de 4, trabajador soltero ' . $this->afiliado->numeroDocumento;
				$datBD = array($this->prm->periodoActual, $this->prm->periodoGirar, $this->prm->tipoGiro, $this->afiliado->idempresa, $this->afiliado->idpersona, $this->afiliado->relaciones->idconyuge, 0, 2903, $this->prm->usuario, $this->prm->fechaGiro);
				$this->log->grabar($ln, $datBD);
				return false;
			}elseif($this->afiliado->bndGiro == false){
				//No cumple con las condiciones para el giro, las causales son grabadas al momento de la validacion
				return false;
			}else{
				//Cumple condiciones para el giro, se procesan los beneficiarios
				return $this->beneficiariosIndividual();
			}

		}elseif($this->afiliado->relaciones->cantConyuges >= 1){

			foreach($this->afiliado->relaciones->conyuges as $idconyuge){
				$eCony = false;
				//Afiliado con conyuge permanente
				if(array_key_exists($idconyuge, $this->stackAfiliados)){
					//$this->conyuge = Utiles::deserializa($this->stackAfiliados[$idconyuge]);
					$this->conyuge = Utiles::deserializa($idconyuge);

					if(get_class($this->conyuge) == 'Conyuge' || get_class($this->conyuge) == 'Afiliado'){
						$eCony = true;
					}
				}

				if($eCony == false){
					$this->conyuge = new Conyuge($idconyuge);
					$this->conyuge->obtenerRelaciones();
					$this->stackAfiliados[$idconyuge] = '/tmp/' . $idconyuge . '_' . microtime();
				}

				if(! array_key_exists($this->prm->periodoGirar, $this->conyuge->condicionesXPeriodo)){
					$this->conyuge->obtenerPignoracion();
					$this->conyuge->obtenerEmbargo();
					$this->conyuge->condicionesGiroIndividual($this->prm);
				}

				//RELACION MUTUA TOPES POR PAREJA SIN IMPORTAR CONVIVENCIA
				$salAfiliado = (isset($this->afiliado->condicionesXPeriodo[$this->prm->periodoGirar]['cantSMLV']))?$this->afiliado->condicionesXPeriodo[$this->prm->periodoGirar]['cantSMLV']:0;
				$salConyuge = (isset($this->conyuge->condicionesXPeriodo[$this->prm->periodoGirar]['cantSMLV']))?$this->conyuge->condicionesXPeriodo[$this->prm->periodoGirar]['cantSMLV']:0;
				$sumSalarios = $salAfiliado + $salConyuge;

				if($sumSalarios > 6){
					//Hacen tope de 6 - No se gira a ninguno de los dos
					$ln = "tope de 6 SMLV del Trabajor " . $this->afiliado->numeroDocumento . " con conyuge " . $this->conyuge->numeroDocumento . " en el periodo " . $this->prm->periodoGirar;
					$datBD = array($this->prm->periodoActual, $this->prm->periodoGirar, $this->prm->tipoGiro, $this->afiliado->idempresa, $this->afiliado->idpersona, $this->conyuge->idpersona, 0, 2904, $this->prm->usuario, $this->prm->fechaGiro);
					$this->log->grabar($ln, $datBD);
					return false;
				}elseif($sumSalarios > 4 && $sumSalarios <= 6){
					//tope entre 4 y 6 salarios, se gira solo a uno la mujer preferiblemente sino hace tope de 4 slmv
					if($this->afiliado->sexo == 'F'){
						if($this->afiliado->condicionesXPeriodo[$this->prm->periodoGirar]['afiliacion']['tope'] == false && $this->afiliado->bndGiro == true){
							//echo "tope 4 - 6 giro a mujer afil " . $this->afiliado->idpersona . ' - ' . $this->afiliado->idempresa . ' - ' . $this->prm->periodoGirar . '<br/>';
							$this->beneficiariosGrupo($this->afiliado, $this->conyuge);
						}
					}elseif($this->conyuge->sexo == 'F'){
						if($this->conyuge->condicionesXPeriodo[$this->prm->periodoGirar]['afiliacion']['tope'] == false && $this->conyuge->bndGiro == true && $this->prm->tipoGiro == 'N'){
							//echo "tope 4 - 6 giro a mujer cony " . $this->conyuge->idpersona . ' - ' . $this->conyuge->idempresa . ' - ' . $this->prm->periodoGirar . '<br/>';
							$this->beneficiariosGrupo($this->conyuge, $this->afiliado);
						}
					}else{
						//@todo error en el sexo ????
					}
				}elseif($sumSalarios > 0 && $sumSalarios <= 4){
					//tope menor a 4
					if($this->afiliado->condicionesXPeriodo[$this->prm->periodoGirar]['afiliacion']['tope'] == false && $this->afiliado->bndGiro == true){
						//echo "tope < 4 giro a pareja afil " . $this->afiliado->idpersona . ' - ' . $this->afiliado->idempresa . ' - ' . $this->prm->periodoGirar . '<br/>';
						$this->beneficiariosGrupo($this->afiliado, $this->conyuge);
					}
					if($this->conyuge->condicionesXPeriodo[$this->prm->periodoGirar]['afiliacion']['tope'] == false && $this->conyuge->bndGiro == true && $this->prm->tipoGiro == 'N'){
						//echo "tope < 4 giro a pareja cony " . $this->conyuge->idpersona . ' - ' . $this->conyuge->idempresa . ' - ' . $this->prm->periodoGirar . '<br/>';
						$this->beneficiariosGrupo($this->conyuge, $this->afiliado);
					}
				}
				if(isset($this->conyuge)){
					//Utiles::serializa($this->conyuge, $this->stackAfiliados[$idconyuge]);
					Utiles::serializa($this->conyuge, $idconyuge);
				}
				unset($this->conyuge);
			}
		}
		/**
		 * Se comenta el codigo a continuacion por que se tiene que validar los topes sin importar la convivencia
		 */
		/*else{
			//@TODO error tiene mas de una conyuge con convivencia
			$ln = "El Trabajor " . $this->afiliado->numeroDocumento . " tiene mas 1 conyuge con convivencia";
			$datBD = array($this->prm->periodoActual, $this->prm->periodoGirar, $this->prm->tipoGiro, $this->afiliado->idempresa, $this->afiliado->idpersona, $this->afiliado->relaciones->idconyuge, 0, 9999, 'YOP', '05/03/2011');
			$this->log->grabar($ln, $datBD);
			return false;
		}*/
	}

	private function beneficiariosIndividual(){
		foreach($this->afiliado->relaciones->grupoFamiliar as $grp){
			if($grp['idparentesco'] != 34 && $grp['idconyuge'] != $this->afiliado->relaciones->idconyuge){
				$benf = new Beneficiario($grp['idbeneficiario']);
				$benf->obtenerDatosBeneficiario($this->afiliado->idpersona);
				$benf->edad($this->prm->{$this->prm->periodoGirar}['fechaFinalPeriodo']);

				if(strlen($grp['iddefuncion']) > 0){
					$benf->defuncion = $grp['iddefuncion'];
					$benf->periodoDefuncion = $grp['perdef'];
				}else{
					$benf->defuncion = null;
					$benf->periodoDefuncion = null;
				}

				if($benf->estado == 'I'){
					//@todo Error Beneficiario con bandera de estado Inactivo
					$ln = "El beneficiario " . $benf->numeroDocumento . " del Trabajor " . $this->afiliado->numeroDocumento . " tiene estado Inactivo";
					$datBD = array($this->prm->periodoActual, $this->prm->periodoGirar, $this->prm->tipoGiro, $this->afiliado->idempresa, $this->afiliado->idpersona, $this->afiliado->relaciones->idconyuge, $benf->idpersona, 2905, $this->prm->usuario, $this->prm->fechaGiro);
					$this->log->grabar($ln, $datBD);
					unset($benf);
					continue;
				}elseif($benf->giro == 'N'){
					//@todo Beneficiario con bandera de giro en N
					$ln = "El beneficiario " . $benf->numeroDocumento . " del Trabajor " . $this->afiliado->numeroDocumento . " con bandera de giro (NO)";
					$datBD = array($this->prm->periodoActual, $this->prm->periodoGirar, $this->prm->tipoGiro, $this->afiliado->idempresa, $this->afiliado->idpersona, $this->afiliado->relaciones->idconyuge, $benf->idpersona, 2906, $this->prm->usuario, $this->prm->fechaGiro);
					$this->log->grabar($ln, $datBD);
					unset($benf);
					continue;
				}else{
					//@todo verificacion de condiciones certificados, edad
					$cert = $benf->verificarCertificado($this->prm->periodoGirar);
					if($cert === true){
						$this->girar($benf, $this->afiliado);
					}else{
						//@todo Error no girar por certificado
						$ln = "El beneficiario " . $benf->numeroDocumento . " del Trabajor " . $this->afiliado->numeroDocumento . " no tiene certificado y/o certificado inactivo";
						$datBD = array($this->prm->periodoActual, $this->prm->periodoGirar, $this->prm->tipoGiro, $this->afiliado->idempresa, $this->afiliado->idpersona, $this->afiliado->relaciones->idconyuge, $benf->idpersona, $cert, $this->prm->usuario, $this->prm->fechaGiro);
						$this->log->grabar($ln, $datBD);
					}
				}
				unset($benf);
			}
		}
	}

	/**
	 *
	 * @param int $bndGirCony Bandera de quien es el titular del giro, 0 para afiliado, 1 para conyuge
	 */
	private function beneficiariosGrupo(Afiliado &$afilGir, Afiliado &$conygir = null){
		foreach($afilGir->relaciones->grupoFamiliar as $ind=>$grp){
			if($grp['idparentesco'] != '34' && ($grp['idconyuge'] == $conygir->idpersona || $grp['idconyuge'] == '0') && $grp['procesado'] == 'N'){

				$benf = new Beneficiario($grp['idbeneficiario']);
				$benf->obtenerDatosBeneficiario($this->afiliado->idpersona);
				$benf->edad($this->prm->{$this->prm->periodoGirar}['fechaFinalPeriodo']);
				if(!$benf->valFechaAfiliacion($this->prm->{$this->prm->periodoGirar}['fechaFinalPeriodo'])){
					$ln = "El beneficiario " . $benf->numeroDocumento . " del Trabajor " . $afilGir->numeroDocumento . " tiene fecha afiliacion mayor al final del periodo";
					$datBD = array($this->prm->periodoActual, $this->prm->periodoGirar, $this->prm->tipoGiro, $afilGir->idempresa, $afilGir->idpersona, $conygir->idpersona, $benf->idpersona, 2936, $this->prm->usuario, $this->prm->fechaGiro);
					$this->log->grabar($ln, $datBD);
					unset($benf);
					continue;
				}

				if(strlen($grp['iddefuncion']) > 0){
					$benf->defuncion = $grp['iddefuncion'];
					$benf->periodoDefuncion = $grp['perdef'];
				}else{
					$benf->defuncion = null;
					$benf->periodoDefuncion = null;
				}

				if($benf->estado == 'I'){
					//@todo Beneficiario con bandera de estado Inactivo
					$ln = "El beneficiario " . $benf->numeroDocumento . " del Trabajor " . $afilGir->numeroDocumento . " tiene estado Inactivo";
					$datBD = array($this->prm->periodoActual, $this->prm->periodoGirar, $this->prm->tipoGiro, $afilGir->idempresa, $afilGir->idpersona, $conygir->idpersona, $benf->idpersona, 2905, $this->prm->usuario, $this->prm->fechaGiro);
					$this->log->grabar($ln, $datBD);
					unset($benf);
					continue;
				}elseif($benf->giro == 'N'){
					//@todo Beneficiario con bandera de giro en N
					$ln = "El beneficiario " . $benf->numeroDocumento . " del Trabajor " . $afilGir->numeroDocumento . " con bandera de giro (NO)";
					$datBD = array($this->prm->periodoActual, $this->prm->periodoGirar, $this->prm->tipoGiro, $afilGir->idempresa, $afilGir->idpersona, $conygir->idpersona, $benf->idpersona, 2906, $this->prm->usuario, $this->prm->fechaGiro);
					$this->log->grabar($ln, $datBD);
					unset($benf);
					continue;
				}else{
					//@todo verificacion de condiciones certificados, edad
					$cert = $benf->verificarCertificado($this->prm->periodoGirar);
					if($cert === true){
						//girar
						//echo "girar a " . $this->afiliado->numeroDocumento . " beneficiario " . $benf->numeroDocumento . '<br/>';
						$bnd = $this->girar($benf, $afilGir, $conygir);
					}else{
						//@todo no girar por certificado
						$ln = "El beneficiario " . $benf->numeroDocumento . " del Trabajor " . $afilGir->numeroDocumento . " no tiene certificado y/o certificado inactivo";
						$datBD = array($this->prm->periodoActual, $this->prm->periodoGirar, $this->prm->tipoGiro, $afilGir->idempresa, $afilGir->idpersona, $conygir->idpersona, $benf->idpersona, $cert, $this->prm->usuario, $this->prm->fechaGiro);
						$this->log->grabar($ln, $datBD);
					}
				}
				$afilGir->relaciones->grupoFamiliar[$ind]['procesado'] = 'S';
				unset($benf);
			}
		}
	}

	/**
	 * Realiza la verificacion de la pignoracion y embargo, y gira
	 */
	private function girar($benf, Afiliado &$afilGir, Afiliado &$conygir = null){

		$concepto = 'GIRO NORMAL';
		$remun = ($this->prm->tipoRemuneracion == 'ingbase')?'totalIngBase':'totalSalBasico';
		$periodo = $this->prm->periodoGirar;
		$idempresa = $afilGir->idempresa;
		$idbeneficiario = $benf->idpersona;
		$idtipobeneficiario = $benf->parentesco;
		$discapacitado = $benf->discapacitado;
		$edad = $benf->edad;

		$idtrabajador = $afilGir->idpersona;
		$cedtra = $afilGir->numeroDocumento;
		$esttra = $afilGir->condicionesXPeriodo[$this->prm->periodoGirar]['afiliacion']['estadoafiliacion'];
		$sextra = $afilGir->sexo;
		$ingbase = $afilGir->condicionesXPeriodo[$this->prm->periodoGirar]['nominas']->{$remun};
		$horas = $afilGir->condicionesXPeriodo[$this->prm->periodoGirar]['nominas']->totalHoras;
		$smlv = $afilGir->condicionesXPeriodo[$this->prm->periodoGirar]['cantSMLV'];

		$idconyuge = isset($conygir->idpersona)?$conygir->idpersona:0;
		$cedcon = isset($conygir->numeroDocumento)?$conygir->numeroDocumento:0;
		$estcony = isset($conygir->condicionesXPeriodo[$this->prm->periodoGirar]['afiliacion']['estadoafiliacion'])?$conygir->condicionesXPeriodo[$this->prm->periodoGirar]['afiliacion']['estadoafiliacion']:'';
		$ingbasecony = isset($conygir->condicionesXPeriodo[$this->prm->periodoGirar]['nominas']->{$remun})?$conygir->condicionesXPeriodo[$this->prm->periodoGirar]['nominas']->{$remun}:0;
		$horascony = isset($conygir->condicionesXPeriodo[$this->prm->periodoGirar]['nominas']->totalHoras)?$conygir->condicionesXPeriodo[$this->prm->periodoGirar]['nominas']->totalHoras:0;
		$smlvcony = isset($conygir->condicionesXPeriodo[$this->prm->periodoGirar]['cantSMLV'])?$conygir->condicionesXPeriodo[$this->prm->periodoGirar]['cantSMLV']:0;

		$ingbasetotal = $ingbase + $ingbasecony;
		$smlvtotal = $smlv + $smlvcony;

		$claseaportante = $afilGir->condicionesXPeriodo[$this->prm->periodoGirar]['afiliacion']['claseaportante'];

		$por = 1;
		$causal = 'NULL';

		if($claseaportante == '2875'){
			$fechamatricula = $afilGir->condicionesXPeriodo[$this->prm->periodoGirar]['afiliacion']['fechamatricula'];
			$tmpBeneficio = Utiles::diasEntre($fechamatricula, '12/29/2010');
			$concepto = 'LEY 1429';
			if($tmpBeneficio >= 0){
				$tmpMatricula = Utiles::diasEntre(date('Y-m-d'), $fechamatricula);
				if($tmpMatricula > 0 && $tmpMatricula <= 730){
					$por = 0;
					$causal = 2875;
				}elseif($tmpMatricula > 730 && $tmpMatricula <= 1095){
					$por = 0.25;
					$causal = 2875;
				}elseif($tmpMatricula > 1095 && $tmpMatricula <= 1460){
					$por = 0.5;
					$causal = 2875;
				}elseif($tmpMatricula > 1460 && $tmpMatricula <= 1825){
					$por = 0.75;
					$causal = 2875;
				}else{
					$por = 1;
				}
			}else{
				//@todo ERROR Empresa no se puede acoger al beneficio 1429 por que la fecha de matricula es mayor a la fecha de promulgacion de la ley
				return false;
			}
		}

		$datEmbargo = $afilGir->embargo->buscarEmbargoBeneficiario($idbeneficiario);
		if($datEmbargo != false){
			$embargo = 'S';
			$tipopago = $datEmbargo['tipopago'];
			$codigopago = $datEmbargo['codigopago'];
			$concepto = 'EMBARGO';
		}else{
			$embargo = 'N';
			$tipopago = $afilGir->condicionesXPeriodo[$this->prm->periodoGirar]['afiliacion']['tipopago'];
			$codigopago = $afilGir->condicionesXPeriodo[$this->prm->periodoGirar]['afiliacion']['tipopago'];
		}

		//Definicion de cantidad de cuotas si es beneficiario muerto
		$numerocuotas = 1;
		if($benf->defuncion != null){
			$ctpagadas = $this->verifCuotasPagadas($benf->periodoDefuncion, $idbeneficiario, $idtrabajador);
			$numerocuotas = 12 - $ctpagadas;
			$programa = 'M';
			$concepto = 'BENEFICIARIO MUERTO';
		}else{
			$programa = 'S';
		}
		//Definicion de valor a pagar de acuerdo al numero de cuotas y si es del sector agrario
		$val = $afilGir->condicionesXPeriodo[$this->prm->periodoGirar]['afiliacion']['agricola'] == 'N'?$this->prm->{$this->prm->periodoGirar}['valorCuota']:$this->prm->{$this->prm->periodoGirar}['valorCuotaCampo'];
		$valor = $val * $numerocuotas;

		//Valor a pagar si el beneficiario es discapacitado
		if($discapacitado == 'S'){
			$concepto = 'DISCAPACITADO';
			$numerocuotas *= 2;
			$valor *= 2;
		}
		$periodoproceso = $this->prm->periodoActual;

		$usuario = $this->prm->usuario;
		$tipogiro = $this->prm->tipoGiro;
		$fechagiro = date('Y-m-d');

		if($this->afiliado->pignoracion->idpignoracion != 0 && $datEmbargo == false){
			$this->afiliado->pignoracion->cuotaMes($valor);
			$idpignoracion = $this->afiliado->pignoracion->idpignoracion;
			$descuento = $this->afiliado->pignoracion->valorCobrar;
		}else{
			$idpignoracion = 0;
			$descuento = 0;
		}
		$idcertificado = 0;

		$valor *= $por;
		$valor = intval($valor);

		$girdoble = $this->verifGiroDoble($periodo, $idbeneficiario, $idtrabajador);
		//$girdoble = false;
		if($girdoble == true){
			//@todo Giro ya realizado, no inserta!!
			$ln = "Giro del periodo " . $periodo . " del Trabajor " . $afilGir->numeroDocumento . " y beneficiario con id " . $idbeneficiario . " ya realizado";
			$this->log->grabar($ln);
		}else{
			$this->con->noRetorno('BEGIN TRANSACTION');
			$sql = "INSERT INTO " . $this->tabla . "(periodo, idempresa, idbeneficiario, idtipobeneficiario, capacidadtrabajo, idtrabajador, idconyuge, tipopago, codigopago, numerocuotas, valor, periodoproceso, programa, embargo, usuario, tipogiro, descuento, fechagiro, idpignoracion, idcertificado, idcausal) ";
			$sql .= "VALUES('$periodo', '$idempresa','$idbeneficiario','$idtipobeneficiario','$discapacitado','$idtrabajador','$idconyuge','$tipopago','$codigopago','$numerocuotas', '$valor','$periodoproceso', '$programa', '$embargo', '$usuario', '$tipogiro', '$descuento', '$fechagiro', '$idpignoracion', '$idcertificado', $causal)";
			$estGir = $this->con->noRetorno($sql);

			$sql = "INSERT INTO " . $this->tablaAud . "(idtrabajador, cedtra, estado, sexo, ingbase, horas, smlvtra, idconyuge, cedcon, estconyuge, ingbasecony, horascony, smlvcony, comper, ingbasetotal, smlvtotal, idbeneficiario, parentesco, edadbenf, captra, codcert, periodo, valor, tipgir, fechagiro, concepto )";
			$sql .= " VALUES($idtrabajador, '$cedtra', '$esttra', '$sextra', $ingbase, $horas, $smlv, $idconyuge, '$cedcon', '$estcony', $ingbasecony, $horascony, $smlvcony, '', $ingbasetotal, $smlvtotal, $idbeneficiario, $idtipobeneficiario, $edad, '$discapacitado', '', $periodo, $valor, '$tipogiro', '$fechagiro', '$concepto')";
			$estGir = $this->con->noRetorno($sql);

			//$this->auditar($afilGir, $this->conyuge, $benf);


			if(! $estGir){
				$this->con->noRetorno('ROLLBACK TRANSACTION');
				return false;
			}else{
				//pignoro y no esta embargado
				if($this->afiliado->pignoracion->idpignoracion != 0 && $datEmbargo == false){
					$estPig = $this->afiliado->pignoracion->confirmarCobro($idbeneficiario, $periodo, $periodoproceso, $usuario);
					if($estPig){
						//defuncion
						if($benf->defuncion != null){
							$estdef = $this->marcarDefuncionProcesada($numerocuotas, $numerocuotas, $usuario, $periodoproceso, $benf->defuncion, $idbeneficiario);
							if($estdef){
								$this->con->noRetorno('COMMIT TRANSACTION');
								return true;
							}else{
								$this->con->noRetorno('ROLLBACK TRANSACTION');
								return false;
							}
						}else{
							$this->con->noRetorno('COMMIT TRANSACTION');
							return true;
						}
					}else{
						$this->con->noRetorno('ROLLBACK TRANSACTION');
						return false;
					}
				}else{
					//giro resto afiliados
					if($benf->defuncion != null){
						$estdef = $this->marcarDefuncionProcesada($numerocuotas, $numerocuotas, $usuario, $periodoproceso, $benf->defuncion, $idbeneficiario);
						if($estdef){
							$this->con->noRetorno('COMMIT TRANSACTION');
							return true;
						}else{
							$this->con->noRetorno('ROLLBACK TRANSACTION');
							return false;
						}
					}else{
						$this->con->noRetorno('COMMIT TRANSACTION');
						return true;
					}

		//$this->con->noRetorno('COMMIT WORK');
				}
			}
		}
	}

	public function girarAfiliadosMuertos(){
		$this->prm->periodoGirar = $this->prm->periodoActual;
		$this->prm->obtenerParametrosBDXPeriodo($this->prm->periodoGirar);
		$sql = "SELECT a20.idtercero, a20.idbeneficiario, idpariente, a15.discapacitado, a19.idtrabajador, a21.idconyuge, a20.tipopago, a20.codigopago FROM aportes020 a20 INNER JOIN aportes019 a19 ON a20.iddefuncion=a19.iddefuncion INNER JOIN aportes015 a15 ON a20.idbeneficiario = a15.idpersona INNER JOIN aportes021 a21 ON a20.idbeneficiario = a21.idbeneficiario AND a19.idtrabajador = a21.idtrabajador  WHERE pagadas < 12 AND a19.idfallecido=a19.idtrabajador";
		$datMuertos = $this->con->enArray($sql);
		foreach($datMuertos as $mt){
			$this->preGiro(0, 0, $mt['idtrabajador']);
		}
	}

	private function _girarAfiliadosMuertos($idpersona = 0){

		$val = $this->afiliado->condicionesXPeriodo[$this->prm->periodoGirar]['afiliacion']['agricola'] == 'N'?$this->prm->{$this->prm->periodoGirar}['valorCuota']:$this->prm->{$this->prm->periodoGirar}['valorCuotaCampo'];
		$numerocuotas = 1;
		$periodoproceso = $this->prm->periodoActual;

		if($idpersona == 0){
			$sql = "SELECT a20.iddetalle20, a20.idtercero, a20.idbeneficiario, idpariente, a15.discapacitado, a19.idtrabajador, TO_CHAR(a19.fechadefuncion, '%Y%m') AS perini, a21.idconyuge, a21.idparentesco, a20.tipopago, a20.codigopago, a20.idempresa FROM aportes020 a20 INNER JOIN aportes019 a19 ON a20.iddefuncion=a19.iddefuncion INNER JOIN aportes015 a15 ON a20.idbeneficiario = a15.idpersona INNER JOIN aportes021 a21 ON a20.idbeneficiario = a21.idbeneficiario AND a19.idtrabajador = a21.idtrabajador  WHERE pagadas < 12 AND a19.idfallecido=a19.idtrabajador";
		}else{
			$sql = "SELECT a20.iddetalle20, a20.idtercero, a20.idbeneficiario, idpariente, a15.discapacitado, a19.idtrabajador, TO_CHAR(a19.fechadefuncion, '%Y%m') AS perini, a21.idconyuge, a21.idparentesco, a20.tipopago, a20.codigopago, a20.idempresa FROM aportes020 a20 INNER JOIN aportes019 a19 ON a20.iddefuncion=a19.iddefuncion INNER JOIN aportes015 a15 ON a20.idbeneficiario = a15.idpersona INNER JOIN aportes021 a21 ON a20.idbeneficiario = a21.idbeneficiario AND a19.idtrabajador = a21.idtrabajador  WHERE pagadas < 12 AND a19.idfallecido=a19.idtrabajador AND a19.idtrabajador=$idpersona";
		}

		$datMuertos = $this->con->enArray($sql);

		foreach($datMuertos as $mt){

			$iddef = $mt['iddetalle20'];
			$periodo = $this->prm->periodoGirar;

			$m = Utiles::periodoMayor($mt['perini'], $periodo);

			if($m == 1){
				//Fecha de defuncion mayor al periodo a girar
				continue;
			}

			$sql = "SELECT COUNT(*) AS cant FROM aportes023 WHERE iddetalle20=$iddef AND procesado='S' AND periodo='$periodo'";
			$cant = $this->con->enArray($sql);
			if($cant[0]['cant'] > 0){
				//Ya se proceso la cuota funeraria de el periodo
				continue;
			}

			$sql = "SELECT first 1 MIN(orden) AS orden, iddetalle23, valor FROM aportes023 WHERE iddetalle20=$iddef AND periodo IS NULL AND (procesado='N' OR procesado IS NULL ) GROUP BY iddetalle23, valor";
			$datPag = $this->con->enArray($sql);

			if(is_array($datPag) && count($datPag) > 0){
				//$idempresa = $afilGir->idempresa;
				$usuario = $this->prm->usuario;
				$tipogiro = $this->prm->tipoGiro;
				$fechagiro = date('Y-m-d');
				$idbeneficiario = $mt['idbeneficiario'];
				$idtipobeneficiario = $mt['idparentesco'];
				$discapacitado = $mt['discapacitado'];
				$idtrabajador = $mt['idtrabajador'];
				$idconyuge = $mt['idconyuge'];
				$tipopago = $mt['tipopago'];
				$codigopago = $mt['codigopago'];
				$idempresa = $mt['idempresa'];
				$valor = $datPag[0]['valor'];
				$iddef23 = $datPag[0]['iddetalle23'];
				$orden = $datPag[0]['orden'];
				//Valor a pagar si el beneficiario es discapacitado
				if($discapacitado != 'N'){
					$numerocuotas *= 2;

		//$valor *= 2;
				}

				if($this->verifGiroDoble($periodo, $idbeneficiario, $idtrabajador)){
					//Giro doble
				}else{
					$this->con->noRetorno('BEGIN WORK');
					$sql = "INSERT INTO " . $this->tabla . "(periodo, idempresa, idbeneficiario, idtipobeneficiario, capacidadtrabajo, idtrabajador, idconyuge, tipopago, codigopago, numerocuotas, valor, periodoproceso, programa, embargo, usuario, tipogiro, descuento, fechagiro, idpignoracion, idcertificado) ";
					$sql .= "VALUES('$periodo', '$idempresa','$idbeneficiario','$idtipobeneficiario','$discapacitado','$idtrabajador','$idconyuge','$tipopago','$codigopago','$numerocuotas', '$valor','$periodoproceso', 'M', 'N', '$usuario', '$tipogiro', '0', '$fechagiro', '0', '0')";
					$estGir = $this->con->noRetorno($sql);
					if(! $estGir){
						$this->con->noRetorno('ROLLBACK WORK');
					}

					$sql = "UPDATE aportes023 SET periodo='$periodo', procesado='S' WHERE iddetalle23=$iddef23 AND orden=$orden";
					$estCuoDef = $this->con->noRetorno($sql);
					if(! $estCuoDef){
						$this->con->noRetorno('ROLLBACK WORK');
					}

					$sql = "SELECT COUNT(iddetalle23) AS cant FROM aportes023 WHERE iddetalle20=$iddef AND procesado='S'";
					$pag = $this->con->enArray($sql);

					$sql = "UPDATE aportes020 SET pagadas='" . $pag[0]['cant'] . "' WHERE iddetalle20=$iddef";
					$estPag = $this->con->noRetorno($sql);
					if(! $estPag){
						$this->con->noRetorno('ROLLBACK WORK');
					}

					$this->con->noRetorno('COMMIT WORK');
				}
			}
		}
	}

	private function marcarNominaProcesada($idtrabajador, $idempresa, $periodo){
		$sql = "UPDATE aportes010 SET control='" . $this->prm->tokenControlGiro . "', procesado='S' WHERE idtrabajador=$idtrabajador AND periodo='$periodo' AND idempresa=$idempresa";
		$estMar = $this->con->noRetorno($sql);
		if($estMar){
			return true;
		}else{
			return false;
		}
	}

	private function marcarDefuncionProcesada($cuotas, $pagadas, $usuario, $periodoactual, $iddefuncion, $idbeneficiario){
		$sql = "UPDATE aportes020 SET (cuotas, pagadas, procesado, fechasistema, usuario, periodoproceso)=($cuotas, $pagadas, 'S', cast(getdate() as date), '$usuario', '$periodoactual')  WHERE iddefuncion=$iddefuncion AND idbeneficiario=$idbeneficiario";
		$estDef = $this->con->noRetorno($sql);
		if($estDef){
			return true;
		}else{
			return false;
		}
	}

	private function verifCuotasPagadas($perdef, $idbeneficiario, $idtrabajador){
		$sql = "SELECT SUM(numerocuotas) AS ctcuotas FROM " . $this->tabla . " WHERE periodo>='$perdef' AND idbeneficiario='$idbeneficiario' AND idtrabajador='$idtrabajador'";
		$datAct = $this->con->enArray($sql);

		$sql = "SELECT SUM(numerocuotas) AS ctcuotas FROM aportes009 WHERE periodo>='$perdef' AND idbeneficiario='$idbeneficiario' AND idtrabajador='$idtrabajador'";
		$datAnt = $this->con->enArray($sql);

		$numCuotas = $datAct[0]['ctcuotas'] + $datAnt[0]['ctcuotas'];

		if($numCuotas >= 1){
			return $numCuotas;
		}else{
			return 0;
		}
	}

	/**
	 * Verifica si ya se giro ese periodo al beneficiario del trabajador enviado en el parametro
	 *
	 * @return boolean Retorna Verdadero si ya presenta giro, Falso en el caso contrario
	 */
	private function verifGiroDoble($periodo, $idbeneficiario, $idtrabajador){
		$sql = "SELECT COUNT(idcuota) AS cant FROM " . $this->tabla . " WHERE periodo='$periodo' AND idbeneficiario='$idbeneficiario' AND idtrabajador='$idtrabajador' AND (anulado != 'S' OR anulado IS NULL) ";
		$datAct = $this->con->enArray($sql);

		$sql = "SELECT COUNT(idcuota) AS cant FROM aportes009 WHERE periodo='$periodo' AND idbeneficiario='$idbeneficiario' AND idtrabajador='$idtrabajador' AND (anulado != 'S' OR anulado IS NULL)";
		$datAnt = $this->con->enArray($sql);

		if($datAct[0]['cant'] >= 1 || $datAnt[0]['cant'] >= 1){
			return true;
		}else{
			return false;
		}
	}

	/**
	 * Funcion para definir las tablas sobre las cuales se almacena la informacion del giro y su auditoria
	 *
	 * @param string $tb1 Tabla para el giro
	 * @param string $tb2 Tabla para la auditoria del giro
	 */
	public function definirTablaGiro($tb1, $tb2){
		$this->tabla = $tb1;
		$this->tablaAud = $tb2;
	}

}
?>
