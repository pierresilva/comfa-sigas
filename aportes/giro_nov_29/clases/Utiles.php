<?php
/**
 *
 * @author JOSE LUIS ROJAS
 * @version 1.0.0
 * @since 29 Octubre 2010
 */
include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'session.php';
session();

require_once ('clases/Consultor.php');

class Utiles{

	static $_patron = '%\A(?:(19|20)[0-9]{2}[-](0[1-9]|1[012])[-](0[1-9]|[12][0-9]|3[01]))\Z%';

	public static function periodoMayor($perIni, $perFin){
		$f1bnd = preg_match('%\A(?:(((19|20)[0-9]{2})(0[1-9]|1[012])))\Z%', $perIni);
		$f2bnd = preg_match('%\A(?:(((19|20)[0-9]{2})(0[1-9]|1[012])))\Z%', $perFin);
		if($f1bnd && $f2bnd){

			$f1u = mktime(0, 0, 0, substr($perIni, 4, 2), 1, substr($perIni, 0, 4));
			$f2u = mktime(0, 0, 0, substr($perFin, 4, 2), 1, substr($perFin, 0, 4));

			if($f1u > $f2u){
				return 1;
			}elseif($f1u < $f2u){
				return - 1;
			}else{
				return 0;
			}
		}else{
			return false;
		}
	}


	/**
	 *
	 * Compara dos fechas y encuentra la mayor
	 *
	 * @param string $fecIni Recibe la fecha inicial, formato mm/dd/yyyy
	 * @param string $fecFin Recibe la fecha final, formato mm/dd/yyyy
	 *
	 * @return integer Devuelve 1 si fecha inicial mayor que fecha final, -1 fecha inicial menor que fecha final y 0 si las fechas son iguales
	 */
	public static function fechaMayor($fecIni, $fecFin){
		$f1bnd = preg_match(self::$_patron, $fecIni);
		$f2bnd = preg_match(self::$_patron, $fecFin);
		if($f1bnd && $f2bnd){
			$f1 = explode('-', $fecIni);
			$f1u = mktime(0, 0, 0, $f1[1], $f1[2], $f1[0]);
			$f2 = explode('-', $fecFin);
			$f2u = mktime(0, 0, 0, $f2[1], $f2[2], $f2[0]);
			if($f1u > $f2u){
				return 1;
			}elseif($f1u < $f2u){
				return - 1;
			}else{
				return 0;
			}
		}else{
			return false;
		}
	}

	public static function serializa($obj, $id){
		$path = '..\..\..\planos_generados\giro\serialized';
		$con = new Consultor();
		//$path = $id;
		$sobj =  serialize($obj);
		file_put_contents($path.DIRECTORY_SEPARATOR.$id, $sobj);

		/*$sql = "SELECT COUNT(*) AS cant FROM tmp_serialized WHERE idpersona=$id";
		$dat = $con->enArray($sql);

		if (is_array($dat) && $dat[0]['cant'] == 0) {
			$sql = "INSERT INTO tmp_serialized(idpersona, objeto) VALUES($id, '$sobj')";
			$con->noRetorno($sql);
		}else{
			$sql = "UPDATE tmp_serialized SET objeto='$sobj' WHERE idpersona=$id";
			$con->noRetorno($sql);
		}*/


	}

	public static function deserializa($id){
		$path = '..\..\..\planos_generados\giro\serialized';
		$con = new Consultor();
		//$path = $id;
		if(is_file($path.DIRECTORY_SEPARATOR.$id)){
			$str = @file_get_contents($path.DIRECTORY_SEPARATOR.$id);
			$sobj = @unserialize($str);
			return $sobj;
		}else{
			return false;
		}

		/*$sql = "SELECT idpersona, objeto FROM tmp_serialized WHERE idpersona=$id";
		$dat = $con->enArray($sql);
		if (is_array($dat)) {
			$sobj = @unserialize(stripslashes($dat[0]['objeto']));
			return $sobj;
		}else{
			return false;
		}*/

	}

	public static function diasEntre($fecha1, $fecha2){
		$f1bnd = preg_match(self::$_patron, $fecha1);
		$f2bnd = preg_match(self::$_patron, $fecha2);
		if($f1bnd && $f2bnd){
			$f1 = explode('-', $fecha1);
			$f1 = array_map('intval', $f1);
			$f1u = mktime(0, 0, 0, $f1[1], $f1[2], $f1[0]);
			$f2 = explode('-', $fecha2);
			$f2 = array_map('intval', $f2);
			$f2u = mktime(0, 0, 0, $f2[1], $f2[2], $f2[0]);
			$dias = ($f1u-$f2u)/(60*60*24);
			return $dias;
		}else{
			return false;
		}
	}

	public static function validezCertificado($datCertificado = array(), $periodo){
		$con = new Consultor();
		if(count($datCertificado) > 0){
			$anoI = substr($datCertificado[0]['periodoinicio'], 0, 4);
			$anoF = substr($datCertificado[0]['periodofinal'], 0, 4);
			$mesP = substr($datCertificado[0]['fechapresentacion'], 0, 2);
			if(count($datCertificado[0]) > 0){
				$sql = "SELECT COUNT(*) AS cant FROM aportes024 WHERE idcertificado=".$datCertificado[0]['idtipocertificado']." AND '$anoI'||mesinicia <= '$periodo' AND  '$anoF'||mesfinal >= '$periodo' AND mesinicia >= '$mesP'";
				$rValCert = $con->enArray($sql);
				if(intval($rValCert[0]['cant']) == 1){
					return true;
				}else{
					return false;
				}
			}else {
				return false;
			}
		}else{
			return false;
		}
	}
}

?>