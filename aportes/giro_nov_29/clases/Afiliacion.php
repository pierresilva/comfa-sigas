<?php
/**
 * @author JOSE LUIS ROJAS
 * @version 1.0.0
 * @since 27 Octubre 2010
 *
 */
include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'session.php';
session();

require_once ('clases/Consultor.php');

class Afiliacion{

	private $con;

	public $tope;

	public $existeafiliacion = 0;

	function __construct(){
		$this->con = new Consultor();
	}

	public function obtenerAfiliacion($idempresa=0, $idpersona, $periodo){
		if($idempresa==0){
			return $this->obtenerAfiliacionSinEmpresa($idpersona, $periodo);
		}else{
			return $this->obtenerAfiliacionConEmpresa($idempresa, $idpersona, $periodo);
		}
	}


	public function obtenerAfiliacionConEmpresa($idempresa, $idpersona, $periodo){
		$sql = "SELECT COUNT(idformulario) AS cant FROM aportes016 WHERE idpersona=$idpersona AND idempresa=$idempresa AND estado='A' AND LEFT(convert(varchar, fechaingreso, 112),6) <= '$periodo'";
		//echo $sql.'<br/>go<br/>';
		$aa = $this->con->enArray($sql);

		if(intval($aa[0]['cant']) == 1){
			$this->existeafiliacion = 1;
			return $this->obtenerAfiliacionActiva($idempresa, $idpersona);
		}

		$sql = "SELECT COUNT(idformulario) AS cant FROM aportes017 WHERE idpersona=$idpersona AND idempresa=$idempresa AND estado='I'";
		//echo $sql.'<br/>go<br/>';
		$ai = $this->con->enArray($sql);

		if(intval($ai[0]['cant']) >= 1){
			$this->existeafiliacion = 1;
			return $this->obtenerAfiliacionInactiva($idempresa, $idpersona, $periodo);
		}
		//@todo Error no se encontro afiliacion activa o inactiva para ese periodo con la empresa
		return false;
	}

	public function obtenerAfiliacionSinEmpresa($idpersona, $periodo){
		$sql = "SELECT idempresa FROM aportes016 WHERE idpersona=$idpersona AND estado='A' AND LEFT(convert(varchar, fechaingreso, 112),6) <= '$periodo'";
		//echo $sql.'<br/>go<br/>';
		$aa = $this->con->enArray($sql);

		if(count($aa) == 1){
			$idempresa = $aa[0]['idempresa'];
			return $this->obtenerAfiliacionActiva($idempresa, $idpersona);
		}

		$sql = "SELECT idempresa FROM aportes017 WHERE idpersona=$idpersona AND estado='I' AND LEFT(convert(varchar, fechaingreso, 112),6) <= '$periodo' AND LEFT(convert(varchar, fecharetiro, 112),6) >= '$periodo'";
		//echo $sql.'<br/>go<br/>';
		$ai = $this->con->enArray($sql);

		if(count($ai) >= 1){
			$idempresa = $ai[0]['idempresa'];
			return $this->obtenerAfiliacionInactiva($idempresa, $idpersona, $periodo);
		}

		//@todo Error no se encontro afiliacion activa o inactiva para ese periodo
		return false;
	}

	private function obtenerAfiliacionActiva($idempresa, $idpersona){
//		$sql = "SELECT idempresa, tipoformulario, fechaingreso, fecharetiro, agricola, tipopago, 'A' AS estadoafiliacion  from aportes016 WHERE idpersona=$idpersona AND idempresa=$idempresa AND estado='A' AND auditado='S'";
		$sql = "SELECT a16.idempresa, a48.claseaportante, a48.fechamatricula, tipoformulario, fechaingreso, fecharetiro, agricola, tipopago, 'A' AS estadoafiliacion FROM aportes016 a16 INNER JOIN aportes048 a48 ON a16.idempresa=a48.idempresa WHERE a16.idpersona=$idpersona AND a16.idempresa=$idempresa AND a16.estado='A' AND auditado='S'";
		$afil = $this->con->enArray($sql);

		if($afil != false && count($afil) > 0){
			return $afil[0];
		}else{
			//@todo Error no se encontro afiliacion activa con la empresa
			return false;
		}
	}

	private function obtenerAfiliacionInactiva($idempresa, $idpersona, $periodo){
		//$sql = "SELECT idempresa, tipoformulario, fechaingreso, fecharetiro, agricola, tipopago, 'I' AS estadoafiliacion  from aportes017 WHERE idpersona=$idpersona AND idempresa=$idempresa AND estado='I' AND auditado='S' AND TO_CHAR(fechaingreso, '%Y%m') <= '$periodo' AND TO_CHAR(fecharetiro, '%Y%m') >= '$periodo'";
		$sql = "SELECT a17.idempresa, a48.claseaportante, a48.fechamatricula, tipoformulario, fechaingreso, fecharetiro, agricola, tipopago, 'I' AS estadoafiliacion FROM aportes017 a17 INNER JOIN aportes048 a48 ON a17.idempresa=a48.idempresa WHERE a17.idpersona=$idpersona AND a17.idempresa=$idempresa AND a17.estado='I' AND auditado='S'  AND LEFT(convert(varchar, fechaingreso, 112),6) <= '$periodo' AND LEFT(convert(varchar, fecharetiro, 112),6) >= '$periodo'";
		$afil = $this->con->enArray($sql);

		if($afil != false && count($afil) > 0){
			return $afil[0];
		}else{
			//@todo Error no se encontro afiliacon inactiva con la empresa para el periodo
			return false;
		}
	}
}

?>