<?php

/** 
 * @author JOSE LUIS ROJAS
 * @version 1.0.0
 * @since 28 Octubre 2010
 * 
 * 
 */

include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'session.php';
session();


//$raiz = $_SESSION ['RAIZ'];
//chdir ( $raiz );

require_once ('clases/Consultor.php');

class Nominas{
	
	private $con = null;
	public $bndNovedades = false;
	
	public $cantidadNominas,$totalHoras,$totalSalBasico,$totalIngBase,$vacaciones,$inc_tem_emfermedad,$inc_tem_acc_trabajo,$lic_maternidad;
	
	function __construct(){
		$this->con = new Consultor();
	}
	
	function obtenerBanderasNomina($idempresa, $idpersona, $periodo){
		//$sql = "SELECT vacaciones, inc_tem_emfermedad, inc_tem_acc_trabajo, lic_maternidad, tarifaaporte FROM aportes010 WHERE idempresa=$idempresa AND idtrabajador=$idpersona AND periodo='$periodo'";
		$sql = "SELECT vacaciones, inc_tem_emfermedad, inc_tem_acc_trabajo, lic_maternidad FROM aportes010 WHERE idempresa=$idempresa AND idtrabajador=$idpersona AND periodo='$periodo'";
		$bnd = $this->con->enArray($sql);
		if($bnd != false && count($bnd) > 0){
			$this->vacaciones = $bnd[0]['vacaciones'];
			$this->inc_tem_emfermedad = $bnd[0]['inc_tem_emfermedad'];
			$this->inc_tem_acc_trabajo = $bnd[0]['inc_tem_acc_trabajo'];
			$this->lic_maternidad = $bnd[0]['lic_maternidad'];
			//$this->tarifaAporte = $bnd[0]['tarifaaporte'];
			
			if($this->lic_maternidad == 'X' || $this->inc_tem_emfermedad == 'X' || $this->inc_tem_acc_trabajo > 0 || $this->vacaciones == 'X'){
				$this->bndNovedades = true;
			}
		}
	}
	
	function obtenerValoresNominas($idpersona, $periodo){
		$sql = "SELECT COUNT(idplanilla) AS cant, SUM(horascotizadas) AS totalhoras, SUM(salariobasico) AS totalsalbasico, SUM(ingresobase) AS totalingbase FROM aportes010 WHERE idtrabajador=$idpersona AND periodo='$periodo' AND correccion='C'";
		$val = $this->con->enArray($sql);
		if($val != false && count($val) > 0){
			$this->cantidadNominas = intval($val[0]['cant']);
			$this->totalHoras = intval($val[0]['totalhoras']);
			$this->totalSalBasico = intval($val[0]['totalsalbasico']);
			$this->totalIngBase = intval($val[0]['totalingbase']);
		}else{
			$this->cantidadNominas = 0;
		}
	}
}

?>