<?php
/**
 * @author JOSE LUIS ROJAS
 * @version 1.0.0
 * @since 26 Octubre 2010
 */
include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'session.php';
session();

require_once ('clases/Consultor.php');
date_default_timezone_set('America/Bogota');

class Aportes{
	
	private static $con;
	
	/**
	 * Busca para el giro general las empresas que cumplan las siguientes condiciones:
	 * 1. Empresas que pagaron periodos iguales o previos al actual dentro de los limites de pago del periodo
	 * 2. Empresas que pagaron el periodo actual antes de la fecha inicial de pago
	 * 3. Empresas que pagaron periodos anteriores al actual dentro de los limites de pago del periodo actual solo con planilla unica
	 * 
	 * @param string $fechaIniPago Fecha inicial de pago del periodo
	 * @param string $fechaFinPago Fecha final de pago del periodo
	 * @param string $fechaFinPeriodo Fecha final del periodo
	 * @param string $periodo Periodo actual a girar
	 * @return array En caso de encontrar aportes retorna un array con estos, sino retorna falso
	 */
	static public function obtenerAportesEmpresasGiroGral($fechaIniPago, $fechaFinPago, $fechaFinPeriodo, $periodo){
		self::$con = new Consultor();
		$sql = "SELECT DISTINCT periodo, a11.idempresa, planilla, a48.fechaaportes, a48.claseaportante FROM aportes011 a11 INNER JOIN aportes048 a48 ON a11.idempresa=a48.idempresa WHERE ((fechapago>'$fechaIniPago' AND fechapago<='$fechaFinPago') OR (a11.fechasistema>'$fechaIniPago' AND a11.fechasistema<='$fechaFinPago')) AND periodo<='$periodo' AND contratista='N' AND a48.legalizada='S' GROUP BY periodo,a11.idempresa, planilla, a48.fechaaportes, a48.claseaportante  UNION SELECT DISTINCT periodo,a11.idempresa,planilla, a48.fechaaportes, a48.claseaportante  FROM aportes011 a11 INNER JOIN aportes048 a48 ON a11.idempresa=a48.idempresa WHERE periodo='$periodo' AND fechapago<'$fechaIniPago' AND contratista='N' AND a48.legalizada='S' GROUP BY periodo, a11.idempresa, planilla, a48.fechaaportes, a48.claseaportante  UNION SELECT DISTINCT periodo,a11.idempresa,planilla, a48.fechaaportes, a48.claseaportante  FROM aportes011 a11 INNER JOIN aportes048 a48 ON a11.idempresa=a48.idempresa WHERE periodo<'$periodo' AND (fechapago>'$fechaIniPago' AND fechapago<='$fechaFinPago') AND contratista='N' AND a48.legalizada='S' AND comprobante='PU' GROUP BY periodo,a11.idempresa,planilla, a48.fechaaportes, a48.claseaportante";
		//$sql = "SELECT DISTINCT periodo, a11.idempresa, planilla, a48.fechaaportes, a48.claseaportante FROM aportes011 a11 INNER JOIN aportes048 a48 ON a11.idempresa=a48.idempresa WHERE a48.claseaportante=2875 AND ((fechapago>'$fechaIniPago' AND fechapago<='$fechaFinPago') OR (a11.fechasistema>'$fechaIniPago' AND a11.fechasistema<='$fechaFinPago')) AND periodo='$periodo' AND contratista='N' AND a48.legalizada='S' GROUP BY periodo,a11.idempresa, planilla, a48.fechaaportes, a48.claseaportante  UNION SELECT DISTINCT periodo,a11.idempresa,planilla, a48.fechaaportes, a48.claseaportante  FROM aportes011 a11 INNER JOIN aportes048 a48 ON a11.idempresa=a48.idempresa WHERE a48.claseaportante=2875 AND periodo='$periodo' AND fechapago<'$fechaIniPago' AND contratista='N' AND a48.legalizada='S' GROUP BY periodo, a11.idempresa, planilla, a48.fechaaportes, a48.claseaportante  UNION SELECT DISTINCT periodo,a11.idempresa,planilla, a48.fechaaportes, a48.claseaportante  FROM aportes011 a11 INNER JOIN aportes048 a48 ON a11.idempresa=a48.idempresa WHERE a48.claseaportante=2875 AND periodo='$periodo' AND (fechapago>'$fechaIniPago' AND fechapago<='$fechaFinPago') AND contratista='N' AND a48.legalizada='S' AND comprobante='PU' GROUP BY periodo,a11.idempresa,planilla, a48.fechaaportes, a48.claseaportante";
		$rAp = self::$con->enArray($sql);
		if($rAp != false && count($rAp) > 0){
			return $rAp;
		}else{
			//trigger_error("No se encontraron aportes de empresas para el periodo $periodo", E_USER_ERROR);
			return false;
		}
	}
	
	static public function obtenerTrabajadoresXEmpresaGiroGral($idempresa, $periodo, $fecinipago, $fecfinpago){
		self::$con = new Consultor();
		$sql = "SELECT idtrabajador FROM aportes010 WHERE idempresa=$idempresa AND periodo='$periodo' AND procesado='N' AND fechapago>'$fecinipago' AND fechapago<='$fecfinpago' AND control IS NULL GROUP BY idtrabajador";
		$rAp = self::$con->enArray($sql);
		if($rAp != false && count($rAp) > 0){
			return $rAp;
		}else{
			//trigger_error("No se encontraron aportes de empresas para el periodo $periodo", E_USER_ERROR);
			//echo $rAp==false?$idempresa.' '.$periodo.' '.$planilla.'<br/>':'';
			return false;
		}
	}
	
	static public function obtenerTrabajadoresXPlanillaEmpresaGiroGral($idempresa, $periodo, $planilla){
		self::$con = new Consultor();
		$sql = "SELECT idtrabajador FROM aportes010 WHERE idempresa=$idempresa AND planilla='$planilla' AND periodo='$periodo' AND procesado='N' AND control IS NULL GROUP BY idtrabajador";
		$rAp = self::$con->enArray($sql);
		if($rAp != false && count($rAp) > 0){
			return $rAp;
		}else{
			//trigger_error("No se encontraron aportes de empresas para el periodo $periodo", E_USER_ERROR);
			//echo $rAp==false?$idempresa.' '.$periodo.' '.$planilla.'<br/>':'';
			return false;
		}
	}
	
	/**
	 * @todo empresas por nit para el giro adicional
	 */
	static public function obtenerAportesEmpresasGiroXNit($fechaIniPago, $fechaFinPago, $fechaFinPeriodo, $periodo, $idempresa){
		self::$con = new Consultor();
		$sql = "SELECT DISTINCT a11.periodo, a11.idempresa, a11.planilla FROM aportes011 a11 INNER JOIN aportes048 a48 ON a11.idempresa=a48.idempresa WHERE ((a11.fechapago>'$fechaIniPago' AND a11.fechapago<='$fechaFinPago') OR (a11.fechasistema>'$fechaIniPago' AND a11.fechasistema<='$fechaFinPago')) AND a11.periodo='$periodo' AND contratista='N' AND a48.legalizada='S' AND a48.fechaaportes <= '$fechaFinPeriodo' AND a11.idempresa=$idempresa GROUP BY a11.periodo, a11.idempresa, a11.planilla UNION SELECT DISTINCT a11.periodo, a11.idempresa, a11.planilla FROM aportes011 a11 INNER JOIN aportes048 a48 ON a11.idempresa=a48.idempresa INNER JOIN aportes010 a10 ON a10.planilla = a11.planilla AND a10.idempresa = a11.idempresa AND a10.periodo = a11.periodo WHERE a11.periodo='$periodo' AND a11.fechapago<'$fechaIniPago' AND contratista='N' AND a48.legalizada='S' AND a48.fechaaportes <= '$fechaFinPeriodo' AND a11.idempresa=$idempresa GROUP BY a11.periodo, a11.idempresa, a11.planilla UNION SELECT DISTINCT a11.periodo, a11.idempresa, a11.planilla FROM aportes011 a11 INNER JOIN aportes048 a48 ON a11.idempresa=a48.idempresa WHERE a11.periodo='$periodo' AND (a11.fechapago>'$fechaIniPago' AND a11.fechapago<='$fechaFinPago') AND contratista='N' AND a48.legalizada='S' AND a11.idempresa=$idempresa AND a48.fechaaportes <= '$fechaFinPeriodo' AND comprobante='PU' GROUP BY a11.periodo, a11.idempresa, a11.planilla ";
		$rAp = self::$con->enArray($sql);
		if($rAp != false && count($rAp) > 0){
			return $rAp;
		}else{
			//trigger_error("No se encontraron aportes de empresas para el periodo $periodo", E_USER_ERROR);
			return false;
		}
	}
	
	/**
	 * @todo empresas por nit y cedula del afiliado para el giro adicional
	 */
	static public function obtenerAportesEmpresasGiroXAfiliado($fechaIniPago, $fechaFinPago, $fechaFinPeriodo, $periodo, $idtrabajador){
		self::$con = new Consultor();
		$sql = "SELECT DISTINCT a11.periodo, a11.idempresa, a11.planilla FROM aportes011 a11 INNER JOIN aportes048 a48 ON a11.idempresa=a48.idempresa INNER JOIN aportes010 a10 ON a10.idempresa = a11.idempresa AND a10.periodo = a11.periodo WHERE ((a11.fechapago>'$fechaIniPago' AND a11.fechapago<='$fechaFinPago') OR (a11.fechasistema>'$fechaIniPago' AND a11.fechasistema<='$fechaFinPago')) AND a11.periodo='$periodo' AND contratista='N' AND a48.legalizada='S' AND a48.fechaaportes <= '$fechaFinPeriodo' AND idtrabajador=$idtrabajador GROUP BY a11.periodo, a11.idempresa, a11.planilla UNION SELECT DISTINCT a11.periodo, a11.idempresa, a11.planilla FROM aportes011 a11 INNER JOIN aportes048 a48 ON a11.idempresa=a48.idempresa INNER JOIN aportes010 a10 ON a10.idempresa = a11.idempresa AND a10.periodo = a11.periodo WHERE a11.periodo='$periodo' AND a11.fechapago<'$fechaIniPago' AND contratista='N' AND a48.legalizada='S' AND a48.fechaaportes <= '$fechaFinPeriodo' AND idtrabajador=$idtrabajador GROUP BY a11.periodo, a11.idempresa, a11.planilla UNION SELECT DISTINCT a11.periodo, a11.idempresa, a11.planilla FROM aportes011 a11 INNER JOIN aportes048 a48 ON a11.idempresa=a48.idempresa INNER JOIN aportes010 a10 ON a10.idempresa = a11.idempresa AND a10.periodo = a11.periodo WHERE a11.periodo='$periodo' AND (a11.fechapago>'$fechaIniPago' AND a11.fechapago<='$fechaFinPago') AND contratista='N' AND a48.legalizada='S' AND idtrabajador=$idtrabajador AND a48.fechaaportes <= '$fechaFinPeriodo' AND comprobante='PU' GROUP BY a11.periodo, a11.idempresa, a11.planilla ";
		$rAp = self::$con->enArray($sql);
		if($rAp != false && count($rAp) > 0){
			return $rAp;
		}else{
			//trigger_error("No se encontraron aportes de empresas para el periodo $periodo", E_USER_ERROR);
			return false;
		}
	}
}

?>