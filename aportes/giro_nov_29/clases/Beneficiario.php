<?php
include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'session.php';
session();

require_once ('clases/Consultor.php');
require_once ('aportes/giro/clases/Persona.php');
require_once ('Relaciones.php');
require_once ('Utiles.php');

class Beneficiario extends Persona{
	
	public $con = null;
	public $condicionesXPeriodo = array();
	public $fechaAfiliacion;
	public $bndCertificado = false;
	public $parentesco = 0;
	public $defuncion = null;
	public $periodoDefuncion = null;
	public $idconyuge = 0;
		
	public function __construct($idpersona){
		$this->con = new Consultor();
		$this->idpersona = $idpersona;
	}
	
	public function obtenerDatosPersona(){}
	
	/**
	 * 
	 */
	public function obtenerDatosBeneficiario($idtrabajador){
		$sql = "SELECT idtipodocumento, identificacion, sexo, fechanacimiento, discapacitado, a21.estado, a21.giro, a21.fechaasignacion, a21.idparentesco, a21.idconyuge FROM aportes015 a15 INNER JOIN aportes021 a21 ON a21.idbeneficiario=a15.idpersona WHERE a15.idpersona=$this->idpersona AND a21.idtrabajador=$idtrabajador";
		$dat = $this->con->enArray($sql);
		
		if($dat != false && count($dat) != 0){
			//$this->idpersona = $idpersona;
			$this->discapacitado = $dat[0]['discapacitado'];
			$this->estado = $dat[0]['estado'];
			$this->fechaNacimiento = $dat[0]['fechanacimiento'];
			$this->sexo = $dat[0]['sexo'];
			$this->tipoDocumento = $dat[0]['idtipodocumento'];
			$this->numeroDocumento = $dat[0]['identificacion'];
			$this->giro = $dat[0]['giro'];
			$this->fechaAfiliacion = $dat[0]['fechaasignacion'];
			$this->parentesco = $dat[0]['idparentesco'];
			$this->idconyuge = $dat[0]['idconyuge'];
		}else{
			return false;
		}
	}

	public function valFechaAfiliacion($fecfinper){
		$valafil = Utiles::fechaMayor($this->fechaAfiliacion, $fecfinper);
		if($valafil == -1){
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 * 
	 * @param string $periodo
	 */
	public function verificarCertificado($periodo){
		if($this->discapacitado == 'N'){
			//Hijos/Hermanos sin discapacidad
			if($this->parentesco == 35 || $this->parentesco == 37 || $this->parentesco == 38){
				if($this->edad > 0 && $this->edad <= 12){
					//no necesita certificado
					return true;
				}elseif($this->edad > 12 && $this->edad < 19){
					//certificado de escolaridad o universidad
					$sql = "SELECT idtipocertificado, fechapresentacion, estado, periodoinicio, periodofinal FROM aportes026 WHERE idbeneficiario=$this->idpersona AND periodoinicio <= '$periodo' AND periodofinal >= '$periodo' AND idtipocertificado IN (55,56) AND estado='A'";
					$rCertPers = $this->con->enArray($sql);
					if(count($rCertPers) > 0 && is_array($rCertPers[0]) && count($rCertPers[0]) == 5){
						if(Utiles::validezCertificado($rCertPers, $periodo) == true){
							return true;
						}else{
							//@todo error certificado de escolaridad o universidad vencido
							return 2930;
						}
					}else{
						//@todo error no tiene certificado de escolaridad o universidad
						return 2929;
					}
				}else{
					//edad fuera del rango
					return 2934;
				}
			}elseif($this->parentesco == 36){
				//Padres sin discapacidad
				if($this->edad > 60){
					//Certificado de supervivencia
					$sql = "SELECT idtipocertificado, fechapresentacion, estado, periodoinicio, periodofinal FROM aportes026 WHERE idbeneficiario=$this->idpersona AND periodoinicio <= '$periodo' AND periodofinal >= '$periodo' AND idtipocertificado = 57 AND estado='A'";
					$rCertPers = $this->con->enArray($sql);
					if(count($rCertPers) > 0 && is_array($rCertPers[0]) && count($rCertPers[0]) == 5){
						if(Utiles::validezCertificado($rCertPers, $periodo) == true){
							return true;
						}else{
							//@TODO error certificado de supervivencia vencido
							return 2932;
						}
					}else{
						//@TODO error No tiene certificado de supervivencia
						return 2931;
					}
				}else{
					//Edad fuera del rango
					return 2935;
				}
			}
		}elseif($this->discapacitado == 'S'){
			//Busca Certificado de discapacidad - se presenta solo una vez en la vida
			$sql = "SELECT COUNT(idcertificado) AS cant FROM aportes026 WHERE idbeneficiario=$this->idpersona AND idtipocertificado=58 AND estado = 'A'";
			$rCantDisc = $this->con->enArray($sql);
			if(intval($rCantDisc[0]['cant']) >= 1){
				$sql = "SELECT idtipocertificado, fechapresentacion, estado, periodoinicio, periodofinal FROM aportes026 WHERE idbeneficiario=$this->idpersona AND periodoinicio <= '$periodo' AND periodofinal >= '$periodo' AND idtipocertificado = 57 AND estado='A'";
				$rCertPers = $this->con->enArray($sql);
				if(count($rCertPers) > 0 && is_array($rCertPers[0]) && count($rCertPers[0]) == 5){
					if(Utiles::validezCertificado($rCertPers, $periodo) == true){
						return true;
					}else{
						//@TODO error certificado de supervivencia vencido						
						return 2932;
					}
				}else{
					//@TODO Error no tiene certificado de supervivencia
					return 2931;
				}
			}else{
				//@TODO Error no tiene certificado de discapacidad
				return 2933;
			}
		}
	}
}

?>
