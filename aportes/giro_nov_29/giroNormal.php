<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$url=$_SERVER['PHP_SELF'];
include_once $raiz.DIRECTORY_SEPARATOR.'phpComunes'.DIRECTORY_SEPARATOR.'auditoria.php';
auditar($url);
include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'Consultor.php';

$con = new Consultor();
$sql = "SELECT top 1 periodo, fechalimite, controlproceso, estadogiro FROM aportes012 WHERE procesado = 'N' ORDER BY periodo ASC";
$dper1 = $con->enArray($sql);
$sql = "SELECT top 1 fechalimite FROM aportes012 WHERE procesado = 'S' ORDER BY periodo DESC";
$dper2 = $con->enArray($sql);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>::Giro::</title>
<link type="text/css" href="../../css/formularios/base/ui.all.css" rel="stylesheet" />
<link type="text/css" href="../../css/Estilos.css" rel="stylesheet"/>
<link type="text/css" href="../../css/estilo_tablas.css" rel="stylesheet"/>
<link href="../../css/formularios/custom-theme/jquery-ui.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../../js/jquery-1.3.2.min.js"></script>
<script src="../../js/formularios/ui/jquery-ui-1.7.2.custom.js" type="text/javascript" language="javascript"></script>
<script src="../../js/formularios/ui/i18n/ui.datepicker-es.js" type="text/javascript" language="javascript"></script>
<script src="../../js/formularios/ui/jquery.ui.widget.js" type="text/javascript" language="javascript"></script>
<script src="../../js/formularios/ui/jquery.ui.button.js" type="text/javascript" language="javascript"></script>
<script src="../../js/formularios/ui/jquery.ui.progressbar.js" type="text/javascript" language="javascript"></script>
<script type="text/javascript" src="js/js-giro.js" language="javascript"></script>
<script type="text/javascript" src="js/inicial.js" language="javascript"></script>
<style type="text/css">
#divFormGiro{
	width: 90%;
	margin: 0 auto;
}
#divFormGiro label{
	width: 120px; /*font-size: 11pt;*/ display: inline-block;
}
#divFormGiro input[type=text], #divFormGiro select{
	width: 130px;
	background: white;
	border: 1px solid #669800;
	margin-first: 5px
}
#btnGiroNormal, #btnDeshGiro, #btnHistGiro, #btnGiroAdicIndividual, #btnDeshGiroAdicIndividual, #btnGiroReclamosTrabajador, #btnGiroReclamosEmpresa, #btnGiroAdicEmpresa, #btnDeshGiroAdicEmpresa{
	boder: 1px solid #AFAFAF;
	width: 140px;
	height: 50px;
	text-align: left;
	font-size: 15px;
	background-image: url('../../imagenes/procesar.png');
	background-position: right;
	background-repeat: no-repeat;
	margin-right: 1em;
}
.box-table-a{
	width: none;
}
#chkideall{
	float: left;
}
</style>
</head>

<body>
<div id="tabs">
 <ul>
	<li><a href="#giro-normal">Giro General</a></li>
	<li><a href="adicional/giroAdicional.php">Giro Adicional</a></li>
	<li><a href="giroAdicIndividual.php">Giro Individual</a></li>
	<li><a href="giroAdicEmpresa.php">Giro Empresa</a></li>
 </ul>
<div id="giro-normal">
<table id="tblArchivosCargados" border="0" cellspacing="0" class="box-table-a" style="margin: 1em auto; ">
<tbody>
<tr>
<td style="width: 40%">
<div id="divFormGiro">
<div><label>Periodo a procesar</label><input type="text" id="txtPeriodo" name="txtPeriodo" readonly="readonly" value="<?php echo $dper1[0]['periodo']?>"/></div>
<div><label>Fecha inicial aportes</label><input type="text" id="txtFecInicial" name="txtFecInicial" readonly="readonly" value="<?php echo $dper2[0]['fechalimite']?>"/></div>
<div><label>Fecha final aportes</label><input type="text" id="txtFecFinal" name="txtFecFinal" readonly="readonly" value="<?php echo $dper1[0]['fechalimite']?>"/></div>
<div><label>C&oacute;digo Control</label><input type="text" id="txtCodControl" name="txtCodControl" readonly="readonly" value="<?php echo $dper1[0]['controlproceso']?>"/></div>
<div><label>Estado Giro</label><input type="text" id="txtEstado" name="txtEstado" readonly="readonly" value="<?php echo $dper1[0]['estadogiro']=='C'?'Cerrado':'Abierto' ?>"/></div>
 <!-- <div><label>Salario B&aacute;sico</label><input type="radio" name="radTipSal" id="radSalBas" checked="checked" /></div>
 <div><label>Ingreso B&aacute;se</label><input type="radio" name="radTipSal" id="radIngBas" /></div> -->
</div>
</td>
<td>
<div id="giro-pros">
<div id="progreso"></div>
            					</div>
            					<textarea rows="10" cols="80" id="txtResumen" name="txtResumen"></textarea>
            				</td>
            			</tr>
            		</tbody>
            		<thead>
            			<tr>
            				<th><strong>Giro</strong></th>
            				<th><strong>Resumen</strong></th>
            			</tr>
            		</thead>
            		<tfoot>
            			<tr>
            				<td colspan="2"><p align="center">
            					<input type="button" id="btnGiroNormal" name="btnGiroNormal" value="Procesar Giro" <?php echo $dper1[0]['estadogiro']=='C'?'Disabled="Disabled"':'' ?> />
            					<input type="button" id="btnDeshGiro" name="btnDeshGiro" value="Deshacer Giro" <?php echo $dper1[0]['estadogiro']=='C'?'Disabled="Disabled"':'' ?> />
								<input type="button" id="btnHistGiro" name="btnHistGiro" value="Historico Giro" <?php echo $dper1[0]['estadogiro']=='C'?'Disabled="Disabled"':'' ?> />
							</p></td>
            			</tr>
            		</tfoot>
            		</table>
</div>
</div>
<!-- colaboracion en linea -->
<div id="dialogo-archivo" title="Planilla &Uacute;nica">
<div id="progreso" style="display: none; font-size: 15pt; font-weight: bold;"><span id="pg">0</span> de <span id="tt"></span> archivos</div>
<div id="log"></div>
</div>
<div id="dialog-form2" title="Colaboraci&oacute;n en l&iacute;nea" style="display:none">
<p>Por favor diligencie este formulario para enviar comentarios, errores o falencias encontradas en el proceso. M&aacute;ximo 250 caracteres </p>
<label>Tus comentarios:</label><br />
<textarea name="notas" id="notas" cols="60", rows="10"></textarea>
</div>

<!-- fin colaboracion -->

<!-- Manual Ayuda -->
<div id="ayuda" title="Manual Ayuda Planilla �nica" style="background-image:url('../../imagenes/FondoGeneral0.png')">
</div>
</body>
<script language="javascript">
function mostrarAyuda(){
	$("#ayuda").dialog('open');
	}

function notas(){
	$("#dialog-form2").dialog('open');
	}
</script>
</html>