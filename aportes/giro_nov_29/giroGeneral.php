<?php
/* autor:       orlando puentes
 * fecha:       28/08/2012
 * objetivo:    configuracion de los reportes de radicacion
 **********************************************************
 */

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$usuario=$_SESSION['USUARIO'];
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}
$sql = "Select top 1 periodo from aportes012 where procesado='N' order by periodo";
$rs = $db->querySimple($sql);
$row=$rs->fetch();
$periodo=$row['periodo'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Giro General</title>
<script type="text/javascript" src="../../js/1.6/jquery-1.6.2.min.js" ></script>
<script type="text/javascript" src='../../js/comunes.js' ></script>
<script type="text/javascript" src="js/giroGeneral.js"></script>
<style type="text/css">
.titulo1 {
	font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
	font-size: 16px;
	font-style: oblique;
	font-weight: 100;
	color: #999;
}
</style>
</head>

<body>
<center>
<br />
<img src="../../imagenes/logo_reporte.png" width="362" height="70" />
<br />
<img src="../../imagenes/giroGeneral.png" width="513" height="64" />
<br /><br /><br />
<table border="0" width="30%">
<tr><td class="titulo1">Periodo a Procesar</td><td class="titulo1"><?php echo $periodo; ?></td></tr>
</table>
<br /><br /><br />
<div id="titulo1" style="display:block">
<label style="cursor:pointer" onclick="procesar()"><img src="../../imagenes/procesar2.png" width="143" height="30" /></label>
</div>
<div id="horaInicio"></div>
<div id="horaFin"></div>

</center>
</body>
</html>
