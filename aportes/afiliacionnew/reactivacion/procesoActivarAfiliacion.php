<?php
set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $_SESSION['RAIZ'] . DIRECTORY_SEPARATOR . 'rsc/pdo/IFXDbManejador.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo $cadena;
	exit();
}

$fecha = date("m/d/Y");
$usuario=$_SESSION['USUARIO'];

$c0 = $_REQUEST['v0'];		//idFormulario
$c1 = $_REQUEST['v1'];		//idpersona
$c2 = $_REQUEST['v2'];
$c2 = $fecha . " - " . $usuario . " - " . $c2;

$sql="SELECT *
	  FROM aportes016 a16
	  INNER JOIN aportes017 a17 ON a17.idpersona=a16.idpersona AND a17.idempresa=a16.idempresa
      WHERE a17.idformulario=:idFormulario";

$statement = $db->conexionID->prepare($sql);
$statement->bindValue(":idFormulario", $c0,  PDO::PARAM_STR);
$statement->execute();

if($statement->rowCount() == 0){
	$db->conexionID->beginTransaction();
	
	$sql="	INSERT INTO aportes016(tipoformulario, tipoafiliacion, idempresa, idpersona, fechaingreso, horasdia, horasmes, salario, agricola, cargo, primaria, estado, fecharetiro, motivoretiro, fechanovedad, semanas, fechafidelidad, estadofidelidad, traslado, codigocaja, flag, tempo1, tempo2, fechasistema, usuario, tipopago, categoria, auditado, idagencia, idradicacion)
	SELECT tipoformulario, tipoafiliacion, idempresa, idpersona, fechaingreso, horasdia, horasmes, salario, agricola, cargo, primaria, 'A', null, null, fechanovedad, semanas, null, null, traslado, codigocaja, flag, tempo1, tempo2, CAST(getDate() AS DATE), '".$usuario."', tipopago, categoria, auditado, idagencia, idradicacion
	FROM aportes017 WHERE idformulario=:idFormulario";
	$statement = $db->conexionID->prepare($sql);
	$statement->bindValue(":idFormulario", $c0,  PDO::PARAM_STR);
	$guardada = $statement->execute();
	
	if($guardada){
		
		
		$sqlTipo="SELECT tipoafiliacion,idempresa FROM aportes017 WHERE idformulario=$c0";
		$rs=$db->querySimple($sqlTipo);
		$w=$rs->fetch();
		
		$tipoafiliacionBase=$w['tipoafiliacion'];
		$idempresaBase=$w['idempresa'];
		
		
		if($tipoafiliacionBase !=18){
			
			$sqlActualizar="UPDATE aportes048 SET estado='A' WHERE idempresa=$idempresaBase";
			$rs=$db->queryActualiza($sqlActualizar,'aportes048');
		}
		
		
		$sql="DELETE FROM aportes017 WHERE idformulario=:idFormulario";
		$statement = $db->conexionID->prepare($sql);
		$statement->bindValue(":idFormulario", $c0,  PDO::PARAM_STR);
		$guardada = $statement->execute();
		
		$guardadaActEmp = $statement->execute();
	
		if($guardada){			
			$sql="SELECT * FROM aportes088 WHERE idregistro=:c1 AND identidad=1";
			$statement = $db->conexionID->prepare($sql);
			$statement->bindValue(":c1", $c1,  PDO::PARAM_STR);
			$statement->execute();
			
			if($statement->rowCount() == 0){								
				$sql="	INSERT INTO aportes088 (idregistro,identidad,observaciones) VALUES (:idregistro,1,:observaciones)";
				$statement = $db->conexionID->prepare($sql);
				$statement->bindValue(":idregistro", $c1,  PDO::PARAM_STR);
				$statement->bindValue(":observaciones", $c2,  PDO::PARAM_STR);
				$guardada = $statement->execute();
				
				if($guardada){
					$db->conexionID->commit();
					echo 1;
				} else {
					$db->conexionID->rollBack();
					echo -3;
				}								
			} else {				
				$fila=$statement->fetch();	
				$observaciones=$fila['observaciones'];				
				$c2 = $c2 . "<br>" . $observaciones;

				$sql="	UPDATE aportes088 SET observaciones=:observaciones WHERE idregistro=:idregistro AND identidad=1";
				$statement = $db->conexionID->prepare($sql);
				$statement->bindValue(":idregistro", $c1,  PDO::PARAM_STR);
				$statement->bindValue(":observaciones", $c2,  PDO::PARAM_STR);
				$guardada = $statement->execute();
				
				if($guardada){
					$db->conexionID->commit();
					echo 1;
				} else {
					$db->conexionID->rollBack();
					echo -3;
				}
			}
		} else {
			$db->conexionID->rollBack();
			echo 0;
		}
	} else {
		$db->conexionID->rollBack();
		echo -1;
	}		
} else {
	echo -2;
}

?>