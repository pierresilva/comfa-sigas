<?php
set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");

$root= $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $_SESSION['RAIZ'] . DIRECTORY_SEPARATOR . 'rsc/pdo/IFXDbManejador.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo $cadena;
	exit();
}

$campo0=$_REQUEST['v0'];	//idTipoDocumento
$campo1=$_REQUEST['v1']; 	//Documento

$sql="SELECT * FROM aportes015 a15 WHERE a15.idtipodocumento=:campo0 AND a15.identificacion=:campo1";

$statement = $db->conexionID->prepare($sql);
$statement->bindValue(":campo0", $campo0,  PDO::PARAM_STR);
$statement->bindValue(":campo1", $campo1,  PDO::PARAM_STR);
$statement->execute();

if($statement->rowCount() != 0){
	$fila = array_map("utf8_encode",$statement->fetch());	
	echo json_encode(Array($fila));
} else {
	echo 0;
	exit();
}

?>