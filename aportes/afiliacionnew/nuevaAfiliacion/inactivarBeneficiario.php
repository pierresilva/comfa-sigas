<?php

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once '../../rsc/pdo/IFXDbManejador.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo $cadena;
	exit();
}
	$sql = "SELECT DISTINCT a21.idbeneficiario
			FROM aportes015 a15
				INNER JOIN aportes021 a21 ON a21.idbeneficiario=a15.idpersona
			WHERE
				(DATEDIFF(year,fechanacimiento,getdate() )+   
				case
				when ( Month(getdate()) < Month(fechanacimiento) Or
				(Month(getdate()) = Month(fechanacimiento) And
				Day(getdate()) < day(fechanacimiento))) Then -1 else 0 end)=19
				AND a21.idparentesco in(35,38)
				AND a21.estado='A'";
	$rs = $db->querySimple($sql);
	$banIdBeneficiarios = "";
	while($row = $rs->fetch()){
		$banIdBeneficiarios .= $row["idbeneficiario"].",";
	}
	$banIdBeneficiarios = trim($banIdBeneficiarios,',');
	
	$queryUpdate = "UPDATE aportes021 SET estado='I', idmotivo=3707 WHERE idbeneficiario IN ($banIdBeneficiarios)";
	$db->queryActualiza($queryUpdate);	
?>