var URL=src();
var idPersona=0;
var idConyuge=0;
var idEmpresa=0;
var idBeneficiario=0;
var idFormulario=0;
var idPBiologico=0;
var idRelacion = 0;
var controlPersona=0;
var controlAfiliacion=0;
var idr = 0;
var SMLV = salarioMinimo();
var tempo=0;
var SI = 0;
var formulario=0;
var existePersona=0;
var fechaNaceBen='';

$(document).ready(function(){
	$("#txtNit").bind("change", buscarNuevaEmpresa);
	$("#txtTipoBen2").bind("change", function(){
		if( $("#txtTipoBen2").val() == 38){
			$("#pbiologico2").css("display","block");
		}
		else{
			$("#pbiologico2").css("display","none");
		}
			
		buscarRelacionesBen();
	});
	$("#txtNumero").bind("change", 	function(){
		buscarRegPersona(0,$("#tipDocAfi").val(),$("#txtNumero").val());
	});
	$("#tipDocAfi").bind("change", 	function(){
		var n = $.trim($("#txtNumero").val());
		if(n.length > 0)
			buscarRegPersona(0,$("#tipDocAfi").val(),$("#txtNumero").val());
	});
	
	$("#tipoDocBen2").bind("change",function(){
		contarAfiliaciones();
	});
	$("#numeroPB").blur(function(){
		formulario=1;
		buscarPersona(0,$("#tipoDocPB"),$("#numeroPB"),'','',2);
	});
	
	$("#numeroBen2").blur( function(){
		contarAfiliaciones();
	});
	
	$("#txtRadicacion").blur( buscarRadicacion);
	
	$("#txtTipoBen").bind("change",function(){
		if($("#txtTipoBen").val()==38){
			$("#pbiologico").css("display","block");
		}
		else{
			$("#pbiologico").css("display","none");
		}
	});
	$("#txtTipoBen").bind("change", function(){
		var tb = $("#txtTipoBen").val();
		if(tb == 36 || tb == 37)
			$("#txtMadrePadre").attr("disabled","disabled");
		else
			$("#txtMadrePadre").removeAttr("disabled");
	});
	$("#txtIngreso").datepicker( {changeMonth: true,changeYear: true} );
	$("#txtFechaNaceBen").datepicker( {changeMonth: true,changeYear: true} );
	$("#txtFecAsig2").datepicker( {changeMonth: true,changeYear: true} );
	$("#txtFechaNaceConyuge").datepicker( {changeMonth: true,changeYear: true, minDate: "-60Y", maxDate: "-10Y"} );
	$("#txtFechaNaceBen2").datepicker( {changeMonth: true,changeYear: true} );
	$(".ui-datepicker").css("z-index",3000);
	
	$("#txtSalario").bind("change", calcularCategoria);
	$("#txtFechaNaceBen2").bind("change", function(){
		$("#txtEdadBen").val(calcular_edad($("#txtFechaNaceBen2").val()));
	});
	$("#estados").jCombo(URL+"pdo/estados.php",{selected_value:"48"});
	$("#estadosCony").jCombo(URL+"pdo/estados.php",{selected_value:"48"});
	$("#estadosBen").jCombo(URL+"pdo/estados.php",{selected_value:"48"});
	$("#estadosBen2").jCombo(URL+"pdo/estados.php",{selected_value:"48"});
	
	$("#deptos").jCombo(URL+"pdo/departamentos.php?id=", { 
		parent: "#estados", 
		selected_value: '41' 
	});	
	$("#txtDptoCony").jCombo(URL+"pdo/departamentos.php?id=", { 
		parent: "#estadosCony", 
		selected_value: '41' 
	});
	$("#txtDptoBen").jCombo(URL+"pdo/departamentos.php?id=", { 
		parent: "#estadosBen", 
		selected_value: '41' 
	});
	$("#txtDptoBen2").jCombo(URL+"pdo/departamentos.php?id=", { 
		parent: "#estadosBen2", 
		selected_value: '41' 
	});
	
	$("#ciudad").jCombo(URL+"pdo/municipios.php?id=", { 
		parent: "#deptos"
	});	
	$("#txtCiudadCony").jCombo(URL+"pdo/municipios.php?id=", { 
		parent: "#txtDptoCony"
	});
	$("#txtCiudadBen").jCombo(URL+"pdo/municipios.php?id=", { 
		parent: "#txtDptoBen"
	});
	$("#txtCiudadBen2").jCombo(URL+"pdo/municipios.php?id=", { 
		parent: "#txtDptoBen2"
	});
	
	$("#cboDepto").jCombo(URL+"pdo/dpto.php",{selected_value:"41"});
	
	$("#cboCiudad").jCombo(URL+"pdo/municipios.php?id=", { 
		parent: "#cboDepto"
	});	
	$("#cboZona").jCombo(URL+"pdo/zonas.php?id=", { 
		parent: "#cboCiudad"
	});	
	$("#cboBarrio").jCombo(URL+"pdo/barrios.php?id=", { 
		parent: "#cboZona"
	});	
	
	$("#txtFechaNace").datepicker( {
		changeMonth: true,
		changeYear: true,
		minDate: "-60Y",
		maxDate: "-16Y"
		
	} );
	$(".ui-datepicker").css("z-index",3000);
	
	$( "#tabs" ).tabs();
	$("#tabs #t2").bind("click",function(){
		if (controlPersona==0){
			var error = validarCampos();
			if(error > 0){
				alert("No puede continuar hasta no ACTUALIZAR los datos de la persona!");
				$("div#tabs ul li a[href=#tabs-1]").trigger('click');
			}
			else
				$("#tabs-2").load();
		}
		else 
			$("#tabs-2").load();
	});
	
// relaciones de convivencia

	$("#div-conyuge").dialog({
		autoOpen:false,
		width:740,
		show: "drop",
		hide: "clip",	
		open:function(event,ui){
			$("#tipoDoc2").focus();
			$("#tipoDoc2").val(1);
			$("#estadosCony").val(48).trigger('change');
			$("#txtDptoCony").val(41).trigger('change');
        	
			
	    },//fin funcion open
	    
		buttons: {
			'Grabar': function() {
				if(idConyuge==0)
					insertarPersona();
				else
					actualizarConyuge();
			}		  
		},//fin funcion boton grabar
		close:function(){
			$("#div-conyuge ul").remove();
			$("#div-conyuge table.tablero input:text").val('');
			$("#div-conyuge table.tablero select").val('Seleccione');
			$("table[name='dinamicTable']").remove();
		}//fin funcion close
		
	});//fin dialog
	
	$("#dialog-actualizar-bene").dialog({
		autoOpen:false,
		width:740,
		show: "drop",
		hide: "clip",	
		open:function(event,ui){
			$("#tipoDoc2").focus();
			$("#tipoDoc2").val(1);
			$("#estadosCony").val(48).trigger('change');
			$("#txtDptoCony").val(41).trigger('change');
        	
			
	    },//fin funcion open
	    
		buttons: {
			'Actualizar': function() {
				actualizarBeneficiario();
			}		  
		},//fin funcion boton grabar
		close:function(){
			$("#dialog-actualizar-bene ul").remove();
			$("#dialog-actualizar-bene table.tablero input:text").val('');
			$("#dialog-actualizar-bene table.tablero select").val('Seleccione');
			//$("table[name='dinamicTable']").remove();
		}//fin funcion close
		
	});//fin dialog	
	
	$("#dialog-actualizar-relacion").dialog({
		autoOpen:false,
		width:740,
		show: "drop",
		hide: "clip",	
		open:function(event,ui){
			$("#txtTipoBen").focus();	
	    },//fin funcion open
	    
		buttons: {
			'Actualizar': function() {
				actualizarRelacion();
			}		  
		},//fin funcion boton grabar
		close:function(){
			$("#dialog-actualizar-relacion ul").remove();
			$("#dialog-actualizar-relacion table.tablero input:text").val('');
			$("#dialog-actualizar-relacion table.tablero select").val('Seleccione');
			//$("table[name='dinamicTable']").remove();
		}//fin funcion close
		
	});
	
	$("#dialog-nuevo-beneficiario").dialog({
		autoOpen:false,
		width:740,
		show: "drop",
		hide: "clip",	
		open:function(event,ui){
			$("#numeroBen2").focus();
			$("#estadosBen2").val(48);
			$("#estadosBen2").trigger("change");
	    },//fin funcion open
	    
		buttons: {
			'Grabar': function() {
				guardarRelacion();
			}		  
		},//fin funcion boton grabar
		close:function(){
			$("#dialog-nuevo-beneficiario ul").remove();
			$("#dialog-nuevo-beneficiario table.tablero input:text").val('');
			$("#dialog-nuevo-beneficiario table.tablero select").val('Seleccione');
			$("#pbiologico2").css("display","none");
		}//fin funcion close
		
	});//fin dialog

	$(function(){
		$("#divMotivos").dialog({
			autoOpen:false,
			modal:true,
			width:480,
			height:120,
			resizable:false
		});
	});	
	
});//document ready

function actualizarConyuge(){
	
	var nRelaciones = new GrupoFamiliar();
	nRelaciones = camposGrupoFamiliar(nRelaciones);
	nRelaciones.idtrabajador=idPersona;
	nRelaciones.idbeneficiario=idConyuge;
	nRelaciones.idparentesco=34;
	nRelaciones.idconyuge=idConyuge;
	nRelaciones.conviven=$("#txtConviven").val(); 
	//nRelaciones.fechaafiliacion;
	nRelaciones.estado='A';
	nRelaciones.idtiporelacion=$("#txtRelacionCony").val();
	crearRelacion(nRelaciones,0);
}

function buscarRadicacion(){
	idr = $.trim( $("#txtRadicacion").val() );
	limpiarCampos(0);
	$("#txtRadicacion").val(idr);
	if(idr.length==0) return false;
	$.ajax({
        url: "buscarRadicacion.php",
        async:true,
        data:{v0:idr},
        beforeSend: function(objeto){
        	dialogLoading('show');
        },
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }
       
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("En buscarRadicacion pas� lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
        success: function(datos){
        	if(datos==0){
    			alert("La Radicacion no existe!");
    			
    			$("#txtRadicacion").val('');
    			$("#txtRadicacion").focus();
    			return false;
    			}	
        	$("#div-boton").css("display","block");
    		$("#txtFecha").val(datos.fecharadicacion);
			$("#copiaIdR").val(idr);
    		buscarAfiliacion();
    		
           },
        timeout: 30000,
        type: "GET"
 });
	
}

function buscarAfiliacion(){
	var idr = $.trim( $("#txtRadicacion").val() );
	if(idr.length==0) return false;
	$.ajax({
        url: "buscarAfiliacion.php",
        async:false,
        data:{v0:idr},
        beforeSend: function(objeto){
        	dialogLoading('show');
        },
        complete: function(objeto, exito){
        	dialogLoading('close');
        	
            if(exito != "success"){
                alert("No se completo el proceso!");
            }
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("En buscarAfiliacion pas� lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
        success: function(datos){
        	if(datos==0){
    			alert("NO hay una afiliacion asociado a esta radicacion!");
    			$("#div-boton2").css("display","none");
    			return false;
    			}	
        	$("#txtFormulario").val(datos.tipoformulario);
        	$("#txtAfiliacion").val(datos.tipoafiliacion);
        	$("#txtIngreso").val(datos.fechaingreso);
        	$("#txtSalario").val(datos.salario);
        	$("#txtAgencia").val(datos.idagencia);
        	$("#txtSalario").trigger("change");
        	$("#txtHorasDia").val(8);
        	$("#txtHorasMes").val(240);
			idPersona=datos.idpersona;
			idEmpresa=datos.idempresa;
			idFormulario = datos.idformulario;
        	buscarRegPersona(datos.idpersona,0,0);
        	buscarEmpresa(datos.idempresa);
        	$("#div-boton2").css("display","block");
        	afiliacionPrimaria();
        	buscarConvivenciasExisten();
        	buscarGrupoFamiliar();
           },
        timeout: 30000,
        type: "GET"
 });
	
} 

function buscarRegPersona(idp,idtd,num){
	$.ajax({
        url: "buscarRegPersona.php",
        async:false,
        data:{v0:idp,v1:idtd,v2:num},
        beforeSend: function(objeto){
        	dialogLoading('show');
        },
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("en buscarRegPersona pas� lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
        success: function(datos){
        	if(datos==0){
    			alert("NO existe informacion de la persona!");
    			$("#div-boton").css("display","none");
    			limpiarAfiliado();
    			return false;
    			}	
        	//$("#tdIdp").html(idp);    pendiente
        	nPersona=new Persona();
        	nPersona=datos;
        	idPersona=datos.idpersona;
        	$("#tipDocAfi").val(datos.idtipodocumento);
        	$("#txtNumero").val(datos.identificacion) ;
        	$("#txtPNombre").val(datos.pnombre);
        	$("#txtSNombre").val(datos.snombre);
        	$("#txtPApellido").val(datos.papellido);
        	$("#txtSApellido").val(datos.sapellido);
        	$("#cboCivil").val(datos.idestadocivil);
        	$("#txtFechaNace").val(datos.fechanacimiento);
        	$("#estados").val(datos.idpais);
        	$("#estados").trigger("change");
        	$("#deptos").val(datos.iddepnace);
        	$("#deptos").trigger("change");
        	setTimeout((function(){
        		$("#ciudad").val(datos.idciunace);
        	}),100);
        	$("#cboGenero").val(datos.sexo);
        	$("#txtDireccion").val(datos.direccion);
        	$("#cboDepto").val(datos.iddepresidencia);
        	$("#cboDepto").trigger("change");
        	setTimeout((function(){
        		$("#cboCiudad").val(datos.idciuresidencia);
        		$("#cboCiudad").trigger("change");
        	}),400);
        	
        	setTimeout((function(){
        		$("#cboZona").val($.trim(datos.idzona));
        		$("#cboZona").trigger("change");
        		
        	}),600);
        	setTimeout((function(){
        		$("#cboBarrio").val(datos.idbarrio);
        	}),800);
        	$("#cboPropiedad").val(datos.idpropiedadvivienda);
        	$("#txtTelefono").val(datos.telefono);
        	$("#txtCelular").val(datos.celular);
        	$("#txtCorreo").val(datos.email);
        	$("#txtProfesion").val(datos.idprofesion);
        	var nc=nombre_corto(datos.pnombre,datos.snombre,datos.papellido,datos.sapellido);
        	$("#txtNombreCorto").val(nc);
        	$("#txtMadrePadre option not first").remove();
			$("#txtMadrePadre").append("<option value=" + idPersona + ">Sin padre</option>");
        	$("#txtMadrePadre2 option not first").remove();
			$("#txtMadrePadre2").append("<option value=" + idPersona + ">Sin padre</option>");
           },
        timeout: 30000,
        type: "GET"
 });
	$("#tdNombre").html($("#txtPNombre").val() + ' ' +$("#txtSNombre").val() + ' ' +$("#txtPApellido").val() + ' ' +$("#txtSApellido").val()   );
	
} 

function buscarPersona(idp,idtd,num,pn,pa,op){
	$.ajax({
        url: "buscarPersona.php",
        async:true,
        data:{v0:idp,v1:idtd.val(),v2:num.val(),v3:pn,v4:pa,v5:op},
        beforeSend: function(objeto){
        	dialogLoading('show');
        },
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("En buscarPersona pas� lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
        success: function(datos){
        	if(datos==0){
    			if (confirm("NO existe informacion de la persona desea crear la PERSONA?") == true){
    				newPersonaSimple(idtd,num);
    			}
    			else{
    				if(formulario==1){
    					$("#tipoDocPB").val('');
    					$("#numeroPB").val('');
    				}
    				if(formulario==2){
    					
    				}
    			}
    			}	
        	if(formulario==1){
        		$("#txtpnombrepb").val(datos.pnombre);
        		$("#txtsnombrepb").val(datos.snombre);
        		$("#txtpapellidopb").val(datos.papellido);
        		$("#txtsapellidopb").val(datos.sapellido);
        		idPBiologico=datos.idpersona;
        	}
        	if(formulario==2){
        		
        	}
           },
        timeout: 30000,
        type: "GET"
 });
	
} 

function buscarConyuge(idp){
	var idtd = $("#tipoDoc2").val();
	var num = $("#identificacion2").val();
	
	$.ajax({
        url: "buscarConyuge.php",
        async:true,
        data:{v0:idtd,v1:num},
        beforeSend: function(objeto){
        	dialogLoading('show');
        },
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("En buscarConyuge pas� lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
        success: function(datos){
        	if(datos==0){
    			alert("NO existe informacion de la persona!");
    			idConyuge=0;
    			$("#pNombre2").val('');
            	$("#sNombre2").val('');
            	$("#pApellido2").val('');
            	$("#sApellido2").val('');
            	$("#txtGeneroCony").val(0);
            	$("#txtFechaNaceConyuge").val('');
            	$("#pNombre2").focus();
    			return false;
    			}	
        	$("#pNombre2").val(datos.pnombre);
        	$("#sNombre2").val(datos.snombre);
        	$("#pApellido2").val(datos.papellido);
        	$("#sApellido2").val(datos.sapellido);
        	$("#txtGeneroCony").val(datos.sexo);
        	$("#txtFechaNaceConyuge").datepicker("setDate",new Date(datos.fechanacimiento));
        	$("#txtDptoCony").val(datos.iddepnace).trigger('change');
        	setTimeout((function(){
        		$("#txtCiudadCony").val(datos.idciunace);
        	}),400);
        	idConyuge=datos.idpersona;
        	buscarConvivencia();
           },
        timeout: 30000,
        type: "GET"
 });
	
} 

function buscarConvivencia(){
	$.ajax({
		url: 'buscarConvivencia.php',    //rutaPHP + 'guardarAfiliacion.php',
		async: false,
		data: {v0:idConyuge},
		beforeSend: function(objeto){
			dialogLoading('show');
		},        
		complete: function(objeto, exito){
			dialogLoading('close');
			if(exito != "success"){
				alert("No se completo el proceso!");
			}            
		},
		contentType: "application/x-www-form-urlencoded",
		dataType: "json",
		error: function(objeto, quepaso, otroobj){
			alert("En buscarConvivencia pas� lo siguiente: "+quepaso);
		},
		global: true,
		ifModified: false,
		processData:true,
		success: function(datos){
			var conyo = 0;
			var conot = 0;
			if(datos==0){
				alert("La persona no tiene relaciones de convivencia!");
				return false;
			}
			$.each(datos, function(i,f){
				if (f.idtrabajar == idPersona){
					conyo=1;
				}
				if(f.conviven == 'S'){
					conot=1;
				}
			});
			if(conyo > 0){
				alert("Ya existe una relacion de Convivencia con el afiliado!");
				$( "#div-conyuge" ).dialog( "close" );
				return false;
			}
			if(conot > 0){
				$( "#div-conyuge" ).dialog( "close" );
				alert("Ya existe una relacion de Convivencia en estado A con otro Afiliado, para continuar disuelva primera la relacion existente!");
			}
		},
		timeout: 300000,
		type: "GET"
	});
}

function buscarConvivenciasExisten(){
	$.ajax({
		url: 'buscarConvivenciasExisten.php',    //rutaPHP + 'guardarAfiliacion.php',
		async: false,
		data: {v0:idPersona},
		beforeSend: function(objeto){
			dialogLoading('show');
		},        
		complete: function(objeto, exito){
			dialogLoading('close');
			if(exito != "success"){
				alert("No se completo el proceso!");
			}            
		},
		contentType: "application/x-www-form-urlencoded",
		dataType: "json",
		error: function(objeto, quepaso, otroobj){
			alert("En buscarConvivenciasExisten pas� lo siguiente: "+quepaso);
		},
		global: true,
		ifModified: false,
		processData:true,
		success: function(datos){
			if(datos==0){
				alert("La persona no tiene relaciones de convivencia!");
				return false;
			}
			$("#tabTableRelaciones tbody tr").remove();
			$("#txtMadrePadre option not first").remove();
			$("#txtMadrePadre").append("<option value=" + idPersona + ">Sin padre</option>");
			$("#txtMadrePadre2 option not first").remove();
			$("#txtMadrePadre2").append("<option value=" + idPersona + ">Sin padre</option>");
			SI=0;
			$.each(datos, function(i,fila){

				idr=fila.idrelacion;
				idc=fila.idconyuge; 
				numC=fila.identificacion;	
				nombreC=$.trim(fila.pnombre)+" "+$.trim(fila.snombre)+" "+$.trim(fila.papellido)+" "+$.trim(fila.sapellido);
				conviven=fila.conviven;
				//Insertamos las convivencias en la tablas
				if(conviven=='S'){
					SI=1;
					par=idr+",0";}
				else
					par=idr+",1";
				$("#tabTableRelaciones tbody").append("<tr><td>"+idr+"</td><td>"+numC+"</td><td style=text-align:center><label style=cursor:pointer onclick=disolverConvivencia("+par+","+idc+");>"+conviven+"</label></td><td>"+nombreC+"</td></tr>");
				$("#txtMadrePadre").append("<option value=" + fila.idpersona + ">" + fila.identificacion + "</option>");
				$("#txtMadrePadre2").append("<option value=" + fila.idpersona + ">" + fila.identificacion + "</option>");
			});
		},
		timeout: 300000,
		type: "GET"
	});
	
}

function disolverConvivencia(idr,tipo,idc){
	if(tipo==0){
		if(confirm("Esta seguro de disolver la CONVIVENCIA?")==true){
			disolver(idr);
		}
	}
	else{}

}

function disolver(idr){
	$.ajax({
        url: "disolverConvivencia.php",
        async:true,
        data:{v0:idr},
        beforeSend: function(objeto){
        	dialogLoading('show');
        },
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("En disolver pas� lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
        success: function(datos){
        	if(datos==0){
    			alert("NO se pudo disolver las relaciones!");
    			return false;
    			}	
        	alert("Las relaciones de convivencia fueron disueltas!");
        	buscarConvivenciasExisten();
           },
        timeout: 30000,
        type: "GET"
 });
}

function buscarEmpresa(ide){
	$.ajax({
        url: "buscarEmpresa.php",
        async:true,
        data:{v0:0,v1:ide,V2:2},
        beforeSend: function(objeto){
        	dialogLoading('show');
        },
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("En buscarEmpresa pas� lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
        success: function(datos){
        	if(datos==0){
    			alert("NO existe informacion de la empresa!");
    			$("#div-boton").css("display","none");
    			return false;
    			}	
        	$("#txtNit").val(datos.nit);
        	$("#txtRazon").val(ide + ' - ' + datos.razonsocial);
           },
        timeout: 30000,
        type: "GET"
 });
	
} 

function calcularCategoria(){
    var salario=parseInt($("#txtSalario").val());
    if(isNaN(salario)) return false;
    var sm=parseFloat(salario / SMLV);
    if(sm<=2){
        $('#txtCategoria').val('A');
    }
    if(sm>2 && sm<=4){
        $('#txtCategoria').val('B');
    }
    if(sm>4){
        $('#txtCategoria').val('C');
    }
}

function actualizarPersona(){
	if(validarCampos() > 0) return false;
	
	nPersona.idtipodocumento=$("#tipDocAfi").val();
	nPersona.identificacion=$("#txtNumero").val();
	nPersona.papellido=$("#txtPApellido").val();
	nPersona.sapellido=$("#txtSApellido").val();
	nPersona.pnombre=$("#txtPNombre").val();
	nPersona.snombre=$("#txtSNombre").val();
	nPersona.sexo=$("#cboGenero").val();
	nPersona.direccion=$("#txtDireccion").val();
	nPersona.idbarrio=$("#cboBarrio").val();
	nPersona.telefono=$("#txtTelefono").val();
	nPersona.celular=$("#txtCelular").val();
	nPersona.email=$("#txtCorreo").val();
	nPersona.idpropiedadvivienda=$("#cboPropiedad").val();
	nPersona.idciuresidencia=$("#cboCiudad").val();
	nPersona.iddepresidencia=$("#cboDepto").val();
	nPersona.idzona=$("#cboZona").val();
	nPersona.idestadocivil=$("#cboCivil").val();
	nPersona.fechanacimiento=$("#txtFechaNace").val();
	nPersona.idciunace=$("#ciudad").val();
	nPersona.iddepnace=$("#deptos").val();
	nPersona.idprofesion=$("#cboProfesion").val();
	nPersona.nombrecorto=$("#txtNombreCorto").val();
	nPersona.idpais=$("#estados").val();
	var cadena ='';
	$.each(nPersona, function(i,j){
		cadena += i + '=' + j +',';
	});
	str = JSON.stringify(nPersona);
	//var str = jQuery.param(nPersona);
	//var array = $(nPersona).serializeArray();
	
		$.ajax({
		url: 'guardarPersonaCompleta.php',    //rutaPHP + 'guardarAfiliacion.php',
		async: false,
		data: {v0:str},
		beforeSend: function(objeto){
        	dialogLoading('show');
        },        
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }            
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("En actualizarPersona pas� lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
		success: function(datos){
	  		if(datos==1){
	  			alert("Persona actualizada!");
	  			controlPersona=1;
	  			$( "#tabs" ).tabs("select",1);
	  			$("#txtNit").focus();
	  		}
	  		else
	  			alert("La Persona NO se pudo actualizar!");
	  			
		},
		timeout: 300000,
        type: "GET"
	});
}

function actualizarAfiliacion(){
	var error=0;
	$("table.tablero :input.ui-state-error,select.ui-state-error").removeClass("ui-state-error");
	error=validarTexto($("#txtNit"),error);
	error=validarSelect($("#txtFormulario"),error);
	error=validarSelect($("#txtAfiliacion"),error);
	error=validarSelect($("#txtAgricola"),error);
	error=validarSelect($("#txtEstado"),error);
	error=validarSelect($("#txtAgencia"),error);
	error=validarSelect($("#txtTipoPago"),error);
	error=validarSelect($("#txtCargo"),error);
	error=validarSelect($("#txtMComunitaria"),error);
	error=validarTexto($("#txtIngreso"),error);
	error=validarTexto($("#txtSalario"),error);
	error=validarTexto($("#txtCategoria"),error);
	
	if(error > 0) return false;
	
	var c0 = idFormulario;
	var c1 = $("#txtFormulario").val();
	var c2 = $("#txtAfiliacion").val();
	var c3 = idEmpresa;
	var c4 = idPersona;
	var c5 =  $("#txtIngreso").val();
	var c6 =  $("#txtHorasDia").val();
	var c7 =  $("#txtHorasMes").val();
	var c8 = $("#txtSalario").val();	
	var c9 = $("#txtAgricola").val();
	var c10 = $("#txtCargo").val();
	var c11 = $("#txtTipo").val();	
	var c12 = $("#txtEstado").val();
	var c13 = $("#txtTipoPago").val();
	var c14 = $("#txtCategoria").val();	
	var c15 = $("#txtAgencia").val();
	var c16 = $("#txtMComunitaria").val();
	
	
	$.ajax({
	    url: "guardarAfiliacion.php",
	    async:true,
	    data:{v0:c0,v1:c1,v2:c2,v3:c3,v4:c4,v5:c5,v6:c6,v7:c7,v8:c8,v9:c9,v10:c10,v11:c11,v12:c12,v13:c13,v14:c14,v15:c15,v16:c16},
	    beforeSend: function(objeto){
	    	dialogLoading('show');
	    },
	    complete: function(objeto, exito){
	    	dialogLoading('close');
	    	
	        if(exito != "success"){
	            alert("No se completo el proceso!");
	        }
	    },
	    contentType: "application/x-www-form-urlencoded",
	    dataType: "json",
	    error: function(objeto, quepaso, otroobj){
	        alert("en actualizarAfiliacion pas� lo siguiente: "+quepaso);
	    },
	    global: true,
	    ifModified: false,
	    processData:true,
	    success: function(datos){
	    	if(datos==0){
	    		alert("No se pudo actulizar la Afiliacion!"); 		
	    	}
	    	else{
	    		alert("Afiliacion Actualizada!");  
	    		controlAfiliacion=1;
	    		$( "#tabs" ).tabs("select",2);
	    	}
	       },
	    timeout: 30000,
	    type: "GET"
	});
}

function validarCampos(){
	var error=0;
	$("table.tablero :input.ui-state-error,select.ui-state-error").removeClass("ui-state-error");
	error=validarSelect($("#tipDocAfi"),error);
	error=validarTexto($("#txtNumero"),error);
	error=validarTexto($("#txtPApellido"),error);
	error=validarTexto($("#txtPNombre"),error);
	error=validarTexto($("#txtFechaNace"),error);
	error=validarTexto($("#txtDireccion"),error);
	error=validarTexto($("#txtTelefono"),error);
	error=validarTexto($("#txtNombreCorto"),error);
	error=validarSelect($("#cboCivil"),error);
	error=validarSelect($("#estados"),error);
	error=validarSelect($("#deptos"),error);
	error=validarSelect($("#ciudad"),error);
	error=validarSelect($("#cboGenero"),error);
	error=validarSelect($("#cboDepto"),error);
	error=validarSelect($("#cboCiudad"),error);
	error=validarSelect($("#cboZona"),error);
	error=validarSelect($("#cboPropiedad"),error);
	error=validarSelect($("#cboProfesion"),error);
	
	return error;
}

function buscarNuevaEmpresa(){
	var nit =$("#txtNit").val();
	
	$.ajax({
        url: "buscarEmpresa.php",
        async:true,
        data:{v0:nit,v1:0,V2:1},
        beforeSend: function(objeto){
        	dialogLoading('show');
        },
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("En buscarNuevaEmpresa pas� lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
        success: function(datos){
        	if(datos==0){
    			alert("NO existe informacion de la empresa!");
    			$("#div-boton").css("display","none");
    			return false;
    			}	
        	idEmpresa=datos.idempresa;
        	$("#txtRazon").val(idEmpresa + ' - ' + datos.razonsocial);
           },
        timeout: 30000,
        type: "GET"
 });
}

function afiliacionPrimaria(){
	if(idPersona==0) return false;
	$.ajax({
	    url: "afiliacionPrimaria.php",
	    async:true,
	    data:{v0:idr},
	    beforeSend: function(objeto){
	    	dialogLoading('show');
	    },
	    complete: function(objeto, exito){
	    	dialogLoading('close');
	    	
	        if(exito != "success"){
	            alert("No se completo el proceso!");
	        }
	    },
	    contentType: "application/x-www-form-urlencoded",
	    dataType: "json",
	    error: function(objeto, quepaso, otroobj){
	        alert("En afiliacionPrimaria pas� lo siguiente: "+quepaso);
	    },
	    global: true,
	    ifModified: false,
	    processData:true,
	    success: function(datos){
	    	if(datos==0){
	    		$("#txtTipo").val('S');	    		
	    	}
	    	else{
	    		$("#txtTipo").val('N');	    
	    	}
	       },
	    timeout: 30000,
	    type: "GET"
	});
}

function insertarPersona(){
	var error=0;
	$("table.tablero :input.ui-state-error,select.ui-state-error").removeClass("ui-state-error");
	error=validarSelect($("#tipoDoc2"),error);
	error=validarTexto($("#identificacion2"),error);
	error=validarTexto($("#pApellido2"),error);
	error=validarTexto($("#pNombre2"),error);
	error=validarTexto($("#txtFechaNaceConyuge"),error);
	error=validarSelect($("#txtGeneroCony"),error);
	error=validarSelect($("#txtRelacionCony"),error);
	error=validarSelect($("#txtConviven"),error);
	if(error > 0) return false;
	
	var nPersona = new Persona();
	nPersona=camposPersona(nPersona);
	
	nPersona.idtipodocumento=$("#tipoDoc2").val();
	nPersona.identificacion=$("#identificacion2").val();
	nPersona.papellido=$("#pApellido2").val();
	nPersona.sapellido=$("#sApellido2").val();
	nPersona.pnombre=$("#pNombre2").val();
	nPersona.snombre=$("#sNombre2").val();
	nPersona.sexo=$("#txtGeneroCony").val();
	nPersona.fechanacimiento=$("#txtFechaNaceConyuge").val();
	
	str = JSON.stringify(nPersona);
	$.ajax({
		url: 'insertPersona.php',    //rutaPHP + 'guardarAfiliacion.php',
		async: false,
		data: {v0:str},
		beforeSend: function(objeto){
			dialogLoading('show');
		},        
		complete: function(objeto, exito){
			dialogLoading('close');
			if(exito != "success"){
				alert("No se completo el proceso!");
			}            
		},
		contentType: "application/x-www-form-urlencoded",
		dataType: "json",
		error: function(objeto, quepaso, otroobj){
			alert("En insertPersona pas� lo siguiente: "+quepaso);
		},
		global: true,
		ifModified: false,
		processData:true,
		success: function(datos){
			if(datos > 0){
				$( "#div-conyuge" ).dialog( "close" );
				idConyuge=datos;
				var nRelaciones = new GrupoFamiliar();
				nRelaciones = camposGrupoFamiliar(nRelaciones);
				nRelaciones.idtrabajador=idPersona;
				nRelaciones.idbeneficiario=idConyuge;
				nRelaciones.idparentesco=34;
				nRelaciones.idconyuge=idConyuge;
				nRelaciones.conviven=$("#txtConviven").val(); 
				//nRelaciones.fechaafiliacion;
				nRelaciones.estado='A';
				nRelaciones.idtiporelacion=$("#txtRelacionCony").val();
				crearRelacion(nRelaciones,0);
				alert("Persona Guardada!");
			}
			else
				alert("La Persona NO se pudo guardar!");

		},
		timeout: 300000,
		type: "GET"
	});
}

function nuevaRelacion(){
	$( "#div-conyuge" ).dialog( "open" );
}

function nuevoBeneficiario(){
	$( "#dialog-nuevo-beneficiario" ).dialog( "open" );
}

function crearRelacion(nRel,flag){
	if(flag == 0){
	if($("#txtConviven").val() == 'S')
		if(SI==1){
			alert("Ya existe una relacion de convivencia en S");
			return false;
		}
	}
	str = JSON.stringify(nRel);
	$.ajax({
		url: 'crearRelacion.php',    //rutaPHP + 'guardarAfiliacion.php',
		async: false,
		data: {v0:str},
		beforeSend: function(objeto){
			dialogLoading('show');
		},        
		complete: function(objeto, exito){
			dialogLoading('close');
			if(exito != "success"){
				alert("No se completo el proceso!");
			}            
		},
		contentType: "application/x-www-form-urlencoded",
		dataType: "json",
		error: function(objeto, quepaso, otroobj){
			alert("En crearRelacion pas� lo siguiente: "+quepaso);
		},
		global: true,
		ifModified: false,
		processData:true,
		success: function(datos){
			if(datos==0){
				alert("La relacion no Fue Creada!");
				return false;
			}
			if(flag==0){
				buscarConvivenciasExisten();
				$("#div-conyuge").dialog('close');
				alert ("La relacion fue Creada!");
			}
			else{
				buscarGrupoFamiliar();
				$( "#dialog-nuevo-beneficiario" ).dialog( "close" );
				alert("La relacion fue Creada");
			}
		},
		timeout: 300000,
		type: "GET"
	});
}

function buscarGrupoFamiliar(){
	$.ajax({
        url: "buscaGrupoFamiliar.php",
        async:true,
        data:{v0:idPersona},
        beforeSend: function(objeto){
        	dialogLoading('show');
        },
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("En buscarGrupoFamiliar pas� lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
        success: function(datos){
        	if(datos==0){
    			//alert("NO existe GRUPO FAMILIAR de la persona!");
    			return false;
    			}	
        	$("#tabTableGrupo tbody tr").remove();
        	$.each(datos,function(i,f){
        		var idrel = f.idrelacion;
        		var idb = f.idbeneficiario;
        		var td  = f.codigo;
        		var num = f.identificacionBeneficiario;
        		var nomb = f.pnombre +' '+f.snombre+' '+f.papellido+' '+f.sapellido;
        		var parentesco = f.parentesco;
        		var rela=f.identificacionConyuge;
        		var fecnace = f.fechanacimiento;
        		var edad = f.edad;
        		var embargo = f.embarga;
        		var discapacidad = f.discapacitado;
        		var giro = f.giro;
        		var fechaasignacion = f.fechaasignacion;
        		var estadoAfiliacion = f.estadoAfiliacion;
        		var fechaafiliacion = f.fechaafiliacion;
        		var fechapresentacion = f.fechapresentacion;
        		$("#tabTableGrupo tbody").append("<tr><td><b><label style='cursor:pointer;text-decoration:none;' onclick=(buscarBeneficiario("+idb+"))>"+idb+"</label></b></td>" +
        		"<td>"+td+"</td><td>"+num+"</td><td>"+nomb+"</td><td>"+parentesco+"</td><td>"+rela+"</td><td>"+fecnace+"</td><td>"+edad+"</td>" +
        		"<td style=text-align:center>"+embargo+"</td><td style=text-align:center>"+discapacidad+"</td>" +
        		"<td id='g_"+idb+"' style='text-align:center'><label style=cursor:pointer onclick=actualizarEstadoGiroBeneficiario("+idrel+","+idb+",'"+giro+"');>"+giro+"</label></td>" +
        		"<td>"+fechaasignacion+"</td><td><label style=cursor:pointer onclick=actualizarEstadoBeneficiario("+idrel+","+idb+",'"+f.idparentesco+"','"+estadoAfiliacion+"');>"+estadoAfiliacion+"</label></td><td>"+fechaafiliacion+"</td><td>"+fechapresentacion+"</td>" +
        		"<td style='text-align:center'><label style='cursor:pointer' onclick=buscarRelacion("+idrel+") ><img  src='"+URL+"imagenes/menu/modificar.png' title='Modificar Relacion' /></label></td></tr>");
        		
        		//$("#txtMadrePadre option").remove();
        		
        		//<td style=text-align:center><a class='dialogoEmergente' >"+ced_cony+"<span> "+span+" </span></a></td>
        		//<td style=text-align:center>"+fecnac+"</td><td style=text-align:center>"+edad+"</td><td>"+fasig+"</td><td id='e_"+idb+"' style='text-align:center'><label style=cursor:pointer onclick=actualizarEstadoBeneficiario("+idrel+","+idb+","+idparent+",'"+estado+"');>"+estado+"</label></td><td style=text-align:center>"+fecafi+"</td><td><a style='cursor: pointer' onclick='mostrarDatosCertificados("+idb+");'>"+fechaCertificado+"</a></td><td style='text-align:center'><label style='display:none'>"+idrel+"</label><img style='cursor:pointer' src='"+URL+"imagenes/menu/modificar.png' id='actBen"+idb+"' name='actBen' title='Modificar Beneficiario' /></td></tr>");
        	});
           },
        timeout: 30000,
        type: "GET"
 });
} 

function actualizarEstadoGiro(idrel,giro){
	
}

function buscarBeneficiario(idb){
	idBeneficiario=idb
	$("#dialog-actualizar-bene").dialog('open');
	$.ajax({
        url: "buscarRegPersona.php",
        async:false,
        data:{v0:idb,v1:0,v2:0},
        beforeSend: function(objeto){
        	dialogLoading('show');
        },
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("En buscarBeneficiario pas� lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
        success: function(datos){
        	if(datos==0){
    			alert("NO existe informacion de la persona!");
    			return false;
    			}	
        	$("#tipoDocBen").val(datos.idtipodocumento);
        	$("#numeroBen").val(datos.identificacion);
        	$("#pNombreBen").val(datos.pnombre);
        	$("#sNombreBen").val(datos.snombre);
        	$("#pApellidoBen").val(datos.papellido);
        	$("#sApellidoBen").val(datos.sapellido);
        	$("#txtGeneroBen").val(datos.sexo);
        	$("#txtFechaNaceBen").datepicker("setDate",new Date(datos.fechanacimiento));
        	$("#estadosBen").val(datos.idpais);
        	$("#estadosBen").trigger("change");
        	setTimeout((function(){
        		$("#txtDptoBen").val(datos.iddepnace);
        		$("#txtDptoBen").trigger("change");
        	}),100);
        	setTimeout((function(){
        		$("#txtCiudadBen").val($.trim(datos.idciunace));
        	}),300);
        	
           },
        timeout: 30000,
        type: "GET"
 });
}

function actualizarBeneficiario(idb){
	var error=0;
	$("table.tablero :input.ui-state-error,select.ui-state-error").removeClass("ui-state-error");
	error=validarSelect($("#tipoDocBen"),error);
	error=validarTexto($("#numeroBen"),error);
	error=validarTexto($("#pNombreBen"),error);
	error=validarTexto($("#pApellidoBen"),error);
	error=validarTexto($("#txtFechaNaceBen"),error);
	error=validarSelect($("#txtGeneroBen"),error);
	
	if(error > 0) return false;
	
	var tdb =$("#tipoDocBen").val();
	var numb =$("#numeroBen").val();
	var pnb =$("#pNombreBen").val();
	var snb =$("#sNombreBen").val();
	var pab =$("#pApellidoBen").val();
	var sab =$("#sApellidoBen").val();
	var fnb =$("#txtFechaNaceBen").val();
	var gb =$("#txtGeneroBen").val();
	var pb =$("#estadosBen").val();
	var db =$("#txtDptoBen").val();
	var mb =$("#txtCiudadBen").val();
	
	$.ajax({
		url: 'guardarBeneficiario.php',    //rutaPHP + 'guardarAfiliacion.php',
		async: false,
		data: {v0:idBeneficiario,v1:tdb,v2:numb,v3:pnb,v4:snb,v5:pab,v6:sab,v7:gb,v8:fnb,v9:pb,v10:db,v11:mb},
		beforeSend: function(objeto){
			dialogLoading('show');
		},        
		complete: function(objeto, exito){
			dialogLoading('close');
			if(exito != "success"){
				alert("No se completo el proceso!");
			}            
		},
		contentType: "application/x-www-form-urlencoded",
		dataType: "json",
		error: function(objeto, quepaso, otroobj){
			alert("En actualizarBeneficiario pas� lo siguiente: "+quepaso);
		},
		global: true,
		ifModified: false,
		processData:true,
		success: function(datos){
			if(datos > 0){
				alert("Datos del beneficiario actualizados!");
				$("#dialog-actualizar-bene").dialog('close');
				buscarGrupoFamiliar();
			}
			else
				{
				alert("No se pudo actualizar el Beneficiario! " + datos);
				}

		},
		timeout: 300000,
		type: "GET"
	});
}


function buscarRelacion(idrel){
	idRelacion=idrel;
	$("#dialog-actualizar-relacion").dialog('open');

	$.ajax({
		url: 'buscarRelacion.php',    //rutaPHP + 'guardarAfiliacion.php',
		async: false,
		data: {v0:idrel},
		beforeSend: function(objeto){
			dialogLoading('show');
		},        
		complete: function(objeto, exito){
			dialogLoading('close');
			if(exito != "success"){
				alert("No se completo el proceso!");
			}            
		},
		contentType: "application/x-www-form-urlencoded",
		dataType: "json",
		error: function(objeto, quepaso, otroobj){
			alert("En buscarRelacion pas� lo siguiente: "+quepaso);
		},
		global: true,
		ifModified: false,
		processData:true,
		success: function(datos){
			if(datos == 0){
				$( "#dialog-actualizar-relacion" ).dialog( "close" );
				return false;
			}
			
			var nombre = datos.pnombre + ' ' + datos.snombre + ' ' + datos.papellido + ' ' + datos.sapellido;
			$("#tdBeneficiario").html(nombre);
			$("#tdFechaNace").html(datos.fechanacimiento);
			fechaNaceBen=datos.fechanacimiento;
			$("#txtTipoForBene").val(datos.giro);
			$("#txtFecAsig").val(datos.fechaasignacion);
			$("#txtTipoBen").val(datos.idparentesco);
			if(datos.idparentesco==38)
				$("#pbiologico").css("display", "block");
			else
				$("#pbiologico").css("display", "none");
			if(datos.idparentesco == 36 || datos.idparentesco == 37){
				$("#txtMadrePadre").attr("disabled","disabled");
			}else
				$("#txtMadrePadre").removeAttr("disabled");
		},
		timeout: 300000,
		type: "GET"
	});
}


function actualizarEstadoBeneficiario(idr,idbenef,parentesco,estadoAnt){
	if(confirm("Esta seguro de cambiar el ESTADO del BENEFICIARIO?")==true){
		$('#hideIdRel').val('');				
		var actualizar=false;
		var estado='I';		
		
		if(estadoAnt=="I"){
			estado='A';
			var max=0;
			if(parentesco=='35' || parentesco=='38'){ max=2; 				
 			} else if(parentesco == '36' || parentesco == '37'){ max=1; }
 			else { alert("Existe un error con la identificacion del parentesco del beneficiario"); return; }

			actualizar=contarRelacionesBeneficiarioActivas(idbenef,max);				
		} else { actualizar=true; }
		
		if(actualizar){
			if(estado=='A'){
				updateEstadoBeneficiario(idr,estado,0);
			} else {				
				$('#hideIdRel').val(idr);
				$("#divMotivos").dialog('open');
			}
		} else{
			alert("NO se puede actualizar el estado, \nEl Beneficiario ya se encuentra afiliado con el maximo numero de trabajadores!");
		}
	} 
}

function updateEstadoBeneficiario(idr,est,idmot){
	var idper=idPersona;
	if(est=='I' && idmot==0){
		alert('Seleccione un motivo');
		return;
	} else if(est=='I') $("#divMotivos").dialog('close');
		
	$.ajax({
		url: URL+'phpComunes/actualizarEstadoBeneficiario.php',
		type:'POST',
		dataType:'json',
		async:false,
		data:{v0:idr,v1:est,v2:idmot},
		success:function(dataActualizacion){
			if(esNumeroRespuesta(dataActualizacion)){
				alert('Actualizado correctamente');
				buscarGrupoFamiliar();
				observacionesTab(idper,1); //Falta agregar el div
			} else {
				alert('Ocurrio un error actualizando al beneficiario');
			}					
		}
	});
}

function actualizarEstadoGiroBeneficiario(idr,idbenef,giroAnt){
	if(confirm("Esta seguro de cambiar la bandera de GIRO del BENEFICIARIO?")==true){
		var idper=idPersona;		
		var estado='N';
		
		if(giroAnt=='S'){
			estado='N';
		} else {
			estado='S';
		}
		
		$.ajax({
			url: URL+'phpComunes/actualizarEstadoGiroBeneficiario.php',
			type:'POST',
			dataType:'json',
			async:false,
			data:{v0:idr,v1:estado},
			success:function(dataActualizacion){
				if(esNumeroRespuesta(dataActualizacion)){
					alert('Actualizado correctamente');
					buscarGrupoFamiliar();
					observacionesTab(idper,1);	//Falta agregar el div
				} else {
					alert('Ocurrio un error actualizando al beneficiario');
				}					
			}
		});		
	}
}

function contarRelacionesBeneficiarioActivas(idbenef,max){
	var retorno=false;
	$.ajax({
		url: URL+'phpComunes/buscarRelacionesActivasBeneficiarioHijoHijastro.php',
		type: "POST",
		dataType:"json",
		async:false,
		data: {v0:idbenef},
		success: function(dataRelacion){
			if(!isNaN(dataRelacion) && parseInt(dataRelacion)<max){							
				retorno=true;				
			} 
		}
	});
	return retorno;
}

function actualizarRelacion(){
	
	var error=0;
	$("table.tablero :input.ui-state-error,select.ui-state-error").removeClass("ui-state-error");
	var tb =$("#txtTipoBen").val();
	if(tb == 35){
		error=validarSelect($("#txtMadrePadre"),error);
		}
	
	if(tb==38){
		error=validarSelect($("#tipoDocPB"),error);
		error=validarTexto($("#numeroPB"),error);
		error=validarTexto($("#txtpnombrepb"),error);
		error=validarTexto($("#txtpapellidopb"),error);
		error=validarSelect($("#txtMadrePadre"),error);
	}
	
	if(error > 0) return false;
	
	var txtTipoBen = $("#txtTipoBen").val();
	var txtMadrePadre = $("#txtMadrePadre").val();
	var txtTipoForBene = $("#txtTipoForBene").val();
	var txtFecAsig = $("#txtFecAsig").val();
	var tipoDocPB = $("#tipoDocPB").val();
	var numeroPB = $("#numeroPB").val();
	var txtpnombrepb = $("#txtpnombrepb").val();
	var txtsnombrepb = $("#txtsnombrepb").val();
	var txtpapellidopb = $("#txtpapellidopb").val();
	var txtsapellidopb = $("3txtsapellidopb").val();
	
	
	$.ajax({
		url: 'guardarRelacion.php',    //rutaPHP + 'guardarAfiliacion.php',
		async: false,
		data: {v0:idBeneficiario,v1:tdb,v2:numb,v3:pnb,v4:snb,v5:pab,v6:sab,v7:gb,v8:fnb,v9:pb,v10:db,v11:mb},
		beforeSend: function(objeto){
			dialogLoading('show');
		},        
		complete: function(objeto, exito){
			dialogLoading('close');
			if(exito != "success"){
				alert("No se completo el proceso!");
			}            
		},
		contentType: "application/x-www-form-urlencoded",
		dataType: "json",
		error: function(objeto, quepaso, otroobj){
			alert("En actualizarBeneficiario pas� lo siguiente: "+quepaso);
		},
		global: true,
		ifModified: false,
		processData:true,
		success: function(datos){
			if(datos > 0){
				alert("Datos del beneficiario actualizados!");
				$("#dialog-actualizar-bene").dialog('close');
				buscarGrupoFamiliar();
			}
			else
				{
				alert("No se pudo actualizar el Beneficiario! " + datos);
				}
		},
		timeout: 300000,
		type: "GET"
	});
} 

function buscarRegPersonaBen(idp,idtd,num){
	$.ajax({
        url: "buscarRegPersona.php",
        async:false,
        data:{v0:idp,v1:idtd,v2:num},
        beforeSend: function(objeto){
        	dialogLoading('show');
        },
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("en buscarRegPersonaBen pas� lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
        success: function(datos){
        	if(datos==0){
    			alert("NO existe informacion de la persona!");
    			limpiarBeneficiario();
    			existePersona=0;
    			return false;
    			}	
        	//$("#tdIdp").html(idp);    pendiente
        	nPersona=new Persona();
        	nPersona=datos;
        	idBeneficiario=datos.idpersona;
        	$("#pNombreBen2").val(datos.pnombre);
        	$("#sNombreBen2").val(datos.snombre);
        	$("#pApellidoBen2").val(datos.papellido);
        	$("#sApellidoBen2").val(datos.sapellido);
        	$("#txtFechaNaceBen2").val(datos.fechanacimiento);
        	$("#estadosBen2").val(datos.idpais);
        	$("#estadosBen2").trigger("change");
        	$("#txtDptoBen2").val(datos.iddepnace);
        	$("#txtEdadBen").val(datos.edad);
        	$("#txtDptoBen2").trigger("change");
        	setTimeout((function(){
        		$("#txtCiudadBen2").val(datos.idciunace);
        	}),100);
        	$("#txtGeneroBen2").val(datos.sexo);
        	existePersona=1;
           },
        timeout: 30000,
        type: "GET"
 });
	
} 

function contarAfiliaciones(){
	var idtd = $("#tipoDocBen2").val();
	var num = $.trim($("#numeroBen2").val());
	if(num.length==0) return false;
	$.ajax({
        url: "contarAfiliaciones.php",
        async:true,
        data:{v0:idtd,v1:num},
        beforeSend: function(objeto){
        	dialogLoading('show');
        },
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            } 	
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("En contarAfiliaciones pas� lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
        success: function(datos){
        	if(datos > 0){
    			alert("La persona que trata de afiliar como BENEFICIARIO tiene una afiliacion ACTIVA como afiliado!");
    			$("#tipoDocBen2").val('');
    			$("#numeroBen2").val('');
    			return false;
    			}
        	buscarRegPersonaBen(0,$("#tipoDocBen2").val(),$("#numeroBen2").val());
           },
        timeout: 30000,
        type: "GET"
 });
}

function buscarRelacionesBen(){
	var tb = $("#txtTipoBen2").val();
	if(idBeneficiario==0) return false;
	$.ajax({
        url: "buscarAfiliacionesBen.php",
        async:true,
        data:{v0:idBeneficiario,v1:tb},
        beforeSend: function(objeto){
        	dialogLoading('show');
        },
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("En buscarRelacionesBen pas� lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
        success: function(datos){
        	if(datos > 0){
    			if( tb==35 ){ //hijo
    				if(datos >= 2){
    					alert("La persona tiene DOS afiliacones como beneficiario-hijo no puede tener mas afiliaciones como beneficiario!");
    					$( "#dialog-nuevo-beneficiario" ).dialog( "close" );
    					return false;
    				}
    			}
    			if(tb == 36){//padre
    				if(datos >= 1){
    					alert("La persona tiene UNA afiliacones como beneficiario-Padre, no puede tener mas afiliaciones como beneficiario!");
    					$( "#dialog-nuevo-beneficiario" ).dialog( "close" );
    					return false;
    				}
    			}
    			if(tb == 37){//hermano
    				if(datos >= 1){
    					alert("La persona tiene UNA afiliacones como beneficiario-Hijo-hermano, no puede tener mas afiliaciones como beneficiario!");
    					$( "#dialog-nuevo-beneficiario" ).dialog( "close" );
    					return false;
    				}
    			}
    			if(tb == 38){//hijastro
    				if(datos >= 2){
    					alert("La persona tiene UNA afiliacones como beneficiario-Hijo-Hijastro, no puede tener mas afiliaciones como beneficiario!");
    					$( "#dialog-nuevo-beneficiario" ).dialog( "close" );
    					return false;
    				}
    			}
    			}
	
           },
        timeout: 30000,
        type: "GET"
 });
}

function guardarRelacion(){
	var tb = parseInt($("#txtTipoBen2").val());
	var edad = parseInt($("#txtEdadBen").val());
	var dis = parseInt($("input[name=rDiscapacidad]:checked").val());
	var td = parseInt($("#tipoDocBen2").val());
	if(tb == 35 || tb == 38 || tb == 37 ){
		if (edad > 18 && dis==0){
			alert("Hijos - Hijastros - Hermanos, mayores de 18 NO discapacitados no se pueden agregar al grupo familiar!");
			return false;
		}
		if (edad > 7 && edad < 18 && td != 2 ){
			alert("El tipo de documento del beneficiario debe ser TARJETA DE IDENTIDAD!");
			return false;
		}
		if (edad < 8 && td !=6){
			alert("El tipo de documento del beneficiario debe ser REGISTRO CIVIL!");
			return false;
		}
	}
	if (tb == 36 && edad < 60 && dis == 0){
		alert ("Los padres deben ser mayores de 60 a�os para ser beneficiarios!");
		return false;
	}
	if(existePersona==0){
		insertarBeneficiario();
		if(idBeneficiario==0){
			alert("No se pudo guardar la Persona!");
			return false;
		}
		
	}
	var error=0;
	$("table.tablero :input.ui-state-error,select.ui-state-error").removeClass("ui-state-error");
	error=validarSelect($("#tipoDocBen2"),error);
	error=validarSelect($("#estadosBen2"),error);
	error=validarSelect($("#txtCiudadBen2"),error);
	error=validarSelect($("#txtGeneroBen2"),error);
	error=validarSelect($("#txtTipoBen2"),error);
	error=validarSelect($("#txtTipoForBene2"),error);
	if($("#txtTipoBen2").val()==38 || $("#txtTipoBen2").val()==35)
		error=validarSelect($("#txtMadrePadre2"),error);
	error=validarTexto($("#numeroBen2"),error);
	error=validarTexto($("#pNombreBen2"),error);
	error=validarTexto($("#pApellidoBen2"),error);
	error=validarTexto($("#txtFechaNaceBen2"),error);
	fechaafiliacion
	if (error > 0) return false;
	
	var nRelaciones = new GrupoFamiliar();
	nRelaciones = camposGrupoFamiliar(nRelaciones);
	nRelaciones.idtrabajador=idPersona;
	nRelaciones.idbeneficiario=idBeneficiario;
	nRelaciones.idparentesco=tb;
	nRelaciones.idconyuge=idConyuge;
	nRelaciones.embarga = 'N';
	nRelaciones.fechaasignacion = $("#txtFecAsig2").val();
	nRelaciones.estado='A';
	nRelaciones.giro=$("#txtTipoForBene2").val();
	nRelaciones.fechaafiliacion=$("#").val();
	crearRelacion(nRelaciones,1);
	
}

function limpiarBeneficiario(){
	$("#pNombreBen2").val('');
	$("#sNombreBen2").val('');
	$("#pApellidoBen2").val('');
	$("#sApellidoBen2").val('');
	$("#txtFechaNaceBen2").val('');
	$("#estadosBen2").val('');
	$("#txtDptoBen2").val('');
	$("#txtEdadBen").val('');
	$("#txtCiudadBen2").val('');
	$("#txtGeneroBen2").val(0);
}

function insertarBeneficiario(){

	var error=0;
	$("table.tablero :input.ui-state-error,select.ui-state-error").removeClass("ui-state-error");
	error=validarSelect($("#tipoDocBen2"),error);
	error=validarTexto($("#numeroBen2"),error);
	error=validarTexto($("#pNombreBen2"),error);
	error=validarTexto($("#pApellidoBen2"),error);
	error=validarTexto($("#txtFechaNaceBen2"),error);
	error=validarSelect($("#txtGeneroBen2"),error);
	error=validarSelect($("#estadosBen2"),error);
	error=validarSelect($("#txtDptoBen2"),error);
	error=validarSelect($("#txtCiudadBen2"),error);
	if(error > 0) return false;
	
	var nPersona = new Persona();
	nPersona=camposPersona(nPersona);
	
	nPersona.idtipodocumento=$("#tipoDocBen2").val();
	nPersona.identificacion=$("#numeroBen2").val();
	nPersona.papellido=$("#pApellidoBen2").val();
	nPersona.sapellido=$("#sApellidoBen2").val();
	nPersona.pnombre=$("#pNombreBen2").val();
	nPersona.snombre=$("#sNombreBen2").val();
	nPersona.sexo=$("#txtGeneroBen2").val();
	nPersona.fechanacimiento=$("#txtFechaNaceBen2").val();
	nPersona.idpais=$("#estadosBen2").val();
	nPersona.iddepnace=$("#txtDptoBen2").val();
	nPersona.idciunace=$("#txtCiudadBen2").val();
	str = JSON.stringify(nPersona);
	$.ajax({
		url: 'insertPersona.php',    //rutaPHP + 'guardarAfiliacion.php',
		async: false,
		data: {v0:str},
		beforeSend: function(objeto){
			dialogLoading('show');
		},        
		complete: function(objeto, exito){
			dialogLoading('close');
			if(exito != "success"){
				alert("No se completo el proceso!");
			}            
		},
		contentType: "application/x-www-form-urlencoded",
		dataType: "json",
		error: function(objeto, quepaso, otroobj){
			alert("En insertPersona pas� lo siguiente: "+quepaso);
		},
		global: true,
		ifModified: false,
		processData:true,
		success: function(datos){
			if(datos > 0){
				idBeneficiario=datos;
			}
			else
				alert("La Persona NO se pudo guardar!");

		},
		timeout: 300000,
		type: "GET"
	});

}

function limpiarAfiliado(){
	$("#tipDocAfi").val('');
	$("#txtNumero").val('') ;
	$("#txtPNombre").val('');
	$("#txtSNombre").val('');
	$("#txtPApellido").val('');
	$("#txtSApellido").val('');
	$("#cboCivil").val('');
	$("#txtFechaNace").val('');
	$("#deptos").val('');
	$("#cboGenero").val('');
	$("#txtDireccion").val('');
	$("#cboDepto").val('');
	$("#cboCiudad").val('');
	$("#cboZona").val('');
	$("#cboBarrio").val('');
	$("#cboPropiedad").val('');
	$("#txtTelefono").val('');
	$("#txtCelular").val('');
	$("#txtCorreo").val('');
	$("#txtProfesion").val('');
	$("#txtNombreCorto").val('');
	$("#txtMadrePadre option not first").remove();
	$("#txtMadrePadre").append("<option value=" + idPersona + ">Sin padre</option>");
	$("#txtMadrePadre2 option not first").remove();
	$("#txtMadrePadre2").append("<option value=" + idPersona + ">Sin padre</option>");
}