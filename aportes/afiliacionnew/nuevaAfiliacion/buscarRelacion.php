<?php
set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'newrsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'SQLDbManejador.php';
$db = SQLDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo $cadena;
	exit();
}

$idr=$_REQUEST['v0'];
$sql="SELECT idrelacion, idtrabajador, idbeneficiario, idparentesco, idconyuge, conviven, aportes021.fechaafiliacion, fechaasignacion,fechanacimiento, giro, aportes021.estado, idtiporelacion,fechaestado, idbiologico,pnombre,snombre,papellido,sapellido
FROM dbo.aportes021 INNER JOIN aportes015 ON  aportes021.idbeneficiario=aportes015.idpersona WHERE idrelacion =$idr";
$fila = $db->conexionID->query($sql)->fetch();
if( is_array( $fila )){
	echo json_encode($fila);
}
else 
	echo 0;
?>