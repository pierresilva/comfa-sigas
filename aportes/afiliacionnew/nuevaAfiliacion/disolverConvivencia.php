<?php
set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");

$root= $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $_SESSION['RAIZ'] . DIRECTORY_SEPARATOR . 'newrsc/pdo/SQLDbManejador.php';

$db = SQLDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo $cadena;
	exit();
}

$idr=$_REQUEST['v0'];
$sql="Select idtrabajador,idconyuge from aportes021 where idrelacion=$idr";
$idp=$db->conexionID->query($sql)->fetch(PDO::FETCH_OBJ)->idtrabajador;
$idc=$db->conexionID->query($sql)->fetch(PDO::FETCH_OBJ)->idconyuge;
$sql="UPDATE aportes021 SET conviven='N',estado='I' WHERE idconyuge=$idc AND idtrabajador=$idp AND idparentesco=34";
$db->conexionID->beginTransaction();
$rs=$db->conexionID->exec($sql);
if($rs==0){
	$db->conexionID->rollBack();
	echo 0;
	exit();
}
$sql ="UPDATE aportes021 SET conviven='N',estado='I' WHERE idconyuge=$idp AND idtrabajador=$idc AND idparentesco=34";
$rs=$db->conexionID->exec($sql);
if(is_null($rs)){
	$db->conexionID->rollBack();
	echo 0;
	exit();
}
$db->conexionID->commit();
echo 1;

?>