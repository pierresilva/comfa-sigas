var ide;
var URL=src();
var cargoAdmin = 0;
var telefonoAdmin = "";
var celularAdmin = "";
var URL_PHP = "php/";
var idAfiliacion;
function mostrar(){
	$("#trOculto").show();
	$("#trOculto2").show();
	$("#imgMostrar").hide();
	$("#imgOcultar").show();
} 
function ocultar(){
	$("#trOculto").hide();
	$("#trOculto2").hide();
	$("#imgOcultar").hide();
	$("#imgMostrar").show();
}

//METODO BUSCAR LA EMPRESA, RETORNA LOS DATOS DE LA EMPRESA
function buscarEmpresa( nit ){
	var retorno = new Array();
	var $retornoAjax = new Array();
	var $url = URL_PHP + "buscarEmpresa.php";
	var $data = {v0:nit};
	var $async = false;
	retorno.error = 0;
	
	$retornoAjax = metodoAjax( $url, $data, $async );
	if ( $retornoAjax.error == 1 || $retornoAjax.error == 2 ) {
		retorno.error = $retornoAjax.error; 
	}
	else {
		retorno.datos = $retornoAjax.datos;
	}
	return retorno;
	
}

//METODO PARA BUSCAR EL CODIGO CIIU/ACTIVIDAD ECONOMICA
function buscarActividadEco(){
	
	var idCiiu = parseInt( $( "#txtCiiu" ).val().trim() );
	$( "#txtActividadEconomica, #txtCiiu" ).val( "" );
	if( isNaN(idCiiu) )	return false;
	
	var $retornoAjax = new Array();
	var $url = URL_PHP + "buscarCiiu.php";
	var $data = {v0:idCiiu};
	var $async = false;
	
	$retornoAjax = metodoAjax( $url, $data, $async );
	
	if ( $retornoAjax.error == 1 ) {
		alert( "La Actividad Economica no existe en nuestra base" );
	}else if( $retornoAjax.error == 2  ) {
		return false;
	}else{ 
		$( "#txtActividadEconomica" ).val( $retornoAjax.datos.descripcion );
		$( "#txtCiiu" ).val( $retornoAjax.datos.idciiu );
	}
	
}

//METODO PARA BUSCAR EL REPRESENTANTE LEGAL Y ADMINISTRADOR 
function buscarAdminRepre( op ) {
	var hidId = txtNumero = cmbTipoDocum = tdNombre = nomMetodo = "";
	
	if( op == "Repre" ) {
		hidId = "#hidIdRepre";
		cmbTipoDocum = "#cmbTipoDocumRepre";
		txtNumero = "#txtIdentRepre";
		tdNombre = "#tdNombrRepre";
		nomMetodo = "buscarAdminRepre( 'Repre' )";
	}else if ( op == "Admin" ) {
		hidId = "#hidIdAdmin";
		cmbTipoDocum = "#cmbTipoDocumAdmin";
		txtNumero = "#txtIdentAdmin";
		tdNombre = "#tdNombrAdmin";
		nomMetodo = "buscarAdminRepre( 'Admin' )";
	}
	
	var idTipoDocumento = $( cmbTipoDocum ).val();
	var identificacion = $( txtNumero ).val().trim();
	
	$( hidId ).val( "" );
	$( tdNombre ).text( "" );
	$( txtNumero + "," + cmbTipoDocum ).removeClass( "ui-state-error" );
	if ( identificacion == "" ) {
		return false;
	}else if ( idTipoDocumento == 0 ) {
		$( cmbTipoDocum ).addClass( "ui-state-error" );
		return false;
	}
	
	var arrayDatos = new Array();
	
	arrayDatos = buscarPersona( "", idTipoDocumento, identificacion, 2);

	if( arrayDatos.error == 1 ) {
		alert( "La Persona no existe en nuestra base" );
		$( hidId + "," + txtNumero ).val( "" );
		newPersonaSimple( idTipoDocumento, identificacion, nomMetodo, txtNumero );
		return false;
	}else if ( arrayDatos.error == 2 ){
		$( hidId + "," + txtNumero ).val( "" );
		$( tdNombre ).text( "" );
		return false;
	} 
	
	$( tdNombre ).text( arrayDatos.nombre );
	$( hidId ).val( arrayDatos.idpersona );
	$( txtNumero ).val( arrayDatos.identificacion );
	if( op == "Admin" ){
		$( "#txtTelFijoCon" ).val( arrayDatos.telefono );
		$( "#txtTelCelCon" ).val( arrayDatos.celular );
		celularAdmin = arrayDatos.celular;
		telefonoAdmin = arrayDatos.telefono;
		
		var retorno = new Array();
		retorno = buscarCargo( arrayDatos.idpersona );
		if( retorno.error == 0 ){
			$( "#cmbCargo" ).val( retorno.datos.cargo );
			cargoAdmin = retorno.datos.cargo;
			idAfiliacion = retorno.datos.idformulario;
		}
	}
}

//METODO BUSCAR CARGO 
function buscarCargo( idPersona ){
	//BUSCAR CARGO
	var retorno = new Array();
	var $retornoAjax = new Array();
	var $url = URL_PHP + "buscarAfiliacion.php";
	var $data = {v0:idPersona};
	var $async = false;
	retorno.error = 0;
	
	$retornoAjax = metodoAjax( $url, $data, $async );
	if ( $retornoAjax.error == 1 || $retornoAjax.error == 2 ) {
		retorno.error = $retornoAjax.error; 
	}else{
		retorno.datos = $retornoAjax.datos;
	}
	return retorno;
}

//METODO BUSCAR PERSONA CON LOS PARAMETROS.
//tipoDocumento: TIPO DOCUMENTO DE LA PERSONA
//identificacion: NUMERO IDENTIFICACION DE LA PERSONA
//op: OPCION PARA IDENTIFICAR EL TIPO DE CONDICION EN LA QUERY
function buscarPersona( idPersona, tipoDocumento, identificacion, op ) {
	var retorno = new Array();
	var $retornoAjax = new Array();
	var $url = URL_PHP + "buscarPersona.php";
	var $data = {v0:tipoDocumento, v1:identificacion, v2:op, v3:idPersona};
	var $async = false;
	retorno.error = 0;
	
	$retornoAjax = metodoAjax( $url, $data, $async );
	if ( $retornoAjax.error == 1 || $retornoAjax.error == 2 ) {
		retorno.error = $retornoAjax.error; 
	}
	else {
		retorno.nombre = $retornoAjax.datos.pnombre + " " + $retornoAjax.datos.snombre + " " + $retornoAjax.datos.papellido + " " + $retornoAjax.datos.sapellido;
		retorno.idpersona = $retornoAjax.datos.idpersona;
		retorno.identificacion = $retornoAjax.datos.identificacion;
		retorno.idtipodocumento = $retornoAjax.datos.idtipodocumento;
		retorno.telefono = $retornoAjax.datos.telefono;
		retorno.celular = $retornoAjax.datos.celular;
	}
	return retorno;
}

//METODO PARA PARA CREAR UNA PERSONA, INCLUYE EL DIALOGO CON LOS PARAMETROS
//tipoDocum: TIPO DOCUMENTO DE LA PERSONA QUE SE CREARA
//identificacion: IDENTIFICACION DE LA PERSONA QUE SE CREARA
//funcion: NOMBRE DE LA FUNCION PARA EJECUTAR DESPUES DE GUARDAR LA PERSONA
//txtNumero: CAMPO IDENTIFICAICON DE LA PERSONA SE ADMINISTRADOR O REPRESENTANTE
function newPersonaSimple( tipoDocum, identificacion, funcion, txtNumero ) {
	if ( identificacion == "" ) return false; 
	var tipodoc=tipoDocum;
	var doc=identificacion;
	
	$("#dialog-persona").dialog({
		height: 228,
		width: 650,
		draggable:false,
		modal: true,
		open: function(){
			$('#dialog-persona select,#dialog-persona input[type=text]').val('');
			$("#tDocumento").val(tipodoc);
			$("#txtNumeroP").val(doc);
			$("#tDocumento").attr("disabled","disabled");
			actDatepicker($("#fechaNaceDialogPersona"));
		},
		buttons: {
			'Guardar datos': function() {
				var error=0;
				
				$("table.tablero :input.ui-state-error,select.ui-state-error").removeClass("ui-state-error");
				
				error=validarSelect($("#tDocumento"),error);
				error=validarTexto($("#txtNumeroP"),error);
				error=validarTexto($("#spNombre"),error);
				error=validarTexto($("#spApellido"),error);
				error=validarTexto($("#fechaNaceDialogPersona"),error);
				
				if(error > 0) return false;	
				
				var tipodocumento = $( "#tDocumento" ).val();
				var documento = $( "#txtNumeroP" ).val().trim();
				var prNombre = $( "#spNombre" ).val().trim();
				var seNombre = $( "#ssNombre" ).val().trim();
				var prApellido = $( "#spApellido" ).val().trim();
				var seApellido = $( "#ssApellido" ).val().trim();
				var noCorto = nombreCorto( prNombre, seNombre, prApellido, seApellido );
				var fechaNace = $( "#fechaNaceDialogPersona" ).val().trim(); 
			
				var $retornoAjax = new Array();
				var $url = URL_PHP + "guardarPersonaSimple.php";
				var $data = {v0:tipodocumento,v1:documento,v2:prApellido,v3:seApellido,v4:prNombre,v5:seNombre,v19:noCorto,v8:fechaNace};
				var $async = false;
				
				$retornoAjax = metodoAjax( $url, $data, $async );
				
				if( $retornoAjax.error == 1 ) {
					alert( "No se pudo guardar la persona" );
					return false;
				}else if( $retornoAjax.error == 2 ) {
					return false;
				}else{
					$( txtNumero ).val( identificacion );
					eval( funcion );
					$('#dialog-persona select,#dialog-persona input[type=text]').val('');						
					$("#dialog-persona").dialog( 'close' );		
				}
			}
		},
		close: function() {
			$( '#dialog-persona select,#dialog-persona input[type=text]' ).val('');
			$( this ).dialog( "destroy" );
		}
	});
}

//METODO PARA ACTUALIZAR EL CARGO DEL AFILIADO
function actualizaCargoAdmin( idAfiliacion, cargoAdmin ){
	var $retornoAjax = new Array();
	var $url = URL_PHP + "actualizarAfiliacion.php";
	var $data = {v0:idAfiliacion, v1:cargoAdmin};
	var $async = false;
	
	$retornoAjax = metodoAjax( $url, $data, $async );
	if ( $retornoAjax.error == 1 || $retornoAjax.error == 2 ) {
		return false;
	}
	return true;
}

//METODO PARA ACTUALIZAR EL TELEFONO Y CELULAR DE LA PERSONA
function actualizaDatosAdmin( idPersona, telefono, celular){
	var $retornoAjax = new Array();
	var $url = URL_PHP + "actualizarPersona.php";
	var $data = {v0:idPersona, v1:telefono, v2:celular};
	var $async = false;
	
	$retornoAjax = metodoAjax( $url, $data, $async );
	if ( $retornoAjax.error == 1 || $retornoAjax.error == 2 ) {
		return false;
	}
	return true;
}

function validarCampor(){
	var error = 0;
	//VALIDAR CAMPOS OBLIGATORIOS
	$("table.tablero :input.ui-state-error,select.ui-state-error").removeClass("ui-state-error");
	error = validarSelect( $( "#comboTipoDocumento" ), error );
	error = validarTexto( $( "#txtIdEmpresa" ), error );
	error = validarTexto( $( "#txtNit" ), error );
	error = validarTexto( $( "#txtRazonSocial" ), error );
	error = validarSelect( $( "#comboClaseSociedad" ), error );
	error = validarSelect( $( "#comboSector" ), error );
	error = validarTexto( $( "#txtFechaConstitucion" ), error );
	error = validarTexto( $( "#txtFechaAfiliacion" ), error );
	error = validarTexto( $( "#txtCiiu" ), error );
	error = validarSelect( $( "#comboSeccional" ), error );

	error = validarTexto( $( "#txtDireccion" ), error );
	error = validarSelect( $( "#cmbDepartamento" ), error );
	error = validarSelect( $( "#cmbCiudad" ), error );
	error = validarSelect( $( "#cmbZona" ), error );
	error = validarTexto( $( "#txtTelFijo" ), error );
	error = validarTexto( $( "#txtIdentRepre" ), error );
	error = validarSelect( $( "#cmbContratista" ), error );
	
	error = validarSelect( $( "#cmbIndApor" ), error );
	error = validarTexto( $( "#txtInicioApor" ), error );
	error = validarTexto( $( "#txtNumTrab" ), error );
	error = validarSelect( $( "#cmbEstado" ), error );
	error = validarSelect( $( "#cmbClaseApor" ), error );
	error = validarSelect( $( "#cmbTipoApor" ), error );
	error = validarTexto( $( "#hidIdRepre" ), error );

	if( $( "#txtEmail" ).val().trim() != "" && !validateEmail( $( "#txtEmail" ).val() ) ) {
		$( "#txtEmail" ).addClass( "ui-state-error" );
		error++;
	}
	return error;
}

//METODO PARA EJECUTAR AJAX
//$url: ARCHIVO LOGICA 
//$data: TIPO OBJETO {} CONTIENE LOS DATOS
//$async: false, true 
//RETORNA UN ARRAY 
function metodoAjax( $url, $data, $async ){
	var arrayResultado = new Array();
	arrayResultado.error = 0;
	
	//error = 0 : EJECUCION PERFECTA
	//error = 1 : NO HAY DATOS
	//error = 2 : ERROR EN EL SISTEMA
	
	$.ajax({
		url: $url,  
		async: $async,
		data: $data, 
		beforeSend: function(objeto){
        	dialogLoading('show');
        },        
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso, informe a T.I.");
                arrayResultado.error = 2;
            }            
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("En " + $url + " pas� lo siguiente: " + otroobj + ", informe a T.I.");
            arrayResultado.error = 2;
        },
        global: true,
        ifModified: false,
        processData:true,
		success: function( datos ) {
		
			if( datos == "" || datos == 0 ) {
				arrayResultado.error = 1;
			}else {
				arrayResultado.datos = datos;
			}
		},
		timeout: 300000,
        type: "GET"
	});

	return arrayResultado;
}

function formatMoneda(value){
	var num = value.replace(/\./g,'');
	if(!isNaN(num)){
		num = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1.');
		num = num.split('').reverse().join('').replace(/^[\.]/,'');
		return num;
	}
}
	
function formatNormal(input){
	var txt=input.replace(/\./g,'');
	return (txt.length > 0 && !isNaN(txt) &&  txt>0)?parseInt(txt):0;	
}