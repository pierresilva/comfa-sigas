
$(document).ready(function(){
	
	//ASIGNAR EVENTO CLICK
	$( "#imgBuscarEmpresa" ).bind( "click", buscarEmpresaActualiza );
	$( "#btnActualizar" ).bind( "click", guardar ).button();
	
	//ASIGNAR EVENTO BLUR
	$( "#txtNitEmpresa" ).bind( "blur", buscarEmpresaActualiza );
	$( "#txtCiiu" ).bind( "blur", buscarActividadEco );
	
	//LLENAR COMBOS: DEPARTAMENTOS, MUNICIPIOS, ZONA
	$( "#cmbDepartamento" ).jCombo( URL+"pdo/departamentos.php?id=", { 
		parent: "#cmbPais", 
		selected_value: '41' 
	});	
	$( "#cmbCiudad" ).jCombo( URL+"pdo/municipios.php?id=", { 
		parent: "#cmbDepartamento"
	});
	
	$( "#cmbZona" ).jCombo( URL+"pdo/zonas.php?id=", { 
		parent: "#cmbCiudad"
	});	
	
	//CONTROL DE LOS CAMPOS DEL FORMULARIO
	$( "#txtNitEmpresa" ).focus();
	$( "input[type=text], select" ).attr( "disabled", "disabled" );
	$( "#txtNitEmpresa" ).removeAttr( "disabled", "disabled"  );
	$( "#imgMostrar, #imgOcultar" ).css("cursor","pointer");
	$( "#cmbTipoFormulario" ).val( 3316 ).attr( "disabled", "disabled" );
	
	//ASIGNAR DATEPICKER
	actDatepicker( $( "#txtFechaConstitucion") );
	actDatepicker( $( "#txtFechaMatricula" ) );
	actDatepicker( $( "#txtInicioApor" ) );
	
	var anoActual = new Date();
	var strYearRange = anoActual.getFullYear()-120 +":"+ anoActual.getFullYear();
	$("#txtFechaConstitucion").datepicker( "option", "yearRange", strYearRange );
	$("#txtFechaMatricula").datepicker( "option", "yearRange", strYearRange );
	$("#txtInicioApor").datepicker( "option", "yearRange", strYearRange );
});

function nuevo(){
	$( "input:text, input:hidden" ).val( "" );
	$( "select" ).val( 0 );
	$( "#hidIdAdmin, #hidIdRepre" ).val( "" );    
	$( "#cmbTipoFormulario" ).val( 3316 );
	$( "input[type=text], select" ).attr( "disabled", "disabled" );
	$( "#txtNitEmpresa" ).removeAttr( "disabled", "disabled" );
	$("table.tablero :input.ui-state-error,select.ui-state-error").removeClass("ui-state-error");
	$( "#cmbTipoFormulario" ).val( 3316 ).attr( "disabled", "disabled" );
}
function buscarEmpresaActualiza() {
	
	$("table.tablero :input.ui-state-error,select.ui-state-error").removeClass("ui-state-error");
	
	var nit = $( "#txtNitEmpresa" ).val().trim();
	if ( nit == "" )return false;
	
	var retorno = {};
	retorno = buscarEmpresa( nit );
	
	if ( retorno.error == 2 ) {
		nuevo();
		return false;
	}
	else if ( retorno.error == 1 ) {
		nuevo();
		alert( "La empresa con el nit " + nit + ", no esta en nuestra base de datos" );
		return false;
	}
	
	//DISTRIBUIR LOS DATOS EN LOS CAMPOS CORRESPONDIENTES
	$( "#comboTipoDocumento" ).val( retorno.datos.idtipodocumento );                  // Tipo Documento
	$( "#txtNit" ).val( retorno.datos.nit );                                          // Nit
	$( "#txtRazonSocial" ).val( retorno.datos.razonsocial );                          // Razón Social
	
	$( "#txtIdEmpresa" ).val( retorno.datos.idempresa );                              // Id Empresa
	$( "#comboClaseSociedad" ).val( retorno.datos.idclasesociedad );                  // Clase de Sociedad
	$( "#comboSector" ).val( retorno.datos.idsector );                                // Sector
	$( "#txtFechaConstitucion" ).val( retorno.datos.fechamatricula );                 // Fecha Constitución
	$( "#txtFechaAfiliacion" ).val( retorno.datos.fechasistema );                     // Fecha Afiliacion
	$( "#txtCiiu" ).val( retorno.datos.idcodigoactividad );                           // CIIU
	$( "#txtCiiu" ).trigger( "blur" );
	$( "#comboSeccional" ).val( retorno.datos.seccional );                             // Seccional
	
	//INFORMACION ESPECIFICA
	$( "#txtDireccion" ).val( retorno.datos.direccion );                               // Direccion
	//campos. idPais         = $( "#cmbPais" ).val( retorno.datos. );                  // Pais
	$( "#cmbDepartamento" ).val( retorno.datos.iddepartamento );                       // Departamento
	$( "#cmbDepartamento" ).trigger( "change" ); 
	$( "#cmbCiudad" ).val( retorno.datos.idciudad );                                   // Ciudad
	$( "#cmbCiudad" ).trigger( "change" ); 
	$( "#cmbZona" ).val( retorno.datos.idzona );                                       // Zona
	$( "#cmbZona" ).trigger( "change" ); 
	$( "#txtTelFijo" ).val( retorno.datos.telefono );                                  // Telefono
	//campos. celular        = $( "#txtTelCel" ).val( retorno.datos. );                // Celular
	$( "#txtEmail" ).val( retorno.datos.email );                                       // Correo
	$( "#txtFax" ).val( retorno.datos.fax );                                           // Fax
	$( "#hidIdRepre" ).val( retorno.datos.idrepresentante );                           // Id Representante
	$( "#hidIdAdmin" ).val( retorno.datos.idjefepersonal );                            // Id administrador
	$( "#cmbContratista" ).val( retorno.datos.contratista );                           // Contratista
	
	//OTROS DATOS 
	$( "#cmbIndApor" ).val( retorno.datos.indicador );                                  // Indice del Aporte
	$( "#txtInicioApor" ).val( retorno.datos.fechaaportes );                            // Fecha Inicio Aporte
	$( "#txtNumTrab" ).val( retorno.datos.trabajadores );                               // Numero Trabajadores
	$( "#cmbEstado" ).val( retorno.datos.estado );                                      // Estado
	$( "#cmbClaseApor" ).val( retorno.datos.claseaportante );                           // Clase Aportante
	$( "#cmbTipoApor" ).val( retorno.datos.tipoaportante );                             // Tipo Aportante
	
	var arrayDatos = new Array();
	//BUSCAR DATOS DEL REPRESENTANTE
	arrayDatos = buscarPersona( retorno.datos.idrepresentante, "", "", 1);
	
	if( arrayDatos.error == 1 || arrayDatos.error == 2 ) {
		$( "#hidIdRepre" ).val( "" );
	}else {
		$( "#cmbTipoDocumRepre" ).val( arrayDatos.idtipodocumento );
		$( "#tdNombrRepre" ).text( arrayDatos.nombre );
		$( "#txtIdentRepre" ).val( arrayDatos.identificacion );
		$( "#hidIdRepre" ).val( arrayDatos.idpersona );
	}
	
	arrayDatos = "";
	//BUSCAR DATOS DEL ADMINISTRATIVO
	if( !isNaN ( parseInt( retorno.datos.idjefepersonal ) ) ){
		
		arrayDatos = buscarPersona( retorno.datos.idjefepersonal, "", "", 1);
		
		if( arrayDatos.error == 1 || arrayDatos.error == 2 ) {
			$( "#hidIdAdmin" ).val( "" );
		}else {
			$( "#cmbTipoDocumAdmin" ).val( arrayDatos.idtipodocumento );
			$( "#tdNombrAdmin" ).text( arrayDatos.nombre );
			$( "#hidIdAdmin" ).val( arrayDatos.idpersona );
			$( "#txtIdentAdmin" ).val( arrayDatos.identificacion );
			$( "#txtTelFijoCon" ).val( arrayDatos.telefono );
			$( "#txtTelCelCon" ).val( arrayDatos.celular );
			
			telefonoAdmin = arrayDatos.telefono;
			celularAdmin = arrayDatos.celular;
		
			var retorno = new Array();	
			retorno = buscarCargo( arrayDatos.idpersona );
			if( retorno.error == 0 ){
				$( "#cmbCargo" ).val( retorno.datos.cargo );
				cargoAdmin = retorno.datos.cargo;
				idAfiliacion = retorno.datos.idformulario;
			}
		}
	}
	
	$( "input[type=text], select" ).removeAttr( "disabled", "disabled" );
	$( "#txtNitEmpresa" ).attr("disabled","disabled");	
	$( "#cmbTipoFormulario" ).val( 3316 ).attr( "disabled", "disabled" );
}
function guardar(){	
	
	var nitEmpresa   = $( "#txtNitEmpresa" ).val().trim(); 
	if( nitEmpresa == "" ) return false;
	if( validarCampor() > 0 ) return false;
	
	var campos = {};
	
	campos.idtipoafiliacion = $( "#cmbTipoFormulario" ).val().trim();        // Tipo Formulario | TIPO AFILIACION
	
	//INFORMACION GENERAL
	campos.idtipodocumento = $( "#comboTipoDocumento" ).val().trim();        // Tipo Documento
	campos.nit             = $( "#txtNit" ).val().trim();                    // Nit
	campos.razonsocial     = $( "#txtRazonSocial" ).val().trim();            // Razón Social
	campos.idempresa         = $( "#txtIdEmpresa" ).val().trim();            // Id Empresa
	campos.idclasesociedad   = $( "#comboClaseSociedad" ).val();             // Clase de Sociedad
	campos.idsector          = $( "#comboSector" ).val();                    // Sector
	campos.fechamatricula    = $( "#txtFechaConstitucion" ).val().trim();    // Fecha Constitución
	campos.idcodigoactividad = $( "#txtCiiu" ).val().trim();                 // CIIU
	campos.seccional         = $( "#comboSeccional" ).val().trim();          // Seccional
	
	//INFORMACION ESPECIFICA
	campos.direccion       = $( "#txtDireccion" ).val().trim();              // Direccion
	//campos. idPais         = $( "#cmbPais" ).val();                        // Pais
	campos.iddepartamento  = $( "#cmbDepartamento" ).val();                  // Departamento
	campos.idciudad        = $( "#cmbCiudad" ).val();                        // Ciudad
	campos.idzona          = $( "#cmbZona" ).val();                          // Zona
	campos.telefono        = $( "#txtTelFijo" ).val().trim();                // Telefono
	//campos. celular        = $( "#txtTelCel" ).val().trim();               // Celular
	campos.email           = $( "#txtEmail" ).val().trim();                  // Correo
	campos.fax             = $( "#txtFax" ).val().trim();                    // Fax
	campos.idrepresentante = $( "#hidIdRepre" ).val();                       // Id Representante
	campos.cedrep          = $( "#txtIdentRepre" ).val();                    //identificacion representante
	campos.representante   = $( "#tdNombrRepre" ).text();
	campos.idjefepersonal  = $( "#hidIdAdmin" ).val();                       // Id administrador
	var idCargoAdmin   = $( "#cmbCargo" ).val().trim();                      // Cargo Administrador
	var telefonoAdminA  = $( "#txtTelFijoCon" ).val().trim();                // Telefono Administrador
	var celularAdminA  = $( "#txtTelCelCon" ).val().trim();                  // Celular Administrador
	campos.contratista     = $( "#cmbContratista" ).val();                   // Contratista
	
	//OTROS DATOS 
	campos.indicador      = $( "#cmbIndApor" ).val();                        // Indice del Aporte
	campos.fechaaportes   = $( "#txtInicioApor" ).val();                     // Fecha Inicio Aporte
	campos.trabajadores   = $( "#txtNumTrab" ).val().trim();                 // Numero Trabajadores
	campos.estado         = $( "#cmbEstado" ).val().trim();                  // Estado
	campos.claseaportante = $( "#cmbClaseApor" ).val().trim();               // Clase Aportante
	campos.tipoaportante  = $( "#cmbTipoApor" ).val().trim();                // Tipo Aportante
	campos.porplanilla    = "N";    
	

	var bandera = "";
	
	var $retornoAjax = new Array();
	var $url = URL_PHP + "guardarEmpresa.php";
	var $data = {v0:JSON.stringify( campos )};
	var $async = false;
	
	$retornoAjax = metodoAjax( $url, $data, $async );
	if ( $retornoAjax.error == 1 ) {
		alert( "Error al guardar los datos de la empresa, informe a T.I." );
	}else if ( $retornoAjax.error == 2 ) {
		return false;
	}else {
		alert( "La datos de la empresa se guardaron correctamente" );
		if( idCargoAdmin != cargoAdmin && campos.idjefepersonal != "" ){
			if( actualizaCargoAdmin( idAfiliacion, idCargoAdmin ) )
				alert( "El cargo del Contacto Administrativo fue actualizado" );
			else
				alert( "No fue posible actualizar el cargo del Contacto Administrativo, informe a T.I." );
		}
		if( campos.idjefepersonal != "" && ( telefonoAdmin != telefonoAdminA || celularAdminA != celularAdmin ) ){
			if( actualizaDatosAdmin( campos.idjefepersonal, telefonoAdminA, celularAdminA ) )
				alert( "El tel\u00E9fono y celular del Contacto Administrativo se actualizaron" );
			else
				alert( "No fue posible actualizar el tel\u00E9fono y celular del Contacto Administrativo se actualizaron, informe a T.I." );
		}
		observacionesTab( campos.idempresa, 2 );
		nuevo();
	}
}
