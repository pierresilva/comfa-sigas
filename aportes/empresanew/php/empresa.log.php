<?php
/* autor:       OSCAR URIEL RODRIGUEZ T
 * fecha:       08/04/2013
 * objetivo:    
 */
set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
//include_once $raiz. DIRECTORY_SEPARATOR . DIRECTORY_SEPARATOR . 'pdo' . DIRECTORY_SEPARATOR . 'radicacion.class.php';
//include_once $raiz. DIRECTORY_SEPARATOR . DIRECTORY_SEPARATOR . 'pdo' . DIRECTORY_SEPARATOR . 'empresa.class.php';
include_once $raiz. DIRECTORY_SEPARATOR . DIRECTORY_SEPARATOR . 'pdo' . DIRECTORY_SEPARATOR . 'persona.class.php';

/*$url=$_SERVER['PHP_SELF'];
include_once $raiz.DIRECTORY_SEPARATOR.'phpComunes'.DIRECTORY_SEPARATOR.'auditoria.php'; */

include_once '../../newrsc/pdo/SQLDbManejador.php';
$db = SQLDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}



$caso = isset( $_POST["caso"] )?$_POST["caso"]:"";
$arrayResultado = array( "error"=>"", "retorno"=>"" );

switch ( $caso ) {
	case "CONSU_RADIC":
		$idRadicacion = $_POST["idRadicacion"];
		$tipoRadicacion = 30;
		$objRadicacion = new Radicacion;
		
		if ( ( $rowRadicacion = $objRadicacion->buscarRadicacionTipo( $idRadicacion, $tipoRadicacion )->fetch() ) == false ) {
			$arrayResultado["error"] = "La radicacion no se encontro";
			break;		
		}
		
		$objEmpresa = new Empresa;
		
		if ( ( $rowEmpresa = $objEmpresa->buscarEmpresaNit( $rowRadicacion["nit"] ) ) ==  false ) {
			$arrayResultado["error"] = "La radicacion no se encontro";
			break;
		} 
			
		$arrayResultado["retorno"] = $rowEmpresa;		
		break;
		
	case "CONSU_CIIU":
		$idCiiu = $_POST["idCiiu"];
		
		$sql = "SELECT * FROM aportes079 WHERE idciiu = " .$idCiiu;
		
		if( ( $rowCiiu = $db->querySimple( $sql )->fetch() ) == false ) {
			$arrayResultado["error"] = "La Actividad Economica no existe en nuestra base";
			break;
		}
		
		$arrayResultado["retorno"] = $rowCiiu;
		break;
		
	case "CONSU_PERSO":
		$identificacion = $_POST["identificacion"];
		$tipoDocumento = 1;
		
		$objPersona = new Persona;
		 if( ( $rowPersona = $objPersona->buscarPersona( 0, $tipoDocumento, $identificacion, "", "", 2 )->fetch() ) == false ) {
		 	$arrayResultado["error"] = "La Persona no existe en nuestra base";
		 	break;
	 	}
	 	
	 	$arrayResultado["retorno"] = $rowPersona;
		break;
		
	case "GUARD_PERSO":
		
		$campos=array();
		$campos[0]  = (empty($_REQUEST['v0'])) ? NULL : $_REQUEST['v0'];		//idtipodocumento
		$campos[1]  = (empty($_REQUEST['v1'])) ? NULL : trim($_REQUEST['v1']);	//numero
		$campos[2]  = strtoupper(trim($_REQUEST['v2']));						//primer apellido
		$campos[3]  = strtoupper(trim($_REQUEST['v3']));						//segundo apellido
		$campos[4]  = strtoupper(trim($_REQUEST['v4']));						//primer nombre
		$campos[5]  = strtoupper(trim($_REQUEST['v5']));						//segundo nombre
		$campos[8]  = (empty($_REQUEST['v8'])) ? NULL : $_REQUEST['v8'];		//fecha nace
		$campos[19] = (empty($_REQUEST['v19'])) ? NULL : $_REQUEST['v19'];		//nombre  corto
		
		$objPersona = new Persona;
		
		if ( ( $idPersona = $objPersona->guardarPersona( $campos ) ) == 0 ) {
			$arrayResultado["error"] = "No se pudo guardar la persona";
			break;
		}	 
		
		if( ( $rowPersona = $objPersona->buscarRegistroPersona( $idPersona )->fetch() ) == false ) {
			$arrayResultado["error"] = "Error, consultar con el administrador";
			break;
		}
		
		$arrayResultado["retorno"] = $rowPersona;		
		break;

}
echo json_encode($arrayResultado);
?>