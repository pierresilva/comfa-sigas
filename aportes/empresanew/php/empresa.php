<?php
/* autor:       
 * fecha:       12/02/2010
 * objetivo:    Registrar en la base de datos la recepci?n de documentos con destino a Aportes y Subsidio para la afiliaci?n, modificaci?n o adici?n de informaci?n de los afiliados o empresas aportes de Comfamiliar Huila.
 */
set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

/*$url=$_SERVER['PHP_SELF'];
include_once $raiz.DIRECTORY_SEPARATOR.'phpComunes'.DIRECTORY_SEPARATOR.'auditoria.php'; */

include_once '../../newrsc/pdo/SQLDbManejador.php';
$db = SQLDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="es"><head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Nueva Empresa</title>
<link type="text/css" href="../../newcss/Estilos.css" rel="stylesheet">
<link type="text/css" href="../../newcss/css.1.8/jquery-ui-1.8.17.custom.css" rel="stylesheet">
<link type="text/css" href="../../newcss/marco.css" rel="stylesheet">

<script type="text/javascript" src="../../newjs/1.6/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="../../newjs/1.6/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="../../newjs/direccion.js"></script>
<script language="text/javascript" src="../../newjs/comunes.js"></script>
<script language="javascript" src="../../newjs/jquery.combos.js"></script>
<script language="javascript" src="../../newjs/jquery.utilitarios.js"></script>
<script type="text/javascript" src="js/empresa.js"></script>
</head>

<body>
	<form name="forma">
		<center>
			<table width="75%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="13" height="29" class="arriba_iz">&nbsp;</td>
					<td class="arriba_ce"><span class="letrablanca">::&nbsp;Administraci&oacute;n - Empresa Nueva&nbsp;::</span></td>
					<td width="13" class="arriba_de" align="right">&nbsp;</td>
				</tr>
				<tr>
					<td class="cuerpo_iz">&nbsp;</td>
					<td class="cuerpo_cez">&nbsp;</td>
					<td class="cuerpo_de">&nbsp;</td>
				</tr>
				<tr>
					<td class="cuerpo_iz">&nbsp;</td>
					<td class="cuerpo_ce">
						
						<label style="font-size:10px;font-weight:bold">DATOS RADICACION</label>
						<table width="100%" border="0" cellspacing="0" class="tablero">
							<tr >
								<td width="25%">Radicaci&oacute;n Nro</td>
								<td width="25%">
									<input type="text" name="txtNumeroRadicacion" id="txtNumeroRadicacion" />
									<input type="button" name="btnBuscarRadicacion" id="btnBuscarRadicacion" value="Buscar" />
								</td>
								<td width="25%">Tipo Formulario</td>
								<td width="25%">
									<select name="cmbTipoFormulario" class="box1" id="cmbTipoFormulario" readonly="readonly">
										<option value="0">Seleccione...</option>
										<?php
											$rs = $db->Definiciones ( 56, 4);
											 while ( $row = $rs->fetch () ) { ?>
												<option value="<?php echo $row ['iddetalledef'];?>" <?php  if($row ['codigo'] == '01'){ ?> selected = "selected"<?php } ?>> <?php echo $row ['detalledefinicion']; ?> </option>
										<?php } ?>
									</select>	
								</td>
							</tr>
						</table>
						<label style="font-size:10px;font-weight:bold">INFORMACION GENERAL</label>
						<table width="100%" border="0" cellspacing="0" class="tablero">	
							<tr>
								<td width="25%">Tipo Documento</td>
								<td width="25%">
									<select name="comboTipoDocumento" class="box1" id="comboTipoDocumento"  >
										<option value="0" selected="selected">Seleccione...</option>
										<?php
											$rs = $db->Definiciones ( 1, 4);
											 while ( $row = $rs->fetch () ) { 
												if($row["iddetalledef"] == 5 ||  $row["iddetalledef"] == 1){?>
												<option value="<?php echo $row ['iddetalledef'];?>"> <?php echo $row ['detalledefinicion']; ?> </option>
										<?php }} ?>
									</select><img src="../../imagenes/menu/obligado.png" alt=""	width="12" height="12" />
								</td>
								<td width="25%"><label for="iden">NIT</label></td>
								<td width="25%">
									<label> 
										<input name="txtNit" type="text" class="box1" id="txtNit" onblur="validarLongNumIdent(5,this); buscarEmpresa(this);" value="" onkeydown='solonumeros(this);' onkeyup='solonumeros(this);' />
										 - 
									</label> D&iacute;gito 
									<label for="textfield3">
									</label> 
									<input name="txtDigito" type="text" class="boxcorto" id="txtDigito" readonly /> 
									<label><img src="../../imagenes/menu/obligado.png" width="12" height="12" /></label>
								</td>
							</tr>
							<tr>
								<td>Raz&oacute;n Social</td>
								<td colspan="3"><input name="txtRazonSocial" type="text" class="boxlargo" id="txtRazonSocial" /> 
									<img src="../../imagenes/menu/obligado.png" width="12" height="12" /> -
							      <input name="txtIdEmpresa" type="text" class="box1" id="txtIdEmpresa" readonly /></td>								
							</tr>
							<tr>
								<td width="25%">Clase de Sociedad</td>
								<td width="25%">
									<select id="comboClaseSociedad" name="comboClaseSociedad" onblur="" class="box1" >
										<option value="0" selected="selected">Seleccione...</option>
										<option value="101">NATURAL</option>
										<option value="102">JURIDICA</option>
									</select><img src="../../imagenes/menu/obligado.png" alt=""	width="12" height="12" />
								</td>
								<td width="25%">Sector</td>
								<td width="25%">
									<select name="comboSector" class="box1" id="comboSector" >
										<option value="0" selected="selected">Seleccione...</option>
										<option value="93" >PUBLICO</option>
										<option value="94" >PRIVADO</option>
									</select><img src="../../imagenes/menu/obligado.png" alt=""	width="12" height="12" />	
								</td>
							</tr>
							<tr>
								<td>Fecha Constituci&oacute;n</td>
								<td><input name="txtFechaConstitucion" type="text" class="box1" id="txtFechaConstitucion" value=""  />
									<img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
								<td>Fecha Afiliaci&oacute;n</td>
								<td><input name="txtFechaAfiliacion" type="text" class="box1" id="txtFechaAfiliacion" value="" readonly="readonly"  />
									<img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
							</tr>
							<tr>
								<td>CIIU / Actividad Economica</td>
								<td><input name="txtCiiu" type="text" class="box1" id="txtCiiu" value="" onkeydown='solonumeros(this);' onkeyup='solonumeros(this);' /> - <input name="txtCodigo" type="text" class="boxcorto" id="txtCodigo" readonly="readonly"/>
									<img src="../../imagenes/menu/obligado.png" width="12" height="12" />
								</td>
								<td colspan="3"><input name="txtActividadEconomica" type="text" class="boxlargo" id="txtActividadEconomica" readonly="readonly"/> 
								</td>
							</tr>
							<tr>
								<td width="25%">Seccional</td>
								<td width="25%">
									<select name="comboSeccional" class="box1" id="comboSeccional" >
										<option value="0" selected="selected">Seleccione...</option>
										<option value="01">Neiva</option>
										<option value="02">Garz&oacute;n</option>
										<option value="03">Pitalito</option>
										<option value="04">La Plata</option>
									</select><img src="../../imagenes/menu/obligado.png" alt=""	width="12" height="12" />	
								</td>	
								<td></td>
								<td></td>								
							</tr>
						</table>
						<label style="font-size:10px;font-weight:bold">INFORMACION ESPECIFICA</label>
						<table id="tablaEspecifica" width="100%" border="0" cellspacing="0" class="tablero">	
							<tr>
								<td width="25%">Direcci&oacute;n</td>
								<td colspan="3"><input name="txtDireccion" type="text" class="boxlargo" id="txtDireccion" onfocus="direccion(this,document.getElementById('cmbDepartamento'));" /> 
								<img src="../../imagenes/menu/obligado.png" width="12" height="12" /></td>
							</tr>
							<tr>
								<td>Pais</td>
								<td width="25%">
                                <select name="cmbPais" class="box1" id="cmbPais" >
								<option value="48" selected="selected">Colombia</option>
								 
							    </select></td>
								<td width="25%">Departamento</td>
								<td width="25%"><select name="cmbDepartamento" class="box1" id="cmbDepartamento" >
								<option value="0" selected="selected">Seleccione...</option>
								<?php
								$rs = $db->Departamento();
								while ( $row = $rs->fetch () ) { ?>
								  <option value="<?php echo $row["coddepartamento"];?>"> <?php echo utf8_decode($row["departmento"]); ?></option>
								  <?php } ?>
								  </select>
							    <img src="../../imagenes/menu/obligado.png" alt=""	width="12" height="12" /></td>
							</tr>
							<tr>
							  <td>Ciudad</td>
							  <td><select name="cmbCiudad" class="box1" id="cmbCiudad" >
							    <option value="0" selected="selected">Seleccione...</option>
							    </select>
                              <img src="../../imagenes/menu/obligado.png" alt=""	width="12" height="12" /></td>
							  <td>Zona</td>
							  <td><select name="cmbZona" class="box1" id="cmbZona" >
							    <option value="0" selected="selected">Seleccione...</option>
							    </select>
                              <img src="../../imagenes/menu/obligado.png" alt=""	width="12" height="12" /></td>
						  </tr>
							<tr>
								<td>Telefono Fijo</td>
								<td><input name="txtTelFijo" type="text" class="box1" id="txtTelFijo" value="" onkeyup="solonumeros(this);" onkeydown='solonumeros(this);' />
									<img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
								<td>Telefono Celular</td>
								<td><input name="txtTelCel" type="text" class="box1" id="txtTelCel" value="" onkeyup="solonumeros(this);" onkeydown='solonumeros(this);'/></td>
							</tr>
							<tr>
								<td>E-Mail</td>
								<td><input name="txtEmail" type="text" class="box1" id="txtEmail" value="" onblur="soloemail(this);"/></td>
								<td>Fax</td>
								<td><input name="txtFax" type="text" class="box1" id="txtFax" value="" onkeyup="solonumeros(this);" onkeydown='solonumeros(this);'/></td>
							</tr>
							<tr>
								<td>C.C. Representante Legal</td>
								<td>
									<input name="txtRepresentante" type="text" class="box1" id="txtRepresentante"  onkeyup="solonumeros(this);" onkeydown='solonumeros(this);' />
									<img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
									</td>
								<td colspan="2"><input type="text" class="boxlargo" id="txtNombrRepre" name="txtNombrRepre" readonly="readonly"/></td>
							</tr>
							<tr>
								<td><label for="txtAdmin">C.C. Contacto Administrativo</label></td>
								<td><input name="txtAdmin" type="text" class="box1" id="txtAdmin" onkeypress="" onkeyup="solonumeros(this);" onkeydown='solonumeros(this);'/></td>
								<td colspan="2"><input type="text" class="boxlargo" id="txtNombrAdmin" name="txtNombrAdmin" readonly="readonly"/>
									&nbsp; <img id="imgMostrar" src="../../imagenes/show.png" alt="" width="18" height="18" onclick="mostrar();"/>
									<img id="imgOcultar" src="../../imagenes/hide.png" alt="" width="18" height="18" onclick="ocultar();" style="display: none;"/>
								</td>
							</tr>
							<tr id="trOculto" style="display: none;">
								<td>Cargo</td>
								<td><select name="cmbCargo" class="box1" id="cmbCargo">
										<option value="0" selected="selected">Seleccione...</option>
										<?php
											$rs = $db->Definiciones ( 21, 4);
											 while ( $row = $rs->fetch () ) { ?>
												<option value="<?php echo $row ['iddetalledef'];?>"> <?php echo utf8_decode($row ['detalledefinicion']); ?> </option>
										<?php } ?>
									</select> 
								</td>
								<td>Telefono Fijo</td>
								<td><input name="txtTelFijoCon" type="text" class="box1" id="txtTelFijoCon" value="" onkeyup="solonumeros(this);" onkeydown='solonumeros(this);'/></td>
							</tr>
							<tr id="trOculto2" style="display: none;">
								<td>Telefono Celular</td>
								<td><input name="txtTelCelCon" type="text" class="box1" id="txtTelCelCon" value="" onkeyup="solonumeros(this);" onkeydown='solonumeros(this);'/></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td>Lugar Donde Se <br> Causan Los Salarios</td>
								<td><select name="cmbCausan" class="box1" id="cmbCausan">
										<option value="0" selected="selected">Seleccione...</option>
									</select></td>
								<td>Contratista</td>
								<td><select name="cmbContratista" class="box1" id="cmbContratista" >
										<option value="0">Seleccione...</option>
										<option value="N" selected="selected">NO</option>
										<option value="S">SI</option>
									</select>
									<img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
							</tr>
						</table>
						<label style="font-size:10px;font-weight:bold">OTROS DATOS</label>
						<table width="100%" border="0" cellspacing="0" class="tablero">	
							<tr>
								<td width="25%"><label for="">Ultima Nomina <br> Causada (Periodo)</label></td>
								<td width="25%"><input type="text" id="txtMes" name="txtMes" class="box1" onkeyup="solonumeros(this);" maxlength="6"/></td>
								<td width="25%"> Valor</td>
								<td><input type="text" style="text-align:right" value="0" maxlength="15" id="txtValor" name="txtValor" class="box1" onKeyDown="solonumeros(this);formatMoneda(this);" onKeyUp="solonumeros(this);formatMoneda(this);"/></td>
							</tr>
							<tr>
								<td>Indice Aporte</td>
								<td><select name="cmbIndApor" class="box1" id="cmbIndApor" disabled="disabled">
										<option value="0">Seleccione...</option>
										<?php
											$rs = $db->Definiciones ( 18, 4);
											 while ( $row = $rs->fetch () ) { ?>
												<option value="<?php echo $row ['iddetalledef'];?>" <?php  if($row ['codigo'] == '03'){ ?> selected = "selected"<?php } ?>> <?php echo $row ['detalledefinicion']; ?> </option>
										<?php } ?>
									</select>
									<img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
								<td>Fecha Inicio Aporte</td>
								<td><input name="txtInicioApor" type="text" class="box1" id="txtInicioApor" value="" readonly/>
									<img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
							</tr>
							<tr>
								<td>Nro Trabajadores</td>
								<td><input name="txtNumTrab" type="text" class="box1" id="txtNumTrab" value="" onkeyup="solonumeros(this);" onkeydown='solonumeros(this);'/>
									<img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
								<td>Estado</td>
								<td><select name="cmbEstado" class="box1" id="cmbEstado">
										<option value="0">Seleccione...</option>
										<option value="A" selected="selected">ACTIVO</option>
										<option value="I">INACTIVO</option>
									</select>
									<img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>	
							</tr>
							<tr>
								<td>Clase Aportante</td>
								<td><select name="cmbClaseApor" class="box1" id="cmbClaseApor">
										<option value="0" selected="selected">Seleccione...</option>
										<?php
											$rs = $db->Definiciones ( 33, 3);
											 while ( $row = $rs->fetch () ) { 
												if($row ['iddetalledef']!='2655'){
										?>
												<option value="<?php echo $row ['iddetalledef'];?>"> <?php echo $row ['detalledefinicion']; ?> </option>
										<?php }} ?>
									</select>
									<img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>	
								<td>Tipo Aportante</td>
								<td><select name="cmbTipoApor" class="box1" id="cmbTipoApor">
										<option value="0" selected="selected">Seleccione...</option>
										<?php
											$rs = $db->Definiciones ( 34, 3);
											 while ( $row = $rs->fetch () ) { 
												if($row ['iddetalledef']!='2657'){
										?>
												<option value="<?php echo $row ['iddetalledef'];?>"> <?php echo $row ['detalledefinicion']; ?> </option>
										<?php }} ?>
									</select>
									<img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>	
							</tr>
						</table>
						<table width="100%" border="0" cellspacing="0">
							<tr>
								<td align="left">&nbsp;</td>
							</tr>
							<tr>
								<td align="left" class="Rojo"><img	src="../../imagenes/menu/obligado.png" width="16" height="12" align="left" />Campo Obligado</td>
							</tr>
						</table>
					</td>
					<td class="cuerpo_de"></td>
				</tr>
				<tr>
					<td class="abajo_iz" >&nbsp;</td>
			<td class="abajo_ce" align="center" ><input type="button" value="Guardar" id="btnGuardar" name="btnGuardar" onclick="guardar();"/></td>
					<td class="abajo_de" >&nbsp;</td>
				</tr>
			</table>
		</center>	
	</form>
	<input type="hidden" name="hidIdRepre" id="hidIdRepre" />
	<input type="hidden" name="hidIdAdmin" id="hidIdAdmin" />
	<input type="hidden" name="hidIdCodigActiv" id="hidIdCodigActiv" />
	<!-- formulario direcciones  --> 
	<div id="dialog-form" title="Formulario de direcciones"></div>
	<!-- DIALOGO PERSONA SIMPLE -->
	<div id="dialog-persona" title="Formulario Datos Basicos" style="display:none">
	<center>
		<table width="578" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff" class="tablero">
			<tbody>
				<tr bgcolor="#EBEBEB">
				  <td colspan="4" style="text-align:center" >Datos B&aacute;sicos de la persona</td>
			  </tr>
				<tr>
				  <td >Tipo Documento</td>
				  <td ><select name="tDocumento" id="tDocumento" class="box1" disabled>
		          	<option value="1">CEDULA DE CIUDADANIA</option>
					<option value="2">TARJETA DE IDENTIDAD</option>
					<option value="3">PASAPORTE</option>
					<option value="4">CEDULA DE EXTRANJERIA</option>
					<option value="5">NIT</option>
					<option value="6">REGISTRO CIVIL</option>
					<option value="7">MENOR SIN IDENTIFICACION</option>
					<option value="8">ADULTO SIN IDENTIFICACION</option>
		          </select>
		          </td>
				  <td >N&uacute;mero</td>
				  <td ><input name="txtNumeroP" id="txtNumeroP" type="text" class="box1" readonly></td>
			  </tr>
				<tr>
				  <td >Primer Nombre</td>
				  <td ><input name="spNombre" id="spNombre" type="text" class="box1" onkeydown='sololetras(this);' onkeyup='sololetras(this);' onkeypress="return validarEspacio(event)"></td>
				  <td >Segundo Nombre</td>
				  <td ><input name="ssNombre" id="ssNombre" type="text" class="box1" onkeydown='sololetras(this);' onkeyup='sololetras(this);'></td>
			  </tr>
				<tr>
				  <td >Primer Apellido</td>
				  <td ><input name="spApellido" id="spApellido" type="text" class="box1" onkeydown='sololetras(this);' onkeyup='sololetras(this);' onkeypress="return validarEspacio(event)"></td>
				  <td >Segundo Apellido</td>
				  <td ><input name="ssApellido" id="ssApellido" type="text" class="box1" onkeydown='sololetras(this);' onkeyup='sololetras(this);'></td>
			  </tr>
			  <tr>
		    	<td><label for="fechaNaceDialogPersona">Fecha de nacimiento</label></td>
		    	<td><input type="text" name="fechaNaceDialogPersona" id="fechaNaceDialogPersona" readonly="readonly"/></td>
		    	<td >&nbsp;</td>
				<td >&nbsp;</td>
			  </tr>
				<tr>
					<td >&nbsp;</td>
					<td >&nbsp;</td>
					<td >&nbsp;</td>
					<td >&nbsp;</td>
				</tr>
		</tbody>
		</table>
	</center>
	</div>
	<!-- FORMULARIO OBSERVACIONES-->
	<div id="div-observaciones-tab" style="display:none" title="Observaciones de modificaci&oacute;n">
	<table class="tablero">
	 <tr>
	   <td>Usuario</td>
	   <td colspan="3" >
	   <input name="usuarioObs" id="usuarioObs" class="box1" disabled="disabled" value="<?php echo 'prueba' //$_SESSION['USUARIO']?>" /></td>
	   </tr>
	 <tr>
	   <td>Observaciones</td>
	   <td colspan="3" >
	   <textarea name="observacionGrupo" id="observacionGrupo" cols="45" rows="5" class="boxlargo"></textarea></td>
	   </tr>
	</table>
	<div class="ui-state-highlight ui-corner-all" style="margin: 9px auto; display:none;padding: 6px; text-align: center; border: 1px solid rgb(255, 255, 0); width:300px" id="rtaObservacion"><span class="ui-icon ui-icon-info" style="float: left; margin-right: 0.3em;"></span>Observaci&oacute;n guardada con &eacute;xito.</div>
	</div>

</body>

</html>