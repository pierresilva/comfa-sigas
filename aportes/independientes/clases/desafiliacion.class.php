<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';

include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'utilitario.class.php';

class Desafiliacion{
 	private static $con = null;

 	function __construct(){
 		try{
			self::$con = IFXDbManejador::conectarDB();
			if( self::$con->conexionID==null ){
				throw new Exception(self::$con->error);
			}
		}catch(Exception $e){
			echo $e->getMessage();
			exit();
		}
 	}

 	public function actua_afili_inact($datos){
 		$sql = "UPDATE aportes017 SET 
 					fecharetiro='{$datos[fecha_retiro]}'
 					, motivoretiro={$datos[id_motivo_retiro]} 
 				WHERE idformulario=".$datos['id_afiliacion_inactiva'];

 		$statement = self::$con->conexionID->prepare($sql);
		$guardada = $statement->execute();
		return $guardada == false ? 0 : 1;
 	}

 	public function activ_afili_inact($idDesafiliacion,$usuario){
 		$id_afiliacion_activa = 0;
		$sentencia = self::$con->conexionID->prepare ( "EXEC [kardex].[sp_000_Activar_Afiliacion_Expul_Indep]
				@id_desafiliacion = '$idDesafiliacion'
				, @usuario = '$usuario'
				, @id_afiliacion_activa = :id_afiliacion_activa" );
		$sentencia->bindParam ( ":id_afiliacion_activa", $id_afiliacion_activa, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 4000 );
		$sentencia->execute ();
		if ($id_afiliacion_activa > 0)
			return $id_afiliacion_activa;
		else
			return 0;
 	}

 	public function consultar($arrAtributoValor){
 		$attrEntidad = array(
				array("nombre"=>"id_desafiliacion","tipo"=>"NUMBER","entidad"=>"a251")
				,array("nombre"=>"id_trabajador","tipo"=>"NUMBER","entidad"=>"a251")
				,array("nombre"=>"id_afiliacion_inactiva","tipo"=>"NUMBER","entidad"=>"a251")
				,array("nombre"=>"estado","tipo"=>"TEXT","entidad"=>"a251")
				,array("nombre"=>"fecha_estado","tipo"=>"DATE","entidad"=>"a251")
				,array("nombre"=>"usuario","tipo"=>"TEXT","entidad"=>"a251")
				,array("nombre"=>"fecha_sistema","tipo"=>"DATE","entidad"=>"a251"));
		
		$filtroSql = Utilitario::obtenerFiltro($attrEntidad,$arrAtributoValor);
		$sql = 'SELECT id_desafiliacion, id_trabajador, id_afiliacion_inactiva, a251.estado, fecha_estado, a251.usuario, fecha_sistema
					, a17.fechaingreso, a17.fecharetiro
				FROM aportes251 a251
					LEFT JOIN aportes017 a17 ON a17.idformulario=a251.id_afiliacion_inactiva '
				.$filtroSql;
		return Utilitario::fetchConsulta($sql,self::$con);
 	}

 }