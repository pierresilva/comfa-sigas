<?php
date_default_timezone_set("America/Bogota");
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';

include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'utilitario.class.php';

class ControlNotificacion{
 	private static $con = null;

 	function __construct(){
 		try{
			self::$con = IFXDbManejador::conectarDB();
			if( self::$con->conexionID==null ){
				throw new Exception(self::$con->error);
			}
		}catch(Exception $e){
			echo $e->getMessage();
			exit();
		}
 	}

 	function inicioTransaccion(){
		self::$con->inicioTransaccion();
	}
	function cancelarTransaccion(){
		self::$con->cancelarTransaccion();
	}
	function confirmarTransaccion(){
		self::$con->confirmarTransaccion();
	}

 	/**
	 * Guarda la notificacion del independiente
	 * @param unknown_type $campos
	 * @return number: 0 | 1
	 */
	function guardar_notificacion($arrDatos,$usuario){
		$fechaEstado = empty($arrDatos["fecha_estado"])?"null":"'{$arrDatos["fecha_estado"]}'";
		
		$sql="INSERT INTO dbo.aportes253(informacion,contenido_carta,id_carta_notificacion,id_trabajador,id_estado,fecha_estado,usuario)
				VALUES(:informacion,:contenido_carta,{$arrDatos["id_carta_notificacion"]},{$arrDatos["id_trabajador"]},{$arrDatos["id_estado"]},$fechaEstado,'$usuario')";
		
		$statement = self::$con->conexionID->prepare($sql);
		$statement->bindValue(":informacion", $arrDatos["informacion"],  PDO::PARAM_STR);
		$statement->bindValue(":contenido_carta", $arrDatos["contenido_carta"],  PDO::PARAM_STR);
		$guardada = $statement->execute();
		return $guardada == false ? 0 : 1;
	}

	/**
	 * Guarda observacion al trabajador
	 * @param unknown_type $campos
	 * @return number: 0 | 1
	 */
	function guardar_observacion($arrDatos,$usuario){
		$fecha = date("m/d/Y");
		$flagObservacion = 1;//[1][OBSERVACION TRABAJADOR]
		$observacionAnterior = self::$con->b_obserbacion($arrDatos["id_trabajador"], $flagObservacion);
		
		$observacionNueva = $fecha." - ".$usuario." - ".$arrDatos["observacion"]."<br>".$observacionAnterior;
		
		//Elimina la observacion que tenga
		$sql="delete from aportes088 WHERE idregistro={$arrDatos["id_trabajador"]} AND identidad=$flagObservacion";
		self::$con->queryActualiza($sql);
		
		$rs=self::$con->i_obserbacion($arrDatos["id_trabajador"], $observacionNueva, $flagObservacion, "aportes088");
		return $rs > 0 ? 1 : 0;
	}

	/**
	 * Metodo para actualizar el estado notificacion de la empresa
	 * El parametro debe ser un array indice valor array("atributo"=>valor,...)
	 * @param unknown_type $datos 
	 * @return multitype: 0 | 1
	 */
	public function actualizar_estado_notificacion($datos){
		$sql = "UPDATE dbo.aportes253
				SET 
					id_estado = {$datos["id_estado"]},
					fecha_estado = '{$datos["fecha_estado"]}'
				WHERE id_notificacion={$datos["id_notificacion"]}";
		$statement = self::$con->conexionID->prepare($sql);
		$guardada = $statement->execute();
		return $guardada == false ? 0 : 1;
	}

	/**
	 * Metodo para buscar las notificaciones del independiente
	 * El parametro debe ser un array indice valor array("atributo"=>valor,...)
	 * @param unknown_type $arrAtributoValor
	 * @return multitype: array
	 */
	function buscar_notificacion($arrAtributoValor){
		$attrEntidad = array(
				array("nombre"=>"id_notificacion","tipo"=>"NUMBER","entidad"=>"a253")
				,array("nombre"=>"id_carta_notificacion","tipo"=>"NUMBER","entidad"=>"a253")
				,array("nombre"=>"id_trabajador","tipo"=>"NUMBER","entidad"=>"a253")
				,array("nombre"=>"id_estado","tipo"=>"NUMBER","entidad"=>"a253")
				,array("nombre"=>"fecha_sistema","tipo"=>"DATE","entidad"=>"a253")
				,array("nombre"=>"fecha_estado","tipo"=>"DATE","entidad"=>"a253")
				,array("nombre"=>"identificacion","tipo"=>"TEXT","entidad"=>"a15"));
	
		$filtroSql = Utilitario::obtenerFiltro($attrEntidad,$arrAtributoValor);
	
		$sql = "SELECT a253.id_notificacion,a253.usuario,a253.fecha_sistema,a253.fecha_estado,a253.informacion, a253.id_carta_notificacion, a253.contenido_carta
					, a15.identificacion, a15.pnombre+' '+isnull(a15.snombre,'')+' '+a15.papellido+' '+isnull(a15.sapellido,'') AS razonsocial
					, a91.detalledefinicion AS carta_notificacion
					, b91.detalledefinicion AS estado_notificacion 
				FROM aportes253 a253 
					LEFT JOIN aportes015 a15 ON a15.idpersona=a253.id_trabajador
					LEFT JOIN aportes091 a91 ON a91.iddetalledef=a253.id_carta_notificacion
					LEFT JOIN aportes091 b91 ON b91.iddetalledef=a253.id_estado
				$filtroSql";
		return Utilitario::fetchConsulta($sql,self::$con);
	}

 }