<?php
	set_time_limit(0);
	ini_set("display_errors",'1');
	date_default_timezone_set("America/Bogota");
	$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
	include_once  $root;
	include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';
	include_once $raiz. DIRECTORY_SEPARATOR .'clases'. DIRECTORY_SEPARATOR . 'p.definiciones.class.php';
	
	$usuario=$_SESSION['USUARIO'];
	
	//Objeto de la definicion
	$objDefiniciones = new Definiciones();
	
?> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<title>Independientes | Control Notificaci&oacute;n</title>
		<link type="text/css" href="<?php echo URL_PORTAL; ?>newcss/Estilos.css" rel="stylesheet">
		<link type="text/css" href="<?php echo URL_PORTAL; ?>newcss/css.1.8/jquery-ui-1.8.17.custom.css" rel="stylesheet">
		<link type="text/css" href="<?php echo URL_PORTAL; ?>newcss/marco.css" rel="stylesheet">
		
		<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/1.6/jquery-1.6.2.min.js"></script>
		<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/1.6/jquery-ui-1.8.16.custom.min.js"></script>
		<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/comunes.js"></script>

		<script language="javascript" src="js/control_notificacion.js"></script>
		<style type="text/css">
			.classCampoCorto {background: none repeat scroll 0 0 #F7F7F7; border-style: hidden; border-width: 0; font-size: 11px; padding: 0;}
			.classTextError {color:#ff0000}
		</style>
	</head>
	<body>
		<center>
			<br /><br />
			<table width="80%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="arriba_iz" >&nbsp;</td>
					<td class="arriba_ce" ><span class="letrablanca">::Control Notificaci&oacute;n::</span></td>
					<td class="arriba_de" >&nbsp;</td>
				</tr>
				<tr>
					<td class="cuerpo_iz">&nbsp;</td>
					<td class="cuerpo_ce"><img src="<?php echo URL_PORTAL; ?>imagenes/spacer.gif" alt="" width="2" height="1" /></td>
					<td class="cuerpo_de">&nbsp;</td>
				</tr>
				<tr>
					<td class="cuerpo_iz" >&nbsp;</td>
					<td class="cuerpo_ce" >
						<table border="0" class="tablero" cellspacing="0" width="90%" style="margin:0 auto;">
							<tr>
						    	<td width="20%" style="text-align:right;">
						    		N&uacute;mero Identificaci&oacute;n:
						    		<input type="hidden" id="hidIdTrabajador" name="hidIdTrabajador"/>
						    	</td>
						    	<td >
						    		<input type="text" name="txtNumeroIdentificacion" id="txtNumeroIdentificacion" class="box1"/>
						    		&nbsp; <label id="lblRazonSocial" >&nbsp;</label>
						    	</td>
						  	</tr>
						  	<tr>
							    <td width="100" style="text-align:right;">
						    		Carta Notificaci&oacute;n:
						    	</td>
						    	<td>
						    		<select name="cmbCartaNotificacion" id="cmbCartaNotificacion" class="box1">
						    			<option value="">Seleccione</option>
						    			<?php
								        	$consulta=$objDefiniciones->mostrar_datos(84);
								        	while($row=mssql_fetch_array($consulta)){
								       			echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
								           	}
						    			?>
						    		</select>
						    	</td>
							</tr>
						  	<tr>
						    	<td colspan="2" id="tdMensajeError" class="classTextError"></td>
						  	</tr>
						  	<tr>
						    	<td colspan="2"><b>Notificaciones</b></td>
						  	</tr>
					  		<tr>
					  			<td colspan="2">
									<table border="0" class="tablero" cellspacing="0" width="100%" id="tabNotificacion"> 
										<thead width="100%">
									  		<tr>
									  			<th width="5%"></th>
									  			<th width="30%">Carta</th>
									  			<th width="30%">Estado</th>
									  			<th width="18%">Fecha Notificaci&oacute;n</th>
									  			<th width="18%">Fecha Estado</th>
									  			<th width="30%">Informaci&oacute;n</th>
									  			<th width="30%">...</th>
									  		</tr>
									  	</thead>
									  	<tbody id="tBody" width="100%">		
									  	</tbody>
									</table>
								</td>
							</tr>
							<tr><td colspan="2"></td></tr>
							<tr>
						    	<td width="20%" style="text-align:right;">
						    		Estado Notificaci&oacute;n:
						    	</td>
						    	<td>
						    		<select name="cmbEstado" id="cmbEstado" class="box1 element-required">
						    			<option value="">Seleccione</option>
						    			<?php
								        	$consulta=$objDefiniciones->mostrar_datos(72);
								        	while($row=mssql_fetch_array($consulta)){
								       			echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
								           	}
						    			?>
						    		</select>
						    		<span class="clsObligatorio">&nbsp;&nbsp;&nbsp;</span>
						    	</td>
						  	</tr>
						  	<tr>
						    	<td width="20%" style="text-align:right;">
						    		Fecha Estado:
						    	</td>
						    	<td>
						    		<input typw="text" name="txtFechaEstado" id="txtFechaEstado" readonly="readonly" class="box1"/>
						    		<span class="clsObligatorio">&nbsp;&nbsp;&nbsp;</span>
						    	</td>
						  	</tr>
							<tr >
								<td style="text-align:right;">Observaci&oacute;n:</td>
								<td>
									<textarea name="txaObservacion" id="txaObservacion" cols="80" rows="3" maxlength="250" class="boxlargo element-required"></textarea>
									<span class="clsObligatorio">&nbsp;&nbsp;&nbsp;</span>
						    	</td>
							</tr>
							<tr>
								<td style="text-align:center;" colspan="2">
									<input type="button" name="btnGuardar" id="btnGuardar" value="Guardar" onclick="guardar();"/>
								</td>
							</tr>
					  	</table>
					</td>
					<td class="cuerpo_de" >&nbsp;</td>
				</tr>
				<tr>
					<td class="cuerpo_iz" >&nbsp;</td>
					<td class="cuerpo_ce" >&nbsp;</td>
					<td class="cuerpo_de" >&nbsp;</td>
				</tr>
				<tr>
					<td class="abajo_iz" >&nbsp;</td>
					<td class="abajo_ce" ></td>
					<td class="abajo_de" >&nbsp;</td>
				</tr>
			</table>
		</center>
	</body>
</html>
