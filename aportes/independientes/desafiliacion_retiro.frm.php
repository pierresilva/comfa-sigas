<?php	
	date_default_timezone_set("America/Bogota");
	$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
	include_once  $root;
	include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';
	
	$url=$_SERVER['PHP_SELF'];
	include_once $raiz.DIRECTORY_SEPARATOR.'phpComunes'.DIRECTORY_SEPARATOR.'auditoria.php';
	auditar($url);
?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<title>::Desafiliaci&oacute;n | Retiro Afiliaci&oacute;n::</title>
		
		<link type="text/css" href="<?php echo URL_PORTAL; ?>newcss/Estilos.css" rel="stylesheet">
		<link type="text/css" href="<?php echo URL_PORTAL; ?>newcss/css.1.8/jquery-ui-1.8.17.custom.css" rel="stylesheet">
		<link type="text/css" href="<?php echo URL_PORTAL; ?>newcss/marco.css" rel="stylesheet">
		
		<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/1.6/jquery-1.6.2.min.js"></script>
		<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/1.6/jquery-ui-1.8.16.custom.min.js"></script>
		<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/comunes.js"></script>
		
		<script type="text/javascript" src="js/desafiliacion_retiro.js"></script>
	</head>
	<body>
		<table width="60%" border="0" align="center" cellpadding="0" cellspacing="0">
			<tbody>
		  	<!-- ESTILOS SUPERIOR TABLA-->
		  	<tr>
			    <td width="13" height="29" class="arriba_iz">&nbsp;</td>
			    <td class="arriba_ce">
			    	<span class="letrablanca">::&nbsp;Retiro de Afiliaci&oacute;n por Desafiliaci&oacute;n &nbsp;::</span>
			    </td>
			    <td width="13" class="arriba_de" align="right">&nbsp;</td>
		  	</tr> 
		  	<!-- ESTILOS ICONOS TABLA-->
		  	<tr>
		    	<td class="cuerpo_iz">&nbsp;</td>
		    	<td class="cuerpo_ce">&nbsp;</td>
		    	<td class="cuerpo_de">&nbsp;</td>
		  	</tr>
		  	<!-- ESTILOS MEDIO TABLA-->
		  	<tr>
		    	<td class="cuerpo_iz">&nbsp;</td>
		    	<td class="cuerpo_ce"></td>
		    	<td class="cuerpo_de">&nbsp;</td>
		  	</tr>  
		  	<!-- CONTENIDO TABLA-->
		  	<tr>
		   		<td class="cuerpo_iz">&nbsp;</td>
		   		<td class="cuerpo_ce">
					<table cellspacing="0" width="90%" border="0" class="tablero" align="center">
		      			<tbody>
		      				<tr>
			      				<td >Identificaci&oacute;n:</td>
								<td >
									<input type="text" name="txtIdentificacion" id="txtIdentificacion" class="box1 element-required"/>
									<input type="hidden" name="hidIdTrabajador" id="hidIdTrabajador"/>
									<input type="button" name="btnBuscar" id="btnBuscar" value="Buscar" />
									<span id="spaNombreTrabajador" style="font-weight: bold;"></span>
								</td>
			        		</tr>
			        		<tr><td colspan="2" style="text-align: center;"><b>Desafiliaciones</b></td></tr>
			    			<tr>
			      				<td colspan="2" align="center">
			      				<table class="tablero" id="tblDesafiliacion" align="center">
			      					<thead>
			      						<tr>
			      							<th>...</th>
			      							<th>Id</th>
			      							<th>Estado Desaf.</th>
			      							<th>Fecha Ingreso</th>
			      							<th>Fecha Retiro</th>
			      							<th>Fecha Desafiliaci&oacute;n</th>
			      						</tr>
			      					</thead>
			      					<tbody></tbody>
			      				</table>
			      				</th>
			        		</tr>
			        		<tr><td colspan="2">&nbsp;</td></tr>
			        		<tr>
			      				<td >Fecha Retiro:</td>
								<td >
									<input type="text" name="txtFechaRetiro" id="txtFechaRetiro" class="boxfecha element-required" readonly="readonly" />
									<span class="clsObligatorio">&nbsp;&nbsp;&nbsp;</span>
								</td>
			        		</tr>
			        		<tr>
			      				<td >Motivo Retiro:</td>
								<td >
									<select name="cmbIdMotivoRetiro" id="cmbIdMotivoRetiro" class="boxlargo element-required">
									</select>
									<span class="clsObligatorio">&nbsp;&nbsp;&nbsp;</span>
								</td>
			        		</tr>
			        		<tr>
			        			<td colspan="2" style="text-align: center;">
			        				<input type="button" name="btnGuardar" id="btnGuardar" value="Guardar" />
			        			</td>
			        		</tr>
		      			</tbody>
		      		</table>
					</td>
		    		<td class="cuerpo_de">&nbsp;</td>
		  		</tr>		  
		  		<!-- ESTILOS PIE TABLA-->
		  		<tr>
		    		<td class="abajo_iz" >&nbsp;</td>
		    		<td class="abajo_ce" ></td>
		    		<td class="abajo_de" >&nbsp;</td>
		  		</tr>		  
			</tbody>
		</table>



		<!-- FORMULARIO OBSERVACIONES-->
		<div name="div-observaciones-tab" style="display:none" title="Observaciones de modificaci&oacute;n">
			<table class="tablero">
				<tr>
					<td>Usuario</td>
					<td colspan="3" >
						<input name="usuarioObs" id="usuarioObs" class="box1" disabled="disabled" value="<?php echo $_SESSION['USUARIO']?>" /></td>
				</tr>
				<tr>
					<td>Observaciones</td>
					<td colspan="3" >
						<textarea name="observacionGrupo" id="observacionGrupo" cols="45" rows="5" class="boxlargo"></textarea></td>
				</tr>
			</table>
			<div class="ui-state-highlight ui-corner-all" style="margin: 9px auto; display:none;padding: 6px; text-align: center; border: 1px solid rgb(255, 255, 0); width:300px" id="rtaObservacion"><span class="ui-icon ui-icon-info" style="float: left; margin-right: 0.3em;"></span>Observaci&oacute;n guardada con &eacute;xito.</div>
		</div>
	</body>
</html>