<?php
/**
 * Script de la logica del formulario 
 *
 * @author Oscar
 * @version 0
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
include_once $raiz.DIRECTORY_SEPARATOR.'aportes'.DIRECTORY_SEPARATOR.'independientes'.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'control_notificacion.class.php';

/**
 * Contiene los datos de retorno al formulario
 * @var array
 */
$arrMensaje = array('error'=>0,'data'=>null);

/*Verificar si accion existe*/
$accion = isset($_POST['accion'])?$_POST['accion']:'';

$usuario = $_SESSION["USUARIO"];

switch($accion){
     case 'U':
        $datos = $_POST['datos'];
        $datoEstadoNotif = array("id_estado"=>0,"id_notificacion"=>0,"fecha_estado"=>null);
        $datoEstadoNotif["id_estado"] = $datos["id_estado"];
        $datoEstadoNotif["fecha_estado"] = $datos["fecha_estado"];

        $objControlNotificacion = new ControlNotificacion;

        $banderaError=0;
        $objControlNotificacion->inicioTransaccion();
        //Actualizar el estado de la notificacion
        foreach ($datos["arrDatosNotificacion"] as $rowDatos){
            $datoEstadoNotif["id_notificacion"] = $rowDatos["id_notificacion"];
            if($objControlNotificacion->actualizar_estado_notificacion($datoEstadoNotif)==0){
                $banderaError++;
                break;
            }
        }
        
        //Guardar Observacion Empresa
        if($banderaError==0){
            $rsObservacion = $objControlNotificacion->guardar_observacion(array("id_trabajador"=>$datos["id_trabajador"]
                    ,"observacion"=>$datos["observacion"]),$usuario);
            if($rsObservacion==0)
                $banderaError++;
        }
        
        if($banderaError==0){
            $objControlNotificacion->confirmarTransaccion();
            $arrMensaje["error"] = 0;
        }else{
            //Error
            $objControlNotificacion->cancelarTransaccion();
            $arrMensaje["error"] = 1;
        }

        break;
    case 'S':

		$objDatosFiltro = $_POST["objDatosFiltro"];

		$objControlNotificacion = new ControlNotificacion;
		$resultado = $objControlNotificacion->buscar_notificacion( $objDatosFiltro );
        
        /*Verificar si existen datos*/
        if(count($resultado)>0){
            $arrMensaje["error"] = 0;
            $arrMensaje["data"] = $resultado;
        }
    break;
}

echo ($accion != '') ? json_encode($arrMensaje) : "";

?>