<?php 
/**
 * Script de la logica del formulario 
 *
 * @author Oscar
 * @version 0
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once '../clases' . DIRECTORY_SEPARATOR . 'desafiliacion.class.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'observaciones.class.php';

/**
 * Contiene los datos de retorno al formulario
 * @var array
 */
$arrMensaje = array('error'=>0,'data'=>null);

/*Verificar si accion existe*/
$accion = isset($_POST['accion'])?$_POST['accion']:'';

$usuario = $_SESSION["USUARIO"];

switch($accion){
    case 'U':
        $objDatos = $_POST['datos'];
        $objDesafiliacion = new Desafiliacion;
        $resultado = $objDesafiliacion->actua_afili_inact($objDatos);
        if($resultado>0){
            $arrMensaje["error"] = 0;
        }else{
            $arrMensaje["error"] = 1;
        }

        break;
    case 'ACTIVAR_AFILIACION':
        $objDatos = $_POST['datos'];
        $objDesafiliacion = new Desafiliacion;
        $resultado = $objDesafiliacion->activ_afili_inact($objDatos['id_desafiliacion'],$usuario);
        if($resultado>0){
            $arrMensaje["error"] = 0;
        }else{
            $arrMensaje["error"] = 1;
        }
        break;
    case 'S':

		$objDatosFiltro = $_POST["objDatosFiltro"];

		$objDesafiliacion = new Desafiliacion;
		$resultado = $objDesafiliacion->consultar( $objDatosFiltro );
        
        /*Verificar si existen datos*/
        if(count($resultado)>0){
            $arrMensaje["error"] = 0;
            $arrMensaje["data"] = $resultado;
        }
    break;
}

echo ($accion != '') ? json_encode($arrMensaje) : "";

?>