var uri = "",
	uriLogCausalAjuste = "",
	uriBuscaDetalDefin = "",
	uriBuscarPersona = "";

$(function(){
	uri = src();
	uriLogDesafiliacion = uri + "aportes/independientes/logica/desafiliacion.log.php";
	uriBuscaDetalDefin = uri + "phpComunes/buscaDetalDefin.php";
	uriBuscarPersona = uri + "phpComunes/buscarPersona.php";

	$("#btnBuscar,#btnGuardar").button();
	$("#btnBuscar").click(buscar);
	$("#btnGuardar").click(guardar);

	//Activo los detapicker en el formulario
	datepickerC("FECHA","#txtFechaRetiro");

	// Control para el motivo del retiro
	ctrMotivoRetiro();
});

function nuevo(){
	limpiarCampos2("#txtIdentificacion,#hidIdTrabajador,#txtFechaRetiro"
		+",#cmbIdMotivoRetiro,#spaNombreTrabajador,#tblDesafiliacion tbody");
}

function ctrMotivoRetiro(){
	var data = fetchMotivoRetiro({codigo_definicion:"motiv_retir_afili"});
	var htmlOption = "<option value=''>--Seleccionar--</option>";
	$.each(data,function(i,row){
		htmlOption += "<option value='"+row.iddetalledef+"'>"+row.detalledefinicion+"</option>";
	});

	$("#cmbIdMotivoRetiro").html(htmlOption);
}

function fetchMotivoRetiro(objFiltro){
	var resultado = null;
	
	$.ajax({
		url:uriBuscaDetalDefin,
		type:"POST",
		data:{objDatosFiltro:objFiltro},
		dataType: "json",
		async:false,
		success:function(data){
			resultado = data;
		}
	});
	
	return resultado;
}

function fetchPersona(identificacion){
	var resultado = null;
	
	$.ajax({
		url:uriBuscarPersona,
		type:"POST",
		data:{v0:identificacion},
		dataType: "json",
		async:false,
		success:function(data){
			resultado = data;
		}
	});
	
	return resultado;
}

function fetchDesafiliacion(objFiltro){
	var resultado = null;
	
	$.ajax({
		url:uriLogDesafiliacion,
		type:"POST",
		data:{accion:'S',objDatosFiltro:objFiltro},
		dataType: "json",
		async:false,
		success:function(data){
			resultado = data;
		}
	});
	
	return resultado;
}

function buscar(){
	var identificacion = $("#txtIdentificacion").val().trim();
	if(identificacion==0){
		 nuevo();
		 return;	
	}

	var dataPersona = fetchPersona(identificacion);
	if(dataPersona==0){
		alert("ERROR al consultar los datos de la persona!!");
		return false;
	}

	var idPersona = dataPersona[0].idpersona;
	var accion = 'S';
	var objDatos = {
			id_trabajador: idPersona
	};

	var data = fetchDesafiliacion(objDatos);
	if(data.data){
		data = data.data;
		var htmlDesafiliacion = "";
		$.each(data,function(i,row){
			htmlDesafiliacion += "<tr><td><input type='radio' name='radIdAfiliacion' value='"+row.id_afiliacion_inactiva+"'/></td>"
					+ "<td>"+row.id_desafiliacion+"</td>"
					+ "<td>"+row.estado+"</td>"
					+ "<td>"+row.fechaingreso+"</td>"
					+ "<td>"+row.fecharetiro+"</td>"
					+ "<td>"+row.fecha_sistema+"</td></tr>";
		});

		$("#tblDesafiliacion tbody").html(htmlDesafiliacion);
		$("#hidIdTrabajador").val(idPersona);
		$("#spaNombreTrabajador").html(dataPersona[0].nombrecorto);
	}else{
		alert("No existen datos!!");
		nuevo();
	}
}


function guardar(){
	
	//Verificar campos obligatorios
	if(validateElementsRequired()) return false;
	
	if($(":radio[name='radIdAfiliacion']:checked").length==0){
		alert("Debe indicar la desafiliacion!!");
		return false;
	}
	var idAfiliacionInactiva = $(":radio[name='radIdAfiliacion']:checked").val().trim();
	var idIdTrabajador = $("#hidIdTrabajador").val().trim();
	var objDatos = {
			id_trabajador: idIdTrabajador
			, id_afiliacion_inactiva: idAfiliacionInactiva
			, fecha_retiro: $("#txtFechaRetiro").val().trim()
			, id_motivo_retiro: $("#cmbIdMotivoRetiro").val().trim()
	};
	
	$.ajax({
		url:uriLogDesafiliacion,
		type:"POST",
		data:{accion:'U',datos:objDatos},
		dataType: "json",
		async:false,
		success:function(data){
			if(data.error==0){
				alert("La Afiliacion se retiro correctamente!!");
				nuevo();
				observacionesTab(idIdTrabajador,1);
				return true;
			}
			
			alert("ERROR al actualizar la afiliacion!!");
		}
	});
}
