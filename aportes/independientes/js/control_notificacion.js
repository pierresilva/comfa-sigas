var uri = "",
	uriLogControlNotificacion = "";

$(function(){ 
	uri = src();
	uriLogControlNotificacion = uri + "aportes/independientes/logica/control_notificacion.log.php";

	$("#btnGuardar").button();
	datepickerC("FECHA","#txtFechaEstado");
	
	$("#txtNumeroIdentificacion").blur(function(){
		nuevo();
		
		var identificacion = $("#txtNumeroIdentificacion").val().trim();
		if(!identificacion)
			return false;
		
		var datosTrabajador = buscarDatosPersona(identificacion);				
		if(typeof datosTrabajador != "object"){
			$("#lblRazonSocial").addClass("classTextError").html(
					"No existe el afiliado con la identificacion " + identificacion);
			return false;
		}
		$("#hidIdTrabajador").val(datosTrabajador.idpersona);
		$("#lblRazonSocial").html(datosTrabajador.pnombre+" "+datosTrabajador.snombre+" "+datosTrabajador.papellido+" "+datosTrabajador.sapellido);
	});
	
	$("#cmbCartaNotificacion").change(function(){
		var idPersona = $("#hidIdTrabajador").val().trim();
		var idCartaNotificacion = $("#cmbCartaNotificacion").val();
		
		$("#tdMensajeError,#tBody").html("");
		if(!$(this).val())return false;
		
		//Validar campos
		if(!idPersona){
			$("#tdMensajeError").html("Faltan datos, favor buscar de nuevo el afiliado");
			return false;
		}
		$("#cmbCartaNotificacion").removeClass("ui-state-error");
		if(!idCartaNotificacion){
			$("#cmbCartaNotificacion").addClass("ui-state-error");
			return false;
		}
		
		var objDatos = {
				id_carta_notificacion:idCartaNotificacion,
				id_trabajador:idPersona
			};
		//Buscar datos de la notificacion
		var objDatosNotificacion = buscarDatosNotificacion(objDatos);
		
		//Adiccionar los datos al formulario
		if(typeof objDatosNotificacion == "object"){
			var trHtml = "";
			$.each(objDatosNotificacion,function(i,row){
				trHtml += "<tr>" +
						"<td><input type='checkbox' name='chkIdNotificacion' id='chkIdNotificacion' value='"+row.id_notificacion+"' class='clsIdNotificacion'/></td>" +
						"<td>"+row.carta_notificacion+"</td>" +
						"<td>"+row.estado_notificacion+"</td>" +
						"<td>"+row.fecha_sistema+"</td>" +
						"<td>"+row.fecha_estado+"</td>"+
						"<td>"+row.informacion+"</td>"+
						"<td><label style='cursor:pointer' onClick='generarNotificacionPDf("+row.id_notificacion+");'><img src='../../imagenes/icono_pdf.png' width='24' height='24'></label></td></tr>";
			});
			$("#tBody").html(trHtml);
		}else{
			//No se encontraron datos
			$("#tdMensajeError").html("No existen notificaciones");
		}	
	});
});

function generarNotificacionPDf(idNotificacion){
	var data = "codigo_carta=GENERAR_NOTIFICACION_PDF&id_notificacion="+idNotificacion;
	
	var url=URL+"centroReportes/aportes/independientes/generar_carta.log.php?"+data;
	window.open(url,"_BLANK");
}

/**
 * Busca los datos de la empresa de acuerdo al nit
 * @param nit
 * @returns Datos de la empresa
 */
function buscarDatosPersona(identificacion) {
	var data;
	$.ajax({
		url:URL+"phpComunes/buscarPersona.php",	
		type:"POST",
		data:{v0:identificacion},
		dataType:"json",
		async:false,
		success:function(datos){
			if(typeof datos == "object")
				data = datos[0];
		}
	});
	return data;
}

/**
 * Busca los datos de la notificacion
 * @param nit
 * @returns Datos de la notificacion
 */
function buscarDatosNotificacion(objDatos) {
	var data;
	$.ajax({
		url:uriLogControlNotificacion,	
		type:"POST",
		data:{accion:"S",objDatosFiltro:objDatos},
		dataType:"json",
		async:false,
		success:function(datos){
			if(datos.data)
				data = datos.data;
		}
	});
	return data;
}

function nuevo(){
	limpiarCampos2("#hidIdTrabajador,#lblRazonSocial,#cmbCartaNotificacion," +
			"#tdMensajeError,#tBody,#cmbEstado,#txaObservacion,#txtFechaEstado");
}

function guardar(){
	//Verificar campos obligatorios
	if(validateElementsRequired()) return false;

	//Validar checkbox
	if($(".clsIdNotificacion:checkbox:checked").length==0){
		alert("No hay notificaciones chequeadas");
		return false;
	}
	
	//Preparar datos
	var objDatos = {
			id_estado:$("#cmbEstado").val(),
			id_trabajador:$("#hidIdTrabajador").val(),
			fecha_estado:$("#txtFechaEstado").val(),
			observacion:$("#txaObservacion").val().trim()
		};
	
	var objDatoNotificacion = [];
	$(".clsIdNotificacion:checkbox:checked").each(function(i,row){
		objDatoNotificacion[objDatoNotificacion.length] = {
				id_notificacion:row.value
		};
	});
	
	objDatos.arrDatosNotificacion = objDatoNotificacion;
	
	$.ajax({
		url:uriLogControlNotificacion,	
		type:"POST",
		data:{accion:"U",datos:objDatos},
		dataType:"json",
		async:false,
		success:function(datos){
			if(datos.error==0){
				alert("El control de la notificacion se guardo correctamente");
				$("#txtNumeroIdentificacion").val("");
				nuevo();
			}else{
				alert("Error al guardar los datos");
			}
		}
	});
}
