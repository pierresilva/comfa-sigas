<?php
set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $_SESSION['RAIZ'] . DIRECTORY_SEPARATOR . 'pdo/afiliacion.class.php';

$fechaActual = new DateTime();
$afiliacionArray = json_decode($_REQUEST['v0']);	// Array contenedor de las afiliaciones
$campo1 = $_REQUEST['v1'];  						// motivoretiro
$campo2 = $fechaActual->format("Y-m-d");			// fecha
$campo3 = $_SESSION['USUARIO'];						// usuario 

$cont=0;
$cant=count($afiliacionArray);

for($i=0; $i < $cant; $i++){	
	$campo0 = $afiliacionArray[$i]->idformulario;
	$campo4 = $afiliacionArray[$i]->tipoafiliacion;
	$campo5 = $afiliacionArray[$i]->idempresa;
	$campo6 = $afiliacionArray[$i]->idpersona;
	
	$objeto = new Afiliacion();
	$res=json_encode($objeto->spInactivarAfiliacion($campo0,$campo1,$campo2,$campo3,$campo4,$campo5,$campo6));	
	
	if($res==2 || $res==3)
		$cont++;
}

echo $cont;
?>			
