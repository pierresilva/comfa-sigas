<?php
 set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $_SESSION['RAIZ'] . DIRECTORY_SEPARATOR . 'pdo/radicacion.class.php';
$campos= json_decode($_REQUEST['v0']);

$fechaActual = new DateTime();
$hora = $fechaActual->format("H");
$minuto = $fechaActual->format("i");
$validahora=$hora.":".$minuto;

if($validahora>='17:30'){	
	$fechaActual->modify('+1 day');
}

$campos->fecharadicacion=$fechaActual->format('Ymd');

$objeto = new Radicacion();
echo $objeto->guardarRadicacion($campos);
?>