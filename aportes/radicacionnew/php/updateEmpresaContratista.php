<?php
 set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $_SESSION['RAIZ'] . DIRECTORY_SEPARATOR . 'pdo/empresa.class.php';

/*
 {v0:nEmpresa.idempresa, v1:nEmpresa.estado, v2:nEmpresa.contratista,v3:nEmpresa.claseAportante, v4:nEmpresa.tipoaportante, v5:nEmpresa.idtipoafiliacion}
*/

$campos=array();
$campos[0]  = $_REQUEST['v0'];  					//idempresa
$campos[1]  = $_REQUEST['v1'];  					//estado
$campos[2]  = "'".$_REQUEST['v2']."'";  			//contratista
$campos[3] = "'".$_SESSION['USUARIO']."'";

$objeto = new Empresa();
echo $objeto->updateEmpresaContratista($campos);
?>