<?php
ini_set('display_errors','0');
date_default_timezone_set("America/Bogota");
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$usuario=$_SESSION['USUARIO'];
$fecha=date("m/d/Y");
include_once $_SESSION['RAIZ'] . DIRECTORY_SEPARATOR . 'pdo/persona.class.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

$idp  = $_REQUEST['v0'];
$ob  = $_REQUEST['v1'];
$tipo  = $_REQUEST['v2'];

$objeto = new Persona();
echo json_encode($objeto->insertarAuditoria($idp,$ob,$tipo));


?>