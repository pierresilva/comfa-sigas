<?php
set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $_SESSION['RAIZ'] . DIRECTORY_SEPARATOR . 'pdo/documento.class.php';

$documentosArray = json_decode($_REQUEST['v0']);	// Array contenedor de las afiliaciones
$campo1 = $_REQUEST['v1'];  						// idRadicacion
$campo2 = $_REQUEST['v2'];							// idPersona
$campo3 = $_REQUEST['v3'];							// programa
$campo4 = $_SESSION['USUARIO'];						// usuario 

$cont=0;
$objeto = new Documento();
$cant=count($documentosArray);
$objeto->inicioTransaccion();
$idregistro=$objeto->guardarDocumento($campo1, $campo2, $campo4);

if(intval($idregistro)>0){
	for($i=0; $i < $cant; $i++){	
		$documentosArray[$i]->idregistro=$idregistro;
		$documentosArray[$i]->programa=$campo3;
		$documentosArray[$i]->usuario=$campo4;
		
		$res=$objeto->guardarDetalleDocumento($documentosArray[$i]);
		
		if(intval($res)){
			$cont++;
		} else {$cont=0;
			break;
		}
	}
}

if($cont==$cant){ $objeto->confirmarTransaccion(); } 
else { $objeto->cancelarTransaccion(); }

echo $cont;
?>			
