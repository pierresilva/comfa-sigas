<?php
set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $_SESSION['RAIZ'] . DIRECTORY_SEPARATOR . 'pdo/grupoFamiliar.class.php';
$campos= json_decode($_REQUEST['v0']);

$objeto = new GrupoFamiliar();

$objeto->inicioTransaccion();

if($objeto->updateDisolverConvivencia($campos)==0){ 
	$objeto->cancelarTransaccion(); 
	echo 0;
} else {
	$campos->idtrabajador;
	$campos->idconyuge=$campos->idbeneficiario;
	$campos->idbeneficiario; 
	$campos->idconyuge;
	
	if($objeto->updateDisolverConvivencia($campos)==0){
		$objeto->cancelarTransaccion(); 
		echo 0;
	} else {
		$objeto->confirmarTransaccion();
		echo 1;
	}	
}
?>
