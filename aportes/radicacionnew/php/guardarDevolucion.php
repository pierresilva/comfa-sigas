<?php
 set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $_SESSION['RAIZ'] . DIRECTORY_SEPARATOR . 'pdo/radicacion.class.php';
$campos= json_decode($_REQUEST['v0']);

$fechaActual = new DateTime();
$hora = $fechaActual->format("H");
$minuto = $fechaActual->format("m");
if(intval($hora > 17) || (intval($hora == 17) && intval($minuto) >= 30)){	
	$fechaActual->modify('+1 day');
}
$campos->fecharadicacion=$fechaActual->format('Ymd');
$campos->usuarioanula=$_SESSION['USUARIO'];

$objeto = new Radicacion();
echo $objeto->guardarRadicacion($campos);
?>