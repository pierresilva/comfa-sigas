<?php
set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $_SESSION['RAIZ'] . DIRECTORY_SEPARATOR . 'pdo/grupoFamiliar.class.php';
$campos= json_decode($_REQUEST['v0']);

$objeto = new GrupoFamiliar();

$conyuges = $objeto->buscarConyugeConvive($campos->idtrabajador);
$cont=0;

if(!is_numeric($conyuges)){
	$objeto->inicioTransaccion();
	foreach ($conyuges as $rowConyuge){ $tmp=$rowConyuge["idconyuge"];
		$campos->idbeneficiario=$tmp; $campos->idconyuge=$tmp;
		if($objeto->updateDisolverConvivencia($campos)==0){ $objeto->cancelarTransaccion(); break; }
		if($objeto->updateInactivarBeneficiarioHijastro($campos)==0){ $objeto->cancelarTransaccion(); break; }
		
		$tmp=$campos->idtrabajador;
		$campos->idtrabajador=$campos->idbeneficiario;
		$campos->idbeneficiario=$tmp; $campos->idconyuge=$tmp;
		
		if($objeto->updateDisolverConvivencia($campos)==0){ $objeto->cancelarTransaccion(); break; }
		if($objeto->updateInactivarBeneficiarioHijastro($campos)==0){ $objeto->cancelarTransaccion(); break; }
		else { $objeto->confirmarTransaccion(); $cont++; }
	}	
}

echo $cont;
?>
