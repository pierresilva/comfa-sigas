<?php
/* autor:       orlando puentes
 * fecha:       30/08/2010
 * objetivo:     
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$idpersona= $_REQUEST['v0'];
include_once $raiz.DIRECTORY_SEPARATOR . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'p.tarjeta.class.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';
$objClase=new Tarjeta();
$consulta = $objClase->buscar_tarjeta($idpersona);
$row=mssql_fetch_array($consulta);
$bono=$row['bono'];
$cont=0;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Tarjeta</title>
<link href="<?php echo URL_PORTAL; ?>css/sorterStyle.css" rel="stylesheet" type="text/css" media="screen" />
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/comunes.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/jquery.tablesorter.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/jquery.tablesorter.pager.js"></script>
<script type="text/javascript">
$(document).ready(function(){	
	$("#cargues").tablesorter();
	refrescar();
});
  
function refrescar(){
	$("#cargues tbody tr").remove();
	var num=$("#nRegistrosTarjeta").val();
	var idp=$("#txtIdp").val();
	$.ajax({
		url:URL+'phpComunes/buscarCargues.php',
		type:"POST",
		dataType:"json",
		data: {v0:idp,v1:num},
		async:false,
		success:function(data){
			if(data==0){
				alert("NO existen cargues.");
				return false;
			}
			$.each(data, function(i,n){ 
				var row = (data==false)? "-" : data.ultimo;
				var no=data.length;
				$("#cargues caption span").html(no+" registros encontrados."); 
				$("#cargues tbody").append("<tr><td>"+n.periodogiro+"</td><td>"+n.tipopago+"</td><td>"+n.tercero+"</td><td>"+n.embargado+"</td><td style=text-align:right>"+formatCurrency(n.valor)+"</td><td>"+n.procesado+"</td><td style=text-align:center>"+n.fechasistema+"</td></tr>");  
			});//end each emrpesa Id	<td>"+lic_maternidad+"</td>
			//Actualizar Tabla de sorter 
			$("#cargues").trigger("update");
			$("#cargues tbody tr:odd").addClass("evenrow");	
		}
	});//ajax
}
</script>
</head>
<body>
 <h4>Tarjeta - Saldo - Cargues - Movimientos</h4>
<center>
<table width="60%" border="0" style="border:1px dashed #CCC" >
<tr>
<td width="65%"><label style="font-size:16px; color:#333; font-weight:bold">Valor disponible para retiro</label></td>
<td width="32%" style="text-align:right"><label style="font-size:16px; color:#F00; font-weight:bold;">
<?php echo number_format( $row['saldo'] ); ?></label></td>
</tr>
<tr>
  <td>&nbsp;</td>
  <td>&nbsp;</td>
</tr>
<tr>
  <td><label style="font-size:16px; color:#333; font-weight:bold">N&uacute;mero de TARJETA</label></td>
  <td style="text-align:right"><label style="font-size:16px; color:#F00; font-weight:bold">
  <?php echo $row['bono']; ?>&nbsp;</label></td>
</tr>
<tr>
  <td>&nbsp;</td>
  <td>&nbsp;</td>
</tr>
<tr>
  <td><label style="font-size:16px; color:#333; font-weight:bold">Estado de TARJETA</label></td>
  <td style="text-align:right"><label style="font-size:16px; color:#F00; font-weight:bold">
  <?php echo $row['estado']; ?>&nbsp;(<?php echo $row["detalledefinicion"]; ?>)</label></td>
</tr>
</table>
<label class="Rojo">Cargues a la Tarjeta</label>
</center>
<br />
<table width="100%" border="0" cellspacing="0" class="sortable hover" id="cargues">
<caption style="text-align:left; padding:5px; font-size:11px;"><span></span></caption>
  <thead>
  <tr>
<th class="head"><h3><strong>Periodo</strong></h3></th>
<th class="head"><h3><strong>Tipo</strong></h3></th>
<th class="head"><h3><strong>Tercero</strong></h3></th>
<th class="head"><h3><strong>Embargado</strong></h3></th>
<th class="head"><h3><strong>Valor</strong></h3></th>
<th class="head"><h3><strong>Proc</strong></h3></th>
<th class="head"><h3><strong>Fecha</strong></h3></th>
</tr>
</thead>
<tbody>

</tbody>
</table>
<div id="div-registros">N&uacute;mero de Registros: 
<select name="nRegistrosTarjeta" id="nRegistrosTarjeta" onchange="refrescar();">
	<option selected="selected"  value="10" >10</option>
	<option value="20">20</option>
	<option value="30">30</option>
	<option  value="40">40</option>
    <option  value="50">50</option>
</select>
</div> 
<input type="hidden" name="txtIdp" id="txtIdp" value="<?php echo $idpersona; ?>" />
<input type="hidden" name="txtIdb" id="txtIdb" value="<?php echo $bono; ?>" />
</body>
</html>
