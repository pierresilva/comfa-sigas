<?php
set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'clases' .  DIRECTORY_SEPARATOR . 'defuncion.class.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';
$idtrabajador= $_REQUEST['v0'];
$flagContain=isset($_REQUEST['v99'])?false:true;	//Esta contenido por algun otro formulario?

$objDefuncion=new Defuncion();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>:: Descuentos ::</title>
<link href="<?php echo URL_PORTAL; ?>newcss/sorterStyle.css" rel="stylesheet" type="text/css" media="screen" />
<?php if($flagContain==false): ?>
<link type="text/css" href="<?php echo URL_PORTAL; ?>newcss/Estilos.css" rel="stylesheet"/>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/1.6/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/comunes.js"></script>
<?php endif; ?>

<script language="javascript">
	var URL=src();

	function buscarDetalleDefuncion(idtrabajador,idbeneficiario){
		$("#div-datosDefuncion").dialog({
			title:"Cuotas Monetarias",
			width: 650,
			modal: true,
			draggable:true,
			//zIndex:1000,
			resizable:false,
			closeOnEscape:true,
			open: function(){
				$(this).html('');
				$(this).load('buscarDetalleDefuncion.php',{v0:idtrabajador,v1:idbeneficiario},function(data){
					$(this).html(data);
				});
			},
			close: function() {
				$(this).dialog("destroy");
			}
		});
	}//end function
</script>

<script language="javascript"></script>
</head>
<body>	
	<h4>DEFUNCIONES</h4>	
	<?php 
		$conDefuncion=$objDefuncion->buscarDefunciones($idtrabajador);
		if(!is_numeric($conDefuncion)){	
			$cont=0;		
			foreach ($conDefuncion as $row){ 
				$fallecido=$row['Fpnombre']." ".$row['Fsnombre']." ".$row['Fpapellido']." ".$row['Fsapellido'];	
				$idfallecido=$row['Fidpersona'];			
				$cont++;
				?>
	<table width="100%" border="0" cellspacing="0" class="sortable hover" id="tDefuncion">
		<thead>
			<tr>
				<th class="head"><strong>idFallecido</strong></th>
				<th class="head"><strong>N&uacute;mero</strong></th>
			    <th class="head"><strong>Nombre</strong></th>
			    <th class="head"><strong>Fecha Fallecido</strong></th>
		    </tr>
		</thead>
		<tbody>
			<tr>
				<td><?php echo $idfallecido; ?>&nbsp;</td>
				<td><?php echo number_format($row['Fidentificacion']); ?>&nbsp;</td>
				<td><?php echo $fallecido ?>&nbsp;</td>
				<td><?php echo $row['fechadefuncion']; ?>&nbsp;</td>
			</tr>
		</tbody>
	</table>
	<table width="80%" border="0" cellspacing="0" class="tablero" id="tDetalleDefuncion">
		<tr>
			<th>IdRadicaci&oacute;n</th>
			<th>Fecha Grabaci&oacute;n</th>
			<th>IdTercero</th>
		    <th>Tercero</th>
	    	<th>IdBeneficiario</th>
	    	<th>Beneficiario</th>		    
		</tr>
			<?php 
				$beneficiarios=$objDefuncion->buscarBeneficiarioDefuncion($idtrabajador,$idfallecido);
				if(!is_numeric($beneficiarios)){
					foreach ($beneficiarios as $ben){
						$tercero=$ben['Tpnombre']." ".$ben['Tsnombre']." ".$ben['Tpapellido']." ".$ben['Tsapellido'];
						$beneficiario=$ben['Bpnombre']." ".$ben['Bsnombre']." ".$ben['Bpapellido']." ".$ben['Bsapellido'];					
				?>
		<tr>
			<td><?php echo $ben['idradicacion']; ?>&nbsp;</td>
			<td><?php echo $ben['fechasistema']; ?>&nbsp;</td>
			<td><?php echo $ben['Tidpersona']; ?>&nbsp;</td>
			<td><?php echo $tercero; ?>&nbsp;</td>
			<td style="cursor:pointer;text-decoration:none; font-weight: bold" onclick="buscarDetalleDefuncion('<?php echo $idtrabajador; ?>','<?php echo $ben['Bidpersona']; ?>');" ><?php echo $ben['Bidpersona']; ?>&nbsp;</td>
			<td><?php echo $beneficiario; ?>&nbsp;</td>
		</tr>
					<?php } 
				} ?>
	</table>		
			<?php }
		} else {
			echo "<label class=Rojo>El trabajor no tiene Defunciones!</label>";
		} ?>
	<br />
	<div id="div-datosDefuncion" style="display:none"></div>	
</body>
</html>