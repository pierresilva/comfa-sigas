<?php
/* autor:       orlando puentes
 * fecha:       20/07/2010
 * objetivo:    
 */
 
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'clases'. DIRECTORY_SEPARATOR .'p.subsidios.class.php';

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Subsidios</title>
<link href="<?php echo URL_PORTAL; ?>css/sorterStyle.css" rel="stylesheet" type="text/css" media="screen" />
<!--<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/comunes.js"></script>-->
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/jquery.tablesorter.js">
//destruir el dialog del tab subsidio en la primera tabla de titulo "Subsidio en Revisión"   
cerrarDialogCausal();</script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/tableSorterDESC.js"></script>
<script type="text/javascript">		
	var sorterHistoricoCM = new TINYDESC.tableDESC.sorter("sorterHistoricoCM");
	sorterHistoricoCM.head = "head";
	sorterHistoricoCM.asc = "asc";
	sorterHistoricoCM.desc = "desc";
	sorterHistoricoCM.even = "evenrow";
	sorterHistoricoCM.odd = "oddrow";
	sorterHistoricoCM.evensel = "evenselected";
	sorterHistoricoCM.oddsel = "oddselected";
	sorterHistoricoCM.paginate = true;
	sorterHistoricoCM.pagesize = (5);
	sorterHistoricoCM.currentid = "currentpageHistoricoCM";
	sorterHistoricoCM.limitid = "pagelimitHistoricoCM";
	sorterHistoricoCM.init("tblHistoricoCM",2);
</script>
</head>

<body>
<label class="Rojo">Subsidio en Revisi&oacute;n</label>
<table width="100%" border="0" cellspacing="0" class="sortable hover" id="actual">
<caption style="text-align:left; padding:5px; font-size:11px;"><span></span></caption>
<tr>
<th class="head"><h3>NIT</h3></th>
<th class="head"><h3>Causado</h3></th>
<th class="head"><h3>Embargo</h3></th>
<th class="head"><h3>Defunci&oacute;n</h3></th>
<th class="head"><h3>Relaci&oacute;n</h3></th>
<th class="head"><h3>Ide Benef</h3></th>
<th class="head"><h3>Valor</h3></th>
<th class="head"><h3>Descuento</h3></th>
<th class="head"><h3>Causal</h3></th>
<th class="head"><h3>TG</h3></th>
<th class="head"><h3>Estado</h3></th>
</tr>
<tbody>
<?php  
$idp=$_REQUEST['v0'];
$objClase2=new Subsidios();
$consulta2 = $objClase2->buscar_pre($idp);
$contador = 0;
while($row=mssql_fetch_array($consulta2)){
$con=1;
echo "<tr id='trSubsidioRevicion$contador'>"
?>
	<tr>	
	<td id="tdNit"><?php if($_SESSION['IDROL']==8 || $_SESSION['IDROL']==1 ){ echo "<label style='cursor:pointer'onclick='cambiarNit(\"".$row['nit']."\",\"".$row['idregistro']."\",\"trSubsidioRevicion".$contador."\");'>".$row['nit']."</label>";}else{ echo $row['nit'];} ?></td>
    <td style="text-align:center"><?php echo $row['periodo']; ?></td>
    <td style="text-align:center"><?php echo $row['embarga']; ?></td>
    <td><?php echo $row['defuncion']; ?></td>
    <td><?php echo $row['parentesco']; ?></td>
    <td><?php echo $row['idbeneficiario']; ?></td>
    <td id="tdValorCuota"><?php echo number_format($row['valorcuota']); ?></td>
    <td><?php echo $row['valordescuento']; ?></td>
    <td id="tdCausal"><?php echo $row['causal']; ?></td>
	<td ><?php echo $row['flag2']; ?></td>
    <td id="tdEstado"><?php if($_SESSION['IDROL']==8 || $_SESSION['IDROL']==1 ){ echo "<label style='cursor:pointer'onclick='cambiarCausal(\"".$row['flag1']."\",\"".$row['idregistro']."\",\"trSubsidioRevicion$contador\");'>".$row['flag1']."</label>";}else{ echo $row['flag1'];} ?></td> 
  </tr>
  <?php $contador++;}?>
  </tbody>
</table>
<!--<div id="idDialogCausal" name='idDialogCausal' style="display:none">
  
</div>-->
<br />
<label class="Rojo">Subsidios periodo actual</label>
<table width="100%" border="0" cellspacing="0" class="sortable hover" id="actual">
<caption style="text-align:left; padding:5px; font-size:11px;"><span></span></caption>
<tr>
<th class="head"><h3>NIT</h3></th>
<th class="head"><h3>Causado</h3></th>
<th class="head"><h3>Proceso</h3></th>
<th class="head"><h3>Emb.</h3></th>
<th class="head"><h3>Num.</h3></th>
<th class="head"><h3>Relaci&oacute;n</h3></th>
<th class="head"><h3>Id Ben</h3></th>
<th class="head"><h3>Beneficiario</h3></th>
<th class="head"><h3>TipoP</h3></th>
<th class="head"><h3>Valor</h3></th>
<th class="head"><h3>Desc.</h3></th>
<!-- th class="head"><h3>Pagare</h3></th -->
<th class="head"><h3>TipoG</h3></th>
<th class="head"><h3>Proc.</h3></th>
<th class="head"><h3>Cargue</h3></th>
</tr>
<tbody>
<?php 
$idp=$_REQUEST['v0'];
$objClase2=new Subsidios();
$consulta2 = $objClase2->buscar_actual($idp);
while($row=mssql_fetch_array($consulta2)){
$nombres=$row['pnombre']." ".$row['papellido'];
$con=1;
?>
<tr>
	<td><?php echo $row['nit']; ?></td>
    <td style="text-align:center"><?php echo $row['periodo']; ?></td>
    <td style="text-align:center"><?php echo $row['periodoproceso']; ?></td>
    <td style="text-align:center"><?php echo $row['embargo']; ?></td>
    <td style="text-align:center"><?php echo $row['numche']; ?></td>
    <td><?php echo $row['detalledefinicion']; ?></td>
    <td><?php echo $row['idbeneficiario']; ?></td>
    <td><?php echo $nombres; ?></td>
    <td style="text-align:center"><?php echo $row['tipopago']; ?></td>
    <td><?php echo number_format($row['valor']); ?></td>
    <td><?php echo $row['descuento']; ?></td>
    <!-- td><?php echo $row['idpignoracion']; ?></td -->
    <td style="text-align:center"><?php echo $row['tipogiro']; ?></td>
    <td style="text-align:center"><?php echo $row['procesado']; ?></td>
    <td><?php echo $row['fechatarjeta']; ?></td>
  </tr>
  <?php }?>
  </tbody>
</table>
<br />
<label class="Rojo">Subsidios - Hist&oacute;rico</label>
<table width="100%" border="0" cellspacing="0" class="sortable hover" id="tblHistoricoCM">
<caption style="text-align:left; padding:5px; font-size:11px;"><span></span></caption>
<thead>
<tr>
<!-- th></th -->
<th class="head"><h3>NIT</h3></th>
<th class="head"><h3>Causado</h3></th>
<th class="head"><h3>Proceso</h3></th>
<th class="head"><h3>Emb</h3></th>
<th class="head"><h3>Relaci&oacute;n</h3></th>
<th class="head"><h3>Ide Benef</h3></th>
<th class="head"><h3>Beneficiario</h3></th>
<th class="head"><h3>TipoP</h3></th>
<th class="head"><h3>Valor</h3></th>
<th class="head"><h3>Desc</h3></th>
<!-- th class="head"><h3>Pagare</h3></th -->
<th class="head"><h3>TipoG</h3></th>
<th class="head"><h3>Anulado</h3></th>
<th class="head"><h3>Cargue</h3></th>
</tr>
</thead>
<tbody>
<?php 
$objClase2=new Subsidios();
$consulta2 = $objClase2->buscar_historico($idp,1000);
while($row=mssql_fetch_array($consulta2)){
$nombres=$row['pnombre']." ".$row['snombre']." ".$row['papellido']." ".$row['sapellido'];
$fanu=$row['fechaanula'];
$manu=$row['motivoAnulacion'];
?>
<tr>
	<!-- td><?php echo $con; ?></td -->
	<td><?php echo $row['nit']; ?></td>
    <td style="text-align:center"><?php echo $row['periodo']; ?></td>
    <td style="text-align:center"><?php echo $row['periodoproceso']; ?></td>
    <td style="text-align:center"><?php echo $row['embargo']; ?></td>
    <td><?php echo $row['detalledefinicion']; ?></td>
    <td><?php echo $row['idbeneficiario']; ?></td>
    <td><?php echo $nombres; ?></td>
    <td style="text-align:center"><?php echo $row['tipopago']; ?></td>
    <td><?php echo number_format($row['valor']); ?></td>
    <td><?php echo number_format($row['descuento']); ?></td>
    <!-- td><?php echo $row['idpignoracion']; ?></td -->
    <td style="text-align:center"><?php echo $row['tipogiro']; ?></td>
    <td><a class="dialogoEmergente" ><?php echo $fanu;  ?><span><?php echo $manu;  ?></span></a></td>
    <td><?php echo $row['fechatarjeta']; ?></td>   
  </tr>
  <?php $con++; }?>
</tbody>
</table>
<div id="controls">	
	<div id="perpage">
		<select onChange="sorterHistoricoCM.size(this.value)">
			<option value="5" selected="selected">5</option>
			<option value="10" >10</option>
			<option value="20">20</option>
			<option value="50">50</option>
			<option value="100">100</option>
			<option value="500">500</option>
		</select>
		<label>&nbsp;&nbsp;&nbsp;&nbsp;Registros Por P&aacute;gina</label>
	</div>
	<div id="navigation">
		<img src="<?php echo URL_PORTAL; ?>imagenes/imagesSorter/first.gif" width="16" height="16" alt="first Page" onClick="sorterHistoricoCM.move(-1,true)" />
		<img src="<?php echo URL_PORTAL; ?>imagenes/imagesSorter/previous.gif" width="16" height="16" alt="first Page" onClick="sorterHistoricoCM.move(-1)" />
		<img src="<?php echo URL_PORTAL; ?>imagenes/imagesSorter/next.gif" width="16" height="16" alt="first Page" onClick="sorterHistoricoCM.move(1)" />
		<img src="<?php echo URL_PORTAL; ?>imagenes/imagesSorter/last.gif" width="16" height="16" alt="Last Page" onClick="sorterHistoricoCM.move(1,true)" />
	</div>
	<div id="text">&nbsp;&nbsp;&nbsp;&nbsp;P&aacute;gina <label id="currentpageHistoricoCM"></label> de <label id="pagelimitHistoricoCM"></label></div>	
</div>
</body>

</html>
