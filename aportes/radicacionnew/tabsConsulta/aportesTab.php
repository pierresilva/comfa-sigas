<?php
set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'p.aportes.class.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';
$ide=(empty($_REQUEST['v0'])) ? 'NULL' : $_REQUEST['v0'];

$objAportes=new Aportes();
$result=$objAportes->buscar_aportes($ide);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Aportes</title>
<link href="<?php echo URL_PORTAL; ?>css/sorterStyle.css" rel="stylesheet" type="text/css" media="screen" />
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/tableSorterDESC.js"></script>
<script type="text/javascript">		
	var sorterAportesTrab = new TINYDESC.tableDESC.sorter("sorterAportesTrab");
	sorterAportesTrab.head = "head";
	sorterAportesTrab.asc = "asc";
	sorterAportesTrab.desc = "desc";
	sorterAportesTrab.even = "evenrow";
	sorterAportesTrab.odd = "oddrow";
	sorterAportesTrab.evensel = "evenselected";
	sorterAportesTrab.oddsel = "oddselected";
	sorterAportesTrab.paginate = true;
	sorterAportesTrab.pagesize = (5);
	sorterAportesTrab.currentid = "currentpageAportesTrab";
	sorterAportesTrab.limitid = "pagelimitAportesTrab";
	sorterAportesTrab.init("tblAportesTrab",2);
</script>
</head>
<body >
	<h4>Aportes <?php echo $ide; //$idec ????? ?></h4>
		<table width="100%" border="0" cellspacing="0" class="sortable hover" id="tblAportesTrab">
			<caption style="text-align:left; padding:5px; font-size:11px;"><span></span></caption>
			<thead>
				<tr>
					<th class="head"><h3>NIT</h3></th>
					<th class="head"><h3>Id aporte</h3></th>
					<th class="head"><h3>Periodo</h3></th>
					<th class="head"><h3>Nómina</h3></th>
					<th class="head"><h3>Aporte</h3></th>
					<th class="head"><h3>Trabajadores</h3></th>
					<th class="head"><h3>Ajuste</h3></th>
					<th class="head"><h3>Fecha pago</h3></th>
				</tr>
			</thead>
			<tbody>
				<?php
					$cont=0;
					while($consulta=mssql_fetch_array($result)){
						$cont++;
				?>	
					<tr>
						<td><?php echo $consulta['nit'];  ?></td>
					   	<td><?php echo $consulta['idaporte'];  ?></td>
					    <td style='text-align:center'><?php echo $consulta['periodo'];  ?></td>
					    <td style='text-align:right'><?php echo number_format($consulta['valornomina']);  ?></td>
					    <td style='text-align:right'><?php echo number_format($consulta['valoraporte']);  ?></td>
					    <td style='text-align:right'><?php echo $consulta['trabajadores'];  ?></td>
					    <td style='text-align:center'><?php echo $consulta['ajuste'];  ?></td>
					    <td><?php echo $consulta['fechapago'];  ?></td>
					</tr>
				<?php 	
					}
					if($cont==0){ ?>
						<script type="text/javascript">	
							MENSAJE("NO existen aportes.");
						</script>
					<?php }					
				?>
			</tbody>
		</table>
		<div id="controls">	
			<div id="perpage">
				<select onChange="sorterAportesTrab.size(this.value)">
					<option value="5" selected="selected">5</option>
					<option value="10" >10</option>
					<option value="20">20</option>
					<option value="50">50</option>
					<option value="100">100</option>
					<option value="500">500</option>
				</select>
				<label>&nbsp;&nbsp;&nbsp;&nbsp;Registros Por P&aacute;gina</label>
			</div>
			<div id="navigation">
				<img src="<?php echo URL_PORTAL; ?>imagenes/imagesSorter/first.gif" width="16" height="16" alt="first Page" onClick="sorterAportesTrab.move(-1,true)" />
				<img src="<?php echo URL_PORTAL; ?>imagenes/imagesSorter/previous.gif" width="16" height="16" alt="first Page" onClick="sorterAportesTrab.move(-1)" />
				<img src="<?php echo URL_PORTAL; ?>imagenes/imagesSorter/next.gif" width="16" height="16" alt="first Page" onClick="sorterAportesTrab.move(1)" />
				<img src="<?php echo URL_PORTAL; ?>imagenes/imagesSorter/last.gif" width="16" height="16" alt="Last Page" onClick="sorterAportesTrab.move(1,true)" />
			</div>
			<div id="text">&nbsp;&nbsp;&nbsp;&nbsp;P&aacute;gina <label id="currentpageAportesTrab"></label> de <label id="pagelimitAportesTrab"></label></div>	
		</div>
		       
		<input type="hidden" name="txtIde" id="txtIde" value="<?php echo $ide; ?>" />
</body>
</html>