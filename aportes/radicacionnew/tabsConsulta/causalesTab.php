<?php
/* autor:       orlando puentes
 * fecha:       20/07/2010
 * objetivo:    
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';
$idpersona= $_REQUEST['v0'];
include_once $raiz . DIRECTORY_SEPARATOR . 'clases'. DIRECTORY_SEPARATOR .'p.subsidios.class.php';
$objClase=new Subsidios();
$consulta = $objClase->buscar_causales($idpersona);
$cont=0;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Documento sin título</title>
<link href="<?php echo URL_PORTAL; ?>css/sorterStyle.css" rel="stylesheet" type="text/css" media="screen" />
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/comunes.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/jquery.tablesorter.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/jquery.tablesorter.pager.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
  $("#table5").tablesorter({widthFixed:true})/*.tablesorterPager({container: $("#pager")})*/;
  $("#table5 tbody tr:even").addClass("evenrow");
  });
  </script>
</head>

<body>
<label class="Rojo">Causales de no giro</label>
<table width="100%" border="0" cellspacing="0" class="sortable hover" id="table5">
<thead>
<tr>
<th class="head"><h3><strong>Periodo</strong></h3></th>
<th class="head"><h3><strong>Beneficiario</strong></h3></th>
<th class="head"><h3><strong>Causal</strong></h3></th>
</tr>
</thead>
<tbody>
<?php 
while($row=mssql_fetch_array($consulta)){
	$nombres=$row['pnombre']." ".$row['snombre']." ".$row['papellido']." ".$row['sapellido'];
?>
<tr>
	<td><?php echo $row['periodo']; ?></td>
    <td><?php echo $nombres; ?></td>
    <td><?php echo $row['detalledefinicion']; ?></td>
  </tr>
  <?php }?>
  </tbody>
</table>  

<!-- BOTONES DE ORDENACION -->
<!--   <div id="pager" class="pager">
   <div id="perpage">
	<select class="pagesize">
			<option selected="selected"  value="5">5</option>
			<option value="20">20</option>
			<option value="30">30</option>
			<option  value="40">40</option>
		</select>
        <label>Registros Por P&aacute;gina</label>
        </div>
        <div id="navigation">
		<img src="<?php echo URL_PORTAL; ?>imagenes/imagesSorter/first.gif" class="first"/>
		<img src="<?php echo URL_PORTAL; ?>imagenes/imagesSorter/previous.gif" class="prev"/>
        <img src="<?php echo URL_PORTAL; ?>imagenes/imagesSorter/next.gif" class="next"/>
		<img src="<?php echo URL_PORTAL; ?>imagenes/imagesSorter/last.gif" class="last"/> 
        <label>P&aacute;gina</label>
		<input type="text" class="pagedisplay boxfecha" readonly="readonly" />
       </div>
        
	</div>-->
   
</body>
</html>