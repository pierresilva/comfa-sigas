<?php
set_time_limit ( 0 );
ini_set ( "display_errors", '1' );
date_default_timezone_set ( "America/Bogota" );

$root = $_SERVER ['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'sigas' .DIRECTORY_SEPARATOR . 'session.php'; 
include_once $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';

$FrmActual=basename($_SERVER['PHP_SELF']);
$fecver = date('Ymd h:i:s A',filectime($FrmActual));
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Documentos</title>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>aportes/radicacionnew/js/tabs/documentosTab.js"></script>
</head>
<body>
	<div>
		<div style="text-align:right; height:20px; top:0px; width:221px; position:absolute; left: 20px;"><?php echo 'Versi&oacute;n: ' . $fecver; ?></div>
		<center>
			<h4>LISTADO DOCUMENTOS A SOLICITAR</h4>
			
			<table width="580px" id="tblDocRequeridos">
				
	<!-- FORMULARIO RENOVACION -->
				<tr align="left" width="580px" class="renovacionTab">
					<td> <table margin-left: 17px;" border="0" width="580px">
						<tr align="left">
							<td>
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkRNTFormularioAfiliacion" class="renovacionTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4062"> 
									Formulario afiliaci&oacute;n. 
							</td>
							<td> 
								<input type="text" id="txtRNTFormularioAfiliacion" class="renovacionTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left">
							<td>
								&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkRNTCopiaDocumentoAfiliado" class="renovacionTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4016"> 
									Fotocopia documento identidad. 
							</td>
							<td> 
								<input type="text" id="txtRNTCopiaDocumentoAfiliado" class="renovacionTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left">
							<td>
								&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkRNTGrupoFamiliar" class="renovacionTab contenedor" onChange="onclickCheckContenedor($(this),0);"> 
									Grupo Familiar. 
							</td>
							<td> 
								&nbsp;
							</td>
						</tr>
						<tr align="left" class="trRNTGrupoFamiliar">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkRNTConvivencias" class="renovacionTab contenedor" onChange="onclickCheckContenedor($(this),1);"> 
									Convivencias. 
							</td>
							<td> 
								&nbsp;
							</td>
						</tr>
						<tr align="left" class="trRNTGrupoFamiliar trRNTConvivencias">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkRNTCopiaDocumentoConyuge" class="renovacionTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="4017"> 
									Fotocopia Documento Conyuge. 
							</td>
							<td> 
								<input type="text" id="txtRNTCopiaDocumentoConyuge" class="renovacionTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trRNTGrupoFamiliar trRNTConvivencias">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkRNTCertificadoConvivencia" class="renovacionTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="4018"> 
									Certificado Convivencia. 
							</td>
							<td> 
								<input type="text" id="txtRNTCertificadoConvivencia" class="renovacionTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trRNTGrupoFamiliar trRNTConvivencias">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkRNTRegistroConvivencia" class="renovacionTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="4019"> 
									Registro Matrimonio. 
							</td>
							<td> 
								<input type="text" id="txtRNTRegistroConvivencia" class="renovacionTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>						
						<tr align="left" class="trRNTGrupoFamiliar">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkRNTHijos" class="renovacionTab contenedor" onChange="onclickCheckContenedor($(this),1);"> 
									Hijos menores de 19 a&ntilde;os.
							</td>
							<td> 
								&nbsp;
							</td>
						</tr>
						<tr align="left" class="trRNTGrupoFamiliar trRNTHijos">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkRNTRegistroCivilHijo" class="renovacionTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="4098"> 
									Registro Civil Hijo.
							</td>
							<td> 
								<input type="text" id="txtRNTRegistroCivilHijo" class="renovacionTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trRNTGrupoFamiliar trRNTHijos">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkRNTCopiaDocumentoHijo" class="renovacionTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="4020"> 
									Fotocopia documento Hijo.
							</td>
							<td> 
								<input type="text" id="txtRNTCopiaDocumentoHijo" class="renovacionTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trRNTGrupoFamiliar trRNTHijos">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkRNTDependenciaEconomicaHijo" class="renovacionTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="4021"> 
									Formato Dependencia Econ&oacute;mica.
							</td>
							<td> 
								<input type="text" id="txtRNTDependenciaEconomicaHijo" class="renovacionTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trRNTGrupoFamiliar trRNTHijos">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkRNTCertificadoHijo" class="renovacionTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="4022"> 
									Certificados.
							</td>
							<td> 
								<input type="text" id="txtRNTCertificadoHijo" class="renovacionTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trRNTGrupoFamiliar">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkRNTHijastros" class="renovacionTab contenedor" onChange="onclickCheckContenedor($(this),1);"> 
									Hijastros menores de 19 a&ntilde;os. 
							</td>
							<td> 
								&nbsp;
							</td>
						</tr>
						<tr align="left" class="trRNTGrupoFamiliar trRNTHijastros">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkRNTRegistroCivilHijastro" class="renovacionTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="4099"> 
									Registro Civil Hijastro.
							</td>
							<td> 
								<input type="text" id="txtRNTRegistroCivilHijastro" class="renovacionTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trRNTGrupoFamiliar trRNTHijastros">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkRNTCopiaDocumentoHijastro" class="renovacionTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="4023"> 
									Fotocopia documento Hijastro.
							</td>
							<td> 
								<input type="text" id="txtRNTCopiaDocumentoHijastro" class="renovacionTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trRNTGrupoFamiliar trRNTHijastros">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkRNTDependenciaEconomicaHijastro" class="renovacionTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="4024"> 
									Formato Dependencia Econ&oacute;mica.
							</td>
							<td> 
								<input type="text" id="txtRNTDependenciaEconomicaHijastro" class="renovacionTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trRNTGrupoFamiliar trRNTHijastros">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkRNTSentenciaJudicialHijastro" class="renovacionTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="4025"> 
									Sentencia Judicial.
							</td>
							<td> 
								<input type="text" id="txtRNTSentenciaJudicialHijastro" class="renovacionTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trRNTGrupoFamiliar trRNTHijastros">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkRNTCertificadoHijastro" class="renovacionTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="4026"> 
									Certificados.
							</td>
							<td> 
								<input type="text" id="txtRNTCertificadoHijastro" class="renovacionTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trRNTGrupoFamiliar">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkRNTHermanos" class="renovacionTab contenedor" onChange="onclickCheckContenedor($(this),1);"> 
									Hermanos que dependen econ&oacute;micamente. 
							</td>
							<td> 
								&nbsp;
							</td>
						</tr>
						<tr align="left" class="trRNTGrupoFamiliar trRNTHermanos">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkRNTRegistroCivilHermano" class="renovacionTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="4100"> 
									Registro Civil Hermano.
							</td>
							<td> 
								<input type="text" id="txtRNTRegistroCivilHermano" class="renovacionTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trRNTGrupoFamiliar trRNTHermanos">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkRNTCopiaDocumentoHermano" class="renovacionTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="4027"> 
									Fotocopia documento Hermano.
							</td>
							<td> 
								<input type="text" id="txtRNTCopiaDocumentoHermano" class="renovacionTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trRNTGrupoFamiliar trRNTHermanos">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkRNTRegistroDefuncionPadres" class="renovacionTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="4029"> 
									Registro de Defunci&oacute;n Padres.
							</td>
							<td> 
								<input type="text" id="txtRNTRegistroDefuncionPadres" class="renovacionTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trRNTGrupoFamiliar trRNTHermanos">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkRNTDependenciaEconomicaHermano" class="renovacionTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="4028"> 
									Formato Dependencia Econ&oacute;mica.
							</td>
							<td> 
								<input type="text" id="txtRNTDependenciaEconomicaHermano" class="renovacionTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>						
						<tr align="left" class="trRNTGrupoFamiliar trRNTHermanos">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkRNTSentenciaJudicialHermano" class="renovacionTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="4030"> 
									Sentencia Judicial.
							</td>
							<td> 
								<input type="text" id="txtRNTSentenciaJudicialHermano" class="renovacionTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trRNTGrupoFamiliar trRNTHermanos">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkRNTCertificadoHermano" class="renovacionTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="4031"> 
									Certificados.
							</td>
							<td> 
								<input type="text" id="txtRNTCertificadoHermano" class="renovacionTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>											
						<tr align="left" class="trRNTGrupoFamiliar">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkRNTPadres" class="renovacionTab contenedor" onChange="onclickCheckContenedor($(this),1);"> 
									Padres que dependen econ&oacute;micamente. 
							</td>
							<td> 
								&nbsp;
							</td>
						</tr>
						<tr align="left" class="trRNTGrupoFamiliar trRNTPadres">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkRNTRegistroCivilAfiliado" class="renovacionTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="4032"> 
									Registro Civil Afiliado
							</td>
							<td> 
								<input type="text" id="txtRNTRegistroCivilAfiliado" class="renovacionTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trRNTGrupoFamiliar trRNTPadres">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkRNTCopiaDocumentoPadre" class="renovacionTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="4033"> 
									Fotocopia documento Padre.
							</td>
							<td> 
								<input type="text" id="txtRNTCopiaDocumentoPadre" class="renovacionTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trRNTGrupoFamiliar trRNTPadres">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkRNTCertificacionEPS" class="renovacionTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="4034"> 
									Cetificaci&oacute;n EPS.
							</td>
							<td> 
								<input type="text" id="txtRNTCertificacionEPS" class="renovacionTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trRNTGrupoFamiliar trRNTPadres">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkRNTDependenciaEconomicaPadre" class="renovacionTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="4035"> 
									Formato Dependencia Econ&oacute;mica.
							</td>
							<td> 
								<input type="text" id="txtRNTDependenciaEconomicaPadre" class="renovacionTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trRNTGrupoFamiliar trRNTPadres">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkRNTCertificadoPadre" class="renovacionTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="4036"> 
									Certificados.
							</td>
							<td> 
								<input type="text" id="txtRNTCertificadoPadre" class="renovacionTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
					</table></td>
				</tr>
				
				<tr height="20px" >
					<td></td>
				</tr>
				<tr>					
					<td align="center" >
						<img src="<?php echo URL_PORTAL; ?>imagenes/guardar.png" title="Guardar" style="cursor:pointer" onClick="saveRadicacion();"> &nbsp;&nbsp;&nbsp;
						<img src="<?php echo URL_PORTAL; ?>imagenes/devolver2.png" title="Devolver" style="cursor:pointer" onClick="saveDevolucion(1);">
					</td>					
				</tr>
			</table>
		</center>
	</div>
</body>
</html>