<?php
set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");

$root = $_SERVER ['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';

include_once $_SESSION['RAIZ'] . DIRECTORY_SEPARATOR . 'rsc/pdo/IFXDbManejador.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

$FrmActual=basename($_SERVER['PHP_SELF']);
$fecver = date('Ymd h:i:s A',filectime($FrmActual));
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Informaci&oacute;n</title>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>aportes/radicacionnew/js/tabs/bloqueoTarjetaTab.js"></script>
</head>
<body>
	<table width="90%" border="0" cellspacing="0" cellpadding="0"
		align="center">
		<tr>
			<td width="13" height="29" class="arriba_iz">&nbsp;</td>
			<td class="arriba_ce">
				<span class="letrablanca">:: Informaci&oacute;n Bloqueo Tarjeta ::</span>
				<div style="text-align:right; height:20px; top:58px; width:221px; position:absolute; right: 100px;"><?php echo 'Versi&oacute;n: ' . $fecver; ?></div>
			</td>
			<td width="13" class="arriba_de" align="right">&nbsp;</td>
		</tr>
		<tr>
			<td class="cuerpo_iz">&nbsp;</td>
			<td class="cuerpo_ce"><br />
				<table width="90%" cellspacing="0" cellpadding="0" align="center">
					<tr>
						<td>&nbsp;</td>
					</tr>
				</table>
			</td>
			<td class="cuerpo_de">&nbsp;</td>
		</tr>
		<tr>
			<td class="cuerpo_iz">&nbsp;</td>
			<td class="cuerpo_ce">
				<table width="90%" border="0" cellspacing="0" class="tablero" align="center">
					<tr>
						<td width="17%">Buscar Por:</td>
						<td width="38%">
							<select name="cmbBuscarPor" id="cmbBuscarPor" class="box1" style="width: 250px" onchange="onChangeBuscarPor(this);" >
								<option value="0" selected="selected">Seleccione...</option>
		            			<option value="1">Identificaci&oacute;n</option>
		            			<option value="2">Bono</option>
							</select>
						</td>
						<td width="22.5%"></td>
						<td width="22.5%"></td>						
					</tr>
					<tr id="trBuscarPorPersona">
						<td align="left">Tipo Documento Afiliado</td>
						<td>
							<select name="cmbIdTipoDocumentoAfiliado" id="cmbIdTipoDocumentoAfiliado" class="box1" style="width: 250px" onchange="$('#txtNumero').val('').trigger('blur');" >
								<option value="0" selected="selected">Seleccione...</option>
		            			<?php
		            				$rs = $db->Definiciones ( 1, 1 );															
									while ( $row = $rs->fetch() ) {
										echo "<option value=" . $row ['iddetalledef'] . ">" . $row ['detalledefinicion'] . "</option>";
									}
								?>
							</select> <img src="../../imagenes/menu/obligado.png" width="12" height="12">
						</td>
						<td>N&uacute;mero Documento Afiliado</td>
						<td>
							<input type="text" class="box1" name="txtNumero" maxlength="12" id="txtNumero" onkeyup="validarIdentificacion($('#cmbIdTipoDocumentoAfiliado'),this);"
								onkeydown="validarIdentificacion($('#cmbIdTipoDocumentoAfiliado'),this);" onkeypress="validarIdentificacion($('#cmbIdTipoDocumentoAfiliado'),this);"
								onblur="busquedaPersona($('#cmbIdTipoDocumentoAfiliado'), $('#txtNumero'), $('#tdNombreCompletoAfiliado'));">
							<img src="../../imagenes/menu/obligado.png" width="12" height="12">
						</td>
					</tr>
					<tr id="trBuscarPorBono">
						<td>Bono</td>
						<td colspan="3">
							<input type="text" class="box1" name="txtBono" maxlength="17" id="txtBono" onblur="busquedaBono($('#cmbIdTipoDocumentoAfiliado'), $('#txtBono'), $('#tdNombreCompletoAfiliado'));">
							<img src="../../imagenes/menu/obligado.png" width="12" height="12">
						</td>
					</tr>
					<tr class="trBuscarPor" >
						<td>Nombre Completo</td>
						<td id="tdNombreCompletoAfiliado" colspan="3">&nbsp;</td>						
					</tr>
					<tr class="trBuscarPor" >
						<td>Bono</td>
						<td id="tdBono">&nbsp;</td>						
						<td>Estado</td>
						<td id="tdEstado">&nbsp;</td>
					</tr>
					<tr class="trBuscarPor" >
						<td>Fecha Solicitud</td>
						<td id="tdFechaSolicitud">&nbsp;</td>						
						<td>Saldo</td>
						<td id="tdSaldo">&nbsp;</td>
					</tr>
					<tr class="trBuscarPor" >
						<td>Ubicaci&oacute;n</td>
						<td id="tdUbicacion" colspan="3">&nbsp;</td>
					</tr>
					<tr class="trBuscarPor" >
						<td colspan="4" ><b>TIPO DE NOVEDAD A REGISTRAR</b></td>						
					</tr>
					<tr class="trBuscarPor" >
						<td colspan="4">
							<?php
								$c=-1; $motivos = array();							
								$rs = $db->Definiciones ( 48, 2 );
								while ( $row = $rs->fetch() ) { $c++;
									$motivos[$c] = array("id" => $row['iddetalledef'], "detalle" => $row['detalledefinicion']);									
								}
							?>
							<p id="pBloqueo">
								<input type="radio" name="radTipoNovedad" id="radTipoNovedadBloqueo" style="float:left;" />
								 <label for="tipoNovedad_b" style="width:135px; float:left; text-align:left;">Bloqueo.</label>
								<select name="cmbNovedad" id="cmbNovedadBloqueo" class="box1" style="width: 250px" onchange="onChangeNovedad(this);">
									<option value="0">Seleccione</option>
									<?php foreach($motivos as $posicion => $motivo):
											if(!in_array($motivo["id"],array(2868,2945,2947,2949))):?>
											<option value="<?php echo $motivo["id"]; ?>"><?php echo $motivo["detalle"]; ?></option>
									<?php   endif; endforeach; ?>
								</select>
							</p>
							<p id="pSuspencion">
								<input type="radio" name="radTipoNovedad" id="radTipoNovedadSuspencion" style="float:left;" />
								 <label for="tipoNovedad_s" style="width:135px; float:left; text-align:left;">Suspenci&oacute;n.</label>
								<select name="cmbNovedad" id="cmbNovedadSuspencion" class="box1" style="width: 250px">
									<option value="0">Seleccione</option>
									<?php foreach($motivos as $posicion => $motivo):
											if(in_array($motivo["id"],array(2868,2949))):?>
											<option value="<?php echo $motivo["id"]; ?>"><?php echo $motivo["detalle"]; ?></option>
									<?php   endif; endforeach; ?>
								</select>
							</p>
							<p id="pHabilitacion">
								<input type="radio" name="radTipoNovedad" id="radTipoNovedadHabilitacion" style="float:left;" />
								 <label for="tipoNovedad_h" style="width:135px; float:left; text-align:left;">Habilitaci&oacute;n.</label>
								<select name="cmbNovedad" id="cmbNovedadHabilitacion" class="box1" style="width: 250px">
									<option value="0">Seleccione</option>
									<?php foreach($motivos as $posicion => $motivo):
											if(in_array($motivo["id"],array(2947))):?>
											<option value="<?php echo $motivo["id"]; ?>"><?php echo $motivo["detalle"]; ?></option>
									<?php   endif; endforeach; ?>
								</select>
							</p>
						</td>						
					</tr>
					<tr class="trBuscarPorVerificar" >
						<td colspan="4" >
							<b>VALIDACI&Oacute;N DATOS DEL AFILIADO</b><br>
							<label style="font-size:8pt">Las siguientes preguntas se hacen para verificar que el cliente efectivamente es el propietario de la tarjeta. Seleccione las respuestas de acuerdo a lo contestado por el cliente:</label>
							<ol style="list-style-type: decimal;" id="olPreguntasSeguridad">
								<li id="liPreguntaDireccion" >Cu&aacute;l es su direcci&oacute;n actual?
									<ol style="list-style-type: lower-alpha" id="olPreguntaSobredireccion"></ol>
								</li><br>
								<li id="liPreguntaTelefono" >Cu&aacute;l es su tel&eacute;fono actual?
									<ol style="list-style-type: lower-alpha" id="olPreguntaSobretelefono"></ol>
								</li><br>
								<li id="liPreguntaTieneSaldo" >Actualmente su tarjeta tiene saldo?
									<ol style="list-style-type: lower-alpha" id="olPreguntaSobretienesaldo"></ol>
								</li><br>
								<li id="liPreguntaSaldoTarjeta" >Cu&aacute;l es el saldo actual de su tarjeta?
									<ol style="list-style-type: lower-alpha" id="olPreguntaSobresaldotarjeta"></ol>
								</li><br>
								<li id="liPreguntaEmpresa" >Con cu&aacute;l de las siguientes empresas tiene actualmente una afiliaci&oacute;n?
									<ol style="list-style-type: lower-alpha" id="olPreguntaSobreempresa"></ol>
								</li><br>
							</ol>
							<center><input type="button" value="Validar" id="btnValidarRespuestas" onclick="onClickVerificar()";/></center>
						</td>						
					</tr>
					<tr>
						<td align="left">Notas</td>
						<td colspan="3"><textarea name="txtNotas" id="txtNotas" maxlength="250" class="boxlargo" style="width: 92%; height: 40px;" ></textarea> 
						</td>
					</tr>
				</table>
			</td>
			<td class="cuerpo_de">&nbsp;</td>
		</tr>
		<tr>
			<td class="abajo_iz" >&nbsp;</td>
    		<td class="abajo_ce" align="center" ><img src="../../imagenes/guardar.png" title="Guardar" style="cursor:pointer" onclick="saveRadicacion();" id="btnGuardarRadicacion"></td>
    		<td class="abajo_de" >&nbsp;</td>
		</tr>		
	</table>
</body>
</html>