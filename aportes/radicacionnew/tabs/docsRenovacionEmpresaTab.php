<?php
set_time_limit ( 0 );
ini_set ( "display_errors", '1' );
date_default_timezone_set ( "America/Bogota" );

$root = $_SERVER ['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'sigas' .DIRECTORY_SEPARATOR . 'session.php'; 
include_once $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';

$FrmActual=basename($_SERVER['PHP_SELF']);
$fecver = date('Ymd h:i:s A',filectime($FrmActual));
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Documentos</title>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>aportes/radicacionnew/js/tabs/documentosTab.js"></script>
</head>
<body>
	<div>
		<div style="text-align:right; height:20px; top:0px; width:221px; position:absolute; left: 20px;"><?php echo 'Versi&oacute;n: ' . $fecver; ?></div>
		<center>
			<h4>LISTADO DOCUMENTOS A SOLICITAR</h4>
			
			<table width="580px" id="tblDocRequeridos">
			
	<!-- FORMULARIO RENOVACION EMPRESA -->
				<tr align="left" width="580px" class="renovacionEmpresaTab">
					<td> <table margin-left: 17px;" border="0" width="580px">
						<tr align="left">
							<td>
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkREEFormularioAfiliacion" class="renovacionEmpresaTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4039"> 
									Formulario afiliaci&oacute;n. 
							</td>
							<td> 
								<input type="text" id="txtREEFormularioAfiliacion" class="renovacionEmpresaTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left">
							<td>
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/spacer.gif" width="12" height="12">
								<input type="checkbox" id="chkREERUT" class="renovacionEmpresaTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4040"> 
									RUT. 
							</td>
							<td> 
								<input type="text" id="txtREERUT" class="renovacionEmpresaTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left">
							<td>
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/spacer.gif" width="12" height="12">
								<input type="checkbox" id="chkREECopiaDocumentoRepresentante" class="renovacionEmpresaTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4041"> 
									Fotocopia Documento Identidad Representante. 
							</td>
							<td> 
								<input type="text" id="txtREECopiaDocumentoRepresentante" class="renovacionEmpresaTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left">
							<td>
								&nbsp;&nbsp;&nbsp;
								<input type="radio" name="radREETipoPersona" id="chkREEPersonaJuridica" class="renovacionEmpresaTab contenedor" onChange="onclickRadioContenedor($('input[name=radREETipoPersona]'));"> 
									Persona Jur&iacute;dica. 
							</td>
							<td> 
								&nbsp;
							</td>
						</tr>
						<tr align="left">
							<td>
								&nbsp;&nbsp;&nbsp;
								<input type="radio" name="radREETipoPersona" id="chkREEPersonaNatural" class="renovacionEmpresaTab contenedor" onChange="onclickRadioContenedor($('input[name=radREETipoPersona]'));"> 
									Persona Natural 
							</td>
							<td> 
								&nbsp;
							</td>
						</tr>
						<tr align="left" class="trREEPersonaJuridica">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkREECamaraComercio" class="renovacionEmpresaTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4042"> 
									C&aacute;mara de Comercio.
							</td>
							<td> 
								<input type="text" id="txtREECamaraComercio" class="renovacionEmpresaTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trREEPersonaJuridica">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;								
								<input type="checkbox" id="chkREESoportePago" class="renovacionEmpresaTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4043"> 
									Soporte de Pago.
							</td>
							<td> 
								<input type="text" id="txtREESoportePago" class="renovacionEmpresaTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trREEPersonaJuridica">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;								
								<input type="checkbox" id="chkREENomina" class="renovacionEmpresaTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4044"> 
									N&oacute;mina.
							</td>
							<td> 
								<input type="text" id="txtREENomina" class="renovacionEmpresaTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>						
						<tr align="left" class="trREEPersonaJuridica">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkREEAfiliadoOtraCaja" class="renovacionEmpresaTab contenedor" onChange="onclickCheckContenedor($(this),1);"> 
									Estuvo afiliado a otra caja. 
							</td>
							<td> 
								&nbsp;
							</td>
						</tr>
						<tr align="left" class="trREEPersonaJuridica trREEAfiliadoOtraCaja">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/spacer.gif" width="12" height="12">
								<input type="checkbox" id="chkREEPazySalvo" class="renovacionEmpresaTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="4045"> 
									Paz y salvo.
							</td>
							<td> 
								<input type="text" id="txtREEPazySalvo" class="renovacionEmpresaTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trREEPersonaJuridica">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkREESinAnimoLucro" class="renovacionEmpresaTab contenedor" onChange="onclickCheckContenedor($(this),1);"> 
									Es Sin &Aacute;nimo de Lucro. 
							</td>
							<td> 
								&nbsp;
							</td>
						</tr>
						<tr align="left" class="trREEPersonaJuridica trREESinAnimoLucro">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/spacer.gif" width="12" height="12">
								<input type="checkbox" id="chkREEPersoneriaJuridica" class="renovacionEmpresaTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="4046"> 
									Personer&iacute;a Jur&iacute;dica.
							</td>
							<td> 
								<input type="text" id="txtREEPersoneriaJuridica" class="renovacionEmpresaTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trREEPersonaJuridica">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkREECooperativa" class="renovacionEmpresaTab contenedor" onChange="onclickCheckContenedor($(this),1);"> 
									Es Cooperativa. 
							</td>
							<td> 
								&nbsp;
							</td>
						</tr>
						<tr align="left" class="trREEPersonaJuridica trREECooperativa">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/spacer.gif" width="12" height="12">
								<input type="checkbox" id="chkREECertificadoSuper" class="renovacionEmpresaTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="4047"> 
									Certificado Superintendencia de econom&iacute;a solidaria.
							</td>
							<td> 
								<input type="text" id="txtREECertificadoSuper" class="renovacionEmpresaTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trREEPersonaJuridica">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkREEPropiedadHorizontal" class="renovacionEmpresaTab contenedor" onChange="onclickCheckContenedor($(this),1);"> 
									Es Propiedad horizontal. 
							</td>
							<td> 
								&nbsp;
							</td>
						</tr>
						<tr align="left" class="trREEPersonaJuridica trREEPropiedadHorizontal">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/spacer.gif" width="12" height="12">
								<input type="checkbox" id="chkREEActaNombramiento" class="renovacionEmpresaTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="4048"> 
									Acta de nombramiento de administraci&oacute;n Actual.
							</td>
							<td> 
								<input type="text" id="txtREEActaNombramiento" class="renovacionEmpresaTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trREEPersonaJuridica">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkREEConsorcioUnion" class="renovacionEmpresaTab contenedor" onChange="onclickCheckContenedor($(this),1);"> 
									Es Consorcio o Uni&oacute;n Temporal. 
							</td>
							<td> 
								&nbsp;
							</td>
						</tr>
						<tr align="left" class="trREEPersonaJuridica trREEConsorcioUnion">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/spacer.gif" width="12" height="12">
								<input type="checkbox" id="chkREEActaConformacion" class="renovacionEmpresaTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="4049"> 
									Acta de conformaci&oacute;n.
							</td>
							<td> 
								<input type="text" id="txtREEActaConformacion" class="renovacionEmpresaTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						
						<tr align="left" class="trREEPersonaNatural">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkREENACamaraComercio" class="renovacionEmpresaTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4042"> 
									C&aacute;mara de Comercio.
							</td>
							<td> 
								<input type="text" id="txtREENACamaraComercio" class="renovacionEmpresaTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trREEPersonaNatural">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;								
								<input type="checkbox" id="chkREENANomina" class="renovacionEmpresaTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4044"> 
									N&oacute;mina.
							</td>
							<td> 
								<input type="text" id="txtREENANomina" class="renovacionEmpresaTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trREEPersonaNatural">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkREENAfiliadoOtraCaja" class="renovacionEmpresaTab contenedor" onChange="onclickCheckContenedor($(this),1);"> 
									Estuvo afiliado a otra caja. 
							</td>
							<td> 
								&nbsp;
							</td>
						</tr>
						<tr align="left" class="trREEPersonaNatural trREENAfiliadoOtraCaja">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/spacer.gif" width="12" height="12">
								<input type="checkbox" id="chkREENPazySalvo" class="renovacionEmpresaTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="4045"> 
									Paz y salvo.
							</td>
							<td> 
								<input type="text" id="txtREENPazySalvo" class="renovacionEmpresaTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>											
					</table></td>
				</tr>				
	
				<tr height="20px" >
					<td></td>
				</tr>
				<tr>					
					<td align="center" ><img src="<?php echo URL_PORTAL; ?>imagenes/guardar.png" title="Guardar" style="cursor:pointer" onClick="saveRadicacion();"></td>					
				</tr>
			</table>
		</center>
	</div>
</body>
</html>