<?php
set_time_limit ( 0 );
ini_set ( "display_errors", '1' );
date_default_timezone_set ( "America/Bogota" );

$root = $_SERVER ['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'sigas' .DIRECTORY_SEPARATOR . 'session.php'; 
include_once $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';

$FrmActual=basename($_SERVER['PHP_SELF']);
$fecver = date('Ymd h:i:s A',filectime($FrmActual));
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Documentos</title>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>aportes/radicacionnew/js/tabs/documentosTab.js"></script>
</head>
<body>
	<div>
		<div style="text-align:right; height:20px; top:0px; width:221px; position:absolute; left: 20px;"><?php echo 'Versi&oacute;n: ' . $fecver; ?></div>
		<center>
			<h4>LISTADO DOCUMENTOS A SOLICITAR</h4>
			
			<table width="580px" id="tblDocRequeridos">
			
	<!-- FORMULARIO POSTULACION FONEDE -->
				<tr align="left" width="580px" class="postulacionFonedeTab">
					<td> <table margin-left: 17px;" border="0" width="580px">
						<tr align="left">
							<td>
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkPFTCopiaDocumentoPostulante" class="postulacionFonedeTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4050"> 
									Copia documento identidad Postulante. 
							</td>
							<td> 
								<input type="text" id="txtPFTCopiaDocumentoPostulante" class="postulacionFonedeTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>						
						<tr align="left">
							<td>
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkPFTCopiaDocumentoMenores" class="postulacionFonedeTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="4051"> 
									Registro civil de nacimiento menores de 18 a&ntilde;os. 
							</td>
							<td> 
								<input type="text" id="txtPFTCopiaDocumentoMenores" class="postulacionFonedeTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left">
							<td>
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkPFTCopiaDocumentoMayores" class="postulacionFonedeTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="4052"> 
									Copia documento identidad mayores de 18 a&ntilde;os. 
							</td>
							<td> 
								<input type="text" id="txtPFTCopiaDocumentoMayores" class="postulacionFonedeTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="conPostulacion">
							<td>
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkPFTCertificadoEPS" class="postulacionFonedeTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4053"> 
									Certificado expedido por la &uacute;ltima E.P.S donde estuvo afiliado. 
							</td>
							<td> 
								<input type="text" id="txtPFTCertificadoEPS" class="postulacionFonedeTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="conPostulacion">
							<td>
								&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkPFTCertificadoLaboral" class="postulacionFonedeTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="4054"> 
									Certificado laboral. (CON VINCULACION ANTERIOR A UNA CAJA)
							</td>
							<td> 
								<input type="text" id="txtPFTCertificadoLaboral" class="postulacionFonedeTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="conPostulacion">
							<td>
								&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkPFTCartaTerminacionContrato" class="postulacionFonedeTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="4055"> 
									Carta de terminaci&oacute;n de contrato. (CON VINCULACION ANTERIOR A UNA CAJA)
							</td>
							<td> 
								<input type="text" id="txtPFTCartaTerminacionContrato" class="postulacionFonedeTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>												
					</table></td>
				</tr>
				
				<tr height="20px" >
					<td></td>
				</tr>
				<tr>					
					<td align="center" >
						<img src="<?php echo URL_PORTAL; ?>imagenes/guardar.png" title="Guardar" style="cursor:pointer" onClick="saveRadicacion();"> &nbsp;&nbsp;&nbsp;
						<img src="<?php echo URL_PORTAL; ?>imagenes/devolver2.png" title="Devolver" style="cursor:pointer" onClick="saveDevolucion(1);">
					</td>					
				</tr>
			</table>
		</center>
	</div>
</body>
</html>