<?php
set_time_limit ( 0 );
ini_set ( "display_errors", '1' );
date_default_timezone_set ( "America/Bogota" );

$root = $_SERVER ['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'sigas' .DIRECTORY_SEPARATOR . 'session.php'; 
include_once $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';

$FrmActual=basename($_SERVER['PHP_SELF']);
$fecver = date('Ymd h:i:s A',filectime($FrmActual));
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Documentos</title>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>aportes/radicacionnew/js/tabs/documentosTab.js"></script>
</head>
<body>
	<div>
		<div style="text-align:right; height:20px; top:0px; width:221px; position:absolute; left: 20px;"><?php echo 'Versi&oacute;n: ' . $fecver; ?></div>
		<center>
			<h4>LISTADO DOCUMENTOS A SOLICITAR</h4>
			
			<table width="580px" id="tblDocRequeridos">
			
	<!-- FORMULARIO MOVILIZACION -->
				<tr align="left" width="580px" class="visitaHabitabilidadTab">
					<td> <table margin-left: 17px;" border="0" width="580px">
						<tr align="left">
							<td>
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkVHTFormularioVisita" class="visitaHabitabilidadTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4076"> 
									Formulario Solicitud de la Visita. 
							</td>
							<td> 
								<input type="text" id="txtVHTFormularioVisita" class="visitaHabitabilidadTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left">
							<td>
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkVHTCertificadoPlaneacionMunicipal" class="visitaHabitabilidadTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4073"> 
									Certificado de Planeaci&oacute;n Municipal. 
							</td>
							<td> 
								<input type="text" id="txtVHTCertificadoPlaneacionMunicipal" class="visitaHabitabilidadTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left">
							<td>
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkVHTCertificadoTradicionLibertad" class="visitaHabitabilidadTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4074"> 
									Certificado de Tradici&oacute;n y Libertad de la Vivienda (Original y no mayor a 30 d&iacute;as). 
							</td>
							<td> 
								<input type="text" id="txtVHTCertificadoTradicionLibertad" class="visitaHabitabilidadTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left">
							<td>
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkVHTRecibosLuzAgua" class="visitaHabitabilidadTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4075"> 
									Recibos de luz y agua de la vivienda que se va a comprar.
							</td>
							<td> 
								<input type="text" id="txtVHTRecibosLuzAgua" class="visitaHabitabilidadTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left">
							<td>
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkVHTReciboCaja" class="visitaHabitabilidadTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true); $('#txt' + (this.id).substring(3)).val('2');" value="4072"> 
									Recibo de caja por valor de $50.000 (Original y 2 copias).
							</td>
							<td> 
								<input type="text" id="txtVHTReciboCaja" class="visitaHabitabilidadTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>									
					</table></td>
				</tr>								
	
				<tr height="20px" >
					<td></td>
				</tr>
				<tr>					
					<td align="center" ><img src="<?php echo URL_PORTAL; ?>imagenes/guardar.png" title="Guardar" style="cursor:pointer" onClick="saveRadicacion();"></td>					
				</tr>
			</table>
		</center>
	</div>
</body>
</html>