<?php
set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");

$root = $_SERVER ['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';

$FrmActual=basename($_SERVER['PHP_SELF']);
$fecver = date('Ymd h:i:s A',filectime($FrmActual));
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Informaci&oacute;n</title>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>aportes/radicacionnew/js/tabs/afiliacionEmpresaTab.js"></script>
</head>
<body>
	<table width="90%" border="0" cellspacing="0" cellpadding="0"
		align="center">
		<tr>
			<td width="13" height="29" class="arriba_iz">&nbsp;</td>
			<td class="arriba_ce">
				<span class="letrablanca">:: Informaci&oacute;n Afiliaci&oacute;n Empresa ::</span>
				<div style="text-align:right; height:20px; top:58px; width:221px; position:absolute; right: 100px;"><?php echo 'Versi&oacute;n: ' . $fecver; ?></div>
			</td>
			<td width="13" class="arriba_de" align="right">&nbsp;</td>
		</tr>
		<tr>
			<td class="cuerpo_iz">&nbsp;</td>
			<td class="cuerpo_ce"><br />
				<table width="90%" cellspacing="0" cellpadding="0" align="center">
					<tr>
						<td>&nbsp;</td>
					</tr>
				</table>
			</td>
			<td class="cuerpo_de">&nbsp;</td>
		</tr>
		<tr>
			<td class="cuerpo_iz">&nbsp;</td>
			<td class="cuerpo_ce">
				<table width="90%" border="0" cellspacing="0" class="tablero"
					align="center">
					<tr>
						<td align="left" width="17%">NIT Empresa</td>
						<td>
							<input type="text" name="txtNit" id="txtNit" class="box1" maxlength="14"
							onkeyup="solonumeros(this);" onkeydown="solonumeros(this);" onkeypress="solonumeros(this);"
							onblur="busquedaEmpresa($('#txtNit'), $('#txtRazonSocial'));"> <img src="../../imagenes/menu/obligado.png" 
							alt="Obligatorio" width="12" height="12">
						</td>
					</tr>
					<tr>
						<td align="left" >Raz&oacute;n social</td>
						<td>
							<input type="text" class="boxlargo" name="txtRazonSocial" id="txtRazonSocial" maxlength="100" >
							<img src="../../imagenes/menu/obligado.png" alt="Obligatorio" width="12" height="12">
						</td>
					</tr>
					<tr>
						<td align="left">Notas</td>
						<td colspan="3"><textarea name="txtNotas" id="txtNotas" maxlength="250" class="boxlargo" style="width: 92%; height: 40px;" ></textarea>
						</td>
					</tr>
				</table>
			</td>
			<td class="cuerpo_de">&nbsp;</td>
		</tr>
		<tr>
			<td class="abajo_iz">&nbsp;</td>
			<td class="abajo_ce"></td>
			<td class="abajo_de">&nbsp;</td>
		</tr>
	</table>
</body>
</html>