<?php
set_time_limit ( 0 );
ini_set ( "display_errors", '1' );
date_default_timezone_set ( "America/Bogota" );

$root = $_SERVER ['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'sigas' .DIRECTORY_SEPARATOR . 'session.php'; 
include_once $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';

$FrmActual=basename($_SERVER['PHP_SELF']);
$fecver = date('Ymd h:i:s A',filectime($FrmActual));
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Documentos</title>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>aportes/radicacionnew/js/tabs/documentosTab.js"></script>
</head>
<body>
	<div>
		<div style="text-align:right; height:20px; top:0px; width:221px; position:absolute; left: 20px;"><?php echo 'Versi&oacute;n: ' . $fecver; ?></div>
		<center>
			<h4>LISTADO DOCUMENTOS A SOLICITAR</h4>
			
			<table width="580px" id="tblDocRequeridos">
			
	<!-- FORMULARIO DEPENDIENTE -->
				<tr align="left" width="580px" class="dependienteTab">
					<td> <table margin-left: 17px;" border="0" width="580px">
						<tr align="left">
							<td>
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkDPTFormularioAfiliacion" class="dependienteTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4062"> 
									Formulario afiliaci&oacute;n. 
							</td>
							<td> 
								<input type="text" id="txtDPTFormularioAfiliacion" class="dependienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left">
							<td>
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkDPTCopiaDocumentoAfiliado" class="dependienteTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4016"> 
									Fotocopia documento identidad. 
							</td>
							<td> 
								<input type="text" id="txtDPTCopiaDocumentoAfiliado" class="dependienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left">
							<td>
								&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkDPTGrupoFamiliar" class="dependienteTab contenedor" onChange="onclickCheckContenedor($(this),0);"> 
									Grupo Familiar. 
							</td>
							<td> 
								&nbsp;
							</td>
						</tr>
						<tr align="left" class="trDPTGrupoFamiliar">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkDPTConvivencias" class="dependienteTab contenedor" onChange="onclickCheckContenedor($(this),1);"> 
									Convivencias. 
							</td>
							<td> 
								&nbsp;
							</td>
						</tr>
						<tr align="left" class="trDPTGrupoFamiliar trDPTConvivencias">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkDPTCopiaDocumentoConyuge" class="dependienteTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="4017"> 
									Fotocopia Documento Conyuge. 
							</td>
							<td> 
								<input type="text" id="txtDPTCopiaDocumentoConyuge" class="dependienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trDPTGrupoFamiliar trDPTConvivencias">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img id="imgDPTCertificadoConvivencia" src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkDPTCertificadoConvivencia" class="dependienteTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false); onclickCheckOpcional($(this), $('#chkDPTRegistroConvivencia'));" value="4018"> 
									Certificado Convivencia. 
							</td>
							<td> 
								<input type="text" id="txtDPTCertificadoConvivencia" class="dependienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trDPTGrupoFamiliar trDPTConvivencias">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img id="imgDPTRegistroConvivencia" src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkDPTRegistroConvivencia" class="dependienteTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false); onclickCheckOpcional($(this), $('#chkDPTCertificadoConvivencia')); onclickCheckOpcional($(this), $('#chkDPTDependenciaEconomicaHijo'), true);" value="4019"> 
									Registro Matrimonio. 
							</td>
							<td> 
								<input type="text" id="txtDPTRegistroConvivencia" class="dependienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>						
						<tr align="left" class="trDPTGrupoFamiliar">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkDPTHijos" class="dependienteTab contenedor" onChange="onclickCheckContenedor($(this),1);"> 
									Hijos menores de 19 a&ntilde;os.
							</td>
							<td> 
								&nbsp;
							</td>
						</tr>
						<tr align="left" class="trDPTGrupoFamiliar trDPTHijos">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img id="imgDPTRegistroCivilHijo" src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkDPTRegistroCivilHijo" class="dependienteTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false); onclickCheckOpcional($(this), $('#chkDPTCopiaDocumentoHijo'));" value="4098"> 
									Registro Civil Hijo.
							</td>
							<td> 
								<input type="text" id="txtDPTRegistroCivilHijo" class="dependienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trDPTGrupoFamiliar trDPTHijos">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img id="imgDPTCopiaDocumentoHijo" src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkDPTCopiaDocumentoHijo" class="dependienteTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false); onclickCheckOpcional($(this), $('#chkDPTRegistroCivilHijo'));" value="4020"> 
									Fotocopia documento Hijo.
							</td>
							<td> 
								<input type="text" id="txtDPTCopiaDocumentoHijo" class="dependienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trDPTGrupoFamiliar trDPTHijos">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img id="imgDPTDependenciaEconomicaHijo" src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkDPTDependenciaEconomicaHijo" class="dependienteTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="4021"> 
									Formato Dependencia Econ&oacute;mica.
							</td>
							<td> 
								<input type="text" id="txtDPTDependenciaEconomicaHijo" class="dependienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trDPTGrupoFamiliar trDPTHijos">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkDPTCertificadoHijo" class="dependienteTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="4022"> 
									Certificados.
							</td>
							<td> 
								<input type="text" id="txtDPTCertificadoHijo" class="dependienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trDPTGrupoFamiliar">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkDPTHijastros" class="dependienteTab contenedor" onChange="onclickCheckContenedor($(this),1);"> 
									Hijastros menores de 19 a&ntilde;os. 
							</td>
							<td> 
								&nbsp;
							</td>
						</tr>
						<tr align="left" class="trDPTGrupoFamiliar trDPTHijastros">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img id="imgDPTRegistroCivilHijastro" src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkDPTRegistroCivilHijastro" class="dependienteTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false); onclickCheckOpcional($(this), $('#chkDPTCopiaDocumentoHijastro'));" value="4099"> 
									Registro Civil Hijastro.
							</td>
							<td> 
								<input type="text" id="txtDPTRegistroCivilHijastro" class="dependienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trDPTGrupoFamiliar trDPTHijastros">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img id="imgDPTCopiaDocumentoHijastro" src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkDPTCopiaDocumentoHijastro" class="dependienteTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false); onclickCheckOpcional($(this), $('#chkDPTRegistroCivilHijastro'));" value="4023"> 
									Fotocopia documento Hijastro.
							</td>
							<td> 
								<input type="text" id="txtDPTCopiaDocumentoHijastro" class="dependienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trDPTGrupoFamiliar trDPTHijastros">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img id="imgDPTDependenciaEconomicaHijastro" src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkDPTDependenciaEconomicaHijastro" class="dependienteTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false); onclickCheckOpcional($(this), $('#chkDPTSentenciaJudicialHijastro'));" value="4024"> 
									Formato Dependencia Econ&oacute;mica.
							</td>
							<td> 
								<input type="text" id="txtDPTDependenciaEconomicaHijastro" class="dependienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trDPTGrupoFamiliar trDPTHijastros">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img id="imgDPTSentenciaJudicialHijastro" src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkDPTSentenciaJudicialHijastro" class="dependienteTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false); onclickCheckOpcional($(this), $('#chkDPTDependenciaEconomicaHijastro'));" value="4025"> 
									Sentencia Judicial.
							</td>
							<td> 
								<input type="text" id="txtDPTSentenciaJudicialHijastro" class="dependienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trDPTGrupoFamiliar trDPTHijastros">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkDPTCertificadoHijastro" class="dependienteTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="4026"> 
									Certificados.
							</td>
							<td> 
								<input type="text" id="txtDPTCertificadoHijastro" class="dependienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trDPTGrupoFamiliar">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkDPTHermanos" class="dependienteTab contenedor" onChange="onclickCheckContenedor($(this),1);"> 
									Hermanos que dependen econ&oacute;micamente. 
							</td>
							<td> 
								&nbsp;
							</td>
						</tr>
						<tr align="left" class="trDPTGrupoFamiliar trDPTHermanos">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img id="imgDPTRegistroCivilHermano" src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkDPTRegistroCivilHermano" class="dependienteTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false); onclickCheckOpcional($(this), $('#chkDPTCopiaDocumentoHermano'));" value="4100"> 
									Registro Civil Hermano.
							</td>
							<td> 
								<input type="text" id="txtDPTRegistroCivilHermano" class="dependienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trDPTGrupoFamiliar trDPTHermanos">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img id="imgDPTCopiaDocumentoHermano" src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkDPTCopiaDocumentoHermano" class="dependienteTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false); onclickCheckOpcional($(this), $('#chkDPTRegistroCivilHermano'));" value="4027"> 
									Fotocopia documento Hermano.
							</td>
							<td> 
								<input type="text" id="txtDPTCopiaDocumentoHermano" class="dependienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trDPTGrupoFamiliar trDPTHermanos">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkDPTRegistroDefuncionPadres" class="dependienteTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="4029"> 
									Registro de Defunci&oacute;n Padres.
							</td>
							<td> 
								<input type="text" id="txtDPTRegistroDefuncionPadres" class="dependienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trDPTGrupoFamiliar trDPTHermanos">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img id="imgDPTDependenciaEconomicaHermano" src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkDPTDependenciaEconomicaHermano" class="dependienteTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false); onclickCheckOpcional($(this), $('#chkDPTSentenciaJudicialHermano'));" value="4028"> 
									Formato Dependencia Econ&oacute;mica.
							</td>
							<td> 
								<input type="text" id="txtDPTDependenciaEconomicaHermano" class="dependienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>						
						<tr align="left" class="trDPTGrupoFamiliar trDPTHermanos">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img id="imgDPTSentenciaJudicialHermano" src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkDPTSentenciaJudicialHermano" class="dependienteTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false); onclickCheckOpcional($(this), $('#chkDPTDependenciaEconomicaHermano'));" value="4030"> 
									Sentencia Judicial.
							</td>
							<td> 
								<input type="text" id="txtDPTSentenciaJudicialHermano" class="dependienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trDPTGrupoFamiliar trDPTHermanos">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkDPTCertificadoHermano" class="dependienteTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="4031"> 
									Certificados.
							</td>
							<td> 
								<input type="text" id="txtDPTCertificadoHermano" class="dependienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>											
						<tr align="left" class="trDPTGrupoFamiliar">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkDPTPadres" class="dependienteTab contenedor" onChange="onclickCheckContenedor($(this),1);"> 
									Padres que dependen econ&oacute;micamente. 
							</td>
							<td> 
								&nbsp;
							</td>
						</tr>
						<tr align="left" class="trDPTGrupoFamiliar trDPTPadres">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkDPTRegistroCivilAfiliado" class="dependienteTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="4032"> 
									Registro Civil Afiliado
							</td>
							<td> 
								<input type="text" id="txtDPTRegistroCivilAfiliado" class="dependienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trDPTGrupoFamiliar trDPTPadres">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkDPTCopiaDocumentoPadre" class="dependienteTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="4033"> 
									Fotocopia documento Padre.
							</td>
							<td> 
								<input type="text" id="txtDPTCopiaDocumentoPadre" class="dependienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trDPTGrupoFamiliar trDPTPadres">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkDPTCertificacionEPS" class="dependienteTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="4034"> 
									Certificaci&oacute;n EPS.
							</td>
							<td> 
								<input type="text" id="txtDPTCertificacionEPS" class="dependienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trDPTGrupoFamiliar trDPTPadres">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkDPTDependenciaEconomicaPadre" class="dependienteTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="4035"> 
									Formato Dependencia Econ&oacute;mica.
							</td>
							<td> 
								<input type="text" id="txtDPTDependenciaEconomicaPadre" class="dependienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trDPTGrupoFamiliar trDPTPadres">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkDPTCertificadoPadre" class="dependienteTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="4036"> 
									Certificados.
							</td>
							<td> 
								<input type="text" id="txtDPTCertificadoPadre" class="dependienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
					</table></td>
				</tr>
				
				<tr height="20px" >
					<td></td>
				</tr>
				<tr>					
					<td align="center" >
						<img src="<?php echo URL_PORTAL; ?>imagenes/guardar.png" title="Guardar" style="cursor:pointer" onClick="saveRadicacion();"> &nbsp;&nbsp;&nbsp;
						<img src="<?php echo URL_PORTAL; ?>imagenes/devolver2.png" title="Devolver" style="cursor:pointer" onClick="saveDevolucion(1);">
					</td>					
				</tr>
			</table>
		</center>
	</div>
</body>
</html>