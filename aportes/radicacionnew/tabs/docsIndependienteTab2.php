<?php
set_time_limit ( 0 );
ini_set ( "display_errors", '1' );
date_default_timezone_set ( "America/Bogota" );

$root = $_SERVER ['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'sigas' .DIRECTORY_SEPARATOR . 'session.php'; 
include_once $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';

$FrmActual=basename($_SERVER['PHP_SELF']);
$fecver = date('Ymd h:i:s A',filectime($FrmActual));
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Documentos</title>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>aportes/radicacionnew/js/tabs/documentosTab.js"></script>
</head>
<body>
	<div>
		<div style="text-align:right; height:20px; top:0px; width:221px; position:absolute; left: 20px;"><?php echo 'Versi&oacute;n: ' . $fecver; ?></div>
		<center>
			<h4>LISTADO DOCUMENTOS A SOLICITAR</h4>
			
			<table width="580px" id="tblDocRequeridos">
			
	<!-- FORMULARIO INDEPENDIENTE -->
				<tr align="left" width="580px" class="independienteTab">
					<td> <table margin-left: 17px;" border="0" width="580px">
						<tr align="left">
							<td>
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkIPTFormularioAfiliacion" class="independienteTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4062"> 
									Formulario afiliaci&oacute;n. 
							</td>
							<td> 
								<input type="text" id="txtIPTFormularioAfiliacion" class="independienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left">
							<td>
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkIPTCopiaDocumentoAfiliado" class="independienteTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4016"> 
									Fotocopia documento identidad. 
							</td>
							<td> 
								<input type="text" id="txtIPTCopiaDocumentoAfiliado" class="independienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="independienteFacultativo" >
							<td>
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkIPTConstanciaAportes" class="independienteTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4037"> 
									Constancia de pago de aportes a EPS y AFP. 
							</td>
							<td> 
								<input type="text" id="txtIPTConstanciaAportes" class="independienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="independienteagramiado" >
							<td>
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkIPTConstanciaAgremiado" class="independienteTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4527"> 
									Constancia de trabajador agremiado. 
							</td>
							<td> 
								<input type="text" id="txtIPTConstanciaAgremiado" class="independienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						
						<tr align="left" class="independiextranjero" >
							<td>
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkIPTConstanciaExtrajero" class="independienteTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4037"> 
									Constancia de pago de aportes. 
							</td>
							<td> 
								<input type="text" id="txtIPTConstanciaExtrajero" class="independienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>						
						<tr align="left" class="independiextranjero" >
							<td>
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkIPTCertificadoExtranjero" class="independienteTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4526"> 
									Certificado de extranjeria. 
							</td>
							<td> 
								<input type="text" id="txtIPTCertificadoExtranjero" class="independienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="independientePensionadoVoluntario" >
							<td>
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkIPTCertificadoPension" class="independienteTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4038"> 
									Certificado, desprendible de pensi&oacute;n, &oacute; resoluci&oacute;n. 
							</td>
							<td> 
								<input type="text" id="txtIPTCertificadoPension" class="independienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>					
					</table></td>
				</tr>	
				<!-- De aqui en adelante trabajo grupo familiar -->
				
				<tr align="left">
							<td>
								&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkDPTGrupoFamiliar" class="independienteTab contenedor" onChange="onclickCheckContenedor($(this),0);"> 
									Grupo Familiar. 
							</td>
							<td> 
								&nbsp;
							</td>
						</tr>
						<tr align="left" class="trDPTGrupoFamiliar">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkDPTConvivencias" class="independienteTab contenedor" onChange="onclickCheckContenedor($(this),1);"> 
									Convivencias. 
							</td>
							<td> 
								&nbsp;
							</td>
						</tr>
										
				<tr align="left" class="trDPTGrupoFamiliar trDPTConvivencias">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkDPTCopiaDocumentoConyuge" class="independienteTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="4017"> 
									Fotocopia Documento Conyuge. 
							</td>
							<td> 
								<input type="text" id="txtDPTCopiaDocumentoConyuge" class="independienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trDPTGrupoFamiliar trDPTConvivencias">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img id="imgDPTCertificadoConvivencia" src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkDPTCertificadoConvivencia" class="independienteTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false); onclickCheckOpcional($(this), $('#chkDPTRegistroConvivencia'));" value="4018"> 
									Certificado Convivencia. 
							</td>
							<td> 
								<input type="text" id="txtDPTCertificadoConvivencia" class="independienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trDPTGrupoFamiliar trDPTConvivencias">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img id="imgDPTRegistroConvivencia" src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkDPTRegistroConvivencia" class="independienteTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false); onclickCheckOpcional($(this), $('#chkDPTCertificadoConvivencia')); onclickCheckOpcional($(this), $('#chkDPTDependenciaEconomicaHijo'), true);" value="4019"> 
									Registro Matrimonio. 
							</td>
							<td> 
								<input type="text" id="txtDPTRegistroConvivencia" class="independienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>						
						<tr align="left" class="trDPTGrupoFamiliar">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkDPTHijos" class="independienteTab contenedor" onChange="onclickCheckContenedor($(this),1);"> 
									Hijos menores de 19 a&ntilde;os.
							</td>
							<td> 
								&nbsp;
							</td>
						</tr>
						<tr align="left" class="trDPTGrupoFamiliar trDPTHijos">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img id="imgDPTRegistroCivilHijo" src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkDPTRegistroCivilHijo" class="independienteTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false); onclickCheckOpcional($(this), $('#chkDPTCopiaDocumentoHijo'));" value="4098"> 
									Registro Civil Hijo.
							</td>
							<td> 
								<input type="text" id="txtDPTRegistroCivilHijo" class="independienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trDPTGrupoFamiliar trDPTHijos">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img id="imgDPTCopiaDocumentoHijo" src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkDPTCopiaDocumentoHijo" class="independienteTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false); onclickCheckOpcional($(this), $('#chkDPTRegistroCivilHijo'));" value="4020"> 
									Fotocopia documento Hijo.
							</td>
							<td> 
								<input type="text" id="txtDPTCopiaDocumentoHijo" class="independienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trDPTGrupoFamiliar trDPTHijos">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img id="imgDPTDependenciaEconomicaHijo" src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkDPTDependenciaEconomicaHijo" class="independienteTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="4021"> 
									Formato Dependencia Econ&oacute;mica.
							</td>
							<td> 
								<input type="text" id="txtDPTDependenciaEconomicaHijo" class="independienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trDPTGrupoFamiliar trDPTHijos">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkDPTCertificadoHijo" class="independienteTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="4022"> 
									Certificados.
							</td>
							<td> 
								<input type="text" id="txtDPTCertificadoHijo" class="independienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trDPTGrupoFamiliar">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkDPTHijastros" class="independienteTab contenedor" onChange="onclickCheckContenedor($(this),1);"> 
									Hijastros menores de 19 a&ntilde;os. 
							</td>
							<td> 
								&nbsp;
							</td>
						</tr>
						<tr align="left" class="trDPTGrupoFamiliar trDPTHijastros">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img id="imgDPTRegistroCivilHijastro" src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkDPTRegistroCivilHijastro" class="independienteTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false); onclickCheckOpcional($(this), $('#chkDPTCopiaDocumentoHijastro'));" value="4099"> 
									Registro Civil Hijastro.
							</td>
							<td> 
								<input type="text" id="txtDPTRegistroCivilHijastro" class="independienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trDPTGrupoFamiliar trDPTHijastros">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img id="imgDPTCopiaDocumentoHijastro" src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkDPTCopiaDocumentoHijastro" class="independienteTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false); onclickCheckOpcional($(this), $('#chkDPTRegistroCivilHijastro'));" value="4023"> 
									Fotocopia documento Hijastro.
							</td>
							<td> 
								<input type="text" id="txtDPTCopiaDocumentoHijastro" class="independienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trDPTGrupoFamiliar trDPTHijastros">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img id="imgDPTDependenciaEconomicaHijastro" src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkDPTDependenciaEconomicaHijastro" class="independienteTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false); onclickCheckOpcional($(this), $('#chkDPTSentenciaJudicialHijastro'));" value="4024"> 
									Formato Dependencia Econ&oacute;mica.
							</td>
							<td> 
								<input type="text" id="txtDPTDependenciaEconomicaHijastro" class="independienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trDPTGrupoFamiliar trDPTHijastros">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img id="imgDPTSentenciaJudicialHijastro" src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkDPTSentenciaJudicialHijastro" class="independienteTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false); onclickCheckOpcional($(this), $('#chkDPTDependenciaEconomicaHijastro'));" value="4025"> 
									Sentencia Judicial.
							</td>
							<td> 
								<input type="text" id="txtDPTSentenciaJudicialHijastro" class="independienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trDPTGrupoFamiliar trDPTHijastros">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkDPTCertificadoHijastro" class="independienteTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="4026"> 
									Certificados.
							</td>
							<td> 
								<input type="text" id="txtDPTCertificadoHijastro" class="independienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trDPTGrupoFamiliar">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkDPTHermanos" class="independienteTab contenedor" onChange="onclickCheckContenedor($(this),1);"> 
									Hermanos que dependen econ&oacute;micamente. 
							</td>
							<td> 
								&nbsp;
							</td>
						</tr>
						<tr align="left" class="trDPTGrupoFamiliar trDPTHermanos">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img id="imgDPTRegistroCivilHermano" src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkDPTRegistroCivilHermano" class="independienteTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false); onclickCheckOpcional($(this), $('#chkDPTCopiaDocumentoHermano'));" value="4100"> 
									Registro Civil Hermano.
							</td>
							<td> 
								<input type="text" id="txtDPTRegistroCivilHermano" class="independienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trDPTGrupoFamiliar trDPTHermanos">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img id="imgDPTCopiaDocumentoHermano" src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkDPTCopiaDocumentoHermano" class="independienteTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false); onclickCheckOpcional($(this), $('#chkDPTRegistroCivilHermano'));" value="4027"> 
									Fotocopia documento Hermano.
							</td>
							<td> 
								<input type="text" id="txtDPTCopiaDocumentoHermano" class="independienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trDPTGrupoFamiliar trDPTHermanos">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkDPTRegistroDefuncionPadres" class="independienteTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="4029"> 
									Registro de Defunci&oacute;n Padres.
							</td>
							<td> 
								<input type="text" id="txtDPTRegistroDefuncionPadres" class="independienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trDPTGrupoFamiliar trDPTHermanos">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img id="imgDPTDependenciaEconomicaHermano" src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkDPTDependenciaEconomicaHermano" class="independienteTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false); onclickCheckOpcional($(this), $('#chkDPTSentenciaJudicialHermano'));" value="4028"> 
									Formato Dependencia Econ&oacute;mica.
							</td>
							<td> 
								<input type="text" id="txtDPTDependenciaEconomicaHermano" class="independienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>						
						<tr align="left" class="trDPTGrupoFamiliar trDPTHermanos">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img id="imgDPTSentenciaJudicialHermano" src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkDPTSentenciaJudicialHermano" class="independienteTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false); onclickCheckOpcional($(this), $('#chkDPTDependenciaEconomicaHermano'));" value="4030"> 
									Sentencia Judicial.
							</td>
							<td> 
								<input type="text" id="txtDPTSentenciaJudicialHermano" class="independienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trDPTGrupoFamiliar trDPTHermanos">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkDPTCertificadoHermano" class="independienteTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="4031"> 
									Certificados.
							</td>
							<td> 
								<input type="text" id="txtDPTCertificadoHermano" class="independienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>											
						<tr align="left" class="trDPTGrupoFamiliar">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkDPTPadres" class="independienteTab contenedor" onChange="onclickCheckContenedor($(this),1);"> 
									Padres que dependen econ&oacute;micamente. 
							</td>
							<td> 
								&nbsp;
							</td>
						</tr>
						<tr align="left" class="trDPTGrupoFamiliar trDPTPadres">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkDPTRegistroCivilAfiliado" class="independienteTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="4032"> 
									Registro Civil Afiliado
							</td>
							<td> 
								<input type="text" id="txtDPTRegistroCivilAfiliado" class="independienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trDPTGrupoFamiliar trDPTPadres">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkDPTCopiaDocumentoPadre" class="independienteTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="4033"> 
									Fotocopia documento Padre.
							</td>
							<td> 
								<input type="text" id="txtDPTCopiaDocumentoPadre" class="independienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trDPTGrupoFamiliar trDPTPadres">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkDPTCertificacionEPS" class="independienteTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="4034"> 
									Certificaci&oacute;n EPS.
							</td>
							<td> 
								<input type="text" id="txtDPTCertificacionEPS" class="independienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trDPTGrupoFamiliar trDPTPadres">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkDPTDependenciaEconomicaPadre" class="independienteTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="4035"> 
									Formato Dependencia Econ&oacute;mica.
							</td>
							<td> 
								<input type="text" id="txtDPTDependenciaEconomicaPadre" class="independienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trDPTGrupoFamiliar trDPTPadres">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkDPTCertificadoPadre" class="independienteTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="4036"> 
									Certificados.
							</td>
							<td> 
								<input type="text" id="txtDPTCertificadoPadre" class="independienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
					</table></td>
				</tr>
		
				
				
				
				<!-- Hasta aqui!! -->			
	
				<tr height="20px" >
					<td></td>
				</tr>
				<tr>					
					<td align="center" >
						<img src="<?php echo URL_PORTAL; ?>imagenes/guardar.png" title="Guardar" style="cursor:pointer" onClick="saveRadicacion();"> &nbsp;&nbsp;&nbsp;
						<img src="<?php echo URL_PORTAL; ?>imagenes/devolver2.png" title="Devolver" style="cursor:pointer" onClick="saveDevolucion(1);">
					</td>					
				</tr>
			</table>
		</center>
	</div>
</body>
</html>