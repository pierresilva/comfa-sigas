<?php
set_time_limit ( 0 );
ini_set ( "display_errors", '1' );
date_default_timezone_set ( "America/Bogota" );

$root = $_SERVER ['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'sigas' .DIRECTORY_SEPARATOR . 'session.php'; 
include_once $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';

$FrmActual=basename($_SERVER['PHP_SELF']);
$fecver = date('Ymd h:i:s A',filectime($FrmActual));
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Documentos</title>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>aportes/radicacionnew/js/tabs/documentosTab.js"></script>
</head>
<body>
	<div>
		<div style="text-align:right; height:20px; top:0px; width:221px; position:absolute; left: 20px;"><?php echo 'Versi&oacute;n: ' . $fecver; ?></div>
		<center>
			<h4>LISTADO DOCUMENTOS A SOLICITAR</h4>
			
			<table width="580px" id="tblDocRequeridos">
			
	<!-- FORMULARIO POSTULACION SFV -->
				<tr align="left" width="620px" class="postulacionSFVTab">
					<td> <table margin-left: 17px;" border="0" width="620px">
						<tr align="left">
							<td>
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkPSVFormatoDiligenciado" class="postulacionSFVTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4069"> 
									Formulario de postulaci&oacute;n. 
							</td>
							<td> 
								<input type="text" id="txtPSVFormatoDiligenciado" class="postulacionSFVTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left">
							<td>
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkPSVCedulaAmpliada" class="postulacionSFVTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4129"> 
									Fotocopia de las c&eacute;dulas ampliadas. 
							</td>
							<td> 
								<input type="text" id="txtPSVCedulaAmpliada" class="postulacionSFVTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left">
							<td>
								&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkPSVRegistroCivil" class="postulacionSFVTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4130"> 
									Fotocopia de los registros civiles de los menores de edad. 
							</td>
							<td> 
								<input type="text" id="txtPSVRegistroCivil" class="postulacionSFVTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left">
							<td>
								&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkPSVTarjetaIdentidad" class="postulacionSFVTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4131"> 
									Fotocopia de la tarjeta de identidad de los mayores de 7 a&ntilde;os. 
							</td>
							<td> 
								<input type="text" id="txtPSVTarjetaIdentidad" class="postulacionSFVTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>	
						<tr align="left">
							<td>
								&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkPSVConvivencia" class="postulacionSFVTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4132"> 
									Registro civil de matrimonio o declaraci&oacute;n extrajuicio. 
							</td>
							<td> 
								<input type="text" id="txtPSVConvivencia" class="postulacionSFVTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>						
						<tr align="left">
							<td>
								&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkPSVExtrajuicioCabeza" class="postulacionSFVTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4133"> 
									Declaraci&oacute;n Extrajuicio (MUJER/HOMBRE CABEZA DE HOGAR). 
							</td>
							<td> 
								<input type="text" id="txtPSVExtrajuicioCabeza" class="postulacionSFVTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left">
							<td>
								&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkPSVSupervivencia" class="postulacionSFVTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4134"> 
									Certificado de Supervivencia mayores de 65 a&ntilde;os. 
							</td>
							<td> 
								<input type="text" id="txtPSVSupervivencia" class="postulacionSFVTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left">
							<td>
								&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkPSVDiscapacidad" class="postulacionSFVTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4154"> 
									Certificado m&eacute;dico de discapacidad del integrante del grupo familiar. 
							</td>
							<td> 
								<input type="text" id="txtPSVDiscapacidad" class="postulacionSFVTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left">
							<td>
								&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkPSVCuentaCesantia" class="postulacionSFVTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4135"> 
									Certificado cuenta Ahorro programado y/o cesantias en original (No mayor a 30 d&iacute;as). 
							</td>
							<td> 
								<input type="text" id="txtPSVCuentaCesantia" class="postulacionSFVTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left">
							<td>
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkPSVCuentaCesantiaPoder" class="postulacionSFVTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4136"> 
									Certificado cuenta ahorro programado y/o cesantias en original sin firma por poder. 
							</td>
							<td> 
								<input type="text" id="txtPSVCuentaCesantiaPoder" class="postulacionSFVTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left">
							<td>
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkPSVCapacidadCredito" class="postulacionSFVTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4137"> 
									Certificado de capacidad de credito en original (No mayor a 30 d&iacute;as). 
							</td>
							<td> 
								<input type="text" id="txtPSVCapacidadCredito" class="postulacionSFVTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left">
							<td>
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkPSVCertificadoLaboral" class="postulacionSFVTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4138"> 
									Certificado laboral en original (No mayor a 30 d&iacute;as). 
							</td>
							<td> 
								<input type="text" id="txtPSVCertificadoLaboral" class="postulacionSFVTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left">
							<td>
								&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkPSVMadreComunitaria" class="postulacionSFVTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4139"> 
									Certificado expedido por el ICBF (MADRE COMUNITARIA). 
							</td>
							<td> 
								<input type="text" id="txtPSVMadreComunitaria" class="postulacionSFVTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left">
							<td>
								&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkPSVAfroIndigena" class="postulacionSFVTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4140"> 
									Certificado que acredite condici&oacute;n de miembro afrocolombiano o indigena. 
							</td>
							<td> 
								<input type="text" id="txtPSVAfroIndigena" class="postulacionSFVTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left">
							<td>
								&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkPSVLibertadTradicion" class="postulacionSFVTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4141"> 
									Certificado de libertad y tradici&oacute;n (No mayor a 30 d&iacute;as). 
							</td>
							<td> 
								<input type="text" id="txtPSVLibertadTradicion" class="postulacionSFVTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left">
							<td>
								&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkPSVPropiedadInmueble" class="postulacionSFVTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4142"> 
									Propiedad del inmueble cargada en uno de los integrantes del grupo familiar. 
							</td>
							<td> 
								<input type="text" id="txtPSVPropiedadInmueble" class="postulacionSFVTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left">
							<td>
								&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkPSVPresupuestoVivienda" class="postulacionSFVTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4143"> 
									Presupuesto de la vivienda. 
							</td>
							<td> 
								<input type="text" id="txtPSVPresupuestoVivienda" class="postulacionSFVTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left">
							<td>
								&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkPSVProyectoRadicado" class="postulacionSFVTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4144"> 
									Proyecto Radicado en CCFHUILA. 
							</td>
							<td> 
								<input type="text" id="txtPSVProyectoRadicado" class="postulacionSFVTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left">
							<td>
								&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkPSVAvaluoCatastral" class="postulacionSFVTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4145"> 
									Avaluo Catastral expedido por la entidad competente. 
							</td>
							<td> 
								<input type="text" id="txtPSVAvaluoCatastral" class="postulacionSFVTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left">
							<td>
								&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkPSVResolucionAsignacionLote" class="postulacionSFVTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4146"> 
									Resoluci&oacute;n de asignaci&oacute;n del lote individual. 
							</td>
							<td> 
								<input type="text" id="txtPSVResolucionAsignacionLote" class="postulacionSFVTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left">
							<td>
								&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkPSVResolucionAsignacionSubsidio" class="postulacionSFVTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4147"> 
									Resoluci&oacute;n de asignaci&oacute;n del subsidio diferente al terreno otorgado. 
							</td>
							<td> 
								<input type="text" id="txtPSVResolucionAsignacionSubsidio" class="postulacionSFVTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>	
						<tr align="left">
							<td>
								&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkPSVCertificadoAportes" class="postulacionSFVTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4148"> 
									Certificado de los aportes econ&oacute;micos solidarios de mano de obra ya ejecutada. 
							</td>
							<td> 
								<input type="text" id="txtPSVCertificadoAportes" class="postulacionSFVTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>														
					</table></td>
				</tr>								
	
				<tr height="20px" >
					<td></td>
				</tr>
				<tr>					
					<td align="center" >
						<img src="<?php echo URL_PORTAL; ?>imagenes/guardar.png" title="Guardar" style="cursor:pointer" onClick="saveRadicacion();"> &nbsp;&nbsp;&nbsp;
						<img src="<?php echo URL_PORTAL; ?>imagenes/devolver2.png" title="Devolver" style="cursor:pointer" onClick="saveDevolucion(1);">
					</td>					
				</tr>
			</table>
		</center>
	</div>
</body>
</html>