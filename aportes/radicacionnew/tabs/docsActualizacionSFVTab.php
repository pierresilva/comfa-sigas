<?php
set_time_limit ( 0 );
ini_set ( "display_errors", '1' );
date_default_timezone_set ( "America/Bogota" );

$root = $_SERVER ['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'sigas' .DIRECTORY_SEPARATOR . 'session.php'; 
include_once $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';

$FrmActual=basename($_SERVER['PHP_SELF']);
$fecver = date('Ymd h:i:s A',filectime($FrmActual));
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Documentos</title>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>aportes/radicacionnew/js/tabs/documentosTab.js"></script>
</head>
<body>
	<div>
		<div style="text-align:right; height:20px; top:0px; width:221px; position:absolute; left: 20px;"><?php echo 'Versi&oacute;n: ' . $fecver; ?></div>
		<center>
			<h4>LISTADO DOCUMENTOS A SOLICITAR</h4>
			
			<table width="580px" id="tblDocRequeridos">
			
	<!-- FORMULARIO POSTULACION SFV -->
				<tr align="left" width="620px" class="actualizacionSFVTab">
					<td> <table margin-left: 17px;" border="0" width="620px">
						<tr align="left">
							<td>
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkPSVFormatoDiligenciado" class="actualizacionSFVTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4069"> 
									Formulario de postulaci&oacute;n. 
							</td>
							<td> 
								<input type="text" id="txtPSVFormatoDiligenciado" class="actualizacionSFVTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left">
							<td>
								&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkPSVCedulaAmpliada" class="actualizacionSFVTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4129"> 
									Fotocopia de las c&eacute;dulas ampliadas. 
							</td>
							<td> 
								<input type="text" id="txtPSVCedulaAmpliada" class="actualizacionSFVTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left">
							<td>
								&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkPSVRegistroCivil" class="actualizacionSFVTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4130"> 
									Fotocopia de los registros civiles de los menores de edad. 
							</td>
							<td> 
								<input type="text" id="txtPSVRegistroCivil" class="actualizacionSFVTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left">
							<td>
								&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkPSVTarjetaIdentidad" class="actualizacionSFVTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4131"> 
									Fotocopia de la tarjeta de identidad de los mayores de 7 a&ntilde;os. 
							</td>
							<td> 
								<input type="text" id="txtPSVTarjetaIdentidad" class="actualizacionSFVTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>	
						<tr align="left">
							<td>
								&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkPSVConvivencia" class="actualizacionSFVTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4132"> 
									Registro civil de matrimonio o declaraci&oacute;n extrajuicio. 
							</td>
							<td> 
								<input type="text" id="txtPSVConvivencia" class="actualizacionSFVTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>						
						<tr align="left">
							<td>
								&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkPSVExtrajuicioCabeza" class="actualizacionSFVTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4133"> 
									Declaraci&oacute;n Extrajuicio (MUJER/HOMBRE CABEZA DE HOGAR). 
							</td>
							<td> 
								<input type="text" id="txtPSVExtrajuicioCabeza" class="actualizacionSFVTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left">
							<td>
								&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkPSVSupervivencia" class="actualizacionSFVTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4134"> 
									Certificado de Supervivencia mayores de 65 a&ntilde;os. 
							</td>
							<td> 
								<input type="text" id="txtPSVSupervivencia" class="actualizacionSFVTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left">
							<td>
								&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkPSVDiscapacidad" class="actualizacionSFVTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4154"> 
									Certificado m&eacute;dico de discapacidad del integrante del grupo familiar. 
							</td>
							<td> 
								<input type="text" id="txtPSVDiscapacidad" class="actualizacionSFVTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left">
							<td>
								&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkPSVCuentaCesantia" class="actualizacionSFVTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4135"> 
									Certificado cuenta Ahorro programado y/o cesantias en original (No mayor a 30 d&iacute;as). 
							</td>
							<td> 
								<input type="text" id="txtPSVCuentaCesantia" class="actualizacionSFVTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left">
							<td>
								&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkPSVCuentaCesantiaPoder" class="actualizacionSFVTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4136"> 
									Certificado cuenta ahorro programado y/o cesantias en original sin firma por poder. 
							</td>
							<td> 
								<input type="text" id="txtPSVCuentaCesantiaPoder" class="actualizacionSFVTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left">
							<td>
								&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkPSVCapacidadCredito" class="actualizacionSFVTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4137"> 
									Certificado de capacidad de credito en original (No mayor a 30 d&iacute;as). 
							</td>
							<td> 
								<input type="text" id="txtPSVCapacidadCredito" class="actualizacionSFVTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left">
							<td>
								&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkPSVCertificadoLaboral" class="actualizacionSFVTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4138"> 
									Certificado laboral en original (No mayor a 30 d&iacute;as). 
							</td>
							<td> 
								<input type="text" id="txtPSVCertificadoLaboral" class="actualizacionSFVTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left">
							<td>
								&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkPSVMadreComunitaria" class="actualizacionSFVTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4139"> 
									Certificado expedido por el ICBF (MADRE COMUNITARIA). 
							</td>
							<td> 
								<input type="text" id="txtPSVMadreComunitaria" class="actualizacionSFVTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left">
							<td>
								&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkPSVAfroIndigena" class="actualizacionSFVTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4140"> 
									Certificado que acredite condici&oacute;n de miembro afrocolombiano o indigena. 
							</td>
							<td> 
								<input type="text" id="txtPSVAfroIndigena" class="actualizacionSFVTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left">
							<td>
								&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkPSVLibertadTradicion" class="actualizacionSFVTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4141"> 
									Certificado de libertad y tradici&oacute;n (No mayor a 30 d&iacute;as). 
							</td>
							<td> 
								<input type="text" id="txtPSVLibertadTradicion" class="actualizacionSFVTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left">
							<td>
								&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkPSVPropiedadInmueble" class="actualizacionSFVTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4142"> 
									Propiedad del inmueble cargada en uno de los integrantes del grupo familiar. 
							</td>
							<td> 
								<input type="text" id="txtPSVPropiedadInmueble" class="actualizacionSFVTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left">
							<td>
								&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkPSVPresupuestoVivienda" class="actualizacionSFVTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4143"> 
									Presupuesto de la vivienda. 
							</td>
							<td> 
								<input type="text" id="txtPSVPresupuestoVivienda" class="actualizacionSFVTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left">
							<td>
								&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkPSVProyectoRadicado" class="actualizacionSFVTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4144"> 
									Proyecto Radicado en CCFHUILA. 
							</td>
							<td> 
								<input type="text" id="txtPSVProyectoRadicado" class="actualizacionSFVTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left">
							<td>
								&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkPSVAvaluoCatastral" class="actualizacionSFVTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4145"> 
									Avaluo Catastral expedido por la entidad competente. 
							</td>
							<td> 
								<input type="text" id="txtPSVAvaluoCatastral" class="actualizacionSFVTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left">
							<td>
								&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkPSVResolucionAsignacionLote" class="actualizacionSFVTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4146"> 
									Resoluci&oacute;n de asignaci&oacute;n del lote individual. 
							</td>
							<td> 
								<input type="text" id="txtPSVResolucionAsignacionLote" class="actualizacionSFVTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left">
							<td>
								&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkPSVResolucionAsignacionSubsidio" class="actualizacionSFVTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4147"> 
									Resoluci&oacute;n de asignaci&oacute;n del subsidio diferente al terreno otorgado. 
							</td>
							<td> 
								<input type="text" id="txtPSVResolucionAsignacionSubsidio" class="actualizacionSFVTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>	
						<tr align="left">
							<td>
								&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkPSVCertificadoAportes" class="actualizacionSFVTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4148"> 
									Certificado de los aportes econ&oacute;micos solidarios de mano de obra ya ejecutada. 
							</td>
							<td> 
								<input type="text" id="txtPSVCertificadoAportes" class="actualizacionSFVTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>														
					</table></td>
				</tr>								
	
				<tr height="20px" >
					<td></td>
				</tr>
				<tr>					
					<td align="center" >
						<img src="<?php echo URL_PORTAL; ?>imagenes/guardar.png" title="Guardar" style="cursor:pointer" onClick="saveRadicacion();"> &nbsp;&nbsp;&nbsp;
						<img src="<?php echo URL_PORTAL; ?>imagenes/devolver2.png" title="Devolver" style="cursor:pointer" onClick="saveDevolucion(1);">
					</td>					
				</tr>
			</table>
		</center>
	</div>
</body>
</html>