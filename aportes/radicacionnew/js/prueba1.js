function ajaxContainer(valorIni,url,datos,accion){
	var retorno=valorIni;
	
	$.ajax({
		url: url,
		async: false,
		data: datos,		
		beforeSend: function(objeto){
        	dialogLoading('show');
        },        
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }            
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("Pas� lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,		
		success: function(datoResultado){
			retorno=accion(datoResultado);
		},
		timeout: tiempoFuera,
        type: "GET"
	});
	
	return retorno;
}

function buscarPersona22(cmbtd, txtd, tdnom, nuevo, flag){
	var persona=null;
	idpersonaTab=0;
	idempresaTab=0;
	copyTipoDoc=0;
	flag=7;
	copyDoc='';
	tdnom.html('');
	txtd.removeClass("ui-state-error");
	
	if(validarTexto(txtd,0)>0){return null;}	
	
	var tipodoc=cmbtd.val();
	var doc =txtd.val();
	
	persona = ajaxContainer(persona, 'php/buscarPersona.php', 'v0='+tipodoc+'&v1='+doc+'&v2='+flag,
		function (datoPersona){
			if(datoPersona==0){
				alert("Error en la consulta!");
				return false;
			} else if (datoPersona==1){
				alert("No existe en nuestra base, registre los datos completos!");
				if(nuevo){
					validarIdentificacionInsert(cmbtd,txtd);					
					if(validarTexto(txtd,0)>0){ alert("Documento no valido!"); return null;}
					newPersonaSimple(cmbtd,txtd);
				} else {
					txtd.val('');
					txtd.addClass("ui-state-error");
				}
			} else {
				$.each(datoPersona,function(i,fila){
					persona=fila;
					idpersonaTab=fila.idpersona;
					copyTipoDoc=tipodoc;
					copyDoc=doc;
					var nom = $.trim(fila.pnombre) + " " + $.trim(fila.snombre) + " " + $.trim(fila.papellido) + " " + $.trim(fila.sapellido);					
					tdnom.html(nom);
					
					return;
				});
			}
		}	
	);		
	
	return persona;
}