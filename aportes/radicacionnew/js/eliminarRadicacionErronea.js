var arrIde=new Array();
var arrID=new Array();
var idEmpresa=0;
var idPersona=0;

$(document).ready(function(){
	var smlv=salarioMinimo();
	smlv=(isNaN(smlv) && smlv>0)?616000:smlv;
	$("#txtSMLV").val(smlv);
	

var URL=src();
$("#buscarPor").change(function(){
$("#radicado,#idT").val('');
	if($(this).val()=='2'){
		$("#tipoDocumento").hide();
		$("#radicado").show();
		$("#idT").hide();
	}else{
		$("#tipoDocumento").show();
		$("#radicado").hide();
		$("#idT").show();
		$("#idT").focus();
	}
});

//Buscar Trabajador
$("#buscarT").click(function(){
	eventClick();
});//fin click
});//fin ready

var nuevo=0;
var modificar=0;
var continuar=true;
var msg="";

$(function() {
		$("#ayuda").dialog({
		 	autoOpen: false,
			height: 450, 
			width: 700, 
			draggable:true,
			modal:false,
			open: function(evt, ui){
					$('#ayuda').html('');
					$.get(URL+'help/aportes/modificararfiliaciontrabajador.html',function(data){
							$('#ayuda').html(data);
					});
			 }
		});
		$("#txtFAsignacion").datepicker({
			changeMonth: true,
		    changeYear: true,
		    constrainInput: false,						
		});//fin dialog
	});

function buscarGrupoRadicacion(){
	
	var v0=$("#idT").val();
	var v1=$("#tipoDocumento").val();
	var v2=$("#radicado").val();
	var v3=$("#buscarPor").val();

	
	
	//BUSCAMOS LOS POSIBLES RADICADOS A ELIMINAR 
	$.ajax({
		url:"buscarRadicacionError.php",
		async: false,
		data :{v0:v0,v1:v1,v2:v2,v3:v3},		
		beforeSend: function(objeto){
        	dialogLoading('show');
        },        
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }            
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("En buscar Radicado Paso lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
		success:function(data){
			$("#tabTableRadicado tbody tr").remove();	
			if(data){
				var cadena="";

				$.each(data,function(i,fila){
					
					cadena+="<tr id='idtabla" + fila.idradicacion +"'>" +
								"<td style='text-align:right'  >" + fila.idpersona + "</td>" +
								"<td style='text-align:center' >" + fila.identificacion + "</td>" +
								"<td style='text-align:center' >" + fila.trabajador + "</td>" +
								"<td style='text-align:center' >" + fila.idradicacion + "</td>" +
								"<td style='text-align:center' >" + fila.procesado + "</td>" +
								"<td style='text-align:center' >" + fila.fecharadicacion + "</td>" +
								"<td style='text-align:center' >" + fila.fechaproceso + "</td>" +
								"<td style='text-align:center' >" + fila.detalledefinicion + "</td>" +
							    "<td style='text-align:center' >" + fila.nit + "</td>" +
							    "<td style='text-align:center' >" + fila.razonsocial + "</td>" +
							    "<td style='text-align:center' ><img style='cursor:pointer' src='"+URL+"imagenes/menu/ico_error.png' title='Eliminar Radicado' " +
							    "onclick=eliminarRadicacion(" + fila.idradicacion + "," + fila.idpersona +",'idtabla"+ fila.idradicacion +"'); /></td>"
					    	"</tr>";					
				});//each
				con=data.length;
				$("#tabTableRadicado tbody").append(cadena);
			} else {
				msg='';
				msg+="No hay radicados para eliminar.";
				MENSAJE(msg);
				return false;
			}			
		},//succes
		type: "POST"
	});//ajax radicado		
}

function observacion(idradicacion,idpersona){
	var URL=src();
	$("div[name='div-observaciones-tab']").dialog("destroy");
	$("textarea[name='observacionGrupo']").val("");
	$("div[name='div-observaciones-tab']").dialog({
		  modal:true,
		  width:520,
		  resizable:false,
		  draggable:false,
		  closeOnEscape:false,
		  open:function(){
			  $(this).prev().children().hide();//Oculto la barra de titulo
			  $(this).prev().html("OBSERVACIONES...");//Pongo un nuevo titulo
			  $("textarea[name='observacionGrupo']").focus();
			  },
		  buttons:{
		    'Guardar':function(){
		    	var comentario='Radicacion anulada';
		    	var observacioncaptura=$("textarea[name='observacionGrupo']").val();
				var observacion=comentario+' - '+observacioncaptura;
				var observacionSinEspacios = observacion.replace(/^\s+|\s+$/g,"");
				//Si observacion tiene algun valor, se guarda de lo contrario no pasa nada
			    if(observacionSinEspacios!=""){
				 $.ajax({
					   url:"insertNotas.php",
					   type:"POST",
					   data:{v0:idradicacion,v1:idpersona,v2:observacion},
					   success:function(data){
					    $("[name='rtaObservacion']").fadeIn();//el mensaje esta oculto en la capa div-observacion
						$("div[name='div-observaciones-tab']").dialog("close");
					   }
			     });//ajax
				 alert("Observacion guardada"); 
				}//if observacion
				else{
					alert("No se pudo guardar la observaci\xf3n");
				}
			 }//guardar
		  }//buttons
		  });   
} 
function eliminarRadicacion(idradicacion,idpersona,idTr){
	if(confirm('Realmente desea eliminar el Registro ?'))
	{   

	$.ajax({
		url:"eliminarRadicado.php",
		async: false,
		data :{v0:idradicacion,v1:idpersona},		
		beforeSend: function(objeto){
        	dialogLoading('show');
        },        
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }            
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("En eliminar Radicado Paso lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
		type: "POST",
		success:function(datos){
			if(datos==1)
				{
				$("#"+idTr).remove();	
				alert("El Proceso de anular la radicacion fue exitoso");
				observacion(idradicacion,idpersona);
				}
			else
				{
				alert("El proceso de anular radicacion No se realizo");				
				}
		}
        
	});//$.ajax({	
	}
}