/** VARIABLES LOCALES **/
var idPersonaLocal=0;

$(document).ready(function(){
	$("#filaCertificados,#filaVigencias,#filaFechas,#imgCertificado").hide();
	
	$("#cmbIdTipoDocumentoAfiliado").val(copyTipoDoc);
	$("#txtNumero").val(copyDoc);
	
	setTimeout("$('#tabs').trigger('click');$('#cmbIdTipoDocumentoAfiliado').focus();",700);
});

/**
 * Busca la persona a afiliar
 * 
 * @param cmbtd 		combo que contiene el tipo de documento
 * @param txtd 			text que contiene el documento
 * @param tdnom 		td que contiene el nombre del afiliado
 * @returns {Boolean}
 */
function busquedaPersona(cmbtd, txtd, tdnom){
	idPersonaLocal=0;
	tdnom.html('');
	$("#tbRelacionesActivas").empty();	
	
	var persona=buscarPersona(cmbtd, txtd, tdnom, false);
	if(persona==null || persona==false){ 
		txtd.val('');
		txtd.addClass("ui-state-error"); 
		return false; 
	}
	
	var opcion=comprobarTipoAfiliacion(persona.idpersona,1);
	
	if((opcion['0']-opcion['2'])>0){
		var afiliaciones = buscarAfiliacionesActivasAfiliado(persona.idpersona);
		var contA=0;
		
		if(afiliaciones!=null){
			$.each(afiliaciones,function(i,fila){					
			    if(fila.estado=="A"){ contA++; } 
			});
		}
		
		if(contA>0){
			idPersonaLocal=persona.idpersona;
			var beneficiarios=buscarBeneficiarios(persona.idpersona,1);
			
			if(beneficiarios!=null){
				var cadena="";
				$.each(beneficiarios,function(i,fila){					
					nom = fila.pnombre + " " +fila.snombre +" "+ fila.papellido + " " + fila.sapellido;
					cadena+="<tr>" +
								"<td>"+fila.codigo+"</td>" +
								"<td>"+fila.identificacion+"</td>" +
								"<td>"+nom+"</td>" +
								"<td><center>"+fila.detalledefinicion+"</center></td>" +
								"<td><center>"+fila.estado+"</center></td>" +
							"</tr>";			
				});
				
				$("#tbRelacionesActivas").append(cadena);
			}
		} else {
			alert("La persona que esta radicando solo tiene AFILIACION(ES) PENDIENTE(S)!!");
			txtd.val(''); tdnom.html('');
			txtd.addClass("ui-state-error");
		}
	} else {
		if (opcion['2']>0){
			alert("La persona que esta radicando tiene AFILIACION(ES) PENDIENTE(S) por PU!");			
		} else if (opcion['1']>0){
			alert("La persona que esta radicando esta INACTIVA!");
		} else {
			alert("La persona que esta radicando no tiene AFILIACIONES!");
		}
		
		txtd.val(''); tdnom.html('');
		txtd.addClass("ui-state-error");
	}	
}

/**
 * Guardar la Radicacion
 */
function saveRadicacion(){
	if(iniciarconN()){
		if(validarTodosLosCampos()){
			llenarLasVariables();
			
			nRadicacion.idradicacion=guardarRadicacion(nRadicacion);
			
			if(esNumeroRespuesta(nRadicacion.idradicacion)){					
				$("#txtHoraFin").val(nRadicacion.horafinal);
				nuevaRad=0;
					
				alert("Radicaci\u00F3n guardada correctamente!! \nRADICACI\u00D3N # "+nRadicacion.idradicacion);
				
				$("#txtIdRadicacion").css({ color: "RED"});
				$("#txtIdRadicacion").val(nRadicacion.idradicacion);
				$("#titRadicacion").trigger('click');
				return true;				 
			} else {
				alert("Radicaci\u00F3n NO FUE GUARDADA!!");
				return false;
			}
		}
	}
}

function saveDevolucion(opt){
	if(iniciarconN()){
		if(validarTodosLosCampos(opt)){
			if(opt==1) { enaDivMotivosDevolucion(); return false; }			
			
			var idMotivoDevolucion=$("#cmbMotivosDevolucion").val();
			llenarLasVariablesRadicacion(1);
			nRadicacion.motivodevolucion=idMotivoDevolucion;
			$("#divMotivosDevolucion").dialog('close');
			
			nRadicacion.idradicacion=guardarDevolucion(nRadicacion);
			if(esNumeroRespuesta(nRadicacion.idradicacion)){
				nuevaRad=0;
				$("#txtHoraFin").val(nRadicacion.horafinal);
				alert("Devoluci\u00F3n guardada correctamente!! \nRADICACI\u00D3N # "+nRadicacion.idradicacion);												
						
				$("#txtIdRadicacion").css({ color: "RED"});
				$("#txtIdRadicacion").val(nRadicacion.idradicacion);
				$("#titRadicacion").trigger('click');
				return true;
			} else {
				alert("Devoluci\u00F3n NO FUE GUARDADA!!"); 
				return false;
			}			
		}
	}
}

/**
 * Valida todos los campos necesarios para la radicacion 
 */
function validarTodosLosCampos(opt){
	var error=validarCampos();
	
	error=validarSelect($("#cmbIdTipoDocumentoAfiliado"),error);
	if(esNumeroRespuesta(idPersonaLocal)==false){ $("#txtNumero").val(''); }
	error=validarTexto($("#txtNumero"),error);
	if(esNumeroRespuesta($("#txtBeneficiarios").val())==false){ $("#txtBeneficiarios").val(''); }
	error=validarNumero($("#txtBeneficiarios"),error);
	error=validarNumero($("#txtFolios"),error);
	
	if(error>0){
		alert("Llene los campos obligatorios!");
		nRadicacion=null;
		return false;
	} else {
		if(opt==1){ return true; }
		else if(opt==2){
			error=validarSelect($("#cmbMotivosDevolucion"),error);
			if(error>0){
				nRadicacion=null;
				return false;
			} else {
				return true;
			}
		}
		
		return true;
	}
}

function llenarLasVariablesRadicacion(opt){
	nRadicacion.idtipodocumentoafiliado=$("#cmbIdTipoDocumentoAfiliado").val();
	nRadicacion.numero=$("#txtNumero").val();	
	nRadicacion.folios=$("#txtFolios").val();
	nRadicacion.notas=$.trim($("#txtNotas").val());
	nRadicacion.numerobeneficiario=$("#txtBeneficiarios").val();
	
	if(opt==1){
		nRadicacion.folios=0;
		nRadicacion.asignado="S";
		nRadicacion.procesado="S";
		nRadicacion.anulada="S";
		nRadicacion.devuelto="S";
		nRadicacion.cierre="S";
	}
}

/**
 * LLena las variables necesarias para la radicacion
 */
function llenarLasVariables(opt){
	llenarLasVariablesRadicacion(0);
}