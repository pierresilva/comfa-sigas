/** VARIABLES LOCALES **/
var folios=0;
var idPersonaLocal=0;
var beneficiarios=null;

$(document).ready(function(){
	$("#cmbIdTipoDocumentoAfiliado").val(copyTipoDoc);
	$("#txtNumero").val(copyDoc);
	
	$(".trDFTCertificadoRetiro").hide();
	setTimeout("$('#tabs').trigger('click');$('#cmbIdTipoDocumentoAfiliado').focus();",700);
});

/**
 * Busca la persona a afiliar
 * 
 * @param cmbtd 		combo que contiene el tipo de documento
 * @param txtd 			text que contiene el documento
 * @param tdnom 		td que contiene el nombre del afiliado
 * @returns {Boolean}
 */
function busquedaPersona(cmbtd, txtd, tdnom){
	idPersonaLocal=0;	
	tdnom.html('');
	beneficiarios=null;
	$("#cmbIdFallecido option:not(':first')").remove();	
	$("#cmbIdParentesco").val(0).trigger("change");
	
	var persona=buscarPersona(cmbtd, txtd, tdnom, false);
	if(persona==null || persona==false){ 
		txtd.val('');
		txtd.addClass("ui-state-error"); 
		return false; 
	}
	
	beneficiarios=buscarBeneficiarios(persona.idpersona,0);
	
	if(beneficiarios==null){
		alert("La persona que esta radicando NO TIENE BENEFICIARIOS!!");
		txtd.val(''); tdnom.html('');
		txtd.addClass("ui-state-error");
	} else {
		idPersonaLocal=persona.idpersona;
		
		beneficiarios[beneficiarios.length] = {
				idbeneficiario : persona.idpersona,
				identificacion : persona.identificacion, 
				pnombre : persona.pnombre,
				snombre : persona.snombre,
				papellido : persona.papellido, 
				sapellido : persona.sapellido, 
				idparentesco : 0
		};		
	}	
}

/**
 * Evento al seleccionar un parentesco
 */
function onChangeParentesco(obj){
	var opt=obj.value; var cont=0;
	$("#cmbIdFallecido option:not(':first')").remove();
	$(".trDFTCertificadoRetiro").hide();
	
	if(opt==0 || esNumeroRespuesta(idPersonaLocal)==false){ obj.value=0; return false; }
	
	$.each(beneficiarios,function(i,fila){
		if((opt==1 && fila.idparentesco!=0) || (opt==2 && (fila.idparentesco!=35 && fila.idparentesco!=38)) || (opt==3 && fila.idparentesco!=37) || (opt==4 && fila.idparentesco!=36)){ return; }
		cont++;
		var nom = fila.identificacion + " - " + fila.pnombre + " " + fila.snombre + " " + fila.papellido + " " + fila.sapellido;					
		$('#cmbIdFallecido').append("<option value='" + fila.idbeneficiario + "'>" + nom + "</option>"); 
	});
	
	if(cont==0){
		alert("No tiene beneficiarios activos que correspondan al parentesco seleccionado!!!");
		obj.value=0;
	} else if (opt==1) { $(".trDFTCertificadoRetiro").show(); }
}

/**
 * Evento al seleccionar un beneficiario
 */
function onChangeFallecido(obj){
	var opt=obj.value;	
	if(idPersonaGlobal==opt){
		alert("El fallecido NO puede ser la misma persona que solicita la radicacion!");
		obj.value=0; 
	}
}

/**
 * Guardar la Radicacion
 */
function saveRadicacion(){
	if(iniciarconN()){
		if(validarTodosLosCampos()){		
			llenarLasVariables();
			
			nRadicacion.idradicacion=guardarRadicacion(nRadicacion);
			if(esNumeroRespuesta(nRadicacion.idradicacion)){
				var resFolios=guardarDocumentos(listDocumentos, nRadicacion.idradicacion, idPersonaLocal, "S");
				
				if(esNumeroRespuesta(resFolios)){
					$("#txtHoraFin").val(nRadicacion.horafinal);
					nuevaRad=0;
						
					alert("Radicaci\u00F3n guardada correctamente!! \nRADICACI\u00D3N # "+nRadicacion.idradicacion);
					$("#txtIdRadicacion").css({ color: "RED"});
					$("#txtIdRadicacion").val(nRadicacion.idradicacion);
					$("#titRadicacion").trigger('click');
					return true;
				} else {
					if(anularRadicacion(nRadicacion.idradicacion)){
						alert("Ocurrio un error al guardar los Documentos!!" +
								"\nPOR FAVOR vuelva a intentarlo");
					} else {
						alert("Ocurrio un error al anular la radicacion!!" +
								"\nPOR FAVOR reporte este numero a soporte:" +
								"\# "+nRadicacion.idradicacion);
					}											
					return false;
				}
			} else {
				alert("Radicaci\u00F3n NO FUE GUARDADA!!");
				return false;
			}
		}
	}
}

/**
 * Valida todos los campos necesarios para la radicacion 
 */
function validarTodosLosCampos(){
	var error=validarCampos();
	
	error=validarSelect($("#cmbIdTipoDocumentoAfiliado"),error);
	if(esNumeroRespuesta(idPersonaLocal)==false){ $("#txtNumero").val(''); }
	error=validarTexto($("#txtNumero"),error);
	error=validarSelect($("#cmbIdParentesco"),error);
	error=validarSelect($("#cmbIdFallecido"),error);	
	
	if(error>0){
		$("#titDetRadicacion").trigger('click');
		alert("Llene los campos obligatorios!");
		nRadicacion=null;
		return false;
	} else {
		folios=validarRequeridos();
		if(esNumeroRespuesta(folios)){ return true; }
		else {
			nRadicacion=null;
			return false;
		}
	}
}

/**
 * LLena las variables necesarias para la radicacion
 */
function llenarLasVariables(){
	nRadicacion.idtipodocumentoafiliado=$("#cmbIdTipoDocumentoAfiliado").val();
	nRadicacion.numero=$("#txtNumero").val();	
	nRadicacion.folios=folios;
	nRadicacion.notas=$.trim($("#txtNotas").val());
	nRadicacion.idtipocertificado=$("#cmbIdParentesco").val();
	nRadicacion.idbeneficiario=$("#cmbIdFallecido").val();
}