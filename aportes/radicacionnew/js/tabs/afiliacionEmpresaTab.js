/** VARIABLES LOCALES **/
var objEmpresa=null;
var folios=0;
var esActualizacion=false;

$(document).ready(function(){	
	$("#txtNit").val(copyNit);
	copyNit='';
	
	setTimeout("$('#chkAFEPersonaJuridica').attr('checked',true).trigger('change');",500);
	setTimeout("$('#tabs').trigger('click');$('#txtNit').focus();",700);
});

/**
 * Busca la empresa donde labora el afiliado
 * 
 * @param txtNit 			Nit de la empresa
 * @param lblRazonSocial 	Nombre de la empresa
 * @returns {Boolean}
 */
function busquedaEmpresa(txtNit, txtRazonSocial){
	objEmpresa=null;
	esActualizacion=false;
	txtRazonSocial.val('');
	txtRazonSocial.removeClass("ui-state-error");
	$("#txtRazonSocial").removeAttr('disabled');
	txtNit.removeClass("ui-state-error");
	
	if(validarNumero(txtNit,0)>0){return null;}
	
	var empresa=buscarNit(txtNit,1);		
	if(empresa==null || empresa==false){
			txtRazonSocial.focus();
			return true; 
	} else {
		if(empresa.estado=='P'){
			if(empresa.legalizada=="N" && radicacionPendienteEmpresa(txtNit)==false){
				objEmpresa=empresa;
				esActualizacion=true;
				txtRazonSocial.val(empresa.razonsocial);
				$("#txtRazonSocial").attr('disabled', 'disabled');
				return true;
			} else {
				alert("La EMPRESA que esta radicando esta PENDIENTE. \nTIENE RADICACIONES POR GRABAR!!");
				txtNit.val('');
				txtNit.addClass("ui-state-error");
				return false;
			} 
		} else if(empresa.estado=='I') {
			copyNit=txtNit.val();
			alert("La EMPRESA que esta radicando esta INACTIVA. \nLa Radicaci\u00F3n se cambia a RENOVACION!!");
			$("#cmbIdTipoRadicacion").val('31').trigger('change');
		} else {
			if(empresa.contratista=="S"){
				objEmpresa=empresa;
				esActualizacion=true;
				txtRazonSocial.val(empresa.razonsocial);
				return true;
			} else if (empresa.idtipoafiliacion != 3316) {
				objEmpresa=empresa;
				esActualizacion=true;
				txtRazonSocial.val(empresa.razonsocial);
				return true;
			} else {
				alert("La EMPRESA que esta radicando esta ACTIVA. \nYA EXISTE!!");
				txtNit.val('');
				txtNit.addClass("ui-state-error");
				return false;
			}
		}
	}
}

/**
 * Guardar la Radicacion
 * 
 * @returns {Boolean}
 */
function saveRadicacion(){
	if(iniciarconN()){
		if(validarTodosLosCampos()){
			var nEmpresa=new Empresa();
			nEmpresa=llenarLasVariables(nEmpresa);			
			
			if(nEmpresa==null || nEmpresa==false){				
				nRadicacion=null;
				alert("Se encontr\u00F3 errores en el procesamiento de su solicitud.");				
				return false;
			} else {				
				nRadicacion.idradicacion=guardarRadicacion(nRadicacion);
				if(esNumeroRespuesta(nRadicacion.idradicacion)){
					nuevaRad=0;
					$("#txtHoraFin").val(nRadicacion.horafinal);					
					if(esActualizacion==false) {
						nEmpresa.idempresa=guardarEmpresa(nEmpresa);
					} else {						
						nEmpresa.idempresa=actualizarEmpresa(nEmpresa);
						if(esNumeroRespuesta(nEmpresa.idempresa)){ nEmpresa.idempresa=objEmpresa.idempresa; }
					}
					
					if(esNumeroRespuesta(nEmpresa.idempresa)){
						var resFolios=guardarDocumentos(listDocumentos, nRadicacion.idradicacion, nEmpresa.idempresa, "S");
						
						if(esNumeroRespuesta(resFolios)){
							alert("Radicaci\u00F3n guardada correctamente!! \nRADICACI\u00D3N # "+nRadicacion.idradicacion);
						} else {
							alert("Radicaci\u00F3n guardada con error!! \nRADICACI\u00D3N # " + nRadicacion.idradicacion +
									"\nOcurrio un error al guardar los Documentos!!");
						}
						
						$("#txtIdRadicacion").css({ color: "RED"});
						$("#txtIdRadicacion").val(nRadicacion.idradicacion);
						$("#titRadicacion").trigger('click');
						return true;
					} else {
						if(anularRadicacion(nRadicacion.idradicacion)){
							alert("Ocurrio un error!!" +
									"\nPOR FAVOR vuelva a intentarlo");
						} else {
							alert("Ocurrio un error!!" +
									"\nPOR FAVOR reporte este numero a soporte:" +
									"\# "+nRadicacion.idradicacion);
						}						
						return false;
					}					
				} else {
					alert("Radicaci\u00F3n NO FUE GUARDADA!!"); 
					return false;
				}
			}
		}
	}
}

/**
 * Valida todos los campos necesarios para la radicacion 
 * 
 * @returns {Boolean}
 */
function validarTodosLosCampos(){
	var error=validarCampos();
	
	if(esActualizacion && esNumeroRespuesta(objEmpresa.idempresa)==false){ $("#txtNit").val(''); $("#txtRazonSocial").val(''); }
	error=validarNumero($("#txtNit"),error);
	error=validarTexto($("#txtRazonSocial"),error);
	
	if(error>0){
		$("#titDetRadicacion").trigger('click');
		alert("Llene los campos obligatorios!");
		nRadicacion=null;
		return false;
	} else {
		folios=validarRequeridos();
		if(esNumeroRespuesta(folios)){ return true; }
		else {
			nRadicacion=null;
			return false;
		}	
	}
}

/**
 * LLena las variables necesarias para la radicacion
 * 
 * @param nAfiliacion
 * @returns {Afiliacion}
 */
function llenarLasVariables(nEmpresa){
	nRadicacion.nit=$("#txtNit").val();
	nRadicacion.folios=folios;
	nRadicacion.notas=$.trim($("#txtNotas").val());	
	
	if(esActualizacion){
		if(esNumeroRespuesta(objEmpresa.idempresa)){
			nEmpresa=camposEmpresa(objEmpresa);
			nEmpresa.codigoestado='';
			nEmpresa.fechaestado='';
			if (nEmpresa.idtipoafiliacion == 3316) {
				nEmpresa.porplanilla='S';
			} else {
				nEmpresa.claseaportante='';
				nEmpresa.tipoaportante=2656;
				nEmpresa.indicador=108;
			}
		} else {
			return null;
		}
	} else { nEmpresa=camposEmpresa(nEmpresa); }
		
	nEmpresa.idtipodocumento=5;
	nEmpresa.nit=$("#txtNit").val();
	nEmpresa.codigosucursal='000';
	nEmpresa.principal='S';
	nEmpresa.razonsocial=$("#txtRazonSocial").val();
	nEmpresa.sigla=$("#txtRazonSocial").val();
	nEmpresa.contratista='N';	
	nEmpresa.estado='P';
	nEmpresa.legalizada='N';
	nEmpresa.renovacion='N';
	nEmpresa.idtipoafiliacion=3316;
	nEmpresa.idpais=48;
	
	return nEmpresa;	
}