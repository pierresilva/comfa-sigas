$(document).ready(function(){
	setTimeout("$('#tabs').trigger('click');$('#cmbIdTipoInformacion').focus();",700);
});

function saveRadicacion(){
	if(iniciarconN()){
		if(validarTodosLosCampos()){			
			llenarLasVariables();			
			
			nRadicacion.idradicacion=guardarRadicacion(nRadicacion);
			
			if(esNumeroRespuesta(nRadicacion.idradicacion)){
				$("#txtHoraFin").val(nRadicacion.horafinal);
				nuevaRad=0;
				
				if(cerrarRadicacion(nRadicacion.idradicacion)){
					alert("Radicaci\u00F3n guardada correctamente!! \nRADICACION # "+nRadicacion.idradicacion);						
					
					$("#txtIdRadicacion").css({ color: "RED"});
					$("#txtIdRadicacion").val(nRadicacion.idradicacion);
					$("#titRadicacion").trigger('click');
					return true;				
				} else {
					alert("Ocurrio un error!!" +
							"\nPOR FAVOR reporte este n\u00FAmero a soporte:" +
							"\# "+nRadicacion.idradicacion);
					return false;
				}
			} else {
				alert("Radicaci\u00F3n NO FUE GUARDADA!!"); 
				return false;
			}
		}		
	}		
}

function validarTodosLosCampos(){
	var error=validarCampos();
	
	error=validarSelect($("#cmbIdTipoInformacion"),error);	
	
	if(error>0){
		alert("Llene los campos obligatorios!");
		nRadicacion=null;
		return false;
	} else
		return true;
}

function llenarLasVariables(){
	nRadicacion.idtipoinformacion=$("#cmbIdTipoInformacion").val();
	nRadicacion.notas=$.trim($("#txtNotas").val());
	nRadicacion.idtipodocumentoafiliado=nRadicacion.idtipodocumento;
	nRadicacion.numero=nRadicacion.identificacion;
}