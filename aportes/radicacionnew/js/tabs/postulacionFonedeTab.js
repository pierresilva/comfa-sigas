/** VARIABLES LOCALES **/
var folios=0;
var idPersonaLocal=0;

$(document).ready(function(){
	$("#cmbIdTipoDocumentoAfiliado").val(copyTipoDoc);
	$("#txtNumero").val(copyDoc);
	
	setTimeout("$('#tabs').trigger('click');$('#cmbIdTipoDocumentoAfiliado').focus();",700);
});

/**
 * Busca la persona a afiliar
 * 
 * @param cmbtd 		combo que contiene el tipo de documento
 * @param txtd 			text que contiene el documento
 * @param tdnom 		td que contiene el nombre del afiliado
 * @returns {Boolean}
 */
function busquedaPersona(cmbtd, txtd, tdnom){
	idPersonaLocal=0;	
	tdnom.html('');	
	$("#tdEstadoAfiliado,#tdTipoPostulacion,#tdTiempoVinculacion").html('');
	$(".conPostulacion").hide();
	$(".conPostulacion input[type=checkbox]").removeAttr('checked').trigger("change");
	
	var persona=buscarPersona(cmbtd, txtd, tdnom, true);
	if(persona==null || persona==false){ 
		txtd.focus();
		txtd.addClass("ui-state-error"); 
		return false; 
	}
	
	var salir=false;
	var opcion=comprobarTipoAfiliacion(persona.idpersona,1);
	
	if(opcion instanceof Array){	
		if((opcion['0']-opcion['2'])>0){
			var afiliaciones = buscarAfiliacionesActivasAfiliado(persona.idpersona);
			var contA=0; contP=0;
			
			if(afiliaciones!=null){
				$.each(afiliaciones,function(i,fila){					
				    if(fila.estado=="A"){ contA++; }
				    else if(fila.estado=="P"){ contP++; }
				});
			}
			
			if(contA>0){
				$("#tdEstadoAfiliado").html("&nbsp;ACTIVO");
				alert("La persona que esta radicando se encuentra en estado Activo!!\n" +
						"No es posible continuar.");
				salir=true;
				
			} else if((contP-opcion['2'])>0) {
				$("#tdEstadoAfiliado").html("&nbsp;PENDIENTE");
				alert("La persona que esta radicando se encuentra en estado Pendiente!!\n" +
				"No es posible continuar.");
				salir=true;
			} else {
				$("#tdEstadoAfiliado").html("&nbsp;PENDIENTE POR PU");
			}
		} else {
			if(opcion['1']>0){
				$("#tdEstadoAfiliado").html("&nbsp;INACTIVO");
			} else if(opcion['2']>0){
				$("#tdEstadoAfiliado").html("&nbsp;PENDIENTE POR PU");			
			} else {
				$("#tdEstadoAfiliado").html("&nbsp;SIN AFILIACIONES");
			}
		}
	} else {
		alert("Ocurri\u00F3 un error comprobando el tipo de afiliacion de la persona.\n" +
				"Por favor comuniquese con el administrador del sistema.");	
		salir=true;
		
	}
	
	if(salir==false){
		var contRad=buscarRadicacionPendienteAfiliado(69, cmbtd.val(), txtd.val());
		
		if(contRad<0){
			alert("Ocurri\u00F3 un error buscando radicaciones de la persona.\n" +
				"Por favor comuniquese con el administrador del sistema.");
			salir=true;
		} else if(contRad>0){
			alert("La persona a la cual esta registrando ya se encuentra radicado.");
			salir=true;
		} else {
			idPersonaLocal=persona.idpersona;
			
			var dias=buscarTiempoVinculacion(persona.idpersona);
			var tipoPostulacion = "&nbsp;SIN VINCULACION";
			$("#tdTiempoVinculacion").html("&nbsp" + dias);
			if(dias>360){ 
				tipoPostulacion = "&nbsp;CON VINCULACION";				
				$(".conPostulacion").show();
			}
			$("#tdTipoPostulacion").html(tipoPostulacion);			
		}
	} 
	
	if(salir==true) {
		txtd.val(''); tdnom.html(''); $("#tdEstadoAfiliado,#tdTipoPostulacion,#tdTiempoVinculacion").html('');
		txtd.addClass("ui-state-error");
		return false;
	} else {
		return true;
	}
}

/**
 * Guardar la Radicacion
 * 
 * @returns {Boolean}
 */
function saveRadicacion(){
	if(iniciarconN()){
		if(validarTodosLosCampos()){
			llenarLasVariables();			
			
			nRadicacion.idradicacion=guardarRadicacion(nRadicacion);
			
			if(esNumeroRespuesta(nRadicacion.idradicacion)){
				nuevaRad=0;
				$("#txtHoraFin").val(nRadicacion.horafinal);
				var resFolios=guardarDocumentos(listDocumentos, nRadicacion.idradicacion, idPersonaLocal, "F");
					
				if(esNumeroRespuesta(resFolios)){										
					alert("Radicaci\u00F3n guardada correctamente!! \nRADICACI\u00D3N # " + nRadicacion.idradicacion);					
				} else {
					alert("Radicaci\u00F3n guardada con error!! \nRADICACI\u00D3N # " + nRadicacion.idradicacion +
							"\nOcurrio un error al guardar los Documentos!!");
				}
				
				$("#txtIdRadicacion").css({ color: "RED"});
				$("#txtIdRadicacion").val(nRadicacion.idradicacion);
				$("#titRadicacion").trigger('click');
				return true;
			} else {
				alert("Radicaci\u00F3n NO FUE GUARDADA!!"); 
				return false;
			}
		}
	}
}

function saveDevolucion(opt){
	if(iniciarconN()){
		if(validarTodosLosCampos(opt)){
			if(opt==1) { enaDivMotivosDevolucion(); return false; }			
			
			var idMotivoDevolucion=$("#cmbMotivosDevolucion").val();
			llenarLasVariablesRadicacion(1);
			nRadicacion.motivodevolucion=idMotivoDevolucion;
			$("#divMotivosDevolucion").dialog('close');
			
			nRadicacion.idradicacion=guardarDevolucion(nRadicacion);
			if(esNumeroRespuesta(nRadicacion.idradicacion)){
				nuevaRad=0;
				$("#txtHoraFin").val(nRadicacion.horafinal);
				alert("Devoluci\u00F3n guardada correctamente!! \nRADICACI\u00D3N # "+nRadicacion.idradicacion);												
						
				$("#txtIdRadicacion").css({ color: "RED"});
				$("#txtIdRadicacion").val(nRadicacion.idradicacion);
				$("#titRadicacion").trigger('click');
				return true;
			} else {
				alert("Devoluci\u00F3n NO FUE GUARDADA!!"); 
				return false;
			}			
		}
	}
}

/**
 * Valida todos los campos necesarios para la radicacion 
 * 
 * @returns {Boolean}
 */
function validarTodosLosCampos(opt){
	var error=validarCampos();
	
	error=validarSelect($("#cmbIdTipoDocumentoAfiliado"),error);
	if(esNumeroRespuesta(idPersonaLocal)==false){ $("#txtNumero").val(''); }
	error=validarTexto($("#txtNumero"),error);			
	
	if(error>0){
		$("#titDetRadicacion").trigger('click');
		alert("Llene los campos obligatorios!");
		nRadicacion=null;
		return false;
	} else {
		if(opt==1){ return true; }
		else if(opt==2){
			error=validarSelect($("#cmbMotivosDevolucion"),error);
			if(error>0){
				nRadicacion=null;
				return false;
			} else {
				return true;
			}
		}		
		
		folios=validarRequeridos();
		if(esNumeroRespuesta(folios)){ return true; }
		else {
			nRadicacion=null;
			return false;
		}
	}
}

function llenarLasVariablesRadicacion(opt){
	nRadicacion.idtipodocumentoafiliado=$("#cmbIdTipoDocumentoAfiliado").val();
	nRadicacion.numero=$("#txtNumero").val();	
	nRadicacion.folios=folios;
	nRadicacion.notas=$.trim($("#txtNotas").val());
	nRadicacion.procesado='N';
	
	if(opt==1){
		nRadicacion.folios=0;
		nRadicacion.asignado="S";
		nRadicacion.procesado="S";
		nRadicacion.anulada="S";
		nRadicacion.devuelto="S";
		nRadicacion.cierre="S";
	}
}

/**
 * LLena las variables necesarias para la radicacion
 * 
 * @param nAfiliacion
 * @returns {Afiliacion}
 */
function llenarLasVariables(){
	llenarLasVariablesRadicacion(0);	
}