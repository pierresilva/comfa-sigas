/** VARIABLES LOCALES **/
var idPU=0;
var folios=0;
var idPersonaLocal=0;
var idEmpresaLocal=0;
var afiliaciones=null;
var empresaActualizar=null;
var afiliacionesActivas=false;
var esActualizacionEmpresa=false;
var afiliacionesPendientesPU=false;
var esActualizacionAfiliacion=false;

$(document).ready(function(){
	//Al cargar el tab copiar valor de datos basicos de radicacion
	$("#cmbIdTipoDocumentoAfiliado").val(copyTipoDoc);
	$("#txtNumero").val(copyDoc);
	
	actDatepicker($("#txtFechaIngreso"));	
	
	$("#txtFechaIngreso").datepicker('option', 'yearRange','1900:+1');
	$("#txtFechaIngreso").datepicker('option','maxDate', '+2M');
	
	//Poner foco al combo a tipo de afiliacion independiente
	setTimeout("$('#tabs').trigger('click');$('#cmbTipoAfiliacion').focus();",700);
});

/**
 * Busca la persona a afiliar
 * 
 * @param cmbtd 		combo que contiene el tipo de documento
 * @param txtd 			caja de texto que contiene el Numero de documento
 * @param tdnom 		celda de la tabla que contiene el nombre completo del afiliado
 * @returns {Boolean}
 */
function busquedaPersonaRenovacion(cmbtd, txtd, tdnom){
idPersonaLocal=0;
	idEmpresaLocal=0;
	afiliacionesPendientesPU=false;
	tdnom.html('');
	$('#txtNit').val('');
	$("#lblRazonSocial").html('');
	removeTabs(1);
	eliminarDivsBasura();
	var tipoRadicacionSeleccionada=$("#cmbIdTipoRadicacion option:selected").val();
	
	var persona=buscarPersona(cmbtd, txtd, tdnom, false);
	if(persona==null || persona==false){ 
		txtd.val('');
		txtd.addClass("ui-state-error"); 
		return false; 
	}
	
	var opcion=comprobarTipoAfiliacion(persona.idpersona);
	
	if(opcion==0){
		alert("Ocurri\u00F3 un error comprobando el tipo de Afiliaci\u00F3n");
		txtd.val(''); tdnom.html('');
		txtd.addClass("ui-state-error");
	} else if (opcion==3 || opcion==4){
		alert("La persona que esta radicando es NUEVA, la Radicaci\u00F3n se debe cambia a NUEVA AFILIACI\u00D3N!");
		$("#txtNumero").val('');
		
		//Cambiar Tipo de Afiliacion
		if(tipoRadicacionSeleccionada=='4519'){
			
			$("#cmbIdTipoRadicacion").val("2926");
			$("#cmbIdTipoRadicacion").trigger("change");
		}
		
		
		
				
	} else if(opcion==1 || opcion==2) {
		var afiliaciones = buscarAfiliacionesActivasAfiliado(persona.idpersona);
		var contI=0;
		var contD=0;
		
		if(afiliaciones!=null){
			$.each(afiliaciones,function(i,fila){					
			    if(fila.tipoafiliacion!=18){ contI++; } 
			    if(fila.tipoafiliacion==18){ contD++; } 
			  
			});
			
		
		}
		
		//	Nuevo 2016
		afiliacionesActivas=true;
		
		if(contI>=1){
			alert("Tiene afiliaci\u00F3n como Independiente!!");
			txtd.val(''); tdnom.html('');
			txtd.addClass("ui-state-error");
		} else {
			
			if(contD>=1){
				alert("Tiene afiliaci\u00F3n como Dependiente!!");
				txtd.val(''); tdnom.html('');
				txtd.addClass("ui-state-error");
			}
			idPersonaLocal=persona.idpersona;				
			
			crearTabsInformacion(idPersonaLocal, 0);
			ocultarDivs();
			var benes = buscarBeneficiarios(idPersonaLocal,0);
			if(benes!=null){
				$("#cmbTipoFormulario").val(48);
			}else{
				$("#cmbTipoFormulario").val(49);
			}
			if (opcion==1)
				afiliacionesPendientesPU=true;
		}
	}
	
	idPersonaLocal=persona.idpersona;
	//Los independientes y pensionados pertenecen a un grupo especial llamado: 'INDEPENDIENTE Y/O PENSIONADO' txtd.val()
	var nitEmpresa=0;
	$("#txtNit").val(nitEmpresa).trigger('blur');
	
	
		
	
}


/**
 * Busca la empresa donde labora el afiliado
 * 
 * @param txtNit 			Nit de la empresa
 * @param lblRazonSocial 	Nombre de la empresa
 * @returns {Boolean}
 * 
 */

function busquedaEmpresaRenovacion(txtNit, lblRazonSocial){

	//Set y limpiar datos
	var salir=false;	
	idEmpresaLocal=0;
	afiliaciones=null;
	afiliacionesInactivas=null;
	empresaActualizar=null;
	lblRazonSocial.html('');	
	txtNit.removeClass("ui-state-error");
	var paraAfiliar=0;
	
	//Para Pruebas Descomentar
	var msjeAfi='';
	
	var tipoAfiliacion=$("#cmbTipoAfiliacion").val();
	var tipoRadicacionSeleccionada=$("#cmbIdTipoRadicacion option:selected").val();
	
		
	
	if(esNumeroRespuesta(idPersonaLocal)==false){
		txtNit.val('');		
		$("#txtNumero").val('');		
		$("#txtNumero").addClass("ui-state-error");
		$("#tdNombreCompletoAfiliado").html('');
		return false; 
	}
	if(validarNumero(txtNit,0)>0){return null;}
	
	
	/********************************************************RENOVACION******************************************************************/	
	if(afiliacionesActivas==true || afiliacionesPendientesPU==true){
		
		msjeAfi='ACTIVAS';
		afiliaciones = buscarAfiliacionesActivasAfiliado(idPersonaLocal);
		var contD=0; var contP=0; var contI=0; var estI='A'; var tmp = new Array(); 
		
		if(afiliaciones!=null){
			
			/***************************ACTIVAS*********************************/
			$.each(afiliaciones,function(i,fila){	
				
				//Contar Afiliaciones Independientes contI
			    if(fila.identificacion==fila.nit||fila.nit=='0'){ 
			    	contI++; estI=fila.estado;
			    	idEmpresaLocal=fila.idempresa;
			    } else { 
			    	
			    	//agremiado
			    	if(fila.tipoafiliacion=='4520'){
			    		contI++; 
			    		estI=fila.estado;
				    	idEmpresaLocal=fila.idempresa;
			    	}else{
			    		//Contar Afiliaciones Dependientes contD
				    	tmp[contD]=fila; 
				    	contD++;
				    	if(fila.estado=='P'){
				    		if(esNumeroRespuesta(comprobarEsPU(fila.nit, fila.idpersona))){
				    			contP++;
				    		}
				    	}
			    	}
			    }
			});
			
			
		}else{
			
			msjeAfi='INACTIVAS';
			afiliacionesInactivas = buscarAfiliacionesInactivasAfiliado(idPersonaLocal);
			
			/***************************INACTIVAS*********************************/
				$.each(afiliacionesInactivas,function(i,fila){	
								
								//Contar Afiliaciones Independientes contI
							    if(fila.identificacion==fila.nit||fila.nit=='0'){ 
							    	contI++; estI=fila.estado;
							    	idEmpresaLocal=fila.idempresa;
							    } else { 
							    	
							    	//agremiado
							    	if(fila.tipoafiliacion=='4520'){
							    		contI++; 
							    		estI=fila.estado;
								    	idEmpresaLocal=fila.idempresa;
							    	}else{
							    		//Contar Afiliaciones Dependientes contD
								    	tmp[contD]=fila; 
								    	contD++;
								    	if(fila.estado=='P'){
								    		if(esNumeroRespuesta(comprobarEsPU(fila.nit, fila.idpersona))){
								    			contP++;
								    		}
								    	}
							    	}
							    	
							    }
							});
				
		}
		
		//Para Pruebas Descomentar
		//alert(msjeAfi+"\n\n Total dep:"+contD+" total indp. :"+contI);
		
		if(contI>1&&afiliaciones!=null){
			alert("Tiene m\u00E1s de una afiliaci\u00F3n Independiente!!");
			salir=true;
		} else {
			if(contI==1){
				if(estI=='P' && afiliacionesPendientesPU){				
					idPU=comprobarIndependienteEsPU(idPersonaLocal);
					esActualizacionAfiliacion=esNumeroRespuesta(idPU);
					salir=!esActualizacionAfiliacion;
					if(salir){ 
						alert("No es posible realizar la afiliacion, verifique que se trate de un independiente.\nSu empresa no debe tener trabajadores afiliados!!"); 
					}
				} else if(afiliaciones!=null) {
					//Ya tiene afiliacion como independiente, debe salir
					salir=true; idEmpresaLocal=0;
				}
			}
			
		}
		
		if(salir==false){
			
			if(afiliacionesInactivas!=null){
				
				var msjeAfiliaciones='';
								
				
				
				if(contI==0&&contD>0){
					msjeAfiliaciones+=" Dependiente("+contD+"). Pero NO como independiente,Cambia afiliacion NUEVA.";
					paraAfiliar=1;
					$("#txtNumero").val('');
					alert("Tiene Afiliaci\u00F3nes como Trabajador "+msjeAfiliaciones);
				}
				
				
				//Si tiene historial como dependiente e independiete
				if(contI>0&&contD>0){
					alert("Tiene historial como Dependiente("+contD+") e Independiente("+contI+").Esta seguro renovar como Independiente?");
					//$("#txtNumero").val('');
					//$("#cmbIdTipoRadicacion").val("0");
					//$("#cmbIdTipoRadicacion").trigger("change");
					
				}
				
				
				if(contI==0){
					salir=true; 
					idEmpresaLocal=0;
				}
				
				
			}else{
				
				if(contD>0){
					
					crearTabsInformacion(idPersonaLocal, 0);
					ocultarDivs();
					
					afiliaciones=null;
					if(afiliacionesActivas==true && ((contD-contP)>0)&&afiliacionesInactivas==null){
						//Salir Tambien y no permitir la afiliacion porque tiene activa como dependiente
						//alert("Tiene " + contD + " afiliacion(es) como DEPENDIENTE.");
						salir=true;
						idEmpresaLocal=0;
						
					} else {
							if(afiliaciones!=null){
								alert("Tiene " + contD + " afiliacion(es) PENDIENTES por PU.");
								//Salir Tambien y no permitir la afiliacion
								salir=true;
								idEmpresaLocal=0;
							}
						
					}
				} else { afiliaciones=null; }
				
			}
			
		
		}
	
	}
	/**************************************************************************************************************************************/
	if(salir==false){
		var empresa=null;
		
		if(esNumeroRespuesta(idEmpresaLocal)){ empresa=buscarEmpresaID(idEmpresaLocal); }
		
		if((empresa==null || empresa==false)){ empresa=buscarNit(txtNit, 2); }
		
		if((empresa==null || empresa==false)){
			alert("La empresa No existe.");
			
			idEmpresaLocal=0;			
			lblRazonSocial.html($.trim($("#tdNombreCompletoAfiliado").html()));
			$("#txtNit").val('');
			$("#txtNit").focus();
		} else {
			esActualizacionEmpresa=true;
			empresaActualizar=empresa;
			idEmpresaLocal=empresa.idempresa;			
			lblRazonSocial.html(empresa.razonsocial);
			
			//Si la empresa es LEY 1429 y el tipo afiliacion es diferente de agremiado, el tipo afi. pasa a excento.
			if(empresa.claseaportante==2875&&tipoAfiliacion!='4520'){			
				$('#cmbTipoAfiliacion').append('<option value="3320" selected="selected" >EXCENTO</option>');
				$("#cmbTipoAfiliacion").attr("disabled","disabled");
			}
		}
	}
	
	//Si la persona es agremiado, no debe el Nit ser 0 y no deja continuar.
	if(tipoAfiliacion=='4520'&&txtNit.val()=='0'){
		txtNit.addClass("ui-state-error");
		txtNit.val('').focus();
		return false;
	}
	
	
	if(salir){		
		idPersonaLocal=0;
		txtNit.val('');
		$("#txtNumero").val('');		
		$("#txtNumero").addClass("ui-state-error");
		$("#tdNombreCompletoAfiliado").html('');
		$("#cmbTipoAfiliacion").find("option[value='3320']").remove();
		$("#cmbTipoAfiliacion").removeAttr("disabled");
		

		//Si esta sobre Renovacion Indpte, cambiar a Nueva afiliacion
		if(tipoRadicacionSeleccionada=='4519'){
			
			if(paraAfiliar==1){
				$("#cmbIdTipoRadicacion").val("2926");
				$("#cmbIdTipoRadicacion").trigger("change");
			}
			
			
		}
		
		return false;
	}
	
	$("#hdnIdEmpresa").val(idEmpresaLocal);
}

/**
 * Busca la empresa donde labora el afiliado
 * 
 * @param txtNit 			Nit de la empresa
 * @param lblRazonSocial 	Nombre de la empresa
 * @returns {Boolean}
 
function busquedaEmpresa(txtNit, lblRazonSocial){
	
	
	//Set y limpiar datos
	var salir=false;	
	idEmpresaLocal=0;
	afiliaciones=null;
	empresaActualizar=null;
	lblRazonSocial.html('');	
	txtNit.removeClass("ui-state-error");
	
	var tipoAfiliacion=$("#cmbTipoAfiliacion").val();
	
		
	
	if(esNumeroRespuesta(idPersonaLocal)==false){
		txtNit.val('');		
		$("#txtNumero").val('');		
		$("#txtNumero").addClass("ui-state-error");
		$("#tdNombreCompletoAfiliado").html('');
		return false; 
	}
	if(validarNumero(txtNit,0)>0){return null;}
	
	if(afiliacionesActivas==true || afiliacionesPendientesPU==true){
		afiliaciones = buscarAfiliacionesActivasAfiliado(idPersonaLocal);
		var contD=0; var contP=0; var contI=0; var estI='A'; var tmp = new Array(); 
		
		if(afiliaciones!=null){
			$.each(afiliaciones,function(i,fila){	
				
				//Contar Afiliaciones Independientes contI
			    if(fila.identificacion==fila.nit||fila.nit=='0'){ 
			    	contI++; estI=fila.estado;
			    	idEmpresaLocal=fila.idempresa;
			    } else { 
			    	
			    	//Contar Afiliaciones Dependientes contD
			    	tmp[contD]=fila; 
			    	contD++;
			    	if(fila.estado=='P'){
			    		if(esNumeroRespuesta(comprobarEsPU(fila.nit, fila.idpersona))){
			    			contP++;
			    		}
			    	}
			    }
			});
		}
		
		if(contI>1){
			alert("Tiene m\u00E1s de una afiliaci\u00F3n Independiente!!");
			salir=true;
		} else {
			if(contI==1){
				if(estI=='P' && afiliacionesPendientesPU){				
					idPU=comprobarIndependienteEsPU(idPersonaLocal);
					esActualizacionAfiliacion=esNumeroRespuesta(idPU);
					salir=!esActualizacionAfiliacion;
					if(salir){ 
						alert("No es posible realizar la afiliacion, verifique que se trate de un independiente.\nSu empresa no debe tener trabajadores afiliados!!"); 
					}
				} else {
					alert("Ya Tiene una Afiliaci\u00F3n como Independiente!!");
					salir=true; idEmpresaLocal=0;
				}
			}else{
				
				idEmpresaLocal=0;
				
				
			}
			
		}
		
		if(salir==false){
			crearTabsInformacion(idPersonaLocal, 0);
			ocultarDivs();
			if(contD>0){
				afiliaciones=null;
				if(afiliacionesActivas==true && ((contD-contP)>0)){
					alert("Tiene " + contD + " afiliacion(es) como DEPENDIENTE.");
					//Salir Tambien y no permitir la afiliacion
					salir=true;
					idEmpresaLocal=0;
					
				} else {
					alert("Tiene " + contD + " afiliacion(es) PENDIENTES por PU.");
					//Salir Tambien y no permitir la afiliacion
					salir=true;
					idEmpresaLocal=0;
				}
			} else { afiliaciones=null; }
		}		
	}
	
	if(salir==false){
		var empresa=null;
		
		if(esNumeroRespuesta(idEmpresaLocal)){ empresa=buscarEmpresaID(idEmpresaLocal); }
		
		if((empresa==null || empresa==false)){ empresa=buscarNit(txtNit, 2); }
		
		if((empresa==null || empresa==false)){
			idEmpresaLocal=0;			
			lblRazonSocial.html($.trim($("#tdNombreCompletoAfiliado").html()));			
		} else {
			esActualizacionEmpresa=true;
			empresaActualizar=empresa;
			idEmpresaLocal=empresa.idempresa;			
			lblRazonSocial.html(empresa.razonsocial);
			
			//Si la empresa es LEY 1429 y el tipo afiliacion es diferente de agremiado, el tipo afi. pasa a excento.
			if(empresa.claseaportante==2875&&tipoAfiliacion!='4520'){			
				$('#cmbTipoAfiliacion').append('<option value="3320" selected="selected" >EXCENTO</option>');
				$("#cmbTipoAfiliacion").attr("disabled","disabled");
			}
		}
	}
	
	//Si la persona es agremiado, no debe el Nit ser 0 y no deja continuar.
	if(tipoAfiliacion=='4520'&&txtNit.val()=='0'){
		txtNit.addClass("ui-state-error");
		txtNit.val('').focus();
		return false;
	}
	
	
	if(salir){		
		idPersonaLocal=0;
		txtNit.val('');
		$("#txtNumero").val('');		
		$("#txtNumero").addClass("ui-state-error");
		$("#tdNombreCompletoAfiliado").html('');
		$("#cmbTipoAfiliacion").find("option[value='3320']").remove();
		$("#cmbTipoAfiliacion").removeAttr("disabled");
		return false;
	}
	
	$("#hdnIdEmpresa").val(idEmpresaLocal);
}*/

function changeTipoAfiliacion(){	
		
	
	
	//ESTE ES EL CODIGO NUEVO
	
	var ta=$('#cmbTipoAfiliacion').val();	

	
	$('.independienteFacultativo, .independientePensionadoVoluntario, .independientePensionadoSisCajas, .independienteagramiado, .independiextranjero').hide();
	$(".independienteFacultativo input[type=checkbox]").removeAttr('checked').trigger("change");
	$(".independientePensionadoVoluntario input[type=checkbox]").removeAttr('checked').trigger("change");
	$(".independientePensionadoSisCajas input[type=checkbox]").removeAttr('checked').trigger("change");
	$(".independienteagramiado input[type=checkbox]").removeAttr('checked').trigger("change");
	$(".independiextranjero input[type=checkbox]").removeAttr('checked').trigger("change");
	
	
	if(ta=='19'){ $('.independienteFacultativo').show(); }
	else if(ta=='20') { $('.independientePensionadoVoluntario').show(); }
	else if(ta =='4520') {$('.independienteagramiado').show();}  //La agregue para que salgan los agremiados DINO
	else if(ta =='21'){ $('.independientePensionadoSisCajas').show(); }
	else if(ta =='4209'){ $('.independientePensionadoVoluntario').show(); }
	else if(ta =='4521') {$('.independiextranjero').show();}
	
	
	//ESTE CODIGO VIEJO 
	var tipoAfiliacion=$('#cmbTipoAfiliacion').val();
	/*$("#txtNit").val('');
	$("#lblRazonSocial").empty();
	/*
	 * 	tipoAfiliacion:
	 * 		19 	Indp. Facultativo
	 * 		20 	P. Voluntario
	 * 		21 	P.Sis. Cajas
	 * 		4209 P. Reg. Especial
	 * 		4521 Indp. Extranjero
	 * 		4520 Indp. Agremiado
	 * */
	/*if(tipoAfiliacion=='4520'){
		$("#txtNit").removeAttr("disabled");
	}else{
		$("#txtNit").attr("disabled","disabled");
	}
	
	$('.independienteFacultativo, .independientePensionadoVoluntario').hide();
	$(".independienteFacultativo input[type=checkbox]").removeAttr('checked').trigger("change");
	$(".independientePensionadoVoluntario input[type=checkbox]").removeAttr('checked').trigger("change");
	
	/*if(tipoAfiliacion=='19'){ $('.independienteFacultativo').show(); }
	else if(tipoAfiliacion=='20') { $('.independientePensionadoVoluntario').show(); }*/
	
	/*if(ta=='19'){ $('.independienteFacultativo').show(); }
	else if(ta=='20') { $('.independientePensionadoVoluntario').show(); }
	else if(ta =='4520') {$('.independienteagramiado').show();}  //La agregue para que salgan los agremiados DINO
	else if(ta =='4521') {$('.independiextranjero').show();}*/
	
}

/**
 * Guardar la Radicacion
 * 
 * @returns {Boolean}
 */
function saveRadicacion(){
	if(iniciarconN()){
		if(validarTodosLosCampos()){		
			var nAfiliacion=new Afiliacion();
			nAfiliacion=llenarLasVariables(nAfiliacion);
			
			if(nAfiliacion==null){
				nRadicacion=null;
				alert("Se encontr\u00F3 errores en el procesamiento de su solicitud.");				
				return false;
			} else {									
				/*var nEmpresa=new Empresa();
				nEmpresa=llenarVariablesEmpresa(nEmpresa);
			
				if(nEmpresa==null){
					nRadicacion=null;
					alert("Se encontr\u00F3 errores en el procesamiento de su solicitud.\nError al grabar Empresa Independiente");				
					return false;
				}	*/	
				
				//Guardar Id. empresa si es Inp. guarda 0
				nAfiliacion.idempresa=$("#hdnIdEmpresa").val();
				
				
				nRadicacion.idradicacion=guardarRadicacion(nRadicacion);
				if(esNumeroRespuesta(nRadicacion.idradicacion)){
					var resFolios=guardarDocumentos(listDocumentos, nRadicacion.idradicacion, nAfiliacion.idpersona, "S");
					
					if(esNumeroRespuesta(resFolios)){
						nuevaRad=0;
						$("#txtHoraFin").val(nRadicacion.horafinal);					
						var cant= 0; var res=0;
						if(afiliaciones instanceof Array){ cant=afiliaciones.length; }
						if(cant>0){ res = inactivarAfiliaciones(afiliaciones); }
						
						if(cant==res){
							nAfiliacion.idradicacion=nRadicacion.idradicacion;			
							if(esActualizacionAfiliacion==false){
								nAfiliacion.idformulario=guardarAfiliacion(nAfiliacion);
							} else {
								nAfiliacion.idformulario=idPU;
								nAfiliacion.porplanilla='S';
								nAfiliacion.idformulario=actualizarAfiliacion(nAfiliacion);
							}
							
							if(esNumeroRespuesta(nAfiliacion.idformulario)){						
								alert("Radicaci\u00F3n guardada correctamente!! \nRADICACI\u00D3N # "+nRadicacion.idradicacion+" ide:"+nAfiliacion.idempresa);							
								$("#txtIdRadicacion").css({ color: "RED"});
								$("#txtIdRadicacion").val(nRadicacion.idradicacion);
								$("#titRadicacion").trigger('click');							
								if(cant>0){ alert("Inactivada " + res + " de  " + cant + " Afiliaciones"); }
								return true;
							} else {
								if(anularRadicacion(nRadicacion.idradicacion)){
									alert("Ocurri\u00F3 un error guardando la afiliacion!!" +
											"\nPOR FAVOR vuelva a intentarlo");
								} else {
									alert("Ocurri\u00F3 un error guardando la afiliacion!!" +
											"\nPOR FAVOR reporte este numero a soporte:" +
											"\# "+nRadicacion.idradicacion);
								}
								
								return false;
							}
						} else {
							if(anularRadicacion(nRadicacion.idradicacion)){
								alert("Ocurri\u00F3 un error!! \nSe Inactiv\u00F3 " + res + " de " + cant + "  Afiliaciones");
							} else {
								alert("Ocurri\u00F3 un error inactivando!!" +
										"\nPOR FAVOR reporte este numero a soporte:" +
										"\# "+nRadicacion.idradicacion);
							}
							
							return false;
						}
					} else {
						if(anularRadicacion(nRadicacion.idradicacion)){
							alert("Ocurrio un error al guardar los Documentos!!" +
									"\nPOR FAVOR vuelva a intentarlo");
						} else {
							alert("Ocurrio un error al guardar los Documentos y al tratar de anular la radicacion!!" +
									"\nPOR FAVOR reporte este numero a soporte:" +
									"\# "+nRadicacion.idradicacion);
						}											
						return false;
					}					
				} else {
					alert("Radicaci\u00F3n NO FUE GUARDADA!!"); 
				}
			}
		}
	}
}

function saveDevolucion(opt){
	if(iniciarconN()){
		if(validarTodosLosCampos(opt)){
			if(opt==1) { enaDivMotivosDevolucion(); return false; }			
			
			var idMotivoDevolucion=$("#cmbMotivosDevolucion").val();
			llenarLasVariablesRadicacion(1);
			nRadicacion.motivodevolucion=idMotivoDevolucion;
			$("#divMotivosDevolucion").dialog('close');
			
			nRadicacion.idradicacion=guardarDevolucion(nRadicacion);
			if(esNumeroRespuesta(nRadicacion.idradicacion)){
				nuevaRad=0;
				$("#txtHoraFin").val(nRadicacion.horafinal);
				alert("Devoluci\u00F3n guardada correctamente!! \nRADICACI\u00D3N # "+nRadicacion.idradicacion);												
						
				$("#txtIdRadicacion").css({ color: "RED"});
				$("#txtIdRadicacion").val(nRadicacion.idradicacion);
				$("#titRadicacion").trigger('click');
				return true;
			} else {
				alert("Devoluci\u00F3n NO FUE GUARDADA!!"); 
				return false;
			}			
		}
	}
}

/**
 * Valida todos los campos necesarios para la radicacion 
 * 
 * @returns {Boolean}
 */
function validarTodosLosCampos(opt){
	var error=validarCampos();
	
	error=validarSelect($("#cmbIdTipoDocumentoAfiliado"),error);
	if(esNumeroRespuesta(idPersonaLocal)==false){ $("#txtNumero").val(''); }
	error=validarTexto($("#txtNumero"),error);
	if(esActualizacionEmpresa && esNumeroRespuesta(idEmpresaLocal)==false){ 
		if($("#txtNit").val()!='0'){
			$("#txtNit").val('');
		}
		
	}
	
	//Validar el Nit cuando es vacio
	
	if($("#txtNit").val()==''){
		$("#txtNit").addClass("ui-state-error");
		error++;
	}else{
		//si no dejarlo pasar incluyendo Nit 0.
		$("#txtNit").removeClass("ui-state-error");
	}
			
	
	error=validarSelect($("#cmbTipoFormulario"),error);
	error=validarSelect($("#cmbTipoAfiliacion"),error);
	error=validarTexto($("#txtFechaIngreso"),error);	
	error=validarNumero($("#txtSalario"),error);
	
	if(error>0){
		$("#titDetRadicacion").trigger('click');
		alert("Llene los campos obligatorios!");
		nRadicacion=null;
		return false;
	} else {
		if(opt==1){ return true; }
		else if(opt==2){
			error=validarSelect($("#cmbMotivosDevolucion"),error);
			if(error>0){
				nRadicacion=null;
				return false;
			} else {
				return true;
			}
		}
		
		folios=validarRequeridos();
		if(esNumeroRespuesta(folios)){ return true; }
		else {
			nRadicacion=null;
			return false;
		}
	}
}

function llenarLasVariablesRadicacion(opt){
	nRadicacion.idtipodocumentoafiliado=$("#cmbIdTipoDocumentoAfiliado").val();
	nRadicacion.numero=$("#txtNumero").val();
	nRadicacion.nit=$("#txtNit").val();
	nRadicacion.folios=folios;
	nRadicacion.notas=$.trim($("#txtNotas").val());
	nRadicacion.procesado='N';
	nRadicacion.idtipoformulario=$("#cmbTipoFormulario").val();
	nRadicacion.afiliacionmultiple='N';
	
	if(opt==1){
		nRadicacion.folios=0;
		nRadicacion.asignado="S";
		nRadicacion.procesado="S";
		nRadicacion.anulada="S";
		nRadicacion.devuelto="S";
		nRadicacion.cierre="S";
	}
}

/**
 * LLena las variables necesarias para la radicacion
 * 
 * @param nAfiliacion
 * @returns {Afiliacion}
 */
function llenarLasVariables(nAfiliacion){
	llenarLasVariablesRadicacion(0);
	
	nAfiliacion=camposAfiliacion(nAfiliacion);
	nAfiliacion.tipoformulario=$("#cmbTipoFormulario").val();
	nAfiliacion.tipoafiliacion=$("#cmbTipoAfiliacion").val();	
	nAfiliacion.idpersona=idPersonaLocal;
	nAfiliacion.fechaingreso=$("#txtFechaIngreso").val();
	nAfiliacion.salario=$("#txtSalario").val();
	nAfiliacion.primaria=comprobarSiEsPrimaria();
	nAfiliacion.estado='P';
	nAfiliacion.categoria=calcularCategoria(nAfiliacion.salario);
	nAfiliacion.auditado='N';
	nAfiliacion.vendedor='N';
	
	if(nAfiliacion.primaria=='N') nRadicacion.afiliacionmultiple='S';
	
	if(nAfiliacion.categoria=='D' || nAfiliacion.primaria=='E'){ 
		if(nAfiliacion.categoria=='D'){ $("#txtSalario").val(''); $("#txtSalario").addClass("ui-state-error"); }
		if(nAfiliacion.primaria=='E'){ alert("Ocurri\u00F3 un error comprobando afiliaci\u00F3n Primaria del afiliado"); }
		nRadicacion=null;
		nAfiliacion=null;
	}
	
	return nAfiliacion;
}

/**
 * Comprueba si la afiliacion es primaria
 * 
 * @returns {String} 'S' - 'N'
 */
function comprobarSiEsPrimaria(){
	var primaria='N';
	
	if(afiliacionesActivas){	
		var opcion=contarAfiliacionesPrimaria(idPersonaLocal);
		
		if(opcion==2){
			primaria='S';
		} else if(opcion==1){
			if(esActualizacionEmpresa){
				opcion=contarSiAfiliacionEsPrimaria(idPersonaLocal,idEmpresaLocal);
			
				if(opcion==2) primaria='S';
			}
		}
		
		if(opcion==0) primaria="E";
	} else {
		primaria='S';
	}
		
	return primaria;
}

/**
 * Llena todos los datos de la empresa, para lo cual la guarda o actualiza segun sea el caso
 * 
 * @param nEmpresa
 * @returns {Empresa}
 */
function llenarVariablesEmpresa(nEmpresa){
	
	
	var nRadicacionEmpresa=jQuery.extend({}, nRadicacion);
	if(esActualizacionEmpresa){
		if(esNumeroRespuesta(idEmpresaLocal) && empresaActualizar!=null){
			nEmpresa=camposEmpresa(empresaActualizar);
			if(esActualizacionAfiliacion==true){ nEmpresa.porplanilla='S'; }
			nRadicacionEmpresa.idtiporadicacion=31;
			nEmpresa.renovacion='S';			
		} else {
			return null;
		}
	} else {
		nEmpresa=camposEmpresa(nEmpresa);
		nRadicacionEmpresa.idtiporadicacion=30;
		nEmpresa.renovacion='N';
	}	
	
	nEmpresa.idtipodocumento=$("#cmbIdTipoDocumentoAfiliado").val();
	nEmpresa.nit=$("#txtNumero").val();
	nEmpresa.codigosucursal='000';
	nEmpresa.principal='S';
	nEmpresa.razonsocial=$("#lblRazonSocial").html().replace("&nbsp;", "");
	nEmpresa.sigla=$("#lblRazonSocial").html().replace("&nbsp;", "");
	nEmpresa.contratista='N';
	nEmpresa.claseaportante=2655;
	nEmpresa.tipoaportante=2657;
	nEmpresa.estado='A';
	nEmpresa.legalizada='S';	
	
	var idtipoafiliacion=3316;
	var tipoafiliacion=$("#cmbTipoAfiliacion").val();
	if(tipoafiliacion==19){ idtipoafiliacion=3317; }
	else if(tipoafiliacion==21){ idtipoafiliacion=3318; }
	else if(tipoafiliacion!=3320){ idtipoafiliacion=3319; }	
	nEmpresa.idtipoafiliacion=idtipoafiliacion;
	
	nRadicacionEmpresa.idradicacion=guardarRadicacion(nRadicacionEmpresa);
	if(esNumeroRespuesta(nRadicacionEmpresa.idradicacion)){		
		if(esActualizacionEmpresa){
			if(esNumeroRespuesta(actualizarEmpresa(nEmpresa))==false){ nEmpresa.idempresa=0; }
		} else {
			nEmpresa.idempresa=guardarEmpresa(nEmpresa);
		}
		
		if(esNumeroRespuesta(nEmpresa.idempresa)){
			if(!cerrarRadicacion(nRadicacionEmpresa.idradicacion)){
				alert("Ocurrio un error al CERRAR la Radicacion de la empresa Independiente!!" +
						"\nPOR FAVOR reporte este numero a soporte:" +
						"\# "+nRadicacionEmpresa.idradicacion);				
			}			
			
			return nEmpresa;
		} else {
			if(anularRadicacion(nRadicacionEmpresa.idradicacion)){
				alert("Ocurrio un error al GRABAR la empresa Independiente!!" +
						"\nPOR FAVOR vuelva a intentarlo");
			} else {
				alert("Ocurrio un error al ANULAR la Radicacion de la empresa Independiente!!" +
						"\nPOR FAVOR reporte este numero a soporte:" +
						"\# "+nRadicacionEmpresa.idradicacion);
			}
			
			return null;
		}
		
	} else {
		return null;
	}	
}