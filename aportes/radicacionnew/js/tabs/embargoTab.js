/** VARIABLES LOCALES **/
var idPersonaLocal=0;
var folios=0;

$(document).ready(function(){
	$("#cmbIdTipoDocumentoAfiliado").val(copyTipoDoc);
	$("#txtNumero").val(copyDoc);
	
	setTimeout("$('#chkEMBEmbargoJuzgado').attr('checked',true).trigger('change');",500);
	setTimeout("$('#tabs').trigger('click');$('#cmbIdTipoDocumentoAfiliado').focus();",700);
});

/**
 * Busca la persona a afiliar
 * 
 * @param cmbtd 		combo que contiene el tipo de documento
 * @param txtd 			text que contiene el documento
 * @param tdnom 		td que contiene el nombre del afiliado
 * @returns {Boolean}
 */
function busquedaPersona(cmbtd, txtd, tdnom){
	idPersonaLocal=0;	
	tdnom.html('');	
	
	var persona=buscarPersona(cmbtd, txtd, tdnom, false);
	if(persona==null || persona==false){ 
		txtd.val('');
		txtd.addClass("ui-state-error"); 
		return false; 
	}
	
	var beneficiarios=buscarBeneficiarios(persona.idpersona,1);
	
	if(beneficiarios==null){
		alert("La persona que esta radicando NO TIENE BENEFICIARIOS!!");
		txtd.val(''); tdnom.html('');
		txtd.addClass("ui-state-error");
	} else {
		var contSinEmbargo=0;
		$.each(beneficiarios,function(i,fila){
			if(fila.embarga!='S'){ contSinEmbargo++; } 
		});
		
		if(contSinEmbargo==0){
			alert("La persona que esta radicando NO TIENE BENEFICIARIOS SIN EMBARGO!!");
			txtd.val(''); tdnom.html('');
			txtd.addClass("ui-state-error");
			beneficiario=null;
		} else {
			idPersonaLocal=persona.idpersona;
		}
	}		
}

/**
 * Guardar la Radicacion
 */
function saveRadicacion(){
	if(iniciarconN()){
		if(validarTodosLosCampos()){		
			llenarLasVariables();
			
			nRadicacion.idradicacion=guardarRadicacion(nRadicacion);
			
			if(esNumeroRespuesta(nRadicacion.idradicacion)){
				nuevaRad=0;
				$("#txtHoraFin").val(nRadicacion.horafinal);				
				var resFolios=guardarDocumentos(listDocumentos, nRadicacion.idradicacion, idPersonaLocal, "V");
				
				if(esNumeroRespuesta(resFolios)){										
					alert("Radicaci\u00F3n guardada correctamente!! \nRADICACI\u00D3N # " + nRadicacion.idradicacion);					
				} else {
					alert("Radicaci\u00F3n guardada con error!! \nRADICACI\u00D3N # " + nRadicacion.idradicacion +
							"\nOcurrio un error al guardar los Documentos!!");
				}
								
				$("#txtIdRadicacion").css({ color: "RED"});
				$("#txtIdRadicacion").val(nRadicacion.idradicacion);
				$("#titRadicacion").trigger('click');
				return true;
			} else {
				alert("Radicaci\u00F3n NO FUE GUARDADA!!");
				return false;
			}
		}
	}
}

function saveDevolucion(opt){
	if(iniciarconN()){
		if(validarTodosLosCampos(opt)){
			if(opt==1) { enaDivMotivosDevolucion(); return false; }			
			
			var idMotivoDevolucion=$("#cmbMotivosDevolucion").val();
			llenarLasVariablesRadicacion(1);
			nRadicacion.motivodevolucion=idMotivoDevolucion;
			$("#divMotivosDevolucion").dialog('close');
			
			nRadicacion.idradicacion=guardarDevolucion(nRadicacion);
			if(esNumeroRespuesta(nRadicacion.idradicacion)){
				nuevaRad=0;
				$("#txtHoraFin").val(nRadicacion.horafinal);
				alert("Devoluci\u00F3n guardada correctamente!! \nRADICACI\u00D3N # "+nRadicacion.idradicacion);												
						
				$("#txtIdRadicacion").css({ color: "RED"});
				$("#txtIdRadicacion").val(nRadicacion.idradicacion);
				$("#titRadicacion").trigger('click');
				return true;
			} else {
				alert("Devoluci\u00F3n NO FUE GUARDADA!!"); 
				return false;
			}			
		}
	}
}

/**
 * Valida todos los campos necesarios para la radicacion 
 */
function validarTodosLosCampos(opt){
	var error=validarCampos();
	
	error=validarSelect($("#cmbIdTipoDocumentoAfiliado"),error);
	if(esNumeroRespuesta(idPersonaLocal)==false){ $("#txtNumero").val(''); }
	error=validarTexto($("#txtNumero"),error);		
	
	if(error>0){		
		$("#titDetRadicacion").trigger('click');
		alert("Llene los campos obligatorios!");
		nRadicacion=null;
		return false;
	} else {
		if(opt==1){ return true; }
		else if(opt==2){
			error=validarSelect($("#cmbMotivosDevolucion"),error);
			if(error>0){
				nRadicacion=null;
				return false;
			} else {
				return true;
			}
		}
		
		folios=validarRequeridos();
		if(esNumeroRespuesta(folios)){ return true; }
		else {
			nRadicacion=null;
			return false;
		}
	}
}

function llenarLasVariablesRadicacion(opt){
	nRadicacion.idtipodocumentoafiliado=$("#cmbIdTipoDocumentoAfiliado").val();
	nRadicacion.numero=$("#txtNumero").val();	
	nRadicacion.folios=folios;
	nRadicacion.notas=$.trim($("#txtNotas").val());
	
	if(opt==1){
		nRadicacion.folios=0;
		nRadicacion.asignado="S";
		nRadicacion.procesado="S";
		nRadicacion.anulada="S";
		nRadicacion.devuelto="S";
		nRadicacion.cierre="S";
	}
}

/**
 * LLena las variables necesarias para la radicacion
 */
function llenarLasVariables(){
	llenarLasVariablesRadicacion(0);	
}