/**
 * Funcion-clase que contiene todos los campos existentes en la base de datos de la tabla de radicacion(aportes004) 
 */
function Radicacion(){
	var idradicacion;
	var fecharadicacion;
	var identificacion;
	var idtipodocumento;
	var idtiporadicacion;
	var idtipopresentacion;
	var horainicio;
	var horafinal;
	var tiempo;
	var notas;
	var idtipoinformacion;
	var idtipodocumentoafiliado;
	var numero;
	var nit;
	var folios;
	var idtipocertificado;
	var idbeneficiario;
	var observaciones;
	var asignado;
	var fechaasignacion;
	var digitalizada;
	var fechadigitalizacion;
	var procesado;
	var fechaproceso;
	var flag;
	var tempo1;
	var fechasistema;
	var usuario;
	var usuarioasignado;
	var anulada;
	var usuarioanula;
	var idtipoformulario;
	var devuelto;
	var motivodevolucion;
	var idagencia;
	var idtipodocben;
	var numerobeneficiario;
	var afiliacionmultiple;
	var cierre;
	var recibeventanilla;
	var recibegrabacion;
	var fecharecibegraba;
	var idtipomovilizacion;
}

/**
 * Funcion que inicializa todos los campos de la radicacion 
 */
function camposRadicacion(objeto){
	objeto.idradicacion = esUndefined(objeto.idradicacion);
	objeto.fecharadicacion = esUndefined(objeto.fecharadicacion);
	objeto.identificacion = esUndefined(objeto.identificacion);
	objeto.idtipodocumento = esUndefined(objeto.idtipodocumento);
	objeto.idtiporadicacion = esUndefined(objeto.idtiporadicacion);
	objeto.idtipopresentacion = esUndefined(objeto.idtipopresentacion);
	objeto.horainicio = esUndefined(objeto.horainicio);
	objeto.horafinal = esUndefined(objeto.horafina);
	objeto.tiempo = esUndefined(objeto.tiempo);
	objeto.notas = esUndefined(objeto.notas);
	objeto.idtipoinformacion = esUndefined(objeto.idtipoinformacion);
	objeto.idtipodocumentoafiliado = esUndefined(objeto.idtipodocumentoafiliado);
	objeto.numero = esUndefined(objeto.numero);
	objeto.nit = esUndefined(objeto.nit);
	objeto.folios = esUndefined(objeto.folios);
	objeto.idtipocertificado = esUndefined(objeto.idtipocertificado);
	objeto.idbeneficiario = esUndefined(objeto.idbeneficiario);
	objeto.observaciones = esUndefined(objeto.observaciones);
	objeto.asignado = esUndefined(objeto.asignado);
	objeto.fechaasignacion = esUndefined(objeto.fechaasignacion);
	objeto.digitalizada = esUndefined(objeto.digitalizada);
	objeto.fechadigitalizacion = esUndefined(objeto.fechadigitalizacion);
	objeto.procesado = esUndefined(objeto.procesado);
	objeto.fechaproceso = esUndefined(objeto.fechaproceso);
	objeto.flag = esUndefined(objeto.flag);
	objeto.tempo1 = esUndefined(objeto.tempo1);
	objeto.fechasistema = esUndefined(objeto.fechasistema);
	objeto.usuario = esUndefined(objeto.usuario);
	objeto.usuarioasignado = esUndefined(objeto.usuarioasignado);
	objeto.anulada = esUndefined(objeto.anulada);
	objeto.usuarioanula = esUndefined();
	objeto.idtipoformulario = esUndefined(objeto.idtipoformulario);
	objeto.devuelto = esUndefined(objeto.devuelto);
	objeto.motivodevolucion = esUndefined(objeto.motivodevolucion);
	objeto.idagencia = esUndefined(objeto.idagencia);
	objeto.idtipodocben = esUndefined(objeto.idtipodocben);
	objeto.numerobeneficiario = esUndefined(objeto.numerobeneficiario);
	objeto.afiliacionmultiple = esUndefined(objeto.afiliacionmultiple);
	objeto.cierre = esUndefined(objeto.cierre);
	objeto.recibeventanilla = esUndefined(objeto.recibeventanilla);
	objeto.recibegrabacion = esUndefined(objeto.recibegrabacion);
	objeto.fecharecibegraba = esUndefined(objeto.fecharecibegraba);
	objeto.idtipomovilizacion = esUndefined(objeto.idtipomovilizacion);
	
	return objeto;
}

/**
 * Funcion-clase que contiene todos los campos existentes en la base de datos de la tabla de Afiliacion(aportes016) 
 */
function Afiliacion(){
	var idformulario;
	var tipoformulario; 
	var tipoafiliacion; 
	var idempresa; 
	var idpersona; 
	var fechaingreso; 
	var horasdia; 
	var horasmes; 
	var salario; 
	var agricola; 
	var cargo; 
	var primaria; 
	var estado; 
	var fecharetiro; 
	var motivoretiro; 
	var fechanovedad; 
	var semanas; 
	var fechafidelidad; 
	var estadofidelidad; 
	var traslado; 
	var codigocaja; 
	var flag; 
	var tempo1; 
	var tempo2; 
	var fechasistema; 
	var usuario; 
	var tipopago; 
	var categoria; 
	var auditado; 
	var idagencia; 
	var idradicacion; 
	var vendedor; 
	var codigosalario;
}

/**
 * Funcion que inicializa todos los campos de la afiliacion 
 */
function camposAfiliacion(objeto){
	objeto.idformulario = esUndefined(objeto.idformulario);
	objeto.tipoformulario = esUndefined(objeto.tipoformulario); 
	objeto.tipoafiliacion = esUndefined(objeto.tipoafiliacion); 
	objeto.idempresa = esUndefined(objeto.idempresa); 
	objeto.idpersona = esUndefined(objeto.idpersona); 
	objeto.fechaingreso = esUndefined(objeto.fechaingreso); 
	objeto.horasdia = esUndefined(objeto.horasdia); 
	objeto.horasmes = esUndefined(objeto.horasmes); 
	objeto.salario = esUndefined(objeto.salario); 
	objeto.agricola = esUndefined(objeto.agricola); 
	objeto.cargo = esUndefined(objeto.cargo); 
	objeto.primaria = esUndefined(objeto.primaria); 
	objeto.estado = esUndefined(objeto.estado); 
	objeto.fecharetiro = esUndefined(objeto.fecharetiro); 
	objeto.motivoretiro = esUndefined(objeto.motivoretiro); 
	objeto.fechanovedad = esUndefined(objeto.fechanovedad); 
	objeto.semanas = esUndefined(objeto.semanas); 
	objeto.fechafidelidad = esUndefined(objeto.fechafidelidad); 
	objeto.estadofidelidad = esUndefined(objeto.estadofidelidad); 
	objeto.traslado = esUndefined(objeto.traslado); 
	objeto.codigocaja = esUndefined(objeto.codigocaja); 
	objeto.flag = esUndefined(objeto.flag); 
	objeto.tempo1 = esUndefined(objeto.tempo1); 
	objeto.tempo2 = esUndefined(objeto.tempo2); 
	objeto.fechasistema = esUndefined(objeto.fechasistema); 
	objeto.usuario = esUndefined(objeto.usuario); 
	objeto.tipopago = esUndefined(objeto.tipopago); 
	objeto.categoria = esUndefined(objeto.categoria); 
	objeto.auditado = esUndefined(objeto.auditado); 
	objeto.idagencia = esUndefined(objeto.idagencia); 
	objeto.idradicacion = esUndefined(objeto.idradicacion); 
	objeto.vendedor = esUndefined(objeto.vendedor); 
	objeto.codigosalario = esUndefined(objeto.codigosalario);
	
	return objeto;
}

/**
 * Funcion-clase que contiene todos los campos existentes en la base de datos de la tabla Empresa(aportes048) * 
 */
function Empresa(){
	var idempresa; 
	var idtipodocumento; 
	var nit; 
	var digito; 
	var nitrsn; 
	var codigosucursal; 
	var principal; 
	var razonsocial; 
	var sigla; 
	var direccion; 
	var iddepartamento; 
	var idciudad; 
	var idzona; 
	var telefono; 
	var fax; 
	var url; 
	var email; 
	var idrepresentante; 
	var idjefepersonal; 
	var contratista; 
	var colegio; 
	var exento; 
	var idcodigoactividad; 
	var actieconomicadane; 
	var indicador; 
	var idasesor; 
	var fechamatricula; 
	var idsector; 
	var seccional; 
	var tipopersona; 
	var claseaportante; 
	var tipoaportante; 
	var estado; 
	var codigoestado; 
	var fechaestado; 
	var fechaaportes; 
	var fechaafiliacion; 
	var trabajadores; 
	var aportantes; 
	var conyuges; 
	var hijos; 
	var hermanos; 
	var padres; 
	var tempo; 
	var flag; 
	var usuario; 
	var fechasistema; 
	var idclasesociedad; 
	var rutadocumentos; 
	var legalizada; 
	var renovacion; 
	var cedrep; 
	var representante; 
	var auditado; 
	var idtipoafiliacion;
}

/**
 * Funcion que inicializa todos los campos de la Empresa 
 */
function camposEmpresa(objeto){
	objeto.idempresa = esUndefined( objeto.idempresa ); 
	objeto.idtipodocumento = esUndefined( objeto.idtipodocumento ); 
	objeto.nit = esUndefined( objeto.nit ); 
	objeto.digito = esUndefined( objeto.digito ); 
	objeto.nitrsn = esUndefined( objeto.nitrsn ); 
	objeto.codigosucursal = esUndefined( objeto.codigosucursal ); 
	objeto.principal = esUndefined( objeto.principal ); 
	objeto.razonsocial = esUndefined( objeto.razonsocial ); 
	objeto.sigla = esUndefined( objeto.sigla ); 
	objeto.direccion = esUndefined( objeto.direccion ); 
	objeto.iddepartamento = esUndefined( objeto.iddepartamento ); 
	objeto.idciudad = esUndefined( objeto.idciudad ); 
	objeto.idzona = esUndefined( objeto.idzona ); 
	objeto.telefono = esUndefined( objeto.telefono ); 
	objeto.fax = esUndefined( objeto.fax ); 
	objeto.url = esUndefined( objeto.url ); 
	objeto.email = esUndefined( objeto.email ); 
	objeto.idrepresentante = esUndefined( objeto.idrepresentante ); 
	objeto.idjefepersonal = esUndefined( objeto.idjefepersonal ); 
	objeto.contratista = esUndefined( objeto.contratista ); 
	objeto.colegio = esUndefined( objeto.colegio ); 
	objeto.exento = esUndefined( objeto.exento ); 
	objeto.idcodigoactividad = esUndefined( objeto.idcodigoactividad ); 
	objeto.actieconomicadane = esUndefined( objeto.actieconomicadane ); 
	objeto.indicador = esUndefined( objeto.indicador ); 
	objeto.idasesor = esUndefined( objeto.idasesor ); 
	objeto.fechamatricula = esUndefined( objeto.fechamatricula ); 
	objeto.idsector = esUndefined( objeto.idsector ); 
	objeto.seccional = esUndefined( objeto.seccional ); 
	objeto.tipopersona = esUndefined( objeto.tipopersona ); 
	objeto.claseaportante = esUndefined( objeto.claseaportante ); 
	objeto.tipoaportante = esUndefined( objeto.tipoaportante ); 
	objeto.estado = esUndefined( objeto.estado ); 
	objeto.codigoestado = esUndefined( objeto.codigoestado ); 
	objeto.fechaestado = esUndefined( objeto.fechaestado ); 
	objeto.fechaaportes = esUndefined( objeto.fechaaportes ); 
	objeto.fechaafiliacion = esUndefined( objeto.fechaafiliacion ); 
	objeto.trabajadores = esUndefined( objeto.trabajadores ); 
	objeto.aportantes = esUndefined( objeto.aportantes ); 
	objeto.conyuges = esUndefined( objeto.conyuges ); 
	objeto.hijos = esUndefined( objeto.hijos ); 
	objeto.hermanos = esUndefined( objeto.hermanos ); 
	objeto.padres = esUndefined( objeto.padres ); 
	objeto.tempo = esUndefined( objeto.tempo ); 
	objeto.flag = esUndefined( objeto.flag ); 
	objeto.usuario = esUndefined( objeto.usuario ); 
	objeto.fechasistema = esUndefined( objeto.fechasistema ); 
	objeto.idclasesociedad = esUndefined( objeto.idclasesociedad ); 
	objeto.rutadocumentos = esUndefined( objeto.rutadocumentos ); 
	objeto.legalizada = esUndefined( objeto.legalizada ); 
	objeto.renovacion = esUndefined( objeto.renovacion ); 
	objeto.cedrep = esUndefined( objeto.cedrep ); 
	objeto.representante = esUndefined( objeto.representante ); 
	objeto.auditado = esUndefined( objeto.auditado ); 
	objeto.idtipoafiliacion = esUndefined( objeto.idtipoafiliacion );
}

/**
 * Funcion que verifica si una variable esta sin definir
 * 
 *  @param variable 	elemento a validar
 *  @returns 			retorna la variable despues de validar
 */
function esUndefined(variable){
	if(variable==undefined)
		variable='';
	return variable;
}