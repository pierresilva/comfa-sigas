<?php
/* autor:       Orlando Puentes
 * fecha:       Septiembre 14 de 2010
 * objetivo:    Almacenar en la base de datos Aportes la información de los embargos que tiene el trabajador afiliado.   
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$url = $_SERVER['PHP_SELF'];
include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'auditoria.php';
auditar($url);

include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'p.definiciones.class.php';
$objClase= new Definiciones;
$consulta = $objClase->mostrar_datos(1,2);
include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR .'ciudades.class.php';
$objCiudad=new Ciudades();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Embargos</title>
<link type="text/css" href="../../css/formularios/base/ui.all.css" rel="stylesheet" />
<link type="text/css" href="../../css/Estilos.css" rel="stylesheet"/>
<link type="text/css" href="../../css/estilo_tablas.css" rel="stylesheet"/>
<link type="text/css" href="../../css/formularios/demos.css" />
<link href="../../css/marco.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../../js/jquery-1.4.2.js"></script>
<script type="text/javascript" src="../../js/formularios/ui/ui.core.js"></script>
<script type="text/javascript" src="../../js/formularios/ui/ui.tabs.js"></script>
<script type="text/javascript" src="../../js/formularios/ui/ui.dialog.js"></script>
<script type="text/javascript" src="../../js/formularios/ui/ui.datepicker.js"></script>
<script type="text/javascript" src="../../js/comunes.js"></script>
<script type="text/javascript" src="../../js/jquery.combos.js"></script>
<script type="text/javascript" src="../../js/direccion.js"></script>
<script language="javascript" src="js/embargos.js"></script>
<script type="text/javascript">
shortcut.add("Shift+F",function() {
	var URL=src();
	var url=URL+"aportes/trabajadores/consultaTrabajador.php";
	window.open(url,"_blank");
	},{
		'propagate' : true,
		'target' : document 
	});
</script>
</head>
<body>
<form name="forma">
<br/>
<center>
<table width="95%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="13" height="29" class="arriba_iz">&nbsp;</td>
     <td class="arriba_ce"><span class="letrablanca">::&nbsp;Administracion - Embargos &nbsp;::</span></td>
     <td width="13" class="arriba_de" align="right">&nbsp;</td>
  </tr>
  <tr>
    <td class="cuerpo_iz">&nbsp;</td>
	<td class="cuerpo_ce">
   <img src="../../imagenes/tabla/spacer.gif" width="1" height="1"/> 
   <img src="../../imagenes/tabla/spacer.gif" width="1" height="1"/> 
   <img src="../../imagenes/menu/nuevo.png" title="Nuevo" width="16" height="16" style="cursor:pointer" onClick="nuevo();"/> 
   <img src="../../imagenes/tabla/spacer.gif" width="1" height="1"/> 
   <img src="../../imagenes/menu/grabar.png" title="Guardar" width="16" height="16" onClick="guardar(1)" style="cursor:pointer" id="bGuadar"/> 
   <img src="../../imagenes/tabla/spacer.gif" width="1" height="1"/> 
   <img src="../../imagenes/menu/modificar.png" title="Actualizar" width="16" height="16" onClick="validarCampos(2)" style="cursor:pointer"/> 
   <img src="../../imagenes/tabla/spacer.gif" width="1" height="1"/> 
   <img src="../../imagenes/menu/imprimir.png" width="16" height="16" style="cursor:pointer" title="Imprimir" onClick="window.open('definicionesR.php','mainFrame')" /> 
   <img src="../../imagenes/tabla/spacer.gif" width="1" height="1"/> 
   <img src="../../imagenes/menu/refrescar.png" width="16" height="16" style="cursor:pointer" title="Limpiar campos" onClick="limpiarCampos(); document.forms[0].elements[0].focus();"/> 
   <img src="../../imagenes/tabla/spacer.gif" width="1" height="1"/> 
   <img src="../../imagenes/menu/buscar.png" title="Buscar" width="16" height="16" style="cursor:pointer" onClick="consultaDatos()"/> 
   <img src="../../imagenes/tabla/spacer.gif" width="1" height="1"/> 
   <img src="../../imagenes/menu/informacion.png" width="16" style="border:none; cursor:pointer" title="Manual" onClick="mostrarAyuda();" /> 
   <img src="../../imagenes/menu/notas.png" width="16" height="16" style="cursor: pointer" title="Colaboración en línea" onClick="notas();" /> 
       </td>
    <td class="cuerpo_de">&nbsp;</td>
  </tr>
  <tr>
    <td class="cuerpo_iz">&nbsp;</td>
    <td class="cuerpo_ce"><div id="resultado" style="color:#FF0000"></div></td>
    <td class="cuerpo_de">&nbsp;</td>
  </tr>  
  <tr>
     <td class="cuerpo_iz">&nbsp;</td>
     <td class="cuerpo_ce">
	<table width="95%" border="0" cellspacing="0" class="tablero">
      <tr>
        <td width="25%" align="left">No. Radicaci&oacute;n</td>
        <td width="28%" align="left">
        <select name="pendientesEmb" id="pendientesEmb" class="box1" onchange="buscarRadicacion();">
  <option value="0" selected="selected">Seleccione</option>
                                    
  </select></td>
        <td width="26%" align="left">Fecha </td>
        <td width="21%" align="left"><input name="fecha" class="box1" id="fecha"  /></td>
        </tr>
      <tr>
      <td align="left">Tipo Identificaci&oacute;n</td>
        <td  align="left">
        <select name="tipoI" disabled class="box1" id="tipoI">
        <option value="0" selected="selected">Seleccione...</option>
        <?php
		while($row=mssql_fetch_array($consulta)){
			echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
		}
        ?>
        </select>
          <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
        <td  align="left">N&uacute;mero</td>
        <td  align="left"><input name="numero" class="box1" disabled id="numero" onblur="validarLongNumIdent(document.getElementById('tipoI').value,this);buscarTrabajador(this.value);"  onkeyup="validarCaracteresPermitidos(document.getElementById('tipoI').value,this);"/>
          <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
        </tr>
      <tr>
      <td align="left">Trabajador</td>
        <td align="left"><input name="nombre" type="text" class="boxmediano" id="nombre" readonly="readonly" /></td>
        <td align="left">Id Embargo</td>
        <td align="left"><input name="ide" class=boxfecha id="ide"  /></td>
        </tr>
        <tr>
        <td width="25%" align="left">Fecha Embargo</td>
        <td align="left"><input name="fechaE" class=boxfecha id="fechaE" readonly="readonly"  />
                <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
        <td align="left">Periodo Inicio</td>
        <td align="left"><input name="periodoI" class="box1" id="periodoI"  />
          <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
        </tr>
	    <tr>
	      <td align="left" >Tipo Embargo</td>
	      <td colspan="3" align="left" class="Rojo"><label for="tipoE"></label>
	        <select name="tipoE" class="boxmediano" id="tipoE" onchange="$('#numeroT').val('');limpiarTercero();">
            <option value="0" selected="selected">Seleccione...</option>
            <option value="1">Conyuge</option>
            <option value="2">Padre</option>
            <option value="3">Otro</option>
	          </select>
                <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /> 
              </td>
	      </tr>
	    <tr>
	    <th colspan="4" align="center">Tercero</th>
	    </tr>
	  <tr>
	    <td align="left">Tipo Identificaci&oacute;n</td>
	    <td align="left">
        <select name="tipoIT" class="box1" id="tipoIT">
        <option value="0" selected="selected">Seleccione...</option>
        <?php
		$consulta = $objClase->mostrar_datos(1,2);
		while($row=mssql_fetch_array($consulta)){
		echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
			}
        ?>
        </select>
	      <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
	    <td align="left">N&uacute;mero</td>
            <td align="left"><input name="numeroT" class="box1" id="numeroT" onblur="validarLongNumIdent(document.getElementById('tipoIT').value,this); buscarTercero(this.value);" onkeyup="validarCaracteresPermitidos(document.getElementById('tipoIT').value,this);"  />
	      <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
	  </tr>
	  <tr>
	    <td align="left">P. Nombre</td>
	    <td align="left"><input name="pnombre" class="box1" id="pnombre" onkeypress="return validarEspacio(event)" onkeydown="sololetras(this);" onkeyup="sololetras(this);" />
	      <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
	    <td align="left">S. Nombre</td>
	    <td align="left"><input name="snombre" class="box1" id="snombre" onkeydown="sololetras(this);" onkeyup="sololetras(this);" /></td>
	    </tr>
	  <tr>
	    <td align="left">P. Apellido</td>
	    <td align="left"><input name="papellido" class="box1" id="papellido" onkeypress="return validarEspacio(event)" onkeydown="sololetras(this);" onkeyup="sololetras(this);" />
	      <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
	    <td align="left">S. Apellido</td>
	    <td align="left"><input name="sapellido" class="box1" id="sapellido" onkeydown="sololetras(this);" onkeyup="sololetras(this);" /></td>
	    </tr>
	  <tr>
	    <td align="left">Nombre Corto</td>
	    <td colspan="3" align="left"><input name="nombreCorto" class="boxlargo" id="nombreCorto" readonly="readonly"  />
	      <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
	    </tr>
	  <tr>
	    <td align="left">Depto Residencia</td>
	    <td align="left">
        <select name="cboDepto" class="box1" id="cboDepto" >
        <option value="0">Seleccione..</option>
<?php
	$consulta=$objCiudad->departamentos();
	while($row=mssql_fetch_array($consulta)){
?>
        <option value="<?php echo $row["coddepartamento"]; ?>"><?php echo $row["departmento"]?></option>
<?php
}
?>
      </select>
	      <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
	    <td align="left">Ciudad Residencia</td>
	    <td align="left"><select name="cboCiudad" class="box1" id="cboCiudad">
	    		<option value="0">Seleccione..</option>
	      </select>
	      <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
	    </tr>
	  <tr>
	    <td align="left">Zona Residencia</td>
	    <td align="left"><select name="cboZona" class="box1" id="cboZona">
	    		<option value="0">Seleccione..</option>
	      </select>
	      <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
	    <td align="left">Direcci&oacute;n</td>
	    <td align="left"><input name="tdireccion" type="text" class="box1" id="tdireccion" onfocus="direccion(this,document.getElementById('telefono'));"  /></td>
	  </tr>
	  <tr>
	    <td align="left">Tel&eacute;fono </td>
	    <td align="left"><input name="telefono" class="box1" id="telefono" onkeydown='solonumeros(this);' onkeyup='solonumeros(this);' /></td>
	    <td align="left">Celular</td>
	    <td align="left"><input name="celular" class="box1" id="celular" onkeydown='solonumeros(this);' onkeyup='solonumeros(this);' /></td>
	  </tr>
	  <tr>
	    <td align="left">Tipo Pago</td>
	    <td align="left">
		<select name="tipoP" class="box1" id="tipoP" onchange="buscarTarjeta();">
	        <option value="0" selected="selected">Seleccione...</option>
	        <option value="T">Tarjeta</option>
	        <option value="C">Cheque</option>
		</select>
		<img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
	    <td align="left" id="td_label_tarjeta" style="display:none;">Tarjeta</td>
		<td align="left" id="td_tarjeta"  style="display:none;"><input name="tarjeta" class="box1" id="tarjeta" readonly  /></td>
	  </tr>
	  <tr id="tr_cuenta_bancaria" style="display:none;">
	    <td align="left">Cuenta</td>
	    <td align="left"><input name="cuenta" class="box1" id="cuenta" onkeydown="solonumeros(this);" onkeyup="solonumeros(this);" /></td>
	    <td align="left">Banco</td>
	    <td align="left"><select name="banco" class="box1" id="banco">
	        <option value="0" selected="selected">Seleccione...</option>
	        <?php
			$consulta = $objClase->mostrar_datos(35,2);
			while($row=mssql_fetch_array($consulta)){
				echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
			}
	        ?>
	      </select></td>
	    </tr>
        <tr>
	    <td align="left">Causal</td>
	    <td align="left"><select name="juzgado" class="box1" id="juzgado">
        <option value="0" selected="selected">Seleccione...</option>
        <?php
		$consulta = $objClase->mostrar_datos(36,2);
		while($row=mssql_fetch_array($consulta)){
		echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
			}
        ?>
	      </select>
	      <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
	    <td align="left">Oficio No.</td>
	    <td align="left"><input name="resolucion" class="box1" id="resolucion"  />
	      <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
	    </tr>
	  <tr>
	    <td align="left">Notas</td>
	    <td colspan="3" align="left"><input name="notas" class="boxlargo" id="notas"  /></td>
	    </tr>
	  <tr>
	    <th colspan="4" align="center">Beneficiarios (Seleccione los embargantes)</th>
	    </tr>
        </table>
        <table width="95%" class="tablero" border="0" id="beneficiarios">
	  <tr>
	    <td align="left" width="3%" >&nbsp;</td>
	    <td align="left" width="12%" >Identificaci&oacute;n </td>
	    <td align="left" width="25%" >Nombres</td>
	    <td align="left" width="15%" >Parent </td>
              <td width="7%">Embarga</td>
	    <td align="left" >Relaci&oacute;n con:</td>
	    </tr>
	  
        </table></td>
    <td class="cuerpo_de">&nbsp;</td>
  </tr>
  <tr>
    <td class="abajo_iz" >&nbsp;</td>
    <td class="abajo_ce" ></td>
    <td class="abajo_de" >&nbsp;</td>
  </tr>
</table>
</center>
<input type="hidden" value="<?php echo $_SESSION["USUARIO"];?>" name="usuario" id="usuario" />
<input type="hidden" name="idr" id="idr" />
</form>

<!-- FORMULARIO OBSERVACIONES-->
<div name="div-observaciones-tab" style="display:none" title="Observaciones de modificaci&oacute;n">
<table class="tablero">
 <tr>
   <td>Usuario</td>
   <td colspan="3" >
   <input name="usuarioObs" id="usuarioObs" class="box1" disabled="disabled" value="<?php echo $_SESSION['USUARIO']?>" /></td>
   </tr>
 <tr>
   <td>Observaciones</td>
   <td colspan="3" >
   <textarea name="observacionGrupo" id="observacionGrupo" cols="45" rows="5" class="boxlargo"></textarea></td>
   </tr>
</table>
<div class="ui-state-highlight ui-corner-all" style="margin: 9px auto; display:none;padding: 6px; text-align: center; border: 1px solid rgb(255, 255, 0); width:300px" id="rtaObservacion"><span class="ui-icon ui-icon-info" style="float: left; margin-right: 0.3em;"></span>Observaci&oacute;n guardada con &eacute;xito.</div>
</div>


<!-- formulario direcciones  -->
<div id="dialog-form" title="Formulario de direcciones"></div>

<!-- Manual Ayuda -->
<div id="ayuda" title="Manual de Embargos" style="background-image:url(../../imagenes/FondoGeneral0.png)">
</div>
</body>

</html>