<?php 
date_default_timezone_set('America/Bogota');
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

//arreglo para imprimir
$datos = array("embargante"=>array(),"error"=>0,"descripcion"=>"","registros"=>array());

$identificacion= $_POST["v0"];
$tipoIdentificacion=$_POST["v1"];

//comprobar si la persona existe
$sql = "select idpersona,pnombre+' '+isnull(snombre,'')+' '+papellido+' '+isnull(sapellido,'') as embargante from aportes015 where identificacion='".$identificacion."' and idtipodocumento=".$tipoIdentificacion;   
$rs = $db->querySimple($sql);
if($row = $rs->fetch()){
	$datos["embargante"][0]=$row["idpersona"];
	$datos["embargante"][1]=$row["embargante"];
}else{
	$datos["error"]=1;
	$datos["descripcion"]="La Persona Embargante no existe en SIGAS.";
}
if($datos["error"]==0){
	$sql="select  a18.idembargo,t15.idpersona as idtrabajador,t15.identificacion as idetrabajador,t15.pnombre+' '+isnull(t15.snombre,'')+' '+t15.papellido+' '+isnull(t15.sapellido,'') as trabajador,
       		     b15.idpersona as idbeneficiario,b15.identificacion as idebeneficiario,b15.pnombre+' '+isnull(b15.snombre,'')+' '+b15.papellido+' '+isnull(b15.sapellido,'') as beneficiario
          from aportes018 a18
              inner join aportes022 a22 on a22.idembargo=a18.idembargo
              inner join aportes015 t15 on t15.idpersona=a18.idtrabajador
              inner join aportes015 b15 on b15.idpersona=a22.idbeneficiario
          where a18.estado='A'-- and a18.idtercero=".$datos["embargante"][0];
	$rs=$db->querySimple($sql);
	if($row=$rs->fetch(PDO::FETCH_ASSOC)){
		$datos["registros"][]=$row;
	}else{
		$datos["error"]=1;
		$datos["descripcion"]="El Embargante no tiene embargos a favor.";
	}
}
echo json_encode($datos);
?>