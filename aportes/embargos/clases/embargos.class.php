<?php
/* autor:       Orlando Puentes
 * fecha:       Septiembre 16 de 2010
 * objetivo:    Almacenar en la base de datos toda la información relacionada a los embargos. 
 */
 
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;


include_once $raiz . DIRECTORY_SEPARATOR . 'rsc' . DIRECTORY_SEPARATOR . 'conexion.class.php';

class Embargos{
 //constructor
var $con;
function Embargos(){
 		$this->con=new DBManager;
 	}

function insert($campos){
    if($this->con->conectar()==true){
    $sql="INSERT INTO aportes018(idtrabajador, idtercero, idconyuge, idtipoembargo, fechaembargo, periodoinicio, tipopago, codigopago, idbanco, cuenta, idjuzgado, resolucion, notas, estado, fechasistema, usuario)
    VALUES(".$campos[0].", ".$campos[1].", ".$campos[2].", ".$campos[3].", '".$campos[4]."', '".$campos[5]."', '".$campos[6]."', '".$campos[7]."', ".$campos[8].", '".$campos[9]."', ".$campos[10].", '".$campos[11]."', '".$campos[12]."', 'A', cast(getdate() as date), '".$campos[13]."')";
    return mssql_query($sql,$this->con->conect);
    }
}

function insert_detalle($campos){
    if($this->con->conectar()==TRUE){
        $sql="INSERT INTO aportes022(idembargo, idbeneficiario)
    VALUES(".$campos[0].",".$campos[1].")";
        return mssql_query($sql,$this->con->conect);
    }
}

function actualizar_21($idb,$idt){
    if($this->con->conectar()==TRUE){
        $sql="Update aportes021 set embarga='S' where idbeneficiario=$idb and idtrabajador=$idt";
        return mssql_query($sql,$this->con->conect);
    }
}

function buscar_embargos($param) {
    if($this->con->conectar()==true){
        $sql="SELECT aportes018.*,pnombre,snombre,papellido,sapellido,identificacion,detalledefinicion FROM aportes018
INNER JOIN aportes015 ON aportes018.idtercero=aportes015.idpersona INNER JOIN aportes091 ON aportes018.idjuzgado=aportes091.iddetalledef WHERE idtrabajador=$param";
        return mssql_query($sql,$this->con->conect);
    }
}

function buscar_detalle_e($param) {
    if($this->con->conectar()==true){
        $sql="SELECT aportes022.idbeneficiario,pnombre,snombre,papellido,sapellido FROM aportes022 INNER JOIN aportes015 on aportes022.idbeneficiario=aportes015.idpersona
WHERE idembargo=$param";
        return mssql_query($sql,$this->con->conect);
    }
}

function buscar_id_persona($v0,$v1){
    if($this->con->conectar()==true){
        $sql="Select idpersona from aportes015 where idtipodocumento='$v0' and identificacion='$v1'";
        return mssql_query($sql,$this->con->conect);
    }
}

function buscar_un_embargo($idtra,$idter){
    if($this->con->conectar()==true){
    $sql="SELECT idembargo,aportes015.identificacion, aportes015.pnombre,aportes015.snombre,aportes015.papellido,aportes015.sapellido,
a15.identificacion AS cct, a15.pnombre AS pnt,a15.snombre AS snt,a15.papellido AS pat,a15.sapellido AS sat FROM aportes018
INNER JOIN aportes015 ON aportes018.idtrabajador=aportes015.idpersona INNER JOIN aportes015 a15 ON aportes018.idtercero=a15.idpersona
WHERE aportes018.idtrabajador=$idtra AND idtercero=$idter";
    return mssql_query($sql,$this->con->conect);
    }
}

function inactivar_embargo($param,$param2,$param3) {
    if($this->con->conectar()==true){
        $sql="Update aportes018 set estado='I',fechaestado=cast(getdate() as date),motivoestado='$param2',usuarioestado='$param3' where idembargo=$param";
        return mssql_query($sql,$this->con->conect);
    }
}

function activar_beneficiario($param) {
    if($this->con->conectar()==true){
        $sql="UPDATE aportes021 SET embarga='N' WHERE idbeneficiario IN(SELECT idbeneficiario FROM aportes022 WHERE idembargo=$param)";
        return mssql_query($sql,$this->con->conect);
    }
}


}
?>