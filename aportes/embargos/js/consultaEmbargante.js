$(function(){
	$("#txtNumero").blur(function(){
	    identificacion=$("#txtNumero").val().trim();
	    tipoIdentificacion=$("#cmbTipoDocumento").val(); 
	    if(identificacion=="")
	    	return false;
	    
	    $("#tdMensajeError,#tabEmbargos").html("");
	    
	    $.ajax({
	    	url:"consultaEmbargante.log.php",
	    	type:"POST",
	    	data:{v0:identificacion,v1:tipoIdentificacion},
	    	dataType:"json",
	    	async:true,
	    	success:function(datos){
	    		$("#tdNombres").html(datos["embargante"][0]+" - "+datos["embargante"][1]);
	    		if(datos["error"]==1){
	    			$("#tdMensajeError").html(datos["descripcion"]);
	    			return false;
	    		}
	    		var idEmbargo=0;
	    		$.each(datos["registros"],function(i,row){
	    			if(idEmbargo!=row["idembargo"]){
	    				$("#tabEmbargos").append("<tr><th align='center' colspan='3'>Embargado</th></tr>");
	    				$("#tabEmbargos").append("<tr><td align='center' colspan='3'>Id "+row["idtrabajador"]+" - "+row["idetrabajador"]+"  "+row["trabajador"]+"</td></tr>");
	    				$("#tabEmbargos").append("<tr><th align='center' colspan='3'>Beneficiario</th></tr>");
	    				$("#tabEmbargos").append("<tr><th>Id</th><th>Identificacion</th><th>Nombres</th></tr>");
	    			}
	    			$("#tabEmbargos").append("<tr><td>"+row["idbeneficiario"]+"</td><td>"+row["idebeneficiario"]+"</td><td>"+row["beneficiario"]+"</td></tr>");
	    		});
	    		
	    	}
	    	
	    });
	    
	    
	    	
	    
	   
	});
});