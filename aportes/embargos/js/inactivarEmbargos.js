/*
* @autor:      Ing. Orlando Puentes
* @fecha:      Octubre 7  de 2010
* objetivo:
*/

var URL=src();
var ide=0;

$(document).ready(function(){
	
	nuevo();
	// Dialog Colaboracion en linea
	$("#dialog-form2").dialog({
		autoOpen: false,
		height: 450,
		width: 700,
		modal: false,
		buttons: {
			'Enviar': function() {
				var bValid = true;
				var campo=$('#notas').val();
				var campo0=$.trim(campo);
				if (campo0==""){
					$(this).dialog('close');
					return false;
				}
				var campo1=$('#usuario').val();
				var campo2="consultaEmbargos.php";
				$.post(URL+'phpComunes/colaboracion.php',{v0:campo0,v1:campo1,v2:campo2},
						function(datos){
					if(datos=='1'){
						alert("Su comentario fue enviado correctamente!. Gracias por participar en el mejoramiento de SIGAS");
					}else{
						alert(datos);
					}
				});
				$(this).dialog('close');
			},
			Cancelar: function() {
				$(this).dialog('close');
				$("#dialog-form2").dialog("destroy");
			}
		}
	});
	
	$("#ayuda").dialog({
		autoOpen: false,
		height: 450, 
		width: 700, 
		draggable:true,
		modal:false,
		open: function(evt, ui){
			$('#ayuda').html('');
			$.get(URL+'help/aportes/ayudaInactivarEmbargo.html',function(data){
				$('#ayuda').html(data);
			})
		}
	});
	
	// Buscar Trabajador
	$("#buscarT").click(function(){
		$("#trabajadores").find("p").remove();
		if($("#tipoI").val()==0) {
			alert("Falta tipo identificaci\u00F3n");
			$("#tipoI").focus();
			return false;
		}
		if($("#tipoI2").val()==0) {
			alert("Falta tipo identificaci\u00F3n");
			$("#tipoI2").focus();
			return false;
		}
		// VALIDAR QUE EL identificacion SEA NUMEROS
		var n=parseInt($("#numero").val())
		if( isNaN(n)){
			$(this).next("span.Rojo").html("La Identificaci\u00F3n debe ser num\u00E9rico.").hide().fadeIn("slow");
			return false;
		}
		var n2=parseInt($("#numero2").val())
		if( isNaN(n2)){
			$(this).next("span.Rojo").html("La Identificaci\u00F3n debe ser num\u00E9rico.").hide().fadeIn("slow");
			return false;
		}
	
		if($("#numero").val()==''){
			$(this).next("span.Rojo").html("Ingrese un valor a buscar.").hide().fadeIn("slow");
			$("#numero").focus();
			return false;
		}	
		if($("#numero2").val()==''){
			$(this).next("span.Rojo").html("Ingrese un valor a buscar.").hide().fadeIn("slow");
			$("#numero2").focus();
			return false;
		}
	
	
		var v0=$("#tipoI").val();
		var v1=$("#numero").val();
		var v2=$("#tipoI2").val();
		var v3=$("#numero2").val();
		var idt=0;
		$.getJSON('buscarUnEmbargo.php',{v0:v0,v1:v1,v2:v2,v3:v3},function(data){
			if(data==0){
				alert("Lo lamento el embargo no existe!")
				return false;
			}
	
			$.each(data,function(i,fila){
				ide=fila.idembargo;
				$("#trabajadores").append('<p align=left>Trabajador:&nbsp;<strong>'+fila.identificacion+" "+fila.pnombre+" "+fila.snombre+" "+fila.papellido+" "+fila.sapellido+'</p>');
				$("#trabajadores").append('<p align=left>Tercero:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>'+fila.cct+' '+fila.pnt+' '+fila.snt+' '+fila.pat+' '+fila.sat+'</strong></p');
				$("#trabajadores").append('<br/><hr width=90%><br/>');
				$("#trabajadores").append('<p class=Rojo>&nbsp;&nbsp;&nbsp;&nbsp;Nota: Si estas seguro de INACTIVAR este embargo, escriba el motivo de la inactivaci\u00F3n y luego, haga click en el boton Guardar');
				return;
			});// fin each
		});// fin JSON*/
	});// fin click
// ... ICONO PARA OCULTAR/MOSTRAR FORMULARIO ...
});// fin read
function guardar(){
	if($("#motivo").val().trim().length<10){
		alert("Falta el Motivo Inactivaci\u00F3n o es muy corto!");
		return false;
	}
	if(confirm("�Esta seguro de INACTIVAR este Embargo?")){
		v1=$("#motivo").val().trim();
		$.getJSON('inactivar.php', {v0:ide,v1:v1}, function(datos){
			if(datos==0){
				alert('No se pudo inactivar el embargo, por favor llamar al administrador del sistema!');
				return false;
			}
			alert("Se inactivo el embargo!")
			nuevo();
		})
	}
}

function nuevo(){
	$("#tipoI,#tipoI2").val(1);
	$("#numero,#numero2,#motivo").val("");
	$("#trabajadores").empty();
}

