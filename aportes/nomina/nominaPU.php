<?php
if ( !isset( $_SESSION ['USUARIO'] ) ) {
	session_start();
	$raiz=$_SESSION['RAIZ'];
	include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'session.php';
	session();
	}
else{
	//header("Location: http://10.10.1.121/sigas/error.html");
//exit();
}
include_once $raiz.DIRECTORY_SEPARATOR.'config.php';
$url=$_SERVER['PHP_SELF'];
include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'auditoria.php';
auditar($url);

//$dirPlanos= $ruta_generados. DIRECTORY_SEPARATOR.'Nomina'.DIRECTORY_SEPARATOR.DIRECTORY_SEPARATOR.'planos_asocajas'.DIRECTORY_SEPARATOR;
//$dirProcesar= $ruta_generados. DIRECTORY_SEPARATOR.'planos_generados'.DIRECTORY_SEPARATOR.'nominas'.DIRECTORY_SEPARATOR;

//$dirPlanos = '..\..\..\planos_cargados\Nomina\planos_asocajas'; //se cambia la ruta. nueva ruta queda por fuera de sigas
//$dirProcesar = '..\..\..\planos_generados\nominas'; //se cambia la ruta. nueva ruta queda por fuera de sigas

$dirPlanos='..\..\..\planos_cargados\planillas\cargados';
$dirProcesar = '..\..\..\planos_cargados\planillas\procesadas';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>::Cargar Nomina Planilla &Uacute;nica::</title>
<link type="text/css" href="../../css/formularios/base/ui.all.css" rel="stylesheet" />
<link type="text/css" href="../../css/Estilos.css" rel="stylesheet"/>
<link type="text/css" href="../../css/estilo_tablas.css" rel="stylesheet"/>
<link href="../../css/formularios/custom-theme/jquery-ui.css" rel="stylesheet" type="text/css" />
<link href="../../css/marco.css" rel="stylesheet" type="text/css" />
<link href="../../css/fileuploader.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../../js/jquery-1.4.2.js"></script>
<script src="../../js/formularios/ui/jquery-ui-1.8.custom.js" type="text/javascript" language="javascript"></script>
<script src="../../js/fileuploader.js" type="text/javascript"></script>
<script type="text/javascript" src="js/buscarArchivos.js" language="javascript"></script>
<script type="text/javascript" src="js/leerArchivo.js" language="javascript"></script>
<script type="text/javascript" src="js/procesarNominasPU.js" language="javascript"></script>
<script type="text/javascript" src="../../js/jquery.copy.min.js" language="javascript"></script>
<script type="text/javascript" src="js/inicial.js" language="javascript"></script>
<style type="text/css">
#btnProcesarNominas{
	boder: 1px solid #AFAFAF;
	width: 180px;
	height: 50px;
	text-align: left;
	font-size: 15px;
	background-image: url('../../imagenes/procesar.png');
	background-position: right;
	background-repeat: no-repeat;
}
.box-table-a{
	width: none;
}
</style>
</head>

<body>
<div id="wrapTable">
  <table width="97%" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
     <td width="13" height="29" class="arriba_iz">&nbsp;</td>
     <td class="arriba_ce">
     <span class="letrablanca">::&nbsp;Cargar N&oacute;mina Planilla &Uacute;nica::</span></td>
      <td width="13" class="arriba_de" align="right">&nbsp;</td>
      </tr>

    <tr>
     <td class="cuerpo_iz">&nbsp;</td>
	 <td class="cuerpo_ce">
        <img src="../../imagenes/spacer.gif" width="1" height="1" />
        <img src="../../imagenes/spacer.gif" width="1" height="1" />
        <img src="../../imagenes/spacer.gif" width="1" height="1" />
        <img src="../../imagenes/spacer.gif" width="1" height="1" />
        <img src="../../imagenes/menu/informacion.png" width="16" height="16" style="border: none; cursor: pointer" title="Ayuda en l&iacute;nea" onClick="mostrarAyuda();" />
  <img src="../../imagenes/menu/notas.png" width="16" height="16" style="cursor: pointer" title="Colaboraci&oacute;n en l&iacute;nea" onclick="notas();" />
        <div id="error" style="color:#FF0000"></div>
      </td>
      <td class="cuerpo_ce">&nbsp;</td>
      </tr>

    <tr>
      <td class="cuerpo_iz">&nbsp;</td>
      <!-- TABLAS DE FORMULARIO -->
      <td class="cuerpo_ce">
        <center>
          <table width="90%" border="0" cellspacing="0" class="tablero">
          <tr>
          	<td>
          		<div id="divCargarArchivos" style="display: inline-block">
					<noscript>
						<p>Please enable JavaScript to use file uploader.</p>
						<!-- or put a simple form for upload here -->
					</noscript>
				</div>
	          	<!-- <a id="lnkCargarArchivos" href="\\<?php //echo $_SERVER['SERVER_ADDR'] ?>\planos_asocajas\" target="_blank" style="text-decoration: none; width: 200px; margin: 0 auto; display: inline-block; text-align: center;">
	          		<img src="../../imagenes/Upload.png" alt="" style="border: 0; width: 32px" />Cargar&nbsp;archivos&nbsp;al&nbsp;servidor
	          	</a>-->
	          	<input type="hidden" name="hdnDirPlanos" id="hdnDirPlanos" value="<?php echo $dirPlanos ?>" />
	          	<input type="hidden" name="hdnDirDestino" id="hdnDirDestino" value="<?php echo $dirProcesar ?>" />
	          	<a id="lnkRefrescar" href="#" target="_blank" style="text-decoration: none; width: 130px; margin: 0 auto; display: inline-block; text-align: center;">
	          		<img src="../../imagenes/refrescar.png" alt="" style="border: 0; width: 32px" />Refrescar archivos
	          	</a>
          	</td>
          </tr>
            <tr>
              <td width="186">Archivos cargados por procesar:</td>
            </tr>
            <tr>
            	<td>
            		<table id="tblArchivosCargados" border="0" cellspacing="0" class="box-table-a" style="margin: 1em auto; ">
            			<thead>
            				<th><strong>Nombre Archivo</strong></th>
            				<th><strong>Tama&ntilde;o</strong></th>
            				<th><strong>Fecha Cargue</strong></th>
            				<th><strong>Marcar Todos&nbsp;<input type="checkbox" id="chkMarcarTodo" name="chkMarcarTodo" /></strong></th>
            			</thead>
            		</table>
            	</td>
            </tr>
			<tr>
				<td>
					<p align="center"><input type="button" id="btnProcesarNominas" name="btnProcesarNominas" value="Procesar Nominas" /></p>
				</td>
			</tr>
          </table>
        </center>
       <td class="cuerpo_de">&nbsp;</td><!-- FONDO DERECHA -->
      </center>
      </tr>
    <tr>
      <td class="abajo_iz" >&nbsp;</td>
      <td class="abajo_ce" ></td>
      <td class="abajo_de" >&nbsp;</td>
    </tr>
  </table>
</div>
<!-- colaboracion en linea -->
<div id="div-observaciones-tab"></div>
<div id="dialogo-archivo" title="Planilla &Uacute;nica">
<div id="progreso" style="display: none; font-size: 15pt; font-weight: bold;"><span id="pg">0</span> de <span id="tt"></span> archivos</div>
<div id="log"></div>
</div>
<div id="dialog-form2" title="Colaboraci&oacute;n en l&iacute;nea" style="display:none">
<p>Por favor diligencie este formulario para enviar comentarios, errores o falencias encontradas en el proceso. M&aacute;ximo 250 caracteres </p>
<label>Tus comentarios:</label><br />
<textarea name="notas" id="notas" cols="60" rows="10"></textarea>
</div>

<!-- fin colaboracion -->

<!-- Manual Ayuda -->
<div id="ayuda" title="Manual Ayuda Planilla �nica" style="background-image:url('../../imagenes/FondoGeneral0.png')">
</div>
</body>
<script language="javascript">
function mostrarAyuda(){
	$("#ayuda").dialog('open');
	}

function notas(){
	$("#dialog-form2").dialog('open');
	}
</script>
</html>