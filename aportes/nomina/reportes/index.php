<?php
/* autor:       orlando puentes
 * fecha:       28/09/2010
 * objetivo:
 */
ini_set("display_errors",'1');
if ( !isset( $_SESSION ['USUARIO'] ) ) {
	session_start();
	$raiz=$_SESSION['RAIZ'];
	include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'session.php';
	session();
	}
else{
	//header("Location: http://10.10.1.121/sigas/error.html");
//exit();
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Men&uacute; Reportes Planilla &Uacute;nica</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="<?php echo URL_PORTAL.DIRECTORIO_ESTILOS_CSS; ?>estiloReporte.css" rel="stylesheet" type="text/css">
<script language="javascript" src="<?php echo URL_PORTAL.DIRECTORIO_SCRIPTS_JS; ?>jquery-1.3.2.min.js"></script>
<script language="javascript" src="<?php echo URL_PORTAL.DIRECTORIO_SCRIPTS_JS; ?>comunes.js"></script>
</head>
<body>
<center>
  <table width="100%" border="0">
  <tr>
    <td align="center" ><img src="<?php echo URL_PORTAL; ?>centroReportes/imagenes/razonSocialRpt.png" width="518" height="91" /></td>
  </tr>
  <tr>
    <td align="center" >&nbsp;</td>
  </tr>
  <tr>
    <td class="fecha" ></td>
  </tr>
</table>
<table width="60%" border="0" cellspacing="1" class="tablero">
	<tr>
		<th>Reporte</th>
		<th>Listado de Reportes de Planilla &Uacute;nica</th>
	</tr>
	<tr>
		<td scope="row" align="left">Reporte001</td>
		<td align="left"><label style="cursor: pointer;" onClick="imprimir(1)">Empresas Aportantes Periodo Actual</label></td>
	</tr>
	<tr>
		<td scope="row" align="left" width="20%">Reporte002</td>
		<td align="left"><label style="cursor: pointer;" onClick="imprimir(2)">Empresas con aportes pero sin planilla &uacute;nica.</label></td>
	</tr>
   <!-- <tr>
      <td scope="row" align="left" width="20%">Reporte003</td>
      <td align="left"><label style="cursor:pointer" onClick="imprimir(3);">Trabajadores Activos</label></td>
    </tr> -->
	<tr>
		<td scope="row">Reporte004</td>
		<td><label style="cursor:pointer" onClick="imprimir(4);">Trabajadores Activos NO reportados en Planilla Unica</td>
	</tr>
	<tr>
		<td scope="row">Reporte005</td>
		<td><label style="cursor:pointer" onClick="configurar(4);">Trabajadores con Mas de 240 horas &oacute; Menos de 96 Horas</label></td>
	</tr>
	<tr>
		<td scope="row">Reporte006</td>
		<td><label style="cursor:pointer" onClick="configurar(5);">Novedades por Empresa</label></td>
	</tr>
	<tr>
		<td scope="row">Reporte007</td>
		<td><label style="cursor:pointer" onClick="configurar(6);">Afiliaciones de planilla &uacute;nica NO Legalizadas</label></td>
	</tr>
	<tr>
		<td scope="row">Reporte008</td>
		<td><label style="cursor:pointer" onClick="imprimir(5);">Planillas &uacute;nicas sin soporte de pago.</td>
		</tr>
	<tr>
		<td scope="row">Reporte009</td>
		<td><label style="cursor:pointer" onClick="configurar(7);">Verificaci&oacute;n de &uacute;ltimo retiro reportado por afiliados</label></td>
	</tr>
</table>
  </center>
  </body>
<script language="javascript">
var URL=src();
function configurar(nu){
	switch (nu){
		case 1 :url=URL+"centroReportes/configurarReporte.php?tipo=1&archivo=tarjeta/reporte001.php";
		break;
        case 2 :url=URL+"centroReportes/configurarReporte.php?tipo=1&archivo=tarjeta/reporte003.php";
		break;
		case 3 : url=URL+"eventos/bonoComputador/configReporte004.php";
		break;
		case 4 : url=URL+"aportes/nomina/reportes/configReporte005.php";
		break;
		case 5 : url=URL+"aportes/nomina/reportes/configReporte006.php";
		break;
		case 6 : url=URL+"aportes/nomina/reportes/configReporte007.php";
		break;
		case 7 : url=URL+"aportes/nomina/reportes/configReporte009.php";
		break;
	}
	window.open(url,"myFrame" );
}

function imprimir(nu){
    switch(nu){
        case 1 : url=URL+"centroReportes/aportes/nomina/reporte001.php";
        break;
        case 2 : url=URL+"centroReportes/aportes/nomina/reporte002.php";
		break;
		case 3 : url=URL+"centroReportes/aportes/nomina/reporte003.php";
		break;
		case 4 : url=URL+"centroReportes/aportes/nomina/reporte004.php";
		break;
		case 5 : url=URL+"centroReportes/aportes/nomina/reporte008.php";
		break;
    }
    window.open(url, '_blank')
}
</script>
</html>
