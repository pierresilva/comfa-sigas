/**
 * Acciones iniciales del formulario de nominas por planilla unica
 *
 * @author Jose Luis Rojas
 * @version 1.0.0
 * @since 24 Agosto 2010
 */
$(document).ready(function(){
	//buscarArchivos();
	//Marcar todos
	
	
	$('#btnProcesarNominas').bind('click', procesarPlanilla);
	
	crearCargador();
	
	/*$('#lnkCargarArchivos').bind('click', function(evt){
		var ruta = $(this).attr('href');
		window.prompt ("Por favor copie y abra esta direccion en el navegador", ruta);
		evt.preventDefault();
	});*/
	
	
});


function procesarPlanilla(){
	$("#boton1").hide();
	$.getJSON('contarPlanillas.php',function(datos){
		var x = datos.length;
		alert("Se van a procesar: " + x + " Planillas");
		$("#mensaje1").append("Inicio proceso: " + hora()  +"<br>");
		$.each(datos,function(i,a){
			$("#mensaje1").append("Procesando: " + a.archivo + "<br>" );
			$.ajax({
				  url: 'procesarPlanilla.php',
				  type: 'GET',
				  data:{v0:a.archivo},
				  async: 'false',
				  dataType: 'json',
				  success: function(dato){
					if (dato==0){
						$("#mensaje1").append(hora() + "El archivo " + a.archivo + " fue procesado correctamente<br>" );
					}
					if (dato==1){
						$("#mensaje1").append(hora() + "El archivo " + a.archivo + " YA fue procesado<br>");
					}
					if (dato==2){
						$("#mensaje1").append(hora() + "No se pudo crear la empresa en SIGAS " + a.archivo +"<br>");
					}
					if (dato==3){
						$("#mensaje1").append(hora() + "No se pudo crear la empresa en INFORMA WEB " + a.archivo + "<br>");
					}
					if (dato==4){
						$("#mensaje1").append(hora() + "NO se pudo guardar la cabecera del archivo " + a.archivo + "<br>");
					}
				  }
				})
				if(i==(x-1)){
					$("#mensaje1").append("Fin del proceso: " + hora()  +"<br><br><br>RECUERDE REVISAR LOS ARCHIVOS LOG");
				}
			}); 
		})
										
	}