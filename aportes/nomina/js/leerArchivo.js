/**
 * 
 * Pasa los datos traidos de los archivos de planilla unica a la IG 
 * 
 * @param evt
 * @return String Archivo de planilla unica 
 */
var leerArchivo = function(evt){
	var id = $(this).attr('href');
	evt.preventDefault();
	$('#log').html('');
	$('#dialogo-archivo').dialog({
		width: 500,
		resizable: true,
		modal: true,
		open: function(){
			$('#log').load('leerArchivo.php',{id:id});
		},
		close: function(){
			$(this).dialog('destroy');
		}
	});
}