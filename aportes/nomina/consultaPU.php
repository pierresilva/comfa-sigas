<?php
//include_once 'config.php';
/* autor:       Orlando Puentes
 * fecha:       Julio 23 de 2010
 * objetivo:    Consultar en la base de datos de Aportes y Subsidio el trabajador afiliado a Comfamiliar Huila. 
 */

if ( !isset( $_SESSION ['USUARIO'] ) ) {
	session_start();
	$raiz=$_SESSION['RAIZ'];
	include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'session.php';
	session();
	}
else{
	//header("Location: http://10.10.1.121/sigas/error.html");
//exit();
}
$url=$_SERVER['PHP_SELF'];
include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'auditoria.php';
//include_once 'phpComunes/auditoria.php';
auditar($url);
include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>::CONSULTA NOMINAS PROCESADAS POR NIT::</title>
<link type="text/css" href="<?php echo URL_PORTAL; ?>css/formularios/base/ui.all.css" rel="stylesheet" /> 
<link type="text/css" href="<?php echo URL_PORTAL; ?>css/Estilos.css" rel="stylesheet"/>
<link type="text/css" href="<?php echo URL_PORTAL; ?>css/estilo_tablas.css" rel="stylesheet"/>
<link type="text/css" href="<?php echo URL_PORTAL; ?>css/marco.css" />
<!--  <script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/jquery-1.3.2.min.js"></script> -->
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/jquery-1.4.2.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/formularios/ui/ui.core.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/formularios/ui/ui.tabs.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/formularios/ui/ui.draggable.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/formularios/ui/ui.resizable.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/formularios/ui/ui.dialog.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/formularios/ui/ui.datepicker.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/formularios/ui/ui.button.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/formularios/ui/ui.accordion.js"></script>

<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/comunes.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/llenarCampos.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/direccion.js"></script>

<script type="text/javascript" src="../../js/jquery.tablesorter.js"></script>
<script type="text/javascript" src="../../js/jquery.tablesorter.pager.js"></script>
<script type="text/javascript" src="js/consultaNit.js"></script>
<!--<script type="text/javascript" src="js/aportesTab.js"></script>
<script type="text/javascript" src="js/planillaTab.js"></script>
 <script type="text/javascript" src="js/consultaConyuge.js"></script>  -->

<script type="text/javascript">
shortcut.add("Shift+F",function() {
		                                var URL=src();
		var url=URL+"aportes/trabajadores/consultaTrabajador.php";
    	window.open(url,"_blank");
    },{
	'propagate' : true,
	'target' : document 
        });        
</script>


<script language="javascript">
function mostrarAyuda(){
	$("#ayuda").dialog('open' );
	}
function notas(){
	$("#dialog-form2").dialog('open');
	}	
</script>
<style type="text/css">
.sortable {border:1px solid #eaeaea;margin:10px auto}
.sortable th {background:#f3f3f3; text-align:left; color:#666; border:1px solid #eaeaea; border-right:none; padding:1px}
.sortable th h3 {padding:0 18px; margin:0; font:11px Verdana, Geneva, sans-serif}
.sortable td {padding:1px;border:1px solid #eaeaea; font-size:10px; text-align:left}
.sortable .head h3 {background:url(../../imagenes/imagesSorter/sort.gif) 7px center no-repeat; cursor:pointer; padding:0 18x}
.sortable .desc h3 {background:url(../../imagenes/imagesSorter/desc.gif) 7px center no-repeat; cursor:pointer; padding:0 18px}
.sortable .asc h3 {background:url(../../imagenes/imagesSorter/asc.gif) 7px  center no-repeat; cursor:pointer; padding:0 18px}
.sortable .head:hover, .sortable .desc:hover, .sortable .asc:hover {color:#000}
.sortable .evenrow td {background:#ecf2f6}
.sortable .oddrow td {background:#fff}
.sortable td.evenselected {background:#F8F8F8}
.sortable td.oddselected {background:#ffffff}

#controls { margin:0 auto; height:20px; /*width:800px;*/ overflow:hidden;}
#perpage {float:left; width:200px; margin-left:40px;}
#perpage select {float:left; font-size:11px}
#perpage label {float:left; margin:2px 0 0 5px; font-size:10px}
#navigation {float:left;/* width:600px;*/ text-align:center}
#navigation img {cursor:pointer}
#text {float:left; width:200px; text-align:left; margin-first:2px; font-size:10px}
.pager{ margin:10px;}
</style>
</head>

<body>
<div id="wrapTable">
  <table width="97%" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
      <td width="13" height="29" background="<?php echo URL_PORTAL; ?>imagenes/tabla/arriba_izq.gif">&nbsp;</td>
      <td background="<?php echo URL_PORTAL; ?>imagenes/tabla/arriba_central2.gif"><span class="letrablanca">::&nbsp;Consulta de nominas procesadas por NIT&nbsp;::</span></td>
      <td width="13" background="<?php echo URL_PORTAL; ?>imagenes/tabla/arriba_der.gif" align="right">&nbsp;</td>
      </tr>
    
    <tr>     
      <td background="<?php echo URL_PORTAL; ?>imagenes/tabla/izquierda2.jpg">&nbsp;</td>
      <td align="left" background="<?php echo URL_PORTAL; ?>imagenes/tabla/centro.gif">
        <img src="<?php echo URL_PORTAL; ?>imagenes/spacer.gif" width="1" height="1"> 
        <img src="<?php echo URL_PORTAL; ?>imagenes/spacer.gif" width="1" height="1"> 
        <img src="<?php echo URL_PORTAL; ?>imagenes/spacer.gif" width="1" height="1"> 
        <img src="<?php echo URL_PORTAL; ?>imagenes/spacer.gif" width="1" height="1"> 
        <img src="<?php echo URL_PORTAL; ?>imagenes/menu/informacion.png" width="16" height="16" style="border: none; cursor: pointer" title="Manual" onClick="mostrarAyuda();" /> 
  		<img src="<?php echo URL_PORTAL; ?>imagenes/menu/notas.png" width="16" height="16" style="cursor: pointer" title="Colaboracíon en línea" onclick="notas();" />
        <div id="error" style="color:#FF0000"></div>
      </td>
      <td background="<?php echo URL_PORTAL; ?>imagenes/tabla/derecha.gif">&nbsp;</td>
      </tr>
    <tr>
      <td background="<?php echo URL_PORTAL; ?>imagenes/tabla/izquierda2.jpg">&nbsp;</td>
      <!-- TABLAS DE FORMULARIO -->
      <td align="center" background="<?php echo URL_PORTAL; ?>imagenes/tabla/centro.gif">
        <center>
          <table width="90%" border="0" cellspacing="0" class="tablero">
            <tr>
              <td width="148">NIT</td>
              <td width="149"><input type="text" name="txtNit" id="txtNit" class="box1" onblur="validarLongNumIdent(5,this)" onkeyup="validarCaracteresPermitidos(5,this);"/></td>
              <td width="316"><span id="lblRazsoc"></span></td>
            </tr>
            <tr>
              <td width="148">Sucursal</td>
              <td width="149">
              	<select name="sltSucursal" id="sltSucursal" class="box1">
              		<option value="NA">Seleccione...</option>
              	</select>
              </td>
              <td width="316">&nbsp;</td>
            </tr>
            <tr>
              <td width="20%">Por Periodos&nbsp;<input type="radio" name="radTipoconsulta" id="radConsXPeriodo" checked="checked" /></td>
              <td width="50%">Periodo Inicial&nbsp;&nbsp;<input type="text" name="txtPerIni" id="txtPerIni" class="box1" /> &nbsp;Periodo Final&nbsp;&nbsp;<input type="text" name="txtPerFin" id="txtPerFin" class="box1" /></td>
              <td width="30%">&nbsp;</td>
            </tr>
            <tr>
              <td width="148">Por Fechas&nbsp;<input type="radio" name="radTipoconsulta" id="radConsXFecha" /></td>
              <td width="50%">Fecha Inicial&nbsp;&nbsp;<input type="text" name="txtFecIni" id="txtFecIni" class="box1" /> &nbsp;Fecha Final&nbsp;&nbsp;<input type="text" name="txtFecFin" id="txtFecFin" class="box1" /></td>
              <td width="316">&nbsp;</td>
            </tr>
            <tr>
            <td colspan="3" align="center" style="text-align: center; padding: 1em;">
            <input name="btnConsultar" type="button" class="ui-state-default" id="btnConsultar" value="Consultar" />
                <span class="Rojo"></span>
             </td>
            </tr>
          </table>
        </center>
      
      <td background="<?php echo URL_PORTAL; ?>imagenes/tabla/derecha.gif"></td><!-- FONDO DERECHA -->
      </center>
    <tr>
      <td height="41" background="<?php echo URL_PORTAL; ?>imagenes/tabla/abajo_izq2.gif">&nbsp;</td>
      <td background="<?php echo URL_PORTAL; ?>imagenes/tabla/abajo_central.gif" ></td>
      <td background="<?php echo URL_PORTAL; ?>imagenes/tabla/abajo_der.gif">&nbsp;</td>
    </tr>
    <tr>
  </table>
</div>

<div id="nominas" align="center">
	<h4>I-Ingreso. R-Retiro. VT-Varianci�n Transitoria de Salario. VP-Varianci�n Permanente de Salario. SC-Suspensi&oacute;n Temporal de Contrato.</h4>
    <h4>IE-Incapacidad por Enfermedad. LM-Maternidad. V-Vacaciones. IA-Incapacidad por Accidente.</h4>
          <table cellpadding="0" cellspacing="0"  border="0" class="sortable hover" id="tablaNominas">
	      <caption style="text-align:left; padding:5px; font-size:11px;"><span></span></caption>
		      <thead>
			      <tr>
			        <th class="head"><h3><strong>Periodo</strong></h3></th>
			        <th><h3><strong>Tipo Ident</strong></h3></th>
			        <th class="head"><h3><strong>Ident</strong></h3></th>
			        <th class="head"><h3><strong>Nombre</strong></h3></th>
			        <th class="head"><h3><strong>Horas</strong></h3></th>
			        <th class="head"><h3><strong>Dias</strong></h3></th>
			        <th class="head"><h3><strong>Sal.Basico</strong></h3></th>
			        <th class="head"><h3><strong>Ing.Base</strong></h3></th>
			        <th class="head"><h3><strong>I</strong></h3></th>
			        <th class="head"><h3><strong>R</strong></h3></th>
			        <th class="head"><h3><strong>VT</strong></h3></th>
			        <th class="head"><h3><strong>VP</strong></h3></th>
			        <th class="head"><h3><strong>SC</strong></h3></th>
			        <th class="head"><h3><strong>IE</strong></h3></th>
			        <th class="head"><h3><strong>LM</strong></h3></th>
			        <th class="head"><h3><strong>V</strong></h3></th>
			        <th class="head"><h3><strong>IA</strong></h3></th>
			        <th class="head"><h3><strong>Usuario</strong></h3></th>
			        <th class="head"><h3><strong>Fec.Pago</strong></h3></th>
			        <th class="head"><h3><strong>Fec.Sist</strong></h3></th>
			      </tr>
		      </thead>
	          <tbody>
	     
	          </tbody>
	  </table>
	  <div id="pager" class="pager">
   <div id="perpage">
	<select class="pagesize">
			<option selected="selected"  value="5">5</option>
			<option value="20">20</option>
			<option value="30">30</option>
			<option  value="40">40</option>
		</select>
        <label>Registros Por P&aacute;gina</label>
        </div>
        <div id="navigation">
		<img src="../../imagenes/imagesSorter/first.gif" class="first"/>
		<img src="../../imagenes/imagesSorter/previous.gif" class="prev"/>
        <img src="../../imagenes/imagesSorter/next.gif" class="next"/>
		<img src="../../imagenes/imagesSorter/last.gif" class="last"/> 
        <label>P&aacute;gina</label>
		<input type="text" class="pagedisplay boxfecha" readonly="readonly" />
       </div>
        
	</div>
         </div>

<div id="icon"><span></span></div>

  <!-- div para la imagen ampliada -->
  <div id="imagen" title="DOCUMENTOS">

  </div>
 
 <div id="tabs" style="display:none;">
 <ul>
	<li><a href='#tabs-1' alt='a'>Afiliado</a></li>
    <li><a href='#tabs-2' alt='b'>Persona</a></li>
    <li><a href='#tabs-3' alt='c'>Reclamos</a></li>
    <li><a href='#tabs-4' alt='d'>Descuentos</a></li>
    <li><a href='#tabs-5' alt='e'>Aportes</a></li>
	<li><a href='#tabs-6' alt='f'>Planillas</a></li>
	<li><a href='#tabs-7' alt='g'>Subsidio</a></li>
	<li><a href='#tabs-8' alt='h'>Cargues</a></li>
	<li><a href='#tabs-9' alt='i'>Movimientos</a></li>
	<li><a href='#tabs-10' alt='j'>Causales</a></li>
    <li><a href='#tabs-11' alt='k'>Documentos</a></li>
    <li><a href='#tabs-12' alt='l'>C&oacute;nyuge</a></li>
    </ul>
	
  <div id="tabs-1"></div>
  <div id="tabs-2"></div>
  <div id="tabs-3"></div>
  <div id="tabs-4"></div>
  <div id="tabs-5"></div>
  <div id="tabs-6"></div>
  <div id="tabs-7"></div>
  <div id="tabs-8"></div>
  <div id="tabs-9"></div>
  <div id="tabs-10"></div>
  <div id="tabs-11"></div>
  <div id="tabs-12"></div>
 </div>

<!-- ayuda en linea -->
<div id="ayuda" title="Manual .:. Consulta Trabajadores" style="background-image:url(<?php echo URL_PORTAL; ?>imagenes/FondoGeneral0.png)"></div>
<!-- fin ayuda en linea -->

<!-- colaboracion en linea -->
<div id="div-observaciones-tab"></div>
<div id="dialog-form2" title="Colaboraci&oacute;n en l&iacute;nea" style="display:none">
<p>Por favor diligencie este formulario para enviar comentarios, errores o falencias encontradas en el proceso. M&aacute;ximo 250 caracteres </p>
<label>Tus comentarios:</label><br />
<textarea name="notas" id="notas" cols="60", rows="10"></textarea>
</div>

<!-- fin colaboracion -->

</body>
<script>
$("#idT").focus();
</script>
</html>