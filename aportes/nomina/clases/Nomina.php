<?php
date_default_timezone_set('America/Bogota');
ini_set("display_errors","1");
if ( !isset( $_SESSION ['USUARIO'] ) ) {
	session_start();
	$raiz=$_SESSION['RAIZ'];
	include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'session.php';
	session();
	}
else{
	//header("Location: http://10.10.1.121/sigas/error.html");
//exit();
}
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'conexion.class.php';
include_once $raiz.DIRECTORY_SEPARATOR.'aportes'.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR. 'definiciones.class.php';
include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'p.persona.class.php';
include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'p.afiliacion.class.php';
include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'Logger.php';
include_once $raiz.DIRECTORY_SEPARATOR. 'phpComunes'.DIRECTORY_SEPARATOR.'wssdk'.DIRECTORY_SEPARATOR.'ClientWSInfWeb.php';
include_once $raiz.DIRECTORY_SEPARATOR. 'phpComunes'.DIRECTORY_SEPARATOR.'wssdk'.DIRECTORY_SEPARATOR.'Tercero.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';

/**
 *
 * Procesamiento de las nominas de los trabajadores que son descargadas de asocajas
 *
 * @author Jose Luis Rojas
 * @version 1.0.0
 * @since 25 Agosto 2010
 *
 * @TODO CERTIFICADOS DE PAGOS DE LAS EMPRESAS
 * @TODO CARTAS PARA SER ENVIADAS A LOS EMPLEADORES CON LA RELACION DE TRABAJADORES QUE NO SE ENCUENTRAN AFILIADOS A COMFAMILIAR
 */
class Nomina{

	//const LOG = '/var/www/html/logs/subsidio/nomina';
	const DS = DIRECTORY_SEPARATOR;
	const E_GEN = '..\..\..\planos_generados\nominas\errores';
	const I_GEN = '..\..\..\planos_generados\nominas\informes';

	private $usuario = null;
	private $con = null;
	private $def = null;
	private $per = null;
	private $afi = null;
	private $log = null;
	private $tipoPlanilla = null;
	private $idArchivoPU = null;
	private $nombreArchivoPU = null;
	private $datCabezera = array();
	private $datFinal = array();
	private $logFiles = array();
	private $ws = null;
	private $db=null;
	/*private $idEmpresaSuc = null;*/

	function __construct(){
		set_time_limit(0);
		$this->con = new DBManager();
		$this->con->conectar();
		$this->conInf = new DBManager();
		//Se debe cambiar al momento de entrar a produccion
		//$this->conInf->conectarInformaPrueba();
		$this->def = new Definiciones();
		$this->per = new Persona();
		$this->afi = new Afiliacion();
		$this->log = new Logger(3);
		//configuracion de tabla para logeo de observaciones
		$conf = array('tabla'=>'aportes096', 'campos'=>array('idarchivopu', 'idtipodocumentoemp', 'nit', 'idempresa', 'planilla', 'idtipodocumentra', 'identificacion', 'idpersona', 'papellido', 'sapellido', 'pnombre', 'snombre', 'observacion', 'fechaproc', 'periodo'));
		$this->log->confBD($conf);
		//$this->log->configuracion('/var/www/logs', );
		$this->usuario = $_SESSION['USUARIO'];
		$this->ws = new ClientWSInfWeb('c523d9f153587fbe3c96fee41c26b471', '711b60b69a49aa33bfa888930b2ecf10');
		$this->db = IFXDbManejador::conectarDB();
		if($this->db->conexionID==null){
			$cadena = $this->db->error;
			echo msg_error($cadena);
			exit();
		}
	}

	/**
	 * Procesa las nominas que se encuentren sin procesar en la base de datos
	 *
	 * @param array $nominas Id del archivo de la nomina a procesar
	 * @return Boolean Retorna verdadero si el proceso se realiza de forma correcta, Falso si se presenta error
	 */
	public function procesarArchivos($nominas = array()){
		//@TODO Integracion con los logs!!!!!!! Logs base de datos????????
		if(count($nominas) > 0){
			foreach($nominas as $idnom){
				if(is_numeric($idnom)){
					$datArchivo = $this->buscarNominas($idnom);
					if($datArchivo != false){
						$tipoNom = $this->identificaTipoNomina($datArchivo['nombrearchivo']);
						$this->nombreArchivoPU = $datArchivo['nombrearchivo'];

						//****logs*****************
						$archDatEmp = 'log_Datos_Empresas_' . $this->nombreArchivoPU . '.txt';
						$archErrArcPU = 'log_Errores_Cabezera_' . $this->nombreArchivoPU . '.txt';
						$archErrTra = 'log_Errores_Trabajadores_' . $this->nombreArchivoPU . '.txt';
						$archInfAfi = 'log_Informe_Afiliaciones_' . $this->nombreArchivoPU . '.txt';
						//$archActualDatEmp = 'log_ActalizaDatos_Empresas_'.$this->nombreArchivoPU.'txt';
						//$this->log->confArchivosMultiples(array('I_REP'=>array(self::I_GEN,$archNoReplegal)));


						$this->log->confArchivosMultiples(array('I_DATEMP'=>array(self::I_GEN, $archDatEmp), 'E_CAB'=>array(self::I_GEN, $archErrArcPU), 'E_TRA'=>array(self::I_GEN, $archErrTra), 'I_AFI'=>array(self::I_GEN, $archInfAfi)));
						//*************************


						$archivo = $datArchivo['ruta'] . DIRECTORY_SEPARATOR . $datArchivo['nombrearchivo'];
						$this->idArchivoPU = $datArchivo['idarchivopu'];
						echo '<strong>' . $datArchivo['nombrearchivo'] . '</strong><br/>';
						$fh = fopen($archivo, 'r');
						switch($tipoNom){
							case 'A':
								$this->procesoArchivoTipoA($fh);
								break;
							case 'I':
								$this->procesoArchivoTipoI($fh);
								break;
							case 'IP':
								break;
							default:
								return false;
								break;
						}
						fclose($fh);
					}
				}
			}
		}else{
			//Procesamiento automatico de nominas
		}
	}

	/**
	 * Busca las nominas dentro del sistema operativo y las pasa dentro de un array
	 *
	 * @param string $idnomina Identificador del archivo de nomina a montar
	 * @return array $nominasAprocesar
	 */
	private function buscarNominas($idnomina = null){
		if($idnomina != null){
			//logging**************
			$archErrores = 'log_idarchivo_' . $idnomina . '_errores.txt';
			$this->log->confArchivosMultiples(array('E_GEN'=>array(self::E_GEN, $archErrores)));
			//*********************


			$sql = "SELECT COUNT(idarchivopu) AS cant FROM aportes030 WHERE idarchivopu=$idnomina AND procesado='N' AND tipoproceso='NOM'";
			$r = mssql_query($sql, $this->con->conect);
			$rcant = mssql_fetch_array($r);
			mssql_free_result($r);
			if(intval($rcant['cant']) == 1){
				$sql = "SELECT idarchivopu, ruta, nombrearchivo FROM aportes030 WHERE idarchivopu=$idnomina";
				$r = mssql_query($sql, $this->con->conect);
				$rArch = mssql_fetch_array($r);
				mssql_free_result($r);
				$rArch = array_map('trim', $rArch);
				$archivo = $rArch['ruta'] . DIRECTORY_SEPARATOR . $rArch['nombrearchivo'];
				if(is_file($archivo)){
					return $rArch;
				}else{
					//@TODO Log No existe el archivo en el sistema operativo
					$this->log->logSimpleMultiple('E_GEN', "No existe el archivo $rArch[nombrearchivo] con id $idnomina en el sistema operativo");
					return false;
				}
			}else{
				//No existe archivo en la base de datos por ese idarchivo
				$this->log->logSimpleMultiple('E_GEN', "No existe el archivo de id $idnomina en la base de datos");
				return false;
			}
		}else{
			//@TODO Busqueda de nominas por montar (Automatica)
			$arrayArch = array();
			$sql = "SELECT ruta, nombrearchivo FROM aportes030 WHERE procesado='N' AND tipoproceso='NOM'";
			$r = mssql_query($sql, $this->con->conect);
			while($rArch = mssql_fetch_array($r)){
				$rArch = array_map('trim', $rArch);
				$archivo = $rArch['ruta'] . DIRECTORY_SEPARATOR . $rArch['nombrearchivo'];
				if(is_file($archivo)){
					$arrayArch[] = $rArch;
				}
			}
			mssql_free_result($r);
			return (count($arrayArch) > 0)?$arrayArch:false;
		}
	}

	/**
	 *
	 * Identifica el tipo de nomina (A,I IP) de acuerdo al nombre dado
	 *
	 * @param string $nombreArchivo Nombre del archivo
	 * @param string $tipoArchivo Retorna el identificador de la nomina
	 */
	private function identificaTipoNomina($nombreArchivo){
		$tipo = false;
		$posEnct = stripos($nombreArchivo, "_A");
		if($posEnct !== FALSE){
			//Archivo tipo A
			$tipo = 'A';
		}

		/*$posEnct = stripos($nombreArchivo, "_IP");
		if($posEnct !== FALSE){
			//Archivo tipo A
			$tipo = 'IP';
		}*/

		$posEnct = stripos($nombreArchivo, "_I");
		if($posEnct !== FALSE){
			//Archivo tipo A
			$tipo = 'I';
		}

		return $tipo;
	}

	/**
	 * Procesa los archivos Tipo A
	 *
	 * En estos archivos se encuentra informacion actualizada de las empresas
	 * Se procesa linea a linea el archivo para sacar los datos de acuerdo a la estructura
	 * dicha por la ley
	 *
	 * @param resource $fh Manejador del archivo
	 */
	private function procesoArchivoTipoA($fh){
		echo $fh;
		if(is_resource($fh)){
			if(! feof($fh)){
				$ln = fgets($fh);

				$bndApo = false;
				$bndInf = false;

				$datEmp = array();
				$datPer = array();

				$datEmp['razsoc'] = trim(substr($ln, 0, 200));
				$datEmp['digito'] = trim(substr($ln, 218, 1));
				$datEmp['nit'] = trim(substr($ln, 202, 16));
				$datEmp['tipoiden'] = trim(substr($ln, 200, 2));
				$datEmp['tipoidenInf'] = trim(substr($ln, 200, 2));
				$rTipoid = @mssql_fetch_array($this->def->mostrar_registroXcodigo($datEmp['tipoiden'], 1));
				$datEmp['tipoiden'] = (is_array($rTipoid))?$rTipoid['iddetalledef']:'0';

				$datPer['identificacion'] = intval(substr($ln, 402, 16));
				$datPer['tipoid'] = trim(substr($ln, 419, 2));
				$rTipoid = @mssql_fetch_array($this->def->mostrar_registroXcodigo($datPer['tipoid'], 1));
				$datPer['tipoid'] = (is_array($rTipoid))?$rTipoid['iddetalledef']:'0';

				$datPer['papellido'] = trim(substr($ln, 421, 20));
				$datPer['sapellido'] = trim(substr($ln, 441, 30));
				$datPer['pnombre'] = trim(substr($ln, 471, 20));
				$datPer['snombre'] = trim(substr($ln, 491, 30));

				//Crear persona en aportes015 para el representante legal
				$sql = "SELECT idpersona FROM aportes015 WHERE identificacion='$datPer[identificacion]' AND idtipodocumento=$datPer[tipoid]";
				$r = mssql_query($sql, $this->con->conect);
				$rowIdPer = @mssql_fetch_array($r);
				if(! is_array($rowIdPer)){
					//No existe el representante legal en aportes015
					$idReplegal = $this->crearRepLegal($datPer);
					if($idReplegal != false){
						//@todo Log de representante legal creado en aportes015
						$msj = "Se creo el rep. legal de la empresa $datEmp[nit] con ID $idReplegal";
						$this->log->logSimpleMultiple('I_DATEMP', $msj);
						echo $msj . '<br/>';
					}else{
						//@todo Log NO se pudo crear representante... se aborta continuar con la carga del archivo????????
						$msj = "NO Se creo el rep. legal de la empresa $datEmp[nit] con numero de identificacion $datPer[identificacion]";
						$this->log->logSimpleMultiple('I_DATEMP', $msj);
						echo $msj . '<br/>';
						return false;
					}
				}else{
					//Existe el representante legal, se obtiene el idpersona
					$idReplegal = $rowIdPer['idpersona'];
					mssql_free_result($r);
				}

				/*Creacion de la empresa o actualizacion de datos si ya existe*/
				$sql = "SELECT COUNT(idempresa) AS cuenta FROM aportes048 WHERE nit='$datEmp[nit]' AND principal='S'";
				$r = mssql_query($sql, $this->con->conect);
				$rCuenta = mssql_fetch_array($r);
				$cuenta = intval($rCuenta['cuenta']);
				mssql_free_result($r);

				$datEmp['idReplegal'] = $idReplegal;
				$datEmp['direccion'] = trim(substr($ln, 273, 40));
				$datEmp['codDept'] = trim(substr($ln, 316, 2));
				$datEmp['codCiud'] = trim(substr($ln, 316, 2)) . trim(substr($ln, 313, 3));

				//$telFax = preg_split('[\\h]', trim(substr($ln, 322, 19)), - 1, PREG_SPLIT_NO_EMPTY);
				$datEmp['telefono'] = trim(substr($ln, 322, 10));
				$datEmp['fax'] = trim(substr($ln, 332, 10));
				$datEmp['email'] = trim(substr($ln, 342, 60));

				$datEmp['actDane'] = trim(substr($ln, 318, 4));
				$rDane = @mssql_fetch_array($this->def->mostrar_registroXcodigo($datEmp['actDane'], 30));
				$datEmp['actDane'] = (is_array($rDane))?$rDane['iddetalledef']:'0';

				$datEmp['tipoentidad'] = trim(substr($ln, 270, 1));
				$rJuri = @mssql_fetch_array($this->def->mostrar_registroXcodigo($datEmp['tipoentidad'], 31));
				$datEmp['tipoentidad'] = (is_array($rJuri))?$rJuri['iddetalledef']:'0';

				$datEmp['tipopersona'] = trim(substr($ln, 271, 1));
				$rPers = @mssql_fetch_array($this->def->mostrar_registroXcodigo($datEmp['tipopersona'], 32));
				$datEmp['tipopersona'] = (is_array($rPers))?$rPers['iddetalledef']:'0';

				$datEmp['claseaportante'] = trim(substr($ln, 269, 1));
				$rClasA = @mssql_fetch_array($this->def->mostrar_registroXcodigo($datEmp['claseaportante'], 33));
				$datEmp['claseaportante'] = (is_array($rClasA))?$rClasA['iddetalledef']:'0';

				$datEmp['tipoaportante'] = trim(substr($ln, 551, 1));
				$rTipoA = @mssql_fetch_array($this->def->mostrar_registroXcodigo($datEmp['tipoaportante'], 34));
				$datEmp['tipoaportante'] = (is_array($rTipoA))?$rTipoA['iddetalledef']:'0';

				$datEmp['deptprincipal'] = trim(substr($ln, 572, 2));
				$fm = trim(substr($ln, 562, 571));

				$datEmp['fecmatricula'] = (strlen($fm) > 0)?substr($ln, 567, 2) . '/' . substr($ln, 570, 2) . '/' . substr($ln, 562, 4):'';


				if($cuenta == 1){
					//Existe la empresa como principal, se actualizan algunos datos en aportes048
					$sql = "UPDATE aportes048 SET direccion='$datEmp[direccion]', iddepartamento='".$datEmp[codDept]."', idciudad='".$datEmp[codCiud]."', idzona='$datEmp[codCiud]', telefono='$datEmp[telefono]', fax='$datEmp[fax]', email='$datEmp[email]', idrepresentante=$datEmp[idReplegal], actieconomicadane='$datEmp[actDane]', fechamatricula='$datEmp[fecmatricula]', tipopersona=$datEmp[tipopersona], claseaportante=$datEmp[claseaportante], tipoaportante=$datEmp[tipoaportante], idtipodocumento=$datEmp[tipoiden] WHERE nit='$datEmp[nit]' AND principal='S'";
					$r = mssql_query($sql, $this->con->conect);
					if($r){
						//@mssql_free_result($r);
						$msj = "Se actualizo los datos de la empresa $datEmp[nit]";
						$this->log->logSimpleMultiple('I_DATEMP', $msj);
						echo $msj . '<br/>';
						//$sql = "UPDATE aportes030 SET procesado='S', fechaproceso=TODAY, usuarioproceso='$this->usuario' WHERE idarchivopu=$this->idArchivoPU AND tipoproceso='NOM'";
						//$r = ifx_query($sql, $this->con->conect);
						//@mssql_free_result($r);
						$bndApo = true;
					}else{
						$msj = "NO Se actualizo los datos de la empresa $datEmp[nit]";
						$this->log->logSimpleMultiple('I_DATEMP', $msj);
						//$sql = "UPDATE aportes030 SET procesado='S', fechaproceso=TODAY, usuarioproceso='$this->usuario' WHERE idarchivopu=$this->idArchivoPU AND tipoproceso='NOM'";
						//$r = ifx_query($sql, $this->con->conect);
						echo $msj . '<br/>';

		//return false;
					}
				}elseif($cuenta == 0){
					//No existe la empresa, se crea en aportes048 COMO PRINCIPAL, SUCURSAL 000 Y LEGALIZADA N


					/**
					 * Segun la reunion que se llevo a cabo el dia 8 de agosto de 2011 en T.I, donde estuvieron presente
					 * Rocio Arevalo - Coordinadora Desarrollo financiero
					 * Claudia Cardozo - Tesorera
					 * Orlando Puentes - Ing. Soporte
					 * Jazmin Ospina - Coordinadora Aportes y subsidio
					 * Jose Luis Rojas - Ing. Soporte
					 *
					 * Se tomo la decision de realizar la creacion de la empresa en el aplicativo informaweb
					 * por consiguiente se modifica el codigo a continuacion
					 *
					 *
					 *
					 */

					$idemp = $this->crearEmpresa($datEmp);
					if($idemp != false){
						//@todo Log de empresa creada en aportes048
						$msj = "Se CREO la empresa con nit $datEmp[nit] afiliacion no legalizada";
						$this->log->logSimpleMultiple('I_DATEMP', $msj);
						echo $msj . '<br/>';
						//$sql = "UPDATE aportes030 SET procesado='S', fechaproceso=TODAY, usuarioproceso='$this->usuario' WHERE idarchivopu=$this->idArchivoPU AND tipoproceso='NOM'";
						//$r = ifx_query($sql, $this->con->conect);
						$bndApo = true;
					}else{
						//@todo Log NO se pudo crear empresa... se aborta continuar con la carga del archivo????????
						$msj = "NO Se CREO la empresa con nit $datEmp[nit] afiliacion no legalizada";
						$this->log->logSimpleMultiple('I_DATEMP', $msj);
						echo $msj . '<br/>';

		//$sql = "UPDATE aportes030 SET procesado='S', fechaproceso=TODAY, usuarioproceso='$this->usuario' WHERE idarchivopu=$this->idArchivoPU AND tipoproceso='NOM'";
					//$r = ifx_query($sql, $this->con->conect);
					//return false;
					}
				}else{
					$msj = "Existen dos empresas creadas con el nit $datEmp[nit] y bandera principal";
					$this->log->logSimpleMultiple('I_DATEMP', $msj);
					echo $msj . '<br/>';

		//return false;
				}
				//--------------------------------------------------------------------------------------------------------------------------
				//Verificacion/Creacion del nit en informaweb

				$exsInfWeb = $this->ws->terceroExiste(intval($datEmp['nit']));

				if($exsInfWeb == false){
					//Se crea la empresa en informaweb
					$this->tercero = new Tercero($datEmp['nit'], $datEmp['razsoc'], null, $_SESSION ['USUARIO'], null, null, 0, null, $datEmp['codCiud'], $datEmp['claseaportante'], null, null, 0, 0, 0, $datEmp['codDept'], $datEmp['direccion'], null, $datEmp['fax'], null, null, 0, null, 0, null, null, null, $datEmp['telefono'], 0, 0, null, null, null);
					$idinf =$this->ws->terceroGrabar($this->tercero);
					//$idinf = $this->crearEmpresaInformaweb($datEmp);
					if($idinf != false){
						//@todo Log de empresa creada en aportes048
						$msj = "Se CREO la empresa con nit $datEmp[nit] en INFORMAWEB";
						$this->log->logSimpleMultiple('I_DATEMP', $msj);
						echo $msj . '<br/>';
						//$sql = "UPDATE aportes030 SET procesado='S', fechaproceso=TODAY, usuarioproceso='$this->usuario' WHERE idarchivopu=$this->idArchivoPU AND tipoproceso='NOM'";
						//$r = ifx_query($sql, $this->con->conect);
						$bndInf = true;
					}else{
						//@todo Log NO se pudo crear empresa... se aborta continuar con la carga del archivo????????
						$msj = "NO Se CREO la empresa con nit $datEmp[nit] en INFORMAWEB";
						$this->log->logSimpleMultiple('I_DATEMP', $msj);
						echo $msj . '<br/>';

		//$sql = "UPDATE aportes030 SET procesado='S', fechaproceso=TODAY, usuarioproceso='$this->usuario' WHERE idarchivopu=$this->idArchivoPU AND tipoproceso='NOM'";
					//$r = ifx_query($sql, $this->con->conect);
					//return false;
					}
				}else{
					$bndInf = true;
				}

				$sql = "UPDATE aportes030 SET procesado='S', fechaproceso=CAST(GETDATE() AS DATE), usuarioproceso='$this->usuario' WHERE idarchivopu=$this->idArchivoPU AND tipoproceso='NOM'";
				$r = mssql_query($sql, $this->con->conect);

				if($bndApo == false || $bndInf == false){
					return false;
				}else{
					return true;
				}
			}
		}
	}

	/**
	 * Crea representante legal en aportes015, y devuelve el idpersona del representante legal creado
	 * en caso de error retorna falso
	 *
	 * @param array $datPer Array que contiene los datos personales del representante legal
	 * @return string $idRep Idpersona del representante legal
	 */
	private function crearRepLegal($datPer){
		$sql = "INSERT INTO aportes015(identificacion, idtipodocumento, papellido, sapellido, pnombre, snombre) VALUES('$datPer[identificacion]', $datPer[tipoid], '$datPer[papellido]', '$datPer[sapellido]', '$datPer[pnombre]', '$datPer[snombre]')";
		/*$r = mssql_query($sql, $this->con->conect);
		if($r){
			$row = sql_getsqlca($r);
			$idRep = $row['sqlerrd1'];
			return $idRep;
		}else{
			return false;
		}*/
		$rs=$this->db->queryInsert($sql,'aportes015');
		$error=$this->db->error;
		if(is_array($error)){
			return false;
		}
		return $rs;
	}

	/**
	 * Busca el id de la persona por la que esta pagando por el empleador
	 *
	 * @param integer $tpid
	 * @param string $id
	 */
	private function buscarPersona($dat = array()){
		$r = $this->per->mostrar_registro2($dat['tipoid'], $dat['cedtra']);
		$rwPer = @mssql_fetch_array($r);
		if(is_array($rwPer)){
			return $rwPer['idpersona'];
		}else{
			$cmp[0] = $dat['cedtra'];
			$cmp[1] = $dat['tipoid'];
			$cmp[2] = $dat['priape'];
			$cmp[3] = $dat['segape'];
			$cmp[4] = $dat['nombre'];
			$cmp[5] = $dat['nombre2'];
			$cmp[6] = 'PROC_NOMINA';
			$sql="INSERT INTO aportes015 (identificacion,idtipodocumento,papellido,sapellido,pnombre,snombre,usuario,fechasistema) VALUES ('".$cmp[0]."',".$cmp[1].",'".$cmp[2]."','".$cmp[3]."','".$cmp[4]."','".$cmp[5]."','".$cmp[6]."',cast(getdate() as date))";
			/*$r = $this->per->insertSimple($cmp);
			if($r){
				$rw = sql_getsqlca($r);
				$idpersona = $rw['sqlerrd1'];
				return $idpersona;
			}else{
				//@todo LOG no se pudo crear persona
				return false;
			}*/
			$rs=$this->db->queryInsert($sql,'aportes015');
			$error=$this->db->error;
			if(is_array($error)){
				return false;
			}
			return $rs;
		}
	}

	private function crearEmpresa($datEmp){
		$sql = "INSERT INTO aportes048(idtipodocumento, nit, digito, nitrsn, codigosucursal, principal, razonsocial, direccion, iddepartamento, idciudad, idzona, telefono, fax, email, idrepresentante, idjefepersonal, contratista, estado, fechaaportes, actieconomicadane, fechamatricula, tipopersona, claseaportante, tipoaportante, usuario, fechasistema, legalizada) VALUES($datEmp[tipoiden],'$datEmp[nit]', '$datEmp[digito]', '$datEmp[nit]', '000', 'S', '$datEmp[razsoc]','$datEmp[direccion]','".$datEmp[codDept]."','".$datEmp[codCiud]."','$datEmp[codCiud]','$datEmp[telefono]','$datEmp[fax]','$datEmp[email]',$datEmp[idReplegal],$datEmp[idReplegal],'N','P',CAST(GETDATE() AS DATE),$datEmp[actDane], '$datEmp[fecMatricula]',$datEmp[tipopersona],$datEmp[claseaportante],$datEmp[tipoaportante],'$this->usuario',CAST(GETDATE() AS DATE),'N')";
		/*$r = mssql_query($sql, $this->con->conect);
		if($r){
			$row = sql_getsqlca($r);
			$idEmp = $row['sqlerrd1'];
			return $idEmp;
		}else{
			return false;
		}*/
		$rs=$this->db->queryInsert($sql,'aportes048');
		$error=$this->db->error;
		if(is_array($error)){
			return false;
		}
		return $rs;
	}

	private function crearEmpresaInformaweb($datEmp){
		//FUNCION SIN USO, EL PROCESO DE GUARDADO DE LA EMPRESA LO REALIZA EL WEB SERVICE ClienteWSInfWeb.php
		/*switch($datEmp['tipoidenInf']){
			case 'NI':
				$datEmp['tipoidenInf'] = 'N';
				break;
			case 'CC':
				$datEmp['tipoidenInf'] = 'C';
				break;
			case 'CE':
				$datEmp['tipoidenInf'] = 'E';
				break;
			case 'TI':
				$datEmp['tipoidenInf'] = 'T';
				break;
			default:
				//@todo Por definir tipo de documento para registro civil y pasaporte
				break;
		}

		$sql = "INSERT INTO nits(nit, clase, nombre, direccion, telefono, ciudad, tipoclie, aaereo, fax, depto, pais, estado, t_contrib, grabador, fecha_crea)";
		$sql .= " VALUES('$datEmp[nit]',   '$datEmp[tipoidenInf]', '$datEmp[razsoc]','$datEmp[direccion]', '$datEmp[telefono]',  '$datEmp[codCiud]', 'N', '$datEmp[email]', '$datEmp[fax]',  '$datEmp[codDept]', '169', 'A', '', '$this->usuario', CAST(GETDATE() AS DATE))";
		$r = mssql_query($sql, $this->conInf->conect);
		if($r){
			$row = sql_getsqlca($r);
			$idEmp = $row['sqlerrd1'];
			return $idEmp;
		}else{
			return false;
		}*/
	}

	/**
	 * Procesa los archivos Tipo I
	 *
	 * En estos archivos se encuentra informacion detallada de la nomina de cada trabajador de las empresas
	 *
	 * @param resource $fh Manejador del archivo
	 */
	private function procesoArchivoTipoI($fh){
		if(is_resource($fh)){
			$this->idEmpresaSuc = null;
			while(! feof($fh)){
				$ln = fgets($fh);

				$tipoRegistro = intval(substr($ln, 5, 1));

				switch($tipoRegistro){
					case 1:
						$this->registrosCabezeraTipoI($ln);
						break;
					case 2:
						$this->registrosDetalleTipoI($ln);
						break;
					case 3:
						$this->registrosFinalTipoI($ln);
						break;
					default:
						return false;
						break;
				}
			}
		}
	}

	/**
	 * Identifica el numero el tipo de planilla a procesar
	 * y retorna el nombre de la funcion que se encarga de procesarla
	 *
	 * @param string $pla
	 */
	private function identificaTipoPlanilla($pla){
		switch($pla){
			//Planilla independiente empresas
			case 'Y':
			//Planilla empleados adicionales
			case 'A':
			//Planilla independiente
			case 'I':
			//Planilla empleados de independientes
			case 'S':
			//Planilla mora
			case 'M':
			//Planilla Normal de nomina
			case 'E':
				return 'plaNormalTipoE';
				break;
			//Planilla Correcciones
			case 'N':
				//return false;
				return 'plaCorrecionTipoN';
				break;
			default:
				return false;
				break;
		}
	}

	/**
	 *
	 * Proceso de linea de la cabecera de los archivos tipo I
	 *
	 * @param string $ln
	 */
	private function registrosCabezeraTipoI($ln){
		//@todo Retorna el tipo de planilla para q se procese en la siguiente linea del archivo... detalle del archivo dependiendo del tipo de planilla
		$this->tipoPlanilla = trim(substr($ln, 377, 1));
		$tipoid = trim(substr($ln, 225, 2));
		$rTipoid = @mssql_fetch_array($this->def->mostrar_registroXcodigo($tipoid, 1));
		$tipoid = (is_array($rTipoid))?$rTipoid['iddetalledef']:'0';
		$identi = trim(substr($ln, 227, 16));
		$periodo = trim(substr($ln, 370, 4)) . trim(substr($ln, 375, 2));

		$sql = "SELECT smlv FROM aportes012 WHERE periodo='$periodo'";
		$r = mssql_query($sql, $this->con->conect);
		$rwSmlv = mssql_fetch_array($r);
		mssql_free_result($r);

		$fecpago = substr($ln, 388, 4).'-'.substr($ln, 393, 2) . '-' . substr($ln, 396, 2);
		$planilla = trim(substr($ln, 408, 10));
		$numtotalempleados = intval(substr($ln, 469, 5));
		$codoperador = intval(substr($ln, 479, 2));
		$modPlanilla = intval(substr($ln, 481, 1));
		$mora = intval(substr($ln, 482, 4));
		$numempleadosplanilla = intval(substr($ln, 486, 8));

		$this->datCabezera['nit'] = $identi;
		$this->datCabezera['tipoId'] = $tipoid;
		$this->datCabezera['planilla'] = $planilla;
		$this->datCabezera['periodo'] = $periodo;
		$this->datCabezera['fecpago'] = $fecpago;
		$this->datCabezera['smlv'] = intval($rwSmlv['smlv']);

		//@todo verificar si ya fue cargada la cabezera del archivo
		$sql = "SELECT COUNT(idencabezadopu) AS cant FROM aportes031 WHERE idtipodocumento=$tipoid AND nit='$identi' AND periodopago='$periodo' AND tipodeplanilla='$this->tipoPlanilla'";
		$rCant = mssql_query($sql, $this->con->conect);
		$rwCant = mssql_fetch_array($rCant);
		mssql_free_result($rCant);
		if(intval($rwCant['cant']) == 0){
			$sql = "INSERT INTO aportes031(idtipodocumento,nit,periodopago,tipodeplanilla,fechadepago,numeroplanilla,numerototalempleados,codigooperador,modalidadplanilla,diasdemora,empleadosenplanilla,idarchivo,estado,fechasistema,usuario) VALUES($tipoid, '$identi', '$periodo', '$this->tipoPlanilla', '$fecpago', '$planilla', $numtotalempleados, $codoperador, $modPlanilla, $mora, $numempleadosplanilla, $this->idArchivoPU, 'A', CAST(GETDATE() AS DATE), '$this->usuario')";
			$r = mssql_query($sql, $this->con->conect);
			if(! $r){
				//@todo Error no se pudo grabar cabezera
				$msj = "No se pudo grabar la cabecera del archivo $this->nombreArchivoPU con id $this->idArchivoPU en la BD";
				$this->log->logSimpleMultiple('E_CAB', $msj);
				echo $msj . '<br/>';
			}
		}else{
			//@TODO ya se proceso el encabezado
			$msj = "Cabecera del archivo $this->nombreArchivoPU ya procesada anteriormente";
			$this->log->logSimpleMultiple('E_CAB', $msj);
			echo $msj . '<br/>';
		}
	}

	/**
	 *
	 * Proceso de linea del detalle de los archivos tipo I
	 *
	 * @param string $ln
	 */
	private function registrosDetalleTipoI($ln){
		$nit = $this->datCabezera['nit'];

		/*Datos registrados en la PU*/
		$dat['tipoid'] = trim(substr($ln, 6, 2));
		$rTipoid = @mssql_fetch_array($this->def->mostrar_registroXcodigo($dat['tipoid'], 1));
		$dat['tipoid'] = (is_array($rTipoid))?$rTipoid['iddetalledef']:'0';
		$dat['cedtra'] = intval(substr($ln, 8, 16));
		$dat['tipocotiza'] = trim(substr($ln, 24, 2));
		$dat['subtipocotizante'] = trim(substr($ln, 26, 2));
		$dat['deptLabor'] = trim(substr($ln, 30, 2));
		$dat['muniLabor'] = trim(substr($ln, 32, 3));
		$dat['priape'] = trim(substr($ln, 35, 19));
		$dat['segape'] = trim(substr($ln, 54, 30));
		$dat['nombre'] = trim(substr($ln, 84, 20));
		$dat['nombre2'] = trim(substr($ln, 105, 30));
		$dat['ingreso'] = trim(substr($ln, 135, 1));
		$dat['retiro'] = trim(substr($ln, 136, 1));
		$dat['var_per_sal'] = trim(substr($ln, 137, 1));
		$dat['var_tra_sal'] = trim(substr($ln, 138, 1));
		$dat['sus_tem_con'] = trim(substr($ln, 139, 1));
		$dat['inc_tem_emp'] = trim(substr($ln, 140, 1));
		$dat['lic_maternidad'] = trim(substr($ln, 141, 1));
		$dat['vacaciones'] = trim(substr($ln, 142, 1));
		$dat['inc_tem_acctra'] = intval(substr($ln, 143, 2));
		$dat['diasCotizados'] = intval(substr($ln, 145, 2));
		$dat['basico'] = intval(substr($ln, 147, 9));
		$dat['ingbase'] = intval(substr($ln, 156, 9));
		$dat['porceAporte'] = floatval(substr($ln, 165, 7));
		$dat['valapo'] = intval(substr($ln, 172, 9));
		$dat['variaciones'] = trim(substr($ln, 181, 1));
		$dat['salIntegral'] = trim(substr($ln, 182, 1));

		/**
		 * Modificacion Horas laborada en base a las banderas
		 * @since 08/05/2011
		 * @author Jose Luis Rojas
		 *
		 */

		$dat['horas'] = ($dat['inc_tem_emp'] == 'X' || $dat['lic_maternidad'] == 'X' || $dat['vacaciones'] == 'X' || $dat['inc_tem_acctra'] > 0)?240:$dat['diasCotizados']*8;
		$valhora = $this->datCabezera['smlv'] / 240;
		$valhoralab = floatval($dat['ingbase'] / $valhora);

		if($dat['ingbase'] < $this->datCabezera['smlv']){
			if($dat['sus_tem_con'] == 'X'){
				$dat['horas'] = $dat['diasCotizados'] * 8;
			}elseif($dat['ingreso'] == 'X' || $dat['retiro'] == 'X'){
				$dat['horas'] = ceil($valhoralab);
			}elseif($dat['inc_tem_emp'] == 'X' || $dat['lic_maternidad'] == 'X' || $dat['vacaciones'] == 'X' || $dat['inc_tem_acctra'] > 0){
				$dat['horas'] = 240;
			}else{
				$dat['horas'] = ceil($valhoralab);
			}
		}

		//$dat['basico'] = ($dat['basico'] < $this->datCabezera['smlv'] && ($dat['inc_tem_acctra'] > 0 || $dat['inc_tem_emp'] == 'X'))?$this->datCabezera['smlv']:$dat['basico'];


		$dat['idpersona'] = $this->buscarPersona($dat);

		if(! $dat['idpersona']){
			$camp = array($dat['tipoid'], $dat['cedtra'], $dat['priape'], $dat['segape'], $dat['nombre'], $dat['nombre2'], '', '', 'N', $this->usuario);
			//@todo BUG... doble creacion de persona
			$sql="Insert into aportes015 (idtipodocumento, identificacion, papellido, sapellido, pnombre, snombre, sexo, fechanacimiento, capacidadtrabajo,fechaafiliacion,estadoafiliacion,validado,fechasistema,usuario )
			values('".$camp[0]."','".$camp[1]."','".$camp[2]."','".$camp[3]."','".$camp[4]."','".$camp[5]."','".$camp[6]."','".$camp[7]."','".$camp[8]."',cast(getdate() as date),'A','N',cast(getdate() as date),'".$camp[9]."')";
			/*$r = $this->per->insert_completo($camp);
			$id = sql_getsqlca($r);*/
			$rs=$this->db->queryInsert($sql,'aportes015');
			$error=$this->db->error;
			if(is_array($error)){
				return false;
			}
			$id = $rs;
			$dat['idpersona'] = $id['sqlerrd1'];
			if(! is_numeric($dat['idpersona'])){
				//@todo generar error de no creacion de persona
				$msj = "No se pudo crear nueva persona $dat[cedtra] en la BD";
				$this->log->logSimpleMultiple('E_TRA', $msj);
				echo $msj . '<br/>';
				return false;
			}
		}
		//Se traen los datos del id de la empresa, sino existe la afiliacion se crea si tiene la bandera de ingreso
		$dat['idempresa'] = $this->verificacionAfiliacion($dat);
		if(is_numeric($dat['idempresa']) && $dat['idempresa'] != 0){
			/*Funcion deacuerdo al tipo de planilla a ingresar*/
			$fn = $this->identificaTipoPlanilla($this->tipoPlanilla);
			if($fn != false){
				$this->$fn($dat);
			}else{
				//@TODO ERROR EN LA FUNCION
			}
		}else{
			//@todo No se pudo crear la afiliacion
			$msj = "No se pudo crear la afiliacion para $dat[cedtra] no existe el nit $nit";
			$this->log->logSimpleMultiple('I_AFI', $msj);
			echo $msj . '<br/>';
			return false;
		}
	}

	/**
	 * Procesa el detalle de las planillas de tipo normal
	 */
	private function plaNormalTipoE($dat = array()){
		$nit = $this->datCabezera['nit'];
		//Se busca si ya se proceso el registro del trabajador

		//$sql = "SELECT COUNT(idplanilla) AS cuenta FROM aportes010 WHERE nit='" . $this->datCabezera['nit'] . "' AND tipodocumentoaportante='" . $this->datCabezera['tipoId'] . "' AND identificacion='$dat[cedtra]' AND tipodocumento='$dat[tipoid]' AND periodo='" . $this->datCabezera['periodo'] . "' AND horascotizadas='$dat[horas]' AND salariobasico='$dat[basico]' AND fechapago='".$this->datCabezera['fecpago']."'";
		$sql = "SELECT COUNT(idplanilla) AS cuenta FROM aportes010 WHERE nit='" . $this->datCabezera['nit'] . "' AND tipodocumentoaportante='" . $this->datCabezera['tipoId'] . "' AND identificacion='$dat[cedtra]' AND tipodocumento='$dat[tipoid]' AND periodo='" . $this->datCabezera['periodo'] . "' AND horascotizadas='$dat[horas]' AND salariobasico='$dat[basico]'";
		if(!isset($dat['horas'])){
			echo $sql.'</br>';
		}
		$r = mssql_query($sql, $this->con->conect);
		$rwC = mssql_fetch_array($r);
		mssql_free_result($r);
		if(intval($rwC['cuenta']) == 0){
			$sql = "INSERT INTO aportes010(idarchivo, planilla, tipodocumentoaportante, nit, tipodocumento, identificacion, tipocotizantepu, subtipocotizantepu, coddepartamentotrabaja, codmunicipiotrabaja, papellido, sapellido, pnombre, snombre, tarifaaporte, valoraporte, salariobasico, ingresobase, salariointegral, diascotizados, ingreso, retiro, var_tra_salario, var_per_salario, sus_tem_contrato, inc_tem_emfermedad, lic_maternidad, vacaciones, inc_tem_acc_trabajo, tipo_cotizante, periodo, correccion, idempresa, idtrabajador, horascotizadas, procesado, usuario, fechapago, fechasistema) ";
			$sql .= "VALUES($this->idArchivoPU, '" . $this->datCabezera['planilla'] . "','" . $this->datCabezera['tipoId'] . "','" . $this->datCabezera['nit'] . "','$dat[tipoid]','$dat[cedtra]','$dat[tipocotiza]','$dat[subtipocotizante]','$dat[deptLabor]','$dat[muniLabor]','$dat[priape]','$dat[segape]','$dat[nombre]','$dat[nombre2]','$dat[porceAporte]','$dat[valapo]','$dat[basico]','$dat[ingbase]','$dat[salIntegral]','$dat[diasCotizados]','$dat[ingreso]','$dat[retiro]','$dat[var_tra_sal]','$dat[var_per_sal]','$dat[sus_tem_con]','$dat[inc_tem_emp]','$dat[lic_maternidad]','$dat[vacaciones]','$dat[inc_tem_acctra]','1','" . $this->datCabezera['periodo'] . "','C','$dat[idempresa]','$dat[idpersona]','$dat[horas]','N','$this->usuario', '".$this->datCabezera['fecpago']."', CAST(GETDATE() AS DATE))";
			$r = mssql_query($sql, $this->con->conect);
			$msj = "Se proceso la nomina del trabajador $dat[cedtra] en la afiliacion con el nit $nit";
			$this->log->logSimpleMultiple('I_AFI', $msj);
			echo $msj . '<br/>';
		}else{
			//@TODO YA SE PROCESO LA NOMINA
			$msj = "La nomina del trabajador $dat[cedtra] en la afiliacion con el nit $nit ya se proceso";
			$this->log->logSimpleMultiple('I_AFI', $msj);
			echo $msj . '<br/>';
		}
	}

	/**
	 * Procesa el detalle de las planillas de tipo correccion
	 */
	private function plaCorrecionTipoN($dat = array()){

		$var = $dat['variaciones'];
		$nit = $this->datCabezera['nit'];

		if($var == 'A'){
			//Registro erroneo, se marca la nomina que ya se encuentra insertada como erronea
			//$sql = "SELECT idplanilla FROM aportes010 WHERE nit='" . $this->datCabezera['nit'] . "' AND tipodocumentoaportante='" . $this->datCabezera['tipoId'] . "' AND identificacion='$dat[cedtra]' AND tipodocumento='$dat[tipoid]' AND periodo='" . $this->datCabezera['periodo'] . "' AND correccion='C' AND fechapago='".$this->datCabezera['fecpago']."'";
			$sql = "SELECT idplanilla FROM aportes010 WHERE nit='" . $this->datCabezera['nit'] . "' AND tipodocumentoaportante='" . $this->datCabezera['tipoId'] . "' AND identificacion='$dat[cedtra]' AND tipodocumento='$dat[tipoid]' AND periodo='" . $this->datCabezera['periodo'] . "' AND correccion='C'";
			$r = mssql_query($sql, $this->con->conect);
			$rwC = mssql_fetch_array($r);
			mssql_free_result($r);

			if(intval($rwC['idplanilla']) > 0){
				$sql = "UPDATE aportes010 SET correccion='A' WHERE idplanilla = $rwC[idplanilla]";
				$r = mssql_query($sql, $this->con->conect);
				$msj = "Se marco la nomina del trabajador $dat[cedtra] como ERRONEA en el periodo " . $this->datCabezera['periodo'];
				//$this->log->logSimpleMultiple('I_AFI', $msj);
				echo $msj . '<br/>';
			}

		}elseif($var == 'C'){
			//Registro correcto, se inserta la nueva nomina y se marca como 'C'


			//Se busca si ya se proceso el registro del trabajador
			//$sql = "SELECT COUNT(idplanilla) AS cant FROM aportes010 WHERE nit='" . $this->datCabezera['nit'] . "' AND tipodocumentoaportante='" . $this->datCabezera['tipoId'] . "' AND identificacion='$dat[cedtra]' AND tipodocumento='$dat[tipoid]' AND periodo='" . $this->datCabezera['periodo'] . "' AND correccion='C' AND fechapago='".$this->datCabezera['fecpago']."'";
			$sql = "SELECT COUNT(idplanilla) AS cant FROM aportes010 WHERE nit='" . $this->datCabezera['nit'] . "' AND tipodocumentoaportante='" . $this->datCabezera['tipoId'] . "' AND identificacion='$dat[cedtra]' AND tipodocumento='$dat[tipoid]' AND periodo='" . $this->datCabezera['periodo'] . "' AND correccion='C'";
			$r = mssql_query($sql, $this->con->conect);
			$rwC = mssql_fetch_array($r);
			mssql_free_result($r);

			if(intval($rwC['cant']) == 0){
				$sql = "INSERT INTO aportes010(idarchivo, planilla, tipodocumentoaportante, nit, tipodocumento, identificacion, tipocotizantepu, subtipocotizantepu, coddepartamentotrabaja, codmunicipiotrabaja, papellido, sapellido, pnombre, snombre, tarifaaporte, valoraporte, salariobasico, ingresobase, salariointegral, diascotizados, ingreso, retiro, var_tra_salario, var_per_salario, sus_tem_contrato, inc_tem_emfermedad, lic_maternidad, vacaciones, inc_tem_acc_trabajo, tipo_cotizante, periodo, correccion, idempresa, idtrabajador, horascotizadas, procesado, usuario, fechapago, fechasistema) ";
				$sql .= "VALUES($this->idArchivoPU, '" . $this->datCabezera['planilla'] . "','" . $this->datCabezera['tipoId'] . "','" . $this->datCabezera['nit'] . "','$dat[tipoid]','$dat[cedtra]','$dat[tipocotiza]','$dat[subtipocotizante]','$dat[deptLabor]','$dat[muniLabor]','$dat[priape]','$dat[segape]','$dat[nombre]','$dat[nombre2]','$dat[porceAporte]','$dat[valapo]','$dat[basico]','$dat[ingbase]','$dat[salIntegral]','$dat[diasCotizados]','$dat[ingreso]','$dat[retiro]','$dat[var_tra_sal]','$dat[var_per_sal]','$dat[sus_tem_con]','$dat[inc_tem_emp]','$dat[lic_maternidad]','$dat[vacaciones]','$dat[inc_tem_acctra]','1','" . $this->datCabezera['periodo'] . "','C','$dat[idempresa]','$dat[idpersona]','$dat[horas]','N','$this->usuario', '".$this->datCabezera['fecpago']."', CAST(GETDATE() AS DATE))";

				$r = mssql_query($sql, $this->con->conect);
				$msj = "Se proceso la nomina del trabajador $dat[cedtra] en la afiliacion con el nit $nit como CORRECCION";
				//$this->log->logSimpleMultiple('I_AFI', $msj);
				echo $msj . '<br/>';

			}else{
				$msj = "La nomina del trabajador $dat[cedtra] en la afiliacion con el nit $nit como CORRECCION ya se proceso";
				//$this->log->logSimpleMultiple('I_AFI', $msj);
				echo $msj . '<br/>';
			}
		}
	}

	/**
	 *
	 * Proceso de linea final de los archivos tipo I
	 *
	 * @param string $ln
	 */
	private function registrosFinalTipoI($ln){

		$registro = intval(substr($ln, 0, 5));
		if($registro == 31){
			$this->datFinal['baseCo'] = intval(substr($ln, 6, 10));
			$this->datFinal['valor2'] = intval(substr($ln, 16, 10));
		}elseif($registro == 36){
			$this->datFinal['diasmora'] = intval(substr($ln, 6, 4));
			$this->datFinal['intmora'] = intval(substr($ln, 10, 10));
		}else{
			$nit = $this->datCabezera['nit'];
			$tipDocNit = $this->datCabezera['tipoId'];
			$this->datFinal['total'] = intval(substr($ln, 6, 10));

			$sql = "SELECT COUNT(*) AS cant FROM aportes032 WHERE nit='$nit' AND periodo='" . $this->datCabezera['periodo'] . "' AND totalaportes=" . $this->datFinal['total'];
			$r = mssql_query($sql, $this->con->conect);
			$rwCant = mssql_fetch_array($r);
			mssql_free_result($r);
			if(intval($rwCant['cant']) == 0){
				$sql = "INSERT INTO aportes032(idarchivo, planilla, idtipodocumento, nit, periodo, valor, diasmora, interesmora, totalaportes) ";
				$sql .= "VALUES($this->idArchivoPU, '" . $this->datCabezera['planilla'] . "','$tipDocNit','$nit','" . $this->datCabezera['periodo'] . "'," . $this->datFinal['valor2'] . "," . $this->datFinal['diasmora'] . "," . $this->datFinal['intmora'] . "," . $this->datFinal['total'] . ")";
				$r = mssql_query($sql, $this->con->conect);
				//@todo nomina procesada
				$msj = "Se proceso el final de la nomina del nit $nit";
				$this->log->logSimpleMultiple('I_AFI', $msj);
				echo $msj . '<br/>';
			}else{
				//@todo ya se proceso el final del archivo
				$msj = "El final de la nomina del nit $nit YA fue procesado";
				$this->log->logSimpleMultiple('I_AFI', $msj);
				echo $msj . '<br/>';
			}
			$sql = "UPDATE aportes030 SET procesado='S', fechaproceso=CAST(GETDATE() AS DATE), usuarioproceso='$this->usuario' WHERE idarchivopu=$this->idArchivoPU AND tipoproceso='NOM'";
			$r = mssql_query($sql, $this->con->conect);
		}
	}

	/**
	 * Funcion para verificar si el trabajador se encuentra afiliado con la empresa
	 * que lo esta reportando en nomina, si la afiliacion no existe y la bandera de ingreso
	 * se encuentra marcada, se crea automaticamente la afiliacion
	 *
	 * @param $dat
	 */
	private function verificacionAfiliacion($dat){
		$nit = $this->datCabezera['nit'];
		$tipDocNit = $this->datCabezera['tipoId'];
		$anoAfi = substr($this->datCabezera['periodo'], 0, 4);
		$mesAfi = substr($this->datCabezera['periodo'], 4, 2);

		//Se busca la afiliacion con la empresa
		$sql = "SELECT a16.idempresa, a16.idformulario, a16.fechaingreso FROM aportes048 a48 INNER JOIN aportes016 a16 ON a48.idempresa=a16.idempresa WHERE a48.nit='$nit' AND a16.idpersona={$dat['idpersona']}";
		$r = mssql_query($sql, $this->con->conect);
		$rwAfi = mssql_fetch_array($r);
		mssql_free_result($r);
		if($rwAfi != false && is_array($rwAfi)){
			$idformulario = intval($rwAfi['idformulario']);
			$idempresa = $rwAfi['idempresa'];
			$dat['fecIngreso'] = $rwAfi['fechaingreso'];
		}else{
			$idformulario = 0;
			$idempresa = 0;
			//@TODO NO existe la afiliacion con la empresa
			$mail = array($this->idArchivoPU, $tipDocNit, $nit, $idempresa, $this->datCabezera['planilla'], $dat['tipoid'], $dat['cedtra'], $dat['idpersona'], $dat['priape'], $dat['segape'], $dat['nombre'], $dat['nombre2'], 'EL TRABAJADOR NO SE ENCUENTRA RELACIONADO CON LA EMPRESA', date('m/d/Y'), $this->datCabezera['periodo']);
			$this->log->logSimpleBD($mail);
			$msj = "No existe afiliacion Activa para $dat[cedtra] con el nit $nit";
			$this->log->logSimpleMultiple('I_AFI', $msj);
			echo $msj . '<br/>';
		}
		//mssql_free_result($r);

		//No esta afiliado con la empresa que lo reporta, NO IMPORTA si tiene bandera de ingreso X, se crea la afiliacion
		if($idformulario == 0 || ($dat['ingreso'] == 'X' /*&& $dat['retiro'] == ''*/)){
			$datAfiliacion = $this->novedadIngreso($dat);
			if($datAfiliacion == false){
				//@TODO mostrar error, no se pudo crear la afiliacion
				return 0;
			}else{
				$idformulario = $datAfiliacion['idformulario'];
				$idempresa = $datAfiliacion['idempresa'];
				$dat['fecIngreso'] = $datAfiliacion['fecIngreso'];
			}
		}

		if($idformulario != 0){
			//se actualiza el salario basico en la tabla de afiliaciones
			$sql = "UPDATE aportes016 SET salario=$dat[basico] WHERE idformulario=$idformulario";
			$r = mssql_query($sql, $this->con->conect);
			if(! $r){
				//echo $sql;
				//@todo error no se pudo actualizar el salario de la afiliacion
				$msj = "NO se pudo actualizar el salario del trabajador $dat[cedtra] en la afiliacion con el nit $nit";
				$this->log->logSimpleMultiple('I_AFI', $msj);
				echo $msj . '<br/>';
			}else{
				//@TODO se actualizo el salario de afiliacion
				$msj = "Se actualizo el salario del trabajador $dat[cedtra] en la afiliacion con el nit $nit";
				$this->log->logSimpleMultiple('I_AFI', $msj);
				echo $msj . '<br/>';
			}
		}

		//viene registrada bandera de retiro
		if($idformulario != 0 && $dat['retiro'] == 'X' /*&& $dat['ingreso'] == ''*/){
			$this->novedadRetiro($dat, $idformulario);
		}elseif($idformulario == 0 && $dat['retiro'] == 'X'){
			//@TODO Error no se puede retirar de una afiliacion que no existe.
			return 0;
		}

		return $idempresa;
	}

	/**
	 * Realiza el ingreso de la afiliacion al sistema
	 *
	 * @param array $dat
	 * @return array o false si hay error
	 */
	private function novedadIngreso($dat){
		$nit = $this->datCabezera['nit'];
		$tipDocNit = $this->datCabezera['tipoId'];

		$anoAfi = substr($this->datCabezera['periodo'], 0, 4);
		$mesAfi = substr($this->datCabezera['periodo'], 4, 2);
		$datAfil = array();

		$fecIngresoUnix = mktime(0, 0, 0, $mesAfi, 1, $anoAfi);
		$datAfil['fecIngreso'] = date('m/d/Y', mktime(0, 0, 0, $mesAfi, 1, $anoAfi));

		$sql = "SELECT COUNT(idempresa) AS cant FROM aportes048 WHERE nit='$nit'";//AND idtipodocumento=$tipDocNit AND estado='A' AND legalizada='S'
		$r = mssql_query($sql, $this->con->conect);
		$rwCantEmp = mssql_fetch_array($r);
		mssql_free_result($r);

		if(intval($rwCantEmp['cant']) == 1){
			$sql = "SELECT idempresa FROM aportes048 WHERE nit='$nit' AND estado='A' AND legalizada='S'";
		}elseif(intval($rwCantEmp['cant']) > 1){
			$sql = "SELECT idempresa FROM aportes048 WHERE nit='$nit' AND estado='A' AND principal='S' AND legalizada='S'";
		}else{
			//@todo ERROR no se encuentra idempresa para ese nit, la empresa no existe
			return false;
		}

		//Se busca el idempresa para la afiliacion, se toma la sucursal principal como predeterminada
		//$sql = "SELECT idempresa FROM aportes048 WHERE nit='$nit' AND idtipodocumento=$tipDocNit AND estado='A' AND principal='S' AND legalizada='S'";
		$r = mssql_query($sql, $this->con->conect);
		$rwEmp = mssql_fetch_array($r);
		mssql_free_result($r);
		if(is_array($rwEmp)){

			//Verifica si ya tiene una afiliacion primaria activa, sino tiene afiliacion primaria activa se le crea una afiliacion primaria activa
			//@TODO falta verificar si se debe dejar como afiliacion primaria la de mayor remuneracion
			$sql = "SELECT COUNT(idformulario) AS cant FROM aportes016 WHERE idpersona=$dat[idpersona] AND idempresa=$rwEmp[idempresa]";
			$r = mssql_query($sql, $this->con->conect);
			if(! $r){
				//@todo ERROR no encontro el id de la empresa
				return false;
			}else{
				$rwCantAf = mssql_fetch_array($r);
				mssql_free_result($r);
			}

			$canAf = intval($rwCantAf['cant']);

			if($canAf < 1){
				//Verifica si ya tiene una afiliacion primaria activa, sino tiene afiliacion primaria activa se le crea una afiliacion primaria activa
				//@TODO falta verificar si se debe dejar como afiliacion primaria la de mayor remuneracion
				$sql = "SELECT COUNT(idformulario) AS cant FROM aportes016 WHERE idpersona=$dat[idpersona] AND estado='A' AND primaria='S'";
				$r = mssql_query($sql, $this->con->conect);
				$rwCant = mssql_fetch_array($r);
				mssql_free_result($r);
				$primaria = (intval($rwCant['cant']) == 0)?'S':'N';

				//Verificacion de ultima vez q trabajo en esa empresa (si trabajo), para que la fecha de la nueva afiliacion secundaria no se sobre ponga a la fecha de retiro
				$sql = "SELECT top 1 fechaingreso, fecharetiro FROM aportes017 WHERE idpersona=$dat[idpersona] AND idempresa='$rwEmp[idempresa]' AND estado='I' ORDER BY fecharetiro DESC";
				$r = mssql_query($sql, $this->con->conect);
				$rwTray = @mssql_fetch_array($r);
				mssql_free_result($r);
				if(is_array($rwTray)){
					$fecRet = explode('-', $rwTray['fecharetiro']);
					if(is_array($fecRet) && count($fecRet) == 3){
						$fecRet = mktime(0, 0, 0, $fecRet[0], $fecRet[1], $fecRet[2]);
						if($fecIngresoUnix <= $fecRet){
							//@TODO NO SE CREA LA AFILIACION, YA EXISTE EN LA TRAYECTORIA
							return false;
						}
					}
				}

				//no se encuentra afiliacion con esa empresa, se procede a crear la afiliacion con las banderas auditada y primaria en N; y estado P(endiente)
				//@TODO cambio en la base de datos en la tabla aportes016 el tipo de dato de estado debe ser integer y no char. para q reciba el codigo del estado
				if(! $r){
					//@todo ERROR no inserto
					return false;
				}else{
					$sql = "INSERT into aportes016 (tipoformulario, tipoafiliacion, idempresa, idpersona, fechaingreso,horasdia,horasmes,salario,traslado,codigocaja,tipopago,usuario,estado,fechasistema,auditado, primaria) VALUES('49', 18, $rwEmp[idempresa], $dat[idpersona], '$datAfil[fecIngreso]', 8, 240, $dat[basico],'N','CCF32','T','$this->usuario', 'P', CAST(GETDATE() AS DATE), 'N', '$primaria')";
					//$r = mssql_query($sql, $this->con->conect);
					//$idA = ifx_getsqlca($r);
					$rs=$this->db->queryInsert($sql,'aportes016');
					$error=$this->db->error;
					if(is_array($error)){
						return false;
					}
					$idA = $rs;
				}
				/*if(!is_array($idA)){
				echo $sql;
				exit();
			}*/
				$idA = $idA['sqlerrd1'];
				if(is_numeric($idA)){
					$datAfil['idempresa'] = $rwEmp['idempresa'];
					$datAfil['idformulario'] = $idA;
					//@TODO se creo afiliacion para el trabajador
					$msj = "Se creo nueva afiliacion Activa primaria($primaria) para $dat[cedtra] con el nit $nit";
					$this->log->logSimpleMultiple('I_AFI', $msj);
					echo $msj . '<br/>';
				}else{
					//@TODO no se pudo crear la afiliacion, genera error
					$msj = "NO se pudo crear afiliacion primaria($primaria) para $dat[cedtra] con el nit $nit";
					$this->log->logSimpleMultiple('I_AFI', $msj);
					echo $msj . '<br/>';
					return false;
				}

				return $datAfil;

			}elseif($canAf == 1){
				//@todo Ya esta afiliado con esa empresa y se esta reportando novedad de ingreso, no se habia reportado retiro?
				$sql = "SELECT idformulario FROM aportes016 WHERE idpersona=$dat[idpersona] AND idempresa=$rwEmp[idempresa]";
				$r = mssql_query($sql, $this->con->conect);
				if(! $r){
					//@TODO ERROR esta afiliado pero no se pudo obtener el numero de id de la afiliacion
					return false;
				}else{
					$rwAf = mssql_fetch_array($r);
					mssql_free_result($r);

					$datAfil['idempresa'] = $rwEmp['idempresa'];
					$datAfil['idformulario'] = $rwAf['idformulario'];
				}

				return $datAfil;
			}
		}else{
			//@TODO No existe la empresa principal para crear afiliacion
			$msj = "NO existe el nit $nit para crear afiliacion para el trabajador $dat[cedtra] ";
			$this->log->logSimpleMultiple('I_AFI', $msj);
			echo $msj . '<br/>';
			return false;
		}
	}

	/**
	 *
	 */
	private function novedadRetiro($dat, $idformulario){
		$nit = $this->datCabezera['nit'];

		$anoAfi = substr($this->datCabezera['periodo'], 0, 4);
		$mesAfi = substr($this->datCabezera['periodo'], 4, 2);
		$fechaTemp=str_ireplace("-", "/", $dat['fecIngreso']);
		$fechaI = explode('/', $fechaTemp);
		$fechaIUnix = mktime(0, 0, 0, $fechaI[0], $fechaI[1], $fechaI[2]);
		$periodoIng = date('Ym', mktime(0, 0, 0, $fechaI[0], $fechaI[1], $fechaI[2]));

		$fechaSUnix = mktime(0, 0, 0, date('m'), 1, date('Y'));


		/*$fechaI = explode('/', $dat['fecIngreso']);
		$fechaIUnix = mktime(0, 0, 0, $fechaI[0], $fechaI[1], $fechaI[2]);
		$fechaSUnix = mktime(0, 0, 0, date('m'), 1, date('Y'));*/

		if($periodoIng == $this->datCabezera['periodo']){
			$fechaRetiro = date('m/d/Y', mktime(0, 0, 0, $mesAfi, ($fechaI[1] + $dat['diasCotizados']), $anoAfi));

		//$fechaFidelidad = date('m/d/Y', mktime(0, 0, 0, $fechaI[0], ($fechaI[1] + $dat['diasCotizados']+365), $fechaI[2]));
		}else{
			$fechaRetiro = date('m/d/Y', mktime(0, 0, 0,$mesAfi, $dat['diasCotizados'], $anoAfi));

		//$fechaFidelidad = date('m/d/Y', mktime(0, 0, 0, $fechaI[0], ($dat['diasCotizados']+365), $fechaI[2]));
		}
		//@TODO PENDIENTE CODIGO DE LA NOVEDAD DE RETIRO
		//$sql = "UPDATE aportes016 SET (estado, fecharetiro, motivoretiro, fechanovedad)=('I','$fechaRetiro', 'NOVEDAD RETIRO PLANILLA UNICA " . $this->datCabezera['planilla'] . "', TODAY) WHERE idformulario=$idformulario";
		//$sql = "EXECUTE PROCEDURE inactivar_afiliacion('$fechaRetiro', '2862', $idformulario)";
		$sql = "execute proc_inactivar_afiliacion 'I', {$idformulario}, {$dat['idpersona']}, 2858, '$fechaRetiro', '{$_SESSION["USUARIO"]}', 0";
		$r = mssql_query($sql, $this->con->conect);
		if($r==0){
			//@TODO no se pudo actualizar el estado
			$msj = "NO retirar el trabajador $dat[cedtra] afiliado con el nit $nit";
			$this->log->logSimpleMultiple('I_AFI', $msj);
			echo $msj . '<br/>';
		}else{
			//AC� Q SE TIENE Q HACER?????
			//mssql_free_result($r);
		}
	}
}
?>
