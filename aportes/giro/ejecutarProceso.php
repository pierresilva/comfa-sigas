<?php
	set_time_limit(0);
	ini_set("display_errors",'1');
	date_default_timezone_set("America/Bogota");
	$raiz = "";
	$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
	include_once  $root;
	include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';
	include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
	include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
	$db = IFXDbManejador::conectarDB();
	if($db->conexionID==null){
		$cadena = $db->error;
		echo msg_error($cadena);
		exit();
	}
	try {
		$validador = $_POST["validador"];
		$accion = "";
		
		if($validador=="S"){
			$accion = "VALIDADOR_GIRO_REVISION";
		}
		
		$resultado = null;
		$sql = "EXEC [giros].[sp_000_Invocar] @usuario = '{$_SESSION["USUARIO"]}', @accion = '$accion'";
		$rs = $db->querySimple($sql);
		if( ($row = $rs->fetchObject() ) == true ) {
			
			//Falta implementar el mensaje de salida
			if( @$row->Resultado == "EnMedioDeOtraEjecucion" || @$row->Resultado == "Iniciando" ){
				//El proceso esta en ejecucion
				$resultado = "PE";
			}else if( @$row->Resultado == "EnEsperaDeRevisionManual" ){
				//El proceso esta congelado
				$resultado = "PC";
			}
			//var_dump($row);
		}else{
			//Ojo porque el proceso puede estar ejecutando
			$resultado = "EE1";
		}
	} catch (Exception $e) {
		$resultado = "EE1";
		//echo "Error ".$e->getMessage();
	}
	echo json_encode($resultado);
?>