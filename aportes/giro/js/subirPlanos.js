/*
* @autor:      Ing. Orlando Puentes
* @fecha:      24/09/2010
* objetivo:
*/
/* var URL=src();*/

function mostrarAyuda(){
	$("#ayuda").dialog('open' );
}

function notas(){
	$("#dialog-form2").dialog('open');
}	

$(document).ready(function(){
	
	$("#tipogiro").change(function () {
		
		if ($("#tipogiro").val()==1){	
			$("#tipoarchivo").removeAttr("disabled");
			$("#tIdValor").attr("disabled","disabled");
			$("#tIdValor").val(0);
		}
		
		if ($("#tipogiro").val()==2){
			$("#tIdValor").removeAttr("disabled");
			$("#tipoarchivo").attr("disabled","disabled");
			$("#tipoarchivo").val("selected");
		}
		
	});

});

function procesar(causal,tipogiro,valor){
$("#ajax").html('<img src=../../imagenes/ajax-loader.gif />');
$("#boton").html(' ');
$.ajax({
	url: 'procesarPlano.php',
	data: {v0:causal,v1:tipogiro,v2:valor},
	type: "POST",
	async:false,
	success: function(datos){
	$("#ajax").html('&nbsp;');
    $("#ajax").html(datos);	
	}
});	
}
