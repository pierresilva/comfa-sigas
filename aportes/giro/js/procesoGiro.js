//var bandera = false;
var hilo = null
	, arrDataProcesamientoGlobal = []
	, arrProcesamientoNoValidador = [
	                                 "INICIO"
	                                 ,"Manual: Ejecucion cuota monetaria adicional afiliado"
	                                 ,"Manual: Ejecucion cuota monetaria adicional empresa"
	                                 ,"Manual: Ejecucion cuota monetaria general"
	                                 ,"Manual: Validador giro revision"
	                                 ,"Automatico: Inicio Validador giro revision"];

$(document).ready(function(){		 
	//$("#btnPreparar,#btnProcesar").button();
    /*window.onbeforeunload = function(){
    	if( bandera ){
    		alert("El proceso no ha teminado.");
    		return false;
    	}
    };*/
    var datosPeriodo = buscarPeriodo(),
		htmlPeriodo = "";
    if( datosPeriodo ){
		htmlPeriodo = "<tr><td style='text-align:center;'>" + datosPeriodo.periodo + "</td><td style='text-align:right;'>" + formatCurrency( datosPeriodo.cuotamonetaria ) + "</td>";
		htmlPeriodo += "<td style='text-align:center;'>" + datosPeriodo.procesado + "</td><td style='text-align:center;'>" + datosPeriodo.estadogiro + "</td></tr>";
    }
    $( "#tblInforPeriodo tbody" ).html( htmlPeriodo );
    
    estadoProceso();
    hilo = setInterval(estadoProceso,1000 * 45);
    //clearInterval(hilo);
});

function estadoProceso(){
	$("#hidBandera").val("");
	$.ajax({
		url:"estadoProceso.php",
		type:"POST",
		data:{},
		dataType:"json",
		async:false,
		success:function(datos){
			$("#tblResultado tbody, #tblResultado tfoot tr td").html("");
			if( datos.Bandera != "EE"){
				var htmlTfoot = null;
				var img = null;
				//var datosPeriodo = buscarPeriodo();
				$("#hidBandera").val( datos.Bandera );
				switch( datos.Bandera ){
					case "AB":
						habilitarBoton();
					break;
					case "END":
						//El proceso termino con exito
						mostrarProcedimientos( datos.Descripcion, 0 );
						htmlTfoot ="<h4 style='color:green;'>El proceso de la cuota monetaria ha terminado." +
								"<br/>El nuevo proceso esta para ("+ datos.FechaProceso + ")</h4>";
					break;
					case "AC" :
						//AUN FALTA DAR EL ULTIMO PASO PARA TERMINAR EL GIRO
						mostrarProcedimientos( datos.Descripcion, datos.Consec );
						//console.log("AC");
					break;
					case "IBC":
						//EL PROCESO ESTA EN ESPERA DE LA REVISION DEL USUARIO PARA SEGUIR EL PROCESO
						mostrarProcedimientos( datos.Descripcion, datos.Consec );
					break;
					case "EP":
						mostrarProcedimientos( datos.Descripcion, 0 );
						htmlTfoot = "<img src='" + URL + "imagenes/ajax-loader-fon.gif'/>";
					break;
					case "ERROR":
						//ERROR DEL PROCEDIMIENTO
						clearInterval(hilo);
						mostrarProcedimientos( datos.Descripcion, datos.Consec );
						htmlTfoot = "<h4 style='color:red;'>FIN_CON_ERROR</h4>";
					break;
					case "EDP":
						clearInterval(hilo);
						htmlTfoot = "<h4 style='color:red;'>Error al obtener los procesos ejecutados.</h4>";
					break;
					case "END":
						//el proceso del giro termino
						htmlTfoot = "<h4 style='color:red;'>FIN!!</h4>";
					break;
				}
				if(datos.Bandera != "AB")
					$("#tblResultado tfoot tr td").html( htmlTfoot );
			}
		}
	});
}
//Mostrar los procedimientos ejecutados
function mostrarProcedimientos( arrayDatos, consec, checked = "" ){
	arrDataProcesamientoGlobal = [];
	
	var html = "", desabilitar, arrayProcedimiento, nombreProcedimiento, desabilitar;
	$.each(arrayDatos, function(i, row){
		arrayProcedimiento = row.procedimiento.split(":");
		if(arrayProcedimiento.length == 2 || arrayProcedimiento[0] == "FIN_EXITOSO" ){
			
			arrDataProcesamientoGlobal[arrDataProcesamientoGlobal.length] = row;
			
			if( arrayProcedimiento[0] == "FIN_EXITOSO" )
				nombreProcedimiento = arrayProcedimiento[0];
			else
				nombreProcedimiento = arrayProcedimiento[1];
	
			desabilitar = "";
			revisado = "";
			
			if( parseInt( row.Consec ) === parseInt(consec) ){
				
				desabilitar = checked == "checked" ? " disabled" : "";
				revisado = "<input type='checkbox' onclick='habilitarBoton();' name='ckdConsecutivo' id='ckdConsecutivo' value='" + consec + "' "+ checked + desabilitar + "/>";
			}else {
				revisado = "<img src='" + URL + "imagenes/chk1.png'/>";
			}
			html += "<tr class='ui-state-default'><td>" + nombreProcedimiento + "</td><td>" + row.procesados + "</td><td>" + row.noprocesados + "</td><td>" + revisado + "</td></tr>";
		}	
	});
	$("#tblResultado tbody").html( html );
}

function buscarPeriodo(){
	var datosRetorno = false;
	$.ajax({
		url: "buscarPeriodo.php",
		type: "POST",
		data:{v0:"PA"},
		async: false,        
        dataType: "json",
        processData:true,
		success: function( datos ) {	
			if( typeof datos == "object" ){
				datosRetorno = datos;
			}			
		}
	});
	return datosRetorno;
}

function habilitarBoton(){
	var htmlTfoot = null;
	if( $("#ckdConsecutivo").length == 0 || $("#ckdConsecutivo").is(":checked") ){
		//Boton de Ejecutar
		htmlTfoot = "<input type='button' name='btnEjecutar' id='btnEjecutar' onclick='ejecutarProceso();' value='Ejecutar' class='ui-button ui-widget ui-state-default ui-corner-all'/>";
		clearInterval(hilo);
		
		//Boton para el validador
		if(habilitarBotonValidador()){
			htmlTfoot += "&nbsp;&nbsp;&nbsp;<input type='button' name='btnValidador' id='btnValidador' onclick='ejecutarProceso(\"S\");' value='Validador' class='ui-button ui-widget ui-state-default ui-corner-all'/>";
		}
	
	}
	else if( $("#ckdConsecutivo").length == 1 && $("#ckdConsecutivo").is(":checked") )
		htmlTfoot = "<h4 style='color:red;'>Debe revisar el ultimo procedimiento</h4>";
	$("#tblResultado tfoot tr td").html( htmlTfoot );
}

function habilitarBotonValidador(){
	var resultado = false;
	var contador  = 0;
	$.each(arrDataProcesamientoGlobal,function(i,row){
		var bandera = row.procedimiento.substring(0,(row.procedimiento.length-2));
		if(arrProcesamientoNoValidador.indexOf(row.procedimiento)<0 && arrProcesamientoNoValidador.indexOf(bandera)<0){
			contador++;
			//console.log(bandera);
		}
	});
	
	//Se excluye el procedimiento de inicio
	//if(contador<=arrProcesamientoNoValidador.length && arrDataProcesamientoGlobal.length>0){
	if(!contador>0 && arrDataProcesamientoGlobal.length>0){
		resultado = true;
	}
	
	return resultado;
}

function ejecutarProceso(validador = "N"){
	if(!confirm("Esta seguro de ejecutar el proceso"))
		return false;
	hilo = setInterval(estadoProceso,1000 * 45);
	$("#ckdConsecutivo").removeClass("ui-state-error");
	$("#tblResultado tfoot tr td").html("");
	var htmlTfoot = "";
	var banderaB = false;
	if( $("#ckdConsecutivo").length > 0 ){
		if( $("#ckdConsecutivo").is(":disabled") ){
			banderaB = true;
		}else if( $("#ckdConsecutivo").is(":checked") ){
			//El proceso fue revisado y puede continuar con el otro procedimiento
			var consec = parseInt( $("#ckdConsecutivo").val() ); 
			$.ajax({
				url: "actualizaRevision.php",
				type: "POST",
				data:{consec:consec, estado:1},
				async: false,        
		        dataType: "json",
		        processData:true,
				success: function( datos ) {
					if( parseInt(datos) === 1 ){
						banderaB = true;
					}else{
						clearInterval(hilo);
						htmlTfoot = "<h4 style='color:red;'>Error guardando la revision</h4>";
						$("#tblResultado tfoot tr td").html(htmlTfoot);
					}
				}
			});
		}else{
			$("#ckdConsecutivo").parent().addClass("ui-state-error");
			htmlTfoot = "<h4 style='color:red;'>Debe revisar el ultimo procedimiento</h4>";
			$("#tblResultado tfoot tr td").html(htmlTfoot);
		}
	}else if( $("#hidBandera").val() == "AB" ){
		banderaB = true;
	}
	//Se ejecuta el ultimo procedimiento fue algun Manual
	if( banderaB && $("#hidBandera").val() != "AC" ){
		
		$.ajax({
			url: "ejecutarProceso.php",
			type: "POST",
			data:{v0:"PA",validador:validador},
			async: false,        
	        dataType: "json",
	        processData:true,
			success: function( datos ) {
				if( datos == "PE" ){
					setTimeout(estadoProceso,2000);
				}else if( datos == "PC" ){
					htmlTfoot = "<h4 style='color:red;'>El proceso no puede continuar, revise los procedimientos.</h4>";
					$("#tblResultado tfoot tr td").html(htmlTfoot);
				}else{
					htmlTfoot = "<h4 style='color:red;'>Error, informe al administrador del sistema</h4>";
					$("#tblResultado tfoot tr td").html(htmlTfoot);
					clearInterval(hilo);
				}									
			}
		});
	}else if( banderaB ){
		estadoProceso();
	}	
}