<?php
/* autor:       orlando puentes
 * fecha:       octubre 4 de2010
 * objetivo:    Almacenar los movimientos diarios de las tarjetas.
 */
set_time_limit(0);
date_default_timezone_set("America/Bogota");

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'config.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}
$usuario=$_SESSION['USUARIO'];
include_once $raiz . DIRECTORY_SEPARATOR . 'funcionesComunes' . DIRECTORY_SEPARATOR . 'funcionesComunes.php';
$anno=date("Y");
$mes=date("m");
$dia=date("d"); 
$directorio = $ruta_cargados. DIRECTORY_SEPARATOR. 'giro' .DIRECTORY_SEPARATOR.'planos'.DIRECTORY_SEPARATOR; 

$causal=$_REQUEST["v0"];
$tipogiro=$_REQUEST["v1"];
$valor=$_REQUEST["v2"];

$dir = opendir($directorio);
while ($elemento = readdir($dir))
{
		if(strlen($elemento)>2)
		$archivos[]=$elemento;
} 
closedir($dir);

for($i=0; $i<count($archivos); $i++){
	echo "<br>Procesando archivo: ".$archivos[$i]."<br>";
	$directorio.=$archivos[$i];
	$archivoS = file($directorio); 
	$lineas = count($archivoS);
	foreach ($archivoS as $linea=>$archivoS){ 
		$cad=explode(' ', $archivoS);
		
		if($tipogiro==1){
			
				$sql ="update aportes099 set valorcuota=0,flag1='N',causal=$causal, usuario_modificacion='{$_SESSION["USUARIO"]}', fecha_modificacion=cast(getdate() as datetime) WHERE idregistro =$cad[0]";
				$rs=$db->queryActualiza($sql);
				//echo $rs;
				
		}
		
		if($tipogiro==2){
			$sql ="update aportes099 set valorcuota=$valor,flag1='S',causal=0, usuario_modificacion='{$_SESSION["USUARIO"]}', fecha_modificacion=cast(getdate() as datetime) WHERE idregistro =$cad[0]";
			$rs=$db->queryActualiza($sql);
			//echo $rs;
		}
	} 
	
	/* $viejo=$ruta_cargados."asopagos/saldos/cargados/".$archivos[$i];
	$nuevo=$ruta_cargados."asopagos/saldos/procesados/".$archivos[$i];
	if(moverArchivo($viejo,$nuevo)==false){
		echo "<br>No se pudo mover el archivo: $viejo";
	} */
}
 ?>
