<?php

set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");

$root= $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once '../../rsc/pdo/IFXDbManejador.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="es"><head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Anular Cuota Monetaria</title>
<link type="text/css" href="../../newcss/Estilos.css" rel="stylesheet">
<link type="text/css" href="../../newcss/css.1.8/jquery-ui-1.8.17.custom.css" rel="stylesheet">
<link type="text/css" href="../../newcss/marco.css" rel="stylesheet">

<script type="text/javascript" src="../../newjs/1.6/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="../../newjs/1.6/jquery-ui-1.8.16.custom.min.js"></script>

<script type="text/javascript" src="../../newjs/comunes.js"></script>
<script type="text/javascript" src="../../newjs/jquery.utilitarios.js"></script>
<script type="text/javascript" src="js/anulacionCuotaMonetaria.js"></script>
</head>

<body>
	<form name="forma">
		<br />		
			<div>
				<table width="75%" border="0" cellspacing="0" cellpadding="0" align="center">
					<tr>
						<td width="13" height="29" class="arriba_iz">&nbsp;</td>
						<td class="arriba_ce"><span class="letrablanca">:: Datos Afiliado ::</span></td>
						<td width="13" class="arriba_de" align="right">&nbsp;</td>
					</tr>
					<tr height="35" >
						<td class="cuerpo_iz">&nbsp;</td>
						<td class="cuerpo_ce">
							<table width="90%" cellspacing="0" cellpadding="0"
								align="center">
								<tr>
									<td>
										<img src="../../imagenes/menu/nuevo.png" title="Nuevo" style="cursor: pointer" onclick="nuevo();">										
									</td>
								</tr>
							</table></td>
						<td class="cuerpo_de">&nbsp;</td>
					</tr>
					<tr>
						<td class="cuerpo_iz">&nbsp;</td>
						<td class="cuerpo_ce">
							<table width="90%" border="0" cellspacing="0" cellpadding="0" align="center" class="tablero">
								<tr>
									<td width="10%" style="text-align: center;" >Tipo Documento</td>
									<td width="20%" >
										<label> &nbsp;&nbsp;&nbsp;
											<select name="cmbIdTipoDocumento" id="cmbIdTipoDocumento" class="box1" style="width: 210px" onchange="$('#txtIdentificacion').val('').trigger('blur');" >
												<option value="0" selected="selected">Seleccione...</option>
								           		<?php
													$rs = $db->Definiciones ( 1, 1 );
													while ( $row = $rs->fetch() ) {
														echo "<option value=" . $row ['iddetalledef'] . ">" . $row ['detalledefinicion'] . "</option>";
													} 
												?>
											</select> 
											<img src="../../imagenes/menu/obligado.png" width="12" height="12">
										</label>
									</td >
									<td width="10%" style="text-align: center;" >N&uacute;mero</td>
									<td width="20%" > &nbsp;&nbsp;&nbsp;
										<input id="txtIdPersona" type="hidden" value="" />
										<input name="txtIdentificacion" type="text" class="box1" id="txtIdentificacion" onkeyup="validarIdentificacion($('#cmbIdTipoDocumento'),this);" onkeydown="validarIdentificacion($('#cmbIdTipoDocumento'),this);" 
										onkeypress="validarIdentificacion($('#cmbIdTipoDocumento'),this);" onblur="buscarPersona($('#cmbIdTipoDocumento'), $('#txtIdentificacion'), $('#tdNombreCompleto'), false, $('#txtIdPersona'));" maxlength="12">											
										<img src="../../imagenes/menu/obligado.png" width="12" height="12"></td>
								</tr>
								<tr>
									<td style="text-align: center;" >Nombre Completo</td>
									<td id="tdNombreCompleto" colspan="3">&nbsp;</td>
								</tr>
								<tr>
									<td style="text-align: center;" >Periodo</td>
									<td colspan="3"> &nbsp;&nbsp;&nbsp;
										<input name="txtPeriodo" type="text" class="box1" id="txtPeriodo" style="width: 50px; vertical-align: super;" maxlength="6" readonly="true"  />
										&nbsp;&nbsp;&nbsp;
										<img width="58" height="20" style="cursor:pointer" onclick="buscarCuotasMonetarias();" src="../../imagenes/menu/iconoBuscar.png">
									</td>
								</tr>																	
							</table>
						</td>
						<td class="cuerpo_de">&nbsp;</td>
					</tr>
					<tr>
						<td class="abajo_iz">&nbsp;</td>
						<td class="abajo_ce"></td>
						<td class="abajo_de">&nbsp;</td>
					</tr>
				</table>
				<br />
				<table width="75%" border="0" cellspacing="0" cellpadding="0" align="center" id="tblCuotas">
					<tr>
						<td width="13" height="29" class="arriba_iz">&nbsp;</td>
						<td class="arriba_ce"><span class="letrablanca">:: Cuotas Monetarias Periodo <label id="lblPeriodo"></label> ::</span></td>
						<td width="13" class="arriba_de" align="right">&nbsp;</td>
					</tr>
					<tr>
						<td class="cuerpo_iz">&nbsp;</td>
						<td class="cuerpo_ce"><br />
							<table width="80%" border="0" cellspacing="0" cellpadding="0" align="center" class="tablero">
								<tr>
									<td style="text-align: center;" >Motivo </td>
									<td colspan="3"> 										
										<label> &nbsp;&nbsp;&nbsp;
											<select name="cmbIdCausal" id="cmbIdCausal" class="box1" style="width: 360px" >
												<option value="0" selected="selected">Seleccione...</option>
										      		<?php
														$rs = $db->Definiciones ( 62, 1 );
														while ( $row = $rs->fetch() ) {
															echo "<option value=" . $row ['iddetalledef'] . ">" . $row ['detalledefinicion'] . "</option>";
														} 
													?>
											</select>
										</label>	
									</td>
								</tr>
							</table>
							<br />							
						</td>
						<td class="cuerpo_de">&nbsp;</td>
					</tr>					
					<tr>
						<td class="cuerpo_iz">&nbsp;</td>
						<td class="cuerpo_ce">
							<table width="80%" border="0" cellspacing="0" cellpadding="0" align="center" class="tablero" id="tblCuotasMonetarias">
								<tr>
									<th width="10%">Anulado</th>
									<th width="10%">IdCuota</th>
									<th width="10%">IdBeneficiario</th>
									<th width="60%">Nombre</th>
									<th width="10%">Valor</th>
								</tr>
								<tbody id="tbCuotasMonetarias"></tbody>
							</table>
						</td>
						<td class="cuerpo_de">&nbsp;</td>
					</tr>									
					<tr>
						<td class="abajo_iz">&nbsp;</td>
						<td class="abajo_ce"></td>
						<td class="abajo_de">&nbsp;</td>
					</tr>					
				</table>
			</div>				
	</form>
</body>
</html>