<?php
/* autor:       orlando puentes
 * fecha:       octubre 4 de2010
 * objetivo:    Almacenar los movimientos diarios de las tarjetas.
*/
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'config.php';
$url=$_SERVER['PHP_SELF'];
include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'auditoria.php';
auditar($url);
include_once 'clases' . DIRECTORY_SEPARATOR . 'certificados.class.php';

$objCertificado = new Certificado();
$directorio = $ruta_cargados."certificados/recibidos/";
$dir = opendir($directorio);
while ($elemento = readdir($dir))
{
	$extensiones = explode(".",$elemento) ;
	$nombre = $extensiones[0] ;
	$nombre2  = $extensiones[1] ;
	$tipo = array ("txt","TXT");
	if(in_array($nombre2, $tipo)){
		$archivos[]=$elemento;
	}
}
closedir($dir);

for($i=0; $i<count($archivos); $i++){
	$consulta=$objCertificado->buscar_archivo($archivos[$i]);
	$conr=0;
	while($row = mssql_fetch_array($consulta)){
		$conr++;
	}
	if($conr>0){
		echo "<br><label class=Rojo>El archivo: ".$archivos[$i]." ya fue procesado!</label>";
                unlink($directorio.$archivos[$i]);
		continue;
	}
	$directorio.=$archivos[$i];
	$fp = fopen ($directorio,"r");
	$data = array();
	$con=0;
	$contadores=array();
	for($k=0;$k<15;$k++)
	$contadores[$k]=0;
	$acumula=0;
	while ($data = fgetcsv ($fp,2048,"\t")) {
		if($con>0){
			$fm=substr($data[4],0,10);
			$fm=explode("/", $fm);
			$data[1]="1";
			$data[3]=str_replace(",","",$data[3]);
			$data[8]=str_replace(",","",$data[8]);
			$data[10]=$fm[1]."/".$fm[0]."/".$fm[2];  //substr($data[4],0,10);
			$data[11]=$usuario;

			$contadores[14]=$fm[1]."/".$fm[0]."/".$fm[2];
			$insertar=$objClase->insertar_movimiento($data);
			if ($insertar==FALSE ){
				echo "<br>No se proceso la linea: $con ".$data[0]." ".$data[1]." ".$data[2]." ".$data[3]." ".$data[4]." ".$data[5]." ".$data[6]." ".$data[7]." ".$data[8] ." ".$data[9]."<br>Todo el proceso fue revertido";
				//log
				//$objConn->cancelar_transaccion();
				echo '100';
				exit();
			}
			mssql_free_result($insertar);
			$bono =$data[2];
			$valor=$data[8];
			$acumula+=$data[8];
			$tipo=$data[7];
			$cad=substr($data[7],0,4);
			switch($cad){
				case "Rede" :
					$tipo=1;
					$contadores[6]=$contadores[6]+1;
					$contadores[7]=$contadores[7]+$valor;
					break;
				case "Actu" :
					$tipo=2;
					$contadores[8]=$contadores[8]+1;
					$contadores[9]=$contadores[9]+$valor;
					break;
				case "Reve"   :
					$tipo=3;
					$contadores[10]=$contadores[10]+1;
					$contadores[11]=$contadores[11]+$valor;
					break;
				case "Cons"  :
					$tipo=4;
					$contadores[12]=$contadores[12]+1;
					break;
				case "Bloq"   :
					$tipo=5;
					$contadores[13]=$contadores[13]+1;
					break;
				default:
					echo $data[7]."<br/>";
			}
			if($tipo==1 or $tipo==2 or $tipo==3){
				if($objClase->actualizar_saldo($bono,$valor,$tipo) != true){
					//log
					//$objConn->cancelar_transaccion();
					continue;
				}
			}
		}
		$con++;
		if($con % 5000 == 0)
		$consulta=$objClase->cerrar();
	}
	//1 para los archivos de movimientos
	//conrol
	$contadores[0]=$archivos[$i];
	$contadores[1]=1;
	$contadores[2]=0;
	$contadores[3]=$usuario;
	$contadores[4]=($con-1);
	$contadores[5]=$acumula;
	if ( $objClase->insertar_control($contadores) != true){
		echo "<br><label class=Rojo>No se pudo insertar control archivo movimientos,".$archivos[$i]." intente de nuevo!</label>";
		//log
		//$objConn->cancelar_transaccion();
		continue;
	}
	echo "<br>Archivo procesado: ".$archivos[$i];
	//$objConn->confirmar_transaccion();
	$viejo= $ruta_cargados."movimientos/recibidos/".$archivos[$i];
	$nuevo= $ruta_cargados."movimientos/procesados/".$archivos[$i];
	//$viejo="/var/www/html/planos_cargados/movimientos/recibidos/".$archivos[$i];
	//$nuevo="/var/www/html/planos_cargados/movimientos/procesados/".$archivos[$i];
	if(moverArchivo($viejo,$nuevo)==false){
		echo "<br>No se pudo mover el archivo: $viejo";
	}
}

 ?>
