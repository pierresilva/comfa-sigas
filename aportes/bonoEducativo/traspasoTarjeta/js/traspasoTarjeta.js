// JavaScript Document
var nuevoR=0;
var continuar=0;
var idp=0;
var ide=0;
var codigos='';
var pq=0;
var flag=0;
var idbe=0;
var ts = 0;
var ta = 0;

$(document).ready(function(){
	$('img[alt=Nuevo]').bind('click', nuevo);
	$('img[alt=Guardar]').bind('click', guardar);
	$('#txtnumero').bind('blur', buscarPersona);
	$('input[type=text], select').focus(function(){
        $(this).css({background: "#C2D2FE"});
    }); 
 });


function nuevo(){
	limpiarCampos();
	nuevoR=1;
	idp=0;
	ide=0;
	idbe=0;
	idco=0;
	$("#txtnumero").focus();
	$('#txtfecha').val(fechaHoyCorta());
	$('#ttarj').empty();
	$("#ttarja").empty();
	}

function buscarPersona(){
	$('#ttarj').empty();
	var num = $("#txtnumero").val();
	var idtd = $("#txttipo").val();
	var nom;
	continuar=0;
	num = $.trim(num);
	if(num.length==0) return false;
	$.ajax({
        url: "../afiliados/buscarPersona.php",
        async:true,
        data:{v0:num,v1:idtd},
        beforeSend: function(objeto){
        	dialogLoading('show');
        },
        
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }
			else 
            	if (continuar==1) buscarAfiliacionAct();
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("Error... Pasó lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
        success: function(datos){
        	if(datos==0){
    			alert("La persona no existe!");
    			nuevo();
    			return false;
    			}	
    		$.each(datos,function(i,f){
    			continuar=1;
    			nom = f.pnombre + " " +f.snombre +" "+ f.papellido + " " + f.sapellido;
    			idp=f.idpersona;
				$("#idpersona").val(idp);
    			return;
    			})
    		$("#txtnombre").val(nom);
    		
           },
        timeout: 30000,
        type: "GET"
 });
}

function buscarAfiliacionAct(){
	continuar=0;
	$.ajax({
        url: "../reclamos/afiliados/buscarAfiliacionAct.php",
        async:true,
        data:{v0:idp},
        beforeSend: function(objeto){
        	dialogLoading('show');
        },
        
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("Error... Pasó lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
        success: function(datos){
        	if(datos==0){
    			alert("La persona no tiene Afiliacion!");
    			nuevo();
    			return false;
    			}	
			$("#txtformulario").val(datos[0].formulario);
    		$("#txtafiliacion").val(datos[0].afiliacion);
    		$("#txtestado").val(datos[0].estado);
			$("#txtnit").val(datos[0].nit);
			$("#txtempresa").val(datos[0].razonsocial);
			$("#txtclase").val(datos[0].clase);
			ide=datos[0].idempresa;
			//traer_bene(idp);
			buscarTarjetas();
           },
        timeout: 30000,
        type: "GET"
 });
	}
	
	


	
function guardar(){
	var con=0;
	if(idp==0){
		alert("Falta Informacion del afiliado!");
		return false;
	}
	if(ts == 0 || ta == 0 || ts == ta){
		alert("No Hay Condiciones para Cambio!");
		return false;
	}
	var r = confirm("Esta Seguro de Traspasar los Bonos a Ultima Tarjeta Activa?");
	if(r==true){
		$.ajax({
			url: "tarjetas/t.traspasoTarjetaActiva.php",
		    async:true,
		    data:{v0:idp},
		    beforeSend: function(objeto){
		       	dialogLoading('show');
		    },
		    complete: function(objeto, exito){
		      	dialogLoading('close');
		        if(exito != "success"){
		    	    alert("No se completo el proceso!");
		        }
		    },
		    contentType: "application/x-www-form-urlencoded",        
			dataType: "json",
		    error: function(objeto, quepaso, otroobj){
		        alert("Fallo... Pasó lo siguiente: "+quepaso);
		    },
		    global: true,
		    ifModified: false,
		    processData:true,
		    success: function(datos){
		    	var msg;
		       	if(datos==1){
		    		alert("Subsidios Asignados a Ultima Tarjeta Activa!");
		    	}
		       	if(datos==0){
		    		alert("No se pudo realizar el proceso!");
		    	}
		    	
		    },
		    timeout: 3000,
		    type: "GET"
		});
	}
	
	nuevo();
	}	
	
function buscarTarjetas(){
	pq=0;
	codigos='';
	$.ajax({
		url: "tarjetas/b.tarjetaSubsidio.php",
	    async:true,
	    data:{v0:idp},
	    beforeSend: function(objeto){
	       	dialogLoading('show');
	    },
	    complete: function(objeto, exito){
	      	dialogLoading('close');
	        if(exito != "success"){
	    	    alert("No se completo el proceso!");
	        }
	    },
	    contentType: "application/x-www-form-urlencoded",        
		dataType: "json",
	    error: function(objeto, quepaso, otroobj){
	        alert("Fallo... Pasó lo siguiente: "+quepaso);
	    },
	    global: true,
	    ifModified: false,
	    processData:true,
	    success: function(datos){
	    	var msg;
	       	if(datos==2){
	    		alert("El afiliado no tiene subsidios asignados!");
	    		nuevo();
	    	}
	       	if(datos==0){
	    		alert("No se encontraron tarjetas del Afiliado!");
	    		ts = 0;
	    	}
	        $("#ttarj").empty();	
	    	$.each(datos,function(i,f){
				$("#ttarj").append("<tr><td>"+f.bono+"</td><td>"+f.estado+"</td></tr>");
				ts = f.bono;
	    		return;
	    	})
	    	
	    },
	    timeout: 3000,
	    type: "GET"
	});	
	$.ajax({
		url: "tarjetas/b.ultimaTarjetaActivada.php",
	    async:true,
	    data:{v0:idp},
	    beforeSend: function(objeto){
	       	dialogLoading('show');
	    },
	    complete: function(objeto, exito){
	      	dialogLoading('close');
	        if(exito != "success"){
	    	    alert("No se completo el proceso!");
	        }
	    },
	    contentType: "application/x-www-form-urlencoded",        
		dataType: "json",
	    error: function(objeto, quepaso, otroobj){
	        alert("Fallo... Pasó lo siguiente: "+quepaso);
	    },
	    global: true,
	    ifModified: false,
	    processData:true,
	    success: function(datos){
	    	var msg;
	       	if(datos==0){
	       		alert("No se encontraron tarjetas activas del beneficiario!");
	       		ta = 0;
	    	}
	        $("#ttarja").empty();	
	    	$.each(datos,function(i,f){
				$("#ttarja").append("<tr><td>"+f.bono+"</td><td>"+f.estado+"</td></tr>");
				ta = f.bono;
	    		return;
	    	})
	    	
	    },
	    timeout: 3000,
	    type: "GET"
	});	
	
	}	