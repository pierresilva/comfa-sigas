// JavaScript Document
var nuevoR=0;
var idp=0;
var ide=0;
var continuar=0;


$(document).ready(function(){
	$('img[alt=Nuevo]').bind('click', nuevo);
	//$('img[alt=Guardar]').bind('click', guardar);
	$('#txtnumero').bind('blur', buscarPersona);
	$('input[type=text], select').focus(function(){
        $(this).css({background: "#C2D2FE"});
    }); 
 });


function nuevo(){
	limpiarCampos();
	nuevoR=1;
	idp=0;
	ide=0;
	$("#txtnumero").focus();
	$('#txtfecha').val(fechaHoyCorta());	
	$("#tBeneficiarios").empty();	
}

function buscarPersona(){
	var num = $("#txtnumero").val();
	var idtd = $("#txttipo").val();
	nuevo();
	$("#txtnumero").val(num);
	$("#txttipo").val(idtd);
	
	var nom;
	continuar=0;
	num = $.trim(num);
	if(num.length==0) return false;
	$.ajax({
        url: "../afiliados/buscarPersona.php",
        async:true,
        data:{v0:num,v1:idtd},
        beforeSend: function(objeto){
        	dialogLoading('show');
        },
        
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }			
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("Error... Pasó lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
        success: function(datos){
        	if(datos==0){
    			alert("La persona no existe!");
    			nuevo();
    			return false;
    			}	
    		$.each(datos,function(i,f){
    			continuar=1;
    			nom = f.pnombre + " " +f.snombre +" "+ f.papellido + " " + f.sapellido;
    			idp=f.idpersona;
    			buscarPaquetes();
    			return;
    			})
    		$("#txtnombre").val(nom);    		
           },
        timeout: 30000,
        type: "GET"
 });
}

function buscarAfiliacionAct(){
	continuar=0;
	$.ajax({
        url: "../reclamos/afiliados/buscarAfiliacionAct.php",
        async:true,
        data:{v0:idp},
        beforeSend: function(objeto){
        	dialogLoading('show');
        },
        
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }
            else if(continuar==1)
            		buscarPaquetes();
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("Error... Pasó lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
        success: function(datos){
        	if(datos==0){
    			alert("La persona no tiene Afiliacion!");    			
    			return false;
    		}	
			$("#txtformulario").val(datos[0].formulario);
    		$("#txtafiliacion").val(datos[0].afiliacion);
    		$("#txtestado").val(datos[0].estado);
			$("#txtnit").val(datos[0].nit);
			$("#txtempresa").val(datos[0].razonsocial);	
			
            //buscarPaquetes();
        },
        timeout: 3000,
        type: "GET"
 });
}
	
function buscarPaquetes(){
	pq=0;
	$('#tBeneficiarios').empty();
	$.ajax({
		url: "b.PaqueteAfiliadoSPositivo.php",
	    async:false,
	    data:{v0:idp},
	    beforeSend: function(objeto){
	       	dialogLoading('show');
	    },
	    complete: function(objeto, exito){
	      	dialogLoading('close');
	        if(exito != "success"){
	    	    alert("No se completo el proceso!");
	        }
	    },
	    contentType: "application/x-www-form-urlencoded",        
		dataType: "json",
	    error: function(objeto, quepaso, otroobj){
	        alert("Fallo... Pasó lo siguiente: "+quepaso);
	    },
	    global: true,
	    ifModified: false,
	    processData:true,
	    success: function(datos){
	    	var msg;
	       	if(datos==0){
	    		alert("El afiliado no tiene subsidio escolar asignado!");
	    		nuevo();
	    		return false;
	    	}		        
	        var emb=0;
	       	
	    	$.each(datos,function(i,f){
				var nom = f.pnombre + " " + f.papellido;
				var emp = f.razonsocial;
				ide = f.idempresa;
				
				if(f.estado=='A' || f.estado=='E' ){
					if(f.estado=='A'){
						msg='SUBSIDIO PENDIENTE DE REDIMIR';
					}
					if(f.estado=='E'){
						msg='SUBSIDIO REDIMIDO';
					}
					pq++;
					
					if(f.embargo=='S'){
						msg='SUBSIDIO EMBARGADO ';
						emb++;
		    		}
					
					$("#txtempresa").val(emp);
		    		$("#tBeneficiarios").append("<tr id="+(i+1)+"><td>"+(i+1)+"</td><td>"+f.identificacion+"</td><td>"+f.idbeneficiario+"</td><td>"+f.edadbeneficiario+"</td><td id='tdNombre'>"+nom+"</td><td>"+msg+"</td><td>"+f.observaciones+"</td></tr>");		    		
				}
	    	})
	    	
	    	if(pq==0){
	    		alert("El afiliado no tiene subsidio escolar para suspender!");
	    		nuevo();
	    		return false;
	    	} else
	    		buscarAfiliacionAct();
	    },
	    timeout: 3000,
	    type: "GET"
	});
}	