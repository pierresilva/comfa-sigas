// JavaScript Document
var nuevoR=0;
var continuar=0;
var idp=0;
var ide=0;
var codigos='';
var pq=0;
var flag=0;
var idbe=0;

$(document).ready(function(){
	$('img[alt=Nuevo]').bind('click', nuevo);
	$('img[alt=Guardar]').bind('click', guardar);
	$('#txtnumero').bind('blur', buscarPersona);
	$('input[type=text], select').focus(function(){
        $(this).css({background: "#C2D2FE"});
    }); 
 });


function nuevo(){
	limpiarCampos();
	nuevoR=1;
	idp=0;
	ide=0;
	idbe=0;
	idco=0;
	$("#txtnumero").focus();
	$('#txtfecha').val(fechaHoyCorta());
	$('#tbene').empty();
	$("#tBeneficiarios").empty();
	}

function buscarPersona(){
	$('#tbene').empty();
	var num = $("#txtnumero").val();
	var idtd = $("#txttipo").val();
	var nom;
	continuar=0;
	num = $.trim(num);
	if(num.length==0) return false;
	$.ajax({
        url: "../../afiliados/buscarPersona.php",
        async:true,
        data:{v0:num,v1:idtd},
        beforeSend: function(objeto){
        	dialogLoading('show');
        },
        
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }
			else 
            	if (continuar==1) buscarAfiliacionAct();
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("Error... Pasó lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
        success: function(datos){
        	if(datos==0){
    			alert("La persona no existe!");
    			nuevo();
    			return false;
    			}	
    		$.each(datos,function(i,f){
    			continuar=1;
    			nom = f.pnombre + " " +f.snombre +" "+ f.papellido + " " + f.sapellido;
    			idp=f.idpersona;
				$("#idpersona").val(idp);
    			return;
    			})
    		$("#txtnombre").val(nom);
    		
           },
        timeout: 30000,
        type: "GET"
 });
}

function buscarAfiliacionAct(){
	continuar=0;
	$.ajax({
        url: "buscarAfiliacionAct.php",
        async:true,
        data:{v0:idp},
        beforeSend: function(objeto){
        	dialogLoading('show');
        },
        
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("Error... Pasó lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
        success: function(datos){
        	if(datos==0){
    			alert("La persona no tiene Afiliacion!");
    			nuevo();
    			return false;
    			}	
			$("#txtformulario").val(datos[0].formulario);
    		$("#txtafiliacion").val(datos[0].afiliacion);
    		$("#txtestado").val(datos[0].estado);
			$("#txtnit").val(datos[0].nit);
			$("#txtempresa").val(datos[0].razonsocial);
			$("#txtclase").val(datos[0].clase);
			ide=datos[0].idempresa;
			traer_bene(idp);
			buscarPaquetes();
           },
        timeout: 30000,
        type: "GET"
 });
	}
	
function traer_bene(idp){
	var nom;
	var html;
	continuar=0;
	if(idp==0){
		alert("No hay ID Afiliado"); 
		return false;
	}
	$.ajax({
        url: "buscarBeneficiarios.php",
        async:true,
        data:{v0:idp},
        beforeSend: function(objeto){
        	dialogLoading('show');
        },
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("Error... Pasó lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
        success: function(datos){
        	if(datos==0){
    			alert("No hay relaciones de convivencia!");
    			return false;
    			}	
    		$.each(datos,function(i,f){
    			continuar=1;
    			nom = f.pnombre + " " +f.snombre +" "+ f.papellido + " " + f.sapellido;
				if(f.embarga=="S")
					var emb ='S';
				else
					var emb='N';
    			$("#tbene").append("<tr><td>"+f.idbeneficiario+"</td><td>"+nom+"</td><td>"+f.detalledefinicion+"</td><td>"+f.idconyuge+"</td><td style='text-align:center'>"+f.fechanacimiento+"</td><td style='text-align:center'>"+f.edad+"</td><td style='text-align:center'>"+emb+"</td><td style='text-align:center'>"+f.capacidadtrabajo+"</td><td style='text-align:center'>"+f.estado+"</td><td style='text-align:center'><input class='ck' type='checkbox' name='checkbox' value='"+f.idbeneficiario+"' /></td></tr>")
    			return;
    			})
           },
        timeout: 30000,
        type: "GET"
 });
}	

function marcarTodo(){
	if(flag==0){
		$('.ck').each(function(){
     	var checkbox = $(this);
	 	$('[name="checkbox"].ck').attr('checked',true);
      });
	  flag=1;
	}
	else{
		$('.ck').each(function(){
     	var checkbox = $(this);
	 	$('[name="checkbox"].ck').attr('checked',false);
      });
	  flag=0;
		}
	}
	
function guardar(){
	var con=0;
	if(idp==0){
		alert("Falta Informacion del afiliado!");
		return false;
		}
	if($("#motivo").val()==0){
		alert("Seleccione motivo del reclamo!");
		$("#motivo").focus();
		return false;
	}
	$('.ck').each(function(){
     	var checkbox = $(this);
	 	if( checkbox.is(':checked') == true ) con++ 
      });
	 if(con==0){
		 alert("No hay beneficiarios AUTORIZADOS, marque almenos UNO!");
		 return false;
		 }
	 
	var marcados=0;
	var idmotivo=$("#motivo").val();
	$('.ck').each(function(){
     	var checkbox = $(this);
	 	if( checkbox.is(':checked') == true ){
			idbe= checkbox.attr('value');
			$.ajax({
		        url: "../guardarReclamo.php",
		        async:false,
		        data:{v0:idp,v1:idbe,v2:ide,v3:idmotivo,v4:1},
		        beforeSend: function(objeto){
		        	dialogLoading('show');
		        },
		        
		        complete: function(objeto, exito){
		        	dialogLoading('close');
		            if(exito != "success"){
		                alert("No se completo el proceso!");
		            }
		        },
		        contentType: "application/x-www-form-urlencoded",
		        dataType: "json",
		        error: function(objeto, quepaso, otroobj){
		            alert("Error... Pasó lo siguiente: "+quepaso);
		        },
		        global: true,
		        ifModified: false,
		        processData:true,
		        success: function(datos){
		        	if(datos==0){
		    			alert("No se pudo asignar el subsidio de: " + idbe);
		    			return false;
		    			}	
					if(datos==9){
						alert("El beneficiario ya tiene subsidio asignado!");
						return false;
						}
		    		marcados=marcados+1;
		           },
		        timeout: 300000,
		        type: "GET"
				});
		}
		 });
	alert("Subsidios asignados "+marcados+", los pueden reclamar!");
	nuevo();
	}	
	
function buscarPaquetes(){
	pq=0;
	codigos='';
	$.ajax({
		url: "../../afiliados/b.PaqueteAfiliado.php",
	    async:true,
	    data:{v0:idp},
	    beforeSend: function(objeto){
	       	dialogLoading('show');
	    },
	    complete: function(objeto, exito){
	      	dialogLoading('close');
	        if(exito != "success"){
	    	    alert("No se completo el proceso!");
	        }
	    },
	    contentType: "application/x-www-form-urlencoded",        
		dataType: "json",
	    error: function(objeto, quepaso, otroobj){
	        alert("Fallo... Pasó lo siguiente: "+quepaso);
	    },
	    global: true,
	    ifModified: false,
	    processData:true,
	    success: function(datos){
	    	var msg;
	       	if(datos==0){
	    		alert("El afiliado no tiene subsidios asignados!");
	    	}	
	        $("#tBeneficiarios").empty();	
	    	$.each(datos,function(i,f){
				var nom = f.pnombre + " " + f.papellido;
				//var emp = f.nit + " " + f.razonsocial;
				var estilo='';
				
				if(f.estado=='E'){
					var ag='';
					switch (f.idagencia){
						case '01':
							ag='NEIVA';
							break;
						case '02':
							ag='GARZON';
							break;
						case '03':
							ag='PITALITO';
							break;
						case '04':
							ag='LA PLATA';
							break;
					}					
					msg='REDIMIDO EL: '+ f.fechaentrega+" "+f.horaentrega.substring(0,5)+", "+f.observaciones+" en "+ag;
				} else if(f.embargo=='S'){
					msg='SUBSIDIO EMBARGADO ';
					estilo="style='color:#FF0000;'";
	    		}
	    		else if(f.estado=='A'){
	    			msg='SUBSIDIO PENDIENTE REDIMIR';
	    			pq++;
	    			if(pq==1)
	    				codigos=f.idbeneficiario;
	    			else
	    				codigos = codigos + ',' + f.idbeneficiario;
	    		}
				//$("#txtempresa").val(emp);
	    		$("#tBeneficiarios").append("<tr "+ estilo +"><td>"+(i+1)+"</td><td>"+f.idbeneficiario+"</td><td>"+f.edadbeneficiario+"</td><td>"+nom+"</td><td>"+msg+"</td></tr>");
	    		return;
	    	})
	    	$('#embargo').html("Subsidios a entregar: " + pq);
	    	if(pq>0)
	    		$("#entregar").html("<label onclick='entregarPaquete()'><font color='green' size=+2 style = 'cursor:pointer'>Entregar</font></label>")
	        },
	        timeout: 3000,
	        type: "GET"
	});	
	
	}	