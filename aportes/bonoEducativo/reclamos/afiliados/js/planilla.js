// JavaScript Document


function traerPlanilla(idp){
	var nom;
	var html = "";
	$("#trPlanillas").html("");
	
	continuar=0;
	if(idp==0){
		alert("No hay ID Afiliado"); 
		return false;
	}
	$.ajax({
        url: "buscarPlanilla.php",
        async:true,
        data:{v0:idp},
        beforeSend: function(objeto){
        	dialogLoading('show');
        },
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("Error... Pasó lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
        success: function(datos){
        	if(datos==0){
    			alert("No hay relaciones de Planillas!");
    			return false;
    		}	
    		$.each(datos,function(i,n){
    			continuar=1;
    			$("#trPlanillas").append("<tr><td style=text-align:right>"+n.nit+"</a></td><td style=text-align:right>"+n.planilla+"</td><td style=text-align:center>"+n.periodo+"</td><td style=text-align:right>"+formatNumber(parseInt(n.salariobasico))+"</td><td style=text-align:right>"+formatNumber(parseInt(n.ingresobase))+"</td><td style=text-align:right>"+n.diascotizados+"</td><td style=text-align:center>"+n.fechapago+"</td><td>"+n.ingreso+"</td><td>"+n.retiro+"</td><td>"+n.var_tra_salario+"</td><td>"+n.vacaciones+"</td><td>"+n.lic_maternidad+"</td><td>"+n.tipo_cotizante+"</td><td>"+n.procesado+"</td><td>"+n.correccion+"</td><td>"+n.inc_tem_emfermedad+"</td><td>"+n.inc_tem_acc_trabajo+"</td><td>"+n.sus_tem_contrato+"</td></tr>");
    			return;
    		});
        },
        timeout: 30000,
        type: "GET"
	});
	
}

