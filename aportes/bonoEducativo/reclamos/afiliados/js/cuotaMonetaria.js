// JavaScript Document


function traercuota(idp){
	var nom;
	var html;
	continuar=0;
	if(idp==0){
		alert("No hay ID Afiliado"); 
		return false;
	}
	$.ajax({
        url: "buscarCuotaMonetaria.php",
        async:true,
        data:{v0:idp},
        beforeSend: function(objeto){
        	dialogLoading('show');
        },
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("Error... Paso lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
        success: function(datos){
        	if(datos==0){
    			alert("No registros de subsidios pagados!");
    			return false;
    			}	
    		$.each(datos,function(i,f){
    			continuar=1;
    			nom = f.pnombre + " " +f.snombre +" "+ f.papellido + " " + f.sapellido;
    			$("#tcuotas").append("<tr><td style='text-align:center'>"+f.periodo+"</td><td>"+f.idbeneficiario+"</td><td>"+nom+"</td><td style='text-align:center'>"+formatNumber(f.valor)+"</td></tr>")
    			return;
    			})
           },
        timeout: 30000,
        type: "GET"
 });
}