// JavaScript Document


function traerconyuge(idp){
	var nom;
	var html;
	continuar=0;
	if(idp==0){
		alert("No hay ID Afiliado"); 
		return false;
	}
	$.ajax({
        url: "buscarConyuge.php",
        async:true,
        data:{v0:idp},
        beforeSend: function(objeto){
        	dialogLoading('show');
        },
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("Error... Pasó lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
        success: function(datos){
        	if(datos==0){
    			alert("No hay relaciones de convivencia!");
    			return false;
    			}	
    		$.each(datos,function(i,f){
    			continuar=1;
    			nom = f.pnombre + " " +f.snombre +" "+ f.papellido + " " + f.sapellido;
    			$("#trelaciones").append("<tr><td>"+f.idconyuge+"</td><td>"+f.identificacion+"</td><td>"+nom+"</td><td style='text-align:center'>"+f.conviven+"</td></tr>")
    			return;
    			})
           },
        timeout: 30000,
        type: "GET"
 });
}

function traerbene(idp){
	var nom;
	var html;
	continuar=0;
	if(idp==0){
		alert("No hay ID Afiliado"); 
		return false;
	}
	$.ajax({
        url: "buscarBeneficiarios.php",
        async:true,
        data:{v0:idp},
        beforeSend: function(objeto){
        	dialogLoading('show');
        },
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("Error... Pasó lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
        success: function(datos){
        	if(datos==0){
    			alert("No hay relaciones de convivencia!");
    			return false;
    			}	
    		$.each(datos,function(i,f){
    			continuar=1;
    			nom = f.pnombre + " " +f.snombre +" "+ f.papellido + " " + f.sapellido;
				if(f.embarga=="S")
					var emb ='S';
				else
					var emb='N';
    			$("#tbeneficiarios").append("<tr><td>"+f.idbeneficiario+"</td><td>"+nom+"</td><td>"+f.idconyuge+"</td><td style='text-align:center'>"+f.fechanacimiento+"</td><td style='text-align:center'>"+f.edad+"</td><td style='text-align:center'>"+emb+"</td><td></td></tr>")
    			return;
    			})
           },
        timeout: 30000,
        type: "GET"
 });
}
