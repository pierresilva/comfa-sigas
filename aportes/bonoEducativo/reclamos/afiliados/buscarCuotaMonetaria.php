<?php
date_default_timezone_set('America/Bogota');
ini_set("display_errors",'1');
session_start();
$idp =$_REQUEST['v0'];

include_once '../../rsc/pdo/SQLDbManejador.php';
$db = SQLDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo $cadena;
	exit();
}
$filas = array();
$sql="SELECT TOP 10 periodo,idbeneficiario, papellido,sapellido,pnombre,snombre,valor
FROM (
SELECT periodo,idbeneficiario,valor
from Aportes.dbo.aportes009 
WHERE isnumeric(periodo)=1 AND idtrabajador=$idp
UNION
SELECT periodo,idbeneficiario,valor
from Aportes.dbo.aportes014 
WHERE isnumeric(periodo)=1 AND idtrabajador=$idp
) a
INNER JOIN Aportes.dbo.aportes015 ON a.idbeneficiario = aportes015.idpersona
ORDER BY periodo DESC";
$rs=$db->querySimple($sql);
if(is_null($rs)){
	echo 0;
	exit();
}
while ($row=$rs->fetch()) {
		$filas[] = $row;
}
if(count($filas)>0)
	echo json_encode($filas);
else 
	echo 0;
?>