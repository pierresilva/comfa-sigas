<?php
set_time_limit(0);
date_default_timezone_set('America/Bogota'); 
ini_set("display_errors",'1');

$root= $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $_SESSION['RAIZ'] . DIRECTORY_SEPARATOR . '/rsc/pdo/IFXDbManejador.php';
$idp =$_REQUEST['v0'];

$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo $cadena;
	exit();
}
$anno=date("Y")-1;

$filas = array();
$sql="DECLARE @anno CHAR(4) = $anno
SELECT idbeneficiario, idconyuge, identificacion, pnombre, snombre,papellido,sapellido,a91.detalledefinicion,fechanacimiento, 
convert(DECIMAL(7,3),DATEDIFF(DAY, fechanacimiento, @anno+'-12-31') / 365.25) AS edad, idconyuge,embarga,capacidadtrabajo,c.estado 
FROM Aportes.dbo.aportes021 c 
left JOIN Aportes.dbo.aportes015 p ON c.idbeneficiario=p.idpersona 
left JOIN Aportes.dbo.aportes091 a91 ON a91.iddetalledef=c.idparentesco 
WHERE c.idparentesco IN(35,36,37,38) AND  idtrabajador=$idp";
$rs=$db->querySimple($sql);
if(is_null($rs)){
	echo 0;
	exit();
}
while ($row=$rs->fetch()) {
		$filas[] = $row;
	}
if(count($filas)>0)
	echo json_encode($filas);
else 
	echo 0;
?>