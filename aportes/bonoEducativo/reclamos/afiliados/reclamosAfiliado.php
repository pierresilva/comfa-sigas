<?php
set_time_limit(0);
date_default_timezone_set('America/Bogota');
ini_set("display_errors",'1');

$root= $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Reclamo Afiliado</title>
<link href="../../css/jquery-ui-1.9.2.custom.min.css" type="text/css" rel="stylesheet"  />
<link href="../../css/estilo.css" type="text/css" rel="stylesheet"  />
<script  src="../../js/jquery-1.8.3.js"></script>
<script  src="../../js/jquery-ui-1.9.2.custom.min.js"></script>
<script  src="../../js/funciones.js"></script>
<script  src="js/reclamosAfiliado.js"></script>
<script  src="js/grupo.js"></script>
<script  src="js/planilla.js"></script>
<script  src="js/aportes.js"></script>
<script  src="js/cuotaMonetaria.js"></script>
<script>
$(function() {
     $( "#tabs" ).tabs({
     beforeLoad: function( event, ui ) {
     ui.jqXHR.error(function() {
     ui.panel.html(
     "No se pudo cargar esta pestaña. Vamos a tratar de solucionar este problema lo antes posible. Si esto no sería una demostración." );
     });
     }
     });
   });
</script>
</head>
<body>
<form id="forma">
<center>
<div id="tabs" style="width:90%">
<ul>
  <li><a href="#tabs-1">Afiliado</a></li>
  <!--<li><a href="grupo.php">Grupo Familiar</a></li>
  <li><a href="cuotaMonetaria.php">Cuota Monetaria</a></li>
  <li><a href="planilla.php">Planilla Unica</a></li>
  <li><a href="aportes.php">Aportes</a></li>-->
</ul>
<div id="tabs-1">
    <table width="90%" border="0" cellspacing="0" cellpadding="0">
  	<tr>
    <td width="13" height="29" background="../../imagenes/tablero/arriba_izq.gif">&nbsp;</td>
    <td background="../../imagenes/tablero/arriba_central2.gif">
    <span class="letrablanca">::Reclamo Intividual &nbsp;::</span></td>
    <td width="13" background="../../imagenes/tablero/arriba_der.gif" align="right">&nbsp;</td>
  	</tr>
  	<tr>
    <td background="../../imagenes/tablero/izquierda2.jpg">&nbsp;</td>
    <td background="../../imagenes/tablero/centro.gif" style="text-align:left">
    <img src="../../imagenes/iconos/spacer.gif" width="1" height="1">
	<img src="../../imagenes/iconos/spacer.gif" width="1" height="1">
	<img src="../../imagenes/iconos/nuevo.png" alt="Nuevo" width="16" height="16" style="cursor:pointer" title="Nuevo">
	<img src="../../imagenes/iconos/spacer.gif" width="1" height="1">
	<img src="../../imagenes/iconos/grabar.png" alt="Guardar" width="16" height="16" style="cursor:pointer">
	</td>
    <td background="../../imagenes/tablero/derecha.gif">&nbsp;</td>
  	</tr>
  	<tr>
    <td background="../../imagenes/tablero/izquierda2.jpg">&nbsp;</td>
    <td align="center" background="../../imagenes/tablero/centro.gif">
	<div id="resultado" style="color:#FF0000"></div>
	</td>
    <td background="../../imagenes/tablero/derecha.gif">&nbsp;</td>
  </tr>
  <tr>
    <td background="../../imagenes/tablero/izquierda2.jpg">&nbsp;</td>
    <td align="center" background="../../imagenes/tablero/centro.gif">
	<table width="95%" border="0" cellspacing="0" class="tablero">
      <tr>
        <td width="25%">Fecha</td>
        <td width="26%"><input name='txtfecha' class='boxfecha' id="txtfecha" readonly="" value=""></td>
        <td width="26%">&nbsp;</td>
        <td width="23%">&nbsp;</td>
      </tr>
      <tr>
      <td>Tipo Documento</td>
        <td><select name="txttipo" class="box1" id="txttipo">
        <option value="1" selected="selected">Cedula</option>
        <option value="2">TARJETA DE IDENTIDAD</option>
        <option value="3">PASAPORTE</option>
        <option value="4">CEDULA DE EXTRANJERIA</option>
        </select></td>
        <td>Numero</td>
        <td><input name="txtnumero" type="text" class="box1" id="txtnumero" onkeypress="return solonumeros(event)" onfocus="color1(this);" onblur="color2(this)" /></td>
        </tr>
      <tr>
        <td>Nombres</td>
        <td colspan="3"><input name="txtnombre" type="text" class="boxlargo" id="txtnombre" readonly="" ></td>
      </tr>
      <tr>
      <td>Estado</td>
      <td colspan="3"><input name='txtestado' class='boxfecha' id="txtestado" onFocus="color1(this);" onBlur="color2(this);"></td>
      </tr>
	  <tr>
      <td>Nit</td>
      <td colspan="3"><label>
      <input name="txtnit" type="text" class="box1" id="txtnit" onKeyPress="return solonumeros(event)" onFocus="color1(this);" onBlur="color2(this),buscarCedula(this);" >
      <input name="txtempresa" type="text" class="boxlargo" id="txtempresa" readonly="" />
      </label></td>
      </tr>
      <tr>
      <td>Tipo Afiliación</td>
      <td><input name='txtafiliacion' class='box1' id="txtafiliacion" onFocus="color1(this);" onBlur="color2(this);"></td>
      <td>Tipo Formulario</td>
      <td><input name='txtformulario' class='box1' id="txtformulario" onfocus="color1(this);" onblur="color2(this);" /></td>
      </tr>
      
      <tr>
      <td>Clase Aportante</td>
      <td colspan="3"><input name='txtclase' class='boxlargo' id="txtclase" onFocus="color1(this);" onBlur="color2(this);"></td>
      </tr>
      
	  <tr>
      <td>Motivo</td>
      <td colspan="3">
      <select name="motivo" id="motivo" class="boxlargo">
		<option value="0">[Seleccione]</option>
		<option value="01">01 EMPRESA HIZO APORTE EXTEMPORANEO</option>
		<option value="02">02 EMPRESA PAGO APORTE CON CEDULA ERRADA</option>
		<option value="03">03 EMPRESA MANDO HORAS INCORRECTAS</option>
		<option value="04">04 RETROACTIVIDAD</option>
		<option value="05">05 ERROR AL RETIRAR EN NOMINA</option>
		<option value="06">06 BANDERA DE NO GIRO TRABAJADOR Y/O BENEFICIARIO</option>
		<option value="07">07 ERROR FECHA DE NACIMIENTO DEL BENEFICIARIO</option>
        <option value="08">08 SISTEMA TOMO EL INGRESO BASE PARA EL GIRO</option>
		<option value="09">09 BENEFICIARIO NO MIGRADO</option>
		<option value="10">10 CARACTERES ESPECIALES EN EL NOMBRE</option>
		<option value="11">11 BENEFICIARIO DISCAPACITADO</option>
		<option value="12">12 AFILIADO INACTIVO</option>
		<option value="13">13 GIRO OCTUBRE POR NIT DIFERENTE AL QUE APORTA EL MES DE NOVIMEBRE</option>
		<option value="14">14 TIENE DOS PU</option>
		<option value="15">15 LA EMPRESA QUE APORTE SI ES LA MISA QUE LO TIENE AFILIADO</option>
		<option value="16">16 SOPORTA DOCUMENTOS A 31 DE DICIEMBRE DE 2015</option>
		<option value="17">17 EMPRESA MARCADA COMO 1429 PAGA EL 4%</option>
		<option value="99"> OTROS</option>
        </select>
        </td>
        </tr>
	  </table>
 
    <h3 style="text-align:left">Beneficiarios</h3>
    <center>
    <table width="85%" border="0" cellspacing="0" class="tablero">
    <tr>
    <th width="3%">Id</th>
    <th width="36%">Nombre</th>
    <th width="8%">Parentesco</th>
    <th width="9%">Relación</th>
    <th width="13%">Nacimiento</th>
    <th width="11%">Edad (31.12)</th>
    <th width="8%">Embargo</th>
    <th width="10%">Discapacidad</th>
	<th width="8%">Estado</th>
    <th width="12%"><span style="cursor:pointer" onclick="marcarTodo();">Autorizar</span></th>
    </tr>
    <tbody id="tbene">
    </tbody>
    </table>
    <h3 style="text-align:left">Subsidios Asignados</h3>
    <table width="100%" border="0" cellspacing="0" class="tablero" >
		<tr>
        <td width="1%"><strong>N</strong></td>
        <td width="5%"><strong>CodBen</strong></td>
        <td width="4%"><strong>Edad</strong></td>
        <td width="18%"><strong>Beneficiario</strong></td>
		<td width="67%"><strong>Estado</strong></td>
		</tr>
        <tbody id="tBeneficiarios">
        </tbody>
		</table>  
	<table width="95%" border="0">
      <tr>
        <td width="67%" ><span class="Rojo">Manual:</span><span class="celda">
          <br>
        </span>
        <ol start="1" type="1">
        <li><strong>Haga click en el bot&oacute;n Nuevo<img src="../../imagenes/iconos/nuevo.png" width="16" height="16"> </strong></li>
        <li><strong>Escriba el n&uacute;mero de identificaci&oacute;n del trabajador y presione la tecla tabulador.</strong></li>
        </ol>
        <p><span class="celda"> </span></p></td>
        <td width="33%" ><center><div id="imagen" style="display:none"><img src="../../imagenes/27-0.gif"></div></center>&nbsp;</td>
      </tr>
    </table></td>
    <td background="../../imagenes/tablero/derecha.gif">&nbsp;</td>
  </tr>
  <tr>
    <td height="38" background="../../imagenes/tablero/abajo_izq2.gif">&nbsp;</td>
    <td background="../../imagenes/tablero/abajo_central.gif"></td>
    <td background="../../imagenes/tablero/abajo_der.gif">&nbsp;</td>
  </tr>
</table>   
    </div>
</div>
</center>
<input type="hidden" id="idpersona" />
</form>
</body>
</html>