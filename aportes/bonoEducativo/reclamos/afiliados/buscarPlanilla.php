<?php
date_default_timezone_set('America/Bogota');
ini_set("display_errors",'1');
session_start();
$idp =$_REQUEST['v0'];

include_once '../../rsc/pdo/SQLDbManejador.php';
$db = SQLDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo $cadena;
	exit();
}
$filas = array();
$sql="select TOP 10 idplanilla, planilla, aportes010.nit, aportes048.razonsocial as rsocial, salariobasico, ingresobase, diascotizados, ingreso, retiro, var_tra_salario, var_per_salario, sus_tem_contrato,inc_tem_emfermedad, lic_maternidad, vacaciones, inc_tem_acc_trabajo, tipo_cotizante, periodo, correccion, aportes010.idempresa, idtrabajador, horascotizadas, procesado, control, fechapago, aportes010.fechasistema, usuariomodifica, fechamodifica
from Aportes.dbo.aportes010 
inner join Aportes.dbo.aportes048 on aportes048.nit=aportes010.nit 
where idtrabajador=$idp ORDER BY periodo desc";
$rs=$db->querySimple($sql);
if(is_null($rs)){
	echo 0;
	exit();
}
while ($row=$rs->fetch()) {
		$filas[] = $row;
	}
if(count($filas)>0)
	echo json_encode($filas);
else 
	echo 0;
?>