<?php
set_time_limit(0);
date_default_timezone_set('America/Bogota');
ini_set("display_errors",'1');

$root= $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$agencia=$_SESSION['AGENCIA'];
$usuario=$_SESSION['USUARIO'];



include_once $_SESSION['RAIZ'] . DIRECTORY_SEPARATOR . '/rsc/pdo/IFXDbManejador.php';
	$db = IFXDbManejador::conectarDB();
	if($db->conexionID==null){
		$cadena = $db->error;
		echo $cadena;
		exit();
	}
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Suspender Subsidio Escolar</title>
<link href="../../css/jquery-ui-1.9.2.custom.min.css" type="text/css" rel="stylesheet"  />
<link href="../../css/estilo.css" type="text/css" rel="stylesheet"  />
<script  src="../../js/jquery-1.8.3.js"></script>
<script  src="../../js/jquery-ui-1.9.2.custom.min.js"></script>
<script  src="../../js/funciones.js"></script>
<script  src="js/suspenderPaquete.js"></script>
</head>
<body>
<br />
<center>
<form id="suspender">
<table width="80%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="13" height="29" background="../../imagenes/tablero/arriba_izq.gif">&nbsp;</td>
    <td background="../../imagenes/tablero/arriba_central2.gif">
    <span class="letrablanca">::Suspender Subsidios Escolares &nbsp;::</span></td>
    <td width="13" background="../../imagenes/tablero/arriba_der.gif" align="right">&nbsp;</td>
  </tr>
  <tr>
    <td background="../../imagenes/tablero/izquierda2.jpg">&nbsp;</td>
    <td background="../../imagenes/tablero/centro.gif" style="text-align:left">
    <img src="../../imagenes/iconos/spacer.gif" width="1" height="1">
	<img src="../../imagenes/iconos/spacer.gif" width="1" height="1">
	<img src="../../imagenes/iconos/nuevo.png" alt="Nuevo" width="16" height="16" style="cursor:pointer" title="Nuevo">
	<img src="../../imagenes/iconos/spacer.gif" width="1" height="1">
	<!-- img src="../../imagenes/iconos/grabar.png" alt="Guardar" width="16" height="16" style="cursor:pointer" -->
	</td>
    <td background="../../imagenes/tablero/derecha.gif">&nbsp;</td>
  </tr>
  <tr>
    <td background="../../imagenes/tablero/izquierda2.jpg">&nbsp;</td>
    <td align="center" background="../../imagenes/tablero/centro.gif">
	<div id="resultado" style="color:#FF0000"></div>
	</td>
    <td background="../../imagenes/tablero/derecha.gif">&nbsp;</td>
  </tr>
  <tr>
    <td background="../../imagenes/tablero/izquierda2.jpg">&nbsp;</td>
    <td align="center" background="../../imagenes/tablero/centro.gif">
	<table width="95%" border="0" cellspacing="0" class="tablero">
      <tr>
        <td width="25%">Fecha</td>
        <td width="26%"><input name='txtfecha' class='boxfecha' id="txtfecha" readonly="" value=""></td>
        <td width="26%">&nbsp;</td>
        <td width="23%">&nbsp;</td>
      </tr>
      <tr>
      <td>Tipo Documento</td>
        <td><select name="txttipo" class="box1" id="txttipo">
        <option value="1" selected="selected">Cedula</option>
        <option value="2">TARJETA DE IDENTIDAD</option>
        <option value="3">PASAPORTE</option>
        <option value="4">CEDULA DE EXTRANJERIA</option>
        </select></td>
        <td>Numero</td>
        <td><input name="txtnumero" type="text" class="box1" id="txtnumero" onkeypress="return solonumeros(event)" onfocus="color1(this);" onblur="color2(this)" /></td>
        </tr>
      <tr>
        <td>Nombres</td>
        <td colspan="3"><input name="txtnombre" type="text" class="boxlargo" id="txtnombre" readonly="" ></td>
      </tr>
      <tr>
      <td>Estado</td>
      <td colspan="3"><input name='txtestado' class='boxfecha' id="txtestado" onFocus="color1(this);" onBlur="color2(this);"></td>
      </tr>
	  <tr>
      <td>Nit</td>
      <td colspan="3"><label>
      <input name="txtnit" type="text" class="box1" id="txtnit" onKeyPress="return solonumeros(event)" onFocus="color1(this);" onBlur="color2(this),buscarCedula(this);" >
      <input name="txtempresa" type="text" class="boxlargo" id="txtempresa" readonly="" />
      </label></td>
      </tr>
      <tr>
      <td>Tipo Afiliación</td>
      <td><input name='txtafiliacion' class='box1' id="txtafiliacion" onFocus="color1(this);" onBlur="color2(this);"></td>
      <td>Tipo Formulario</td>
      <td><input name='txtformulario' class='box1' id="txtformulario" onfocus="color1(this);" onblur="color2(this);" /></td>
      </tr>	  
	  </table>
 
    <h3 style="text-align:left">Beneficiarios Subsidio Escolar</h3>
    <center>
    <table width="80%" border="0" cellspacing="0" class="tablero" id="tbene">
    <tr>
    	<th width="1%"><strong>N</strong></th>
        <th width="6%"><strong>CedCon</strong></th>
        <th width="6%"><strong>CodBen</strong></th>
        <th width="5%"><strong>Edad</strong></th>
        <th width="40%"><strong>Beneficiario</strong></th>
		<th width="30%"><strong>Estado</strong></th>
		<th width="12%"><strong>QUITAR</strong></th>
    </tr>
    <tbody id="tBeneficiarios">
    </tbody>
    </table>      
	<table width="95%" border="0">
      <tr>
        <td width="67%" ><span class="Rojo">Manual:</span><span class="celda">
          <br>
        </span>
        <ol start="1" type="1">
        <li><strong>Haga click en el bot&oacute;n Nuevo &nbsp;<img src="../../imagenes/iconos/nuevo.png" width="16" height="16"> </strong></li>
        <li><strong>Escriba el n&uacute;mero de identificaci&oacute;n del trabajador y presione la tecla tabulador.</strong></li>        
        </ol>
        <p><span class="celda"> </span></p></td>
        <td width="33%" ><center><div id="imagen" style="display:none"><img src="../../imagenes/27-0.gif"></div></center>&nbsp;</td>
      </tr>
    </table></td>
    <td background="../../imagenes/tablero/derecha.gif">&nbsp;</td>
  </tr>
  <tr>
    <td height="38" background="../../imagenes/tablero/abajo_izq2.gif">&nbsp;</td>
    <td background="../../imagenes/tablero/abajo_central.gif"></td>
    <td background="../../imagenes/tablero/abajo_der.gif">&nbsp;</td>
  </tr>
</table>
</center>
</form>
</center>
</body>
</html>