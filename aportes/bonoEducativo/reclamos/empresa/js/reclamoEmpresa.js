// JavaScript Document
var nuevoR=0;
var continuar=0;
var idp=0;
var ide=0;
var codigos='';
var pq=0;
var flag=0;
var idbe=0;
var idempresa=0;

//JavaScript Document
//funcion para obtener cookie en javascript
function getCookie(c_name)
{
	var i,x,y,ARRcookies=document.cookie.split(";");
	for (i=0;i<ARRcookies.length;i++)
	{
			x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
			y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
			x=x.replace(/^\s+|\s+$/g,"");
			if (x==c_name)
			{
				return unescape(y);
			}// fin if
	}// fin for
	}
URL=src();

$(function(){
$("#txtPeriodo").datepicker({
        dateFormat: 'yymm',
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        onClose: function(dateText, inst) {
        	var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
            $(this).datepicker('setDate', new Date(year, month, 1));
            }
        }); 

	$("#txtPeriodo").focus(function () {
		$(".ui-datepicker-calendar").hide();
		$("#ui-datepicker-div").position({
			my: "center top",
			at: "center bottom",
			of: $(this)
		});
	});
});

$(document).ready(function(){
	$('img[alt=Nuevo]').bind('click', nuevo);
	$('img[alt=Guardar]').bind('click', guardar);
	//$('#txtnit').bind('blur', buscarEmpresa);
	$('input[type=text], select').focus(function(){
        $(this).css({background: "#C2D2FE"});
    }); 
 });


function nuevo(){
	limpiarCampos();
	nuevoR=1;
	idp=0;
	ide=0;
	idbe=0;
	//idco=0;
	$("#txtPeriodo").focus();
	$('#txtfecha').val(fechaHoyCorta());
	$('#tAfiliado').empty();
	$("#tblNoAsignado").html("");
	$("#tblAsignado").html("");
}

function buscarEmpresa(){
	$('#tAfiliado').empty();
	var nit = $("#txtnit").val().trim();
	var periodo = $("#txtPeriodo").val().trim();
	
	continuar=0;
	if(nit.length==0) return false;
	if(periodo.length==0) return false;
	
	$.ajax({
        url: "../../empresa/buscarEmpresa.php",
        async:true,
        data:{v0:nit},
        beforeSend: function(objeto){
        	dialogLoading('show');
        },
        
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }
			else 
            	if (continuar==1) buscarAfiliado();
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("Error... Pasó lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
        success: function(datos){
        	if(datos==0){
    			alert("La empresa no existe!");
    			nuevo();
    			return false;
    		}	
    		$.each(datos,function(i,f){
    			continuar=1;
    			idempresa = f.idempresa;
				$("#idempresa").val(f.idempresa);
				$("#txtempresa").val(f.razonsocial);
				claseaportante=f.claseaportante;
				
				if(claseaportante==2875){
					continuar=0;
					alert("La empresa no tiene derecho a subsidio escolar esta acogida a la ley 1429");
					nuevo();
					return true;
    			}
				
				
    			return;
    		})
    		
         },
        //timeout: 30000,
        type: "GET"
 });
}

function buscarAfiliado(){
	continuar=0;
	var periodo = $("#txtPeriodo").val().trim();
	$.ajax({
        url: "buscarAfiliado.php",
        async:true,
        data:{v0:idempresa, v1:periodo},
        beforeSend: function(objeto){
        	dialogLoading('show');
        },
        
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("Error... Pasó lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
        success: function(datos){
        	if(datos=="N"){
    			alert("La empresa no tiene Afiliados con ese periodo base!");
    			nuevo();
    			return false;
    		}
        	ct=1;
        	$.each(datos,function(i,row){
        		
						$("#tAfiliado").append("<tr id='trAfiliado'><td>"+ct+"</td><td id='tdCC"+i+"'><input type='hidden' value='"+row.idbeneficiario+"' id='hidIdBeneficiario"+i+"' /> <input type='hidden' value='"+row.idtrabajador+"' id='hidIdAfiliado"+i+"' />"+row.identificacion+"</td><td id='tdAfil"+i+"'>"+row.afil+"</td><td id='tdBene"+i+"'>"+row.bene+"</td><td>"+row.edad+"</td><td>"+row.embargo+"</td><td>"+row.discapacitado+"</td></tr>");
				ct=ct+1;
        	});
        },
        //timeout: 3000,
        type: "POST"
	});
}
function guardar(){
	var con=0;
	$("#tblNoAsignado").html("");
	$("#tblAsignado").html("");
	
	if(idempresa==0){
		alert("Falta Informacion de la empresa!");
		return false;
	}
	if($("#motivo").val()==0){
		alert("Seleccione motivo del reclamo!");
		$("#motivo").focus();
		return false;
	}
	var marcados=0;
	var idmotivo=$("#motivo").val();
	$('#tAfiliado #trAfiliado').each(function(i,row){
		
		var idbe= $("#hidIdBeneficiario"+i).val();
		var idp = $("#hidIdAfiliado"+i).val();
		
		if (idp==undefined){
		var idp=0;
		var idbe=0;	
		}
		
		$.ajax({
	        url: "../guardarReclamo.php",
	        async:false,
	        data:{v0:idp,v1:idbe,v2:idempresa,v3:idmotivo,v4:2},
	        beforeSend: function(objeto){
	        	dialogLoading('show');
	        },
	        
	        complete: function(objeto, exito){
	        	dialogLoading('close');
	            if(exito != "success"){
	                alert("No se completo el proceso!");
	            }
	        },
	        contentType: "application/x-www-form-urlencoded",
	        dataType: "json",
	        error: function(objeto, quepaso, otroobj){
	            alert("Error... Pasó lo siguiente: "+quepaso);
	        },
	        global: true,
	        ifModified: false,
	        processData:true,
	        success: function(datos){
	        	if(datos==0){
	    			//alert("No se pudo asignar el paquete de: " + idbe);
	        		$("#tblNoAsignado").append("<tr><td>"+$("#tdCC"+i).text()+"</td><td>"+$("#tdAfil"+i).text()+"</td><td>"+$("#tdBene"+i).text()+"</td></tr>");
	    			return false;
	    			con++;
	    			}	
				if(datos==9){
					//alert("El beneficiario ya tiene paquete asignado!");
					$("#tblAsignado").append("<tr><td>"+$("#tdCC"+i).text()+"</td><td>"+$("#tdAfil"+i).text()+"</td><td>"+$("#tdBene"+i).text()+"</td></tr>");
					return false;
					}
	    			marcados=marcados+1;
	           },
	        //timeout: 300000,
	        type: "GET"
			});
	 });
	if(con==0){
		$("#divResultado").show();
		$("#divResultado").dialog({
			modal:true,
			width:600,
			height:600,
			title:"Reclamo Empresa",
			buttons:{
				Aceptar:function(){
					$(this).dialog("close");
					$(this).dialog("destroy");
				}
				
			}
		});
	}
	
	alert("Subsidios asignados "+marcados+", los pueden reclamar!");
	if(marcados>0){
		var usuario=$("#txtusuario").val();
		var url="http://"+getCookie("URL_Reportes")+"/bono/rptExcel003.jsp?v0="+usuario+"&v1="+marcados;
		window.open(url,"_NEW");
	}
	
	//nuevo();
}
/*	
function buscarPaquetes(){
	pq=0;
	codigos='';
	$.ajax({
		url: "../../afiliados/b.PaqueteAfiliado.php",
	    async:true,
	    data:{v0:idp},
	    beforeSend: function(objeto){
	       	dialogLoading('show');
	    },
	    complete: function(objeto, exito){
	      	dialogLoading('close');
	        if(exito != "success"){
	    	    alert("No se completo el proceso!");
	        }
	    },
	    contentType: "application/x-www-form-urlencoded",        
		dataType: "json",
	    error: function(objeto, quepaso, otroobj){
	        alert("Fallo... Pasó lo siguiente: "+quepaso);
	    },
	    global: true,
	    ifModified: false,
	    processData:true,
	    success: function(datos){
	    	var msg;
	       	if(datos==0){
	    		alert("El afiliado no tiene paquete escolar asignado!");
	    	}	
	        $("#tBeneficiarios").empty();	
	    	$.each(datos,function(i,f){
				var nom = f.pnombre + " " + f.papellido;
				var emp = f.nit + " " + f.razonsocial;
				if(f.entregado=='S') 
					msg='PAQUETE ENTREGDO A: '+f.observaciones;
				else if(f.embargo=='S'){
	    		msg='PAQUETE EMBARGADO ';
	    		}
	    		else if(f.entregado=='N'){
	    			msg='PAQUETE PENDIENTE DE ENTREGAR';
	    			pq++;
	    			if(pq==1)
	    				codigos=f.idbeneficiario;
	    			else
	    				codigos = codigos + ',' + f.idbeneficiario;
	    		}
				$("#txtempresa").val(emp);
	    		$("#tBeneficiarios").append("<tr><td>"+(i+1)+"</td><td>"+f.identificacion+"</td><td>"+f.idbeneficiario+"</td><td>"+f.edadbeneficiario+"</td><td>"+nom+"</td><td>"+msg+"</td></tr>");
	    		return;
	    	})
	    	$('#embargo').html("Paquetes a entregar: " + pq);
	    	if(pq>0)
	    		$("#entregar").html("<label onclick='entregarPaquete()'><font color='green' size=+2 style = 'cursor:pointer'>Entregar</font></label>")
	        },
	        timeout: 3000,
	        type: "GET"
	});	
	
	}	*/