<?php
set_time_limit(0);
date_default_timezone_set('America/Bogota');
ini_set("display_errors",'1');

$root= $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
$usuario=$_SESSION['USUARIO'];
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Reclamo Afiliado</title>
<link href="../../css/jquery-ui-1.9.2.custom.min.css" type="text/css" rel="stylesheet"  />
<link href="../../css/estilo.css" type="text/css" rel="stylesheet"  />
<script  src="../../js/jquery-1.8.3.js"></script>
<script  src="../../js/jquery-ui-1.9.2.custom.min.js"></script>
<script  src="../../js/funciones.js"></script>
<script  src="js/reclamoEmpresa.js"></script>
<script>
$(function() {
     $( "#tabs" ).tabs({
     beforeLoad: function( event, ui ) {
     ui.jqXHR.error(function() {
     ui.panel.html(
     "No se pudo cargar esta pestaña. Vamos a tratar de solucionar este problema lo antes posible. Si esto no sería una demostración." );
     });
     }
     });
   });
</script>
</head>
<body>
<form id="forma">
<input type="hidden" id="txtusuario" name="txtusuario" value="<?php echo $usuario; ?>" /> 
<center>
	<div id="divResultado" style="display:none;">
		<table class="tablero" align="center" width="500">
			<tr><td colspan="3"><b>No se pudo asignar el subsidio de:</b></td></tr>
			<tr><th>C.C.</th><th>Afiliado</th><th>Beneficiario</th></tr>
			<tbody id="tblNoAsignado"></tbody>
		</table>
		<table class="tablero" align="center" width="500">
			<tr><td colspan="3"><b>Subsidio Ya asignado:</b></td></tr>
			<tr><th>C.C.</th><th>Afiliado</th><th>Beneficiario</th></tr>
			<tbody id="tblAsignado"></tbody>
		</table>
	</div>
    <table width="90%" border="0" cellspacing="0" cellpadding="0">
  	<tr>
    <td width="13" height="29" background="../../imagenes/tablero/arriba_izq.gif">&nbsp;</td>
    <td background="../../imagenes/tablero/arriba_central2.gif">
    <span class="letrablanca">::Reclamo Empresa &nbsp;::</span></td>
    <td width="13" background="../../imagenes/tablero/arriba_der.gif" align="right">&nbsp;</td>
  	</tr>
  	<tr>
    <td background="../../imagenes/tablero/izquierda2.jpg">&nbsp;</td>
    <td background="../../imagenes/tablero/centro.gif" style="text-align:left">
    <img src="../../imagenes/iconos/spacer.gif" width="1" height="1">
	<img src="../../imagenes/iconos/spacer.gif" width="1" height="1">
	<img src="../../imagenes/iconos/nuevo.png" alt="Nuevo" width="16" height="16" style="cursor:pointer" title="Nuevo">
	<img src="../../imagenes/iconos/spacer.gif" width="1" height="1">
	<img src="../../imagenes/iconos/grabar.png" alt="Guardar" width="16" height="16" style="cursor:pointer">
	</td>
    <td background="../../imagenes/tablero/derecha.gif">&nbsp;</td>
  	</tr>
  	<tr>
    <td background="../../imagenes/tablero/izquierda2.jpg">&nbsp;</td>
    <td align="center" background="../../imagenes/tablero/centro.gif">
	<div id="resultado" style="color:#FF0000"></div>
	</td>
    <td background="../../imagenes/tablero/derecha.gif">&nbsp;</td>
  </tr>
  <tr>
    <td background="../../imagenes/tablero/izquierda2.jpg">&nbsp;</td>
    <td align="center" background="../../imagenes/tablero/centro.gif">
	<table width="95%" border="0" cellspacing="0" class="tablero">
      <tr>
        <td width="25%">Fecha</td>
        <td width="26%"><input name='txtfecha' class='boxfecha' id="txtfecha" readonly="" value=""></td>
        <td width="26%">&nbsp;</td>
        <td width="23%">&nbsp;</td>
      </tr>
	  <tr>
		<td>Periodo</td>
		<td colspan="3">
		<input type="text" name="txtPeriodo" id="txtPeriodo" class="box1" onFocus="color1(this);"/>
		</td>
		</tr>
	  <tr>
	    <td>Nit</td>
	    <td colspan="3"><label>
	      <input name="txtnit" type="text" class="box1" id="txtnit" onKeyPress="return solonumeros(event)" onFocus="color1(this);" onBlur="color2(this),buscarEmpresa(this);" >
	      <input name="txtempresa" type="text" class="boxlargo" id="txtempresa" readonly="" />
	      </label></td>
	  </tr>
      <td>Motivo</td>
      <td colspan="3">
      <select name="motivo" id="motivo" class="boxlargo">
      <option value="0">[Seleccione]</option>
      <option value="01">01 EMPRESA HIZO APORTE EXTEMPORANEO</option>
	  <option value="02">02 EMPRESA PAGO APORTE CON CEDULA ERRADA</option>
	  <option value="03">03 EMPRESA MANDO HORAS INCORRECTAS</option>
		<option value="04">04 RETROACTIVIDAD</option>
		<option value="05">05 ERROR AL RETIRAR EN NOMINA</option>
		<option value="06">06 BANDERA DE NO GIRO TRABAJADOR Y/O BENEFICIARIO</option>
		<option value="07">07 ERROR FECHA DE NACIMIENTO DEL BENEFICIARIO</option>
        <option value="08">08 SISTEMA TOMO EL INGRESO BASE PARA EL GIRO</option>
        <option value="09">09 EMPRESA MARCADA COMO 1429 PAGANDO 4%</option>
		<option value="99"> OTROS</option>
        </select>
        </td>
        </tr>
	  </table>
 
    <h3 style="text-align:left">Afiliados</h3>
    <center>
    <table width="80%" border="0" cellspacing="0" class="tablero">
    <tr>
    <th >No</th>
    <th >C.C Afiliado</th>
    <th >Afiliado</th>
    <th >Beneficiario</th>
    <th >Edad</th>
    <th >Embargo</th>
    <th >Discapacitado</th>
    </tr>
    <tbody id="tAfiliado">
    </tbody>
    </table>
	<table width="95%" border="0">
      <tr>
        <td width="67%" ><span class="Rojo">Manual:</span><span class="celda">
          <br>
        </span>
        <ol start="1" type="1">
        <li><strong>Haga click en el bot&oacute;n Nuevo<img src="../../imagenes/iconos/nuevo.png" width="16" height="16"> </strong></li>
        <li><strong>Escriba el Nit de la empresa  y presione la tecla tabulador.</strong></li>
        </ol>
        <p><span class="celda"> </span></p></td>
        <td width="33%" ><center><div id="imagen" style="display:none"><img src="../../imagenes/27-0.gif"></div></center>&nbsp;</td>
      </tr>
    </table></td>
    <td background="../../imagenes/tablero/derecha.gif">&nbsp;</td>
  </tr>
  <tr>
    <td height="38" background="../../imagenes/tablero/abajo_izq2.gif">&nbsp;</td>
    <td background="../../imagenes/tablero/abajo_central.gif"></td>
    <td background="../../imagenes/tablero/abajo_der.gif">&nbsp;</td>
  </tr>
</table>   
    </div>

</center>
<input type="hidden" id="idempresa" />
</form>
</body>
</html>