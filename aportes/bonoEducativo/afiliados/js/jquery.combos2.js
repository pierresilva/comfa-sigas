// jCombo v2.0
// Carlos De Oliveira cardeol@gmail.com (c) Feb 2013
;(function ($, window, document, undefined) {
	var varDepartamento=null;
	var varCiudad=null;
	var varZona=null;
	var varBarrio=null;
	
	$.fn.jCombo = function(url, opt) {		
		var defaults = {				
				parent: null,
				first_optval : "0",
				selected_value : "0",
				initial_text: "-- Seleccione --",
				method: "GET",
				dataType: "jsonp"								
		};				
		var opt = $.extend( defaults, opt) ;
		var obj = $(this);
		if(opt.parent!=null) {
			var $parent = $(opt.parent);			
			$parent.removeAttr("disabled","disabled");
			$parent.bind('change',  function(e) {
				//obj.attr("disabled","disabled");
				if($(this).val()!=opt.first_optval) obj.removeAttr("disabled");
				__render(	obj, { 
					url: url, 
					id: $(this).val(),
					first_optval: opt.first_optval, 
					initext: opt.initial_text, 
					inival: opt.selected_value,
					method: opt.method,
					dataType: opt.dataType
				});
			});
		} else __render(obj,{ 
			url: url,
			id: "",
			first_optval: opt.first_optval,
			initext: opt.initial_text,
			inival: opt.selected_value,
			method: opt.method,
			dataType: opt.dataType
		});					
		function __render($obj,$options) {
			var response = '<option value="' + $options.first_optval + '">' + $options.initext + '</option>';
			
			if(!($options.id==null || $options.id==0)) {
				var consulta=false;
				switch ($options.url){
					case "1":
						response += fillVar(varDepartamento,$options.url,$options.id,$options.inival);
					break;
					case "2":
						response += fillVar(varCiudad,$options.url,$options.id,$options.inival);
					break;
					case "3":
						response += fillVar(varZona,$options.url,$options.id,$options.inival);
					break;
					case "4":
						response += fillVar(varBarrio,$options.url,$options.id,$options.inival);
					break;
				}
			} 
			$obj.html(response);					           										
			$obj.trigger("change");								
		}
		function fillVar (obj,opt,idparent,inival) {
			var response="";
			if(obj==null) {
				$.ajax({
					type: "GET",
					dataType: "json",					
					url: URL+"pdo/buscarCiudades.php?id=" + opt,
					async: false,
					success: function(data){
						obj = data;
						switch (opt){
							case "1":
								varDepartamento = data;
							break;
							case "2":
								varCiudad = data;
							break;
							case "3":
								varZona = data;
							break;
							case "4":
								varBarrio = data;
							break;
						}					
					}
				});
			}
			
			var selected;		
			$.each(obj,function(i,data){
				if ( data.idparent == idparent ) {
					selected = (data.idchild==inival)?' selected="selected"':'';
					response += '<option value="' + data.idchild + '"' + selected + '>' + data.child + '</option>';
				}
			});
			return response;
		}
	}
})( jQuery, window, document );