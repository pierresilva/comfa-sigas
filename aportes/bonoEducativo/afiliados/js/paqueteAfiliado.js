// JavaScript Document
var nuevoR=0;
var continuar=0;
var idper=0;
var codigos='';
var pq=0;
var saldo=0;

$(function(){
    //Para escribir solo letras
    $('#txtnotas').validarCampoOPA(' abcdefghijklmn�opqrstuvwxyz��iou1234567890');
    $('#txtnotas').bind('blur', validarCampoOPA(' abcdefghijklmn�opqrstuvwxyz��iou1234567890') );
});

function nuevo(){
	limpiarCampos();
	nuevoR=1;
	$("#txtnumero").focus();
	$('#txtfecha').val(fechaHoyCorta());
	$('#txtmesa').val($('#mesa').val());
	$("#tBeneficiarios").empty();
	$('#embargo').html("");
	$("#entregar").html("");
	$("#radioA").attr('checked', true);
	$("#radioT").attr('checked', false);
	$.getJSON('saldo.php',{v0:1},function(datos){
		$('#disponible').html(datos);
		saldo=parseInt(datos);
		if(isNaN(saldo))
		{
			saldo=0;
		}
		
	});
	}

function buscarPersona(){
	if(nuevoR==0){
		alert("Hagla clik en Nuevo");
		return false;
	}
	var num = $("#txtnumero").val();
	var idtd = $("#txttipo").val();
	var nom ="";
	continuar=0;
	num = $.trim(num);
	if(num.length==0) return false;
	$.ajax({
        url: "buscarPersona.php",
        async:true,
        data:{v0:num,v1:idtd},
        beforeSend: function(objeto){
        	dialogLoading('show');
        },
        
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }
            if (continuar==1) buscarPaquetes();
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("Pas� lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
        success: function(datos){
        	if(datos==0){
    			alert("La persona no existe!");
    			nuevo();
    			return false;
    			}	
    		$.each(datos,function(i,f){
    			continuar=1;
    			nom = f.pnombre + " " + f.snombre +" "+ f.papellido + " " + f.sapellido;
    			idper=f.idpersona;
    			return;
    			});
    		$("#txtnombres").val(nom);
    		var msg = "Entregado a: " + nom;
    		$("#txtnotas").val(msg);
           },
        timeout: 30000,
        type: "GET"
 });
}

function buscarPaquetes(){
	pq=0;
	codigos='';
	$.ajax({
		url: "b.PaqueteAfiliado.php",
	    async:true,
	    data:{v0:idper},
	    beforeSend: function(objeto){
	       	dialogLoading('show');
	    },
	    complete: function(objeto, exito){
	      	dialogLoading('close');
	        if(exito != "success"){
	    	    alert("No se completo el proceso!");
	        }
	    },
	    contentType: "application/x-www-form-urlencoded",        
		dataType: "json",
	    error: function(objeto, quepaso, otroobj){
	        alert("Fallo... Pas� lo siguiente: "+quepaso);
	    },
	    global: true,
	    ifModified: false,
	    processData:true,
	    success: function(datos){
	    	var msg;
	       	if(datos==0){
	    		alert("El afiliado no tiene paquete escolar asignado!");
	    		nuevo();
	    		return false;
	    	}
			
			/** BUSCA PERSONA **/
			var tipodoc = $("#txttipo").val();
			var doc = $("#txtnumero").val();	
	
			$.ajax({
				url: "buscarPersonaAct.php",
				cache: false,
				async:true,
				type: "POST",
				data: {v0:tipodoc,v1:doc},
				dataType:"json",
				success: function(data){
					
					$.each(data,function(i,f){	

					$("#txtDireccion").val(f.direccion);	
					$("#txtTelefono").val(f.telefono);
					$("#txtCelular").val(f.celular);	
					$("#txtEmail").val(f.email);	
					$("#cboDepto").val($.trim(f.iddepresidencia)).trigger('change');
					$("#cboCiudad").val($.trim(f.idciuresidencia)).trigger("change");
					$("#cboZona").val($.trim(f.idzona)).trigger("change");
					$("#cboBarrio").val($.trim(f.idbarrio));
					
					})
				}
			});
			
			/**INICIALIZA DIV PARA ACTUALIZAR DATOS BASICOS DE LA PERSONA **/
			newActualizaPersona($("#txttipo").val(),$("#txtnumero").val());	
			
	        $("#tBeneficiarios").empty();	
	    	$.each(datos,function(i,f){	    		
				var nom = f.pnombre + " " + f.papellido;
				var emp = f.nit + " " + f.razonsocial;
				var estilo='';
				var horaentrega=f.horaentrega;
				var fechaentrega=f.fechaentrega;
				if(horaentrega==null)
				{
					horaentrega='';	
				}		
				
				if(f.entregado=='S'){
					var ag='';
					switch (f.agencia){
						case '01':
							ag='NEIVA';
							break;
						case '02':
							ag='GARZON';
							break;
						case '03':
							ag='PITALITO';
							break;
						case '04':
							ag='LA PLATA';
							break;
					
					}					
					msg='PAQUETE ENTREGADO EL: '+ fechaentrega+" "+horaentrega.substring(0,5)+", "+f.observaciones+" en "+ag+" MESA: "+f.orden;
				} else if(f.embargo=='S'){
					msg='PAQUETE EMBARGADO ';
					estilo="style='color:#FF0000;'";
	    		}
	    		else if(f.entregado=='N'){
	    			msg='PAQUETE PENDIENTE DE ENTREGAR';
	    			pq++;
	    			if(pq==1)
	    				codigos=f.idbeneficiario;
	    			else
	    				codigos = codigos + ',' + f.idbeneficiario;
						
	    		}
				$("#txtempresa").val(emp);
	    		$("#tBeneficiarios").append("<tr "+ estilo +"><td>"+(i+1)+"</td><td>"+f.identificacion+"</td><td>"+f.idbeneficiario+"</td><td>"+f.edadbeneficiario+"</td><td>"+nom+"</td><td>"+msg+"</td></tr>");
	    		return;
	    	})
			if(pq==0){$("#dialog-actualiza-persona").dialog("close");}
	    	$('#embargo').html("Paquetes a entregar: " + pq);
			
	    	if(pq > saldo){
				$("#dialog-actualiza-persona").dialog("close");
	    		alert("No tiene paquetes disponibles para entregar, solicite al almacen!");
	    		nuevo();
	    		return false;
	    	}
	    	if(pq>0)
	    		$("#entregar").html("<label onclick='entregarPaquete()'><font color='green' size=+2 style = 'cursor:pointer'>Entregar</font></label>")
	        },
	        timeout: 30000,
	        type: "GET"
	});	
	
	}

function entregarPaquete(){
	var ob = $("#txtnotas").val();
	if( $("#txtnotas").val().length < 10){
		alert("Escriba la observci\u00F3n correspondiente, m\u00EDnimo 10 caracteres!");
		return false;
		}
	$.ajax({
        url: "descargarPaquetes.php",
        async:true,
        data:{v0:codigos,v1:ob,v2:pq,v3:idper},
        beforeSend: function(objeto){
        	dialogLoading('show');
        },
        
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("Pas� lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
        success: function(datos){
        	var msg;
        	var pq=0;
        	if(datos==0){
    			alert("Los paquetes NO fueron marcados como entregados!");
    			return false;
    			}	
        	alert("Los paquetes fueron marcados como entregados!");
        	nuevo();
			
           },
        timeout: 30000,
        type: "GET"
});	
}

function mensaje(x){
	if(x==1){
		var msg="Entregado a: "+ $("#txtnombres").val();
		document.getElementById("txtnotas").readOnly=true;
		}
	else{
		var msg="Autorizado a: ";
		document.getElementById("txtnotas").readOnly=false;
	}
	$("#txtnotas").val(msg);
}

function desactivar() {


    if($("#check:checked").val()==1) {
        $("#txtDireccion").attr('disabled', 'disabled');
		$("#txtTelefono").attr('disabled', 'disabled');
		$("#txtCelular").attr('disabled', 'disabled');
		$("#txtEmail").attr('disabled', 'disabled');
		$("#cboDepto").attr('disabled', 'disabled');
		$("#cboCiudad").attr('disabled', 'disabled');
		$("#cboZona").attr('disabled', 'disabled');
		$("#cboBarrio").attr('disabled', 'disabled');
    }
    else {
        $("#txtDireccion").removeAttr("disabled");
		$("#txtTelefono").removeAttr("disabled");
		$("#txtCelular").removeAttr("disabled");
		$("#txtEmail").removeAttr("disabled");
		$("#cboDepto").removeAttr("disabled");
		$("#cboCiudad").removeAttr("disabled");
		$("#cboZona").removeAttr("disabled");
		$("#cboBarrio").removeAttr("disabled");
	}
}
/**
 * Dialogo para la Actualizacion de persona 
 *  
 * @param tipodoc 	Select del tipo de documento
 * @param doc 		Text del documento
 */
function newActualizaPersona(tipodoc,doc){
		
	document.getElementById('check').checked=false;
	desactivar();
	$("#dialog-actualiza-persona").dialog({
		height: 440,
		width: 821,
		draggable:false,
		modal: true,
		closeOnEscape: false,
		buttons: {
			'Actualizar datos': function() {
				
				var error=0;
				var tipodoc = $("#txttipo").val();
				var doc = $("#txtnumero").val();					
				
				//AUDITORIA				
				var campos = "";				
				$.ajax({
					url: 'buscarPersonaAct.php',
					cache: false,
					type: "POST",
					async:false,
					data: {v0:tipodoc,v1:doc},	
					dataType:"json",
					success: function(data){

					$.each(data,function(i,f){
												
						$("table.tablero :input.ui-state-error,select.ui-state-error").removeClass("ui-state-error");
				if($("#check:checked").val()!=1){
						if(($("#txtTelefono").val()).length<7){
							if(($("#txtTelefono").val()).length!=''){
							$("#txtTelefono").addClass("ui-state-error");	
							error++;
							alert("Telefono - minimo 7 digitos");
							}
						}
						if(($("#txtTelefono").val())==0){
							$("#txtTelefono").addClass("ui-state-error");
							error++;
						}
						if(($("#txtCelular").val()).length>10 || ($("#txtCelular").val()).length<10){
							if(($("#txtCelular").val()).length!=''){
							$("#txtCelular").addClass("ui-state-error");	
							error++;
							alert("Celular - minimo 10 digitos");
							}
						}
						if(($("#cboDepto").val())==0){
							$("#cboDepto").addClass("ui-state-error");
							error++;
						}
						if(($("#cboCiudad").val())==0){
							$("#cboCiudad").addClass("ui-state-error");
							error++;
						}
						if(($("#cboZona").val())==0){
							$("#cboZona").addClass("ui-state-error");
							error++;
						}
						if(($("#txtDireccion").val())==0){
							$("#txtDireccion").addClass("ui-state-error");
							error++;
						}
						
						if(error>0){
							alert("Llene los campos obligatorios!");
							return false;
						}
										
						campos +='Campos(';
							
							//alert($.trim($("#txtTelefono").val()));
							//alert(f.telefono);
							if($.trim($("#txtDireccion").val())!=f.direccion)
							{
							  campos += " Direccion ";
							}
							if($.trim($("#txtTelefono").val())!=f.telefono) 
							{							 
							  campos += " Telefono ";
							}
							if($.trim($("#txtCelular").val())!=f.celular) 
							{
							  campos += " Celular ";
							}
							if($.trim($("#txtEmail").val())!=f.email) 
							{
							  campos += " Email ";
							}
							if($("#cboDepto").val()!=f.iddepresidencia) 
							{							
							 	campos += " Departamento ";
							}
							if($("#cboCiudad").val()!=f.idciuresidencia) 
							{
								campos += " Ciudad ";
							}
							if($("#cboZona").val()!=$.trim(f.idzona)) 
							{
								campos += " Zona ";
							}
							if($("#cboBarrio").val()!=f.idbarrio) 
							{
								campos += " Barrio ";
							}
							campos +=')';
					} //FIN CHECK != 1
						//var idpersona=$("#idPersona").val();
						/** VALOR DEL CHECK **/
						if($("#check:checked").val()==1){
						var idb = 'S';
						}else{
						var idb = 'N'
						}
						/** CAMPOS **/
						var direccion = $("#txtDireccion").val();
						var telefono = $("#txtTelefono").val();
						var celular = $("#txtCelular").val();
						var email = $("#txtEmail").val();
						var depto = $("#cboDepto").val();
						var ciudad = $("#cboCiudad").val();
						var zona = $("#cboZona").val();
						var barrio = $("#cboBarrio").val();
						
						var observacion=campos;
						var URLs=src();
						 $.ajax({
							   url: "guardarAuditaPersona.php",
							   type:"POST",
							   async:false,
							   data:{v0:idper,v1:observacion,v2:idb,v3:direccion,v4:telefono,v5:celular,v6:email
							   ,v7:depto,v8:ciudad,v9:zona,v10:barrio},
							   success:function(data){
								   var e = parseInt(data);
								   if(e>0){
									   alert("Datos Actualizados");
									   $("#dialog-actualiza-persona").dialog("close");
								   }else{
									   alert("Error Datos Actualizados");
								   }
							   }
					     });//ajax
						 
						 }) //$.each
					}
				});	
				// FIN AUDITORIA
				
			}	
		},
	    open: function() {
	          //Hide closing "X" for this dialog only.
	          $(this).parent().children().children("a.ui-dialog-titlebar-close").remove();
	    },
		close: function() {
			$('#dialog-actualiza-persona select,#dialog-actualiza-persona input[type=text]').val('');
			$(this).dialog("destroy");
		}
	});
}

/**MANDA EL REPORTE A EXEL**/
	function procesar01(){
	
	var usuario=$("#txtusuario").val();
	var fechaIni=$("#txtFechaI").val();
	var fechaFin=$("#txtFechaF").val();
	var usuario2=$("#txtUsuario2").val();
	
	if((fechaIni) > (fechaFin)){
		alert("Fecha inicial mayor a la final");
		return false;
	}
		url="http://10.10.1.105:8080/sigasReportes/paquete/rptExcel02.jsp?v0="+fechaIni+"&v1="+fechaFin+"&v2="+usuario+"&v3="+usuario2;
		//alert(url);
	window.open(url,"_NEW");
	
}

