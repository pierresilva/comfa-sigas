<?php

set_time_limit(0);
date_default_timezone_set('America/Bogota'); 
ini_set("display_errors",'1');

$root= $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $_SESSION['RAIZ'] . DIRECTORY_SEPARATOR . '/rsc/pdo/IFXDbManejador.php';
$arrRetorno = array("error"=>0,"data"=>null);
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	$arrRetorno["error"] = 1;
	$arrRetorno["procesados"] = 0;
	$arrRetorno["data"] = $cadena;
	exit();
}

$usuario=$_SESSION['USUARIO'];
$periodo = $_POST["periodo"];
$valor = $_POST["valor"];
$procesadosI = $_POST["procesadosI"];

if($periodo!="" && $valor != "" ){	
	$resultado = 0;
	$_SESSION["bandera"] = 1;
	$sentencia = $db->conexionID->prepare ( "EXEC [dbo].[sp_BonoEducativo]
			  @usuario = '$usuario'
			, @periodo = '$periodo'
			, @valor = '$valor'
			, @procesadosI = '$procesadosI'
			, @resultado = :resultado" );
	$sentencia->bindParam ( ":resultado", $resultado, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 4000 );
	$sentencia->execute();
	
	if ($resultado > 0){
		if($resultado == 1){
			//Bien
			$arrRetorno["error"] = 0;
			$arrRetorno["procesados"] = -1;
			$_SESSION["bandera"] = 0;
		}else{
			$arrRetorno["error"] = 0;
			$arrRetorno["procesados"] = $resultado;
			$_SESSION["bandera"] = 1;
		}		
	}else{
		//Error
		$arrRetorno["error"] = 1;
		$arrRetorno["procesados"] = 0;
	}
	
}else{
	if($_SESSION["bandera"]==0){
		$arrRetorno["error"] = 0;
		$arrRetorno["procesados"] = 0;
	}else{
		$arrRetorno["error"] = 1;
		$arrRetorno["procesados"] = 0;
	}	
	unset($_SESSION["bandera"]);
	
}

echo json_encode($arrRetorno);

?>