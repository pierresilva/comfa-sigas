/*
* @autor:      Ing. Orlando Puentes
* @fecha:      07/10/2010
* objetivo:
*/
var URL=src();
//Dialog Ayuda
function mostrarAyuda(){
	$("#ayuda").dialog('open' );
	}

function notas(){
	$("#dialog-form2").dialog('open');
	}	

function nuevaMigracion(){
	nuevo = true;
	limpiarCampos();
}

$(document).ready(function(){
	nuevo = false;
	idEmprOrigen = null;
	idEmprDestino = null;
	
	//barra de progreso init
	$("#progreso").progressbar({value:0});
	
	//control de tablas en listado de trabajadores
	h=$("body").innerHeight();
	var alto=h/2;
	
	
	$(window).scroll(function(){
	
		var pos=$(this).scrollfirst();
		
		if(pos>alto){
			$("#tableOrigenHidden").fadeIn();
			if($("#pasar").is(":visible")){
				$("#pasar").animate({first:pos+"px"},300)
			}
			
		}else{
			$("#tableOrigenHidden").fadeOut();
			$("#pasar").css("first","8px");
		}
	
	});
		
	//Dialog ayuda
	$("#ayuda").dialog({
	 	autoOpen: false,
		height: 450,
		width: 700,
		draggable:true,
		modal:false,
		open: function(evt, ui){
				$('#ayuda').html('');
				$.get('../../help/aportes/ayudaMigracion.html',function(data){
						$('#ayuda').html(data);
				});
		 }
	});

	//Dialog Colaboracion en linea
	$("#dialog-form2").dialog({
		autoOpen: false,
		height: 400,
		width: 500,
		modal: true,
		buttons: {
			'Enviar': function() {
			var bValid = true;
			var campo=$('#notas').val();
			var campo0=$.trim(campo);
			if (campo0==""){
				$(this).dialog('close');
					return false;
				}
			var campo1=$('#usuario').val();
			var campo2="radicacion.php";
			$.post(URL+'phpComunes/colaboracion.php',{v0:campo0,v1:campo1,v2:campo2},
			function(datos){
				if(datos=='1'){
					alert("Su comentario fue enviado correctamente!. Gracias por participar en el mejoramiento de SIGAS");
				}else{
					alert(datos);
				}
			});
			$(this).dialog('close');
			},
			Cancelar: function() {
				$(this).dialog('close');
				$("#dialog-form2").dialog("destroy");
			}
		}
	});
		
	//DATEPICKER
	$("#fecIngreso,#fecRetiro").datepicker();
	$("#fecIngreso").datepicker("option", "minDate", "-6M");
	
	//seleccionar todos los check
	$("#todosOrigen").live("click",function(){
		$("#tOrigen input:checkbox").attr("checked",$(this).is(":checked"));
	});
	
	//Validar si esta seleccionado almenos un check apra mostrar el boton enviar	
	$("#tOrigen input:checkbox").live("click",function(){
		//Mostrar el boton de pasar si al menos un check es seleccionado	
		if($("#tOrigen input:checkbox:not('#todosOrigen')").is(":checked")){
			$("#pasar").fadeIn();
		}else{
			$("#pasar").hide();
		}
	});	
	
});//fin ready


//Buscar Empresa
var ide=0;
function buscarEmpresa(op){
	//op=1 Trabajdor ORIGEN
	//op=2 trabajador DESTINO
	//Variables a utilizar de empresas
	$("#nitOrigen,#nitDestino").removeClass("ui-state-error");
	$("#resultado").html('');
	//ocultar boton pasar
	$("#pasar").hide();
	
    //capturar nit
	if(op==1){ 
		if($("#nitOrigen").val() == ''){
			$("#nitOrigen").addClass("ui-state-error");
			$("#nitOrigen").focus();
			return false;
		}
		$("#empresaOrigen").html('');
		nit = $("#nitOrigen").val();
	}
	if(op==2){
		if($("#nitOrigen").val()==$("#nitDestino").val()){
			$("#resultado").html("No puede migrar trabajadores a la misma empresa.").hide().fadeIn();
			$("#nitDestino").val('').focus();
			return false;
		}
		
	    if($("#nitDestino").val()==''){
			$("#nitDestino").addClass("ui-state-error");
			$("#nitDestino").focus();
			return false;
		}

	    $("#empresaDestino").html('') ;
	    nit=$("#nitDestino").val() ;
	}
		
	//------------------------Traer de la BD dato de la empresa
    $.getJSON(URL+'phpComunes/buscarEmpresa.php',{v0:nit},function(data){
       if(data==0){
         $("#resultado").html("la empresa NO existe.").hide().fadeIn();
		 return false;
       }else{
    	   $.each(data, function(i,fila){
    		   rs=fila.razonsocial;
    		   ide=fila.idempresa;
    		   if(op == 1 ){
    			   $("#empresaOrigen").html(rs);
    			   idEmprOrigen = ide;
    			   listarTrabajadores(ide,op);
    		   }else{
    			   $("#empresaDestino").html(rs);
    			   idEmprDestino = ide;
    			   listarTrabajadores(ide,op);
    		   }
    	   });
       }
    });//---------------------Fin buscar empresa json--------------------

  }//END FN BUSCAR t

	function listarTrabajadores(ide,op){
		//Buscar Trabajadores en BD.
		$.getJSON(URL+'phpComunes/buscarAfiliaciones.php',{v0:ide},function(datos){
			if(datos==0){
				$("#resultado").html("La empresa NO tiene empleados afiliados").hide().fadeIn();
				return false;
			}else{
				if(op==1){		  				 
					$("#tOrigen tr:not(':first'),#tDestino tr:not(':first')").remove();
					cargarTrabajadores(datos);
				}
				contarTrabajadores();
			}//end else
		});//json trbaajdores
	
	}


pct=0;
function cargarTrabajadores(datos){
	$("#progreso").show();
	var cuentaTrabajadores=datos.length;
   	if(cuentaTrabajadores>0){	
		for(i=0;i<cuentaTrabajadores;i=i+1){
			var tr=$("<tr id='filaO_"+datos[i].idpersona+"'></tr>");
			var td0=$("<td><input type='checkbox' id='chkO"+i+"' ></td>");
			var td1= $("<td>"+datos[i].idpersona+"</td>");
			var td2=$("<td style='text-align:left'>"+datos[i].identificacion+"</td>");
			var td3=$("<td>"+datos[i].papellido+" "+datos[i].sapellido+" "+datos[i].pnombre+" "+datos[i].snombre+"</td>");		
			var fila=tr.append(td0.add(td1).add(td2).add(td3));
			var porcion=i+1;
			pct =calcularPorcentaje(cuentaTrabajadores,porcion);	
			$("#tOrigen").append(fila);
			barraProgreso(pct);		
		}
	}
}

function calcularPorcentaje(total,porcion){
	var pct=Math.round((porcion*100)/total);
	return pct;
}

function barraProgreso(pct){
	
	$("#progreso").progressbar("value",pct);
	if(pct==100)
	$("#progreso").hide();
   }

//Pasar los trabajadores de origen a destino, esta funcion está en el boton [->] el cual esta oculto(display:hidden)
function migrarTrabajadores(){
	//if(confirm("Esta seguro de migrar los trabajadores seleccionados?")){
	$("#tOrigen td input:checked").each(function(i){
	    var row=$(this).parents("tr").attr("id");//extraigo el valor del id de la fila ej: fila1
		var aux=$("#"+row).html(); // extraigo el html de la fila ej: <td> ...</td>		       
		var id = row.replace("filaO_","");
	    $("#"+row).fadeOut(400,function(){
	    	$(this).remove();
	    	contarTrabajadores(); 
	    });
		$("#tDestino").append("<tr id='filaD_"+id+"'>"+aux+"</tr>").hide().fadeIn(900);
		$("#filaD_"+id+" td").find(":checkbox").replaceWith("<img style='margin-left:40%' src='"+URL+"/imagenes/undo.png' id='undo"+id+"' title='Devolver' onClick='devolver("+id+")'/>");
		contarTrabajadores();  
	});

	//a la ultima fila le doy la clase hightlight, y a los hermanos se las quito si la tienen...
	$("#tDestino tr:last").addClass("ui-state-highlight").siblings().removeClass("ui-state-highlight");
}//fn pasarT

//function devolver
function devolver(id){
	var row=$("#filaD_"+id).html();
	$("#filaD_"+id).remove();
	 contarTrabajadores();  
	$("#tOrigen").append("<tr id='filaO_"+id+"'>"+row+"</tr>").hide().fadeIn(900);
	$("#filaO_"+id+" td").find("img").replaceWith("<input type='checkbox' id='chkO"+id+"'>");
	contarTrabajadores();  
}

function devolverTodos(){
	if($("#tDestino tr:not(':first')").lengt==0){
		return false;
	}

	$("#tDestino tr:not(':first')").each(function(i,n){
		var row=n.id;
		$("#"+row).remove();
		contarTrabajadores();  
		$("#buscarOrigen").trigger("click");
		$("#tOrigen input:checked").attr("checked",false);
	});
}


///GUARDAR MIGRACION///
var arrNuevo=[];
function guardarMigracion(){
	
	if(nuevo == true){
		error=0;	
		$("table.tablero input:text").removeClass("ui-state-error");
		$("#tDestino").removeClass("ui-state-error");
		
		if($("#nitOrigen").val()==''){
			$("#nitOrigen").addClass("ui-state-error");
			error++;
		}
		
		if($("#fecIngreso").val()==''){
			$("#fecIngreso").addClass("ui-state-error");
			error++;
		}
		
		if($("#nitDestino").val()==''){
			$("#nitDestino").addClass("ui-state-error");
			error++;
		}	
		
		if($("#fecRetiro").val()==''){
			$("#fecRetiro").addClass("ui-state-error");
			error++;
		}
		
		if($("#nitDestino").val()==$("#nitOrigen").val()){
			$("#nitDestino").addClass("ui-state-error");
			$("#nitOrigen").addClass("ui-state-error");
			error++;
		}
		
		if(error>0) 
			return false;
		var totalFilas=$("#tDestino tr").length;
		if(totalFilas==1){
			$("#resultado").html("NO ha migrado ningun trabajador.");
			$("#bGuardar").show();
			return false;
		}
		
		if(totalFilas>1){
			trabajadores=parseInt(totalFilas)-1;
			if(confirm("Esta seguro de migrar los trabajadores("+trabajadores+") seleccionados?")){
				migrados = 0;
				noMigrados = 0;
				arregloIds = [];
				$("#tDestino tr:not(':first')").each(function(index){
					var idPersona=$(this).children("td:eq(1)").text();
					arregloIds[index] = idPersona;
				});
				var objFecha = new Date();//creo un objeto tipo fecha
				var anioActual = objFecha.getFullYear();//Obtengo el a�o actual
				objFecha.setYear(anioActual+1);
				var mesActual = objFecha.getMonth()+1;
				mesActual = (mesActual <10)?"0"+mesActual.toString():mesActual.toString();
				var diaActual = objFecha.getDate();
				diaActual = (diaActual <10)?"0"+diaActual.toString():diaActual.toString();
				var fechaEnUnAnio = objFecha.getFullYear()+"-"+mesActual+"-"+diaActual;
				//Listado de trabajadores a migrar
				$.getJSON(URL+'aportes/migrasTrabajador/migrarTrabajador.php',{
					idspersonas:arregloIds.join(),
					idempresaorigen: idEmprOrigen, 
					fecharetiro: $("#fecRetiro").val(), 
					idempresadestino: idEmprDestino,
					fechaingreso: $("#fecIngreso").val(),
					fechafidelidad: fechaEnUnAnio,
					},
					function(respuesta){
						if(respuesta.nummigrados == 0)
							alert("NO se migr\xf3 ningun trabajador.");
						else if(respuesta.nummigrados > 0)
							alert("Se migraron "+ respuesta.nummigrados +" trabajadores. \nTrabajadores NO MIGRADOS: "+ respuesta.numnomigrados);
						
						if(respuesta.numnomigrados > 0){
							respuesta.nomigrados.forEach(function(idPersona, indNoMigrados, arreglo){
								$("#filaD_"+idPersona).css("border","red solid 1px");
								$("#filaD_"+idPersona).css("color","red");
							});
						}
						nuevo = false;
					}
				);
			}else{
				$("#resultado").html("No hay trabajadores para migrar.").hide().fadeIn();
			}
		}//confirm
	}else{
		alert("Debe seleccionar primero el boton NUEVO.");
		return false;
	}
	
	
}
//limpiar campos
function limpiarCampos(){

$("#todosOrigen").attr("disabled",false);	 
$("#tOrigen tr:not(':first')").remove();	
$("#tDestino tr:not(':first')").remove();
$("table.tablero input:text").val('');
$("input:checkbox").attr("checked",false);
$("#empresaOrigen,#empresaDestino,#resultado").html("");
$("table.tablero td input:text.ui-state-error").removeClass("ui-state-error");
$("#trabajadoresOrigen,#trabajadoresDestino").empty();
$("#nitOrigen").focus();	
}//limpiar campos

function contarTrabajadores(){
	trabajadoresDestino= $("#tDestino tr:not(':first')").length;			
	trabajadoresOrigen = $("#tOrigen tr:not(':first')").length;	
	$("#trabajadoresDestino").html(trabajadoresDestino);
	$("#trabajadoresOrigen").html(trabajadoresOrigen);
}

