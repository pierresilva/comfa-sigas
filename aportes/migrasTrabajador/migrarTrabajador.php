<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'p.afiliacion.class.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

$objAfiliacion = new Afiliacion();
$idsPersonas = $_REQUEST["idspersonas"];
$arrIdsPersonas = explode(",",$idsPersonas);
$idEmpresaOrigen = $_REQUEST["idempresaorigen"];
$fechaRetiro = $_REQUEST["fecharetiro"];
$idEmpresaDestino = $_REQUEST["idempresadestino"];
$fechaIngreso = $_REQUEST["fechaingreso"];
$fechaFidelidad = $_REQUEST["fechafidelidad"];
$idCausalInactivacion = 2858; // inactivacion por proceso

$migrados = 0;
$noMigrados = 0;

$arrResultado = array("nomigrados" => array(), "nummigrados" => 0, "numnomigrados" => 0);

foreach($arrIdsPersonas as $idPersona){
	$sql = "select 
		aportes016.idpersona, 
		aportes016.idformulario, 
		aportes016.tipoformulario,
		tipoafiliacion, 
		horasdia,
		horasmes,
		agricola, 
		cargo,
		categoria,
		aportes016.fechaingreso, 
		aportes016.estado, 
		aportes016.salario, 
		aportes016.traslado,
		aportes016.codigocaja,
		aportes016.tipopago,
		aportes015.idtipodocumento, 
		aportes015.identificacion,
		aportes015.papellido, 
		aportes015.sapellido, 
		aportes015.pnombre, 
		aportes015.snombre,
		aportes091.codigo 
		from 
		aportes016 
		inner join aportes015 on aportes016.idpersona = aportes015.idpersona
		INNER JOIN aportes091 ON aportes015.idtipodocumento=aportes091.iddetalledef 
		where 
		aportes016.idempresa=$idEmpresaOrigen and 
		aportes016.estado='A' and 
		aportes016.idpersona=$idPersona";
	$result = $db->querySimple($sql);
	$afiliacionInicial = $result->fetch();
	
	// ejecutar procedimiento almacenado para inactivar la afiliación	
	/*$sql = "execute sp_inactivar {$afiliacionInicial["idformulario"]},$idCausalInactivacion,'$fechaRetiro','{$_SESSION["USUARIO"]}', 0";
	$resultInactivar = $db->querySimple($sql);*/
	$sql = "INSERT INTO aportes017(
					tipoformulario,
					tipoafiliacion,
					idempresa,
					idpersona,
					fechaingreso,
					horasdia,
					horasmes,
					salario,
					agricola,
					cargo,
					primaria,
					estado,
					fecharetiro,
					motivoretiro,
					fechanovedad,
					semanas,
					fechafidelidad,
					estadofidelidad,
					traslado,
					codigocaja,
					flag,
					tempo1,
					tempo2,
					fechasistema,
					usuario,
					tipopago,
					categoria,
					auditado,
					idagencia,
					idradicacion)
			VALUES('{$afiliacionInicial["tipoformulario"]}',
					'{$afiliacionInicial["tipoafiliacion"]}',
					'$idEmpresaOrigen',
					'{$afiliacionInicial["idpersona"]}',
					'{$afiliacionInicial["fechaingreso"]}',
					'{$afiliacionInicial["horasdia"]}',
					'{$afiliacionInicial["horasmes"]}',
					'{$afiliacionInicial["salario"]}',
					'{$afiliacionInicial["agricola"]}',
					'{$afiliacionInicial["cargo"]}',
					'N',
					'I',
					'$fechaRetiro',
					'$idCausalInactivacion',
					'{$afiliacionInicial["fechanovedad"]}',
					'{$afiliacionInicial["semanas"]}',
					'$fechaFidelidad',
					'A',
					'{$afiliacionInicial["traslado"]}',
					'{$afiliacionInicial["codigocaja"]}',
					'{$afiliacionInicial["flag"]}',
					'{$afiliacionInicial["tempo1"]}',
					'{$afiliacionInicial["tempo2"]}',
					cast(getdate() as date),
					'{$_SESSION["USUARIO"]}',
					'{$afiliacionInicial["tipopago"]}',
					'{$afiliacionInicial["categoria"]}',
					'{$afiliacionInicial["auditado"]}',
					'{$afiliacionInicial["idagencia"]}',
					'{$afiliacionInicial["idradicacion"]}')";
	$resultInactivar = $db->queryActualiza($sql);
	
	if($resultInactivar==0){		
		// "NO SE pudo inactivar la afiliacion, consulte con el administrador";
		$noMigrados++;
		$arrResultado["nomigrados"][] = $idPersona;
		$arrResultado["numnomigrados"] = $noMigrados;
	} else if($resultInactivar==1) {	

		$sql = "DELETE FROM aportes016 WHERE idformulario = {$afiliacionInicial["idformulario"]}";
		$resultBorrar = $db->queryActualiza($sql);
		
		$estado = 'A';
			$primaria = 'S';
			$idRadicacion = NULL;
			
			$sql="insert into aportes016 (
					tipoformulario,
					tipoafiliacion,
					idempresa, 
					idpersona,
					fechaingreso,
					horasdia,
					horasmes,
					salario,
					agricola, 
					cargo,
					traslado,
					codigocaja,
					categoria,
					usuario,
					tipopago,
					primaria,
					estado,
					fechasistema,
					auditado,
					idagencia,
					idradicacion)
				values (:tipoformulario, :tipoafiliacion, :idempresa, :idpersona, :fechaingreso, :horasdia, :horasmes, :salario, :agricola, :cargo, :traslado, :codigocaja, :categoria, :usuario, :tipopago, :primaria, :estado, cast(getdate() as date),'N', :idagencia, :idradicacion)";
			
			$statement = $db->conexionID->prepare($sql);
			$guardada = false;
			$statement->bindParam(":tipoformulario", $afiliacionInicial["tipoformulario"], PDO::PARAM_INT);
			$statement->bindParam(":tipoafiliacion", $afiliacionInicial["tipoafiliacion"], PDO::PARAM_INT);
			$statement->bindParam(":idempresa", $idEmpresaDestino, PDO::PARAM_INT);
			$statement->bindParam(":idpersona", $idPersona, PDO::PARAM_INT);
			$statement->bindParam(":fechaingreso", $fechaIngreso, PDO::PARAM_STR);
			$statement->bindParam(":horasdia", $afiliacionInicial["horasdia"], PDO::PARAM_STR);
			$statement->bindParam(":horasmes", $afiliacionInicial["horasmes"], PDO::PARAM_STR);
			$statement->bindParam(":salario", $afiliacionInicial["salario"], PDO::PARAM_INT);
			$statement->bindParam(":agricola", $afiliacionInicial["agricola"], PDO::PARAM_STR);
			$statement->bindParam(":cargo", $afiliacionInicial["cargo"], PDO::PARAM_INT);
			$statement->bindParam(":categoria", $afiliacionInicial["categoria"], PDO::PARAM_STR);
			$statement->bindParam(":traslado", $afiliacionInicial["traslado"], PDO::PARAM_STR);
			$statement->bindParam(":codigocaja", $afiliacionInicial["codigocaja"], PDO::PARAM_STR);
			$statement->bindParam(":tipopago",  $afiliacionInicial["tipopago"], PDO::PARAM_STR);
			$statement->bindParam(":usuario", $_SESSION['USUARIO'], PDO::PARAM_STR);
			$statement->bindParam(":primaria", $primaria, PDO::PARAM_STR);
			$statement->bindParam(":estado", $estado, PDO::PARAM_STR);
			$statement->bindParam(":idagencia", $_SESSION['AGENCIA'], PDO::PARAM_STR);
			$statement->bindParam(":idradicacion", $idRadicacion, PDO::PARAM_INT);
			$guardada = $statement->execute();
			if($guardada){
				// buscar id de la empresa creada
				$resultNuevaAfi = $db->conexionID->lastInsertId();
				$migrados++;
				$arrResultado["nummigrados"] = $migrados;
			}else{
				// errores
				$error = $statement->errorInfo();
				//echo "Error codigo {$error[0]} con el mensaje >>> {$error[2]}";
				$noMigrados++;
				$arrResultado["nomigrados"][] = $idPersona;
				$arrResultado["numnomigrados"] = $noMigrados;
			}
	}
	
}
print_r(json_encode($arrResultado));
die();
?>