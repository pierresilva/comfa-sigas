/*
 * @autor:      Ing. Orlando Puentes
 * @fecha:      07/10/2010
 * objetivo:
 */
var URL=src();
var ide =0;
var idn=0;
var nuevoR=0;
var contar=0;
var codigoDefinicion='';

$(document).ready(function(){
//	tooltip para el link de descarga
	$("#txtFechaPago").datepicker( {
		changeMonth: true,
		changeYear: true,
		minDate: "-60Y",
		maxDate: "1D"
	} );
	$(".ui-datepicker").css("z-index",3000);
	$("#txtFechaPago").bind("change", function(){
		$("#txtFechaPago").focus();
	});	
	$("#descargar a").hover(function(){
		$("#tooltip").sfirst().animate({opacity: "show", first: "90"}, "slow"); 
	},function(){
		$("#tooltip").sfirst().animate({opacity: "hide", first: "100"}, "slow"); 		
	});
	$("#txtNit").bind("blur", buscarEmpresa);

//	Dialog ayuda
	$("#ayuda").dialog({
		autoOpen: false,
		height: 450,
		width: 700,
		draggable:true,
		modal:false,
		open: function(evt, ui){
			$('#ayuda').html('');
			$.get(URL +'help/aportes/AyudaNominaManual.html',function(data){
				$('#ayuda').html(data);
			});
		}
	});


	$("table.tablaR tr:even").addClass("zebra");
	$("#txtPeriodo").datepicker({
		dateFormat: 'yy-mm',
		changeMonth: true,
		changeYear: true,
		showButtonPanel: true,
		onClose: function(dateText, inst) { 
			var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
			var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
			$(this).datepicker('setDate', new Date(year, month, 1));
		}
	});

	$("#txtPeriodo").focus(function () {
		$(".ui-datepicker-calendar").hide();
		$("#ui-datepicker-div").position({
			my: "center top",
			at: "center bottom",
			of: $(this)
		});
	});


});//fin ready

//-----------------Nuevo
function nuevaNomina(){
	camposLimpiar();
}

//---Limpiar Campos
function camposLimpiar(){
	camposLimpiarAccion();
	$("#txtNit").focus();	
}

function camposLimpiarAccion(){
	ide=0;
	contar=0;
	
	$("#ajax").html("");
	$("#descargar").hide();
	$("#txtDiasMora").val('0');
	$("#txtValorMora").val('0');
	$("#codigoDefinicion").val('');
	$("table.tablero input:text").val('');
	$("table.tablero td input:text.ui-state-error").removeClass("ui-state-error");
	
	$("#listarT").hide();
	$("#divlistaTable").hide();
	$("#linkQuitarNoCheckEmpleados").hide();
	$("#listaTable tr:not(:first)").remove();
	
	$("#agregarT").hide();
	$("#nuevoTable").hide();
	$("#eliminarUltimo").hide();
	$("#nuevoTable tr:not(:first)").remove();
}

function buscarEmpresa(){
	var nit = $.trim($("#txtNit").val());
	camposLimpiarAccion();
	$("#txtNit").val(nit);
	var dondefocus="email";

	if(nit.length == 0) return false
	$.ajax({
		url: "buscarEmpresa.php",
		async:true,
		data:{v0:nit,v1:ide,V2:1},
		beforeSend: function(objeto){
			dialogLoading('show');
			$("#listarT").hide();
			$("#agregarT").hide();
		},
		complete: function(objeto, exito){
			dialogLoading('close');
			if(exito != "success"){
				alert("No se completo el proceso!");
			}
			$("#" + dondefocus).focus();
		},
		contentType: "application/x-www-form-urlencoded",
		dataType: "json",
		error: function(objeto, quepaso, otroobj){
			alert("Pas� lo siguiente: "+quepaso);
		},
		global: true,
		ifModified: false,
		processData:true,
		success: function(datos){
			if(datos==0){
				alert("NO existe informacion de la empresa!");
				$("#div-boton").css("display","none");
				camposLimpiar();
				dondefocus="txtNit";
				return false;
			} else {
				var rz = datos.idempresa+' - ' + datos.razonsocial;
				$("#txtRazon").val(rz);            	
				ide=datos.idempresa;
				$("#listarT").show();
				$("#agregarT").show();
			}
		},
		timeout: 30000,
		type: "GET"
	});

} 

function listarAfiliados(){
	/*$("#nuevoTable").hide();
	$("#eliminarUltimo").hide();
	$("#nuevoTable tr:not(:first)").remove();*/

	if(ide == 0) return false;
	$.ajax({
		url: "ListarAfiliados.php",
		async:true,
		data:{v0:ide},
		beforeSend: function(objeto){
			dialogLoading('show');
			$("#listaTable tr:not(':first')").remove();
		},
		complete: function(objeto, exito){
			dialogLoading('close');
			$("#listaTable tr:even").addClass("zebra");
			if(exito != "success"){
				alert("No se completo el proceso!");
			}
		},
		contentType: "application/x-www-form-urlencoded",
		dataType: "json",
		error: function(objeto, quepaso, otroobj){
			alert("Pas� lo siguiente: "+quepaso);
		},
		global: true,
		ifModified: false,
		processData:true,
		success: function(datos){
			if(datos==0){
				alert("NO existen afiliados por esta empresa!");
				$("#div-boton").css("display","none");
				//camposLimpiar();
				return false;
			}	

			//**************
			var i=1;
			$.each(datos,function(i,fila){
				$("#listaTable").append("<tr id='row"+i+"'><td>"+(i+1)+"</td><td style='text-align:left'><input type=text id='idtd"+i+"' class=boxcorto value="+fila.codigo+"></td> <td style='text-align:right'><input type=text id='cc"+i+"' class=box1 value="+fila.identificacion+"></td><td style='text-align:right'><input type=text id='apep"+i+"' class=box1 value="+fila.papellido+"></td><td style='text-align:left'><input type=text id='apes"+i+"' class=box1 value="+fila.sapellido+"></td><td style='text-align:left'><input type=text id='nomp"+i+"' class=box1 value="+fila.pnombre+"></td><td style='text-align:left'><input type=text id='noms"+i+"' class=box1 value="+fila.snombre+"></td><td><input type=text class=boxcorto id='dias"+i+"' value=30></td><td style='text-align:right'><input type=text class=box1 id='salario"+i+"' value="+fila.salario+"></td><td><input type='checkbox' id='chkI"+i+"'></td><td><input type='checkbox' id='chkR"+i+"'></td><td><input type='checkbox' id='chkL"+i+"'></td><td><input type='checkbox' id='chkM"+i+"'></td><td><input type='checkbox' id='chkV"+i+"'></td><td><img src=../../imagenes/borrar.png onClick=borrarFila('row"+i+"');> </tr>");
				//</td> 
			});

			$("#divlistaTable").show();
			$("#linkQuitarNoCheckEmpleados").show();
		},
		timeout: 30000,
		type: "GET"
	});
}

function QuitarNoCheckEmpleados(){
	$("#listaTable tr").removeClass("0k");
	$("#listaTable input[id^=chk]:checked").parent().parent().addClass("0k");
	$("#listaTable tr").removeClass("ui-state-error");
	$("#listaTable tr:not(:first):not(.0k)").addClass("ui-state-error");

	var cantidadErrores = $("#listaTable tr:not(:first):not(.0k)").length;

	if(cantidadErrores>0){
		if(confirm("Deseas eliminar " + cantidadErrores + " filas sin checkear?")==true){
			$("#listaTable tr:not(:first):not(.0k)").remove();

			if($("#listaTable tr:not(:first)").length==0){
				$("#divlistaTable").hide();
				$("#linkQuitarNoCheckEmpleados").hide();
			}

			var tds=$("#listaTable tr:not(:first) td:first-child");
			var contadorFilas=1;
			$.each(tds,function(i,fila){
				$(this).text(contadorFilas);
				contadorFilas++;		        
			});
		}
	}
}

//borrar una fila
function borrarFila(id){
	if(confirm("Esta segura de borrar la fila No " + $("#listaTable #" + id + " td:first").text())){
		$("#listaTable #"+id).remove();

		if($("#listaTable tr:not(:first)").length==0){
			$("#divlistaTable").hide();
			$("#linkQuitarNoCheckEmpleados").hide();	    	
		}

		var tds=$("#listaTable tr:not(:first) td:first-child");
		var contadorFilas=1;
		$.each(tds,function(i,fila){
			$(this).text(contadorFilas);
			contadorFilas++;		        
		});
	}
}


//--------------Agregar Nuevo Trabajador

function agregarNuevo(){
	/*$("#divlistaTable").hide();
	$("#linkQuitarNoCheckEmpleados").hide();
	$("#listaTable tr:not(:first)").remove();*/

	$("#eliminarUltimo").show();
	//Mostrar la fila de los trabajadores a adicionar
	contar=$("#nuevoTable tr:not(:first)").length+1;
	$("#nuevoTable").show().append("<tr>" +
			"<td>"+contar+"</td>" +
			"<td><input type='text' maxlength=2 id='tpd2"+contar+"'/></td>" +
			"<td><input type='text' maxlegth=12 id='cedula2"+contar+"' onblur='buscarPersona(this,"+contar+")' /></td>" +
			"<td><input type='text' maxlegth=30 id='papellido2"+contar+"' /></td>" +
			"<td><input type='text' maxlegth=30 id='sapellido2"+contar+"' /></td>" +
			"<td><input type='text' maxlegth=30 id='pnombre2"+contar+"' /></td>" +
			"<td><input type='text' maxlegth=30 id='snombre2"+contar+"'/></td>" +
			"<td><input type='text' maxlegth=2 id='dias2"+contar+"' onblur='validarDias(this);'/></td>" +
			"<td><input type='text' maxlegth=12  id='sueldo2"+contar+"'/></td>" +
			"<td><input type='checkbox' id='chknI2"+contar+"'></td>" +
			"<td><input type='checkbox' id='chknR2"+contar+"'></td>" +
			"<td><input type='checkbox' id='chknL2"+contar+"'></td>" +
			"<td><input type='checkbox' id='chknM2"+contar+"'></td></tr>");
	
	$("#nuevoTable tr:last").show("highlight",1000);
	$("#nuevoTable tr input:text").addClass("ui-state-default");
	$("#nuevoTable tr input:text[id^='tpd']").addClass("boxcorto");
	$("#nuevoTable tr input:text[id^='tpd']").val("CC");
	$("#nuevoTable tr input:text[id^='dias']").addClass("boxcorto");
	$("#nuevoTable tr input:text[id^='sueldo']").addClass("boxfecha");
	$("#nuevoTable tr input:text[id^='cedula']").addClass("boxfecha");
	$("#nuevoTable tr input:text[id^='cedula']").focus();
}

function eliminarUltimo(){
	$("#nuevoTable tr:last").remove();

	if($("#nuevoTable tr:not(:first)").length==0){
		$("#eliminarUltimo").hide();
		$("#nuevoTable").hide();
	}
}

function buscarPersona(obj,j){
	var numero=obj.value;
	if($("#cedula2"+j).val()=='')return false;
	var idtd=1;
	$.ajax({
		url: "buscarPersona.php",
		async:true,
		data:{v0:idtd,v1:numero},
		beforeSend: function(objeto){
			dialogLoading('show');
		},
		complete: function(objeto, exito){
			dialogLoading('close');
			if(exito != "success"){
				alert("No se completo el proceso!");
			}
		},
		contentType: "application/x-www-form-urlencoded",
		dataType: "json",
		error: function(objeto, quepaso, otroobj){
			alert("Pas� lo siguiente: "+quepaso);
		},
		global: true,
		ifModified: false,
		processData:true,
		success: function(datos){
			if(datos==0){
				alert("NO existe informacion de la persona!");
				$("#div-boton").css("display","none");
				return false;
			}	
			$("#pnombre2"+j).val(datos.pnombre);
			$("#snombre2"+j).val(datos.snombre);
			$("#papellido2"+j).val(datos.papellido);
			$("#sapellido2"+j).val(datos.sapellido);
			$("#dias2"+j).focus();
		},
		timeout: 30000,
		type: "GET"
	});	
}

function validarDias(obj){
	if(obj.value>30){
		alert("Los dias no pueden ser mayor a 30!");
		obj.value='';
		obj.focus();
	}
}

function guardarNomina(){
	$("#div-botones").css("display","none");
	var error = validarCampos();
	if(error > 0) {
		$("#div-botones").css("display","block");
		return false;
	}
	guardarDatos();
	if(idn == 0) return false;
	if($("#listaTable tr:not(':first')").length>0){ guardarAntiguos(); }
	if($("#nuevoTable tr:not(':first')").length>0){ guardarNuevos(); }
	generarPlano();
}

function guardarDatos(){
	var nit=$("#txtNit").val();
	var email=/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test($("#email").val());
	var fechaPago   =$("#txtFechaPago").val();
	var nomina      =$("#txtNomina").val();
	var aporte      =$("#txtCancelado").val();
	var recibo      =$("#txtRecibo").val();
	var porcentaje     =$("#porcentaje").val();
	var periodo     =$("#txtPeriodo").val();
	var diasMora    =$("#txtDiasMora").val();
	var valorMora   =$("#txtValorMora").val();
	var nroEmpleados = $("#nroEmpleados").val();
	$.ajax({
		url: "guardarNomina.php",
		async:false,
		data:{v0:nit,v1:email,v2:fechaPago,v3:nomina,v4:aporte,v5:recibo,v6:porcentaje,v7:periodo,v8:diasMora,v9:valorMora,v10:nroEmpleados},
		beforeSend: function(objeto){
			dialogLoading('show');
		},
		complete: function(objeto, exito){
			dialogLoading('close');
			if(exito != "success"){
				alert("No se completo el proceso!");
			}
		},
		contentType: "application/x-www-form-urlencoded",
		dataType: "json",
		error: function(objeto, quepaso, otroobj){
			alert("Pas� lo siguiente: "+quepaso);
			$("#div-botones").css("display","block");
		},
		global: true,
		ifModified: false,
		processData:true,
		success: function(datos){
			if(datos==0){
				alert("No se pudo Grabar la Nomina!");
				$("#div-botones").css("display","block");
				return false;
			}	
			idn = datos;
			alert("Se creo la NOMINA manual Numero: " + idn);
		},
		timeout: 30000,
		type: "GET"
	});	
}

function validarCampos(){
	var error=0;
	$("table.tablero :input.ui-state-error,select.ui-state-error").removeClass("ui-state-error");
	error=validarSelect($("#txtNit"),error);
	error=validarTexto($("#txtFechaPago"),error);
	error=validarTexto($("#txtRecibo"),error);
	error=validarTexto($("#txtNomina"),error);
	error=validarTexto($("#txtCancelado"),error);
	error=validarTexto($("#txtPeriodo"),error);
	error=validarTexto($("#txtDiasMora"),error);
	error=validarTexto($("#txtValorMora"),error);
	error=validarTexto($("#nroEmpleados"),error);
	
	if(error==0 && ($("#listaTable tr:not(':first')").length==0 && $("#nuevoTable tr:not(':first')").length==0)){ 
		alert('Debe tener al menos un trabajador listado o agregado');
		error++;
	} else if(error==0){
		$("#nuevoTable tr:not(':first')").each(function(j){
			var i=parseInt(j + 1);
			error=validarTexto($("#tpd2"+i),error);
			error=validarTexto($("#cedula2"+i),error);
			error=validarTexto($("#papellido2"+i),error);
			error=validarTexto($("#pnombre2"+i),error);
			error=validarTexto($("#dias2"+i),error);
			error=validarTexto($("#sueldo2"+i),error);
		});
	}
	
	return error;
}

function guardarAntiguos(){
	var ced;
	var papellido;
	var sapellido;
	var pnombre;
	var snombre;
	var dias;
	var sueldo;
	var j;
	var r;
	var l;
	var m;
	var td;

	$("#listaTable tr:not(':first')").each(function(index){
		i=(this.id).substring(3);
		ced=$("#cc"+i).val();
		papellido=$("#apep"+i).val();
		sapellido=$("#apes"+i).val();
		pnombre=$("#nomp"+i).val();
		snombre=$("#noms"+i).val();
		dias=$("#dias"+i).val();
		sueldo=$("#salario"+i).val();
		j=($("#chkI"+i).is(":checked")? 'X' :'' );
		r=($("#chkR"+i).is(":checked")? 'X' :'' );
		l=($("#chkL"+i).is(":checked")? 'X' :'' );
		m=($("#chkM"+i).is(":checked")? 'X' :'' );
		td=$("#idtd"+i).val();
		guardarAfiliado(ced,papellido,sapellido,pnombre,snombre,dias,sueldo,j,r,l,m,td);
		ced='';
		papellido='';
		sapellido='';
		pnombre='';
		snombre='';
		dias='';
		sueldo='';
		j='';
		r='';
		l='';
		m='';
		td='';
	});
}

function guardarNuevos(){
	var ced;
	var papellido;
	var sapellido;
	var pnombre;
	var snombre;
	var dias;
	var sueldo;
	var j;
	var r;
	var l;
	var m;
	var td;


	$("#nuevoTable tr:not(':first')").each(function(j){
		var i=parseInt(j + 1);
		ced=$("#cedula2"+i).val();
		papellido=$("#papellido2"+i).val();
		sapellido=$("#sapellido2"+i).val();
		pnombre=$("#pnombre2"+i).val();
		snombre=$("#snombre2"+i).val();
		dias=$("#dias2"+i).val();
		sueldo=$("#sueldo2"+i).val();
		j=($("#chknI2"+i).is(":checked")? 'X' :'' );
		r=($("#chknR2"+i).is(":checked")? 'X' :'' );
		l=($("#chknL2"+i).is(":checked")? 'X' :'' );
		m=($("#chknM2"+i).is(":checked")? 'X' :'' );
		td=$("#tpd2"+i).val();
		guardarAfiliado(ced,papellido,sapellido,pnombre,snombre,dias,sueldo,j,r,l,m,td);
		ced='';
		papellido='';
		sapellido='';
		pnombre='';
		snombre='';
		dias='';
		sueldo='';
		j='';
		r='';
		l='';
		m='';
		td='';
	});

}
function  guardarAfiliado(ced,papellido,sapellido,pnombre,snombre,dias,sueldo,j,r,l,m,td){
	$.ajax({
		url: "guardarAfiliado.php",
		async:false,
		data:{v0:idn,v1:td,v2:ced,v3:papellido,v4:sapellido,v5:pnombre,v6:snombre,v7:dias,v8:sueldo,v9:j,v10:r,v11:l,v12:m},
		beforeSend: function(objeto){
			dialogLoading('show');
		},
		complete: function(objeto, exito){
			dialogLoading('close');
			if(exito != "success"){
				alert("No se completo el proceso!");
				$("#div-botones").css("display","block");
			}
		},
		contentType: "application/x-www-form-urlencoded",
		dataType: "json",
		error: function(objeto, quepaso, otroobj){
			alert("Pas� lo siguiente: "+quepaso);
		},
		global: true,
		ifModified: false,
		processData:true,
		success: function(datos){
			if(datos==0){
				alert("No se pudo Grabar la Afiliacion!");
				$("#div-botones").css("display","block");
				return false;
			}	
		},
		timeout: 30000,
		type: "GET"
	});	
}

function generarPlano(){
	$.ajax({
		url: "generarPlano.php",
		async:true,
		data:{v0:idn},
		beforeSend: function(objeto){
			dialogLoading('show');
			$("#agregarT").hide();
		},
		complete: function(objeto, exito){
			dialogLoading('close');
			if(exito != "success"){
				alert("No se completo el proceso!");
				$("#div-botones").css("display","block");
			}
		},
		contentType: "application/x-www-form-urlencoded",
		dataType: "json",
		error: function(objeto, quepaso, otroobj){
			alert("Pas� lo siguiente: "+quepaso);
		},
		global: true,
		ifModified: false,
		processData:true,
		success: function(datos){
			if(datos==0){
				alert("NO se pudo generar la PLANILLA UNICA!");
				$("#div-botones").css("display","block");
				return false;
			}	
			alert("Proceso terminado con Exito!, puede descargar el archivo");
			$("#ajax").html("<label style='cursor:pointer' onClick='descargar();'><img src=../../imagenes/descargarArchivo.png width=143 height=30 /></label><br>");
			$("#ajax").show();
			$("#div-botones").css("display","block");

		},
		timeout: 30000,
		type: "GET"
	});

}

function descargar(){
	var url=URL+'phpComunes/descargaPlanos.php';
	window.open(url);
}