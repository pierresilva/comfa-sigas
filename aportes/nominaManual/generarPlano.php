<?php
// @autor:		Orlando Puentes A
// @fecha:		mayo 31/2010
// @formulario:	guardar datos persona - modulo empresa
ini_set("display_errors","1");
date_default_timezone_set('America/Bogota');
$idn =$_REQUEST['v0'];
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
$raiz = $_SESSION['RAIZ'];
include_once $raiz.DIRECTORY_SEPARATOR.'config.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo $cadena;
	exit();
}

$planilla='ccf'.$idn; 

$sql="select aportes080.nit,aportes080.email,aportes080.fechapago,aportes080.valornomina,aportes080.aporte,aportes080.porcentaje,documento,direccion,
aportes080.periodo,aportes080.diasmora,	aportes080.valormora,aportes080.empleados,aportes048.digito,aportes048.razonsocial
from aportes080 inner join aportes048 on aportes080.nit = aportes048.nit where idnomina=$idn";
$row = $db->querySimple($sql)->fetch();
/////ENCABEZADO

$ruta=$ruta_cargados."planillas".DIRECTORY_SEPARATOR."cargados".DIRECTORY_SEPARATOR;

$fecha=date("Y-m-d");
$nombreArchivo=$fecha ."_Manual_NI_".$row['nit']."_CCF_".$row['documento']."_I_".$row['periodo'].".txt";
$archivo=$ruta.$nombreArchivo;
$fileHandle = fopen($archivo, 'w') ;
fclose($fileHandle);

$campo01 = "00000";
$campo02 = "1";
$campo03 = "05";
$campo04 = str_pad("891180008",16);
$campo05 = "2";
$campo06 = str_pad($row['razonsocial'],200);
$campo07 = "NI";
$campo08 = str_pad($row['nit'],16);
$campo09 = "0";
$campo10 = " ";
$campo11 =str_pad($row['direccion'],40);
$campo12 = "001";
$campo13 = "41";
$campo14 = "0000000000";
$campo15 = "0000000000";
$campo16 = str_pad(' ',60);
$campo17 = $row['periodo'];
$campo18 = "E";
$campo19 = $fecha;
$campo20 = $row['fechapago']; //$fecha;
$campo21 = str_pad($planilla,10);
$campo22 = str_pad(0,10);
$campo23 = 0;
$campo24 = str_pad(0,10);
$campo25 = str_pad(0,40);
$campo26 = str_pad($row['empleados'],5);
$campo27 = str_pad($row['empleados'],5);
$campo28 = '32';
$campo29 = '0';
$campo30 = str_pad($row['diasmora'],4);
$campo31 = str_pad($row['empleados'],8);

$cadena=$campo01.$campo02.$campo03.$campo04.$campo05.$campo06.$campo07.$campo08.$campo09.$campo10.$campo11.$campo12.$campo13.$campo14.$campo15.$campo16.$campo17.$campo18.$campo19.$campo20.$campo21.$campo22.$campo23.$campo24.$campo25.$campo26.$campo27.$campo28.$campo29.$campo30.$campo31."\r\n";
$fileHandle = fopen($archivo, 'a');
fwrite($fileHandle, $cadena);
fclose($fileHandle);

$diasmora=$row['diasmora'];
$valormora=$row['valormora'];
//DETALLE
$sql="select * from aportes081 where idnomina=$idn";
$rs=$db->querySimple($sql);
$con=0;
$suma_ibc = 0;
$aporte_obli = 0;
while($row=$rs->fetch()){
	$row = array_map("utf8_decode", $row);
    $con++;
    $camp01 =str_pad($con,5,0,STR_PAD_LEFT);
    $camp02 ="2";
    $camp03 =$row['tipoidentificacion'];
    $camp04 =str_pad($row['identificacion'],16," ",STR_PAD_LEFT);
    $camp05 ="00";
    $camp06 ="00";
    $camp07 =" ";
    $camp08 =" ";
    $camp09 ="41";
    $camp10 ="001";
    $camp11 =str_pad($row['papellido'],20);
    $camp12 =str_pad($row['sapellido'],30);
    $camp13 =str_pad($row['pnombre'],20);
    $camp14 =str_pad($row['snombre'],30);
    $camp15 =str_pad($row['ingreso'],1);
    $camp16 =str_pad($row['retiro'],1);
    $camp17 =" ";
    $camp18 =" ";
    $camp19 =" ";
    $camp20 =str_pad($row['licencia'],1);
    $camp21 =str_pad($row['maternidad'],1);
    $camp22 =" ";
    $camp23 ="00";
    $camp24 =str_pad($row['dias'],2,"0");
    $camp25 =str_pad($row['salario'],9);
    $ibc=round((intval($row['salario'])/30)*intval($row['dias']),0);
    $camp26 =str_pad($ibc,9);
    $suma_ibc=$suma_ibc+$ibc;
    $camp27 =str_pad(0,7);
    $aporte=ceil(0.04*$ibc);
    $camp28 =str_pad($aporte,9);
    $aporte_obli=$aporte_obli+$aporte;
    $camp29 =" ";
    $camp30 =" ";

    $cadena=$camp01.$camp02.$camp03.$camp04.$camp05.$camp06.$camp07.$camp08.$camp09.$camp10.$camp11.$camp12.$camp13.$camp14.$camp15.$camp16.$camp17.$camp18.$camp19.$camp20.$camp21.$camp22.$camp23.$camp24.$camp25.$camp26.$camp27.$camp28.$camp29.$camp30."\r\n";
    $fileHandle = fopen($archivo, 'a');
    fwrite($fileHandle, $cadena);
    fclose($fileHandle);
}
///// REGLON 31 APORTES

$camp01="00031";
$camp02="3";
$camp03 = str_pad($suma_ibc,10,"0",STR_PAD_LEFT);
$camp04=str_pad($aporte_obli,10,"0",STR_PAD_LEFT);
$cadena=$camp01.$camp02.$camp03.$camp04."\r\n";
$fileHandle = fopen($archivo, 'a');
fwrite($fileHandle, $cadena);
fclose($fileHandle);

///// REGLON 36 MORA

$camp01="00036";
$camp02="3";
$camp03=str_pad($diasmora,4,"0",STR_PAD_LEFT);
$camp04=str_pad($valormora,10,"0",STR_PAD_LEFT);
$cadena=$camp01.$camp02.$camp03.$camp04."\r\n";
$fileHandle = fopen($archivo, 'a');
fwrite($fileHandle, $cadena);
fclose($fileHandle);


///// REGLON 39 TOTAL

$camp01="00039";
$camp02="3";
$camp03=str_pad(($valormora+$aporte_obli),10,"10",STR_PAD_LEFT);
$cadena=$camp01.$camp02.$camp03."\r\n";
$fileHandle = fopen($archivo, 'a');
fwrite($fileHandle, $cadena);
fclose($fileHandle);
$enlace=$archivo;
$_SESSION['ENLACE']=$enlace;
$_SESSION['ARCHIVO']=$nombreArchivo;
echo 99;

?>
