<?php
/* autor:       Orlando Puentes
 * fecha:       Septiembre 5 de 2010
 * objetivo:    Generar el archivo plano para el proceso de los aportes de las empresas que no lo hacen a trav�s de la Planilla �nica. 
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$url=$_SERVER['PHP_SELF'];
include_once $raiz.DIRECTORY_SEPARATOR.'phpComunes'.DIRECTORY_SEPARATOR.'auditoria.php';
auditar($url);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<title>::N&Oacute;MINA MANUAL::</title>
<link href="../../css/Estilos.css" rel="stylesheet" type="text/css" /> 
<link href="../../css/marco.css" rel="stylesheet" type="text/css" />
<link href="../../newcss/css.1.8/jquery-ui-1.8.17.custom.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../../newjs/1.6/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="../../newjs/1.6/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="../../newjs/comunes.js"></script>
<script type="text/javascript" src="../../newjs/jquery.utilitarios.js"></script>
<script type="text/javascript" src="../../js/comunes.js"></script>
<script type="text/javascript" src="js/nominaManual.js"></script>

<script type="text/javascript">
shortcut.add("Shift+F",function() {
		                                var URL=src();
		var url=URL+"aportes/trabajadores/consultaTrabajador.php";
    	window.open(url,"_blank");
    },{
	'propagate' : true,
	'target' : document 
        });        
</script>

</head>
<body>
<!-- TABLA OCULTA DE LISTADO  width81%  left:9.2-->
 
<table cellspacing="0" border="0" width="90%" style="position: fixed; left: 4%; display:none" id="listaTableHidden" class="tablaR hover">
  <thead>
    <tr class="zebra"> 
      <th width="7%"><strong>Cont</strong></th>
      <th width="8%"><strong>Cedula</strong></th>
      <th width="15%"><strong>1er APELLIDO </strong></th>
      <th width="10%"><strong>2do APELLIDO </strong></th>
      <th width="15%"><strong>1er NOMBRE </strong></th>
      <th width="13%"><strong>2do NOMBRE</strong></th>
      <th width="9%"><strong>DIAS</strong></th>
      <th width="9%"><strong>SB</strong></th>
      <th width="2%"><strong>I</strong></th>
      <th width="1%"><strong>R</strong></th>
      <th width="0%"><strong>L</strong></th>
      <th width="0%"><strong>M</strong></th>
      <th width="0%"><strong>V</strong></th>
      <th width="0%"><strong>E</strong></th>
    </tr>
  </thead>
</table>
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tbody><tr>
    <td width="13" height="29" class="arriba_iz">&nbsp;</td>
   <td class="arriba_ce"><span class="letrablanca">::&nbsp;N&oacute;mina - Pagos Aportes Sin Planilla &nbsp;::</span></td>
    <td width="13" class="arriba_de" align="right">&nbsp;</td>
  </tr>
  <tr>
    <td class="cuerpo_iz">&nbsp;</td>
    <td class="cuerpo_ce">
    <div id="div-botones">
      <img height="1" width="1" src="../../imagenes/spacer.gif">
	  <img height="1" width="1" src="../../imagenes/spacer.gif"/>
      <img src="../../imagenes/menu/nuevo.png" width="16"  height=16 style="cursor:pointer" title="Guardar" onClick="nuevaNomina();" />
      <img height="1" width="1" src="../../imagenes/spacer.gif"/>
	  <img src="../../imagenes/menu/grabar.png" width="16"  height=16 style="cursor:pointer" title="Guardar" onClick="guardarNomina();" />
      <img src="../../imagenes/spacer.gif" width="1" height="1"/>
      <img src="../../imagenes/menu/informacion.png" width="16" style="border:none; cursor:pointer" title="Manual" onClick="mostrarAyuda();" />
      </div>
    </td>
    <td class="cuerpo_de">&nbsp;</td>
  </tr>
  <tr>
    <td class="cuerpo_iz">&nbsp;</td>
    <td class="cuerpo_ce">&nbsp;</td>
   <td class="cuerpo_de">&nbsp;</td>
  </tr>
  <tr>
    <td class="cuerpo_iz">&nbsp;</td>
     <td class="cuerpo_ce">
	
	<table width="95%" cellspacing="0" border="0" class="tablero">
      <tbody>
	    <tr>
        <td width="15%">NIT</td>
        <td width="20%"><input id="txtNit" name="txtNit" class="box1" ></td>
        <td colspan="2"><strong><input type="text" id="txtRazon" name="txtRazon" class="boxlargo" disabled="true"></strong></td>
        </tr>
        <tr>
          <td>Email:</td>
          <td colspan="3"><input  id="email" class="boxlargo" name="email"></td>
          </tr>
        <tr>
        <td width="25%">Fecha Pago</td>
        <td width="25%"><input class="box1" id="txtFechaPago"  readonly="readonly">
          <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
        <td width="25%">Recibo Nro </td>
        <td width="25%"><input  id="txtRecibo" class="box1" name="txtRecibo">
          <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
        </tr>
      <tr>
          <td width="15%">Valor Nomina </td>
        <td width="20%"><input  id="txtNomina" class="box1" name="txtNomina">
          <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
        <td width="20%">Valor Cancelado </td>
        <td><input  id="txtCancelado" class="box1" name="txtCancelado">
          <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
        </tr>
      <tr>
        <td>Porcentaje</td>
        <td><select class="box1" id="porcentaje">
          <option value="0">0.0%</option>
          <option value="1">1.0%</option>
          <option value="2">2.0%</option>
          <option value="3">3.0%</option>	
          <option value="4" selected="selected">4.0%</option>
        </select>
          <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
        <td>Periodo Pago (aaaa-mm) </td>
        <td><input  maxlength="7" id="txtPeriodo" class="box1" name="txtPeriodo" readonly>
          <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
        </tr>
      <tr>
        <td>Dias de Mora </td>
        <td><input maxlength="2" value="0"   id="txtDiasMora" class="box1" name="txtDiasMora"> <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
        <td>Valor de Mora </td>
        <td><input value="0" id="txtValorMora" class="box1" name="txtValorsMora">
          <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
        </tr>
      <tr>
        <td>Nro Empleados</td>
        <td><input  id="nroEmpleados" class="box1"  />
          <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        </tr>
    </tbody>
    </table>

	  <br>
      <div id="ajax" style="display:none" align="center" class="Rojo">Espere por favor...</div>
      <br>
	  <div  id="linkEmpleados" align="center"><input type="button" class="ui-state-default" id="listarT" style= "cursor:pointer; padding:5px; background-image:url(../../imagenes/trabajadores.png) ; background-repeat:no-repeat; background-position:right center; padding-right:25px;" onClick="listarAfiliados();" value="Listar Afiliados" /></div>
	  
	  <br>
	  <div  id="linkQuitarNoCheckEmpleados" align="center"><input type="button" class="ui-state-default" id="btnQuitarNoCheckEmpleados" style= "cursor:pointer; padding:5px; background-image:url(../../imagenes/menu/ico_error.png) ; background-repeat:no-repeat; background-position:right center; padding-right:25px;" onClick="QuitarNoCheckEmpleados();" value="Eliminar No Checkeados" /></div>
	 
	  <div id="divlistaTable">
		<h5>ACTIVOS</h5>
		<table width="96%" cellspacing="0" border="0" class="tablaR hover"  id="listaTable">
			<thead>
				<tr >
					<th  width="4%"><strong>Cont</strong></th>
					<th width="4%"><strong>T.D.</strong></th>
					<th width="4%"><strong>Cedula</strong></th>
					<th width="12%"><strong>1er APELLIDO </strong></th>
					<th width="11%"><strong>2do APELLIDO </strong></th>
					<th width="14%"><strong>1er NOMBRE </strong></th>
					<th width="16%"><strong>2do NOMBRE</strong></th>
					<th width="4%"><strong>DIAS</strong></th>
					<th width="18%"><strong>SB</strong></th>
					<th width="3%"><strong>I</strong></th>
					<th width="3%"><strong>R</strong></th>
					<th width="3%"><strong>L</strong></th>
					<th width="2%"><strong>M</strong></th>
					<th width="2%"><strong>V</strong></th>
					<th width="2%" style="color:#F00" >E</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	  </div>
	<br>	
	 <!--
	 CUANDO SE QUIERA MOSTRAR LOS INACTIVOS
	<table width="92%" border="0" cellspacing="0" class="tablero">
		<tbody id="tablaCamposI">
      <tr>
        <td colspan="9" class="Bold12"><div align="center">INACTIVOS</div></td>
        </tr>
      <tr>
        <td width="3%">Cont</td>
        <td width="9%"><div align="center">Cedula</div></td>
        <td width="14%">1er APELLIDO </td>
        <td width="13%">2do APELLIDO</td>
        <td width="16%">1er NOMBRE</td>
        <td width="27%">2do NOMBRE</td>
        <td width="6%"><div align="center">DIAS</div></td>
        <td width="5%"><div align="center">SB</div></td>
        <td width="7%"><div align="center">I</div></td>
        </tr>
	  </tbody>
    </table>
	<br>
	-->
	
	<div id="espera">&nbsp;</div>
<div align="center">
<input type="button" onClick="agregarNuevo();"  id="agregarT" value="Agregar Afiliado" style="cursor:pointer; padding:5px; background-image:url(../../imagenes/addWorker.png) ; background-repeat:no-repeat; background-position:right center; padding-right:25px;display:none;" class="ui-state-default">
</div>
<br />
<br />
<table width="92%" cellspacing="0" border="0" class="tablaR"  id="nuevoTable" style="display:none;">
	  <thead>
	    <tr >
	      <th  width="4%"><strong>Cont</strong></th>
          <th><strong>T.D</strong></th>
	      <th width="8%"><strong>Cedula</strong></th>
	      <th width="12%"><strong>1er APELLIDO </strong></th>
	      <th width="11%"><strong>2do APELLIDO </strong></th>
	      <th width="14%"><strong>1er NOMBRE </strong></th>
	      <th width="16%"><strong>2do NOMBRE</strong></th>
	      <th width="4%"><strong>DIAS</strong></th>
	      <th width="18%"><strong>SB</strong></th>
	      <th width="3%"><strong>I</strong></th>
	      <th width="3%"><strong>R</strong></th>
	      <th width="3%"><strong>L</strong></th>
	      <th width="4%"><strong>M</strong></th>
	      </tr>
	    </thead>
</table>
      <br />
      <div id="eliminarUltimo" align="center" style="display:none;"><img src="../../imagenes/menu/ico_error.png" width="16" height="16" title="Eliminar ultimo" style="cursor:pointer;" onClick="eliminarUltimo()"></div>
<div id="camposocultos">&nbsp;
  <input type="hidden" value="0" id="numnuevos" name="numnuevos">
	<input type="hidden" value="0" id="numactivos" name="numactivos">
	<input type="hidden" value="0" id="numinactivos" name="numinactivos">
	<input type="hidden" value="0" id="razsoc" name="razsoc">
	<input type="hidden" value="0" id="digver" name="digver">
	<input type="hidden" value="0" id="direccion" name="direccion">
	</div>
	
    </td>
    <td class="cuerpo_de">&nbsp;</td>
  </tr>
  <tr>
    <td class="abajo_iz" >&nbsp;</td>
    <td class="abajo_ce" ></td>
    <td class="abajo_de" >&nbsp;</td>
  </tr>
</tbody></table>

<div id="dialogo-archivo" title="Planilla &Uacute;nica">
<div id="progreso" style="display: none; font-size: 15pt; font-weight: bold;"><span id="pg">0</span> de <span id="tt"></span> archivos</div>
<div id="log"></div>
</div>

<!-- Manual Ayuda -->
<div id="ayuda" title="Manual n&oacute;mina Manual" style="background-image:url('../../imagenes/FondoGeneral0.png')">
</div>

</body>
<script>
nuevaNomina();
</script>
</html>
