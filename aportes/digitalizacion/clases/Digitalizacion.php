<?php
/* autor:       Orlando Puentes
 * fecha:       Junio 10 de 2010
 * objetivo:    Procesar los datos digitalizados de las afiliaciones. 
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

error_reporting(E_ALL);
setlocale(LC_TIME, "es_CO");
include_once $raiz . DIRECTORY_SEPARATOR . 'rsc' . DIRECTORY_SEPARATOR . 'conexion.class.php';

class Digitalizacion{
	
	private $fecRadicacion=null;
	private $identificacion=null;
	private $negocio=null;
	private $rutaDigitalizados='/var/www/html/Digitalizacion';
	private $rutas=array();
	
	function __construct($fecharadicacion, $identificacion, $negocio){
		$this->fecRadicacion=$fecharadicacion;
		$this->identificacion=$identificacion;
		switch(intval($negocio)){
			case 28:
			case 29:
			case 70:
			case 69:
				$this->negocio='Subsidio';
				break;
			case 30:
			case 31:
			case 105:
				$this->negocio='Empresa';
				break;
			default:
				//@TODO ERROR TIPO DE NEGOCIO NO PERMITIDO
				break;
		}
	}
	
	private function verificaRutaDocumentos(){
		$link=new DBManager();
		$rutas=array();
		if($link->conectar()==true){
			if($this->negocio=='Subsidio'){
				$sql="SELECT rutadocumentos FROM aportes015 WHERE identificacion='$this->identificacion'";
				$r=mssql_query($sql, $link->conect);
				if($r){
					$row=@mssql_fetch_array($r);
					if(is_array($row)&&count($row)>0){
						$row ['rutadocumentos']=trim($row ['rutadocumentos']);
						if(is_dir($row ['rutadocumentos'])){
							$rutas ['rutaRel']=$row ['rutadocumentos'];
							$rutas ['rutaCmp']=$this->convertirRutaRel2Cmp($row ['rutadocumentos']);
							return $rutas;
						}else{
							return false;
						}
					}else{
						return false;
					}
				}else{
					return false;
				}
			}else if($this->negocio=='Empresa'){
				$sql="SELECT DISTINCT rutadocumentos FROM aportes048 WHERE nit='$this->identificacion'";
				$r=mssql_query($sql, $link->conect);
				if($r){
					$i=0;
					while($row ['rutadocumentos']=@mssql_fetch_array($r)){
						$i++;
					}
					if($i>1){
						trigger_error("Existe mas de una ruta de documentos para la empresa $this->identificacion", E_USER_WARNING);
						return false;
					}else if($i==1){
						$row ['rutadocumentos']=trim($row ['rutadocumentos']);
						if(is_dir($row ['rutadocumentos'])){
							$rutas ['rutaRel']=$row ['rutadocumentos'];
							$rutas ['rutaCmp']=$this->convertirRutaRel2Cmp($row ['rutadocumentos']);
							return $rutas;
						}else{
							return false;
						}
					}else{
						return false;
					}
				}
			}else{
				return false;
			}
		}
		$link->cerrarConexion();
	}
	
	private function convertirRutaRel2Cmp($ruta){
		if(preg_match("%((?:$this->rutaDigitalizados/))((?:Empresa/|Subsidio/)(?:20|10)[0-9]{2}/(?:enero/|febrero/|marzo/|abril/|mayo/|junio/|julio/|agosto/|septiembre/|octubre/|noviembre/|diciembre/)[a-zA-Z��0-9]{1,})%i", $ruta, $rutasEnc)){
			$compartida=$rutasEnc [1];
			$compartida=str_ireplace('/', '\\', $compartida);
			$rutaCompartida="\\\\".$_SERVER ['SERVER_ADDR']."\\Digitalizacion\\$compartida";
			return $rutaCompartida;
		}else{
			trigger_error("La ruta grabada en la base de datos $ruta no corresponde a la ruta del servidor", E_USER_ERROR);
			return false;
		}
	}
	
	//@todo Terminar el metodo de grabacion, hay que tener en cuenta el tipo de documento OJO
	private function grabarRutaBD(){
		$link=new DBManager();
		if($link->conectar()==true){
			if($this->negocio=='Subsidio'){
				$sql = "SELECT idpersona FROM aportes015 WHERE identificacion='$this->identificacion'";
			}else if($this->negocio=='Empresa'){
				
			}
		}
	}
	
	function crearRutaServidor(){
		$existeRuta=$this->verificaRutaDocumentos();
		
		if(!$existeRuta){
			if(!is_dir($this->rutaDigitalizados)){
				if(!mkdir($this->rutaDigitalizados, 0777, true)){
					trigger_error("No se pudo crear la ruta de digitalizaci&oacute;n $this->rutaDigitalizados", E_USER_WARNING);
					return false;
				}
			}
			if(chdir($this->rutaDigitalizados)){
				$cwd=getcwd();
				if(chmod($cwd, 0777)){
					if(preg_match('%\A(?:(0[1-9]|1[012])[/](0[1-9]|[12][0-9]|3[01])[/](19|20)[0-9]{2})\Z%', $this->fecRadicacion)){
						$fec=explode('/', $this->fecRadicacion);
						$fecSl=strftime("%Y/%B/%d", mktime(0, 0, 0, $fec [0], $fec [1], $fec [2]));
						$fecBsl=strftime("%Y\\%B\\%d", mktime(0, 0, 0, $fec [0], $fec [1], $fec [2]));
						$ruta=$cwd.DIRECTORY_SEPARATOR.$this->negocio.DIRECTORY_SEPARATOR.$fecSl.DIRECTORY_SEPARATOR.$this->identificacion;
						if(!is_dir($ruta)){
							if(mkdir($ruta, 0777, true)){
								$rutaCompartida="\\\\".$_SERVER ['SERVER_ADDR']."\\Digitalizacion\\$this->negocio\\$fecBsl\\$this->identificacion";
								$this->rutas ['rutaRel']=$ruta;
								$this->rutas ['rutaCmp']=$rutaCompartida;
								return $this->rutas;
							}else{
								trigger_error("No se pudo crear la ruta de digitalizaci&oacute;n $ruta", E_USER_WARNING);
								return false;
							}
						}else{
							@chmod($ruta, 0777);
							$rutaCompartida="\\\\".$_SERVER ['SERVER_ADDR']."\\Digitalizacion\\$this->negocio\\$fecBsl\\$this->identificacion";
							$this->rutas ['rutaRel']=$ruta;
							$this->rutas ['rutaCmp']=$rutaCompartida;
							return $this->rutas;
						}
					}else{
						trigger_error("La fecha de radicaci&oacute;n $this->fecRadicacion no cumple el formato MM/DD/YYYY", E_USER_WARNING);
						return false;
					}
				}
			}
		}else{
			return $existeRuta;
		}
	}
}
?>