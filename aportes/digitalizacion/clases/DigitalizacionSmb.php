<?php
/* autor:       Orlando Puentes
 * fecha:       Junio 8 de 2010
 * objetivo:    Procesar los datos digitalizados de las afiliaciones. 
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

error_reporting(E_ALL);
include_once $raiz . DIRECTORY_SEPARATOR . 'smb.php';
/*include 'smb.php'; 23-02-2012 */

class DigitalizacionSmb{
	
	const BSL="\\";
	const SL="/";
	
	private $fecRadicacion=null;
	private $rutaServidor=null;
	private $rutaLocal=null;
	private $usuarioSmb=null;
	private $contrasenaSmb=null;
	private $ipLocal=null;
	private $dominio=null;
	
	function __construct($fecharadicacion, $smbParam=array()){
		$this->fecRadicacion=$fecharadicacion;
		$this->usuarioSmb=$smbParam['usuario'];
		$this->contrasenaSmb=$smbParam['contrasena'];
		$this->dominio=$smbParam['dominio'];
	}
	
	function crearRutaLocal(){
		$soWin=$this->obtenerTipoSo();
		$this->ipLocal=$this->obtenerIP();
		//$sp=($soWin==true)?$this->BSL:$this->SL;
		$fec=explode('/', $this->fecRadicacion);
		
		if($soWin==true){
			$rut="smb://$this->usuarioSmb:$this->usuarioSmb@$this->ipLocal/c$";
		}else{
			$rut="smb://$this->dominio;$this->usuarioSmb:$this->contrasenaSmb@$this->ipLocal/DigitalizacionSmb/";
		}
		
		if(!is_dir($rut)){
			if(!mkdir($rut)){
				trigger_error("No se pudo crear el directorio $rut en el equipo $this->ipLocal", E_USER_ERROR);
			}else{
				echo strftime("%Y$sp%B$sp%d", mktime(0, 0, 0, $fec[0], $fec[1], $fec[2]));
			}
		}else{
			//echo strftime("%Y$sp%B$sp%d", mktime(0, 0, 0, $fec[0], $fec[1], $fec[2]));
			echo $rut.'/DigitalizacionSmb';
			if(is_dir($rut.'/DigitalizacionSmb')){
				echo 's';
			}else{
				mkdir($rut.'/DigitalizacionSmb');
			}
		}
	}
	
	private function obtenerTipoSo(){
		if(stripos($_SERVER['HTTP_USER_AGENT'], 'win')!==FALSE){
			return true;
		}else{
			return false;
		}
	}
	
	private function obtenerIP(){
		if(!empty($_SERVER['HTTP_CLIENT_IP'])){
			//check ip from share internet
			$ip=$_SERVER['HTTP_CLIENT_IP'];
		}elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
			//to check ip is pass from proxy
			$ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
		}else{
			$ip=$_SERVER['REMOTE_ADDR'];
		}
		return $ip;
	}
}

$para['usuario']='digitalizacion';
$para['contrasena']='digitalizacion';
$para['dominio']='COMFAMILIAR';
$dig=new DigitalizacionSmb('06/03/2010', $para);
$dig->crearRutaLocal();
?>