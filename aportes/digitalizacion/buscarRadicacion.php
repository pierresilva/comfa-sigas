<?php
/* autor:       Orlando Puentes
 * fecha:       Junio 9 de 2010
 * objetivo:    Realizar la union de Radicación con los demas formularios
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'radicacion' . DIRECTORY_SEPRATOR . 'clases' . DIRECTORY_SEPARATOR . 'radicacion.class.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'Digitalizacion.php';
/*include '../radicacion/clases/radicacion.class.php';
include 'clases/Digitalizacion.php'; 23-02-2012 */

$noRadicacion=trim($_REQUEST ['rad']);

$rad=new Radicacion();

$resultado=$rad->buscarRadicacionNoDitalizada($noRadicacion);
if($resultado==true){
	$radicacion=mssql_fetch_array($resultado);
	if(count($radicacion)>0 && is_array($radicacion)){
		$radicacion=array_map('trim', $radicacion);
		
		switch(intval($radicacion['idtiporadicacion'])){
			case 30:
			case 31:
			case 105:
				$radicacion['numero'] = $radicacion['nit'];
				break;
		}
		$fecDigitalizacion=date('m/d/Y');
		$dig=new Digitalizacion($fecDigitalizacion,$radicacion['numero'],$radicacion['idtiporadicacion']);
		$rutas = $dig->crearRutaServidor();
		if(is_array($rutas)){
			$radicacion['rutaCmp']=$rutas['rutaCmp'];
			$radicacion ['error']='0';	
		}else{
			$radicacion['error']='1';
		}		
	}else{
		$radicacion ['error']='1';
	}
	echo json_encode($radicacion);
}else{
	$radicacion ['error']='1';
	echo json_encode($radicacion);
}
?>