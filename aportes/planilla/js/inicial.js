/**
 * Acciones iniciales del formulario de nominas por planilla unica
 *
 * @author Jose Luis Rojas
 * @version 1.0.0
 * @since 24 Agosto 2010
 */
$(document).ready(function(){
	buscarArchivos();
	//Marcar todos
	$('#chkMarcarTodo').bind('click',function(){
		if($(this).is(':checked')){
			$("input[type=checkbox][id!=chkMarcarTodo]").attr('checked','checked');
		}else{
			$("input[type=checkbox][id!=chkMarcarTodo]").attr('checked','');
		}
	});
	
	$('#btnProcesarNominas').bind('click', procesarPlanilla);
	
	crearCargador();
	
	/*$('#lnkCargarArchivos').bind('click', function(evt){
		var ruta = $(this).attr('href');
		window.prompt ("Por favor copie y abra esta direccion en el navegador", ruta);
		evt.preventDefault();
	});*/
	
	$('#lnkRefrescar').bind('click', function(evt){
		evt.preventDefault();
		buscarArchivos();
	});
});


function procesarPlanilla(){
	$("#boton1").hide();
	$.getJSON('contarPlanillas.php',function(datos){
		var x = datos.length;
		alert("Se van a procesar: " + x + " Planillas");
		$.each(datos,function(i,a){
			$("#mensaje1").append("Procesando: " + a.archivo + "<br>" );
			$.getJSON('procesarPlanilla.php',{v0:a.archivo},function(dato){
				if (dato==0){
					$("#mensaje1").append("El archivo " + a.archivo + " fue procesado correctamente");
				}
				if (dato==1){
					$("#mensaje1").append("El archivo " + a.archivo + " YA fue procesado");
				}	
				
				//2 no se puede crear la empresa en sigas
				//3 no se pudo crear la empresa en informa web
				//4 no se pudo grabar la cabecera de la planilla
				
				});
			}); 
		})
										
	}