
$(function(){
	//direccion raiz del proyecto
	var URL=src();
	
	//crear el datepicker para los campos de periodo
	datepickerC("PERIODO","#txtPeriodoInicial");
	datepickerC("PERIODO","#txtPeriodoFinal");
	datepickerC("PERIODO","#txtPeriodoTraslado");
	//Crear el botton
	$("#btnTrasladar").button();
	nuevo();
	
	$("#cmbTipoIdentificacion").change(function(){
		nuevo();
	});
	
	//Evento para chequear todos los checkbox
	$("#imgChequear").click(function(){
		var srcImagen1 = URL+"imagenes/chk1.png";
		var srcImagen0 = URL+"imagenes/chk0.png";
		var estadoChequer = $("#hidChequearTodo").val();
		if(estadoChequer==0) {
			$("#tabPlanillas :checkbox").parent().parent().addClass("ui-state-default");
			$("#tabPlanillas :checkbox").attr("checked",true);
			$("#hidChequearTodo").val(1);
			$("#imgChequear").attr("src",srcImagen0);
			abilitarCamposTraslado();
		}else if(estadoChequer==1) {
			$("#tabPlanillas :checkbox").parent().parent().removeClass("ui-state-default");
			$("#tabPlanillas :checkbox").attr("checked",false);
			$("#hidChequearTodo").val(0);
			$("#imgChequear").attr("src",srcImagen1);
			desabilitarCamposTraslado();
		}
	});
	
	//Evento para buscar los datos de la empresa
	$("#txtIdentificacion").blur(function(){
		nuevo();
		
		var identificacion = $("#txtIdentificacion").val().trim();
		var tipoIdentificacion = $("#cmbTipoIdentificacion").val();
		if(identificacion==0)
			return false;
		var mensajeError = "No se encontro la ";
		var objDatos;
		//Buscar empresa | trabajador
		if(tipoIdentificacion==5) {
			objDatos = buscarDatosEmpresa(identificacion);
			mensajeError += "empresa";
		}else {
			objDatos = buscarDatosPersona(identificacion,tipoIdentificacion);
			mensajeError += "persona";
		}
		
		if(typeof objDatos!="object") {
			$("#lblNombre").addClass("classTextError").html(mensajeError);
			desabilitarCampoPeriodo();
			return false;
		}
		
		if(tipoIdentificacion==5) {
			$("#lblNombre").text(objDatos.razonsocial);
			$("#hidIdIdentificacion").val(objDatos.idempresa);
		}else {
			$("#lblNombre").text(objDatos.pnombre+" "+objDatos.snombre+" "+objDatos.papellido+" "+objDatos.sapellido);
			$("#hidIdIdentificacion").val(objDatos.idpersona);
		}
		abilitarCampoPeriodo();
	});	
	
	//Metodo para buscar los aportes de la empresa
	$("#btnBuscarPlanilla").click(function(){
		limpiarCampos2("#tdMensajeError");
		desabilitarCamposTraslado();
		var periodoInicial = $("#txtPeriodoInicial").val();
		var periodoFinal = $("#txtPeriodoFinal").val();
		var tipoIdentificacion = parseInt($("#cmbTipoIdentificacion").val())==5?"Empresa":"Trabajador";
		var idIdentificacion = $("#hidIdIdentificacion").val(); 
		$("#tabPlanillas").css('display','none');
		$("#txtPeriodoInicial,#txtPeriodoFinal").removeClass("ui-state-error");
		//Validaciones
		if(periodoInicial==0){$("#txtPeriodoInicial").addClass("ui-state-error");return false;}
		if(periodoFinal==0){$("#txtPeriodoFinal").addClass("ui-state-error");return false;}
		if(parseInt(periodoInicial)>parseInt(periodoFinal)){
			$("#txtPeriodoFinal").val("");
			return false;
		}
		//Enviar peticion para obtener los datos de la planillas
		$.ajax({
			url:"buscarPlanilla.php",
			type:"POST",
			data:{tipoIdentificacion:tipoIdentificacion,periodoInicial:periodoInicial,periodoFinal:periodoFinal,idIdentificacion:idIdentificacion},
			dataType:"json",
			async:false,
			success:function(datos) {
				if(typeof datos!="object" || datos.length==0) {
					$("#tdMensajeError").html("No existen Planillas");
					limpiarCampos2("#tBody");
					return false;
				}
				
				var tr = "";
				$.each(datos,function(i,row){
					tr += "<tr id='trPlanilla"+i+"'><td><input type='checkbox' name='chIdRegistro' id='chIdRegistro' " +
									"value='"+row.idplanilla+"' onclick='activarDesactivarMod(\"trPlanilla"+i+"\");'/></td>" +
							"<td>"+row.nit+"</td>" + 
							"<td>"+row.planilla+"</td>" +
							"<td>"+row.identificaTrabajador+"</td>" +
							"<td>"+row.trabajador+"</td>" +
							"<td>"+row.periodo+"</td>" +
							"<td>"+formatNumber(row.salariobasico)+"</td>" +
							"<td>"+formatNumber(row.ingresobase)+"</td>" +
							"<td>"+formatNumber(row.valoraporte)+"</td>" +
							"<td>"+row.diascotizados+"</td>" +
							"<td>"+row.fechapago+"</td></tr>";
				});
				$("#tabPlanillas").css("display","block");
				$("#tBody").html(tr);
			}
		});	
	});
	
	//Evento para buscar los datos de la empresa que va ha trasladar los aportes
	$("#txtNitTraslado").blur(function(){
		$("#lblRazonSocialTraslado").removeClass("classTextError");
		var nit = $("#txtNitTraslado").val().trim();
		if(nit=="")
			return false;
		datosEmpresaTraslado = buscarDatosEmpresa(nit);
		if(typeof datosEmpresaTraslado != "object"){
			$("#lblRazonSocialTraslado").addClass("classTextError").html(
					"No existe empresa con el nit " + nit);
			limpiarCampos2("#hidIdEmpresaTraslado");
			return false;
		}
		$("#lblRazonSocialTraslado").html(datosEmpresaTraslado.razonsocial);
		$("#hidIdEmpresaTraslado").val(datosEmpresaTraslado.idempresa);
	});	
	
	//Evento para manipular los campos del traslado de acuerdo al tipo
	$("#cmbTipoTraslado").change(function(){
		var tipo = $(this).val();
		limpiarCampos2("#txtNitTraslado,#hidIdEmpresaTraslado,#lblRazonSocialTraslado,#txtPeriodoTraslado");
		$("#txtNitTraslado,#txtPeriodoTraslado").removeClass("ui-state-error");
		if(tipo=="Periodo") {
			$("#trEmpresaTraslado").hide();
			$("#trPeriodoTraslado").show();
		}else if(tipo=="Empresa") {
			$("#trEmpresaTraslado").show();
			$("#trPeriodoTraslado").hide();
		}else if(tipo=="EmpresaPeriodo") {
			$("#trEmpresaTraslado,#trPeriodoTraslado").show();
		}
	});
});

/**
 * Busca los datos de la empresa de acuerdo al nit
 * @param nit
 * @returns Datos de la empresa
 */
function buscarDatosEmpresa(nit) {
	var data;
	$.ajax({
		url:URL+"phpComunes/buscarEmpresaNit.php",	
		type:"POST",
		data:{v0:nit},
		dataType:"json",
		async:false,
		success:function(datos){
			if(typeof datos == "object")
				data = datos[0];
		}
	});
	return data;
}

function buscarDatosPersona(identificacion,tipoIdentificacion) {
	var data;
	$.ajax({
		url:URL+"phpComunes/buscarPersona2.php",	
		type:"POST",
		data:{v0:tipoIdentificacion,v1:identificacion},
		dataType:"json",
		async:false,
		success:function(datos){
			if(typeof datos == "object")
				data = datos[0];
		}
	});
	return data;
}

function abilitarCampoPeriodo(){
	$("#btnBuscarPlanilla,#txtPeriodoInicial,#txtPeriodoFinal").attr("disabled",false);
}

function desabilitarCampoPeriodo(){
	$("#btnBuscarPlanilla,#txtPeriodoInicial,#txtPeriodoFinal").attr("disabled",true);
	limpiarCampos2("#txtPeriodoInicial,#txtPeriodoFinal");
}

function abilitarCamposTraslado(){
	$("#tblTraslado").show();
}

function desabilitarCamposTraslado(){
	$("#tblTraslado").hide();
	$("#cmbTipoTraslado").val("Periodo").trigger("change");
	limpiarCampos2("#txtNitTraslado,#lblRazonSocialTraslado,#hidIdEmpresaTraslado,#txtPeriodoTraslado");
}

//activar o desactivar los campos para modificar datos del aporte
function activarDesactivarMod(id){
	var numChecked = $("#tabPlanillas :checkbox:checked").length;
	var checked = $("#"+id+" #chIdRegistro").is(":checked");
	//Habilitar los campos del traslado
	if(numChecked>0)
		abilitarCamposTraslado();
	else
		desabilitarCamposTraslado();
	//Cambiar apariencia la fila del aporte
	if(checked==true)
		$("#"+id).addClass("ui-state-default");
	else
		$("#"+id).removeClass("ui-state-default");
			
}

//limpiar los campos
function nuevo() {
	desabilitarCampoPeriodo();
	desabilitarCamposTraslado();
	limpiarCampos2("#tdMensajeError,#tBody,#hidIdIdentificacion,#lblNombre,#hidChequearTodo");
	$("#tabPlanillas").hide();
	$("#lblNombre").removeClass("classTextError");
	$("#imgChequear").attr("src","../../imagenes/chk1.png");
}

//modificar el aporte
function trasladar(){
	//Datos de la empresa antigua
	var idIdentificacion = $("#hidIdIdentificacion").val().trim();
	var objTipoIdentificacion = $("#cmbTipoIdentificacion");
	var objIdentificacion = $("#txtIdentificacion");
	var objNombre = $("#lblNombre");
	var objPeriodoInicial = $("#txtPeriodoInicial");
	var objPeriodoFinal = $("#txtPeriodoFinal");
	
	var tipoTraslado = $("#cmbTipoTraslado").val();
	//Datos de la empresa nueva
	var idEmpresaTraslado = parseInt($("#hidIdEmpresaTraslado").val());
	var objNitTraslado = $("#txtNitTraslado");
	var objRazonSocialTrasl = $("#lblRazonSocialTraslado");
	var objPeriodoTraslado = $("#txtPeriodoTraslado");
	
	var numPlanillas = $("#tabPlanillas :checkbox:checked").length;
	
	$("#cmbTipoIdentificacion,#txtIdentificacion,#txtPeriodoInicial,#txtPeriodoFinal" +
			",#txtNitTraslado,#txtPeriodoTraslado" +
			",#lblRazonSocialTraslado,#lblRazonSocial").removeClass("ui-state-error");

	//validacion campos vacios
	if(idIdentificacion==0){objNombre.addClass("ui-state-error");return false;}
	if(objTipoIdentificacion.val()==0){objTipoIdentificacion.addClass("ui-state-error");return false;}
	if(objIdentificacion.val()==0){objIdentificacion.addClass("ui-state-error");return false;}
	if(objPeriodoInicial.val()==0){objPeriodoInicial.addClass("ui-state-error");return false;}
	if(objPeriodoFinal.val()==0){objPeriodoFinal.addClass("ui-state-error");return false;}
	
	if(numPlanillas==0) {
		alert("Debe chequear las planillas a trasladar");
		return false;
	}
	if(tipoTraslado=="Periodo") {
		if(objPeriodoTraslado.val()==0){objPeriodoTraslado.addClass("ui-state-error");return false;}
	}else if(tipoTraslado=="Empresa") {
		if(idEmpresaTraslado==0){objRazonSocialTrasl.addClass("ui-state-error");return false;}
		if(objNitTraslado.val()==0){objNitTraslado.addClass("ui-state-error");return false;}
	}else {
		if(idEmpresaTraslado==0){objRazonSocialTrasl.addClass("ui-state-error");return false;}
		if(objNitTraslado.val()==0){objNitTraslado.addClass("ui-state-error");return false;}
		if(objPeriodoTraslado.val()==0){objPeriodoTraslado.addClass("ui-state-error");return false;}
	}
	
	var arrIdPlanillas = new Array();
	//------------------------
	//Obtenemos los id de los aportes a trasladar
	$("#tabPlanillas :checkbox:checked").each(function(i,row){
		arrIdPlanillas[arrIdPlanillas.length] = parseInt($(this).val());
	});
	
	//----------------------------------
	var objData = {
			tipoTraslado:tipoTraslado
			, arrIdPlanillas:arrIdPlanillas
			, idEmpresaTraslado:idEmpresaTraslado
			, nitTraslado:objNitTraslado.val().trim()
			, periodoTraslado:objPeriodoTraslado.val()}
	//peticion para guardar el traslado
	$.ajax({
		url:"guardarTraslado.php",
		type:"POST",
		data:objData,
		dataType:"json",
		async:false,
		success:function(datos){
			if(datos == 0){
				alert("El traslado de la planilla no fue posible");
				return false;
			}
			alert("El traslado de la planilla se realizo correctamente");
			nuevo();
			limpiarCampos2("#txtIdentificacion");
		}	
	});
}
	
	