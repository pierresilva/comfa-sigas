var idplanilla;
var nit;
var idpersona;
var pnom;
var snom;
var pape;
var sape;
var continuar; //variable que determina si se puede guardar

function buscarPlanilla(idp){
	idplanilla=idp;
	nit=0;
	idpersona=0;
	continuar=true;
	
	$.ajax({
		url: URL+'phpComunes/buscarPlanillaxIdplanilla.php',
		type: "POST",
		data: {v0:idp},
		async: false,
		dataType: 'json',
		success: function(data){
			if(data==0){
				alert("Planilla no fue encontrada");
				continuar=false;
				return false;
			}
			
			$.each(data, function(i, fila){
				nit=fila.nit;
				idpersona=fila.idpersona;
				pnom=fila.pnombre;
				snom=fila.snombre;
				pape=fila.papellido;
				sape=fila.sapellido;
				
				$("#tdEmpresa").html(fila.nit + " - " + fila.razonsocial);
				$("#txtTipoD").val(fila.idtipodocumento);
				$("#txtNumero").val(fila.identificacion);
				$("#tdPersona").html($.trim(fila.pnombre+" "+fila.snombre+" "+fila.papellido+" "+fila.sapellido));
				$("#tdPlanilla").html(fila.planilla);
				$("#txtAporte").val($.trim(parseInt(fila.valoraporte)));
				$("#txtsb").val($.trim(parseInt(fila.salariobasico)));
				$("#txtib").val($.trim(parseInt(fila.ingresobase)));
				$("#txtdc").val($.trim(fila.diascotizados));
				$("#txthc").val($.trim(fila.horascotizadas));
				$("#txtPeriodo").val($.trim(fila.periodo));
			});
		}
	
	});
};

function buscarPersona(){
	var td=parseInt($("#txtTipoD").val(),10);	
	var num=$("#txtNumero").val();					
	
	if(num.length==0){
		alert("Digite un numero de identificacion");
		continuar=false;
		return false;
	}
	
	$.ajax({
		url: URL+'phpComunes/pdo.buscar.personaAxEmpresa.php',
		type: "POST",
		data: {v0:nit,v1:td,v2:num},
		async: false,
		dataType: 'json',
		success: function(data){
			if(data==0){
				alert("No existe la persona en nuestra Base de Datos!");
				$("#txtNumero").val('');
				$("#tdPersona").html('');
				continuar=false;
				return false;
			}
			else{
				idpersona=data[0].idpersona;
				pnom=data[0].pnombre;
				snom=data[0].snombre;
				pape=data[0].papellido;
				sape=data[0].sapellido;
				
				$("#tdPersona").html($.trim(data[0].pnombre+" "+data[0].snombre+" "+data[0].papellido+" "+data[0].sapellido));
				continuar=true;
			}
		}	
	});
};

function actualizarPlanilla(){
	//buscarPlanilla(idplanilla);
	if (continuar==false){
		alert("Existen errores que no permiten continuar con el proceso");
		return false;
	}
	
	var td=parseInt($("#txtTipoD").val(),10);	//TipoDocumento
	var num=$("#txtNumero").val();				//Identificacion
	var aporte=$("#txtAporte").val();			//ValorAporte
	var sb=$("#txtsb").val();					//SalarioBasico
	var ib=$("#txtib").val();					//IngresoBasico
	var dc=$("#txtdc").val();					//DiasCotizados
	var hc=$("#txthc").val();					//HorasCotizados
	var periodo=$("#txtPeriodo").val();			//Periodo
	
	if(num.length==0){
		alert("Digite un numero de identificacion");		
		return false;
	}
	
	if(aporte.length==0 || aporte<0){
		alert("Digite un valor de aporte valido");		
		return false;
	}
	
	if(sb.length==0 || sb<0){
		alert("Digite un salario basico valido");		
		return false;
	}
	
	if(ib.length==0 || ib<0){
		alert("Digite un ingreso basico valido");		
		return false;
	}
	
	if(dc.length==0 || dc<0){
		alert("Digite una cantidad de dias cotizados valido");		
		return false;
	}
	
	if(hc.length==0 || hc<0){
		alert("Digite una cantidad de horas cotizadas valida");		
		return false;
	}
	
	$.getJSON('actualizarPlanilla.php',{v0:idpersona,v1:td,v2:num,v3:pape,v4:sape,v5:pnom,v6:snom,v7:aporte,v8:sb,v9:ib,v10:dc,v11:hc,v12:periodo,v13:idplanilla},function(datos){
		if(datos==0){
			MENSAJE("La planilla NO fue actualizado!");
		}else {
		    alert("Registro actualizado!");
		    newModificarPlanilla();			
		}
	});
};

function newModificarPlanilla(){
	location.reload();
};