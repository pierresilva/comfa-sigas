/**
 * Acciones iniciales del formulario de planilla
 *
 * @author Orlando Puentes A.
 * @version 1.0.0
 * 
 */
var URL= src();
$(document).ready(function(){
	//buscarArchivos();
	//Marcar todos
	
	
	$('#btnProcesarNominas').bind('click', procesarPlanilla);
	
	crearCargador();
	
	/*$('#lnkCargarArchivos').bind('click', function(evt){
		var ruta = $(this).attr('href');
		window.prompt ("Por favor copie y abra esta direccion en el navegador", ruta);
		evt.preventDefault();
	});*/
});


function procesarPlanilla(){
	$("#boton1").hide();
	$("#tbErrores").html("");	
	
	var con=0;
	$.ajax({
		  url: 'contarPlanillas.php',
		  type: 'GET',
		  async:false,
		  dataType: 'json',
		  success: function(datos){	
			if(datos==1){
				alert ("No hay archivos para procesar!");
				return false;
			}
			var x = datos.length;
			$("#mensaje1").append("Se van a procesar: " + x + " Planillas<br>Inicio proceso: " + hora()  +"<br>");
			$.each(datos,function(i,a){
				$("#mensaje2").html("<br/><center><img src=../../imagenes/ajax-loader-fon.gif></center>")
				$.ajax({
					  url: 'procesarPlanilla.php',
					  data:{v0:a.archivo},
					  async:false,
					  dataType: 'json',
					  success: function(dato){
						  con++;
						  var cad = "<br>" + hora() + " - " + con + " El archivo " + a.archivo;
						  if (dato==0){
							  cad+= " fue procesado correctamente";
						  }
						  if (dato==1){
							  cad +=  " YA fue procesado";
						  }
						  if (dato==2){
							  cad += " No se pudo crear la empresa en SIGAS";
						  }
						  if (dato==3){
							  cad += " No se pudo crear la empresa en INFORMA WEB";
						  }
						  if (dato==4){
							  cad += " NO se pudo guardar la cabecera del archivo";
						  }
						  $("#mensaje1").append(cad);
					  }
				});
				
				 $.ajax({
						url: 'leerLogErrores.php',
						dataType: 'json',
						async: false,
						success: function(datosErrores){
						   if (datosErrores instanceof Array && datosErrores.length>0){
						       $.each(datosErrores,function(i,fila){
						    	   if(fila.Tipo=='ErrorUno'){
					    	       $("#tbErrores").append("<tr>" +
					        	           "<td style='text-align: center' >" + fila.nombreArchivo + "</td>" +
					        	           "<td>&nbsp;" + fila.error + "</td>" +
					        	           "<td style='text-align: center'>" + fila.tipoDocumento + "</td>" +
					        	           "<td style='text-align: right'>" + fila.identificacion + "&nbsp;&nbsp;</td>" +
					                    "</tr>");
					    	       		$("#trErrores").show();
						    	   }/*Fin if(fila.Tipo=='ErrorUno'){*/
						    	   else{
						    		   $("#tbErroresLog").append("<tr>" +
						    	    		   "<td style='text-align: center' >" + fila.error + "</td>" +
						        	           "<td>&nbsp;" + fila.Planilla + "</td>" +
						        	           "<td style='text-align: center'>" + fila.Nit + "</td>" +
						        	           "<td style='text-align: center'>" + fila.Identificacion + "</td>" +
						        	           "<td style='text-align: right'>" + fila.Periodo + "&nbsp;&nbsp;</td>" +
						        	           "<td style='text-align: right'>" + fila.FechaPago + "&nbsp;&nbsp;</td>" +
						                    "</tr>");
						    		   		$("#trErroresLog").show();
						    	   }/*Fin else{*/
								});
							}
					    }	
					});
				
				if(i==(x-1)){
					
					//*********************************************************************
					
					$.ajax({
						  url: 'contarPlanillas.php',
						  type: 'GET',
						  async:false,
						  dataType: 'json',
						  success: function(datos){	
							if(datos==1){
								var p = 0;
								
								$("#mensaje1").append("<br>Fin del proceso: " + hora()  +"<br>Planillas Sin Procesar: "+ p +"<br><br>RECUERDE REVISAR LOS ARCHIVOS LOG<br>");
								$("#mensaje2").html("")
								
							}else{
							var p = datos.length;
							
							$("#mensaje1").append("<br>Fin del proceso: " + hora()  +"<br>Planillas Sin Procesar: "+ p +"<br><br>RECUERDE REVISAR LOS ARCHIVOS LOG<br>");
							$("#mensaje2").html("")
							
							}
						  }
					});
					
					//*********************************************************************
						
				}
			});
		}
	});										
}