var idPlanilla=0;

$(document).ready(function(){
	var URL=src();

//Buscar Planilla
	$("#buscarP").click(function(){
		idPlanilla=0;
		
		$("#buscarP").next("span.Rojo").html("");
		$("#planillas").find("p").remove();
		$("#lstP").remove();
		//espera
		dialogLoading("show");		

		if($("#nit").val()==''){
			$(this).next("span.Rojo").html("Ingrese el Nit de la Empresa.").hide().fadeIn("slow");
			dialogLoading("hide");
			$("#nit").focus();
			return false;
		}	
		if($("#periodo").val()==''){
			$(this).next("span.Rojo").html("Ingrese el periodo de la planilla.").hide().fadeIn("slow");
			dialogLoading("hide");
			$("#periodo").focus();
			return false;
		}
		if($("#idT").val()==''){
			$(this).next("span.Rojo").html("Ingrese la identificacion del trabajador.").hide().fadeIn("slow");
			dialogLoading("hide");
			$("#idT").focus();
			return false;
		}		
		
		var v0=$("#nit").val();
		var v1=$("#periodo").val();
		var v2=$("#tipoDocumento").val();
		var v3=$("#idT").val();
		
		var contadorPla=0;		
		var cadenaPlanillas='';
		
		$.ajax({
			url: URL+'phpComunes/buscarPlanilla.php',
			type: "POST",
			data: {v0:v0,v1:v1,v2:v2,v3:v3},
			async: false,
			dataType: 'json',
			success: function(data){
				if(data==0){
					$("#buscarP").next("span.Rojo").html("No se encontr&oacute; planillas").hide().fadeIn("slow");
					dialogLoading("hide");	
					return false;
				}				
				 
				$("#nit").val('');
				$("#periodo").val('');
				$("#tipoDocumento").val('');
				$("#idT").val('');

				$.each(data,function(i,fila){
					idp=fila.idplanilla;
					nom =fila.pnombre+" "+fila.papellido;
					pla =fila.planilla;
					
					contadorPla++;
					cadenaPlanillas+='<p align="left" onclick="enviarPlanilla('+idp+')">'+idp+" "+nom+" "+pla+'</p>';
					
				});//fin each	for
				$("#planillas").append(cadenaPlanillas);
				dialogLoading("hide");	
				$("#planillas").before("<p id='lstP' align='center'>Planillas encontradas:<b> "+contadorPla+"</b></p>");

			}//success
		});

	});//fin click
	
//... ICONO PARA OCULTAR/MOSTRAR FORMULARIO ...
$("div#icon span").click(function(){
	$(this).toggleClass("toggleIcon");
	$("div#wrapTable").slideToggle();	
});//fin click icon	

});//fin ready



//... FUNCION PARA ENVIAR NIT PARA ACORDEON ...//
function enviarPlanilla(idp){	
	idPlanilla=idp;

	$("div#icon").show();
	$("#planillas").find("p").remove();
	$("div#wrapTable").slideUp("normal");
	$("#tabsA").show("slow",function(){
		$("#tabsA ul li:first a").trigger("click");
	});

	$("#tabsA ul li a:not(':last')").one("click",function(){
		var aid=$(this).attr("id");
		 switch(aid){
			case "a0": $("#tabs-1").load("tabs/planillaTab.php",{v0:idPlanilla}); break;
		 }
	 });

	//Carga los tabs
	$("#tabsA").tabs({
		spinner:'<em>Loading&#8230;</em>',
		selected:0,
	});//end tab	
	
	$("#lstP").remove();
}//END FUNCTION BUSCAR PLANILLA