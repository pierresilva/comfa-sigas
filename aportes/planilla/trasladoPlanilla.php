<?php
	set_time_limit(0);
	ini_set("display_errors",'1');
	date_default_timezone_set("America/Bogota");
	$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
	include_once  $root;
	$usuario=$_SESSION['USUARIO'];
	$fecver = date('Ymd h:i:s A',filectime('trasladoPlanilla.php'));
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<title>Traslado PU</title>
		<link type="text/css" href="../../css/Estilos.css" rel="stylesheet"/>
		<link type="text/css" href="../../css/marco.css" rel="stylesheet"/>
		<link type="text/css" rel="stylesheet" href="../../css/css.1.8/jquery-ui-1.8.17.custom.css" />
		<script language="javascript" src="../../js/1.6/jquery-1.6.2.min.js"></script>
		<script language="javascript" src="../../js/1.6/jquery-ui-1.8.16.custom.min.js"></script>
		<script language="javascript" src="../../js/comunes.js"></script>
		<script language="javascript" src="js/trasladoPlanilla.js"></script>
		<style type="text/css">
			.classCampoCorto {background: none repeat scroll 0 0 #F7F7F7; border-style: hidden; border-width: 0; font-size: 11px; padding: 0;}
			.classTextError {color:#ff0000} 
		</style>
	</head>
	<body>
		<center>
			<br /><br />
			<table width="70%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="arriba_iz" >&nbsp;</td>
					<td class="arriba_ce" ><span class="letrablanca">::Traslado PU::</span>
						<div style="text-align:right;float:right;">
							<?php echo 'Versi&oacute;n: ' . $fecver; ?>
						</div>
					</td>
					<td class="arriba_de" >&nbsp;</td>
				</tr>
				<tr>
					<td class="cuerpo_iz">&nbsp;</td>
					<td class="cuerpo_ce"><img src="../../imagenes/spacer.gif" alt="" width="2" height="1" /></td>
					<td class="cuerpo_de">&nbsp;</td>
				</tr>
				<tr>
					<td class="cuerpo_iz" >&nbsp;</td>
					<td class="cuerpo_ce" >
						<table border="0" class="tablero" cellspacing="0" width="100%" >
							<tr>
						    	<td width="150" style="text-align:right;">
						    		Tipo Identificacion:
						    	</td>
						    	<td>
						    		<select name="cmbTipoIdentificacion" id="cmbTipoIdentificacion" class="box1">
						    			<option value="1">CEDULA DE CIUDADANIA</option>
						    			<option value="4">CEDULA DE EXTRANJERIA</option>
						    			<option value="5">NIT</option>
						    		</select>
						    		<input type="text" name="txtIdentificacion" id="txtIdentificacion" class="box1"/>
						    		<input type="hidden" name="hidIdIdentificacion" id="hidIdIdentificacion"/>
						    		&nbsp; <label id="lblNombre" >&nbsp;</label>
						    	</td>
						  	</tr>
						  	<tr>
							    <td style="text-align:right;">Periodos:</td>
							    <td >
							    	<input  type="text" readonly="readonly"size="10" name="txtPeriodoInicial" id="txtPeriodoInicial" disabled="disabled"/>
							    	 - <input  type="text" readonly="readonly"size="10" name="txtPeriodoFinal" id="txtPeriodoFinal" disabled="disabled"/> 
							    	&nbsp;&nbsp;<input type="button" value="Buscar" name="btnBuscarPlanilla" id="btnBuscarPlanilla" class="ui-state-default" disabled="disabled"/>
							    </td>
							</tr>
						  	<tr>
						    	<td colspan="2" id="tdMensajeError" class="classTextError"></td>
						  	</tr>
						  	<tr>
						    	<td colspan="2"><b>Planillas</b></td>
						  	</tr>
					  	</table>
					  	
						<table border="0" class="tablero" cellspacing="0" width="100%" id="tabPlanillas" style="display: block;"> 
							<thead width="100%">
						  		<tr>
						  			<th width="5%">
						  				<img src="../../imagenes/chk1.png" id="imgChequear" style="cursor:pointer;"/>
						  				<input type="hidden" name="hidChequearTodo" id="hidChequearTodo" value=""/>
						  			</th>
						  			<th width="8%">Nit</th>
						  			<th width="8%">PU</th>
						  			<th width="10%">Identificaci&oacute;n</th>
						  			<th width="15%">Trabajador</th>
						  			<th width="5%">Periodo</th>
						  			<th width="8%">Basico</th>
						  			<th width="8%">Base</th>
						  			<th width="8%">Aporte</th>
						  			<th width="5%">Dias</th>
						  			<th width="5%">Fecha Pago</th>
						  		</tr>
						  	</thead>
						  	<tbody id="tBody" width="100%">		
						  	</tbody>
						  	<tfoot><tr><th colspan="11">&nbsp;</th></tr></tfoot>
						</table>
						<table border="0" class="tablero" cellspacing="0" width="100%" id="tblTraslado" >
							<tr><td colspan="2"><b>Datos del Traslado</b></td></tr>
							<tr>
						    	<td width="100" style="text-align:right;">
						    		Tipo Traslado:
						    	</td>
						    	<td>
						    		<select name="cmbTipoTraslado" id="cmbTipoTraslado">
						    			<option value="Periodo">Periodo</option>
						    			<option value="Empresa">Empresa</option>
						    			<option value="EmpresaPeriodo">Empresa y Periodo</option>
						    		</select>
						    	</td>
						  	</tr>
						  	<tr>
							    <td colspan="2" ></td>
							</tr>
							<!-- TIPO TRASLADO EMPRESA -->
							<tr id="trEmpresaTraslado">
								<td style="text-align:right;">Nit:</td>
								<td>
									<input type="text" name="txtNitTraslado" id="txtNitTraslado" />
									<input type="hidden" name="hidIdEmpresaTraslado" id="hidIdEmpresaTraslado" />
						    		&nbsp; <label id="lblRazonSocialTraslado" >&nbsp;</label>
						    	</td>
							</tr>
							<!-- TIPO TRASLADO PERIODO -->
							<tr id="trPeriodoTraslado">
								<td style="text-align:right;">Periodo:</td>
								<td>
									<input  type="text" readonly="readonly"size="10" name="txtPeriodoTraslado" id="txtPeriodoTraslado"/>
						    	</td>
							</tr>
							<tr>
								<td style="text-align:center;" colspan="2">
									<input type="button" name="btnTrasladar" id="btnTrasladar" value="Trasladar" onclick="trasladar();"/>
								</td>
							</tr>
					  	</table>
					</td>
					<td class="cuerpo_de" >&nbsp;</td>
				</tr>
				<tr>
					<td class="cuerpo_iz" >&nbsp;</td>
					<td class="cuerpo_ce" >&nbsp;</td>
					<td class="cuerpo_de" >&nbsp;</td>
				</tr>
				<tr>
					<td class="abajo_iz" >&nbsp;</td>
					<td class="abajo_ce" ></td>
					<td class="abajo_de" >&nbsp;</td>
				</tr>
			</table>
		</center>
	</body>
</html>
