<?php
	date_default_timezone_set('America/Bogota');
	$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
	include_once  $root;
	include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
	include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
	
	include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'p.planilla.unica.class.php';
	
	$db = IFXDbManejador::conectarDB();
	if($db->conexionID==null){
		$cadena = $db->error;
		echo msg_error($cadena);
		exit();
	}
	
	//Parametros
	$tipoTraslado = $_REQUEST["tipoTraslado"];
	$idEmpresaTraslado = $_REQUEST["idEmpresaTraslado"];
	$nitTraslado = $_REQUEST["nitTraslado"];
	$periodoTraslado = $_REQUEST["periodoTraslado"];
	$arrIdPlanillas = $_REQUEST["arrIdPlanillas"];
	
	$idPlanillas = join($arrIdPlanillas,',');
	
	$objPlanilla = new Planilla();
	if($tipoTraslado=="Periodo") {
		$idEmpresaTraslado = null;
		$nitTraslado = null;
	}else if($tipoTraslado=="Empresa") {
		$periodoTraslado = null;
	}
	$rs = $objPlanilla->actualizar_traslado_planilla($_SESSION["USUARIO"],$idPlanillas,$idEmpresaTraslado,$nitTraslado,$periodoTraslado);
	if($rs==true)
		echo 1;
	else 
		echo 0;
?>