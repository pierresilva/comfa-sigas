<?php
/* ******************
autor:	Orlando Puentes A.
fecha;	Sep 10 2012
Objeto:
*/

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
$url=$_SERVER['PHP_SELF'];
include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'auditoria.php';
auditar($url);
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}
$sql="SELECT DISTINCT usuario FROM aportes004 WHERE fecharadicacion= CAST(GETDATE() AS DATE) AND idtiporadicacion IN(28,29,70,183,2919,2926)";
$rs=$db->querySimple($sql);

$Sql="SELECT IDRADICACION,FOLIOS,TIEMPO,NUMERO,PNOMBRE,PAPELLIDO,NIT,DETALLEDEFINICION, idtiporadicacion,aportes004.usuario FROM aportes004 
INNER JOIN aportes015 ON APORTES004.numero = APORTES015.identificacion
INNER JOIN aportes091 ON APORTES004.idtiporadicacion=APORTES091.iddetalledef
WHERE fecharadicacion= CAST(GETDATE() AS DATE) AND idtiporadicacion IN(28,29,70,183,2919,2926)
AND devuelto='N' AND anulada='N' AND procesado='N' AND aportes004.usuario='submoc'
ORDER BY idtiporadicacion,idradicacion"
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>::Cierre diario de radicaciones::</title>
<link type="text/css" href="../../css/css.1.8/jquery-ui-1.8.17.custom.css" rel="stylesheet" />
<link type="text/css" href="../../css/Estilos.css" rel="stylesheet"/>
<link href="../../css/marco.css" rel="stylesheet" type="text/css" />

<script language="javascript" src="../../js/1.6/jquery-1.6.2.min.js"></script>
<script language="javascript" src="../../js/1.6/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="../../js/comunes.js"></script>
<script type="text/javascript" src="../../js/phpjs/phpencodeutf8.js"></script>
<script type="text/javascript" src="../../js/phpjs/phpmd5.js"></script>
<script type="text/javascript" src="js/conciliar.js" language="javascript"></script>

<style type="text/css">
#btnProcesarNominas{
	boder: 1px solid #AFAFAF;
	width: 180px;
	height: 50px;
	text-align: left;
	font-size: 15px;
	background-image: url('../../imagenes/procesar.png');
	background-position: right;
	background-repeat: no-repeat;
}
.box-table-a{
	width: none;
}
</style>
</head>
<body>
<center>
<table width="95%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td class="arriba_iz" >&nbsp;</td>
<td colspan="2" class="arriba_ce" ><span class="letrablanca">::titulo&nbsp;::</span></td>
<td class="arriba_de" >&nbsp;</td>
</tr>
<tr>
<td class="cuerpo_iz">&nbsp;</td>
<td colspan="2" class="cuerpo_ce">
<img src="../../imagenes/menu/nuevo.png" alt="Nuevo" width="16" height="16" style="cursor:pointer" title="Nuevo" onclick="nuevoR()" />
<img src="../../imagenes/spacer.gif" alt="" width="1" height="1" />
<img src="../../imagenes/menu/grabar.png" alt="Guardar" width="16" height="16" style="cursor:pointer" title="Guardar" onclick="guardarR();" id= "bGuardar" />  
<img src="../../imagenes/spacer.gif" alt="" width="1" height="1" />
</td>
<td class="cuerpo_de">&nbsp;</td>
</tr>
<tr>
<td class="cuerpo_iz" >&nbsp;</td>
<td colspan="2" class="cuerpo_ce" >
<table width="100%" border="0" class="tablero">
<tr>
<td>Usuario</td>
<td>
<select id="usuarios" name="usuarios" class="box1" onchange="listar()">
<option value='0' selected="selected">Seleccione</option>
<?php 
while ($row=$rs->fetch()){
 	echo "<option value='{$row['usuario']}'>{$row['usuario']}</option>";
}
?>
</select>
</td>
<td></td>
<td>Fecha</td>
</tr>
</table>
</td>
<td class="cuerpo_de" >&nbsp;</td>
</tr>
<tr>
<td class="cuerpo_iz" >&nbsp;</td>
<td colspan="2" class="cuerpo_ce" ><label class="RojoGrande">Radicaciones del Grupo 1 - Afiliados</label></td>
<td class="cuerpo_de" >&nbsp;</td>
</tr>

<tr>
<td class="cuerpo_iz" >&nbsp;</td>
<td colspan="2" class="cuerpo_ce" >
	<table id="tAfiliado" class="tablero" width="100%">
    <tbody></tbody>
    </table> 
</td>
<td class="cuerpo_de" >&nbsp;</td>
</tr>
<tr class="RojoGrande">
<td class="cuerpo_iz" >&nbsp;</td>
<td colspan="2" class="cuerpo_ce" ><label class="RojoGrande">Radicaciones del Grupo 2 - Empresa</label></td>
<td class="cuerpo_de" >&nbsp;</td>
</tr>
<tr>
<td class="cuerpo_iz" >&nbsp;</td>
<td colspan="2" class="cuerpo_ce" >
<table id="tEmpresa" class="tablero" width="100%">
    <tbody></tbody>
</table>
</td>
<td class="cuerpo_de" >&nbsp;</td>
</tr>
<tr>
<td class="cuerpo_iz" >&nbsp;</td>
<td colspan="2" class="cuerpo_ce" ><label class="RojoGrande">Radicaciones del Grupo 3 - FONEDE</label></td>
<td class="cuerpo_de" >&nbsp;</td>
</tr>
<tr>
<td class="cuerpo_iz" >&nbsp;</td>
<td colspan="2" class="cuerpo_ce" >
<table id="tFonede" class="tablero" width="100%">
    <tbody></tbody>
</table>
</td>
<td class="cuerpo_de" >&nbsp;</td>
</tr>
<tr class="RojoGrande">
<td class="cuerpo_iz" >&nbsp;</td>
<td class="cuerpo_ce" width="50%" >
<label class="RojoGrande">
Recibe:&nbsp;<input type="text" id="usuarioRecibe" class="textopeq" onblur="verificar();" />&nbsp;Clave:&nbsp; <input type="password" class="textopeq" id="claveRecibe" onblur="vericaClave();" />
</label></td>
<td class="cuerpo_ce" >Entrega:&nbsp;
  <input type="password" class="textopeq" id="claveEntrega" onblur="vericaClave2();" /></td>
<td class="cuerpo_de" >&nbsp;</td>
</tr>
<tr class="RojoGrande">
<td class="cuerpo_iz" >&nbsp;</td>
<td colspan="2" class="cuerpo_ce" >&nbsp;</td>
<td class="cuerpo_de" >&nbsp;</td>
</tr>
<tr>
<td class="abajo_iz" >&nbsp;</td>
<td colspan="2" class="abajo_ce" ></td>
<td class="abajo_de" >&nbsp;</td>
</tr>
</table>
</center>

</body>

</html>
