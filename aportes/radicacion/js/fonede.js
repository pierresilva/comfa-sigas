var datosPersonaFonede = null;
var fechaRetiroCertificadoFonede = null;
/*
 * Inicializaci�n de los formularios de fonede
 */
function initFonede() {
	$("#fecha_ingr_otra_caja,#fecha_ret_otra_caja").datepicker({
		changeMonth: true,
		changeYear: true,
		onClose: function(dateText, inst){
			if($("#fecha_ingr_otra_caja").val() != "" && $("#fecha_ingr_otra_caja").val() != "")
				tiempoLaborado();
		}
	});
	$('#txtTiempo').unbind('blur');
	$("#txtTiempo").bind("focus",function(){
		tiempoLaborado();
	});
	
	inicializarChecklistFonede();
	//$("#fecharetiro_trayactual_dialogo_fonede").datepicker({changeMonth: true,changeYear: true});
	radicacionesFonedePendiente = false;
	tiempoFonedeCalculado = false;
	finBuscarRadFonede = false;
	fechaRetiroCertificado = false;
	puedeEntrarProcesoFonede = true;
	$("#span_tiempo_total_fonede").empty();
	$("#tbody_tabla_tray_otras_cajas tr").remove();
}


function inicializarChecklistFonede(){
	//lista de checkeo de documentos para postulaci�n a fonede
	$("#doc_formulario_fonede").attr("checked",false);
	$("#doc_cedula_fonede").attr("checked",false);
	$("#doc_cert_eps_fonede").attr("checked",false);
	$("#doc_cert_laboral_fonede").attr("checked",false);
	$("#check_decextrajuicio_fonede").attr("checked",false);
	$("#check_sentdivorcio_fonede").attr("checked",false);
	$("#check_ceculacony_fonede").attr("checked",false);
	$("#check_regcivilhijos_fonede").attr("checked",false);
	$("#check_cedula_padres").attr("checked",false);
	$("input:radio[id^='no']").trigger("click");
	$("#docs_fonede div:not(:first)").hide();
}

function nuevoRFonede(){
	radicacionesFonedePendiente = false;
	tiempoFonedeCalculado = false;
	finBuscarRadFonede = false;
	fechaRetiroCertificado = false;
	puedeEntrarProcesoFonede = true;
	$("#span_tiempo_total_fonede").empty();
	$("#tbody_tabla_tray_otras_cajas tr").remove();
	$('#txtIdentificacion').unbind('blur');
	$('#txtTiempo').unbind('blur');
	inicializarChecklistFonede();
}


/*
 * Funcionalidad de la lista de chequeo para las postulaciones a FONEDE
 * @author Oswaldo G.
 * 09-08-2011
 */
function setValidacionesDocumentosFonede(){
	$("input[name=convinculacion_fonede],input[name=convivencia_fonede],input[name=hijos_fonede],input[name=padres_fonede]").click(function(){
		 var name = $(this).attr("name");
		 var campo = name.replace("_fonede","");
		 var idCapaDocsFonede = "#docs_fonede";
		 switch(campo){
		 	case 'convinculacion': 
		 		($(this).val()=='s' ? $(idCapaDocsFonede +" div:eq(1)").show() : $(idCapaDocsFonede +" div:eq(1)").hide().find("input:checkbox").attr("checked",false));
		 		break;
		 	case 'convivencia':
		 		if ($(this).val()=='s'){
		 			$(idCapaDocsFonede +" div:eq(2)").show();
		 		}else{
		 			$("#no_conyactivo_fonede").trigger("click");
		 			$("#check_decextrajuicio_fonede").attr("checked",false);
		 			$("#check_sentdivorcio_fonede").attr("checked",false);
		 			$("#check_ceculacony_fonede").attr("checked",false);
		 			$(idCapaDocsFonede +" div:eq(2)").hide();
		 		}
		 		break;
		 	case 'hijos':
		 		if($(this).val()=='s'){
		 			$(idCapaDocsFonede +" div:eq(4)").show();
		 		}else{
		 			$(idCapaDocsFonede +" div:eq(4)").hide().find("input:checkbox").attr("checked",false);
		 		}
		 		break;
		 	case 'padres':
		 		if($(this).val()=='s'){
		 			$(idCapaDocsFonede +" div:eq(5)").show();
		 		}else{
		 			$(idCapaDocsFonede +" div:eq(5)").hide().find("input:checkbox").attr("checked",false);
		 		}
		 		break;
		 }
	});

	$("input[name=conyactivo_fonede]").bind('click',function(){
		 if($(this).val() == 's'){
			 $("#conyactivo_docs").show(); 
		 }else{
			 $("#conyactivo_docs").hide().find("input:checkbox").attr("checked",false);
			 
		 }
	});
}


function guardarFonede(){
	campo9 = $('#notas8').val();
	campo11 = $('#txtIdentificacion').val();
	campo13 = $('#tFolios8').val();
	campo18 = $('#sltTipoIdentificacion8').val();
	if($('#hdnEstadoPostulante').val()=='A'){
		//alert("El postulante tiene una afiliaci\u00F3n activa con la caja, \r\nDesea INACTIVAR la afiliaci\u00F3n actual?");
		//return;
	}else if($('#hdnIdpostulacion').val() != ''){
		//alert("No se puede grabar la postulaci\u00F3n, ya cuenta con una postulaci\u00F3n en el sistema");
		//return;
	}
	if(puedeEntrarProcesoFonede){
		return {campo_9: campo9, campo_11: campo11, campo_13: campo13, campo_18: campo18};
	}
	
}
	
function validarFonede(){
	var mensajes = "";
	if($('#sltTipoIdentificacion8').val() == '0'){
		mensajes+="<li>Falta Tipo identificaci\u00F3n del postulante</li>";
	}
	
	if($("#txtIdentificacion").val().length == 0){
		mensajes += "<li>Falta identificaci\u00F3n del postulante</li>";
	}else{
		num = $("#txtIdentificacion").val().length;
		if(num <3 || num>10)
			mensajes += "<li>La identificaci\u00F3n es de 3 a 11 d\u00CDgitos";
		num = parseInt($("#txtIdentificacion").val());
		
		if(isNaN(num)){
			mensajes += "<li>La identificaci\u00F3n es n\u00FAmerica!</li>";
		}
	}
	
	if($("#tFolios8").val().length == 0){
		mensajes+="<li>Falta n\u00FAmero de folios</li>";
	}else{
		num = $("#tFolios8").val();
		if(isNaN(num))
			mensajes += "<li>El folio es n\u00FAmerico!</li>";
		if(num==0)
			mensajes += "<li>N\u00FAmero de folios mayor que 0</li>";
	}
		
	/**
	 * Validaciones de lista de chequeo para postulaciones a fonede
	 */
	if(!$("#doc_formulario_fonede").is(':checked'))
		mensajes += "<li>Debe presentar el formulario de postulaci\u00F3n.</li>";
	
	if(!$("#doc_cedula_fonede").is(':checked'))
		mensajes += "<li>Debe presentar fotocopia de la c\u00E9dula del postulante.</li>";
	
	if(!$("#doc_cert_eps_fonede").is(':checked'))
		mensajes += "<li>Debe presentar certificado de la EPS.</li>";
		
	var conVinculacion = $("input[name=convinculacion_fonede]:checked").val();
	if(conVinculacion == 's'){
		if(!$("#doc_cert_laboral_fonede").is(':checked'))
			mensajes += "<li>Debe presentar certificado laboral o carta de terminaci\u00F3n de contrato indicando fecha de retiro.</li>";
	}
		
	var convivencia = $("input[name=convivencia_fonede]:checked").val();
	if(convivencia == 's'){
		var conyAtivo = $("input[name=conyactivo_fonede]:checked").val();
		if(conyAtivo == 's'){
			var declExtraJuicio = $("#check_decextrajuicio_fonede").is(':checked');
			var sentenciaDivorcio = $("#check_sentdivorcio_fonede").is(':checked');
			
			if(!declExtraJuicio && !sentenciaDivorcio){
				mensajes += "<li>Debe presentar Declaraci\u00F3n extra juicio \u00F3 sentencia de divorcio indicando que no hay relaci\u00F3n de convivencia.</li>";
			}
		}
		
		if(!$("#check_ceculacony_fonede").is(':checked')){
			mensajes += "<li>Debe presentar fotocopia de la cedula del c\u00F3nyuge.</li>";
		}
			
	}
		
	var hijosHerm = $("input[name=hijos_fonede]:checked").val();
	if(hijosHerm == 's'){
		if(!$("#check_regcivilhijos_fonede").is(':checked'))
			mensajes += "<li>Debe presentar registro civil.</li>";
	}
	
	var padres = $("input[name=padres_fonede]:checked").val();
	if(padres == 's'){
		if(!$("#check_cedula_padres").is(':checked'))
			mensajes += "<li>Debe presentar fotocopia de la c\u00E9dula de el(los) padre(s).</li>";
	}
	
	// El usuario debe postularse con al menos un beneficiario (padre, hijo/hermano � c�nyuge)
	if(convivencia == 'n' && hijosHerm == 'n' && padres == 'n'){
		mensajes += "<li>Debe postularse al menos con un beneficiario.</li>";
	}
	
	if(!puedeEntrarProcesoFonede){
		mensajes += "<li>No puede radicar una solicitud al subsidio FONEDE. El usuario puede tener radicaciones y/o postulaciones a FONEDE pendientes.</li>";
	}
	
	return mensajes;	
}

function buscarRadicacionesFonede(idPersona, procesadoR){
	$.ajax({
		url: '../../phpComunes/buscarRadicacionesFonede.php',
		async: false,
		dataType: 'json',
		data: ({idpersona:idPersona, procesado: procesadoR}),
		success: function(datos){
			if(datos != 0){
				alert("La persona TIENE RADICACIONES para solicitud de subsidio FONEDE SIN PROCESAR.\n No se realizar\u00E1 ning\u00FAn c\u00E1lculo.");
				radicacionesFonedePendiente = true;
				puedeEntrarProcesoFonede = false;
			
			}
		}
	});
	return 1;
}

function buscarRadicacionesFonedeIdentifPersona(tipoDoc,numDoc,procesadoR){
	$.ajax({
		url: '../../phpComunes/buscarRadicacionesFonedePorTipoNumIdentif.php',
		async: false,
		dataType: 'json',
		data: ({tipoid:tipoDoc, identificacion: numDoc, procesado: procesadoR}),
		success: function(datos){
			if(datos != 0){
				alert("La persona TIENE RADICACIONES para solicitud de subsidio FONEDE SIN PROCESAR.\n No se realizar\u00E1 ning\u00FAn c\u00E1lculo.");
				radicacionesFonedePendiente = true;
				puedeEntrarProcesoFonede = false;
			}
		}
	});
	return 1;
}

/*
 * Verifica si la persona ya ha culminado su proceso en fonede
 * */
function tienePostulacionesTerminadas(idPersona){
	$.ajax({
		url: '../../phpComunes/buscarPostulacionesFonedeCulminadas.php',
		async: false,
		dataType: 'json',
		data: ({idpersona:idPersona}),
		success: function(datos){
			if(datos != 0){
				alert("La persona ya ha sido beneficiario del subsidio de FONEDE (Banderas X, S, C y T).\n No se realizar\u00E1 ning\u00FAn c\u00E1lculo.");
				puedeEntrarProcesoFonede = false;
			}
		}
	});
	return;
}

/*
 * Verifica si la persona se encuentra activa en proceso de FONEDE
 * */
function tienePostulacionesActivas(idPersona){
	$.ajax({
		url: '../../phpComunes/buscarPostulacionesFonedeActivas.php',
		async: false,
		dataType: 'json',
		data: ({idpersona:idPersona}),
		success: function(datos){
			if(datos != 0){
				alert("La persona tiene una postulaci\u00F3n actualmente ACTIVA para FONEDE (Bandera A, P o G).\n No se realizar\u00E1 ning\u00FAn c\u00E1lculo.");
				puedeEntrarProcesoFonede = false;
			}
		}
	});
	return;
}


/**
 * @author tiogg
 * 05-08-2011
 */
function mostrarTrayectoriasPersona(persona){
	$.ajax({
		url: '../../phpComunes/buscarHistoricoFonede.php',
		async: false,
		dataType: 'json',
		data: ({idPersona:persona.idpersona}),
		success: function(datos){
			if(datos==0){
				//MENSAJE("No existe en nuestra base de datos, postulaci�n sin vinculaci�n");
				return false;
			}else{
				
				for(var c=0; c<datos.length; c++){
					var trayectoria = datos[c];
					var htmlTr = "<tr><td>"+ (c+1) +"</td>" +
					 "<td>"+ trayectoria.razonsocial +"</td>" +
					 "<td>"+ trayectoria.fechaingreso +"</td>" +
					 "<td>"+ trayectoria.fecharetiro +"</td>" +
					 "<td>"+ trayectoria.dias +"</td>" +
					 "<td>"+ trayectoria.telefono +"</td></tr>";
					 $("#tbody_tabla_hist_laboral").append(htmlTr);
				}
			}
		}
	});
}

function calcularTiempoFonede(trayecorias){
	var x = '';
	var y = $('#txtFechaFinal').val();
	var arr1 = x.split('/');
	var arr2 = y.split('/');

	if(!arr1 || !arr2 || arr1.length != 3 || arr2.length != 3) {
		return;
	}
	 
	var dt1 = new Date();
	dt1.setFullYear(arr1[2], arr1[0], arr1[1]);
	var dt2 = new Date();
	dt2.setFullYear(arr2[2], arr2[0], arr2[1]);
	var tiempodias = (dt2.getTime() - dt1.getTime()) / (60 * 60 * 24 * 1000);
	tiempodias = parseInt(tiempodias);
		
	
	if(tiempodias <= 0){
		$('#txtFechaInicial, #txtFechaFinal').addClass('ui-state-error');
		return false;
	}else{
		$('#txtFechaInicial, #txtFechaFinal').removeClass('ui-state-error');
		$('#txtTiempo').val(tiempodias);
		
		var tt = 0;
		$('input.tiempo').each(function(){
			tt += parseInt($(this).val());
		});
		
		$('#txtTiempoTotal').val(tt);
	}
}

function recalcularTiempoFonede(tiempoRestar, idFilaBorrar){
	var restar = (tiempoRestar == null)? 0 : parseInt(tiempoRestar);
	var trBorrar = (idFilaBorrar == null)?'':idFilaBorrar;
	var mensajeVinculacion = '';
	var totalAfiliacionesCaja = (isNaN($("#totalTiempoAplicarFonede").val()))? 0 : parseInt($("#totalTiempoAplicarFonede").val()); // tiempo total para aplicar a fonede con las trayectorias de COMFAMILIAR HUILA
	
	if(totalAfiliacionesCaja > 365)
		mensajeVinculacion = "\nPuede realizar la postulaci\u00F3n CON VINCULACI\u00D3N.";
	
	var totalAfiliacionesOtrasCajas = 0; // n�mero de d�as que la persona ha estado afiliada con otras cajas distintas de comfamiliar huila

	var arrOtrasCajas = [];
	var tiempoMenor;
	var tiempoMayor;
	$('span[id^="span_tiempo_otras_cajas_"]').each(function(i,span){
		var tiempo = $("#"+span.id).text();
		totalAfiliacionesOtrasCajas = totalAfiliacionesOtrasCajas + parseInt(tiempo);
		arrOtrasCajas[i] = parseInt(tiempo);
	});

	if(trBorrar != ''){
		$(trBorrar).remove();
	}
	var totalAfiliacionesParaAplicar = totalAfiliacionesCaja + totalAfiliacionesOtrasCajas;

	if(totalAfiliacionesParaAplicar > 0 && restar > 0){
		totalAfiliacionesParaAplicar = (totalAfiliacionesParaAplicar - restar);
	}

	$("#span_tiempo_total_fonede").text(totalAfiliacionesParaAplicar);
	
	if(totalAfiliacionesParaAplicar > 365){
		mensajeVinculacion = "\nPuede realizar la postulaci\u00F3n CON VINCULACI\u00D3N.";
		$("#si_convinculacion_fonede").trigger('click');
	}else{
		mensajeVinculacion = "\nPuede realizar la postulaci\u00F3n SIN VINCULACI\u00D3N.";
		$("#no_convinculacion_fonede").trigger('click');
	}
	
	alert("El afiliado puede postularse a FONEDE."+ mensajeVinculacion);
	
	return totalAfiliacionesParaAplicar;
	
}

/**
 * Calcular tiempo laborado en di�logo de agregaci�n de trayectorias con otras cajas (FONEDE)
 *
 */
var tiempoLaborado = function(){
	var fl = $(this).parents('tr').attr('id');
	var x = $('#fecha_ingr_otra_caja').val();
	var y = $('#fecha_ret_otra_caja').val();
	
	var regexpFechas = /^((\d{4}))\/(|(0[1-9])|(1[0-2]))\/((0[1-9])|(1\d)|(2\d)|(3[0-1]))$/;
	if(!x.match(regexpFechas) || !y.match(regexpFechas)){
		$("#fecha_ingr_otra_caja").addClass('ui-state-error');
		$("#fecha_ret_otra_caja").addClass('ui-state-error');
		$("#txtTiempo").val("");
	}else{
		$("#fecha_ingr_otra_caja").removeClass('ui-state-error');
		$("#fecha_ret_otra_caja").removeClass('ui-state-error');
	}
	
	var arr1 = x.split('/');
	var arr2 = y.split('/');

	if(!arr1 || !arr2 || arr1.length != 3 || arr2.length != 3) {
		return;
	}
	 
	var dt1 = new Date();
	dt1.setFullYear(arr1[0], arr1[1]-1, arr1[2]);
	var dt2 = new Date();
	dt2.setFullYear(arr2[0], arr2[1]-1, arr2[2]);
	
	var fechaHoy = new Date();
	var marcaTiempoHoy = fechaHoy.getTime(); // en milisegundos
	var fechaHace3Agnos = new Date();
	fechaHace3Agnos.setFullYear(fechaHoy.getFullYear()-3, fechaHoy.getMonth(), fechaHoy.getDate(), 0, 0, 0, 0);
	var marcaTiempoHace3Agnos = fechaHace3Agnos.getTime();
	var marcaTiempoDt1 = dt1.getTime();
	var marcaTiempoDt2 = dt2.getTime();
	if(marcaTiempoHoy < marcaTiempoDt2 || marcaTiempoHoy < marcaTiempoDt1){
		$('#fecha_ingr_otra_caja, #fecha_ret_otra_caja').addClass('ui-state-error');
		tt = 0;
		$('#txtTiempo').val(parseInt(tt));
		return false;
	}

	if((marcaTiempoDt1 <= marcaTiempoHace3Agnos && marcaTiempoDt2 >= marcaTiempoHace3Agnos) ||
	   (marcaTiempoDt1 >= marcaTiempoHace3Agnos && marcaTiempoDt2 >= marcaTiempoHace3Agnos) ||
	   (marcaTiempoDt2 >= marcaTiempoHace3Agnos)
	  ){
		
		var marcaNivelInferior = (marcaTiempoDt1 <= marcaTiempoHace3Agnos)? marcaTiempoHace3Agnos : marcaTiempoDt1;
		var marcaNivelSuperior = (marcaTiempoDt2 >= marcaTiempoHoy)? marcaTiempoHoy : marcaTiempoDt2;
		var tiempodias = (marcaNivelSuperior - marcaNivelInferior) / (60 * 60 * 24 * 1000);
		tiempodias = tiempodias;
		
	}else{
		tiempodias = 0;
	}
	
	if(tiempodias <= 0){
		$('#fecha_ingr_otra_caja, #fecha_ret_otra_caja').addClass('ui-state-error');
		tt = 0;
		$('#txtTiempo').val(tt);
		return false;
	}else{
		$('#fecha_ingr_otra_caja, #fecha_ret_otra_caja').removeClass('ui-state-error');
		$('#txtTiempo').val(tiempodias);
		
		var tt = 0;
		$('input.tiempo').each(function(){
			tt += parseInt($(this).val());
		});
	}
};

/**
 * Valida si el campo que ejecuta el m�todo est� vac�o
 * @returns
 */
var tieneCamposVacios = function(){
	var valor = $.trim($(this).val());
	if(valor.length == 0){
		$(this).addClass('ui-state-error');
		return true;
	}else{
		if($(this).hasClass('ui-state-error')){
			$(this).removeClass('ui-state-error');
		}
		return false;
	}
};

var buscarCajas = function(request, response) {
	if ( request.term in cache ) {
		response( cache[ request.term ] );
		return;
	}
	request.tipo = 'CCF';
	$.ajax({
		url: "../../fonede/proc/buscarAdministradora.php",
		dataType: "json",
		data: request,
		success: function( data ) {
			cache[ request.term ] = data;
			response( data );
		}
	});
};

/**
 * Validaci�n de campos auto-complete para el di�logo de trayectorias en otras cajas
 * @returns
 */
var validarAutoCompletar = function(){
	var valHdn = $(this).nextAll('input[type="hidden"]').val();
	if(valHdn.length == 0){
		$(this).addClass('ui-state-error');
	}else{
		if($(this).hasClass('ui-state-error')){
			$(this).removeClass('ui-state-error');
		}
	}
};



function verDatosFormularioFonede(){
	$('#sltTipoIdentificacion8').html($('#select option').clone());
	$("#div8").css({display:"block"});
	//$('#docs_fonede').css({display:"block"});
	//$("#txtIdentificacion,#tFolios8").numeric();
	$("#dialog-fecharetiro_fonede").dialog({
		autoOpen: false,
		height: 300,
		width: 500,
		modal: true,
		buttons: {
			'Consultar': function() {
				var fechaRetiroCertificado = $('#fecharetiro_trayactual_fonede').val();
				if(fechaRetiroCertificado == ''){
					fechaRetiroCertificado = false;
				}
				if(fechaRetiroCertificado != false){
					var regexpFechaRetiro = /^(|(0[1-9])|(1[0-2]))\/((0[1-9])|(1\d)|(2\d)|(3[0-1]))\/((\d{4}))$/;
					if(!fechaRetiroCertificado.match(regexpFechaRetiro)){
						fechaRetiroCertificado = false;
						alert("Verifique la fecha de retiro (formato: MM/DD/AAAA).");
					}else{
						abrirTabsFonede(datosPersonaFonede,fechaRetiroCertificado);
						$(this).dialog('close');
					}
				}
			},
			Cancelar: function() {
				$(this).dialog('close');
				$("#dialog-fecharetiro_fonede").dialog("destroy");
			}
		}
	});
	$("#fecharetiro_trayactual_fonede").datepicker({
		changeMonth: true,
		changeYear: true,
		maxDate: "+0D",
		dateFormat: 'mm/dd/yy'
	});
	
	$('#txtIdentificacion').bind('blur', function(){
		removeAllTabs();
		var fechaActual = new Date();
		var agnoActual = fechaActual.getFullYear();
		var agnoHace3Agnos = agnoActual-3;
		var fecha3Agnos = new Date();
		fecha3Agnos.setFullYear(agnoHace3Agnos);
		var marca3Agnos = fecha3Agnos.getTime();
		
		
		$('#nombrePostulante').html('');
		$('#hdnIdpostulante').val('');
		$('#hdnIdpostulacion').val('');
		$('#hdnEstadoPostulante').val('X');
		$('div.cont-hist:gt(0)').remove();
		var tipIdentif = parseInt($("#sltTipoIdentificacion8").val());
		if($('#txtIdentificacion').val().length==0 || tipIdentif==0){
			//MENSAJE("Falta identificaci\u00F3n del postulante");
			alert("Falta identificaci\u00F3n del postulante");
		}else{
			
			num = $('#txtIdentificacion').val().length;
			if(num < 3 || num > 11){
				//MENSAJE("La identificacion es de 3 a 11 d�gitos");
				alert("La identificacion es de 3 a 11 d�gitos");
			}else{
				num = parseInt($('#txtIdentificacion').val());
				if(isNaN(num) || tipIdentif==0){
					//MENSAJE("La identificacion es n\u00FAmerica!");
					alert("La identificacion es n\u00FAmerica!");
				}else{
		
					$.ajax({
						url: '../../phpComunes/buscarPersona3.php',
						async: false,
						dataType: 'json',
						data: ({v1:$('#txtIdentificacion').val(), v0:$('#sltTipoIdentificacion8').val()}),
						success: function(datos){
							if(datos==0){
								//MENSAJE("No existe en nuestra base de datos, postulaci�n SIN VINCULACI\u00D3N.");
								alert("No existe en nuestra base de datos, postulaci\u00F3n SIN VINCULACI\u00D3N.");
								//$("#no_convinculacion_fonede").attr('checked',true);
								$("#no_convinculacion_fonede").trigger('click');
								//$("#docs_fonede div:eq(1)").hide();
								
								var finBuscarRadFonede = buscarRadicacionesFonedeIdentifPersona($('#sltTipoIdentificacion8').val(), $('#txtIdentificacion').val(),'N');
								
								if(confirm("La persona presenta certificados de afiliaciones a otras cajas?.")){
									$("#opt_afiotrascajas_si").attr('checked',true);
									$("#div-nuevohistorial").show();
									$("#txtCajas").focus();
								}
								
								return false;
							}else{
								
								// buscar radicaciones de solicitud de subsidio FONEDE
								var finBuscarRadFonede = buscarRadicacionesFonede(datos[0].idpersona,'N');
								
								if(datos[0].idpostulacion != null){
									//MENSAJE("Esta persona ya cuenta con una postulaci�n a FONEDE");
									alert("Esta persona ya cuenta con POSTULACIONES A FONEDE. \nVerifique en la pesta\u00F1a de postulaciones.");
									puedeEntrarProcesoFonede = false;
							//console.log(puedeEntrarProcesoFonede);
									tienePostulacionesTerminadas(datos[0].idpersona);
									tienePostulacionesActivas(datos[0].idpersona);
								}
								
								
								if(datos[0].estadoafil == 'A' && puedeEntrarProcesoFonede){
									//MENSAJE("El postulante tiene una afiliaci�n activa con la caja");
									// @TODO Se debe utilizar un di�logo UI de Jquery, para capturar la fecha con un Datepicker
									//fechaRetiroCertificado = prompt("El postulante tiene una afiliaci\u00F3n activa con la caja.\nVerifique las trayectorias en la pesta�a Afiliado.","Fecha de retiro MM/DD/AAAA");
									
									datosPersonaFonede = datos;
									fechaRetiroCertificadoFonede = fechaRetiroCertificado;
									$("#dialog-fecharetiro_fonede").dialog('open');
									$("#p_inactivar_afiliado_fonede").show();
									$("#p_inactivar_afiliado_fonede").bind('click',function(){
										alert("se inactiva el afiliado");
									});
								}else{
									abrirTabsFonede(datos,false);
								}
							}
						}					
					});
				}
			}
		}
	});
	
	$( "input[name=opt_afiotrascajas_fonede]").bind("click",function(){
		var seleccionado = $(this).val();
		if(seleccionado == 's'){
			$("#div-nuevohistorial").show();
		}else{
			$("#div-nuevohistorial").hide();
		}
		$("#div-nuevohistorial input[type=text]").val("");
	});
		
		
	$( "#txtCajas" ).autocomplete({
		minLength: 2,
		autoFocus: true,
		delay: 0,
		source: buscarCajas
	});
		
	$('#txtCajas').bind( "autocompleteselect", function(event, ui){
		var objCaja = ui.item;
		
		$(this).next('input[type="hidden"]').val(objCaja.id);
		
		//Borra el valor que existe de seleccion para evitar que se tome el que ha sido elegido y despues sea modificado
		$('#txtCajas').bind('focus', function(){
			$(this).val('');
			$(this).next('input[type="hidden"]').val('');
		});
		
		//Valida que el valor seleccionado se encuentre dentro de los posibles
		$('#txtCajas').bind('blur', validarAutoCompletar);
		
		//Valida campos vacios
		$('#fecha_ingr_otra_caja, #fecha_ret_otra_caja').bind('blur', function(){
			tieneCamposVacios();
			tiempoLaborado();
		});
	});
		
	$("#btn_agregar_tray_otra_caja").bind('click', function(){
	
		var x = $('#fecha_ingr_otra_caja').val();
		var y = $('#fecha_ret_otra_caja').val();
		
		if(x != '' && y != '' && $("#txtCajas").val() != ""){
			var tiempodias = $('#txtTiempo').val();
		
			if(parseInt(tiempodias) > 0){
				var numero = $('tr[id^="tr_tray_otras_cajas_"]').length +1;
				var htmlTr = "<tr id=\"tr_tray_otras_cajas_"+ numero +"\">" +
							 "<td>" + $("#txtCajas").val() +"</td>" +
							 "<td>"+ x +"</td>" +
							 "<td>"+ y  +"</td>" +
							 "<td><span id='span_tiempo_otras_cajas_"+ numero +"'>"+ tiempodias  +"</span></td>" +
							 "<td><input type='button' id='btn_quitar_tray_otras_cajas_"+ numero +"' value='Quitar' /></td>" +
							 "</tr>";
				$("#tbody_tabla_tray_otras_cajas").append(htmlTr);
				
				$("#txtCajas").val("");
				$("#hdnCajas").val("");
				$("#fecha_ingr_otra_caja").val("");
				$("#fecha_ret_otra_caja").val("");
				$("#txtTiempo").val("");
				
				recalcularTiempoFonede(0);
				
				$("#btn_quitar_tray_otras_cajas_"+ numero).bind('click',function(){
					var numero = this.id.replace("btn_quitar_tray_otras_cajas_","");
					var tr = "tr_tray_otras_cajas_" + this.id.replace("btn_quitar_tray_otras_cajas_","");
					var tiempo = $("#span_tiempo_otras_cajas_"+numero).text();
					var tiempoNuevo = recalcularTiempoFonede(tiempo, "#"+tr);
					$("#span_tiempo_total_fonede").text(tiempoNuevo);
				});
			}
		}else{
			alert("Verifique los datos de la trayectoria a agregar.");
		}
	
	});
} //fin funcion que muestra el formulario de datos para FONEDE

function abrirTabsFonede(datos,fechaRetiroCertificado){
	$('#nombrePostulante').html(datos[0].pnombre+' '+datos[0].snombre+' '+datos[0].papellido+' '+datos[0].sapellido);
	$('#hdnIdpostulante').val(datos[0].idpersona);
	$('#hdnEstadoPostulante').val(datos[0].estadoafil);
	$('#hdnIdpostulacion').val(datos[0].idpostulacion);
	$('#tabs').tabs('add',"../../aportes/trabajadores/tabConsulta/fichaTab.php?flag=1&v0="+ datos[0].idpersona +"&fechaRetiroUltTray="+ fechaRetiroCertificado +"&fonede=true","Afiliado");
	$('#tabs').tabs('select',1);
	$('#tabs').tabs("add","../../aportes/trabajadores/tabConsulta/planillaTab.php?v0="+ datos[0].idpersona,"Planilla");
	$('#tabs').tabs("add","../../aportes/trabajadores/tabConsulta/postulacionesFonedeTab.php?v0="+ datos[0].idpersona,"Postulaciones");
}