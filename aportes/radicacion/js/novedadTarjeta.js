/**
 * @author Oswaldo Gonz�lez
 * @date 20-04-2012
 * Script que gestiona el comportamiento del formulario de radicaci�n de novedad de tarjetas
 **/


/**
 * Inicializa el comportamiento de los controles del formulario de radicaci�n de novedades de tarjeta.
 */
function initNovedadTarjeta(){
	estadoTarjeta = null;
	idp=0;
	idbono=0;
	identidadVerificada = false;
	
	//Dialog ayuda
	$("#ayudaNovedadTarjeta").dialog({
		autoOpen: false,
		height: 500,
		width: 750,
		draggable:true,
		modal:false,
		open: function(evt, ui){
			$('#ayuda').html('');
			$.get(URL +'help/tarjetas/manualayudaNovedadtarjeta.html',function(data){
				$('#ayuda').html(data);
			});
		}
	});
	
	// campo de b�squeda por bono o por identificaci�n
	$("#buscarPorNovedad").bind('change',function(){
 		$("#lnombrenovedad,#lbononovedad,#lestadonovedad,#lfechasolnovedad,#lsaldonovedad,#lubicacionnovedad").html(" ");
 		$("#numero5,#tipoINovedad,#txtBonoNovedad").val('');
	});
	
	//Funcionamiento controles de documentaci�n de bloqueo de tarjetas
	$("input[name=motnovedad]").click(function(){
		var valor = $(this).attr("id").replace("motb_novedad_","");
		if(valor == 'perdida'){
			$("#docsNovedadTarjeta p:eq(0)").show();
			$("#docsNovedadTarjeta p:eq(1)").hide().find("input:checkbox").attr("checked",false);
		}else{
			$("#docsNovedadTarjeta p:eq(1)").show();
			$("#docsNovedadTarjeta p:eq(0)").hide().find("input:checkbox").attr("checked",false);
		}
	});
	
	$("#docsNovedadTarjeta input:checked").attr("checked",false);
	$("#docsNovedadTarjeta p").hide();
	
	// tipo de motivo de la novedad: B,S � H
	$("input[name=tipoNovedad]").bind("click",function(){
		var tipoNov = $(this).attr("id").replace("tipoNovedad_","");
		
		switch(tipoNov){
			case 'b': // bloqueo
				$("#novedad_b").val("").attr("disabled",false);
				$("#novedad_s").val("").attr("disabled",true);
				$("#novedad_h").val("").attr("disabled",true);
				$("#tr_verifica_afiliado").hide();
				break;
			case 's': // suspenci�n
				$("#novedad_b").val("").attr("disabled",true);
				$("#novedad_s").val("").attr("disabled",false);
				$("#novedad_h").val("").attr("disabled",true);
				$("#tr_verifica_afiliado").show();
				break;
			case 'h': // habilitaci�n
				$("#novedad_b").val("").attr("disabled",true);
				$("#novedad_s").val("").attr("disabled",true);
				$("#novedad_h").val("").attr("disabled",false);
				$("#tr_verifica_afiliado").show();
				break;
		}
	});
		
	$("#novedad_b,#novedad_s,#novedad_h").bind("change",function(){
		// DETERIORO O MALA CALIDAD
		if($(this).val() == 2865){
			$("#motb_novedad_deteriodo").trigger("click");
		}else if($(this).val() == 2866 || $(this).val() == 2867){
			// PERDIDA O ROBO
			$("#motb_novedad_perdida").trigger("click");
		}else{
			$("#docsNovedadTarjeta input:checked").attr("checked",false);
			$("#docsNovedadTarjeta p").hide();
		}
	});
	
	$("#btnValidRespuestas").bind("click",function(){
		validarPreguntasSeguridad();
	});
	
}


/**
 * Mostrar campos de b�squeda de tarjetas seg�n el valor de la lista
 */
function mostrarTarjetaNovedad(){
	var opt=$("#buscarPorNovedad").val();
	if(opt==0){
		$("#tNumeroNovedad").css("display", "none"); 
		$("#tBonoNovedad").css("display", "none"); 
		$("#numero4").val("");
	}

	if(opt==1){
		$("#tNumeroNovedad").css("display", "block"); 
		$("#tBonoNovedad").css("display", "none"); 
		$("#tipoINovedad").focus();
	}

	if(opt==2){
		$("#tBonoNovedad").css("display", "block"); 
		$("#tNumeroNovedad").css("display", "none"); 
		$("#txtBonoNovedad").focus();
	}
}

/**
 * Realiza una b�squeda de los datos del afiliado y la tarjeta por medio del n�mero de identificaci�n del afiliado
 * @returns {Boolean}
 */
function buscarPersonaTarjetaNovedad(){
	identidadVerificada = false;
	estadoTarjeta = null;
	$("#notas20").val("");
	
	$("#tipoNovedad_b,#tipoNovedad_s,#tipoNovedad_h").attr("checked",null);
	$("#novedad_b,#novedad_s,#novedad_h").val("").trigger("change").attr("disabled",true);
	var tipoI = $("#tipoINovedad").val();
	var numero = $("#numero5").val();
	
	$("#lbononovedad").html('');
	$("#lfechasolnovedad").html('');
	$("#lestadonovedad").html('');
	$("#lnombrenovedad").html('');
	$("#lsaldonovedad").html('');
	$("#lubicacionnovedad").html('');
	$("#txtBonoNovedad").val('');

	$("#tr_verifica_afiliado").hide();
	$("#ol_preguntas_seguridad li").hide();
	$(".opciones_respesta").empty();
	
	if(numero.length==0){
		alert("Digite el n\u00FAmero de identificaci\u00F3n!");
		$("#numero5").focus();
		return false;
	}
	
	$.getJSON(URL+'phpComunes/buscarPersona2.php',{v0:tipoI,v1:numero},function(data){
		if(data==0){
			alert("Lo lamento, el n\u00FAmero no existe!");
			$("#numero5").val("");
			$("#numero5").focus();
			return false;
		}
		var nom = "";
		$.each(data,function(i,fila){
			nom = fila.pnombre+" "+fila.snombre+" "+fila.papellido+" "+fila.sapellido;
			idp=fila.idpersona;
		});
		
		$("#lnombrenovedad").html(nom);
		$.getJSON(URL+'phpComunes/pdo.buscar.tarjeta.act.php',{v0:idp},function(datos){
			if(datos==0){
				alert("Lo lamento, no hay bono asociado a ese n�mero!");
				return false;
			}
			$.each(datos,function(i,fila){
				idbono = fila.idtarjeta;
				estadoTarjeta = fila.estado;
				var ubicacion = "";
				if(fila.ubicacion == '01')
					ubicacion = "Neiva";
				else if(fila.ubicacion == '02')
					ubicacion = "Garzon";
				else if(fila.ubicacion == '03')
					ubicacion = "Pitalito";
				else if(fila.ubicacion == '04')
					ubicacion = "La Plata";
				if(fila.estado != 'B'){
					$("#lbononovedad").html(fila.bono);
					$("#lfechasolnovedad").html(fila.fechasolicitud);
					$("#lestadonovedad").html(fila.estado);
					if(fila.saldo == null)
						fila.saldo = '0';	
					$("#lsaldonovedad").html(formatCurrency(fila.saldo));
					$("#lubicacionnovedad").html(ubicacion);
					$("#txtBonoNovedad").val(fila.bono);
					$("[name=tipoNovedad]").attr("checked",false);
					$("[name=novedad]").val("").attr("checked",false).attr("disabled",true);
					$("#tipoNovedad_b").focus();
					
					
					$.getJSON(URL+"aportes/radicacion/generarPreguntasVerifica.php",{idafiliado: idp},function(response){
						$.each(response,function(c,pregunta){
							pregunta.desordenar = function() {
								for ( var i = this.length-1; i > 0; i-- ) {
									var j = Math.floor( i * Math.random() );
									var tmp = this[ j ];
									this[ j ] = this[ i ];
									this[ i ] = tmp;
								}
								return this;
							};
							pregunta.desordenar();
							$("#li_pregunta_"+c).show();
							$.each(pregunta,function(ind,respuesta){
								var clase = (respuesta.correcta)? "correcta":"";
								if(c == "saldotarjeta")
									respuesta.respuesta = formatCurrency(respuesta.respuesta);
								var campoRespuesta = "<li id='li_respuesta_"+c+"_"+ind+"' ><input type='radio' name='respuesta"+ c +"' id='respuesta"+ c +"_"+ ind +"' class='"+ clase +"' /> <label for='respuesta"+ c +"_"+ ind +"'>"+ respuesta.respuesta +"</label></li>";
								$("#opciones_pregunta_"+c).append(campoRespuesta);
							});
						});
					});
					
					
				}else{
					alert("La tarjeta tiene estado BLOQUEADO, no puede continuar.");
					$("#lbononovedad").html(fila.bono);
					$("#lfechasolnovedad").html(fila.fechasolicitud);
					$("#lestadonovedad").html(fila.estado);
					$("#lnombrenovedad").html(nom);
					if(fila.saldo == null)
						fila.saldo = '0';	
					$("#lsaldonovedad").html(formatCurrency(fila.saldo));
					$("#lubicacionnovedad").html(ubicacion);
					$("#txtBonoNovedad").val(fila.bono);
					$("[name=tipoNovedad]").attr("checked",false);
					$("[name=novedad]").val("").attr("checked",false).attr("disabled",true);
					$("#tipoINovedad").focus();
					return false;
				}
				
				
				return;
			});
		});
		
	});
}

/*
 * Realiza una b�squeda de los datos del afiliado y la tarjeta por medio del n�mero de bono
 */
function buscarPersona2TarjetaNovedad(){
	identidadVerificada = false;
	estadoTarjeta = null;
	var bono=$.trim($("#txtBonoNovedad").val());
	var idp=0;
	
	$("#notas20").val("");
	$("#tipoNovedad_b,#tipoNovedad_s,#tipoNovedad_h").attr("checked",null);
	$("#novedad_b,#novedad_s,#novedad_h").val("").trigger("change").attr("disabled",true);
	$("#lbononovedad").html('');
	$("#lfechasolnovedad").html('');
	$("#lestadonovedad").html('');
	$("#lnombrenovedad").html('');
	$("#lsaldonovedad").html('');
	$("#lubicacionnovedad").html('');
	$("#txtBonoNovedad").val('');
	$("#tr_verifica_afiliado").hide();
	$("#ol_preguntas_seguridad li").hide();
	$(".opciones_respesta").empty();
	
	if(bono.length==0){
		alert("Digite el n\u00FAmero de Bono!");
		$("#txtBonoNovedad").focus();
		return false;
	}

	$.ajax({
		url: URL+'phpComunes/pdo.buscar.tarjeta.act2.php',
		type: "POST",
		async: false,
		data: {v0:bono},
		dataType: "json",
		success: function(datos){
			if(datos==0){
				alert("Lo lamento, el bono no existe!");
	             limpiarCamposBloqueo();
				return false;
			}
			idbono=datos[0].idtarjeta;
			idp=datos[0].idpersona;
			$("#lbononovedad").html(datos[0].bono);
			$("#lfechasolnovedad").html(datos[0].fechasolicitud);
			$("#lsaldonovedad").html(formatCurrency(datos[0].saldo));
			$("#lestadonovedad").html(datos[0].estado);
			$("#lubicacionnovedad").html(ubicacion);
			
			var ubicacion = "";
			if(datos[0].ubicacion == '01')
				ubicacion = "Neiva";
			else if(datos[0].ubicacion == '02')
				ubicacion = "Garzon";
			else if(datos[0].ubicacion == '03')
				ubicacion = "Pitalito";
			else if(datos[0].ubicacion == '04')
				ubicacion = "La Plata";
			
			estadoTarjeta=datos[0].estado;
			var tarjeta = datos[0];
			$.getJSON(URL+'phpComunes/pdo.buscar.persona.id.php',{v0:idp},function(data){
				if(data==0){
					alert("Lo lamento, el n\u00FAmero no existe!");
					$("#numero5").val("");
					$("#numero5").focus();
					return false;
				}
				$.each(data,function(i,fila){
					var nom=fila.pnombre+" "+fila.snombre+" "+fila.papellido+" "+fila.sapellido;
					$("#lnombrenovedad").html(nom);
					$("#tNumeroNovedad").show();
					$("#tipoINovedad").val(fila.idtipodocumento);
					$("#numero5").val(fila.identificacion);
					return;
				});
			});
			
			if(estadoTarjeta != 'B'){
				$("#lbononovedad").html(tarjeta.bono);
				$("#lfechasolnovedad").html(tarjeta.fechasolicitud);
				$("#lestadonovedad").html(tarjeta.estado);
				if(tarjeta.saldo == null)
					tarjeta.saldo = '0';	
				$("#lsaldonovedad").html(formatCurrency(tarjeta.saldo));
				$("#lubicacionnovedad").html(ubicacion);
				$("#txtBonoNovedad").val(tarjeta.bono);
				$("#tipoNovedad_b").focus();
				
				$.getJSON(URL+"aportes/radicacion/generarPreguntasVerifica.php",{idafiliado: idp},function(response){
					$.each(response,function(c,pregunta){
						pregunta.desordenar = function() {
							for ( var i = this.length-1; i > 0; i-- ) {
								var j = Math.floor( i * Math.random() );
								var tmp = this[ j ];
								this[ j ] = this[ i ];
								this[ i ] = tmp;
							}
							return this;
						};
						pregunta.desordenar();
						$("#li_pregunta_"+c).show();
						$.each(pregunta,function(ind,respuesta){
							var clase = (respuesta.correcta)? "correcta":"";
							if(c == "saldotarjeta")
								respuesta.respuesta = formatCurrency(respuesta.respuesta);
							var campoRespuesta = "<li id='li_respuesta_"+c+"_"+ind+"' ><input type='radio' name='respuesta"+ c +"' id='respuesta"+ c +"_"+ ind +"' class='"+ clase +"' /> <label for='respuesta"+ c +"_"+ ind +"'>"+ respuesta.respuesta +"</label></li>";
							$("#opciones_pregunta_"+c).append(campoRespuesta);
						});
					});
				});
			}else{
				alert("La tarjeta tiene estado BLOQUEADO, no puede continuar.");
				$("#lbononovedad").html(tarjeta.bono);
				$("#lfechasolnovedad").html(tarjeta.fechasolicitud);
				$("#lestadonovedad").html(tarjeta.estado);
				$("#lnombrenovedad").html(nom);
				if(tarjeta.saldo == null)
					tarjeta.saldo = '0';
				$("#lsaldonovedad").html(formatCurrency(tarjeta.saldo));
				$("#lubicacionnovedad").html(ubicacion);
				$("#txtBonoNovedad").val(tarjeta.bono);
				$("[name=tipoNovedad]").attr("checked",false);
				$("[name=novedad]").val("").attr("checked",false).attr("disabled",true);
				$("#tipoINovedad").focus();
				return false;
			}
			
			
		}
	});
}


function limpiarCamposTarjetaNovedad(){
	$("table.tablero input:text").val('');
	$("table.tablero select").val(0);
	$("#lnombrenovedad,#lbononovedad,#lfechasolnovedad,#lsaldonovedad").html(" ");
	$("#lestadonovedad").html("");
	$("#tNumeroNovedad").css("display", "none"); 
	$("#tBonoNovedad").css("display", "none"); 
	$("#numero5").val("");
	$("#buscarPorNovedad").focus();
}


function limpiarCamposNovedad(){
	$("table.tablero input:text").val('');
	$("#lnombrenovedad,#lbononovedad,#lfechasolnovedad,#lsaldonovedad").html(" ");
	$("#lestadonovedad").html("");
	$("#tNumeroNovedad").css("display", "none"); 
	$("#tBonoNovedad").css("display", "none");
	$("[name=tipoNovedad]").attr("checked",false);
	$("[name=novedad]").val("").attr("checked",false).attr("disabled",true);
	$("#numero5,#notas20").val("");
	$("#txtBonoNovedad").val("");
	$("#select").focus();
}

function guardarNovedadTarjeta(){
	var opt=$("#buscarPorNovedad").val();
	var notas=$("#notas20").val();
	if(opt==0){
		alert("No hay informacion para guardar!");
		return false;
	}
	if(notas.length==0){
		alert("Escriba las notas!");
		return false;
	}
	if(notas.length<10){
		alert("Las notas son muy cortas (min. 10 caracteres).");
		return false;
	}
	
	var campoTipoNovedad = $("[name=tipoNovedad]:checked");
	var idCodigoBloqueo = $("#"+ "novedad_" +campoTipoNovedad.attr("id").replace("tipoNovedad_","")).val();
	// estado B � estado S � H, todos son bloqueos, pero con distinto c�digo y nombre de estado
	$.ajax({
		url: URL+'asopagos/novedades/bloquear.php',
		type: "POST",
		async: false,
		data: {v0:idbono,v1:idCodigoBloqueo},
		success: function(datos){
			if(datos==0){
				alert("Lo lamento, no se pudo generar el bloqueo de la tarjeta!");
				return false;
			}
			if(datos==2)
				alert("Lo lamento, no se pudo grabar el registro de bloqueo. Es posible que haya un bloqueo pendiente de procesar.\n Por favor comunicar al Administrador del sistema.");

			if(datos==1){
				if(notas.length>0){
					$.getJSON(URL+'asopagos/guardarNota.php',{v0:idbono,v1:notas},function(data){
						if(data==0)
							alert("No se pudo guardar las notas, por favor comunicar al Administrador del Sistema.");
						
						if(data==1){
							alert("La tarjeta (id. "+ idbono +") fue BLOQUEADA, no olvide generar el plano.");
							$("#buscarPorNovedad").val("").trigger("change");
							limpiarCamposNovedad();
						}else{
							alert("No se pudo guardar las notas, por favor comunicar al Administrador del Sistema.");
						}
					});
				}
			}
		}
	});
	
}

function mostrarAyudaNovedadTarjeta(){
	$("#ayudaNovedadTarjeta").dialog('open');
}


function validarPreguntasSeguridad(){
	var opcionesRespuesta = $(".correcta");
	var seleccionesOk = $("[class=correcta]:checked");
	if((opcionesRespuesta.length - seleccionesOk.length) >= seleccionesOk.length){
		alert("Hay respuestas incorrectas. La verificaci\xf3n de seguridad NO FUE APROBADA.");
		identidadVerificada = false;
	}else{
		alert(seleccionesOk.length +" de "+ opcionesRespuesta.length +" respuestas correctas. La verificaci\xf3n de seguridad FUE APROBADA.");
		identidadVerificada = true;
	}
}