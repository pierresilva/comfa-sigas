/*
* @autor:      Ing. Orlando Puentes
* @fecha:      27/07/2010
* objetivo:
*/
// JavaScript Document

$(function() {
		$("#ayuda").dialog({
		 	autoOpen: false,
			height: 450, 
			width: 600, 
			draggable:true,
			modal:false,
			open: function(evt, ui){
					$('#ayuda').html('');
					$.get('../../help/aportes/ayudaConsultaRadicacion.html',function(data){
							$('#ayuda').html(data);
					})
			 }
		});
	});
	
$(function() {
	$("#dialog-form2").dialog("destroy");
	$("#dialog-form2").dialog({
		autoOpen: false,
		height: 400,
		width: 500,
		modal: true,
		buttons: {
			'Enviar': function() {
			var bValid = true;
			var campo=$('#notas').val();
			var campo0=$.trim(campo);
			if (campo0==""){
				$(this).dialog('close');
					return false;
				}
			var campo1=$('#usuario').val();
			var campo2="radicacion.php";
			$.post('../../phpComunes/colaboracion.php',{v0:campo0,v1:campo1,v2:campo2}, 								
			function(datos){
				if(datos=='1'){
					alert("Su comentario fue enviado correctamente!. Gracias por participar en el mejoramiento de SIGAS");
				}else{
					alert(datos);
				}
		});
		$(this).dialog('close');
		},
		Cancelar: function() {
			$(this).dialog('close');
			$("#dialog-form2").dialog("destroy");
		}
	}		
	});
	/*
	$('#enviar-notas')
		.button()
		.click(function() {
		$('#dialog-form2').dialog('open');
	});
	*/
});	


	
function consultaDatos(){
	var idr=$('#txtId').val();
	if(idr.length==0){
		return false;
		}
	$.getJSON('buscarRadicacionId.php',{v0:idr},function(datos){
		if(datos==0){
			alert("Lo siento, la radicación no existe!");
			return false;
			}
		else
		{
			$('#txtId2').val(datos.fecharadicacion);
			$('#iden').val(datos.identificacion);
			$('#select').val(datos.idtipodocumento);
			$('#textfield').val(datos.pnombre);
			$('#textfield3').val(datos.snombre);
			$('#textfield2').val(datos.papellido);
			$('#textfield4').val(datos.sapellido);
			$('#select2').val(datos.idtiporadicacion);
			$('#select3').val(datos.idtipopresentacion);
			$('#textfield7').val(datos.horainicio);
			$('#textfield8').val(datos.horafinal);
			$('#textfield11').val(datos.usuario);
			$('#tiempo').val(datos.tiempo);
			$('#textfield12').val(datos.asignado);
			$('#textfield13').val(datos.usuarioasignado);
			$('#textfield14').val(datos.fechaasignacion);
			$('#textfield15').val(datos.procesado);
			$('#textfield16').val(datos.fechaproceso);
			$('#textfield17').val(datos.gigitalizada);
			$('#textfield18').val(datos.fechadigitalizacion);
			$('#textfield19').val(datos.notas);
			mostrar_div(datos.idtiporadicacion,datos);
			}
		})
	}
	
function mostrar_div(idr,datos){
	var idr=parseInt(idr);
	$('#div1,#div2,#div3,#div4,#div5,#div6,#div7,#div8,#div9,#div10,#div11,#div12,#div13').css({display:"none"});
	switch(idr){
		case 27 : 
			$('#lTipoInf').val(datos.idtipoinformacion);
			$('#div1').css({display:"block"});
			break;
		case 28 : 
			$('#sltTipoIdentificacion2').html($('#select option').clone());
			$('#tNumero2').val(datos.numero);
			$('#tNit2').val(datos.nit);
			$('#tFolios2').val(datos.folios);
			$('#sltTipoIdentificacion2').val(datos.idtipodocumentoafiliado);
			$('#tRazon2').val(datos.razonsocial);
			$('#div2').css({display:"block"});
			break;
		case 29 : 
			$('#sltTipoIdentificacion3').html($('#select option').clone());
			$('#tNumero3').val(datos.numero);
		    $('3tNit3').val(datos.nit);
		    $('#tFolios3').val(datos.folios);
			$('#sltTipoIdentificacion3').val(datos.idtipodocumentoafiliado);
			$('#div3').css({display:"block"});
			break;
		case 30 : 
			$('#tNit4').val(datos.nit);
			$('#tFolios4').val(datos.folios)
			$('#div4').css({display:"block"});
			break;
		case 31 :
		 	$('#tNit5').val(datos.nit);
			$('#tFolios5').val(datos.folios);
			$('#div5').css({display:"block"});
			break;
		case 32 : 
			$('#sltTipoIdentificacion6').html($('#select option').clone());
			$('#tNumero6').val(datos.numero);
			$('#tFolios6').val(datos.folios);
			$('#lTipoCer').val(datos.idtipocertificado);
			$('#tBene6').val(datos.idbeneficiario);
			$('#div6').css({display:"block"});
			break;
		case 33 : 
			$('#tNumero7').val(datos.numero);
			$('#div7').css({display:"block"});
			break;
		case 69 : 
			$('#sltTipoIdentificacion8').html($('#select option').clone());
			$('#txtIdentificacion').val(datos.numero);
			$('#tFolios8').val(datos.folios);
			$('#sltTipoIdentificacion8').val(datos.idtipodocumentoafiliado);
			$('#div8').css({display:"block"});
			break;
		case 70 : 
			$('#sltTipoIdentificacion9').html($('#select option').clone());
			$('#tNumero9').val(datos.numero);
			$('#div9').css({display:"block"})
			break;
		case 105 :
			$('#tNit10').val(datos.nit);
			$('#tFolios10').val(datos.folios);
			$('#div10').css({display:"block"});
			break;
		case 169 :		//reclamo no giro trabajador
			$('#sltTipoIdentificacion11').html($('#select option').clone());
			$('tNit11'),val(datos.nit);
			$('#div11').css({display:"block"});
			break;
		case 170 :		//reclamo no giro empresa
			$('#tNit12').val(datos.nit);
			$('#div12').css({display:"block"});
			break;
		case 183 : 
			$('#sltTipoIdentificacion13').html($('#select option').clone());
			$('#tNumero13').val(datos.numero);
			$('#comboQuien').val();
			$('#comboFallecido').val();
			$('#sltTipoIdentificacion13').val(datos.idtipodocumentoafiliado);
			$('#div13').css({display:"block"});
			break;
		}
	
	}	