var URL=src();
var nuevo=0;

$(function() {
	$("#btn_recibir_trab").bind("click",function(){
		$("#dialog_recibir").dialog("open");
		$("#str_tipo_info_confirmar").text("Trabajador");
		$("#tipo_info_confirmar").val("t");
	});
	
	$("#btn_recibir_emp").bind("click",function(){
		$("#dialog_recibir").dialog("open");
		$("#str_tipo_info_confirmar").text("Empresa");
		$("#tipo_info_confirmar").val("e");
	});
	
	$("#btn_recibir_fonede").bind("click",function(){
		$("#dialog_recibir").dialog("open");
		$("#str_tipo_info_confirmar").text("FONEDE");
		$("#tipo_info_confirmar").val("f");
	});
	
	
	$("#dialog_recibir").dialog("destroy");
	$("#dialog_recibir").dialog({
		autoOpen: false,
		height: 240,
		width: 680,
		modal: true,
		open: function(event, ui){
			$("#usuario_recibe, #clave_recibe").val('');
		},
		buttons: {
			'Confirmar': function() {
				var usuario = $('#usuario_recibe').val();
				var clave = md5($('#clave_recibe').val());
				$.getJSON('verificarUsuario.php',{v0:usuario},function(datos){
					if(datos==0){
						alert("El usuario no existe!");
						$("#usuario_recibe").val('');
						return false;
					}
					var clavemd5 = datos;
					if(clave != clavemd5){
						alert ("La clave esta errada!");
						$("#clave_recibe").val('');
						return false;
					}
					
					// datos correctos, se puede recibir las radicaciones
					var radicaciones = [];
					var strTipo = "";
					switch($("#tipo_info_confirmar").val()){
						case "t": strTipo = "trab"; break;
						case "e": strTipo = "emp"; break;
						case "f": strTipo = "fonede"; break;
					}
					
					var camposRadicacion = $("input[name^=rad_recibir_"+ strTipo +"]");
					$.each(camposRadicacion,function(cont, campo){
						radicaciones[cont] = $(campo).attr("id").replace("rad_recibir_"+strTipo+"_","");
					});

					if(radicaciones.length > 0){
						$.getJSON('recibirRadicaciones.php',{radicaciones:radicaciones.join(), usuarioRecibe: usuario},function(respuesta){
							if(respuesta.codigo == 1){
								alert(respuesta.mensaje +"("+ respuesta.datos.join() +").");
								$(this).dialog('close');
								switch($("#tipo_info_confirmar").val()){
									case "t":
										$("#tbl_rad_trab tbody").empty();
										$("#tbl_rad_trab tbody").append("<tr><td colspan='9'>Seleccione Nuevo para cargar registros.</td></tr>");
										break;
									case "e":
										$("#tbl_rad_emp tbody").empty();
										$("#tbl_rad_emp tbody").append("<tr><td colspan='10'>Seleccione Nuevo para cargar registros.</td></tr>");
										break;
									case "f":
										$("#tbl_rad_fonede tbody").empty();
										$("#tbl_rad_fonede tbody").append("<tr><td colspan='10'>Seleccione Nuevo para cargar registros.</td></tr>");
										break;
								}

							}else{
								alert(respuesta.mensaje);
							}
						});
					}
				});
				$(this).dialog('close');
			},
			Cancelar: function() {
				$(this).dialog('close');
				$("#dialog_recibir").dialog("destroy");
			}
		}
		
	});
	
	$("#tbl_rad_trab tbody, #tbl_rad_emp tbody, #tbl_rad_fonede tbody").empty();
	$("#tbl_rad_trab tbody").append("<tr><td colspan='9'>Seleccione Nuevo para cargar registros.</td></tr>");
	$("#tbl_rad_emp tbody").append("<tr><td colspan='10'>Seleccione Nuevo para cargar registros.</td></tr>");
	$("#tbl_rad_fonede tbody").append("<tr><td colspan='10'>Seleccione Nuevo para cargar registros.</td></tr>");
	
});

function cargarRadicaciones(tipo){
	switch(tipo){
		case "trabajador":
			$("#tbl_rad_trab tbody").empty();
			$("#tbl_rad_trab tbody").append("<tr><td colspan='9'>No Hay Registros</td></tr>");
			break;
		case "empresa":
			$("#tbl_rad_emp tbody").empty();
			$("#tbl_rad_emp tbody").append("<tr><td colspan='10'>No Hay Registros</td></tr>");
			break;
		case "fonede":
			$("#tbl_rad_fonede tbody").empty();
			$("#tbl_rad_fonede tbody").append("<tr><td colspan='10'>No Hay Registros</td></tr>");
			break;
	}
	$.getJSON('cargarRadicacionesARecibir.php',{tipo:tipo},function(radicaciones){
		if(radicaciones.length >0){
			$.each(radicaciones,function(cont,radicacion){
				switch(tipo){
					case "trabajador":
						var strTr = "<tr><td>"+ (cont+1) +"</td><td><input type='checkbox' disabled='disabled' checked='checked' name='rad_recibir_trab_"+ radicacion.idradicacion +"' id='rad_recibir_trab_"+ radicacion.idradicacion +"'  /> "+ radicacion.idradicacion+ "</td>"+
									"<td>"+ radicacion.tiporad +"</td><td>"+ radicacion.numero +"</td><td>"+ radicacion.pnombre +" "+ radicacion.snombre +" "+ radicacion.papellido +" "+ radicacion.sapellido +"</td>" +
									"<td>"+ radicacion.folios +"</td>"+
									"<td>"+ radicacion.agencia +"</td>"+
									"<td>"+ radicacion.procesado +"</td>"+
									"<td>"+ radicacion.usuario +"</td></tr>";
						$("#tbl_rad_trab tbody").append(strTr);
						break;
					case "empresa":
						var strTr = "<tr><td>"+ (cont+1) +"</td><td><input type='checkbox' disabled='disabled' checked='checked' name='rad_recibir_emp_"+ radicacion.idradicacion +"' id='rad_recibir_emp_"+ radicacion.idradicacion+ "'  /> "+ radicacion.idradicacion +"</td>"+
										"<td>"+ radicacion.tiporad +"</td><td>"+ radicacion.numero +" - " +radicacion.pnombre_pr +" "+ radicacion.snombre_pr +" "+ radicacion.papellido_pr +" "+ radicacion.sapellido_pr +"</td>"+
										"<td>"+ radicacion.nit +"</td><td>"+ radicacion.razonsocial +"</td><td>"+ radicacion.folios +"</td><td>"+ radicacion.agencia +"</td>"+
										"<td>"+ radicacion.procesado +"</td>"+
										"<td>"+ radicacion.usuario +"</td></tr>";
						$("#tbl_rad_emp tbody").append(strTr);
						break;
					case "fonede":
						var strTr = "<tr><td>"+ (cont+1) +"</td><td><input type='checkbox' disabled='disabled' checked='checked' name='rad_recibir_fonede_"+ radicacion.idradicacion +"' id='rad_recibir_fonede_"+ radicacion.idradicacion +"'  /> "+ radicacion.idradicacion +"</td>"+
									"<td>"+ radicacion.numero +"</td><td>"+ radicacion.pnombre +" "+ radicacion.snombre +" "+ radicacion.papellido +" "+ radicacion.sapellido +"</td>"+
									"<td>"+ radicacion.folios +"</td>"+
									"<td>"+ radicacion.agencia +"</td>"+
									"<td>"+ radicacion.procesado +"</td>"+
									"<td>"+ radicacion.usuario +"</td></tr>";
						$("#tbl_rad_fonede tbody").append(strTr);
						break;
				}
			});
				
		}
		
	});
}

function nuevoR(){
	$("#tbl_rad_trab tbody, #tbl_rad_emp tbody, #tbl_rad_fonede tbody").empty();
	$("#tbl_rad_trab tbody").append("<tr><td colspan='9'>No Hay Registros</td></tr>");
	$("#tbl_rad_emp tbody").append("<tr><td colspan='10'>No Hay Registros</td></tr>");
	$("#tbl_rad_fonede tbody").append("<tr><td colspan='10'>No Hay Registros</td></tr>");
	cargarRadicaciones("trabajador");
	cargarRadicaciones("empresa");
	cargarRadicaciones("fonede");
}

function camposLimpiar(){
	$("#tbl_rad_trab tbody, #tbl_rad_emp tbody, #tbl_rad_fonede tbody").empty();
	$("#tbl_rad_trab tbody").append("<tr><td colspan='9'>Seleccione Nuevo para cargar registros.</td></tr>");
	$("#tbl_rad_emp tbody").append("<tr><td colspan='10'>Seleccione Nuevo para cargar registros.</td></tr>");
	$("#tbl_rad_fonede tbody").append("<tr><td colspan='10'>Seleccione Nuevo para cargar registros.</td></tr>");
}