/**
 * @author Oswaldo Gonz�lez
 * @date 18-04-2012
 * Script que gestiona el comportamiento del formulario de bloque de tarjetas
 */


function initBloqueoTarjeta(){
	var estadoTarjeta=0;
	idp=0;
	idbono=0;
	
	//Dialog ayuda
	$("#ayudaBloqueoTarjeta").dialog({
		autoOpen: false,
		height: 500,
		width: 750,
		draggable:true,
		modal:false,
		open: function(evt, ui){
			$('#ayuda').html('');
			$.get(URL +'help/tarjetas/manualayudaBloqueotarjeta.html',function(data){
				$('#ayuda').html(data);
			});
		}
	});
	
	$("#buscarPor").bind('change',function(){
 		$("#lnombre,#lbono,#lfechasol,#lsaldo").html(" ");
 		$("#numero,#tipoI").val('');
		$("#lestado").html("");
	});
	
	//Funcionamiento controles de documentaci�n de bloqueo de tarjetas
	$("input[name=motbloqueo]").click(function(){
		var valor = $(this).attr("id").replace("motb_","");
		if(valor == 'perdida'){
			$("#docsBloqueoTarjeta p:eq(0)").show();
			$("#docsBloqueoTarjeta p:eq(1)").hide().find("input:checkbox").attr("checked",false);
		}else{
			$("#docsBloqueoTarjeta p:eq(1)").show();
			$("#docsBloqueoTarjeta p:eq(0)").hide().find("input:checkbox").attr("checked",false);
		}
	});
	
	$("#docsBloqueoTarjeta input:checked").attr("checked",false);
	$("#docsBloqueoTarjeta p").hide();
	
	$("#codBloqueo").bind("change",function(){
		// DETERIORO O MALA CALIDAD
		if($(this).val() == 2865){
			$("#motb_deteriodo").trigger("click");
		}else if($(this).val() == 2866 || $(this).val() == 2867){
			// PERDIDA O ROBO
			$("#motb_perdida").trigger("click");
		}else{
			$("#docsBloqueoTarjeta input:checked").attr("checked",false);
			$("#docsBloqueoTarjeta p").hide();
		}
		
	});
	
}


/**
 * Mostrar campos de b�squeda de tarjetas seg�n el valor de la lista
 */
function mostrarTarjeta(){
	var opt=$("#buscarPor").val();
	if(opt==0){
		$("#tNumero").css("display", "none"); 
		$("#tBono").css("display", "none"); 
		$("#numero4").val("");
	}

	if(opt==1){
		$("#tNumero").css("display", "block"); 
		$("#tBono").css("display", "none"); 
		$("#tipoI").focus();
	}

	if(opt==2){
		$("#tBono").css("display", "block"); 
		$("#tNumero").css("display", "none"); 
		$("#txtBono").focus();
	}
}

function buscarPersonaTarjeta(){
	estadoTarjeta = null;
	$("#notas16").val("");
	$("#codBloqueo").val("").trigger("change");
	var tipoI=$("#tipoI").val();
	var numero=$("#numero4").val();
	
	$("#lbono").html('');
	$("#lfechasol").html('');
	$("#lestado").html('');
	$("#lnombre").html('');
	$("#lsaldo").html('');
	$("#txtBono").val('');
	
	if(numero.length==0){
		alert("Digite el n\u00FAmero de identificaci\u00F3n!");
		$("#numero4").focus();
		return false;
	}
	
	$.getJSON(URL+'phpComunes/buscarPersona2.php',{v0:tipoI,v1:numero},function(data){
		if(data==0){
			alert("Lo lamento, el n\u00FAmero no existe!");
			$("#numero4").val("");
			$("#numero4").focus();
			return false;
		}
		$.each(data,function(i,fila){
			var nom=fila.pnombre+" "+fila.snombre+" "+fila.papellido+" "+fila.sapellido;
			idp=fila.idpersona;
			$("#lnombre").html(nom);
			$.getJSON(URL+'phpComunes/pdo.buscar.tarjeta.act.php',{v0:idp},function(datos){
				if(datos==0){
					alert("Lo lamento, no hay bono asociado a ese n�mero!");
					return false;
				}
				$.each(datos,function(i,fila){
					idbono = fila.idtarjeta;
					estadoTarjeta = fila.estado;
					if(fila.estado == 'A'){
						$("#lbono").html(fila.bono);
						$("#lfechasol").html(fila.fechasolicitud);
						$("#lestado").html(fila.estado);
						if(fila.saldo == null){
							fila.saldo = '0';	
						}
						$("#lsaldo").html(formatCurrency(fila.saldo));
						//$("#tBono").show();
						$("#txtBono").val(fila.bono);
						$("#codBloqueo").focus();
					}else{
						alert("La tarjeta no esta activa (Estado "+ fila.estado +")");
						$("#lbono").html(fila.bono);
						$("#lfechasol").html(fila.fechasolicitud);
						$("#lestado").html(fila.estado);
						$("#lnombre").html(nom);
						if(fila.saldo == null){
							fila.saldo = '0';	
						}
						$("#lsaldo").html(formatCurrency(fila.saldo));
						$("#txtBono").val(fila.bono);
						$("#tipoI").focus();
						return false;
					}
					return;
				});
			});
		});
		
	});
}

function buscarPersona2Tarjeta(){
	var bono=$.trim($("#txtBono").val());
	var idp=0;
	if(bono.length==0){
		alert("Digite el n\u00FAmero de Bono!");
		$("#txtBono").focus();
		return false;
	}

	$.ajax({
		url: URL+'phpComunes/pdo.buscar.tarjeta.act2.php',
		type: "POST",
		async: false,
		data: {v0:bono},
		dataType: "json",
		success: function(datos){
			if(datos==0){
				alert("Lo lamento, el bono no existe!");
	             limpiarCamposBloqueo();
				return false;
			}
			idbono=datos[0].idtarjeta;
			idp=datos[0].idpersona;
			$("#lbono").html(datos[0].bono);
			$("#lfechasol").html(datos[0].fechasolicitud);
			$("#lsaldo").html(formatCurrency(datos[0].saldo));
			$("#lestado").html(datos[0].estado);
			estadoTarjeta=datos[0].estado;
			
			$.getJSON(URL+'phpComunes/pdo.buscar.persona.id.php',{v0:idp},function(data){
				if(data==0){
					alert("Lo lamento, el n\u00FAmero no existe!");
					$("#numero4").val("");
					$("#numero4").focus();
					return false;
				}
				$.each(data,function(i,fila){
					var nom=fila.pnombre+" "+fila.snombre+" "+fila.papellido+" "+fila.sapellido;
					$("#lnombre").html(nom);
					$("#tNumero").show();
					$("#tipoI").val(fila.idtipodocumento);
					$("#numero4").val(fila.identificacion);
					return;
				});
			});
		}
	});
}


function limpiarCamposTarjeta(){
	$("table.tablero input:text").val('');
	$("table.tablero select").val(0);
	$("#lnombre,#lbono,#lfechasol,#lsaldo").html(" ");
	$("#lestado").html("");
	$("#tNumero").css("display", "none"); 
	$("#tBono").css("display", "none"); 
	$("#numero4").val("");
	$("#buscarPor").focus();
}


function limpiarCamposBloqueo(){
	$("table.tablero input:text").val('');
	$("#lnombre,#lbono,#lfechasol,#lsaldo").html(" ");
	$("#lestado").html("");
	$("#tNumero").css("display", "none"); 
	$("#tBono").css("display", "none"); 
	$("#numero4").val("");
	$("#txtBono").focus();
	mostrarTarjeta();
}

function guardarBloqueoTarjeta(){
	if(estadoTarjeta == 'B'){
		alert("La tarjeta YA esta BLOQUEADA!");
		return false;
	}
	var opt=$("#buscarPor").val();
	var notas=$("#notas16").val();
	if(opt==0){
		alert("No hay informacion para guardar!");
		return false;
	}
	if(notas.length==0){
		alert("Escriba las notas!");
		return false;
	}
	if(notas.length<10){
		alert("Las notas son muy cortas (min. 10 caracteres).");
		return false;
	}
	var codigo=parseInt($("#codBloqueo").val());
	$.ajax({
		url: URL+'asopagos/novedades/bloquear.php',
		type: "POST",
		async: false,
		data: {v0:idbono,v1:codigo},
		success: function(datos){
			if(datos==0){
				alert("Lo lamento, no se pudo BLOQUEAR la tarjeta!");
				return false;
			}
			if(datos==2)
				alert("Lo lamento, no se pudo grabar el registro de bloqueo, por favor comunicar al Administrador del sistema.");
			
			if(datos==1){
				if(notas.length>0){
					$.getJSON(URL+'asopagos/guardarNota.php',{v0:idbono,v1:notas},function(data){
						if(data==0)
							alert("No se pudo guardar las notas, por favor comunicar al Administrador del Sistema.");
						
						if(data==1){
							alert("La tarjeta (id. "+ idbono +") fue BLOQUEADA, no olvide generar el plano.");
							$("#buscarPor").val("").trigger("change");
							limpiarCampos();
						}else{
							alert("No se pudo guardar las notas, por favor comunicar al Administrador del Sistema.");
						}
					});
				}
			}
		}
	});
}

function mostrarAyudaBloqueoTarjeta(){
	$("#ayudaBloqueoTarjeta").dialog('open' );
}