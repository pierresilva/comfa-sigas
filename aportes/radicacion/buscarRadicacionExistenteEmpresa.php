<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
include_once $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'config.php';
include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'p.definiciones.class.php';
include_once $raiz.DIRECTORY_SEPARATOR.'aportes'.DIRECTORY_SEPARATOR.'radicacion'.DIRECTORY_SEPARATOR.'clases'. DIRECTORY_SEPARATOR.'radicacion.class.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

$objDefinicion = new Definiciones();

$tr	=(empty($_REQUEST['v0'])) ? 'NULL' : $_REQUEST['v0'];//tipo radicacion
$nit=(empty($_REQUEST['v1'])) ? 'NULL' : $_REQUEST['v1'];//nit
$sql="select iddetalledef,concepto,count(*) as pendientes from aportes091 LEFT JOIN aportes004 on iddetalledef=idtiporadicacion WHERE iddetalledef in($tr) and procesado='N' and nit='$nit' group by iddetalledef,concepto";
$indices=explode(",",$tr);
$array=array();
$rs=$db->querySimple($sql);
foreach($indices as $i){
	$definicion = mssql_fetch_assoc($objDefinicion->mostrar_registro($i)); 
	$array[$i]= array("idtiporadicacion" => $definicion["iddetalledef"],
					  "pendientes" => 0,
					  "nomtiporadicacion" => $definicion["concepto"]);
	
}
$cont = 0;
while($row=$rs->fetch()){
	$array[$row["iddetalledef"]] = array("idtiporadicacion" => $row["iddetalledef"],
											 "pendientes" => $row["pendientes"],
											 "nomtiporadicacion" => $row["concepto"]);
}
print_r(json_encode($array));
die();
?>