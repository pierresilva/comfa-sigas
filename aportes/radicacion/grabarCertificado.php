<?php
/* autor:       orlando puentes
 * fecha:       22/07/2010
 * objetivo:    
 */
 date_default_timezone_set('America/Bogota');
 
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
include_once($_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'config.php');
include_once $raiz. DIRECTORY_SEPARATOR .'clases' . DIRECTORY_SEPARATOR . 'p.certificados.class.php';

$campo0=(empty($_REQUEST['v0'])) ? 'NULL' : $_REQUEST['v0'];	//idbeneficiario
$campo1=(empty($_REQUEST['v1'])) ? 'NULL' : $_REQUEST['v1'];	//idparentesco
$campo2=(empty($_REQUEST['v2'])) ? 'NULL' : $_REQUEST['v2'];	//idtipocertificado
$campo3=(empty($_REQUEST['v3'])) ? 'NULL' : $_REQUEST['v3'];	//periodo inicio
$campo4=(empty($_REQUEST['v4'])) ? 'NULL' : $_REQUEST['v4'];	//periodo final
$campo5=(empty($_REQUEST['v5'])) ? 'NULL' : $_REQUEST['v5'];	//forma presentacion
$campo7=(empty($_REQUEST['v6'])) ? 'NULL' : $_REQUEST['v6'];	//capacidad de trabajo
$campo6=$_SESSION['USUARIO'];

$objClase= new Certificados;
$consulta=$objClase->inactivar_certificados($campo0,$campo2);
//idbeneficiario, idparentesco, idtipocertificado, periodoinicio, periodofinal, fechapresentacion, formapresentacion, estado, usuario(9)	
if ( $objClase->insert_certificado(array($campo0,$campo1,$campo2,$campo3,$campo4,$campo5,$campo6)) == true){
	
	if ($campo7=='I'){
		$objClase->insert_certificado_discapacitado(array($campo0,$campo1,$campo2,$campo3,$campo4,$campo5,$campo6));
	}
	
	echo "El certificado se grabo!";
}else{
	echo 'El certificado NO se pudo guardar!';
} 
die();
?>