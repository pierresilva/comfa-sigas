<?php
/* autor:       orlando puentes
 * fecha:       22/07/2010
 * objetivo:    
 */
ini_set("display_errors",'1');
date_default_timezone_set('America/Bogota');
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once $root;

include_once ($raiz. DIRECTORY_SEPARATOR.'aportes'.DIRECTORY_SEPARATOR.'radicacion'.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'radicacion.class.php');
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

$fechaActual = new DateTime();
	
$campo0 = (empty($_REQUEST['v0'])) ? 'NULL' : $_REQUEST['v0'];		//id
$campo1 = (empty($_REQUEST['v1'])) ? 'NULL' : $_REQUEST['v1'];		//fecha radica
$campo2 = (empty($_REQUEST['v2'])) ? 'NULL' : "'".$_REQUEST['v2']."'";		//identificacion
$campo3 = (empty($_REQUEST['v3'])) ? 'NULL' : "'".$_REQUEST['v3']."'";		//idtdoc
$campo4 = (empty($_REQUEST['v4'])) ? 'NULL' : $_REQUEST['v4'];		//id tipo radica
$campo5 = (empty($_REQUEST['v5'])) ? 'NULL' : $_REQUEST['v5'];		//id tipo prese
$campo6 = (empty($_REQUEST['v6'])) ? 'NULL' : "'".$_REQUEST['v6']."'";	//hoa inicio	
$campo7 = (empty($_REQUEST['v7'])) ? 'NULL' : "'".$_REQUEST['v7']."'";	// hora final	
$campo8 = 0;				//
$campo9 = (empty($_REQUEST['v9'])) ? 'NULL' : "'".$_REQUEST['v9']."'";	//notas
$campo10 = (empty($_REQUEST['v10'])) ? 'NULL' : $_REQUEST['v10'];	//id tipo informacion	
$campo11 = (empty($_REQUEST['v11'])) ? 'NULL' : "'".$_REQUEST['v11']."'";	//numero
$campo12 = (empty($_REQUEST['v12'])) ? 'NULL' : "'".$_REQUEST['v12']."'";	//nit
$campo13 = (empty($_REQUEST['v13'])) ? 'NULL' : $_REQUEST['v13'];	//folios
$campo14 = (empty($_REQUEST['v14'])) ? 'NULL' : $_REQUEST['v14'];	//id tipo certificadp
$campo15 = (empty($_REQUEST['v15'])) ? 'NULL' : $_REQUEST['v15'];	//id beneficiario
$campo16 = (empty($_REQUEST['v16'])) ? 'NULL' : $_REQUEST['v16'];	//observaciones
$campo17 = (empty($_REQUEST['v17'])) ? 'NULL' : "'".$_REQUEST['v17']."'";	//usuario
$campo18 = (empty($_REQUEST['v18'])) ? 'NULL' : "'".$_REQUEST['v18']."'";	//id tipo doc afiliado
$campo19 = "'".$_SESSION['AGENCIA']."'";	//id agencia
$campo20 = (empty($_REQUEST['v20'])) ? 'NULL' : $_REQUEST['v20'];	//tipo formulario
$campo21 = (empty($_REQUEST['v21'])) ? 'NULL' : $_REQUEST['v21'];	//id tipo doc ben
$campo22 = (empty($_REQUEST['v22'])) ? 'NULL' : $_REQUEST['v22'];	//numero iden benefi
$campo23 = (empty($_REQUEST['v23'])) ? 'NULL' : "'".$_REQUEST['v23']."'";	//afiliacion mul

$hora = $fechaActual->format("H");
$minuto = $fechaActual->format("m");
if(intval($hora > 17) || (intval($hora == 17) && intval($minuto) >= 30)){
	// Si la hora actual sobrepasa las 17:30, la radicaci�n entra con fecha del d�a siguiente
	$fechaActual->modify('+1 day');
}
$campo1 = $fechaActual->format("Y-m-d");

$sql="insert into aportes004 (fecharadicacion, identificacion, idtipodocumento, idtiporadicacion, idtipopresentacion, horainicio, horafinal, notas, idtipoinformacion, numero, nit, folios, idtipocertificado, idbeneficiario, observaciones, usuario, idtipodocumentoafiliado, idagencia, idtipoformulario, idtipodocben, numerobeneficiario, afiliacionmultiple,fechasistema) values('$campo1', $campo2, $campo3, $campo4, $campo5, $campo6, $campo7, $campo9, $campo10, $campo11, $campo12, $campo13, $campo14, $campo15, $campo16, $campo17,$campo18,$campo19,$campo20,$campo21,$campo22,$campo23,cast(getdate() as date))";
$rs=$db->queryInsert($sql,'aportes004');
if(is_null($rs))
	echo 0;
else
	echo $rs;
?>