<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once ($raiz. DIRECTORY_SEPARATOR.'fonede'.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'Postulante.php');
include_once ($raiz. DIRECTORY_SEPARATOR.'fonede'.DIRECTORY_SEPARATOR.'proc'.DIRECTORY_SEPARATOR.'ParametrosFonede.class.php');

/*require_once 'fonede/clases/Postulante.php';
require_once 'fonede/proc/ParametrosFonede.class.php';*/
$seccional = $_SESSION['AGENCIA'];
$idradicacion = $_REQUEST['idradicacion'];

if(isset($_SESSION['CRRPOS'])){
	$pos = unserialize($_SESSION['CRRPOS']);

	if(is_object($pos) && get_class($pos) == 'Postulante'){
		$pos->postulacion->noRadicacionPostulacion = $idradicacion;
		$pos->postulacion->fechaRadicacion = date('Y-m-d h:i:s');
		$bandera = parametrosFonede::obtenerBanderaFonede('R'); // bandera R por defecto para la postulación
		if(is_array($bandera)){
			$pos->postulacion->estadoPostulacion = $bandera["iddetalledef"];
		}
		
		//$pos->postulacion->estadoPostulacion = 2683;
		$pos->postulacion->seccional = $seccional;
		
		if($pos->postulacion->idpostulacion == 0){
			if($pos->idpersona == 0){
				//no existe la persona, se crea la informacion basica para generar el idpersona
				$pos->grabarDatosPersonales();
			}
			if($pos->postulacion->idpostulacion == 0 && $pos->idpersona != 0){
				//No existe la postulacion, se crea la informacion basica para genera el idpostulacion
				$pos->postulacion->grabar($pos->idpersona);
			}
			if($pos->idpersona != 0 && $pos->postulacion->idpostulacion != 0){
				foreach($pos->postulacion->historiaLaboral as $hist){
					//Se recorre el array con que almacena el obj del historial laboral y se ejecuta el metodo de grabacion de cada uno
					$hist->grabar($pos->postulacion->idpostulacion);
				}
			}
			unset($_SESSION['CRRPOS']);
			echo json_encode($pos->postulacion->idpostulacion);
		}else{
			//La postulacion ya existe
			echo json_encode(0);
		}
	
	}else{
		//@todo Error, se borraron los datos del postulante de la sesion
	}
}
?>