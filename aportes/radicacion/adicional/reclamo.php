
<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
$usuario=$_SESSION['USUARIO'];
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';

$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

$sql = "SELECT usuario,  COUNT(idreclamo) AS cantidad FROM aportes055 WHERE estado = 'A' GROUP BY usuario";

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Requerimiento</title>
<link type="text/css" href="../../../css/Estilos.css" rel="stylesheet"/>
<link type="text/css" href="../../../css/marco.css" rel="stylesheet"/>
<link type="text/css" rel="stylesheet" href="../../../css/css.1.8/jquery-ui-1.8.17.custom.css" />
<script language="javascript" src="../../../js/1.6/jquery-1.6.2.min.js"></script>
<script language="javascript" src="../../../js/1.6/jquery-ui-1.8.16.custom.min.js"></script>
<script language="javascript" src="js/listarReclamos.js"></script>

</head>

<body>
<center>
<table width="80%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td class="arriba_iz" >&nbsp;</td>
<td class="arriba_ce" ><span class="letrablanca">::Reclamos de Usuario::</span></td>
<td class="arriba_de" >&nbsp;</td>
</tr>
<tr>
<td class="cuerpo_iz">&nbsp;</td>
<td class="cuerpo_ce">
</td>
<td class="cuerpo_de">&nbsp;</td>
</tr>
<tr>
<td class="cuerpo_iz" >&nbsp;</td>
<td class="cuerpo_ce" >
<table width="50%" border="0" class="tablero" id="idTableRs" cellspacing="0" align="center">
  <tr>
    <td width="18%"><b>Usuario</b></td>
    <td width="18%"><b>N&uacute;mero Reclamos</b></td>
    <td width="18%"><b>Estado</b></td>
  </tr>
    <?php
		$rs=$db->querySimple($sql);
		$con = 0;
		$acum = "";
		while ($row = $rs->fetch()){
			
			$acum .= "<tr><td><input type='hidden' name='txtIdReclamo$con' id='txtIdReclamo$con' value='".$row['usuario']."'/>".$row['usuario']."</td><td>".$row['cantidad']."</td><td>A</td></tr>";
			$con++;
		}
		if($acum == ''){
			$acum = "<tr><td colspan='3'><font color='red'>No hay Usuarios para procesar!</font></td></tr>";
		}
		echo $acum;
	?>
</table></td>
<td class="cuerpo_de" >&nbsp;</td>
</tr>
<tr>
<td class="cuerpo_iz" >&nbsp;</td>
<td class="cuerpo_ce" align="center" id="tdBotton"><img src="../../../imagenes/procesar2.png" onclick="procesar();"/></td>
<td class="cuerpo_de" >&nbsp;</td>
</tr>
<tr>
<td class="abajo_iz" >&nbsp;</td>
<td class="abajo_ce" >&nbsp;</td>
<td class="abajo_de" >&nbsp;</td>
</tr>
</table>

</center>
</body>
</html>
