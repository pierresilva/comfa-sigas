<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
include_once $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . "config.php";
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

$strRadicaciones = (empty($_REQUEST['radicaciones']))? "" : $_REQUEST['radicaciones'];	//ids de radicaciones a recibir
$usuarioRecibe = (empty($_REQUEST['usuarioRecibe'])) ? null : $_REQUEST['usuarioRecibe'];
$respuesta = array("codigo" => 0, "mensaje" => "", "datos" => null);
if($usuarioRecibe == null || $usuarioRecibe == ""){
	$mensaje = "No se encontraron datos del usuario que recibe ($usuarioRecibe).";
	$respuesta["mensaje"] = $mensaje;
	print_r(json_encode($respuesta));
	die();
}else{
	$sql = "Select usuario FROM aportes519 WHERE usuario = '$usuarioRecibe'";
	$rs = $db->querySimple($sql);
	$usuario = $rs->fetch();
	if(!$usuario){
		$mensaje = "No se encontraron datos del usuario que recibe ($usuarioRecibe).";
		$respuesta["mensaje"] = $mensaje;
		print_r(json_encode($respuesta));
		die();
	}
}

if($strRadicaciones != ""){
	$respuesta["datos"] = explode(",",$strRadicaciones);
	$sql = "UPDATE aportes004 SET recibegrabacion = '$usuarioRecibe', fecharecibegraba = cast(getdate() as date) WHERE idradicacion in ($strRadicaciones)";
	$rs = $db->queryActualiza($sql,'aportes004');
	if(is_null($rs)){
		$cadena = $db->error;
		$msg = "Error en la tabla aportes015: ";
		$msg .= msg_error($cadena);
		$respuesta["codigo"] = 3;
		$respuesta["mensaje"] = $msg;
		print_r(json_encode($respuesta));
		exit();
	}else{
		$respuesta["codigo"] = 1;
		$mensaje = "Las radicaciones fueron marcadas como recibidas.";
		$respuesta["mensaje"] = $mensaje;
		print_r(json_encode($respuesta));
	}
	
}else{
	$respuesta["codigo"] = 2;
	$mensaje = "No se recibieron radicaciones a marcar.";
	$respuesta["mensaje"] = $mensaje;
	print_r(json_encode($respuesta));
}
die();
?>