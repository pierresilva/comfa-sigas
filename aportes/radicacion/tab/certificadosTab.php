<?php
/* autor:       orlando puentes
 * fecha:       19/07/2010
 * objetivo:    
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$idp= $_REQUEST['idp'];
include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'p.certificados.class.php';
$objClase=new Certificados();
$consulta = $objClase->certificados_idp($idp);
$cont=0;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Certificados</title>
<script type="text/javascript" src="../../js/script.js"></script>
<script type="text/javascript">
  var sorter = new TINY.table.sorter("sorter");
	sorter.head = "head";
	sorter.asc = "asc";
	sorter.desc = "desc";
	sorter.even = "evenrow";
	sorter.odd = "oddrow";
	sorter.evensel = "evenselected";
	sorter.oddsel = "oddselected";
	sorter.paginate = true;
	sorter.pagesize = (5);
	sorter.currentid = "currentpage2";
	sorter.limitid = "pagelimit2";
	sorter.init("table2",1);
  </script>
</head>
<body>
<h4>Certificados</h4>
<table width="100%" border="0" cellspacing="0" class="sortable" id="table2">
<thead>
<tr>
<th><h3>Beneficiario</h3></th>
<th><h3>Tipo</h3></th>
<th><h3>Certificado</h3></th>
<th><h3>P Inicio</h3></th>
<th><h3>P Final</h3></th>
<th><h3>F Presentaci&oacute;n</h3></th>
<th><h3>Estado</h3></th>
</tr>
</thead>
<tbody>
<?php 
while($row=mssql_fetch_array($consulta)){
	$nombre=$row['pnombre']." ".$row['snombre']." ".$row['papellido']." ".$row['sapellido'];
?>
<tr>
    <td style="text-align:right"><?php echo $nombre; ?></td>
    <td style="text-align:center"><?php echo $row['detalledefinicion']; ?></td>
    <td style="text-align:right"><?php echo $row['certificado']; ?></td>
    <td style="text-align:right"><?php echo $row['periodoinicio']; ?></td>
    <td style="text-align:right"><?php echo $row['periodofinal']; ?></td>
    <td style="text-align:center"><?php echo $row['fechapresentacion']; ?></td>
    <td style="text-align:center"><?php echo $row['estado']; ?></td>
     </tr>
  <?php }?>
  </tbody>
</table>
<div id="controls">
		<div id="perpage">
			<select onChange="sorter.size(this.value)">
			<option value="5" selected="selected">5</option>
				<option value="10" >10</option>
				<option value="20">20</option>
				<option value="50">50</option>
				<option value="100">100</option>
			</select>
			<label>Registros Por P&aacute;gina</label>
		</div>
		<div id="navigation">
			<img src="../../imagenes/imagesSorter/first.gif" width="16" height="16" alt="first Page" onClick="sorter.move(-1,true)" />
			<img src="../../imagenes/imagesSorter/previous.gif" width="16" height="16" alt="first Page" onClick="sorter.move(-1)" />
			<img src="../../imagenes/imagesSorter/next.gif" width="16" height="16" alt="first Page" onClick="sorter.move(1)" />
			<img src="../../imagenes/imagesSorter/last.gif" width="16" height="16" alt="Last Page" onClick="sorter.move(1,true)" />
		</div>
		<div id="text">P&aacute;gina <label id="currentpage2"></label> de <label id="pagelimit2"></label></div>
	</div>
</body>
</html>