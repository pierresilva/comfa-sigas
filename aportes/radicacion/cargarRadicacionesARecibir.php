<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
$url=$_SERVER['PHP_SELF'];
include_once $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . "config.php";
include_once $raiz.DIRECTORY_SEPARATOR .'phpComunes'.DIRECTORY_SEPARATOR.'auditoria.php';
auditar($url);
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejaInforma.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
include_once $raiz. DIRECTORY_SEPARATOR .'clases'. DIRECTORY_SEPARATOR . 'p.definiciones.class.php';
date_default_timezone_set('UTC');
$db = IFXDbManejaInforma::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

$tipoRadicaciones = $_REQUEST["tipo"];
$fechaRadicacion = "cast(getdate() as date)";

switch($tipoRadicaciones){
	case "trabajador":
		// Radicaciones asociadas a trabajadores
		$sql = "SELECT ag.agencia,td.codigo as cod_tipodoc, td.detalledefinicion as tipodoc, t.pnombre,t.snombre,t.papellido,t.sapellido,tr.detalledefinicion as tiporad,r.* 
				FROM aportes004 r 
				inner join aportes015 t ON t.identificacion = r.numero 
				inner join aportes091 td on td.iddetalledef = t.idtipodocumento
				inner join aportes091 tr on tr.iddetalledef = r.idtiporadicacion
				inner join aportes500 ag ON ag.codigo = r.idagencia
				WHERE idtiporadicacion in (27,28,29,70,169,183,192,195,2919,2926) AND anulada = 'N' AND procesado='N' AND cierre='S' AND (recibegrabacion is null OR recibegrabacion = '' ) AND fecharadicacion = $fechaRadicacion";
		break;
	case "empresa":
		// Radicaciones asociadas a empresas
		$sql = "SELECT ag.agencia,pr.pnombre as pnombre_pr,pr.snombre as snombre_pr, pr.papellido as papellido_pr, pr.sapellido as sapellido_pr, e.razonsocial,tr.detalledefinicion as tiporad,r.* 
				FROM aportes004 r 
				inner join aportes015 pr ON pr.identificacion = r.numero 
				inner join aportes048 e ON e.nit = r.nit
				inner join aportes091 tr on tr.iddetalledef = r.idtiporadicacion
				inner join aportes500 ag ON ag.codigo = r.idagencia
				WHERE idtiporadicacion in (30,31,33,170) AND anulada = 'N' AND procesado='N' AND cierre='S' AND (recibegrabacion is null OR recibegrabacion = '' ) AND fecharadicacion = $fechaRadicacion";
		break;
	case "fonede":
		// Radicaciones asociadas a FONEDE
		$sql = "SELECT ag.agencia,td.codigo as cod_tipodoc, td.detalledefinicion as tipodoc, t.pnombre,t.snombre,t.papellido,t.sapellido,tr.detalledefinicion as tiporad,r.* 
				FROM aportes004 r 
				inner join aportes015 t ON t.identificacion = r.numero 
				inner join aportes091 td on td.iddetalledef = t.idtipodocumento
				inner join aportes091 tr on tr.iddetalledef = r.idtiporadicacion
				inner join aportes500 ag ON ag.codigo = r.idagencia
				WHERE idtiporadicacion in (69) AND anulada = 'N' AND procesado='N' AND cierre='S' AND (recibegrabacion is null OR recibegrabacion = '' ) AND fecharadicacion = $fechaRadicacion";
		break;
}

$resRadicaciones = $db->querySimple($sql);
$radicaciones = array();
$cont=0; 
while($radicacion = $resRadicaciones->fetch())
	$radicaciones[count($radicaciones)] = $radicacion;

print_r(json_encode($radicaciones));
die();
?>