<?php
/* autor:       Orlando Puentes
 * fecha:       Agosto 10 de 2010
 * objetivo:    Registrar en la base de datos la recepci?n de documentos con destino a Aportes y Subsidio para la afiliaci?n, modificaci?n o adici?n de informaci?n de los afiliados o empresas aportes de Comfamiliar Huila.
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$url=$_SERVER['PHP_SELF'];
include_once $raiz.DIRECTORY_SEPARATOR.'phpComunes'.DIRECTORY_SEPARATOR.'auditoria.php';
include_once $raiz.DIRECTORY_SEPARATOR.'funcionesComunes'.DIRECTORY_SEPARATOR.'funcionesComunes.php';
include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'p.definiciones.class.php';
include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'ciudades.class.php';
$objClase=new Definiciones();
$objCiudad=new Ciudades();

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="es"><head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Radicaci&oacute;n</title>
<link type="text/css" href="../../css/css.1.8/jquery-ui-1.8.17.custom.css" rel="stylesheet">
<link type="text/css" href="../../css/marco.css" rel="stylesheet">
<link type="text/css" href="../../css/Estilos.css" rel="stylesheet">

<script type="text/javascript" src="../../js/1.6/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="../../js/1.6/jquery-ui-1.8.16.custom.min.js"></script>

<script type="text/javascript" src="../../js/comunes.js"></script>
<script type="text/javascript" src="../../js/jquery.combos.js"></script>
<script type="text/javascript" src="../../js/direccion.js"></script>
<script type="text/javascript" src="../../js/jquery.tablesorter.js"></script>
<script type="text/javascript" src="../../js/jquery.tablesorter.pager.js"></script>	
<script type="text/javascript" src="../../js/llenarCampos.js"></script>
<script type="text/javascript" src="js/control.js"></script>
<script type="text/javascript" src="js/radicacion.js"></script>
<script type="text/javascript" src="js/fonede.js"></script>
<script type="text/javascript" src="js/new.radicacion.js"></script>
<script type="text/javascript" src="js/bloqueoTarjeta.js"></script>
<script type="text/javascript" src="js/novedadTarjeta.js"></script>

<script type="text/javascript">
	shortcut.add("Shift+F",function() {
	    var URL=src();
		var url=URL+"aportes/trabajadores/consultaTrabajador.php";
		window.open(url,"_blank");
	},{
		'propagate' : true,
		'target' : document 
	});
</script>
<style type="text/css">
<!--
.Estilo5 {	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
}
#tabs #tabs-1 table tr td #div13 .tablero tr td strong {
	font-size: 14px;
}
-->

.error{ background-color:#FFC6C6}
   
	#pie li {
	font-size: 10px;
	color: #999;
	padding:0;
	
}
h4{
	margin: 0;
	padding: 0;
}    
    </style>
</head>
<body>
<form name="forma">

<!-- <div class="demo">  -->
<div id="tabs">
<ul>
	<li><a href="#tabs-1">Radicaci&oacute;n</a></li>	
 </ul>
     <!-- Tabs Radicacion -->
<div id="tabs-1">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="13" height="29" class="arriba_iz">&nbsp;</td>
    <td class="arriba_ce"><span class="letrablanca">::&nbsp;Administracion - Radicaci&oacute;n &nbsp;::</span></td>
    <td width="13" class="arriba_de" align="right">&nbsp;</td>
  </tr>
  <tr>
	<td class="cuerpo_iz">&nbsp;</td>
	<td class="cuerpo_ce">
	<img src="../../imagenes/tabla/spacer.gif" width="1" height="1">
	<img src="../../imagenes/spacer.gif" width="1" height="1">
	<img src="../../imagenes/menu/nuevo.png" title="Nuevo" width="16" height="16" style="cursor:pointer" onclick="nuevoR();">
	<img src="../../imagenes/spacer.gif" width="12" height="1">
	<img src="../../imagenes/menu/grabar.png" width="16" height="16" style="cursor:pointer" title="Guardar" onclick="validarCampos(1);" id="bGuardar"> 
	<img src="../../imagenes/spacer.gif" width="12" height="1">
	<img src="../../imagenes/menu/refrescar.png" width="16" height="16" style="cursor:pointer" title="Limpiar campos" onclick="limpiarCampos(); document.forms[0].elements[0].focus();nuevoR();">
	<img src="../../imagenes/spacer.gif" width="8" height="1">
	<img src="../../imagenes/menu/informacion.png" width="16" height="16" style="border:none; cursor:pointer" title="Manual" onclick="mostrarAyuda();">
	<!--<img src="../../imagenes/menu/notas.png" width="16" height="16" style="cursor: pointer" title="Colaboraci?n en l?nea" onClick="notas();">-->
	<!--<img src="../../imagenes/menu/revisar.png" title="Revisar" name="revisar" width="16" height="16" id="revisar" style="border:none; cursor:pointer">-->
	 <img src="../../imagenes/spacer.gif" width="8" height="1">
	 <img src="../../imagenes/menu/devolver.png" title="Devoluci&oacute;n de documentos" name="devolucion" width="16" height="16" id="devolucion" style="border:none; cursor:pointer" onClick="devolucionDocumentos()" />
     <br>
     <font size=1 face="arial">Nuevo&nbsp;Guardar&nbsp;Limpiar&nbsp;Info&nbsp;Devolver</font>
     <!--<img src="../../imagenes/menu/copy.png" title="Copiar" name="copiar" id="copiar" style="border:none; cursor:pointer" onclick="copiarCampos();" width="16" height="16" />-->
     </td>
    <td class="cuerpo_de">&nbsp;</td>
  </tr>
  <tr>
    <td class="cuerpo_iz">&nbsp;</td>
    <td align="left" class="cuerpo_ce">
    <div id="error" style="color:#FF0000"></div>
    </td>
    <td class="cuerpo_de">&nbsp;</td>
  </tr>
  <tr>
    <td class="cuerpo_iz">&nbsp;</td>
    <td align="center" class="cuerpo_ce">
	<table width="100%" border="0" cellspacing="0" class="tablero">
      <tr>
        <td width="17%">Radicaci&oacute;n Nro</td>
        <td width="29%"><input name="txtId" class="boxfecha" id="txtId" readonly /></td>
        <td width="17%">Fecha</td>
        <td width="37%"><input name="txtId2" class="box1" id="txtId2" readonly /></td>
        </tr>
       <tr>
      <td>Tipo Documento</td>
        <td><label>
          <select name="select" id="select" class="box1" onchange="iniciarconN(this,0);document.getElementById('textfield22').value=''">
            <option value="0" selected="selected">Seleccione...</option>
            <?php
			
			$consulta = $objClase->mostrar_datos( 1,1 );
            while( $row = mssql_fetch_array( $consulta ) ){
				echo "<option value=". $row['iddetalledef'].">".$row['detalledefinicion']."</option>";
			}
        ?>
          </select>
          <img src="../../imagenes/menu/obligado.png" width="12" height="12"></label></td>
        <td>N&uacute;mero</td>
        <td><input name="textfield22" type="text" class="box1" id="textfield22" onfocus="iniciarconN(this,'');" onBlur="validarLongNumIdent(document.getElementById('select').value,this);buscarPersona(this);" onKeyPress="tabular(this,event);" onkeyup="validarCaracteresPermitidos(document.getElementById('select').value,this);"><img src="../../imagenes/menu/obligado.png" width="12" height="12"></td>
        </tr>
       <tr>
      <td>Primer Nombre</td>
        <td><input name="textfield" type="text" class="box1" id="textfield" onkeyup='sololetras(this);' onkeypress='return validarEspacio(event);' >
          <img src="../../imagenes/menu/obligado.png" width="12" height="12"></td>
        <td>Segundo Nombre</td>
        <td><input name="textfield3" type="text" class="box1" id="textfield3" onkeyup='sololetras(this);'></td>
        </tr>
	   <tr>
	    <td>Primer Apellido</td>
	    <td><input name="textfield2" type="text" class="box1" id="textfield2" onKeyPress="tabular (this, event); return validarEspacio(event);" onkeyup='sololetras(this);' >
	      <img src="../../imagenes/menu/obligado.png" width="12" height="12"></td>
	    <td>Segundo Apellido</td>
	    <td><input name="textfield4" type="text" class="box1" id="textfield4" onkeyup='sololetras(this);'></td>
	    </tr>
	  <tr>
	    <td>Tipo Radicaci&oacute;n </td>
	    <td colspan="3"><select name="select2" id="select2" class="boxmediano" onChange="vertab(this);">
	      <option value="0" selected="selected">Seleccione</option>
	      <?php
		
			$consulta = $objClase->mostrar_datos( 6,3 );
            while( $row = mssql_fetch_array( $consulta ) ){
			echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
			}
          ?>
	      </select>	      <img src="../../imagenes/menu/obligado.png" width="12" height="12"></td>
	    </tr>
	    <tr>
	    <td>Presentaci&oacute;n</td>
	    <td>
	    <select name="select3" id="select3" class="box1">
	      <option value="0" selected="selected">Seleccione</option>
	      <?php
		
			$consulta = $objClase->mostrar_datos( 13,3 );
            while( $row = mssql_fetch_array( $consulta ) ){
			echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
			}
        ?>
	      </select>
	      <img src="../../imagenes/menu/obligado.png" width="12" height="12">
	      </td>
	    <td>Usuario</td>
	    <td><input name="textfield11" type="text" class="box1" id="textfield11" readonly value=""></td>
	    </tr>
       <tr>
	    <td>Hora Inicio</td>
	    <td><input name="textfield7" type="text" class="box1" id="textfield7" readonly>&nbsp;<span id="horaDia" style="color:#FF0000"></span></td>
	    <td>Hora Final</td>
	    <td><input name="textfield8" type="text" class="box1" id="textfield8" readonly></td>
	    </tr>
	  <tr>
	    <td colspan="4" > 
	    	<input type="hidden" name="tExiste" id="tExiste">
  			<input type="hidden" value="<?php echo $_SESSION["USUARIO"];?>" name="usuario" id="usuario">
	    &nbsp;</td>
	    </tr>
    </table>
    </td>
    <td class="cuerpo_de">&nbsp;</td>
    </tr>
  <tr>
<td class="abajo_iz" >&nbsp;</td>
<td class="abajo_ce" ></td>
<td class="abajo_de" >&nbsp;</td>
</tr>
</table>

<!-- tipo de informacion index 1 style="display:none" --> 

<div id="div1" >
<table width="100%" border="0" cellspacing="0" class="tablero">
<tr>
  <th colspan="2" align="left"><center>Informaci&oacute;n Adicional Informaci&oacute;n</center></th>
  </tr>
<tr><td align="left" width="17%">Tipo de informaci&oacute;n</td>
<td>
<select name="lTipoInf" id="lTipoInf" class="boxlargo">
<option value="0" selected="selected">Seleccione...</option>
<?php

$consulta = $objClase->mostrar_datos(8,3);
 while( $row = mssql_fetch_array( $consulta ) ){
	echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
}
?>
</select><img src="../../imagenes/menu/obligado.png" width="12" height="12">
</td>
</tr>
<tr><td>Notas</td><td><textarea name="notas1" id="notas1" class="boxlargo"></textarea></td></tr>
</table>
</div>
<!-- fin div 1 -->

<!-- DEPENDIENTES -->
<div id="div2" >
<table width="100%" border="0" cellspacing="0" class="tablero">
<tr>
<th colspan="4" align="left"><center>
    Informaci&oacute;n Adicional Afiliaci&oacute;n Trabajador
</center></th>
</tr>
<tr>
<td align="left" width="17%">Tipo Documento Afiliado</td>
<td>
<select class="box1" name="sltTipoIdentificacion2" id="sltTipoIdentificacion2" onkeypress="tabular (this, event);" onchange="restablecerValoresCampos();document.getElementById('tNumero2').value=''"></select>
<img src="../../imagenes/menu/obligado.png" width="12" height="12">
</td>
<td>N&uacute;mero</td>
<td><input type="text" class="box1" name="tNumero2" id="tNumero2" onBlur="validarLongNumIdent(document.getElementById('sltTipoIdentificacion2').value,this);buscarPersona2(this,9,$('#sltTipoIdentificacion2').val());" onKeyPress="tabular (this, event);" onkeyup="validarCaracteresPermitidos(document.getElementById('sltTipoIdentificacion2').value,this);">
  <img src="../../imagenes/menu/obligado.png" width="12" height="12"></td>
</tr>
<tr>
  <td>Primer Apellido</td>
  <td><input name="papellido" type="text" class="box1" id="papellido" onkeypress="return validarEspacio(event)" onkeydown='sololetras(this);' onkeyup='sololetras(this);'>
    <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12"></td>
  <td>Segundo Apellido</td>
  <td><input name="sapellido" type="text" class="box1" id="sapellido" onkeydown='sololetras(this);' onkeyup='sololetras(this);'></td>
</tr>
<tr>
  <td>Primer Nombre</td>
  <td><input name="pnombre" type="text" class="box1" id="pnombre" onkeypress="return validarEspacio(event)" onkeydown='sololetras(this);' onkeyup='sololetras(this);'>
    <img src="../../imagenes/menu/obligado.png" alt="Obligatorio" width="12" height="12"></td>
  <td>Segundo Nombre</td>
  <td><input name="snombre" type="text" class="box1" id="snombre" onkeydown='sololetras(this);' onkeyup='sololetras(this);'></td>
</tr>
<tr>
  <td>Nombre Corto</td>
  <td colspan="3"><input name="ncorto" type="text" class="boxlargo" id="ncorto" readonly><span></span></td>
  </tr>
<tr>
  <td>NIT Empleador</td>
  <td colspan="3"><input type="text" class="box1" name="tNit2" id="tNit2" onkeypress="return validarEspacio(event);" onkeyup="solonumeros(this);" onkeydown="tabular (this, event); solonumeros(this);" onblur="validarLongNumIdent('5',this);buscarAfiliacionAxNit(this);" >    <img src="../../imagenes/menu/obligado.png" alt="Obligatorio" width="12" height="12">
    <input type="text" class="boxlargo" name="tRazon2" id="tRazon2" readonly></td>
  </tr>
<tr>
<td>Fecha Ingreso</td>
<td><input name="fecIngresoNuevaAfi" type="text" class="boxfecha" id="fecIngresoNuevaAfi" readonly></td>
<td>Salario</td>
<td><input name="salarioNuevaAfi" type="text" class="boxfecha" id="salarioNuevaAfi" onBlur="calcularCategoria(this.value,document.getElementById('categoriaNuevaAfi').id)" onkeydown="solonumeros(this);" onkeyup="solonumeros(this);">
  <img src="../../imagenes/menu/obligado.png" alt="Obligatorio" width="12" height="12">
  <input id="categoriaNuevaAfi" type="hidden"></td>
</tr>
<tr>
<td align="left" width="17%">Tipo de afiliaci&oacute;n</td>
<td><select name="tAfiliacion" id="tAfiliacion" class="box1">
  <!--<option value="0" selected="selected">Seleccione</option>-->
  <?php
    $consulta = $objClase->mostrar_datos(2,3);
  while( $row = mssql_fetch_array( $consulta ) ){
  	if(($row['iddetalledef'])=="18")
  	{
		echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
  	}
	}
	?>
</select>  <img src="../../imagenes/menu/obligado.png" width="12" height="12">&nbsp; </td>
<td>Tipo Formulario</td>
<td><select name="tipForm" class="box1" id="tipForm" >
  <option selected="selected">Seleccione..</option>
  <?php
	$consulta = $objClase->mostrar_datos(9,3);
    while( $row = mssql_fetch_array( $consulta ) ){
		echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
	}
	?>
</select>
  <img src="../../imagenes/menu/obligado.png" alt="Obligatorio" width="12" height="12"></td>
</tr>
<tr>
<td align="left" width="17%">N&uacute;mero de folios</td>
<td><input type="text" class="box1" name="tFolios2" id="tFolios2" maxlength="2"><img src="../../imagenes/menu/obligado.png" width="12" height="12"></td>
<td>Tarjeta</td>
<td><input name="tarjetaNuevaAfi" type="text" disabled="disabled" class="boxfecha" id="tarjetaNuevaAfi"></td>
</tr>
<tr>
  <td align="left">Notas</td>
  <td colspan="3"><textarea name="notas2" id="notas2" class="boxlargo"></textarea></td>
</tr>
</table>
<br />
<hr />

<div id="docs" align="left">
	<!-- DOCUMENTOS AFILIADO -->
<div>
      <h4>LISTADO DOCUMENTOS A SOLICITAR</h4>
      <p>
      <input type="checkbox" name="docTrabajador" id="formulario">
      Formulario afiliaci&oacute;n.
      <img src="../../imagenes/menu/obligado.png" width="12" height="12">
      </p>
      <p>
      <input type="checkbox" name="docTrabajador"  id="docCedula">
      Fotocopia de la c&eacute;dula
      <img src="../../imagenes/menu/obligado.png" width="12" height="12">
      </p>
      <span style="background:url(../../imagenes/FondoTabla1.png) repeat-x; border:1px dotted #666; display:block;width:300px"  class="ui-corner-all">
      <table border="0" width="100%">
      <tr><td>&nbsp;</td><td align="center"><b>SI</b></td><td align="center"><a><strong>NO</strong></a></td></tr>
      <tr>
        <td><strong>&iquest;Convivencia?</strong></td>
        <td align="center"><input type="radio" name="rconviven" id="si2" value="s"></td>
        <td align="center"><input name="rconviven" type="radio" id="no2" value="n" checked="checked"></td>
      </tr>
      <tr>
        <td><strong>&iquest;Hijos-Hermanos?</strong></td>
        <td align="center"><input type="radio" name="rhijos" id="si3" value="s"></td>
        <td align="center"><input name="rhijos" type="radio" id="no3" value="n" checked="checked"></td>
      </tr>
      <tr>
        <td><strong>&iquest;Padres?</strong>&nbsp;
          <select id="tipoServicio" style="display:none">
            <option value="0">Seleccione..</option>
            <option  value="1">SERVICIOS</option>
            <option  value="2">SUBSIDIO</option>
            </select></td>
        <td align="center"><input type="radio" name="rpadres" id="si" value="s"></td>
        <td align="center"><input name="rpadres" type="radio" id="no" value="n" checked="checked"></td>
        
      </tr>
      </table>
      </span>
	</div>
	<!-- DOCUMENTOS CONYUGE -->
	<div>
	<hr>
 <h4>Documentos Conyuge</h4>
   <p>
   <input type="checkbox" name="docConyuge" id="check1">
   Registro civil MATRIMONIO<sup>1</sup>.</p>
   <p>
   <input type="checkbox" name="docConyuge" id="check2">
   Fotocopia AMPLIADA y LEGIBLE de la c&eacute;dula del c&oacute;nyuge.
   </p>
  	<p>
	<input type="checkbox" name="docConyugeInd" id="check3_docs">
	Formato de Convicencia.
	</p>
	</div>
	<!-- DOCUMENTOS HIJOS -->
	<div>
	<hr>
<h4>Documentos  Hijos-Hijastros-Hermanos</h4>
<p>
  <input type="checkbox" name="docHijos" id="check3">
  Fotocopia Registro civil.
  <img src="../../imagenes/menu/obligado.png" width="12" height="12">
</p>
<p>
  <input type="checkbox" name="docHijos" id="check4">
  Certificado de escolaridad<sup>2</sup>.
</p>
<p>
  <input type="checkbox" name="docHijos" id="check5">
  Sentencia judicial de custodia.
</p>
<p>
  <input type="checkbox" name="docHijos" id="check6">
  Sentencia de adopci&oacute;n.
</p>
<p>
  <input type="checkbox" name="docHijos" id="check7">
  Certificado de discapacidad.
</p>

	</div>
<!-- DOCUMENTOS PADRES -->
<div>
<hr>
<h4>Documentos  Padres</h4>
<p>
  <input type="checkbox" name="docPadres" id="check8">
  Fotocopia de la c&eacute;dula AMPLIADA.
  <img src="../../imagenes/menu/obligado.png" width="12" height="12">
</p>
<p>
  <input type="checkbox" name="docPadres" id="check9">
  Registro civil trabajador.
  <img src="../../imagenes/menu/obligado.png" width="12" height="12">
</p>
<p>
  <input type="checkbox" name="docPadres" id="check10">
  Certificado de supervivencia<sup>3</sup>.
</p>
<p>
  <input type="checkbox" name="docPadres" id="check11">
  Declaraci&oacute;n extrajuicio.
</p>
	</div>
</div>
<!-- FIN DIV DOCS -->
<br />
<ul id="pie" type="1" style="border-top:1px dashed #999; text-align:left">
<li>(1)Escritura P&uacute;blica &oacute; sentencia judicial para parejas del MISMO sexo. </li>
<li>(2)Hijos entre 12 y 18 a&oacute;os de edad, y aprobado por el plantel.</li>
<li>(3)Adultos  Mayores de 60 a&ntilde;os.</li>
<li><img src="../../imagenes/menu/obligado.png" width="12" height="12">Documentos m&iacute;nimos requeridos.</li>
</ul>
</div>
<!-- fin 2 -->    

<!--  RENOVACION TRABAJADOR index 3 style="display:none" -->
<div id="div3" >
	<table width="100%" border="0" cellspacing="0" class="tablero">
	    <th colspan="4">Informaci&oacute;n Adicional Renovaci&oacute;n Trabajador</th>
		<tr>
		<td align="left" width="17%">Tipo Documento Afiliado</td>
		<td width="28%"><select class="box1" name="sltTipoIdentificacion3" id="sltTipoIdentificacion3" onkeypress="tabular (this, event);" onchange="restablecerValoresCampos();document.getElementById('tNumero3').value=''"></select><img src="../../imagenes/menu/obligado.png" width="12" height="12"></td>
		<td width="19%">N&uacute;mero</td>
		<td width="36%"><input type="text" class="box1" name="tNumero3" id="tNumero3"  onblur="validarLongNumIdent(document.getElementById('sltTipoIdentificacion3').value,this);buscarPersona2(this,11,$('#sltTipoIdentificacion3').val());" onKeyPress="tabular (this, event);" onkeyup="validarCaracteresPermitidos(document.getElementById('sltTipoIdentificacion3').value,this);">
		  <img src="../../imagenes/menu/obligado.png" width="12" height="12"></td>
		</tr>
		<tr>
		<td align="left" width="17%">Nombre</td>
		<td colspan="3"><input type="text" class="boxlargo" name="tNombre3" id="tNombre3" readonly></td>
		</tr>
		<tr>
		<td align="left" width="17%">NIT Empleador</td>
		<td colspan="3"><input type="text" class="box1" name="tNit3" id="tNit3" onkeypress="return validarEspacio(event);" onBlur="validarLongNumIdent('5',this);buscarAfiliacionAxNit(this);" onKeyDown="tabular (this, event); solonumeros(this);" onkeyup='solonumeros(this);'><img src="../../imagenes/menu/obligado.png" width="12" height="12"><input type="text" class="boxlargo" name="tRazon3" id="tRazon3" readonly></td>
		</tr>
		<tr>
		  <td align="left">Salario</td>
		  <td><input type="text" class="box1" name="salario" id="salario" onBlur="calcularCategoria(this.value,document.getElementById('categoria').id)" onkeydown='solonumeros(this);' onkeyup='solonumeros(this);'>
		    <img src="../../imagenes/menu/obligado.png" alt="Obligatorio" width="12" height="12">
		    <input id="categoria" type="hidden">
		    </td>
		  <td>No Tarjeta</td>
		  <td><input type="text" class="box1" name="tarjeta" id="tarjeta" readonly>
		    <img src="../../imagenes/menu/obligado.png" alt="Obligatorio" width="12" height="12"></td>
		</tr>
		<tr>
		  <td align="left">Fecha Ingreso</td>
		  <td><input type="text" class="boxfecha" name="fecIngresoRenTr" id="fecIngresoRenTr" readonly>
		    <img src="../../imagenes/menu/obligado.png" alt="Obligatorio" width="12" height="12"></td>
		  <td>N&uacute;mero de folios</td>
		 
		 <td><input type="text" class="box1" name="tFolios3" id="tFolios3" maxlength="2" onkeydown='solonumeros(this);' onkeyup='solonumeros(this);'>
		    <img src="../../imagenes/menu/obligado.png" width="12" height="12"></td>
		</tr>
		<tr>
		  <td align="left">Tipo formulario</td>
		  <td><select name="tipForm2" class="box1" id="tipForm2">
		    <option selected="selected">Seleccione..</option>
		    <?php
			
			$consulta = $objClase->mostrar_datos(9,3);
		    while( $row = mssql_fetch_array( $consulta ) ){
				echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
			}
			?>
		  </select>
		    <img src="../../imagenes/menu/obligado.png" alt="Obligatorio" width="12" height="12"></td>
		  <td>Afiliaci&oacute;n M&uacute;ltiple</td>
		  <td><select name="multiple" class="box1" id="multiple">
		    <option value="N" selected="selected">NO</option>
		    <option value="S">SI</option>
		  </select>
		    <img src="../../imagenes/menu/obligado.png" alt="Obligatorio" width="12" height="12"></td>
		</tr>
		<tr><td>Notas</td><td colspan="3"><textarea name="notas3" id="notas3" class="boxlargo"></textarea></td></tr>
	</table>

	<div id="docsRen" align="left">
		<!-- DOCUMENTOS RENOVACION AFILIADO -->
		<div>
		      <h4>LISTADO DOCUMENTOS A SOLICITAR</h4>
		      <p>
		      <input type="checkbox" name="docTrabajadorRen" id="formularioRen">
		      Formulario afiliaci&oacute;n.
		      <img src="../../imagenes/menu/obligado.png" width="12" height="12">
		      </p>
		      <p>
		      <input type="checkbox" name="docTrabajadorRen"  id="docCedulaRen">
		      Fotocopia de la c&eacute;dula
		      </p>
		      <span style="background:url(../../imagenes/FondoTabla1.png) repeat-x; border:1px dotted #666; display:block;width:300px"  class="ui-corner-all">
		      <table border="0" width="100%">
		      <tr><td>&nbsp;</td><td align="center"><b>SI</b></td><td align="center"><a><strong>NO</strong></a></td></tr>
		      <tr>
		        <td><strong>&iquest;Convivencia?</strong></td>
		        <td align="center"><input type="radio" name="rconvivenRen" id="si2Ren" value="s"></td>
		        <td align="center"><input name="rconvivenRen" type="radio" id="no2Ren" value="n" checked="checked"></td>
		      </tr>
		      <tr>
		        <td><strong>&iquest;Hijos-Hermanos?</strong></td>
		        <td align="center"><input type="radio" name="rhijosRen" id="si3Ren" value="s"></td>
		        <td align="center"><input name="rhijosRen" type="radio" id="no3Ren" value="n" checked="checked"></td>
		      </tr>
		      <tr>
		        <td><strong>&iquest;Padres?</strong>&nbsp;
		          <select id="tipoServicioRen" style="display:none">
		            <option value="0">Seleccione..</option>
		            <option  value="1">SERVICIOS</option>
		            <option  value="2">SUBSIDIO</option>
		            </select></td>
		        <td align="center"><input type="radio" name="rpadresRen" id="siRen" value="s"></td>
		        <td align="center"><input name="rpadresRen" type="radio" id="noRen" value="n" checked="checked"></td>
		        
		      </tr>
		      </table>
		      </span>
		</div>
		<!-- DOCUMENTOS CONYUGE -->
		<div>
			<hr>
			 <h4>Documentos Conyuge</h4>
			  <p>
				  <input type="checkbox" name="docConyugeRen" id="check1Ren">
				  <label for="check1Ren" >Registro civil MATRIMONIO<sup>1</sup>.</label>
			  </p>
			  <p>
				  <input type="checkbox" name="docConyugeRen" id="check2Ren">
				  <label for="check2Ren" >Fotocopia AMPLIADA y LEGIBLE de la c&eacute;dula del c&oacute;nyuge.</label>
			  </p>
			  <p>
				<input type="checkbox" name="docConyugeRen" id="check3Ren"> <label for="check3Ren">Formato de convivencia.</label>
			  </p>
		</div>
			<!-- DOCUMENTOS HIJOS -->
			<div>
				<hr>
				<h4>Documentos  Hijos-Hijastros-Hermanos</h4>
				<p>
					<input type="checkbox" name="docHijosRen" id="check3Ren">
					Fotocopia Registro civil.
					<img src="../../imagenes/menu/obligado.png" width="12" height="12">
				</p>
				<p>
					<input type="checkbox" name="docHijosRen" id="check4Ren">
					Certificado de escolaridad<sup>2</sup>.
				</p>
				<p>
					<input type="checkbox" name="docHijosRen" id="check5Ren">
					Sentencia judicial de custodia.
				</p>
				<p>
					<input type="checkbox" name="docHijosRen" id="check6Ren">
					Sentencia de adopci&oacute;n.
				</p>
				<p>
					<input type="checkbox" name="docHijosRen" id="check7Ren">
					Certificado de discapacidad.
				</p>
			</div>
		<!-- DOCUMENTOS PADRES -->
			<div>
				<hr>
				<h4>Documentos  Padres</h4>
				<p>
				  <input type="checkbox" name="docPadresRen" id="check8Ren">
				  Fotocopia de la c&eacute;dula AMPLIADA.
				  <img src="../../imagenes/menu/obligado.png" width="12" height="12">
				</p>
				<p>
				  <input type="checkbox" name="docPadresRen" id="check9Ren">
				  Registro civil trabajador.
				  <img src="../../imagenes/menu/obligado.png" width="12" height="12">
				</p>
				<p>
				  <input type="checkbox" name="docPadresRen" id="check10Ren">
				  Certificado de supervivencia<sup>3</sup>.
				</p>
				<p>
				  <input type="checkbox" name="docPadresRen" id="check11Ren">
				  Declaraci&oacute;n extrajuicio.
				</p>
			</div>
		</div>
		<!-- FIN DIV DOCS -->
</div>
<!-- fin 3 -->  

<!--  AFILIACION EMPRESA index 4 -->
<div id="div4" >
<table width="100%" border="0" cellspacing="0" class="tablero">
<tr>
<th colspan="2" align="left"><center>Informaci&oacute;n Adicional Afiliaci&oacute;n Empresa</center></th>
</tr>
<tr>
	<td align="left" width="17%">NIT Empresa</td>
	<td><input type="text" class="box1" name="tNit4" id="tNit4" onBlur="validarLongNumIdent('5',this);contarNits(this);" onKeyPress="tabular (this, event);" onkeyup='solonumeros(this);'><img src="../../imagenes/menu/obligado.png" width="12" height="12"></td>
</tr>
<tr>
	<td align="left" ><label for="tRazon4">Raz&oacute;n social</label></td>
	<td><input type="text" class="boxlargo" name="tRazon4" id="tRazon4"></td>
</tr>
<tr>
	<td align="left" width="17%">N&uacute;mero de folios</td>
	<td><input type="text" class="box1" name="tFolios4" id="tFolios4" maxlength="2" onKeydown="solonumeros(this);" onkeyup='solonumeros(this);'><img src="../../imagenes/menu/obligado.png" width="12" height="12"></td>
</tr>
<tr><td>Notas</td><td><textarea name="notas4" id="notas4" class="boxlargo"></textarea></td></tr>
</table>

<!-- CAPA DE DOCUMENTOS PARA AFILIACION EMPRESA -->
	<div id="docsAfiEmpresa" align="left">
		<div>
			<h4>LISTADO DOCUMENTOS A SOLICITAR</h4>
			<p>
				<input type="checkbox" name="formAfiEmpresa" id="formAfiEmpresa"> <label for="formAfiEmpresa">Formulario de afiliaci&oacute;n.</label>
			</p>
			<p>
				<input type="checkbox" name="rut1" id="checkrut1"> <label for="checkrut1">RUT.</label>
			</p>
			<p>
				<input type="checkbox" name="nomina" id="checknomina"> <label for="checknomina">Nomina.</label>
			</p>
			<p>
				<input type="checkbox" name="camaradecomercio" id="camaradecomercio"> <label for="camaradecomercio">C&aacute;mara de comercio.</label>
			</p>
			<br>
			<strong>Tipo de Empresa:</strong>
			<br>
			<span style="background:url(../../imagenes/FondoTabla1.png) repeat-x; border:1px dotted #666; display:block;width:300px"  class="ui-corner-all">
		          <strong>Persona Natural </strong>
		          <input type="radio" name="rempresa" id="natural" value="n"><br>
		          <strong>Persona Juridica</strong>
		          <input type="radio" name="rempresa" id="juridica" value="j" checked="checked"><br>
		          <table id="t_personanatural">
					<tr>
		          		<th>&nbsp;</th>
		          		<th>Si</th>
		          		<th>No</th>
		          	</tr>
		          	<tr>
		          		<td>Estuvo afiliado a otra caja?</td>
		          		<td><input type="radio" name="pn_afiotracaja" id="pn_afiotracaja_si" value="s"></td>
		          		<td><input type="radio" name="pn_afiotracaja" id="pn_afiotracaja_no" value="n"></td>
		          	</tr>
		          </table>
		          
		          <table id="t_personajuridica">
		          	<tr>
		          		<th>&nbsp;</th>
		          		<th>Si</th>
		          		<th>No</th>
		          	</tr>
		          	<tr>
		          		<td>Estuvo afiliado a otra caja?</td>
		          		<td><input type="radio" name="pj_afiotracaja" id="pj_afiotracaja_si" value="s"></td>
		          		<td><input type="radio" name="pj_afiotracaja" id="pj_afiotracaja_no" value="n"></td>
		          	</tr>
		          	<tr>
		          		<td>Sin &aacute;nimo de Lucro?</td>
		          		<td><input type="radio" name="animodelucro" id="animodelucro_si" value="s"></td>
		          		<td><input type="radio" name="animodelucro" id="animodelucro_no" value="n"></td>
		          	</tr>
		          	<tr>
		          		<td>Es cooperativa?</td>
		          		<td><input type="radio" name="cooperativa" id="cooperativa_si" value="s"></td>
		          		<td><input type="radio" name="cooperativa" id="cooperativa_no" value="n"></td>
		          	</tr>
		          	<tr>
		          		<td>Es propiedad horizontal?</td>
		          		<td><input type="radio" name="propiedadhorizontal" id="propiedadhorizontal_si" value="s"></td>
		          		<td><input type="radio" name="propiedadhorizontal" id="propiedadhorizontal_no" value="n"></td>
		          	</tr>
		          	<tr>
		          		<td>Es consorcio o uni&oacute;n temporal?</td>
		          		<td><input type="radio" name="consorcio" id="consorcio_si" value="s"></td>
		          		<td><input type="radio" name="consorcio" id="consorcio_no" value="n"></td>
		          	</tr>
		          </table>
		      </span>
		</div>
		<!-- DOCUMENTOS NATURAL -->
		<div>
			<hr>
			<h4>Documentos Persona Natural</h4>
			<p>
				<input type="checkbox" name="fotoCedula" id="checkFotoCedula">
				Fotocopia c&eacute;dula del Empleador.
			</p>
			<p id="p_pn_pazysalvo">
				<input type="checkbox" name="pn_pazysalvo" id="pn_checkpazysalvo">
				Paz y salvo.
			</p>
		</div>
		<!-- DOCUMENTOS JURIDICA -->
		<div>
			<hr>
			<h4>Documentos Persona Jur&iacute;dica</h4>
			<p id="p_pj_pazysalvo">
				<input type="checkbox" name="pj_pazysalvo" id="pj_checkpazysalvo">
				Paz y salvo.
			</p>
			<p id="p_lucro">
				<input type="checkbox" name="lucro" id="checklucro">
				Personer&iacute;a Jur&iacute;dica.
			</p>
			<p id="p_ecosolida">
				<input type="checkbox" name="ecosolida" id="checkecosolida">
				Certificado Superintendencia de econom&iacute;a solidaria.
			</p>
			<p id="p_propiedadhorizontal">
				<input type="checkbox" name="chk_propiedadhorizontal" id="chk_propiedadhorizontal">
				Acta de nombramiento de administraci&oacute;n Actual.
			</p>
			<p id="p_consorcio">
				<input type="checkbox" name="chk_consorcio" id="chk_consorcio">
				Acta de conformaci&oacute;n.
			</p>
		</div>
	</div>
	<!-- FIN CAPA DE DOCUMENTOS PARA AFILIACION EMPRESA -->
	</div>
<!-- fin 4 -->    

<!--  RENOVACION EMPRESA index 5 -->  
<div id="div5" >
<table width="100%" border="0" cellspacing="0" class="tablero">
<tr>
<th colspan="2" align="left"><center>Informaci&oacute;n Adicional Renovac&oacute;n Empresa</center></th>
  </tr>
    <tr>
    <td align="left" width="17%">NIT Empresa</td>
    <td>
    	<input type="text" class="box1" name="tNit5" id="tNit5" onBlur="validarLongNumIdent('5',this);buscarNitInactiva2(this);" onKeyPress="tabular (this, event); solonumeros(this);" onkeyup='solonumeros(this);'><img src="../../imagenes/menu/obligado.png" width="12" height="12">
    	<input type="text" class="boxlargo" name="tRazon5" id="tRazon5" readonly>
    </td>
    </tr>
    <tr>
    <td align="left" width="17%">N&uacute;mero de folios</td>
    <td><input type="text" class="box1" name="tFolios5" id="tFolios5" maxlength="2" onKeyPress="solonumeros(this);" onkeyup='solonumeros(this);'><img src="../../imagenes/menu/obligado.png" width="12" height="12"></td>
    </tr>
    <tr><td>Notas</td><td><textarea name="notas5" id="notas5" class="boxlargo"></textarea></td></tr>
    </table>
</div>
<!-- fin 5 -->   

<!--   CERTIFICADOS index 6    -->
<div id="div6" >
    <table width="100%" border="0" cellspacing="0" class="tablero">
    <tr>
<th colspan="4" align="left"><center>
  Informaci&oacute;n Adicional Certificados
</center></th>
  </tr>
    <tr>
    <td align="left" width="17%">Tipo Documento Afiliado</td>
    <td width="31%"><select class="box1" name="sltTipoIdentificacion6" id="sltTipoIdentificacion6" onkeypress="tabular (this, event);" onchange="$('#tNumero6').val('').trigger('blur');"></select><img src="../../imagenes/menu/obligado.png" width="12" height="12"></td>
    <td width="16%">N&uacute;mero</td>
    <td width="36%"><input type="text" name="tNumero6" id="tNumero6" class="box1" onBlur="validarLongNumIdent(document.getElementById('sltTipoIdentificacion6').value,this);buscarPersona2(this,1,$('#sltTipoIdentificacion6').val());" onKeyPress="tabular (this, event);" onkeyup="validarCaracteresPermitidos(document.getElementById('sltTipoIdentificacion6').value,this);">
      <img src="../../imagenes/menu/obligado.png" width="12" height="12"></td>
    </tr>
    <tr>
      <td>Nombre</td><td colspan="3"><input type="text" class="boxlargo" name="tNombre6" id="tNombre6" readonly>
    </td></tr>
      <tr><td>Beneficiario</td><td colspan="3">
    <select name="lisBeneficiarios" id ="lisBeneficiarios" class="boxlargo" onchange="$('#lTipoCer').val(0).trigger('change');">
    <option value="0" selected="selected">Seleccione...</option>
    </select>
    <img src="../../imagenes/menu/obligado.png" width="12" height="12">
    Actualizar Aqu&iacute;*</td></tr>
    <tr><td align="left" width="17%">Tipo de certificado</td>
    <td colspan="3"><select name="lTipoCer" id="lTipoCer" class="boxlargo" onChange="validarCert(this)">
    <option value="0" selected="selected">Seleccione...</option>
    <option value="55" id="tc0">Escolaridad</option>
    <option value="56" id="tc1">Universidad Semestre A</option>
    <option value="56" id="tc2">Universidad Semestre B</option>
    <option value="57" id="tc3">Supervivencia</option>
    <option value="58" id="tc4">Discapacidad</option>
    </select><img src="../../imagenes/menu/obligado.png" width="12" height="12">
    </td>
    </tr>
     <tr>
       <td colspan="4">
        <div id="cerVigencia">
        <table width="100%" border="0">
        <tr>
    
      <td>A&ntilde;o Vigencia</td>
      <td colspan="3">
       <input type="radio" name="vigencia" id="vigencia1" value="0" onClick="vVigencia(0)"> <label id="l1" for="vigencia1" >A&ntilde;o Actual</label><br/>
      <input type="radio" name="vigencia" id="vigencia2" value="1" onClick="vVigencia(1)"> <label id="l2" for="vigencia2" >A&ntilde;o anterior</label><br/> 
      <!--<input type="radio" name="vigencia" id="vigencia1" value="0"> <label id="l1" for="vigencia1" >A&ntilde;o Actual</label><br/>
      <input type="radio" name="vigencia" id="vigencia2" value="1"> <label id="l2" for="vigencia2" >A&ntilde;o anterior</label><br/>-->
      Periodo Inicial
        <input name="pInicialV" type="text" class="box1 monthPicker" id="pInicialV" value="" onBlur="validarVigencia(this.value,1); validarPeriodosDatePicker(this.id,'pFinalV','errorVig'); " disabled="disabled" maxlength="6">
        Periodo Final
        <input name="pFinalV" type="text" class="box1 monthPickerF" id="pFinalV" value="" onBlur="validarVigencia(this.value,2); validarPeriodosDatePicker('pInicialV',this.id,'errorVig');" disabled="disabled" maxlength="6">
        <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12">
        <span class="Rojo" id="errorVig"></span>
        </td>
        
    </tr>
        </table>
       </div>
       </td>
       </tr>
     
    <tr>
      <td>N&uacute;mero de folios</td><td colspan="3"><input type="text" name="tFolios6" id="tFolios6" class="box1" maxlength="2" onKeydown="solonumeros(this);" onkeyup='solonumeros(this);'><img src="../../imagenes/menu/obligado.png" width="12" height="12"></td></tr>
    <tr><td>Notas</td><td colspan="3"><textarea name="notas6" id="notas6" class="boxlargo"></textarea></td></tr>
    </table>
</div>
<!-- fin 6 -->    

<!--   NOVEDADES EMPRESA index 7  -->
    
<div id="div7">
<table width="100%" border="0" cellspacing="0" class="tablero">
<tr>
<th colspan="2" align="left"><center>
    Informaci&oacute;n Adicional Novedades Empresa
</center></th>
</tr>  
<tr>
<td align="left" width="17%">NIT</td>
<td><input type="text" name="tNit7" id="tNit7" class="box1" onBlur="validarLongNumIdent('5',this);buscarNit(this);" onKeyPress="tabular (this, event); solonumeros(this);" onkeyup='solonumeros(this);'><img src="../../imagenes/menu/obligado.png" width="12" height="12"></td>
    </tr>  
<tr>
<td width="17%">Empresa</td>
<td><input type="text" class="boxlargo" name="tRazon7" id="tRazon7" readonly>
</td>
</tr>
<tr>
<td>N&uacute;mero de folios</td>
<td><input type="text" name="tFolios7" id="tFolios7" class="box1" maxlength="2" onKeyPress="solonumeros(this);" onkeyup='solonumeros(this);'><img src="../../imagenes/menu/obligado.png" width="12" height="12"></td>
</tr>
<tr>
<td>Notas</td>
<td><textarea name="notas7" id="notas7" class="boxlargo"></textarea></td>
</tr>
</table>
</div>

<!-- fin 7 -->   

<!--   AFILIACION FONEDE  index 8   -->
    
<div id="div8">  
   
<table width="100%" border="0" cellspacing="0" class="tablero">
<tr>
  <th colspan="4" align="left"><center>Informaci&oacute;n Adicional Afiliaci&oacute;n FONEDE</center></th>
</tr>
<tr>
    <td align="left" width="17%">Tipo documento Afiliado</td>
    <td><select class="box1" name="sltTipoIdentificacion8" id="sltTipoIdentificacion8" onkeypress="tabular (this, event);" onchange="document.getElementById('txtIdentificacion').value=''"></select><img src="../../imagenes/menu/obligado.png" width="12" height="12"></td>
</tr>
<tr>
    <td>N&uacute;mero</td>
    <td>
    	<input type="text" class="box1" name="txtIdentificacion" id="txtIdentificacion" onBlur="validarLongNumIdent(document.getElementById('sltTipoIdentificacion8').value,this);" onkeyup="validarCaracteresPermitidos(document.getElementById('sltTipoIdentificacion8').value,this);">
    	<img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12">
    	<input type="hidden" id="hdnIdpostulante" name="hdnIdpostulante" value="X">
    	<input type="hidden" id="hdnEstadoPostulante" name="hdnEstadoPostulante" value="X">
    	<input type="hidden" id="hdnIdpostulacion" name="hdnIdpostulacion" value="X">
    </td>
</tr>
    <tr>
    <td align="left" width="17%">N&uacute;mero de folios</td>
    <td colspan="3"><input type="text" class="box1" name="tFolios8" id="tFolios8" maxlength="2" onkeydown='solonumeros(this);' onkeyup='solonumeros(this);' ><img src="../../imagenes/menu/obligado.png" width="12" height="12"></td>
    </tr>
    <tr><td>Notas</td><td colspan="3"><textarea name="notas8" id="notas8" class="boxlargo"></textarea></td></tr>
    <tr>
      <td colspan="2"><h3>Hist&oacute;rico de afiliaciones a otras cajas</h3></td>
    </tr>
    <tr>
      <td>Tiene afiliaciones a otras cajas de compensaci&oacute;n?</td>
      <td colspan="3">
      	<input type="radio" id="opt_afiotrascajas_si" name="opt_afiotrascajas_fonede" value="s" /> <label for="opt_afiotrascajas_si" >Si</label>
      	<input type="radio" id="opt_afiotrascajas_no" name="opt_afiotrascajas_fonede" value="n" /> <label for="opt_afiotrascajas_no" >No</label> 
	  </td>
    </tr>
    <tr>
      <td colspan="4">
		<!-- Dialogo para crear nueva historial laboral y afiliacion a otras cajas -->
		<div id="div-nuevohistorial" style="display:none">
			<table class="tablero">
				<tr>
					<td>
						<span class="item max_1">
							<label class="pequenio">Caja de Compensaci&oacute;n</label> 
							<input type="text" name="txtCajas" id="txtCajas" class="grande campotexto requerido" alt="Caja de compensacion" />
							<input type="hidden" name="hdnCajas" id="hdnCajas" value="" />
						</span>
					</td>
				</tr>
				<tr>
					<td>
						<span class="item max_2">
							<label class="mediano">Fecha de ingreso</label> 
							<input type="text" name="fecha_ingr_otra_caja" id="fecha_ingr_otra_caja" class="mediano campotexto requerido fechas" alt="Fecha de ingreso" />
						</span>
						<span class="item">
							<label class="mediano">Fecha de retiro</label> 
							<input type="text" name="fecha_ret_otra_caja" id="fecha_ret_otra_caja" class="mediano campotexto requerido fechas" alt="Fecha de retiro" />
						</span>
					</td>
				</tr>
				<tr>
					<td>
						<span class="item max_1">
							<label class="mediano">Tiempo(dias)</label> 
							<input type="text" name="txtTiempo" id="txtTiempo" class="pequeno campotexto requerido" readonly alt="Tiempo laborado en la empresa" />
						</span>
					</td>
				</tr>
				<tr>
					<td>
						<input type="button"  id="btn_agregar_tray_otra_caja" value="Agregar" />
					</td>
				</tr>
			</table>
		</div>
      </td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td colspan="3">
      	<label style="font-size:18pt" >Tiempo fonede: </label><span id="span_tiempo_total_fonede" style="font-size:16pt" ></span>
      	<table>
      		<thead>
      			<tr>
      				<th>Caja</th>
      				<th>Fecha ingr.</th>
      				<th>Fecha ret.</th>
      				<th>Tiempo (dias)</th>
      			</tr>
      		</thead>
      		<tbody id="tbody_tabla_tray_otras_cajas">
      		</tbody>
      	</table>
      </td>
    </tr>
    <!-- <tr>
    	<td colspan="2" id="div-hist-laboral">
    		<div id="tabPanel" style="margin: 1%">
				<ul>
					<li><a href="../../fonede/afiliacion/mods/modHistorialLaboral.php"><span>Historial laboral</span></a></li>
				</ul>
			</div>
    	</td>
    </tr> -->
    </table>
    
<div id="capa_pestana_hist_laboral"></div>
    
    <div id="docs_fonede" align="left">
		<!-- DOCUMENTOS POSTULACION FONEDE -->
		<div>
			<h4>LISTADO DOCUMENTOS A SOLICITAR</h4>
			<p>
				<input type="checkbox" name="doc_formulario_fonede" id="doc_formulario_fonede">
				<label for="doc_formulario_fonede">Formulario de postulaci&oacute;n.</label>
				<img src="../../imagenes/menu/obligado.png" width="12" height="12">
			</p>
			<p>
				<input type="checkbox" name="doc_cedula_fonede"  id="doc_cedula_fonede">
				<label for="doc_cedula_fonede">Fotocopia de la c&eacute;dula</label>
				<img src="../../imagenes/menu/obligado.png" width="12" height="12">
			</p>
			<p>
				<input type="checkbox" name="doc_cert_eps_fonede"  id="doc_cert_eps_fonede">
				<label for="doc_cert_eps_fonede">Certificado EPS </label>
				<img src="../../imagenes/menu/obligado.png" width="12" height="12">
			</p>
			<span style="background:url(../../imagenes/FondoTabla1.png) repeat-x; border:1px dotted #666; display:block;width:300px"  class="ui-corner-all">
				<table border="0" width="100%">
					<tr>
						<td>&nbsp;</td><td align="center"><b>SI</b></td><td align="center"><a><strong>NO</strong></a></td>
					</tr>
					<tr>
						<td><strong>&iquest;Con vinculaci&oacute;n?</strong></td>
						<td align="center"><input type="radio" name="convinculacion_fonede" id="si_convinculacion_fonede" value="s"></td>
						<td align="center"><input type="radio" name="convinculacion_fonede" id="no_convinculacion_fonede" value="n" checked="checked"></td>
					</tr>
					<tr>
						<td><strong>&iquest;Convivencia?</strong></td>
						<td align="center"><input type="radio" name="convivencia_fonede" id="si_convivencia_fonede" value="s"></td>
						<td align="center"><input type="radio" name="convivencia_fonede" id="no_convivencia_fonede" value="n" checked="checked"></td>
					</tr>
					<tr>
						<td><strong>&iquest;Hijos, Hijastros, Hermanos ?</strong></td>
						<td align="center"><input type="radio" name="hijos_fonede" id="si_hijos_fonede" value="s"></td>
						<td align="center"><input type="radio" name="hijos_fonede" id="no_hijos_fonede" value="n" checked="checked"></td>
					</tr>
					<tr>
						<td>
							<strong>&iquest;Padres?</strong>&nbsp;
						</td>
						<td align="center"><input type="radio" name="padres_fonede" id="si_padres_fonede" value="s"></td>
						<td align="center"><input type="radio" name="padres_fonede" id="no_padres_fonede" value="n" checked="checked"></td>
					</tr>
				</table>
			</span>
			<!-- DOCUMENTOS POSTULADO -->
			<div>
				<hr>
				<h4>Documentos postulado</h4>
				<p>
					<input type="checkbox" name="doc_cert_laboral_fonede"  id="doc_cert_laboral_fonede">
					<label for="doc_cert_laboral_fonede" >Certificado laboral</label> <img src="../../imagenes/menu/obligado.png" width="12" height="12">
				</p>
			</div>
			<!-- DOCUMENTOS CONYUGE -->
			<div>
				<hr>
				<h4>Documentos Conyuge</h4>
				<fieldset>
					<legend>C&oacute;nyuge activo en la caja?</legend>
					<p>
						<input type="radio" name="conyactivo_fonede" id="si_conyactivo_fonede" value="s"> <label for="si_conyactivo_fonede">Si</label>
						<input type="radio" name="conyactivo_fonede" id="no_conyactivo_fonede" value="n" checked="checked" > <label for="no_conyactivo_fonede">No</label>
						<br/>
						<div id="conyactivo_docs">
							<label><strong>Nota:</strong> Debe demostrar que ya no existe relaci&oacute;n de convivencia</label>
							<p>
								<input type="checkbox" name="check_decextrajuicio_fonede" id="check_decextrajuicio_fonede">
								<label for="check_decextrajuicio_fonede" >Declaraci&oacute;n extra juicio.</label>
							</p>
							<p>
								<input type="checkbox" name="check_sentdivorcio_fonede" id="check_sentdivorcio_fonede">
								<label for="check_sentdivorcio_fonede" >Sentencia de divorcio.</label>
							</p>
						</div>
					</p>
				</fieldset>
				<p>
					<input type="checkbox" name="check_ceculacony_fonede" id="check_ceculacony_fonede">
					<label for="check_ceculacony_fonede">Fotocopia AMPLIADA y LEGIBLE de la c&eacute;dula del c&oacute;nyuge.</label>
				</p>
			</div>
			<!-- DOCUMENTOS HIJOS, HIJASTROS, HERMANOS -->
			<div>
				<hr>
				<h4>Documentos  Hijos-Hijastros-Hermanos</h4>
				<p>
					<input type="checkbox" name="check_regcivilhijos_fonede" id="check_regcivilhijos_fonede">
					<label for="check_regcivilhijos_fonede">Fotocopia Registro civil.</label>
					<img src="../../imagenes/menu/obligado.png" width="12" height="12">
				</p>
			</div>
			<!-- DOCUMENTOS PADRES -->
			<div>
				<hr>
				<h4>Documentos  Padres</h4>
				<p>
					<input type="checkbox" name="check_cedula_padres" id="check_cedula_padres">
					<label for="check_cedula_padres">Fotocopia de la c&eacute;dula AMPLIADA.</label>
					<img src="../../imagenes/menu/obligado.png" width="12" height="12">
				</p>
			</div>
		<!-- FIN DIV DOCS FONEDE -->
		</div>
	</div>

	<div id="dialog-fecharetiro_fonede" title="Afiliado activo" >
		<p>El postulante tiene una afiliaci&oacute;n activa con la caja.<br/>Verifique las trayectorias en la pesta&ntilde;a afiliado.</p>
		<p>Digite la fecha de retiro seg&uacute;n el certificado laboral.</p>
			<label for="fecharetiro_trayactual_fonede">Fecha de retiro</label>
			<input type="text" name="fecharetiro_trayactual_fonede" id="fecharetiro_trayactual_fonede" /><br/>
			<p id="p_inactivar_afiliado_fonede" style="display:none;">
				<label for="check_inactivar_afiliado_fonede">Inactivar afiliado</label> <input type="checkbox" name="check_inactivar_afiliado_fonede" id="check_inactivar_afiliado_fonede" />
			</p>
	</div>
  
</div> 

<!-- fin 8 -->
	<br />


<!--  AFILIACION BENEFICIARIOS index 9 -->
<div id="div9">
<table width="100%" border="0" cellspacing="0" class="tablero">
<tr>
  <th colspan="4" align="left"><center>Informaci&oacute;n Adicional Afiliaci&oacute;n Beneficiarios</center></th>
  </tr>
    <tr>
    <td align="left" width="17%">Tipo Documento Afiliado</td>
    <td width="25%"><select class="box1" name="sltTipoIdentificacion9" id="sltTipoIdentificacion9" onkeypress="tabular (this, event);" onchange="document.getElementById('tNumero9').value=''"></select><img src="../../imagenes/menu/obligado.png" width="12" height="12"></td>
    <td width="23%">N&uacute;mero</td>
    <td width="35%"><input type="text" class="box1" name="tNumero9" id="tNumero9" onBlur="validarLongNumIdent(document.getElementById('sltTipoIdentificacion9').value,this);buscarPersonaAfiliada(this);" onKeyPress="tabular (this, event);" onkeyup="validarCaracteresPermitidos(document.getElementById('sltTipoIdentificacion9').value,this);">
      <img src="../../imagenes/menu/obligado.png" width="12" height="12"></td>
    </tr>
    <tr>
    <td align="left" width="17%">Nombre</td>
    <td colspan="3"><input type="text" class="boxlargo" name="tNombre9" id="tNombre9" readonly></td>
    </tr>
    <tr>
    <td align="left" width="17%">&nbsp;</td>
    <td colspan="3">&nbsp;</td>
    </tr>
    <tr>
    <td align="left" width="17%">N&uacute;mero de Beneficiarios</td>
    <td colspan="3">
    <input type="text" class="box1" name="tNumero91" id="tNumero91" onKeyPress="tabular (this, event);" onKeydown="solonumeros(this);" onkeyup='solonumeros(this);'><img src="../../imagenes/menu/obligado.png" width="12" height="12">
    <div id="nombreBeneficiario"></div>
    </td>
    </tr>
    <tr>
    <td align="left" width="17%">N&uacute;mero de folios</td>
    <td colspan="3"><input type="text" class="box1" name="tFolios9" id="tFolios9" maxlength="2" onKeydown="solonumeros(this);" onkeyup='solonumeros(this);'><img src="../../imagenes/menu/obligado.png" width="12" height="12"></td>
    </tr>
    <tr><td>Notas</td><td colspan="3"><textarea name="notas9" id="notas9" class="boxlargo"></textarea></td></tr>
    </table>
</div>
<!-- fin 9 --> 

<!-- AFILIACION NUEVA SUCURSAL  index 10  -->

<div id="div10" >
<table width="100%" border="0" cellspacing="0" class="tablero">
<tr>
<th colspan="2">Informaci&oacute;n Adicional Nueva Sucursal </th>
</tr>
<tr>
    <td align="left" width="17%">NIT Empresa</td>
    <td><input type="text" class="box1" name="tNit10" id="tNit10" onBlur="validarLongNumIdent('5',this);buscarNit(this);" onKeyPress="tabular (this, event); solonumeros(this);" onkeyup='solonumeros(this);'><img src="../../imagenes/menu/obligado.png" width="12" height="12"><input type="text" class="boxlargo" name="tRazon10" id="tRazon10" readonly></td>
    </tr>
    <tr>
    <td align="left" width="17%">N&oacute;mero de folios</td>
    <td><input type="text" class="box1" name="tFolios10" id="tFolios10" maxlength="2" onKeyPress="solonumeros(this);" onkeyup='solonumeros(this);'><img src="../../imagenes/menu/obligado.png" width="12" height="12"></td>
    </tr>
    <tr><td>Notas</td><td><textarea name="notas10" id="notas10" class="boxlargo"></textarea></td></tr>
    </table>
</div>    
<!-- fin 10 --> 

<!-- reclamo trabajador no giro index 11 style="display:none" -->
<div id="div11"  >
<table width="100%" border="0" cellspacing="0" class="tablero">
<tr>
<th colspan="4" ><center>Informaci&oacute;n Adicional Reclamo Trabajador</center> </th>
</tr>
<!-- <tr>
<td align="left" width="17%">NIT Empresa</td>
<td colspan="3"><input type="text" class="box1" name="tNit11" id="tNit11" onBlur="buscarNit(this);" onKeyPress="tabular (this, event);"><img src="../../imagenes/menu/obligado.png" width="12" height="12">
<input type="text" class="boxlargo" name="tRazon11" id="tRazon11" readonly></td>
</tr>-->
<tr> 
<td align="left" width="17%">Tipo Documento</td>
<td><select class="box1" name="sltTipoIdentificacion11" id="sltTipoIdentificacion11" onkeypress="tabular (this, event);" onchange="document.getElementById('tNumero11').value=''">
</select>
  <img src="../../imagenes/menu/obligado.png" alt="Campo obligdo" width="12" height="12"></td>
<td>N&uacute;mero</td>
<td><input type="text" class="box1" name="tNumero11" id="tNumero11" onBlur="validarLongNumIdent(document.getElementById('sltTipoIdentificacion11').value,this);buscarPersona2(this,3,$('#sltTipoIdentificacion11').val());" onKeyPress="tabular (this, event);" onkeyup="validarCaracteresPermitidos(document.getElementById('sltTipoIdentificacion11').value,this);">
  <img src="../../imagenes/menu/obligado.png" alt="Campo obligdo" width="12" height="12"></td>
</tr>
<tr>
<td align="left">Afiliado</td>
<td colspan="3"><input name="tNombre11" type="text" class="boxlargo" id="tNombre11" readonly></td>
</tr>
<tr>
<td align="left">&nbsp;</td>
</tr>
<tr>
<td align="left" width="17%">Causal</td>
<td colspan="3">
<select name="causal1" id="causal1" class="boxlargo">
<option value="0" selected="selected">Seleccione</option>
<?php

$consulta = $objClase->mostrar_datos(23,3);
while( $row = mssql_fetch_array( $consulta ) ){
	echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
}
?>
</select><img src="../../imagenes/menu/obligado.png" width="12" height="12"></td>
    </tr>
<tr>
    <td align="left" width="17%">Periodo Inicial</td>
    <td width="29%"><input name="periodoI1" type="text" class="box1 monthPicker1" id="periodoI1" maxlength="6"><img src="../../imagenes/menu/obligado.png" width="12" height="12"></td>
    <td width="17%">Periodo Final</td>
    <td width="37%"><input name="periodoF1" type="text" class="box1 monthPicker1" id="periodoF1" maxlength="6">
      <img src="../../imagenes/menu/obligado.png" width="12" height="12">
      <span class="Rojo" id="errorVig1"></span></td>
     </tr>
 
 <tr><td>Notas</td><td colspan="3"><textarea name="notas11" id="notas11" class="boxlargo"></textarea></td></tr>
</table>
</div>
<!-- fin 11  -->

<!-- reclamo empresa no giro index 12 -->
<div id="div12"  >
<table width="100%" border="0" cellspacing="0" class="tablero">
<tr>
<th colspan="4" ><center>Informaci&oacute;n Adicional Reclamo Empresa</center> </th>
</tr>
<tr>
<td align="left" width="17%">NIT Empresa</td>
<td colspan="3"><input type="text" class="box1" name="tNit12" id="tNit12" onBlur="validarLongNumIdent('5',this);buscarNit(this), buscarSucursalesCombo(this)" onKeyPress="tabular (this, event);" value="891180008" onkeyup='solonumeros(this);'><img src="../../imagenes/menu/obligado.png" width="12" height="12">
<input type="text" class="boxlargo" name="tRazon12" id="tRazon12" readonly></td>
</tr>
<tr>
<td align="left" width="17%">Sucursal</td>
<td colspan="3">
<select class="box1" name="comboSucursal" id="comboSucursal" >
<option value="0" selected="selected">Seleccione</option>

</select>
  <img src="../../imagenes/menu/obligado.png" alt="Campo obligdo" width="12" height="12"></td>
</tr>
<tr>
  <td align="left" width="17%">Causal</td>
  <td colspan="3">
  <select name="causal2" id="causal2" class="boxlargo">
  <option value="0" selected="selected">Seleccione</option>
  <?php

$consulta = $objClase->mostrar_datos(24,3);
while( $row = mssql_fetch_array( $consulta ) ){
	echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
}
?>
  </select><img src="../../imagenes/menu/obligado.png" width="12" height="12"></td>
</tr>
<tr>
    <td align="left" width="17%">Periodo Inicial</td>
    <td width="29%"><input name="periodoI2" type="text" class="box1 monthPicker2" id="periodoI2" maxlength="6"><img src="../../imagenes/menu/obligado.png" width="12" height="12"></td>
    <td width="17%">Periodo Final</td>
    <td width="37%"><input name="periodoF2" type="text" class="box1 monthPicker2" id="periodoF2" maxlength="6">
      <img src="../../imagenes/menu/obligado.png" width="12" height="12">
      <span class="Rojo" id="errorVig2"></span></td>
     </tr>
<tr><td>Notas</td><td colspan="3"><textarea name="notas12" id="notas12" class="boxlargo"></textarea></td></tr>
    </table>
</div>
<!-- fin 12  -->      

<!-- defunciones index 13 -->
<div id="div13"  >
<table width="100%" border="0" cellspacing="0" class="tablero">
<tr>
<th colspan="4" ><center>Informaci&oacute;n Adicional Defunciones</center> </th>
</tr>
<tr>
<td align="left" width="17%">Tipo Documento Afiliado</td>

<td><select class="box1" name="sltTipoIdentificacion13" id="sltTipoIdentificacion13" onkeypress="tabular (this, event);">
</select><img src="../../imagenes/menu/obligado.png" width="12" height="12"></td>
<td>N&uacute;mero</td>
<td><input type="text" class="box1" name="tNumero13" id="tNumero13" onBlur="validarLongNumIdent(document.getElementById('sltTipoIdentificacion13').value,this); buscarPersona2(this,4,$('#sltTipoIdentificacion13').val());" onKeyPress="tabular (this, event);" onkeyup="validarCaracteresPermitidos(document.getElementById('sltTipoIdentificacion13').value,this);"></td>
</tr>
<tr>
  <td align="left">Afiliado</td>
  <td colspan="3" id="nombreT"><input name="tNombre13" type="text" class="boxlargo" id="tNombre13"  onKeydown="solotexto(this);" onkeyup='solotexto(this);'></td>
</tr>
<tr>
  <td align="left">Quien falleci&oacute;?</td>
  <td colspan="3">
  <select id="comboQuien" name="comboQuien" class="boxlargo" onChange="buscarFallecido(this);">
  <option value="0" selected="selected">Seleccione</option>
  <option value="1">Trabajador</option>
  <option value="2">Hijo</option>
  <option value="3">Padre</option>
  </select>
  <img src="../../imagenes/menu/obligado.png" alt="Campo obligdo" width="12" height="12"></td>
</tr>
<tr>
<td align="left" width="17%">Fallecido</td>
<td colspan="3">
<select class="boxlargo" name="comboFallecido" id="comboFallecido" onblur="validarDifuntoConQuienRadica();">
<option value="0" selected="selected">Seleccione</option>

</select>
  <img src="../../imagenes/menu/obligado.png" alt="Campo obligdo" width="12" height="12"></td>
</tr>
<tr>
  <td align="left" width="17%">Folios</td>
  <td colspan="3"><input type="text" class="box1" name="tFolios13" id="tFolios13" maxlength="2" onKeydown="solonumeros(this);" onkeyup='solonumeros(this);'>
    <img src="../../imagenes/menu/obligado.png" width="12" height="12"></td>
</tr>
<tr><td>Notas</td><td colspan="3"><textarea name="notas13" id="notas13" class="boxlargo"></textarea></td></tr>
    </table>
    
    <!-- DOCUMENTOS DEFUNCION -->
    <div id="docsDefuncion">
		<h4>LISTADO DOCUMENTOS A SOLICITAR</h4>
		<p>
			<input type="checkbox" name="doc_registrodef" id="doc_registrodef">
			<label for="doc_registrodef">Registro de defunci&oacute;n.</label>
			<img src="../../imagenes/menu/obligado.png" width="12" height="12">
		</p>
		<p>
			<input type="checkbox" name="doc_cartasolicitud"  id="doc_cartasolicitud">
			<label for="doc_cartasolicitud">Carta de solicitud</label>
			<img src="../../imagenes/menu/obligado.png" width="12" height="12">
		</p>
		<p>
			<input type="checkbox" name="doc_fotocopiaccben"  id="doc_fotocopiaccben">
			<label for="doc_fotocopiaccben" >Fotocopia del documento de identidad del solicitante.</label> <img src="../../imagenes/menu/obligado.png" width="12" height="12">
		</p>
		<span style="background:url(../../imagenes/FondoTabla1.png) repeat-x; border:1px dotted #666; display:block;width:300px"  class="ui-corner-all">
			<table border="0" width="100%">
				<tr>
					<td>&nbsp;</td><td align="center"><b>Beneficiario</b></td><td align="center"><a><strong>Trabajador</strong></a></td>
				</tr>
				<tr>
					<td><strong>&iquest;Qui&eacute;n es el Fallecido?</strong></td>
					<td align="center"><input type="radio" name="rfallecido" id="benef_fallecido" value="b"></td>
					<td align="center"><input type="radio" name="rfallecido" id="traba_fallecido" value="t"></td>
				</tr>
			</table>
		</span>
		<!-- DOCUMENTOS MUERTE DE TRABAJADOR -->
		<div>
			<hr>
			<h4>Documentos de trabajador fallecido</h4>
			<p>
				<input type="checkbox" name="doc_certificadoretiro" id="doc_certificadoretiro">
				<label for="doc_certificadoretiro">Certificado de retiro de la empresa.</label>
				<img src="../../imagenes/menu/obligado.png" width="12" height="12">
			</p>
		</div>
	</div>
    <!-- FIN DOCUMENTOS DEFUNCION -->
</div>
<!-- fin 13  -->  
    
<!-- disolucion convivencia index 14 -->
<div id="div14"  >
<table width="100%" border="0" cellspacing="0" class="tablero" id="div-conyuge">
<tr>
<th colspan="4" ><center>Disoluci&oacute;n Convivencia</center> </th>
</tr>
<tr>
  <td align="left">Tipo Documento Afiliado</td>
  <td><select class="box1" name="sltTipoIdentificacion14" id="sltTipoIdentificacion14" onkeypress="tabular (this, event);" onchange="document.getElementById('tNumero14').value=''">
  </select>
    <img src="../../imagenes/menu/obligado.png" width="12" height="12"></td>
  <td>N&uacute;mero</td>
  <td><input type="text" class="box1" name="tNumero14" id="tNumero14" onBlur="validarLongNumIdent(document.getElementById('sltTipoIdentificacion14').value,this);buscarPersona2(this,5,$('#sltTipoIdentificacion14').val());" onKeyPress="tabular (this, event);" onkeyup="validarCaracteresPermitidos(document.getElementById('sltTipoIdentificacion14').value,this);">
  <img src="../../imagenes/menu/obligado.png" width="12" height="12"><input type="text" class="boxfecha" readonly>
   </td>
</tr>
<tr>
<td align="left" width="17%">Afiliado</td>
<td colspan="3" ><input name="tNombre14" type="text" class="boxlargo" id="tNombre14" readonly ></td>
</tr>
<tr>
  <td colspan="4" align="left">
  <h3>Relaci&oacute;n de Convivencia</h3>
<table width="100%" border="0" cellspacing="0" class="tablero" id="conyuge">
<tr>
<th width="10%">Identificaci&oacute;n</th>
<th width="69%" >Nombre C&oacute;nyuge</th>
<th width="7%" >&nbsp;</th>
<th width="7%" >&nbsp;</th>
<th width="7%" >&nbsp;</th>
</tr>
</table>
  </td>
  </tr>
<tr><td>Notas</td><td colspan="3"><textarea name="notas14" id="notas14" class="boxlargo"></textarea></td></tr>
    </table>
</div>
<!-- fin 14  -->    

<!-- formularios multiples index 15 -->
<div id="div15"  >
	<table width="100%" border="0" cellspacing="0" class="tablero" >
		<tr>
			<th colspan="2" ><center>Afiliaciones Multiples</center> </th>
			</tr>
			<tr>
			  <td align="left">NIT </td>
			  <td colspan="2">
			  <input type="text" class="box1" name="tNit15" id="tNit15" onBlur="validarLongNumIdent('5',this); buscarNit(this);" onKeyPress="tabular (this, event);" onkeyup='solonumeros(this);'> <img src="../../imagenes/menu/obligado.png" width="12" height="12">
			  <input type="text" class="boxlargo" name="tRazon15" id="tRazon15" readonly></td>
			</tr>
			<tr>
			 <td align="left" width="17%">Nro Afiliaciones</td>
			 <td >
			 <input type="text" class="boxfecha" name="tNumero15" id="tNumero15" onKeydown="solonumeros(this);" onkeyup='solonumeros(this);'>
			  <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12">
			  </td>
			</tr>
			<tr>
			  <td align="left">Folios</td>
			  <td align="left">
			  <input type="text" class="boxfecha" name="tFolios15" id="tFolios15" maxlength="2" onKeydown="solonumeros(this);" onkeyup='solonumeros(this);'><img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12">
			  </td>
			  </tr>
			 <tr><td>Notas</td>
			 <td ><textarea name="notas15" id="notas15" class="boxlargo"></textarea></td>
	 	</tr>
	</table>
</div>
<!-- fin 15  -->

<!-- BLOQUEO TARJETAS --> 
<div  id="div16">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tablero">
		<tr><th colspan="4">Bloqueo de Tarjeta</th></tr>
		<tr><th colspan="4" align="left"><img src="../../imagenes/menu/informacion.png" width="16" height="16" style="border:none; cursor:pointer" title="Manual" onclick="mostrarAyudaBloqueoTarjeta()" /></th></tr>
		<tr>
			<td>Buscar Por</td>
			<td colspan="3">
				<select name="buscarPor" id="buscarPor" class="box1" onchange="mostrarTarjeta();">
					<option value="0" selected="selected">Seleccione</option>
					<option value="1">Identificación</option>
					<option value="2">Bono</option>
				</select>
			</td>
		</tr>
		<tr>
			<td colspan="4">
				<div id="tNumero" style="display:none">
					<table width="100%" border="0" cellspacing="0">
						<tr>
						    <td>Tipo Documento</td>
						    <td>
							    <select name="tipoI" id="tipoI" class="box1" >
							    <?php
								$consulta=$objClase->mostrar_datos(1, 2);
								while($row=mssql_fetch_array($consulta)){
									echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
								}
								?>
							    </select>
						    </td>
						    <td>N&uacute;mero</td>
						    <td><input name="numero4" type="text" class="box1" id="numero4" onblur="buscarPersonaTarjeta();" onkeypress="tabular(this,event);" /></td>
						</tr>
					</table>
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="4">
				<div id="tBono" style="display:none">
					<table width="90%" border="0" cellspacing="0" class="tablero">
						<tr>
							<td width="25%">Bono N&uacute;mero</td>
							<td width="25%"><input name="txtBono" type="text" class="box1" id="txtBono" onblur="buscarPersona2Tarjeta();" onkeypress="tabular(this,event);" onkeyup="solotexto(this);" /></td>
							<td width="25%">&nbsp;</td>
							<td width="25%">&nbsp;</td>
						</tr>
					</table>
				</div>
			</td>
		</tr>
		<tr>
			<td>Nombres</td>
			<td colspan="3" id="lnombre" width="75%">&nbsp;</td>
		</tr>
		<tr>
			<td>Bono</td>
			<td id="lbono" width="25%">&nbsp;</td>
			<td>Estado</td>
			<td id="lestado" width="25%">&nbsp;</td>
		</tr>
		<tr>
			<td>Fecha Solicitud</td>
			<td id="lfechasol"></td>
			<td>Saldo</td>
			<td id="lsaldo"></td>
		</tr>
		<tr>
			<td>Motivo</td>
			<td colspan="3" width="75%">
				<select name="codBloqueo" class="boxlargo" id="codBloqueo">
					<option value="0" selected="selected">Seleccione</option>
					<?php
					$consulta=$objClase->mostrar_datos(48, 2);
						while($row=mssql_fetch_array($consulta)){
						echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
					}
					?>
				</select>
			</td>
		</tr>
		<tr>
			<td>Notas</td>
			<td colspan="3" width="75%"><textarea class="boxlargo" name="notas16" id="notas16" ></textarea></td>
		</tr>
	</table>
	<!-- CAPA DOCUMENTOS BLOQUEO DE TARJETA -->
    <div id="docsBloqueoTarjeta" align="left">
		<div>
			<h4>LISTADO DOCUMENTOS A SOLICITAR</h4>
			<br>
			<strong>Motivo de bloqueo:</strong>
			<br>
			<span style="background:url(../../imagenes/FondoTabla1.png) repeat-x; border:1px dotted #666; display:block;width:300px"  class="ui-corner-all">
				<strong>P&eacute;rdida/Robo</strong>
				<input type="radio" name="motbloqueo" id="motb_perdida" value="p"><br>
				<strong>Deterioro</strong>
				<input type="radio" name="motbloqueo" id="motb_deteriodo" value="d"><br>
			</span>
		</div>
		<p><input type="checkbox" name="chkdenuncia" id="chkdenuncia" > <label for="chkdenuncia">Denuncia.</label></p>
		<p><input type="checkbox" name="chktarjetadet" id="chktarjetadet" > <label for="chktarjetadet">Tarjeta deteriorada.</label></p>
	</div>
    <!-- FIN CAPA DOCUMENTOS SOLICITADOS CREAR CONVIVENCIA -->
	<div id="ayudaBloqueoTarjeta" title="Manual .:. Bloqueo Tarjeta" style="background-image:url(../../imagenes/FondoGeneral0.png)"></div>
</div>
<!-- FIN RENOVACION TARJETAS DIV 16 -->

<!-- DIV 17 CREAR CONVIVENCIA -->
<div  id="div17">
<table width="100%" border="0" cellspacing="0" class="tablero" id="cony">
  <tr><th colspan="4" >Informaci&oacute;n Adicional Crear Convivencia</th></tr>
   <tr>
	<td width="25%">Tipo Documento Afiliado</td>
	<td width="25%"><select class="box1" name="sltTipoIdentificacion17" id="sltTipoIdentificacion17" onkeypress="tabular (this, event);" onchange="document.getElementById('tNumero17').value=''">
</select>
  <img src="../../imagenes/menu/obligado.png" width="12" height="12"></td>
<td width="25%">N&uacute;mero</td>
<td width="25%"><input type="text" class="box1" name="tNumero17" id="tNumero17" onBlur="validarLongNumIdent(document.getElementById('sltTipoIdentificacion17').value,this);buscarPersona2(this,7,$('#sltTipoIdentificacion17').val());"  onkeypress="tabular (this, event);" onkeyup="validarCaracteresPermitidos(document.getElementById('sltTipoIdentificacion17').value,this);">
  <img src="../../imagenes/menu/obligado.png" width="12" height="12"></td>
 </tr> 
  <tr>
  <td>Afiliado</td>
  <td><input type="text" class="boxlargo" name="tNombre17" id="tNombre17" readonly></td>
  <td> Empleador</td>
  <td><input type="text" class="box1" name="tNit17" id="tNit17">
    <input type="text" class="boxlargo" name="tRazon17" id="tRazon17" readonly></td>
 </tr> 
  <tr>
      <td>Fecha afiliaci&oacute;n</td>
      <td><input type="text" name="fafiliacion" id="fafiliacion" readonly class="boxfecha"></td>
      <td>Estado afiliaci&oacute;n</td>
      <td><input type="text" name="afiEstado" id="afiEstado" class="box1" readonly></td>
  </tr>
  <tr>
  <td>Tipo afiliaci&oacute;n</td>
  <td><input type="text" name="tipoAfi17" id="tipoAfi17" class="box1" readonly></td>
  <td>Tipo Documento Conyuge</td>
  <td><select class="box1" name="sltTipoIdentificacion17A" id="sltTipoIdentificacion17A" onkeypress="tabular (this, event);">
</select>&nbsp;</td>
</tr>
   <tr>
    <td>N&uacute;mero</td>
    <td><input type="text" class="box1" name="tNumeroC" id="tNumeroC" onBlur="buscarPersona2(this,8,$('#sltTipoIdentificacion17').val());"  onkeypress="tabular (this, event);" onKeydown="solotexto(this);" onkeyup='solotexto(this);'></td>
    <td>Conyuge</td>
    <td><input type="text" class="boxlargo" name="tNombreC" id="tNombreC" readonly></td>
   </tr>
   <tr>
    <td>Tipo de relaci&oacute;n</td>
    <td>
    <select name="tipRel" class="box1" id="tipRel">
	<option selected="selected" value="0">Seleccione..</option>
	<option value="2147">CONYUGE</option>
	<option value="2148">COMPA&Ntilde;ERO(A)</option>
    </select></td>
    <td>N&oacute;mero de folios</td>
    <td><input type="text" class="box1" name="tFolios17" id="tFolios17" maxlength="2" onKeydown="solonumeros(this);" onkeyup='solonumeros(this);'></td>
  </tr>
  <tr>
    <td>Notas</td>
    <td colspan="3"><textarea name="notas17" id="notas17" class="boxlargo"></textarea><!--  id="textarea"--></td>
  </tr>  
    </table>
    
    <!-- CAPA DOCUMENTOS SOLICITADOS CREAR CONVIVENCIA -->
    <br/>
    <div id="docsCrearConvivencia">
    	<h4>Documentos Requeridos</h4>
    	<div style="background:url(../../imagenes/FondoTabla1.png) repeat-x; border:1px dotted #666; display:block;width:300px"  class="ui-corner-all">
    		<p>
	  			<label for="cFotocopia">Fotocopia C&eacute;dula C&oacute;nyuge</label> <input type="checkbox" name="cFotocopia" id="cFotocopia">
	  		</p>
	    	<p>
	    		<label for="cMatrimonio">Registro Civil Matrimonio</label> <input type="checkbox" name="cMatrimonio" id="cMatrimonio"> 
	    	</p>
			<p>
				<label for="cFormatoConvivencia">Formato de convivencia</label> <input type="checkbox" name="cFormatoConvivencia" id="cFormatoConvivencia">
			</p>
		</div>
    </div>
    <!-- FIN CAPA DOCUMENTOS SOLICITADOS CREAR CONVIVENCIA -->
</div>
<!-- FIN  CERAR CONVIVENCIA DIV 17 -->

<!-- DIV 18 EMBARGOS-->
<div  id="div18">
<table width="100%" border="0" cellspacing="0" class="tablero" id="cony">
  <tr>
    <th colspan="4" >Embargos</th></tr>
   <tr>
	<td width="25%">Tipo Documento Afiliado</td>
	<td width="25%"><select class="box1" name="sltTipoIdentificacion18" id="sltTipoIdentificacion18" onkeypress="tabular (this, event);" onchange="document.getElementById('tNumero18').value=''">
</select>
  <img src="../../imagenes/menu/obligado.png" width="12" height="12"></td>
<td width="25%">N&uacute;mero</td>
<td width="25%"><input type="text" class="box1" name="tNumero18" id="tNumero18" onBlur="validarLongNumIdent(document.getElementById('sltTipoIdentificacion18').value,this); buscarPersona2(this,12,$('#sltTipoIdentificacion18').val());"  onkeypress="tabular (this, event);" onkeyup="validarCaracteresPermitidos(document.getElementById('sltTipoIdentificacion18').value,this);">
  <img src="../../imagenes/menu/obligado.png" width="12" height="12"></td>
 </tr> 
  <tr>
  <td>Afiliado</td>
  <td><input type="text" class="boxlargo" name="tNombre18" id="tNombre18" readonly></td>
  <td>N&oacute;mero de folios</td>
  <td><input type="text" class="box1" name="tFolios18" id="tFolios18" maxlength="2" onKeydown="solonumeros(this);" onkeyup='solonumeros(this);'> 
   <img src="../../imagenes/menu/obligado.png" width="12" height="12"></td>
 </tr> 
  <tr>
      <td>Notas</td>
      <td><textarea name="notas18" id="notas18" class="boxlargo"></textarea></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
  </tr>  
    </table>
</div>
<!-- FIN  CERAR CONVIVENCIA DIV 18 -->

<!--div 19 independientes -->
<div id="div19" >
<table width="100%" border="0" cellspacing="0" class="tablero">
<tr>
	<th colspan="4" align="left"><center>Informaci&oacute;n Adicional Afiliaci&oacute;n Independientes
	</center></th>
</tr>
<tr>
	<td align="left" width="17%">Tipo Documento Afiliado</td>
	<td>
		<select class="box1" name="sltTipoIdentificacion19" id="sltTipoIdentificacion19" onkeypress="tabular (this, event);" onchange="document.getElementById('tNumero19').value=''">
		<option value="0" selected="selected">Seleccione...</option>
            <?php
			$consulta = $objClase->mostrar_datos( 1,1 );
            while( $row = mssql_fetch_array( $consulta ) ){
            	if($row['iddetalledef']!=5)//si es diferente de NIT
				echo "<option value=". $row['iddetalledef'].">".$row['detalledefinicion']."</option>";
			}
        ?>
		</select>
		<img src="../../imagenes/menu/obligado.png" width="12" height="12">
	</td>
	<td>N&uacute;mero</td>
	<td><input type="text" class="box1" name="tNumero19" id="tNumero19" onBlur="validarLongNumIdent(document.getElementById('sltTipoIdentificacion19').value,this);new_buscarPersona();afilActPendNit(0);" onkeyup="validarCaracteresPermitidos(document.getElementById('sltTipoIdentificacion19').value,this);">
		<img src="../../imagenes/menu/obligado.png" width="12" height="12">
	</td>
	<td><input id="idEmpresaIndependiente" type="hidden"></td>
</tr>
<tr>
    <td>Nombre Completo</td>
    <td colspan="3"><input name="nomCompleto" type="text" class="boxlargo" id="nomCompleto" readonly>
    </td>
</tr>
<tr>
    <td>NIT Empleador</td>
    <td colspan="3"><input type="text" class="box1" name="tNit19" id="tNit19" readonly ><img src="../../imagenes/menu/obligado.png" alt="Obligatorio" width="12" height="12">
    	<input type="text" class="boxlargo" name="tRazon19" id="tRazon19" readonly>
    </td>
</tr>
<tr>
    <td>Fecha Ingreso</td>
    <td><input name="fecIngreso19" type="text" class="boxfecha" id="fecIngreso19" readonly></td>
    <td>Salario</td>
    <td><input name="salario19" type="text" class="boxfecha" id="salario19" onBlur="calcularCategoria(this.value, document.getElementById('categoria19').id)" onkeydown='solonumeros(this);' onkeyup='solonumeros(this);'>
	    <img src="../../imagenes/menu/obligado.png" alt="Obligatorio" width="12" height="12">
	    <input id="categoria19" type="hidden">
    </td>
</tr>
<tr>
	<td align="left" width="17%">Tipo de afiliaci&oacute;n</td>
	<td>
	<select name="tAfiliacion19" id="tAfiliacion19" class="box1" onkeydown='solotexto(this);' onkeyup='solotexto(this);'>
		<option value="0" selected="selected">Seleccione</option>
		<?php
	    $consulta1 = $objClase->mostrar_datos(2,3);
	    while( $row = mssql_fetch_array( $consulta1 ) ){
	    	if(($row['iddetalledef'])!="18")
	    	{
			  echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
	    	}
		}
		?>
	</select><img src="../../imagenes/menu/obligado.png" width="12" height="12">&nbsp;
	</td>
	<td>Tipo Formulario</td>
	<td>
		<select name="tipForm19" class="box1" id="tipForm19" >
		  <option value="49">Servicios</option>
		</select>
		<img src="../../imagenes/menu/obligado.png" alt="Obligatorio" width="12" height="12">
	</td>
</tr>
<tr id="condicionAfiliacion" style="display:none">
			<td align="left" width="17%">Exento</td>
			<td>
			<select name="exento12" class="box1" id="exento12">
			  <option value="1">Si</option>
			  <option value="0">No</option>
			</select></td>
			<td width="18%">Aportes</td>
			<td>
			<select name="porcentaje12" class="box1" id="porcentaje12">
			  <option value="0" selected="selected">Seleccione...</option>
			  <?php
				$consulta=$objClase->mostrar_datos_varios(18, 4, "01,02");
				while($row=mssql_fetch_array($consulta)){
					echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
				}
			  ?>
			</select></td>
</tr>
<tr>
	<td align="left" width="17%">N&uacute;mero de folios</td>
	<td><input type="text" class="box1" name="tFolios19" id="tFolios19" maxlength="2" onkeydown='solonumeros(this);' onkeyup='solonumeros(this);'><img src="../../imagenes/menu/obligado.png" width="12" height="12"></td>
	<td><input id="variasAfiliacionesActivas" type="hidden"></td>
	<td>&nbsp;</td>
</tr>
<tr>
	<td align="left">Notas</td>
	<td colspan="3"><textarea name="notas19" id="notas19" class="boxlargo"></textarea>
	</td>
</tr>
</table>

	<!-- CAPA DE DOCUMENTOS PARA TRABAJADOR INDEPENDIENTE -->
	<div id="docsInd" align="left">
		<!-- DOCUMENTOS AFILIADO -->
		<div>
		      <h4>LISTADO DOCUMENTOS A SOLICITAR</h4>
		      <p>
			      <input type="checkbox" name="docTrabajadorInd" id="formularioInd">
			      <label for="formularioInd" >Formulario afiliaci&oacute;n.</label>
			      <img src="../../imagenes/menu/obligado.png" width="12" height="12">
		      </p>
		      <p>
			      <input type="checkbox" name="docTrabajadorInd"  id="docCedulaInd">
			      <label for="docCedulaInd" >Fotocopia de la c&eacute;dula</label>
			      <img src="../../imagenes/menu/obligado.png" width="12" height="12">
		      </p>
		      <p id="p1" style="display: none">
			      <input type="checkbox" name="certSegSocialInd"  id="certSegSocialInd">
			      <label for="certSegSocialInd" >Certificado o desprendible de pensi&oacute;n, ó resoluci&oacute;n.</label>
			      <img src="../../imagenes/menu/obligado.png" width="12" height="12">
		      </p>
		      <p id="p2" style="display: none">
			      <input type="checkbox" name="docTrabajadorInd"  id="docCedulaInd">
			      <label for="docCedulaInd" >Recibo de pago de salud.</label>
			      <img src="../../imagenes/menu/obligado.png" width="12" height="12">
		      </p>
		      <p id="p3_ind" style="display:none">
		      	<input type="checkbox" id="docConstanciaSegSocial" name="docConstanciaSegSocial" />
		      	<label for="docConstanciaSegSocial">Constancia de pago de aportes a EPS y AFP.</label> <img width="12" height="12" src="../../imagenes/menu/obligado.png">
		      </p>
		      <span style="background:url(../../imagenes/FondoTabla1.png) repeat-x; border:1px dotted #666; display:block;width:300px"  class="ui-corner-all">
			      <table border="0" width="100%">
				      <tr><td>&nbsp;</td><td align="center"><b>SI</b></td><td align="center"><a><strong>NO</strong></a></td></tr>
				      <tr>
				        <td><strong>&iquest;Convivencia?</strong></td>
				        <td align="center"><input type="radio" name="rconvivenInd" id="si2Ind" value="s"></td>
				        <td align="center"><input name="rconvivenInd" type="radio" id="no2Ind" value="n" checked="checked"></td>
				      </tr>
				      <tr>
				        <td><strong>&iquest;Hijos-Hermanos?</strong></td>
				        <td align="center"><input type="radio" name="rhijosInd" id="si3Ind" value="s"></td>
				        <td align="center"><input name="rhijosInd" type="radio" id="no3Ind" value="n" checked="checked"></td>
				      </tr>
				      <tr>
				        <td><strong>&iquest;Padres?</strong>&nbsp;
				          <select id="tipoServicioInd" style="display:none">
				            <option value="0">Seleccione..</option>
				            <option  value="1">SERVICIOS</option>
				            </select></td>
				        <td align="center"><input type="radio" name="rpadresInd" id="siInd" value="s"></td>
				        <td align="center"><input name="rpadresInd" type="radio" id="noInd" value="n" checked="checked"></td>
				        
				      </tr>
			      </table>
		      </span>
		</div>
		<!-- DOCUMENTOS CONYUGE -->
		<div>
			<hr/>
			<h4>Documentos Conyuge</h4>
			<p>
				<input type="checkbox" name="docConyugeInd" id="check1Ind">
				Registro civil MATRIMONIO<sup>1</sup>.
			</p>
			<p>
				<input type="checkbox" name="docConyugeInd" id="check2Ind">
				Fotocopia AMPLIADA y LEGIBLE de la c&eacute;dula del c&oacute;nyuge.
			</p>
			<p>
				<input type="checkbox" name="docConyugeInd" id="check3Ind">
				Formato de Convicencia.
			</p>
		</div>
		<!-- DOCUMENTOS HIJOS -->
		<div>
			<hr>
			<h4>Documentos  Hijos-Hijastros-Hermanos</h4>
			<p>
			<input type="checkbox" name="docHijosInd" id="check1HijosInd">
			Fotocopia Registro civil.
			<img src="../../imagenes/menu/obligado.png" width="12" height="12">
			</p>
			<p>
			<input type="checkbox" name="docHijosInd" id="check4Ind">
			Certificado de escolaridad<sup>2</sup>.
			</p>
			<p>
			<input type="checkbox" name="docHijosInd" id="check5Ind">
			Sentencia judicial de custodia.
			</p>
			<p>
			<input type="checkbox" name="docHijosInd" id="check6Ind">
			Sentencia de adopci&oacute;n.
			</p>
			<p>
			<input type="checkbox" name="docHijosInd" id="check7Ind">
			Certificado de discapacidad.
			</p>
		</div>
		<!-- DOCUMENTOS PADRES -->
		<div>
			<hr>
			<h4>Documentos  Padres</h4>
			<p>
			  <input type="checkbox" name="docPadresInd" id="check8Ind">
			  Fotocopia de la c&eacute;dula AMPLIADA.
			  <img src="../../imagenes/menu/obligado.png" width="12" height="12">
			</p>
			<p>
			  <input type="checkbox" name="docPadresInd" id="check9Ind">
			  Registro civil trabajador.
			  <img src="../../imagenes/menu/obligado.png" width="12" height="12">
			</p>
			<p>
			  <input type="checkbox" name="docPadresInd" id="check10Ind">
			  Certificado de supervivencia<sup>3</sup>.
			</p>
			<p>
			  <input type="checkbox" name="docPadresInd" id="check11Ind">
			  Declaraci&oacute;n extrajuicio.
			</p>
		</div>
	</div>
	<!-- FIN CAPA DE DOCUMENTOS PARA TRABAJADOR INDEPENDIENTE -->

	<br/>
	<ul  id="pieInd" type="1" style="border-top:1px dashed #999; text-align:left">
		<li>(1)Escritura P&uacute;blica &oacute; sentencia judicial para parejas del MISMO sexo. </li>
		<li>(2)Hijos entre 12 y 18 a&oacute;os de edad, y aprobado por el plantel.</li>
		<li>(3)Adultos  Mayores de 60 a&ntilde;os.</li>
		<li><img src="../../imagenes/menu/obligado.png" width="12" height="12">Documentos m&iacute;nimos requeridos.</li>
	</ul>
</div>
<!-- fin div 19 independientes  -->
 
 <!-- DIV 20 FORMULARIO NOVEDADES TARJETA -->
 <div  id="div20">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tablero">
		<tr><th colspan="4">Novedad de Tarjeta</th></tr>
		<tr><th colspan="4" align="left"><img src="../../imagenes/menu/informacion.png" width="16" height="16" style="border:none; cursor:pointer" title="Manual" onclick="mostrarAyudaNovedadTarjeta()" /></th></tr>
		<tr>
			<td>Buscar Por</td>
			<td colspan="3">
				<select name="buscarPorNovedad" id="buscarPorNovedad" class="box1" onchange="mostrarTarjetaNovedad();">
					<option value="0" selected="selected">Seleccione</option>
					<option value="1">Identificación</option>
					<option value="2">Bono</option>
				</select>
			</td>
		</tr>
		<tr>
			<td colspan="4">
				<div id="tNumeroNovedad" style="display:none">
					<table width="100%" border="0" cellspacing="0">
						<tr>
						    <td>Tipo Documento</td>
						    <td>
							    <select name="tipoINovedad" id="tipoINovedad" class="box1" onchange="document.getElementById('numero5').value=''">
							    <?php
								$consulta=$objClase->mostrar_datos(1, 2);
								while($row=mssql_fetch_array($consulta)){
									echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
								}
								?>
							    </select>
						    </td>
						    <td>N&uacute;mero</td>
						    <td><input name="numero5" type="text" class="box1" id="numero5" onblur="validarLongNumIdent(document.getElementById('tipoINovedad').value,this);buscarPersonaTarjetaNovedad();" onkeypress="tabular(this,event);" onkeyup="validarCaracteresPermitidos(document.getElementById('tipoINovedad').value,this);"/></td>
						</tr>
					</table>
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="4">
				<div id="tBonoNovedad" style="display:none">
					<table width="90%" border="0" cellspacing="0" class="tablero">
						<tr>
							<td width="25%">Bono N&uacute;mero</td>
							<td width="25%"><input name="txtBonoNovedad" type="text" class="box1" id="txtBonoNovedad" onblur="buscarPersona2TarjetaNovedad();" onkeypress="tabular(this,event);" onkeyup="solonumeros(this);" /></td>
							<td width="25%">&nbsp;</td>
							<td width="25%">&nbsp;</td>
						</tr>
					</table>
				</div>
			</td>
		</tr>
		<tr>
			<td>Nombres</td>
			<td colspan="3" id="lnombrenovedad" width="75%">&nbsp;</td>
		</tr>
		<tr>
			<td>Bono</td>
			<td id="lbononovedad" width="25%">&nbsp;</td>
			<td>Estado</td>
			<td id="lestadonovedad" width="25%">&nbsp;</td>
		</tr>
		<tr>
			<td>Fecha Solicitud</td>
			<td id="lfechasolnovedad"></td>
			<td>Saldo</td>
			<td id="lsaldonovedad"></td>
		</tr>
		<tr>
			<td>Ubicaci&oacute;n</td><td colspan="3" id="lubicacionnovedad"></td>
		</tr>
		<tr>
			<td colspan="4"><h3>Qu&eacute; tipo de novedad desea registrar?</h3></td>
		</tr>
		<tr>
			<td colspan="4">
				<?php
					$consulta=$objClase->mostrar_datos(48, 2);
					$motivos = array();
					$c=0;
					while($row=mssql_fetch_array($consulta)){
						$motivos[$c] = array("id" => $row['iddetalledef'], "detalle" => $row['detalledefinicion']);
						$c++;
					}
				?>
				<p>
				<input type="radio" name="tipoNovedad" id="tipoNovedad_b" style="float:left;" /> <label for="tipoNovedad_b" style="width:120px; float:left; text-align:left;">Bloqueo.</label>
				<select name="novedad" id="novedad_b" >
					<option value="">Seleccione</option>
					<?php foreach($motivos as $posicion => $motivo):
							if(!in_array($motivo["id"],array(2868,2945,2947,2949))):?>
							<option value="<?php echo $motivo["id"]; ?>"><?php echo $motivo["detalle"]; ?></option>
					<?php   endif; endforeach; ?>
				</select>
				</p>
				<p>
				<input type="radio" name="tipoNovedad" id="tipoNovedad_s" style="float:left;" /> <label for="tipoNovedad_s" style="width:120px; float:left; text-align:left;">Suspenci&oacute;n.</label>
				<select name="novedad" id="novedad_s" >
					<option value="">Seleccione</option>
					<?php foreach($motivos as $posicion => $motivo):
							if(in_array($motivo["id"],array(2868,2949))):?>
							<option value="<?php echo $motivo["id"]; ?>"><?php echo $motivo["detalle"]; ?></option>
					<?php   endif; endforeach; ?>
				</select>
				</p>
				<p>
				<input type="radio" name="tipoNovedad" id="tipoNovedad_h" style="float:left;" /> <label for="tipoNovedad_h" style="width:120px; float:left; text-align:left;">Habilitaci&oacute;n.</label>
				<select name="novedad" id="novedad_h" >
					<option value="">Seleccione</option>
					<?php foreach($motivos as $posicion => $motivo):
							if(in_array($motivo["id"],array(2947))):?>
							<option value="<?php echo $motivo["id"]; ?>"><?php echo $motivo["detalle"]; ?></option>
					<?php   endif; endforeach; ?>
				</select>
				</p>
			</td>
		</tr>
		<tr id="tr_verifica_afiliado">
			<td colspan="4">
				<h2 style="display: inline; float:letf;">Validaci&oacute;n de datos de afiliado</h2> <img src="../../imagenes/menu/obligado.png" width="20" height="20" align="left" >
				<p style="font-size:10pt">Las siguientes preguntas se hacen para verificar que el cliente efectivamente es el propietario de la tarjeta. Seleccione las respuestas de acuerdo a lo contestado por el cliente:</p>
				<ol style="list-style-type: decimal;" id="ol_preguntas_seguridad">
					<li id="li_pregunta_direccion" >Cu&aacute;l es su direcci&oacute;n actual?
						<ol style="list-style-type: lower-alpha" id="opciones_pregunta_direccion" class="opciones_respesta">
						</ol>
					</li>
					<li id="li_pregunta_telefono" >Cu&aacute;l es su tel&eacute;fono actual?
						<ol style="list-style-type: lower-alpha" id="opciones_pregunta_telefono" class="opciones_respesta">
						</ol>
					</li>
					<li id="li_pregunta_tienesaldo" >Actualmente su tarjeta tiene saldo?
						<ol style="list-style-type: lower-alpha" id="opciones_pregunta_tienesaldo" class="opciones_respesta">
						</ol>
					</li>
					<li id="li_pregunta_saldotarjeta" >Cu&aacute;l es el saldo actual de su tarjeta?
						<ol style="list-style-type: lower-alpha" id="opciones_pregunta_saldotarjeta" class="opciones_respesta">
						</ol>
					</li>
					<li id="li_pregunta_empresa" >Con cu&aacute;l de las siguientes empresas tiene actualmente una afiliaci&oacute;n?
						<ol style="list-style-type: lower-alpha" id="opciones_pregunta_empresa" class="opciones_respesta">
						</ol>
					</li>
				</ol>
				<span id="span_mensajevalidrespuestas"></span>
				<input type="button" value="Validar" id="btnValidRespuestas" />
			</td>
		</tr>
		<tr>
			<td>Notas</td>
			<td colspan="3" width="75%"><textarea class="boxlargo" name="notas20" id="notas20" ></textarea></td>
		</tr>
	</table>
	<!-- CAPA DOCUMENTOS NOVEDAD DE TARJETA -->
    <div id="docsNovedadTarjeta" align="left">
		<div>
			<h4>LISTADO DOCUMENTOS A SOLICITAR</h4>
			<br>
			<strong>Motivo de bloqueo:</strong>
			<br>
			<span style="background:url(../../imagenes/FondoTabla1.png) repeat-x; border:1px dotted #666; display:block;width:300px"  class="ui-corner-all">
				<strong>P&eacute;rdida/Robo</strong>
				<input type="radio" name="motnovedad" id="motb_novedad_perdida" value="p"><br>
				<strong>Deterioro</strong>
				<input type="radio" name="motnovedad" id="motb_novedad_deteriodo" value="d"><br>
			</span>
		</div>
		<p><input type="checkbox" name="chkdenuncianovedad" id="chkdenuncianovedad" > <label for="chkdenuncianovedad">Denuncia.</label></p>
		<p><input type="checkbox" name="chktarjetadetnovedad" id="chktarjetadetnovedad" > <label for="chktarjetadetnovedad">Tarjeta deteriorada.</label></p>
	</div>
    <!-- FIN CAPA DOCUMENTOS SOLICITADOS NOVEDAD DE TARJETA -->
	<div id="ayudaNovedadTarjeta" title="Manual .:. Novedades Tarjeta" style="background-image:url(../../imagenes/FondoGeneral0.png)"></div>
</div>
<!-- FIN DIV FORMULARIO NOVEDADES TARJETA -->
 
<table width="100%" border="0" cellspacing="0">
<tr>
<td align="left"><span class="Rojo">Notas: SIEMPRE HAGA CLICK SOBRE EL BOTON NUEVO PARA ACTUALIZAR LA HORA DE INICIO.</span></td>
</tr>
<tr>
  <td align="left" class="Rojo"><img src="../../imagenes/menu/obligado.png" width="16" height="12" align="left">Campo Obligado</td>
</tr>
    </table>    
    
    </td>
    <td class="cuerpo_de"></td>
  </tr>
  <tr>
    <td class="abajo_iz" >&nbsp;</td>
    <td class="abajo_ce" ></td>
    <td class="abajo_de" >&nbsp;</td>
  </tr>
</table>

<!-- div general  -->
</div>
</div>
  
  <input type="hidden" name="tExisteTrab" id="tExisteTrab">
  <input type="hidden" name="idTarjeta" id="idTarjeta">
  <input type="hidden" name="idPersona" id="idPersona">
  <input type="hidden" name="idEmpresa" id="idEmpresa">
  <input type="hidden" name="idConyuge" id="idConyuge">
  <input type="hidden" name="txtTempo" id="txtTempo">

<!-- VENTANA CONYUGE DIALOG style="display:none;" -->
<div id="div-conyuge2" title="::FORMULARIO NUEVO CONYUGE::" style="display:none"  >
<h4 align="left"> DATOS DEL CONYUGE </h4>
<table width="100%"  border="0" cellspacing="0" class="tablero">
<tr>
<td width="25%">Tipo Documento</td>
<td width="25%">
<select name="tipoDoc2" class="box1" id="tipoDoc2">
<option value="0">Seleccione..</option>
<?php
	
	$consulta = $objClase->mostrar_datos(1,2);
    while( $row = mssql_fetch_array( $consulta ) ){
		echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
	}
?>
</select><img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12"></td>
<td width="25%">Identificaci&oacute;n</td>
<td width="25%"><input name="identificacion2" type="text" class="box1" id="identificacion2" maxlength="10" onBlur="buscarPersona2(this,10,document.getElementById('tipoDoc2').value);" onkeyup="solotexto(this);" onkeydown="solotexto(this);" onkeypress="tabular(this,event);">
<img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12"></td>
</tr>         
<tr>
<td>Primer Nombre</td>
<td class="box1">
<input name="pNombre" type="text" class="box1" id="pNombre" onkeypress="return validarEspacio(event);" onkeydown='sololetras(this);' onkeyup='sololetras(this);'>
<img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12"></td>
<td>Segundo Nombre</td>
<td><input name="sNombre" type="text" class="box1" id="sNombre" onkeydown='sololetras(this);' onkeyup='sololetras(this);'></td>
</tr>
<tr>
<td>Primer Apellido</td>
<td><input name="pApellido" type="text" class="box1" id="pApellido" onkeypress="return validarEspacio(event);" onkeydown='sololetras(this);' onkeyup='sololetras(this);'>
<img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12"></td>
<td>Segundo Apellido</td>
<td><input name="sApellido" type="text" class="box1" id="sApellido" onkeydown='sololetras(this);' onkeyup='sololetras(this);'></td>
</tr>
<tr>
<td>Sexo</td>
<td colspan="3"><select name="sexo2" class="box1" id="sexo2">
<option selected="selected" value="0">Seleccione..</option>
<option value="M">Masculino</option>
<option value="F">Femenino</option>
</select><img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12"></td>
</tr>
<tr>
<td>Tel&eacute;fono</td>
<td><input name="telefono2" type="text" class="box1" id="telefono2" maxlength="7" onkeydown='solonumeros(this);' onkeyup='solonumeros(this);'>
</td>
<td>Celular</td>
<td><input name="celular2" type="text" class="box1" id="celular2" maxlength="10" onkeydown='solonumeros(this);' onkeyup='solonumeros(this);'></td>
</tr>
<tr>
<td>E-mail</td>
<td colspan="3"><input name="email2" type="text" class="boxlargo" id="email2" onblur="soloemail(this)"></td>
</tr>
<tr>
<td>Tipo Vivienda</td>
<td><select name="tipoVivienda2" class="box1" id="tipoVivienda2">
<option value="0">Seleccione..</option>
<option value="U">Urbana</option>
<option value="R">Rural</option>
</select></td>
<td>Departamento Residencia</td>
<td>
<select name="cboDepto" class="box1" id="cboDepto">
<option value="0">Seleccione..</option>
<?php
	
	$consulta = $objCiudad->departamentos();
    while( $row = mssql_fetch_array( $consulta ) ){
	?>          
	<option value="<?php echo $row["coddepartamento"]; ?>"><?php echo $row["departmento"]?></option>
	<?php
	}
?>              
</select></td>
</tr>
<tr>
<td>Ciudad Residencia</td>
<td>
<select name="cboCiudad" class="box1" id="cboCiudad">
</select></td>
<td>Zona</td>
<td><select name="cboZona" class="box1" id="cboZona">
</select></td>
</tr>
<tr>
<td>Direcci&oacute;n</td>
<td><input name="direccion2" type="text" class="box1" id="direccion2"  onfocus="direccion(this,document.getElementById('cboBarrio'));">
</td>
<td>Barrio</td>
<td><select name="cboBarrio" class="box1" id="cboBarrio">
</select></td>
</tr>
<tr>
<td>Estado Civil</td>
<td><select name="estadoCivil2" class="box1" id="estadoCivil2">
<option value="0">Seleccione..</option>
<?php

$consulta = $objClase->mostrar_datos(10,3);
while( $row = mssql_fetch_array( $consulta ) ){ 
	if($row['iddetalledef']!=54 && $row['iddetalledef']!=53){
		echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
	}
}
?>
</select></td>
<td>Fecha De Nacimiento</td>
<td><input name="fecNac2" type="text" class="box1" id="fecNac2" value="mmddaaaa" maxlength="8">
<img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12">
<input type="hidden" name="fecNacHide2" id="fecNacHide2"></td>
</tr>
<tr>
<td>Departamento Nacimiento</td>
<td><select name="cboDepto2" class="box1" id="cboDepto2">
<option value="0">Seleccione..</option>
<?php

$consulta = $objCiudad->departamentos();
 while( $row = mssql_fetch_array( $consulta ) ){
?>          
	<option value="<?php echo $row["coddepartamento"]; ?>"><?php echo $row["departmento"]?></option>
<?php
	}
?>
</select></td>
<td>Ciudad Nacimiento</td>
<td><select name="cboCiudad2" class="box1" id="cboCiudad2">
</select></td>
</tr>
<tr>
<td>Capacidad Trabajo</td>

<td><select name="capTrabajo2" class="box1" id="capTrabajo2">
<option value="N">Normal</option>
<option value="I">Discapacidad</option>
</select></td>
<td>Profesi&oacute;n</td>
<td><select name="profesion2" class="box1" id="profesion2">
<option value="0">Seleccione..</option>
<?php

$consulta = $objClase->mostrar_datos(20,3);
 while( $row = mssql_fetch_array( $consulta ) ){
		echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
}
?>
<option>Otro..</option>
</select></td>
</tr>
<tr>
  <td>Nit</td>
  <td><input name="nitConyuge" type="text" class="box1" id="nitConyuge" readonly></td>
  <td>Empresa donde trabaja</td>
  <td><input name="empCony" type="text" class="box1" id="empCony" readonly></td>
</tr>
<tr>
<td>Salario </td>
<td><input name="salCony" type="text" class="box1" id="salCony" readonly></td>
<td>?Subsidio?</td>
<td><input name="subCony"  type="text" class="box1" id="subCony" readonly></td>
</tr>
<tr>
  <td>Nombre Corto</td>
  <td colspan="3"><input name="nombreCorto" type="text" class="boxlargo" id="nombreCorto" readonly></td>
  </tr>
</table>
</div>
<!-- FIN DIV CONYUGE -->

<!-- fin <div class="demo">  -->
<div style="display:none">
<input type="radio" name="vigencia" id="vigencia3">
</div>
</form>

<!-- div para la imagen ampliada -->
  <div id="imagen" title="DOCUMENTOS">

  </div>
  <!-- VENTANA DIALOG BARRIO -->
<div id="barrioDialog" title="NUEVO BARRIO" style="display:none; background-image:url('../../imagenes/FondoGeneral0.png')">
	<p><label>Digitar Barrio:</label><input id="nuevoBarrio" type="text" style="width:200px"></p>
   
</div>
<!-- VENTANA DIALOG PORFESION -->
<div id="profesionDialog" title="NUEVA PROFESI&oacute;N" style="display:none;">
	<p><label>Digitar Profesi&oacute;n:</label>
    <input id="nuevaProf" type="text" style="width:200px"></p>
   
</div>

<!-- formulario direcciones  --> 
<div id="dialog-form" title="Formulario de direcciones"></div>

<!-- colaboracion en linea -->
<div id="div-observaciones-tab"></div>
<div id="dialog-form2" title="Colaboraci&oacute;n en l&iacute;nea">
<p>Por favor diligencie este formulario para enviar comentarios, errores o falencias encontradas en el proceso. M&aacute;ximo 250 caracteres </p>
<label>Tus comentarios:</label><br />
<textarea name="notas" id="notas" cols="60" rows="10"></textarea>
</div>


<!-- fin colaboracion -->

<div id="ayuda" title="Manual Radicaci&oacute;n" style="background-image:url('../../imagenes/FondoGeneral0.png')">

</div>

<!-- VENTANA DIALOG RECHAZO DE RADICACION -->
<div id="devolucionDialog" title="::DEVOLUCI&Oacute;N DE DOCUMENTOS::">
<table border="0" cellpadding="5" align="center" class="tablero" width="100%">
<tr>
	<td>Causal de devoluci&oacute;n: </td>
	<td><select id="causalDev" class="boxmediano">
  		<?php
		
		$consulta = $objClase->mostrar_datos(54,1);
         while( $row = mssql_fetch_array( $consulta ) ){
		echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
		}
        ?>
		</select>
       </td>
 </tr>
<tr>
	<td>Nota</td>
    <td><textarea id="notasDevolucion" class="boxmediano"></textarea></td>
</tr>

</table>
</div>

<div id="dialog-persona" title="Formulario Datos Basicos" style="display:none">
<center>
<table width="578" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff" class="tablero">
	<tbody>
		<tr bgcolor="#EBEBEB">
		  <td colspan="4" style="text-align:center" >Datos B&aacute;sicos de la persona</td>
	  </tr>
		<tr>
		  <td >Tipo Documento</td>
		  <td ><select name="tDocumento" id="tDocumento" class="box1">
          <option value="1" selected>Cedula Ciudadania</option>
          </select>
          </td>
		  <td >N&uacute;mero</td>
		  <td ><input name="numero" id="numero" type="text" class="box1"></td>
	  </tr>
		<tr>
		  <td >Primer Nombre</td>
		  <td ><input name="pNombre" id="tpNombre" type="text" class="box1"></td>
		  <td >Segundo Nombre</td>
		  <td ><input name="sNombre" id="tsNombre" type="text" class="box1"></td>
	  </tr>
		<tr>
		  <td >Primer Apellido</td>
		  <td ><input name="pApellido" id="tpApellido" type="text" class="box1"></td>
		  <td >Segundo Apellido</td>
		  <td ><input name="sApellido" id="tsApellido" type="text" class="box1"></td>
	  </tr>
		<tr>
		  <td >Nombre Corto</td>
		  <td colspan="3" ><input name="tncorto" type="text" class="boxlargo" id="tncorto" readonly></td>
	    </tr>
		<tr>
			<td >&nbsp;</td>
			<td >&nbsp;</td>
			<td >&nbsp;</td>
			<td >&nbsp;</td>
		</tr>
</tbody>
</table>
</center>
</div>

</body>
<script language="JavaScript" type="text/javascript">
nuevoR();
nuevo=0;
//------DEVOLUCION DE DOCUMENTOS
function devolucionDocumentos(){
$("#devolucionDialog").dialog("open");
}

function notas(){
	$("#dialog-form2").dialog('open');
}	
	
function mostrarAyuda(){
	$("#ayuda").dialog('open');
	//$("#div-conyuge2").dialog('open');
	}
	
function validarEspacio(e){
	tecla=(document.all) ? e.keyCode : e.which;
	if(tecla==32){
		return false;
	}
}
</script>
</html>