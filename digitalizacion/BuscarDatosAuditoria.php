<?php
ini_set('display_errors','On');
//date_default_timezone_set("America/Bogota");
include_once '../rsc/pdo/IFXDbManejador.php';
include_once '../rsc/pdo/IFXerror.php';

$db = IFXDbManejador::conectarDB(); 
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();	
}// fin if

$campo0=$_REQUEST['v0'];
$campo1=$_REQUEST['v1'];

if($campo0=='5')
{	
	$sql="SELECT DISTINCT au48.nit,au48.fechaafiliacion as fecha FROM auditoria048 au48 WHERE au48.rutadocumentos!='' AND au48.fechaafiliacion!='' AND au48.nit='".$campo1."'
			AND au48.fechaafiliacion!=(select a48.fechaafiliacion from aportes048 a48 where a48.nit=au48.nit and a48.idempresa=au48.idempresa)";

}
else
{
	$sql="SELECT DISTINCT au15.identificacion,au15.fechanacimiento as fecha FROM auditoria015 au15 WHERE au15.rutadocumentos!='' AND au15.fechanacimiento!='' AND au15.idtipodocumento='".$campo0."' AND au15.identificacion='".$campo1."' 
			AND au15.fechanacimiento!=(select a15.fechanacimiento from aportes015 a15 where a15.idtipodocumento=au15.idtipodocumento and a15.identificacion=au15.identificacion) ";
}
$rs=$db->querySimple($sql);

$con=0;
$filas=array();
while ($row= $rs->fetch()){
	$filas[]= $row;
	$con++;
}// fin while
if($con==0){
	echo 0;
	exit();
}// fin if

echo json_encode($filas);
