<?php
/* autor:       Orlando Puentes
 * fecha:       Julio 23 de 2010
 * objetivo:    
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';


$url=$_SERVER['PHP_SELF'];
include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'auditoria.php';
include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'p.definiciones.class.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	echo "<script>alert('Hubo un error');</script>";
	exit();
}

$objClase=new Definiciones();
auditar($url);

$sql="SELECT * FROM aportes500 order by idagencia";
$agencia=$db->querySimple($sql);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="es">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>::Digitalizar::</title>
<link type="text/css" href="../css/css.1.8/jquery-ui-1.8.17.custom.css" rel="stylesheet">
<link type="text/css" href="../css/marco.css" rel="stylesheet">
<link type="text/css" href="../css/Estilos.css" rel="stylesheet">

<script type="text/javascript" src="../js/jquery-1.3.2.min.js"></script>
<script language="javascript" src="../js/comunes.js"></script>
<script type="text/javascript">
shortcut.add("Shift+F",function() {
		var URL=src();
		var url=URL+"aportes/trabajadores/consultaTrabajador.php";
    	window.open(url,"_blank");
    },{
	'propagate' : true,
	'target' : document 
        });        
</script>
<!-- colaboracion en linea  -->
<script language="javascript">
var nuevaDigitalizacion=0;
$(document).ready(function(){
	$("#selectDocDigit").focus();
	$('#but1').bind('click',function(evt){
		var tipoD = "";
		var Agencia = "";
		var carpetaD = "";
		if(nuevaDigitalizacion==0)
		{
			if($('#cedula').val()=="")
			{
				alert("Escriba el número de documento!!!");
			}
			else{
				tipoD=$("#selectDocDigit").val();
				Agencia=$("#selectAgencia").val();
				if ($('#check3').is(":checked")){ 
					carpetaD=$('#check3').val();
				}
				if ($('#check4').is(":checked")){ 
					carpetaD=$('#check4').val();
				}
				if ($('#check5').is(":checked")){ 
					carpetaD=$('#check5').val();
				}
				if ($('#check6').is(":checked")){ 
					carpetaD=$('#check6').val();
				}
				$("#check3,#check4,#check5,#check6,#cedula,#selectDocDigit,#selectAgencia").attr("disabled",true);
				$('#but1').val("Nueva Digitalización");
				nuevaDigitalizacion=1;
				$('#appletd').load('applet/appletCargar.php?tipodocumento='+tipoD+'&agencia='+Agencia+'&cedula='+$('#cedula').val()+'&carpetadocumento='+carpetaD);
			}
		}
		else{
			$("#check3,#check4,#check5,#check6,#cedula,#selectDocDigit,#selectAgencia").attr("disabled",false);
			$("#check4,#check5,#check6").attr("checked",false);
			$("#check3").attr("checked",true);
			$('#but1').val("Digitalizar");
			$("#selectDocDigit").val(1);
			$("#selectAgencia").val(1);
			$('#cedula').val("");
			try
			{
				var idapplet=document.getElementById('idapplet');
				idapplet.killApplet();
			}catch(e)
			{
			}
			$("#appletd").empty();
			nuevaDigitalizacion=0;
		}
	});
//Manual Ayuda
	$(function(){
		$("#ayuda").dialog({
		 	autoOpen: false,
			height: 450, 
			width: 700, 
			open: function(evt, ui){
					$('#ayuda').html('');
					$.get('../help/Digitalizacion/manualAyudadigitalizacion.html',function(data){
							$('#ayuda').html(data);
					});
			 }
		});
	});


//Dialog Colaboracion en linea
$("#dialog-form2").dialog({
		autoOpen: false,
		height: 400,
		width: 500,
		modal: true,
		buttons: {
			'Enviar': function() {
			var bValid = true;
			var campo=$('#notas').val();
			var campo0=$.trim(campo);
			if (campo0==""){
				$(this).dialog('close');
					return false;
				}
			var campo1=$('#usuario').val();
			var campo2="appletDigitalizacion.php";
			$.post(URL+'phpComunes/colaboracion.php',{v0:campo0,v1:campo1,v2:campo2},
			function(datos){
				if(datos=='1'){
					alert("Su comentario fue enviado correctamente!. Gracias por participar en el mejoramiento de SIGAS");
				}else{
					alert(datos);
				}
		});
		$(this).dialog('close');
		},
		Cancelar: function() {
			$(this).dialog('close');
			$("#dialog-form2").dialog("destroy");
		}
	}
	});
});
</script>


</head>

<body>
<form name="forma">
<center>
<!-- TABLA VISIBLE CON BOTONES -->
<br />
<table width="80%" border="0" cellspacing="0" cellpadding="0" align="center">
<tr>
<td width="13" height="29" class="arriba_iz">&nbsp;</td>
<td class="arriba_ce"><span class="letrablanca">::Digitalizar::</span></td>
<td width="13" class="arriba_de" align="right">&nbsp;</td>
</tr>      
<tr>
<td class="cuerpo_iz">&nbsp;</td>
<td class="cuerpo_ce">
<img src="../imagenes/menu/informacion.png" width="16" height="16" style="border: none; cursor: pointer" title="Manual" onClick="mostrarAyuda();" />
<img src="../imagenes/menu/notas.png" width="16" height="16" style="cursor: pointer" title="Colaboración en línea" onclick="notas();" /> 

<td class="cuerpo_de">&nbsp;</td>
</tr> 
<tr>     
<td class="cuerpo_iz">&nbsp;</td>
<td class="cuerpo_ce"><div id="error" style="color:#FF0000"></div></td>
<td class="cuerpo_de">&nbsp;</td>
</tr>
<tr>
<td class="cuerpo_iz">&nbsp;</td>

<td class="cuerpo_ce">
<div id="errores" align="left"></div>
<strong>Tipo Documento:</strong>
          <select name="selectDocDigit" id="selectDocDigit" class="box1">  
            <?php
			
			$consulta = $objClase->mostrar_datos( 1,1 );
            while( $row = mssql_fetch_array( $consulta ) ){
            	if($row['iddetalledef']!=7&&$row['iddetalledef']!=8)
				echo "<option value=". $row['iddetalledef'].">".$row['detalledefinicion']."</option>";
			}
        ?>
          </select>
<strong>Agencia:</strong>
		<select name="selectAgencia" class="box1" id="selectAgencia" >
		<?php 
		while ($row=$agencia->fetch()){
			echo "<option value=".$row['idagencia'].">".$row['agencia']."</option>";
		}
		?>
	</select>
<p>
<strong>Doc:</strong>
<input type="text" id="cedula" size="32">
<!-- <input type="checkbox" name="check1" id="check1" value="0" onchange="chk1()" checked="checked">
<strong>C.C</strong> &nbsp;&nbsp;
<input type="checkbox" name="check2" id="check2" value="1" onchange="chk2()">
<strong>NIT</strong>&nbsp;&nbsp;-->
<input type="button" id="but1" value="Digitalizar"></br></br>
<input type="checkbox" name="check3" id="check3" value="subsidio\" onchange="chk3()" checked="checked">
<strong>Subsido</strong>&nbsp;&nbsp;
<input type="checkbox" name="check4" id="check4" value="fonede\" onchange="chk4()">
<strong>Fonede</strong>&nbsp;&nbsp;
<input type="checkbox" name="check5" id="check5" value="desplazado\" onchange="chk5()">
<strong>Desplazado</strong>&nbsp;&nbsp;
<input type="checkbox" name="check6" id="check6" value="vivienda\" onchange="chk6()">
<strong>Vivienda</strong>&nbsp;&nbsp;
</p>
<div id="appletd" align="center">

</div>
<td class="cuerpo_de">&nbsp;</td><!-- FONDO DERECHA -->
<tr>
<td class="abajo_iz" >&nbsp;</td>
<td class="abajo_ce" ></td>
<td class="abajo_de" >&nbsp;</td>
</tr>
</table>  

<!--colaboracion en linea-->

<div id="dialogo-archivo" title="Archivo banco">
<div id="progreso" style="display: none; font-size: 15pt; font-weight: bold;">Procesado(s) <span id="pg">0</span> de <span id="tt"></span> archivo(s)</div>
<div id="log"></div>
</div>
<div id="dialog-form2" title="Colaboraci&oacute;n en l&iacute;nea" style="display:none">
<p>Por favor diligencie este formulario para enviar comentarios, errores o falencias encontradas en el proceso. M&aacute;ximo 250 caracteres </p>
<label>Tus comentarios:</label><br />
<textarea name="notas" id="notas" cols="60", rows="10"></textarea>
</div>

<!-- Manual Ayuda -->
<div id="ayuda" title="Manual de Digitalización" style="background-image:url(../imagenes/FondoGeneral0.png)"></div>
</form>
</body>
<script language="javascript">
function chk1()
{
	$("#check2").attr("checked",false);
	$("#check1").attr("checked",true);
}
function chk2()
{
	$("#check1").attr("checked",false);
	$("#check2").attr("checked",true);
}
function chk3()
{
	$("#check3").attr("checked",true);
	$("#check4").attr("checked",false);
	$("#check5").attr("checked",false);
	$("#check6").attr("checked",false);
}
function chk4()
{
	$("#check3").attr("checked",false);
	$("#check4").attr("checked",true);
	$("#check5").attr("checked",false);
	$("#check6").attr("checked",false);
}
function chk5()
{
	$("#check3").attr("checked",false);
	$("#check4").attr("checked",false);
	$("#check5").attr("checked",true);
	$("#check6").attr("checked",false);
}
function chk6()
{
	$("#check3").attr("checked",false);
	$("#check4").attr("checked",false);
	$("#check5").attr("checked",false);
	$("#check6").attr("checked",true);
}
function mostrarAyuda(){
	$("#ayuda").dialog('open' );
	}
function notas(){
	$("#dialog-form2").dialog('open');
	}
</script>
</html>
