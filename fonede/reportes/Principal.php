<?php
// este es el codigom para tomar la cookie en PHP periodo
	if (!isset($_COOKIE["periodo"])) {
		$periodo = "";
		$expire=time()+60*60*24;
		setcookie("periodo", $periodo, $expire);
	} else {
		$periodo = $_COOKIE["periodo"];
	}
//echo "periodo=".$periodo;
// este es el codigom para tomar la cookie en PHP periodo

// este es el codigom para tomar la cookie en PHP periodo
	if (!isset($_COOKIE["vigencia"])) {
		$vigencia = "";
		$expire=time()+60*60*24;
		setcookie("vigencia", $vigencia, $expire);
	} else {
		$vigencia = $_COOKIE["vigencia"];
	}
//echo "vigencia=".$vigencia;
// este es el codigom para tomar la cookie en PHP vigencia
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Documento sin t&iacute;tulo</title>
</head>

<body>
<table width="889" border="0" cellspacing="0" class="tablero">
  <tr>
    <td width="285" rowspan="2" style="text-align:center"><strong>CONCEPTO</strong></td>
    <td colspan="2" style="text-align:center"><strong>EJECUCI&Oacute;N DEL MES 
	<?php 
	if($periodo=='01'){ echo "ENERO";}
	if($periodo=='02'){ echo "FEBRERO";}
	if($periodo=='03'){ echo "MARZO";}
	if($periodo=='04'){ echo "ABRIL";}
	if($periodo=='05'){ echo "MAYO";}
	if($periodo=='06'){ echo "JUNIO";}
	if($periodo=='07'){ echo "JULIO";}
	if($periodo=='08'){ echo "AGOSTO";}
	if($periodo=='09'){ echo "SEPTIEMBRE";}
	if($periodo=='10'){ echo "OCTUBRE";}
	if($periodo=='11'){ echo "NOVIEMBRE";}
	if($periodo=='12'){ echo "DICIEMBRE";}	
	?>
    </strong></td>
    <td colspan="2" style="text-align:center"><strong>EJECUCI&Oacute;N TOTAL DEL A&Ntilde;O <?php echo $vigencia; ?></strong></td>
  </tr>
  <tr>
    <td style="text-align:center"><strong>Cantidad</strong></td>
    <td style="text-align:center"><strong>Valor</strong></td>
    <td style="text-align:center"><strong>Cantidad</strong></td>
    <td style="text-align:center"><strong>Valor</strong></td>
  </tr>
  <tr>
    <td>1. Saldo Inicial</td>
    <td width="148" bgcolor="#EBEBEB">&nbsp;</td>
    <td width="147" style="text-align:center"><input name="v" type="text" id="v" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly" style="color:#00F;background:#F7F7F7"/></td>
    <td width="150" bgcolor="#EBEBEB">&nbsp;</td>
    <td width="149" style="text-align:center"><input name="vt" type="text" id="vt" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly" style="color:#00F;background:#F7F7F7"/></td>
  </tr>
  <tr bgcolor="#EBEBEB">
    <td colspan="5"><table width="100%" height="25" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="9%" style="border:0"><strong>2. FUENTES:</strong></td>
        <td width="91%" height="24" style="border:0"><img id="mostrar5" src="../../imagenes/show.png" width="22" height="22" style=" display:none;cursor: pointer" title="Mostrar" onclick="mostrarr('ocultar5','mostrar5','fuentes');" /><img id="ocultar5" src="../../imagenes/hide.png" width="22" height="22" style="cursor: pointer" title="Ocultar" onclick="ocultarr('ocultar5','mostrar5','fuentes');" /></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td colspan="5"><div id="fuentes" style="display:block">
      <table width="100%" border="0" cellspacing="0" class="tablero">
        <tr>
          <td width="32%">2.1. Diferencia por valor del 55%, de los mayores de 18 a&ntilde;os</td>
          <td width="17%" bgcolor="#EBEBEB">&nbsp;</td>
          <td width="17%" style="text-align:center"><input name="v1" type="text" id="v1" onkeydown='format(this);' onkeyup='format(this),calcular(),calcular1();' onfocus="calcular(),calcular1(),calcular1();" value=""/></td>
          <td width="17%" bgcolor="#EBEBEB">&nbsp;</td>
          <td width="17%" style="text-align:center"><input name="vt1" type="text" id="vt1" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
        </tr>
        <tr>
          <td>2.2. Valor del porcentaje no ejecutado al sostenimiento de la SSF</td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td style="text-align:center"><input name="v2" type="text" id="v2" onkeydown='format(this);' onkeyup='format(this),calcular(),calcular1();' value=""/></td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td style="text-align:center"><input name="vt2" type="text" id="vt2" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
        </tr>
        <tr>
          <td>2.3. Disminuci&oacute;n Gastos de Administraci&oacute;n (2%)</td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td style="text-align:center"><input name="v3" type="text" id="v3" onkeydown='format(this);' onkeyup='format(this),calcular(),calcular1();' value=""/></td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td style="text-align:center"><input name="vt3" type="text" id="vt3" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
        </tr>
        <tr>
          <td>2.4. % seg&uacute;n cuociente particular</td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td style="text-align:center"><input name="v4" type="text" id="v4" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly" style="color:#00F;background:#F7F7F7"/></td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td style="text-align:center"><input name="vt4" type="text" id="vt4" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly" style="color:#00F;background:#F7F7F7"/></td>
        </tr>
        <tr>
          <td>2.4.1. 1% Cuociente &lt; al 80%</td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td style="text-align:center"><input name="v5" type="text" id="v5" onkeydown='format(this);' onkeyup='format(this),calcular(),calcular1();' value=""/></td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td style="text-align:center"><input name="vt5" type="text" id="vt5" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
        </tr>
        <tr>
          <td>2.4.2. 2% Cuociente entre el 80 y 100%</td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td style="text-align:center"><input name="v6" type="text" id="v6" onkeydown='format(this);' onkeyup='format(this),calcular(),calcular1();' value=""/></td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td style="text-align:center"><input name="vt6" type="text" id="vt6" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
        </tr>
        <tr>
          <td>2.4.3. 3% Cuociente &gt; al 100%</td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td style="text-align:center"><input name="v7" type="text" id="v7" onkeydown='format(this);' onkeyup='format(this),calcular(),calcular1();' value=""/></td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td style="text-align:center"><input name="vt7" type="text" id="vt7" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
        </tr>
        <tr>
          <td>2.5. Multas impuestas por SSF a la Corporaci&oacute;n art. 24,  Ley 789 /02</td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td style="text-align:center"><input name="v8" type="text" id="v8" onkeydown='format(this);' onkeyup='format(this),calcular(),calcular1();' value=""/></td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td style="text-align:center"><input name="vt8" type="text" id="vt8" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
        </tr>
        <tr>
          <td>2.6. Rendimientos Financieros</td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td style="text-align:center"><input name="v9" type="text" id="v9" onkeydown='format(this);' onkeyup='format(this),calcular(),calcular1();' value=""/></td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td style="text-align:center"><input name="vt9" type="text" id="vt9" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
        </tr>
        <tr>
          <td><strong>3. TOTAL FUENTES</strong></td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td style="text-align:center"><input name="v10" type="text" id="v10" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly" style="color:#00F;background:#F7F7F7"/></td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td style="text-align:center"><input name="vt10" type="text" id="vt10" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly" style="color:#00F;background:#F7F7F7"/></td>
        </tr>
        <tr>
          <td><strong>4. SALDO DISPONIBLE</strong></td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td style="text-align:center"><input name="v11" type="text" id="v11" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly" style="color:#00F;background:#F7F7F7"/></td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td style="text-align:center"><input name="vt11" type="text" id="vt11" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly" style="color:#00F;background:#F7F7F7"/></td>
        </tr>
      </table>
    </div></td>
  </tr>
  <tr bgcolor="#EBEBEB">
    <td colspan="5"><strong>5. APLICACIONES</strong></td>
  </tr>
  <tr>
    <td colspan="5">&nbsp;</td>
  </tr>
  <tr bgcolor="#EBEBEB">
    <td colspan="5"><table width="100%" height="25" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="34%" style="border:0"><strong>6. Programa de Microcr&eacute;dito</strong> ( 35% del Fonede) </td>
        <td width="66%" height="24" style="border:0"><img id="mostrar" src="../../imagenes/show.png" width="22" height="22" style="cursor: pointer" title="Mostrar" onclick="mostrarr('ocultar','mostrar','microcredito');" /><img id="ocultar" src="../../imagenes/hide.png" width="22" height="22" style="display:none;cursor: pointer" title="Ocultar" onclick="ocultarr('ocultar','mostrar','microcredito');" /></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td colspan="5"><div id="microcredito" style="display:none">
      <table width="100%" border="0" align="center" cellspacing="0" class="tablero">
        <tr>
          <td width="32%">6.1. Saldo Inicial</td>
          <td width="17%" bgcolor="#EBEBEB">&nbsp;</td>
          <td width="17%" bgcolor="#EBEBEB" style="text-align:center"><input name="v12" type="text" id="v12" onkeydown='format(this);' onkeyup='format(this),calcular(),calcular1();' value=""/></td>
          <td width="17%" bgcolor="#EBEBEB">&nbsp;</td>
          <td width="17%" style="text-align:center"><input name="vt12" type="text" id="vt12" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
        </tr>
        <tr>
          <td>6.2. Transferencias para Fovis</td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td style="text-align:center"><input name="v13" type="text" id="v13" onkeydown='format(this);' onkeyup='format(this),calcular(),calcular1();' value=""/></td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td style="text-align:center"><input name="vt13" type="text" id="vt13" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
        </tr>
        <tr>
          <td>6.3. Acciones en Entidades de Cr&eacute;dito y Otras aplicaciones</td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td style="text-align:center"><input name="v14" type="text" id="v14" onkeydown='format(this);' onkeyup='format(this),calcular(),calcular1();' value=""/></td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td style="text-align:center"><input name="vt14" type="text" id="vt14" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
        </tr>
        <tr>
          <td>6.4. Reintegros por pago de cr&eacute;ditos y/o devoluciones</td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td style="text-align:center"><input name="v15" type="text" id="v15" onkeydown='format(this);' onkeyup='format(this),calcular(),calcular1();' value=""/></td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td style="text-align:center"><input name="vt15" type="text" id="vt15" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
        </tr>
        <tr>
          <td>6.5. Apropiaci&oacute;n Programas de Microcr&eacute;dito</td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td style="text-align:center"><input name="v16" type="text" id="v16" onkeydown='format(this);' onkeyup='format(this),calcular(),calcular1();' value=""/></td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td style="text-align:center"><input name="vt16" type="text" id="vt16" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
        </tr>
        <tr>
          <td>6.6. Solicitudes recibidas</td>
          <td style="text-align:center"><input name="c1" type="text" id="c1" onkeydown='solonumeros(this);' onkeyup='solonumeros(this),calcular(),calcular1();' value=""/></td>
          <td style="text-align:center"><input name="v17" type="text" id="v17" onkeydown='format(this);' onkeyup='format(this),calcular(),calcular1();' value=""/></td>
          <td style="text-align:center"><input name="ct1" type="text" id="ct1" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
          <td style="text-align:center"><input name="vt17" type="text" id="vt17" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
        </tr>
        <tr>
          <td>6.7. Cr&eacute;ditos Asignados</td>
          <td style="text-align:center"><input name="c2" type="text" id="c2" onkeydown='solonumeros(this);' onkeyup='solonumeros(this),calcular(),calcular1();' value=""/></td>
          <td style="text-align:center"><input name="v18" type="text" id="v18" onkeydown='format(this);' onkeyup='format(this),calcular(),calcular1();' value=""/></td>
          <td style="text-align:center"><input name="ct2" type="text" id="ct2" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
          <td style="text-align:center"><input name="vt18" type="text" id="vt18" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
        </tr>
        <tr>
          <td>6.8. Saldo por ejecutar</td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td style="text-align:center"><input name="v19" type="text" id="v19" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly" style="color:#00F;background:#F7F7F7"/></td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td style="text-align:center"><input name="vt19" type="text" id="vt19" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly" style="color:#00F;background:#F7F7F7"/></td>
        </tr>
      </table>
    </div></td>
  </tr>
  <tr bgcolor="#EBEBEB">
    <td colspan="5"><table width="100%" height="25" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="53%" style="border:0"><strong>7. Programa Subsidios para Desempleados con vinculaci&oacute;n a Cajas</strong> (30% Fonede)</td>
        <td width="47%" height="24" style="border:0"><img id="mostrar1" src="../../imagenes/show.png" width="22" height="22" style="cursor: pointer" title="Mostrar" onclick="mostrarr('ocultar1','mostrar1','subsidiocon');" /><img id="ocultar1" src="../../imagenes/hide.png" width="22" height="22" style="display:none;cursor: pointer" title="Ocultar" onclick="ocultarr('ocultar1','mostrar1','subsidiocon');" /></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td colspan="5"><div id="subsidiocon" style="display:none">
      <table width="100%" border="0" cellspacing="0" class="tablero">
        <tr>
          <td width="32%">7.1. Saldo inicial</td>
          <td width="17%" bgcolor="#EBEBEB">&nbsp;</td>
          <td width="17%" style="text-align:center"><input name="v20" type="text" id="v20" onkeydown='format(this);' onkeyup='format(this),calcular(),calcular1();' value=""/></td>
          <td width="17%" bgcolor="#EBEBEB">&nbsp;</td>
          <td width="17%" style="text-align:center"><input name="vt20" type="text" id="vt20" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
        </tr>
        <tr>
          <td>7.2. Transferencias para Fovis</td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td style="text-align:center"><input name="v21" type="text" id="v21" onkeydown='format(this);' onkeyup='format(this),calcular(),calcular1();' value=""/></td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td style="text-align:center"><input name="vt21" type="text" id="vt21" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
        </tr>
        <tr>
          <td>7.3. Transferencias por Compensaci&oacute;n</td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td style="text-align:center"><input name="v22" type="text" id="v22" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly" style="color:#00F;background:#F7F7F7"/></td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td style="text-align:center"><input name="vt22" type="text" id="vt22" style="color:#00F;background:#F7F7F7" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
        </tr>
        <tr>
          <td>7.3.1. Transferencias otras Cajas</td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td style="text-align:center"><input name="v23" type="text" id="v23" onkeydown='format(this);' onkeyup='format(this),calcular(),calcular1();' value=""/></td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td style="text-align:center"><input name="vt23" type="text" id="vt23" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
        </tr>
        <tr>
          <td>7.3.2. Transferencias interna - otro programa</td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td style="text-align:center"><input name="v24" type="text" id="v24" onkeydown='format(this);' onkeyup='format(this),calcular(),calcular1();' value=""/></td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td style="text-align:center"><input name="vt24" type="text" id="vt24" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
        </tr>
        <tr>
          <td>7.4. Anulaciones o suspensiones por p&eacute;rdida de derechos</td>
          <td style="text-align:center"><input name="c3" type="text" id="c3" onkeydown='solonumeros(this);' onkeyup='solonumeros(this),calcular(),calcular1();' value=""/></td>
          <td style="text-align:center"><input name="v25" type="text" id="v25" onkeydown='format(this);' onkeyup='format(this),calcular(),calcular1();' value=""/></td>
          <td style="text-align:center"><input name="ct3" type="text" id="ct3" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
          <td style="text-align:center"><input name="vt25" type="text" id="vt25" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
        </tr>
        <tr>
          <td>7.5. Apropiaci&oacute;n para Subsidios</td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td style="text-align:center"><input name="v26" type="text" id="v26" onkeydown='format(this);' onkeyup='format(this),calcular(),calcular1();' value=""/></td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td style="text-align:center"><input name="vt26" type="text" id="vt26" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
        </tr>
        <tr>
          <td>7.6. Postulantes</td>
          <td style="text-align:center"><input name="c4" type="text" id="c4" onkeydown='solonumeros(this);' onkeyup='solonumeros(this),calcular(),calcular1();' value=""/></td>
          <td style="text-align:center"><input name="v27" type="text" id="v27" onkeydown='format(this);' onkeyup='format(this),calcular(),calcular1();' value=""/></td>
          <td style="text-align:center"><input name="ct4" type="text" id="ct4" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
          <td style="text-align:center"><input name="vt27" type="text" id="vt27" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
        </tr>
        <tr>
          <td>7.7. Postulantes Aceptados</td>
          <td style="text-align:center"><input name="c5" type="text" id="c5" onkeydown='solonumeros(this);' onkeyup='solonumeros(this),calcular(),calcular1();' value=""/></td>
          <td style="text-align:center"><input name="v28" type="text" id="v28" onkeydown='format(this);' onkeyup='format(this),calcular(),calcular1();' value=""/></td>
          <td style="text-align:center"><input name="ct5" type="text" id="ct5" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
          <td style="text-align:center"><input name="vt28" type="text" id="vt28" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
        </tr>
        <tr>
          <td>7.8.Subsidios Asignados:</td>
          <td style="text-align:center"><input name="c6" type="text" id="c6" onkeydown='solonumeros(this);' onkeyup='solonumeros(this);' value="" readonly="readonly" style="color:#00F;background:#F7F7F7"/></td>
          <td style="text-align:center"><input name="v29" type="text" id="v29" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly" style="color:#00F;background:#F7F7F7"/></td>
          <td style="text-align:center"><input name="ct6" type="text" id="ct6" style="color:#00F;background:#F7F7F7" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
          <td style="text-align:center"><input name="vt29" type="text" id="vt29" style="color:#00F;background:#F7F7F7" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
        </tr>
        <tr>
          <td>7.8.1. Salud</td>
          <td style="text-align:center"><input name="c7" type="text" id="c7" onkeydown='solonumeros(this);' onkeyup='solonumeros(this),calcular(),calcular1();' value=""/></td>
          <td style="text-align:center"><input name="v30" type="text" id="v30" onkeydown='format(this);' onkeyup='format(this),calcular(),calcular1();' value=""/></td>
          <td style="text-align:center"><input name="ct7" type="text" id="ct7" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
          <td style="text-align:center"><input name="vt30" type="text" id="vt30" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
        </tr>
        <tr>
          <td>7.8.2. Alimentaci&oacute;n</td>
          <td style="text-align:center"><input name="c8" type="text" id="c8" onkeydown='solonumeros(this);' onkeyup='solonumeros(this),calcular(),calcular1();' value=""/></td>
          <td style="text-align:center"><input name="v31" type="text" id="v31" onkeydown='format(this);' onkeyup='format(this),calcular(),calcular1();' value=""/></td>
          <td style="text-align:center"><input name="ct8" type="text" id="ct8" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
          <td style="text-align:center"><input name="vt31" type="text" id="vt31" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
        </tr>
        <tr>
          <td>7.8.3. Educaci&oacute;n</td>
          <td style="text-align:center"><input name="c9" type="text" id="c9" onkeydown='solonumeros(this);' onkeyup='solonumeros(this),calcular(),calcular1();' value=""/></td>
          <td style="text-align:center"><input name="v32" type="text" id="v32" onkeydown='format(this);' onkeyup='format(this),calcular(),calcular1();' value=""/></td>
          <td style="text-align:center"><input name="ct9" type="text" id="ct9" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
          <td style="text-align:center"><input name="vt32" type="text" id="vt32" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
        </tr>
        <tr>
          <td>7.9.Subsidios Pagados - Cuotas o Bonos:</td>
          <td style="text-align:center"><input name="c10" type="text" id="c10" onkeydown='solonumeros(this);' onkeyup='solonumeros(this);' value="" style="color:#00F;background:#F7F7F7"/></td>
          <td style="text-align:center"><input name="v33" type="text" id="v33" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly" style="color:#00F;background:#F7F7F7"/></td>
          <td style="text-align:center"><input name="ct10" type="text" id="ct10" style="color:#00F;background:#F7F7F7" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
          <td style="text-align:center"><input name="vt33" type="text" id="vt33" style="color:#00F;background:#F7F7F7" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
        </tr>
        <tr>
          <td>7.9.1. Salud</td>
          <td style="text-align:center"><input name="c11" type="text" id="c11" onkeydown='solonumeros(this);' onkeyup='solonumeros(this),calcular(),calcular1();' value=""/></td>
          <td style="text-align:center"><input name="v34" type="text" id="v34" onkeydown='format(this);' onkeyup='format(this),calcular(),calcular1();' value=""/></td>
          <td style="text-align:center"><input name="ct11" type="text" id="ct11" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
          <td style="text-align:center"><input name="vt34" type="text" id="vt34" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
        </tr>
        <tr>
          <td>7.9.2. Alimentaci&oacute;n</td>
          <td style="text-align:center"><input name="c12" type="text" id="c12" onkeydown='solonumeros(this);' onkeyup='solonumeros(this),calcular(),calcular1();' value=""/></td>
          <td style="text-align:center"><input name="v35" type="text" id="v35" onkeydown='format(this);' onkeyup='format(this),calcular(),calcular1();' value=""/></td>
          <td style="text-align:center"><input name="ct12" type="text" id="ct12" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
          <td style="text-align:center"><input name="vt35" type="text" id="vt35" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
        </tr>
        <tr>
          <td>7.9.3. Educaci&oacute;n</td>
          <td style="text-align:center"><input name="c13" type="text" id="c13" onkeydown='solonumeros(this);' onkeyup='solonumeros(this),calcular(),calcular1();' value=""/></td>
          <td style="text-align:center"><input name="v36" type="text" id="v36" onkeydown='format(this);' onkeyup='format(this),calcular(),calcular1();' value=""/></td>
          <td style="text-align:center"><input name="ct13" type="text" id="ct13" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
          <td style="text-align:center"><input name="vt36" type="text" id="vt36" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
        </tr>
        <tr>
          <td>7.10. Saldo por ejecutar</td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td style="text-align:center"><input name="v37" type="text" id="v37" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly" style="color:#00F;background:#F7F7F7"/></td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td style="text-align:center"><input name="vt37" type="text" id="vt37" style="color:#00F;background:#F7F7F7" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
        </tr>
      </table>
    </div></td>
  </tr>
  <tr bgcolor="#EBEBEB">
    <td colspan="5"><table width="100%" height="25" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="53%" style="border:0"><strong>8. Programa Subsidios para Desempleados sin vinculaci&oacute;n a Cajas </strong>(5% Fonede) </td>
        <td width="47%" height="24" style="border:0"><img id="mostrar2" src="../../imagenes/show.png" width="22" height="22" style="cursor: pointer" title="Mostrar" onclick="mostrarr('ocultar2','mostrar2','subsidiosin');" /><img id="ocultar2" src="../../imagenes/hide.png" width="22" height="22" style="display:none;cursor: pointer" title="Ocultar" onclick="ocultarr('ocultar2','mostrar2','subsidiosin');" /></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td colspan="5"><div id="subsidiosin" style="display:none">
      <table width="100%" border="0" cellspacing="0" class="tablero">
        <tr>
          <td width="32%">8.1. Saldo Inicial</td>
          <td width="17%" bgcolor="#EBEBEB">&nbsp;</td>
          <td width="17%" style="text-align:center"><input name="v38" type="text" id="v38" onkeydown='format(this);' onkeyup='format(this),calcular(),calcular1();' value=""/></td>
          <td width="17%" bgcolor="#EBEBEB">&nbsp;</td>
          <td width="17%" style="text-align:center"><input name="vt38" type="text" id="vt38" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
        </tr>
        <tr>
          <td>8.2. Transferencias para Fovis</td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td style="text-align:center"><input name="v39" type="text" id="v39" onkeydown='format(this);' onkeyup='format(this),calcular(),calcular1();' value=""/></td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td style="text-align:center"><input name="vt39" type="text" id="vt39" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
        </tr>
        <tr>
          <td>8.3. Transferencias por Compensaci&oacute;n</td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td style="text-align:center"><input name="v40" type="text" id="v40" style="color:#00F;background:#F7F7F7" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td style="text-align:center"><input name="vt40" type="text" id="vt40" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly" style="color:#00F;background:#F7F7F7"/></td>
        </tr>
        <tr>
          <td>8.3.1. Transferencias otras Cajas</td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td style="text-align:center"><input name="v41" type="text" id="v41" onkeydown='format(this);' onkeyup='format(this),calcular(),calcular1();' value=""/></td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td style="text-align:center"><input name="vt41" type="text" id="vt41" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
        </tr>
        <tr>
          <td>8.3.2. Transferencias interna - otro programa</td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td style="text-align:center"><input name="v42" type="text" id="v42" onkeydown='format(this);' onkeyup='format(this),calcular(),calcular1();' value=""/></td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td style="text-align:center"><input name="vt42" type="text" id="vt42" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
        </tr>
        <tr>
          <td>8.4. Anulaciones o suspensiones por p&eacute;rdida de derechos</td>
          <td style="text-align:center"><input name="c14" type="text" id="c14" onkeydown='solonumeros(this);' onkeyup='solonumeros(this),calcular(),calcular1();' value=""/></td>
          <td style="text-align:center"><input name="v43" type="text" id="v43" onkeydown='format(this);' onkeyup='format(this),calcular(),calcular1();' value=""/></td>
          <td style="text-align:center"><input name="ct14" type="text" id="ct14" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
          <td style="text-align:center"><input name="vt43" type="text" id="vt43" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
        </tr>
        <tr>
          <td>8.5. Apropiaci&oacute;n para Subsidios</td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td style="text-align:center"><input name="v44" type="text" id="v44" onkeydown='format(this);' onkeyup='format(this),calcular(),calcular1();' value=""/></td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td style="text-align:center"><input name="vt44" type="text" id="vt44" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
        </tr>
        <tr>
          <td>8.6. N&uacute;mero de Postulantes</td>
          <td style="text-align:center"><input name="c15" type="text" id="c15" onkeydown='solonumeros(this);' onkeyup='solonumeros(this),calcular(),calcular1();' value=""/></td>
          <td style="text-align:center"><input name="v45" type="text" id="v45" onkeydown='format(this);' onkeyup='format(this),calcular(),calcular1();' value=""/></td>
          <td style="text-align:center"><input name="ct15" type="text" id="ct15" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
          <td style="text-align:center"><input name="vt45" type="text" id="vt45" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
        </tr>
        <tr>
          <td>8.7. N&uacute;mero de Postulantes Aceptados</td>
          <td style="text-align:center"><input name="c16" type="text" id="c16" onkeydown='solonumeros(this);' onkeyup='solonumeros(this),calcular(),calcular1();' value=""/></td>
          <td style="text-align:center"><input name="v46" type="text" id="v46" onkeydown='format(this);' onkeyup='format(this),calcular(),calcular1();' value=""/></td>
          <td style="text-align:center"><input name="ct16" type="text" id="ct16" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
          <td style="text-align:center"><input name="vt46" type="text" id="vt46" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
        </tr>
        <tr>
          <td>8.8. Subsidios Asignados:</td>
          <td style="text-align:center"><input name="c17" type="text" id="c17" style="color:#00F;background:#F7F7F7" onkeydown='solonumeros(this);' onkeyup='solonumeros(this);' value="" readonly="readonly"/></td>
          <td style="text-align:center"><input name="v47" type="text" id="v47" style="color:#00F;background:#F7F7F7" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
          <td style="text-align:center"><input name="ct17" type="text" id="ct17" style="color:#00F;background:#F7F7F7" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
          <td style="text-align:center"><input name="vt47" type="text" id="vt47" style="color:#00F;background:#F7F7F7" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
        </tr>
        <tr>
          <td>8.8.1. <strong>Artista</strong></td>
          <td style="text-align:center"><input name="c18" type="text" id="c18" style="color:#00F;background:#F7F7F7" onkeydown='solonumeros(this);' onkeyup='solonumeros(this);' value="" readonly="readonly"/></td>
          <td style="text-align:center"><input name="v48" type="text" id="v48" style="color:#00F;background:#F7F7F7" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
          <td style="text-align:center"><input name="ct18" type="text" id="ct18" style="color:#00F;background:#F7F7F7" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
          <td style="text-align:center"><input name="vt48" type="text" id="vt48" style="color:#00F;background:#F7F7F7" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
        </tr>
        <tr>
          <td>8.8.1.1. Salud</td>
          <td style="text-align:center"><input name="c19" type="text" id="c19" onkeydown='solonumeros(this);' onkeyup='solonumeros(this),calcular(),calcular1();' value=""/></td>
          <td style="text-align:center"><input name="v49" type="text" id="v49" onkeydown='format(this);' onkeyup='format(this),calcular(),calcular1();' value=""/></td>
          <td style="text-align:center"><input name="ct19" type="text" id="ct19" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
          <td style="text-align:center"><input name="vt49" type="text" id="vt49" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
        </tr>
        <tr>
          <td>8.8.1.2. Alimentaci&oacute;n</td>
          <td style="text-align:center"><input name="c20" type="text" id="c20" onkeydown='solonumeros(this);' onkeyup='solonumeros(this),calcular(),calcular1();' value=""/></td>
          <td style="text-align:center"><input name="v50" type="text" id="v50" onkeydown='format(this);' onkeyup='format(this),calcular(),calcular1();' value=""/></td>
          <td style="text-align:center"><input name="ct20" type="text" id="ct20" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
          <td style="text-align:center"><input name="vt50" type="text" id="vt50" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
        </tr>
        <tr>
          <td>8.8.1.3. Educaci&oacute;n</td>
          <td style="text-align:center"><input name="c21" type="text" id="c21" onkeydown='solonumeros(this);' onkeyup='solonumeros(this),calcular(),calcular1();' value=""/></td>
          <td style="text-align:center"><input name="v51" type="text" id="v51" onkeydown='format(this);' onkeyup='format(this),calcular(),calcular1();' value=""/></td>
          <td style="text-align:center"><input name="ct21" type="text" id="ct21" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
          <td style="text-align:center"><input name="vt51" type="text" id="vt51" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
        </tr>
        <tr>
          <td>8.8.2. <strong>Deportista</strong></td>
          <td style="text-align:center"><input name="c22" type="text" id="c22" style="color:#00F;background:#F7F7F7" onkeydown='solonumeros(this);' onkeyup='solonumeros(this);' value="" readonly="readonly"/></td>
          <td style="text-align:center"><input name="v52" type="text" id="v52" style="color:#00F;background:#F7F7F7" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
          <td style="text-align:center"><input name="ct22" type="text" id="ct22" style="color:#00F;background:#F7F7F7" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
          <td style="text-align:center"><input name="vt52" type="text" id="vt52" style="color:#00F;background:#F7F7F7" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
        </tr>
        <tr>
          <td>8.8.2.1. Salud</td>
          <td style="text-align:center"><input name="c23" type="text" id="c23" onkeydown='solonumeros(this);' onkeyup='solonumeros(this),calcular(),calcular1();' value=""/></td>
          <td style="text-align:center"><input name="v53" type="text" id="v53" onkeydown='format(this);' onkeyup='format(this),calcular(),calcular1();' value=""/></td>
          <td style="text-align:center"><input name="ct23" type="text" id="ct23" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
          <td style="text-align:center"><input name="vt53" type="text" id="vt53" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
        </tr>
        <tr>
          <td>8.8.2..2. Alimentaci&oacute;n</td>
          <td style="text-align:center"><input name="c24" type="text" id="c24" onkeydown='solonumeros(this);' onkeyup='solonumeros(this),calcular(),calcular1();' value=""/></td>
          <td style="text-align:center"><input name="v54" type="text" id="v54" onkeydown='format(this);' onkeyup='format(this),calcular(),calcular1();' value=""/></td>
          <td style="text-align:center"><input name="ct24" type="text" id="ct24" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
          <td style="text-align:center"><input name="vt54" type="text" id="vt54" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
        </tr>
        <tr>
          <td>8.8.2..3. Educaci&oacute;n</td>
          <td style="text-align:center"><input name="c25" type="text" id="c25" onkeydown='solonumeros(this);' onkeyup='solonumeros(this),calcular(),calcular1();' value=""/></td>
          <td style="text-align:center"><input name="v55" type="text" id="v55" onkeydown='format(this);' onkeyup='format(this),calcular(),calcular1();' value=""/></td>
          <td style="text-align:center"><input name="ct25" type="text" id="ct25" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
          <td style="text-align:center"><input name="vt55" type="text" id="vt55" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
        </tr>
        <tr>
          <td>8.8.3. <strong>Escritor</strong></td>
          <td style="text-align:center"><input name="c26" type="text" id="c26" style="color:#00F;background:#F7F7F7" onkeydown='solonumeros(this);' onkeyup='solonumeros(this);' value="" readonly="readonly"/></td>
          <td style="text-align:center"><input name="v56" type="text" id="v56" style="color:#00F;background:#F7F7F7" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
          <td style="text-align:center"><input name="ct26" type="text" id="ct26" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly" style="color:#00F;background:#F7F7F7"/></td>
          <td style="text-align:center"><input name="vt56" type="text" id="vt56" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly" style="color:#00F;background:#F7F7F7"/></td>
        </tr>
        <tr>
          <td>8.8.3.1. Salud</td>
          <td style="text-align:center"><input name="c27" type="text" id="c27" onkeydown='solonumeros(this);' onkeyup='solonumeros(this),calcular(),calcular1();' value=""/></td>
          <td style="text-align:center"><input name="v57" type="text" id="v57" onkeydown='format(this);' onkeyup='format(this),calcular(),calcular1();' value=""/></td>
          <td style="text-align:center"><input name="ct27" type="text" id="ct27" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
          <td style="text-align:center"><input name="vt57" type="text" id="vt57" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
        </tr>
        <tr>
          <td>8.8.3.2. Alimentaci&oacute;n</td>
          <td style="text-align:center"><input name="c28" type="text" id="c28" onkeydown='solonumeros(this);' onkeyup='solonumeros(this),calcular(),calcular1();' value=""/></td>
          <td style="text-align:center"><input name="v58" type="text" id="v58" onkeydown='format(this);' onkeyup='format(this),calcular(),calcular1();' value=""/></td>
          <td style="text-align:center"><input name="ct28" type="text" id="ct28" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
          <td style="text-align:center"><input name="vt58" type="text" id="vt58" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
        </tr>
        <tr>
          <td>8.8.3.3. Educaci&oacute;n</td>
          <td style="text-align:center"><input name="c29" type="text" id="c29" onkeydown='solonumeros(this);' onkeyup='solonumeros(this),calcular(),calcular1();' value=""/></td>
          <td style="text-align:center"><input name="v59" type="text" id="v59" onkeydown='format(this);' onkeyup='format(this),calcular(),calcular1();' value=""/></td>
          <td style="text-align:center"><input name="ct29" type="text" id="ct29" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
          <td style="text-align:center"><input name="vt59" type="text" id="vt59" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
        </tr>
        <tr>
          <td>8.8.4.<strong> Otros</strong></td>
          <td style="text-align:center"><input name="c30" type="text" id="c30" style="color:#00F;background:#F7F7F7" onkeydown='solonumeros(this);' onkeyup='solonumeros(this);' value="" readonly="readonly"/></td>
          <td style="text-align:center"><input name="v60" type="text" id="v60" style="color:#00F;background:#F7F7F7" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
          <td style="text-align:center"><input name="ct30" type="text" id="ct30" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly" style="color:#00F;background:#F7F7F7"/></td>
          <td style="text-align:center"><input name="vt60" type="text" id="vt60" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly" style="color:#00F;background:#F7F7F7"/></td>
        </tr>
        <tr>
          <td>8.8.4.1. Salud</td>
          <td style="text-align:center"><input name="c31" type="text" id="c31" onkeydown='solonumeros(this);' onkeyup='solonumeros(this),calcular(),calcular1();' value=""/></td>
          <td style="text-align:center"><input name="v61" type="text" id="v61" onkeydown='format(this);' onkeyup='format(this),calcular(),calcular1();' value=""/></td>
          <td style="text-align:center"><input name="ct31" type="text" id="ct31" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
          <td style="text-align:center"><input name="vt61" type="text" id="vt61" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
        </tr>
        <tr>
          <td>8.8.4.2. Alimentaci&oacute;n</td>
          <td style="text-align:center"><input name="c32" type="text" id="c32" onkeydown='solonumeros(this);' onkeyup='solonumeros(this),calcular(),calcular1();' value=""/></td>
          <td style="text-align:center"><input name="v62" type="text" id="v62" onkeydown='format(this);' onkeyup='format(this),calcular(),calcular1();' value=""/></td>
          <td style="text-align:center"><input name="ct32" type="text" id="ct32" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
          <td style="text-align:center"><input name="vt62" type="text" id="vt62" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
        </tr>
        <tr>
          <td>8.8.4.3. Educaci&oacute;n</td>
          <td style="text-align:center"><input name="c33" type="text" id="c33" onkeydown='solonumeros(this);' onkeyup='solonumeros(this),calcular(),calcular1();' value=""/></td>
          <td style="text-align:center"><input name="v63" type="text" id="v63" onkeydown='format(this);' onkeyup='format(this),calcular(),calcular1();' value=""/></td>
          <td style="text-align:center"><input name="ct33" type="text" id="ct33" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
          <td style="text-align:center"><input name="vt63" type="text" id="vt63" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
        </tr>
        <tr>
          <td>8.9.Subsidios Pagados - Cuotas o Bonos:</td>
          <td style="text-align:center"><input name="c34" type="text" id="c34" style="color:#00F;background:#F7F7F7" onkeydown='solonumeros(this);' onkeyup='solonumeros(this);' value="" readonly="readonly"/></td>
          <td style="text-align:center"><input name="v64" type="text" id="v64" style="color:#00F;background:#F7F7F7" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
          <td style="text-align:center"><input name="ct34" type="text" id="ct34" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly" style="color:#00F;background:#F7F7F7"/></td>
          <td style="text-align:center"><input name="vt64" type="text" id="vt64" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly" style="color:#00F;background:#F7F7F7"/></td>
        </tr>
        <tr>
          <td>8.9.1. Salud</td>
          <td style="text-align:center"><input name="c35" type="text" id="c35" onkeydown='solonumeros(this);' onkeyup='solonumeros(this),calcular(),calcular1();' value=""/></td>
          <td style="text-align:center"><input name="v65" type="text" id="v65" onkeydown='format(this);' onkeyup='format(this),calcular(),calcular1();' value=""/></td>
          <td style="text-align:center"><input name="ct35" type="text" id="ct35" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
          <td style="text-align:center"><input name="vt65" type="text" id="vt65" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
        </tr>
        <tr>
          <td>8.9.2. Alimentaci&oacute;n</td>
          <td style="text-align:center"><input name="c36" type="text" id="c36" onkeydown='solonumeros(this);' onkeyup='solonumeros(this),calcular(),calcular1();' value=""/></td>
          <td style="text-align:center"><input name="v66" type="text" id="v66" onkeydown='format(this);' onkeyup='format(this),calcular(),calcular1();' value=""/></td>
          <td style="text-align:center"><input name="ct36" type="text" id="ct36" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
          <td style="text-align:center"><input name="vt66" type="text" id="vt66" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
        </tr>
        <tr>
          <td>8.9.3 Educaci&oacute;n</td>
          <td style="text-align:center"><input name="c37" type="text" id="c37" onkeydown='solonumeros(this);' onkeyup='solonumeros(this),calcular(),calcular1();' value=""/></td>
          <td style="text-align:center"><input name="v67" type="text" id="v67" onkeydown='format(this);' onkeyup='format(this),calcular(),calcular1();' value=""/></td>
          <td style="text-align:center"><input name="ct37" type="text" id="ct37" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
          <td style="text-align:center"><input name="vt67" type="text" id="vt67" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
        </tr>
        <tr>
          <td>8.10. Saldo por ejecutar</td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td style="text-align:center"><input name="v68" type="text" id="v68" style="color:#00F;background:#F7F7F7" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td style="text-align:center"><input name="vt68" type="text" id="vt68" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly" style="color:#00F;background:#F7F7F7"/></td>
        </tr>
      </table>
    </div></td>
  </tr>
  <tr bgcolor="#EBEBEB">
    <td colspan="5"><table width="100%" height="25" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="53%" style="border:0"><strong>9. Programa Capacitaci&oacute;n proceso inserci&oacute;n laboral</strong> ( 25% del Fonede)</td>
        <td width="47%" height="24" style="border:0"><img id="mostrar3" src="../../imagenes/show.png" width="22" height="22" style="cursor: pointer" title="Mostrar" onclick="mostrarr('ocultar3','mostrar3','capacitacion');" /><img id="ocultar3" src="../../imagenes/hide.png" width="22" height="22" style="display:none;cursor: pointer" title="Ocultar" onclick="ocultarr('ocultar3','mostrar3','capacitacion');" /></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td colspan="5"><div id="capacitacion" style="display:none">
      <table width="100%" border="0" cellspacing="0" class="tablero">
        <tr>
          <td width="32%">9.1. Saldo Inicial</td>
          <td width="17%" bgcolor="#EBEBEB">&nbsp;</td>
          <td width="17%" style="text-align:center"><input name="v69" type="text" id="v69" onkeydown='format(this);' onkeyup='format(this),calcular(),calcular1();' value=""/></td>
          <td width="17%" bgcolor="#EBEBEB">&nbsp;</td>
          <td width="17%" style="text-align:center"><input name="vt69" type="text" id="vt69" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
        </tr>
        <tr>
          <td>9.2. Transferencias para Fovis</td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td style="text-align:center"><input name="v70" type="text" id="v70" onkeydown='format(this);' onkeyup='format(this),calcular(),calcular1();' value=""/></td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td style="text-align:center"><input name="vt70" type="text" id="vt70" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
        </tr>
        <tr>
          <td>9.3 Reintegros Cursos No Ejecutados</td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td style="text-align:center"><input name="v71" type="text" id="v71" onkeydown='format(this);' onkeyup='format(this),calcular(),calcular1();' value=""/></td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td style="text-align:center"><input name="vt71" type="text" id="vt71" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
        </tr>
        <tr>
          <td>9.4. Apropiaci&oacute;n para Capacitaci&oacute;n</td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td style="text-align:center"><input name="v72" type="text" id="v72" onkeydown='format(this);' onkeyup='format(this),calcular(),calcular1();' value=""/></td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td style="text-align:center"><input name="vt72" type="text" id="vt72" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
        </tr>
        <tr>
          <td>9.5 N&uacute;mero de solicitudes recibidas</td>
          <td style="text-align:center"><input name="c38" type="text" id="c38" onkeydown='solonumeros(this);' onkeyup='solonumeros(this),calcular(),calcular1();' value=""/></td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td style="text-align:center"><input name="ct38" type="text" id="ct38" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
        </tr>
        <tr>
          <td>9.6. Cursos Ejecutados</td>
          <td style="text-align:center"><input name="c39" type="text" id="c39" onkeydown='solonumeros(this);' onkeyup='solonumeros(this),calcular(),calcular1();' value=""/></td>
          <td style="text-align:center"><input name="v73" type="text" id="v73" onkeydown='format(this);' onkeyup='format(this),calcular(),calcular1();' value=""/></td>
          <td style="text-align:center"><input name="ct39" type="text" id="ct39" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
          <td style="text-align:center"><input name="vt73" type="text" id="vt73" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
        </tr>
        <tr>
          <td>9.7. N&uacute;mero de Beneficiarios</td>
          <td style="text-align:center"><input name="c40" type="text" id="c40" onkeydown='solonumeros(this);' onkeyup='solonumeros(this),calcular(),calcular1();' value=""/></td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td style="text-align:center"><input name="ct40" type="text" id="ct40" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
        </tr>
        <tr>
          <td>9.8. Saldo por ejecutar</td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td style="text-align:center"><input name="v74" type="text" id="v74" style="color:#00F;background:#F7F7F7" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td style="text-align:center"><input name="vt74" type="text" id="vt74" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly" style="color:#00F;background:#F7F7F7"/></td>
        </tr>
      </table>
    </div></td>
  </tr>
  <tr bgcolor="#EBEBEB">
    <td colspan="5"><table width="100%" height="25" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="35%" style="border:0"><strong>10. Gastos de Administraci&oacute;n</strong> (Hasta 5% del Fonede)</td>
        <td width="65%" height="24" style="border:0"><img id="mostrar4" src="../../imagenes/show.png" width="22" height="22" style="cursor: pointer" title="Mostrar" onclick="mostrarr('ocultar4','mostrar4','administracion');" /><img id="ocultar4" src="../../imagenes/hide.png" width="22" height="22" style="display:none;cursor: pointer" title="Ocultar" onclick="ocultarr('ocultar4','mostrar4','administracion');" /></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td colspan="5"><div id="administracion" style="display:none">
      <table width="100%" border="0" cellspacing="0" class="tablero">
        <tr>
          <td width="32%">10.1. Saldo inicial </td>
          <td width="17%" bgcolor="#EBEBEB">&nbsp;</td>
          <td width="17%" style="text-align:center"><input name="v75" type="text" id="v75" onkeydown='format(this);' onkeyup='format(this),calcular(),calcular1();' value=""/></td>
          <td width="17%" bgcolor="#EBEBEB">&nbsp;</td>
          <td width="17%" style="text-align:center"><input name="vt75" type="text" id="vt75" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
        </tr>
        <tr>
          <td>10.2. Transferencias para Fovis</td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td style="text-align:center"><input name="v76" type="text" id="v76" onkeydown='format(this);' onkeyup='format(this),calcular(),calcular1();' value=""/></td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td style="text-align:center"><input name="vt76" type="text" id="vt76" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
        </tr>
        <tr>
          <td>10.3. Transferencias por Compensaci&oacute;n</td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td style="text-align:center"><input name="v77" type="text" id="v77" style="color:#00F;background:#F7F7F7" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td style="text-align:center"><input name="vt77" type="text" id="vt77" onkeydown='format(this);' onkeyup='format(this);' value="" style="color:#00F;background:#F7F7F7"/></td>
        </tr>
        <tr>
          <td>10.3.1. Transferencias otras Cajas</td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td style="text-align:center"><input name="v78" type="text" id="v78" onkeydown='format(this);' onkeyup='format(this),calcular(),calcular1();' value=""/></td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td style="text-align:center"><input name="vt78" type="text" id="vt78" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
        </tr>
        <tr>
          <td>10.3.2. Transferencias interna - otro programa</td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td style="text-align:center"><input name="v79" type="text" id="v79" onkeydown='format(this);' onkeyup='format(this),calcular(),calcular1();' value=""/></td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td style="text-align:center"><input name="vt79" type="text" id="vt79" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
        </tr>
        <tr>
          <td>10.4. Apropiaci&oacute;n para Gastos de Administraci&oacute;n</td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td style="text-align:center"><input name="v80" type="text" id="v80" onkeydown='format(this);' onkeyup='format(this),calcular(),calcular1();' value=""/></td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td style="text-align:center"><input name="vt80" type="text" id="vt80" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
        </tr>
        <tr>
          <td>10.5.Gastos de Administraci&oacute;n ejecutados</td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td style="text-align:center"><input name="v81" type="text" id="v81" onkeydown='format(this);' onkeyup='format(this),calcular(),calcular1();' value=""/></td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td style="text-align:center"><input name="vt81" type="text" id="vt81" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
        </tr>
        <tr>
          <td>10.6. Saldo por ejecutar </td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td style="text-align:center"><input name="v82" type="text" id="v82" style="color:#00F;background:#F7F7F7" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
          <td bgcolor="#EBEBEB">&nbsp;</td>
          <td style="text-align:center"><input name="vt82" type="text" id="vt82" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly" style="color:#00F;background:#F7F7F7"/></td>
        </tr>       
      </table>
    </div></td>
  </tr>
  <tr>
    <td><strong>11. TOTAL APLICACIONES</strong></td>
    <td bgcolor="#EBEBEB">&nbsp;</td>
    <td style="text-align:center"><input name="v83" type="text" id="v83" style="color:#00F;background:#F7F7F7" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
    <td bgcolor="#EBEBEB">&nbsp;</td>
    <td style="text-align:center"><input name="vt83" type="text" id="vt83" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly" style="color:#00F;background:#F7F7F7"/></td>
  </tr>
  <tr>
    <td><strong>12. SALDO FINAL</strong></td>
    <td bgcolor="#EBEBEB">&nbsp;</td>
    <td style="text-align:center"><input name="v84" type="text" id="v84" style="color:#00F;background:#F7F7F7" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly"/></td>
    <td bgcolor="#EBEBEB">&nbsp;</td>
    <td style="text-align:center"><input name="vt84" type="text" id="vt84" onkeydown='format(this);' onkeyup='format(this);' value="" readonly="readonly" style="color:#00F;background:#F7F7F7"/></td>
  </tr>
  <tr>
    <td colspan="5">&nbsp;
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="50%" style=" text-align:right; border:0"><input name="button" id="button"  type="button" onclick="listarejefonede(),guardarejefonede();" value="Generar"/></td>
          <td width="50%" style=" border:0"><input name="modificar" id="modificar"  type="button" onclick="actualizarejefonede();" value="Actualizar" style="display:none" /></td>
        </tr>
      </table>      
    </td>
  </tr>
</table>
</body>
</html>