<?php
ini_set('display_errors','On');
//date_default_timezone_set("America/Bogota");
include_once '../../rsc/pdo/IFXDbManejador.php';
include_once '../../rsc/pdo/IFXerror.php';

$db = IFXDbManejador::conectarDB(); 
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();	
}
$periodo=$_REQUEST['v0'];
$vigencia=$_REQUEST['v1'];

$sql="select sum(diferenciafuentes) as diferenciafuentes,sum(valorporcentajefuentes) as valorporcentajefuentes,sum(disminuciongastosfuentes) as disminuciongastosfuentes,
sum(couciente1fuentes) as couciente1fuentes,sum(couciente2fuentes) as couciente2fuentes,sum(couciente3fuentes) as couciente3fuentes,
sum(multasfuentes) as multasfuentes,sum(rendimientosfuentes) as rendimientosfuentes,sum(saldoinicialmicro) as saldoinicialmicro,
sum(fovismicro) as fovismicro,sum(accionesmicro) as accionesmicro,sum(reintegrosmicro) as reintegrosmicro,sum(apropiacionmicro) as apropiacionmicro,
sum(cantidadsolicitudesmicro) as cantidadsolicitudesmicro,sum(solicitudesmicro) as solicitudesmicro,sum(cantidacreditosmicro) as cantidacreditosmicro,
sum(creditosmicro) as creditosmicro,sum(saldoinicialsubsidioscon) as saldoinicialsubsidioscon,sum(fovissubsidioscon) as fovissubsidioscon,
sum(totaltranfessubsidioscon) as totaltranfessubsidioscon,sum(tranfeotrascajasubsidioscon) as tranfeotrascajasubsidioscon,sum(tranfeinternasubsidioscon) as tranfeinternasubsidioscon,
sum(cantidadanulasubsidioscon) as cantidadanulasubsidioscon,sum(anulacionessubsidioscon) as anulacionessubsidioscon,sum(apropiacionsubsidioscon) as apropiacionsubsidioscon,
sum(cantidadpostusubsidioscon) as cantidadpostusubsidioscon,sum(postulantessubsidioscon) as postulantessubsidioscon,sum(cantidadpostuacepsubsidioscon) as cantidadpostuacepsubsidioscon,
sum(postulantesacepsubsidioscon) as postulantesacepsubsidioscon,sum(cantidadsaludsubasigcon) as cantidadsaludsubasigcon,sum(saludsubasigcon) as saludsubasigcon,
sum(cantidadalimentacionsubasigcon) as cantidadalimentacionsubasigcon,sum(alimentacionsubasigcon) as alimentacionsubasigcon,sum(cantidadeducacionsubasigcon) as cantidadeducacionsubasigcon,
sum(educacionsubasigcon) as educacionsubasigcon,sum(cantidadsaludsubpagcon) as cantidadsaludsubpagcon,sum(saludsubpagcon) as saludsubpagcon,
sum(cantidadalimentacionsubpagcon) as cantidadalimentacionsubpagcon,sum(alimentacionsubpagcon) as alimentacionsubpagcon,sum(cantidadeducacionsubpagcon) as cantidadeducacionsubpagcon,
sum(educacionsubpagcon) as educacionsubpagcon,sum(saldoinicialsubsidiossin) as saldoinicialsubsidiossin,sum(fovissubsidiossin) as fovissubsidiossin,
sum(tranfeotrascajasubsidiossin) as tranfeotrascajasubsidiossin,sum(tranfeinternasubsidiossin) as tranfeinternasubsidiossin,sum(cantidadanulasubsidiossin) as cantidadanulasubsidiossin,
sum(anulacionessubsidiossin) as anulacionessubsidiossin,sum(apropiacionsubsidiossin) as apropiacionsubsidiossin,sum(cantidadpostusubsidiossin) as cantidadpostusubsidiossin,
sum(postulantessubsidiossin) as postulantessubsidiossin,sum(cantidadpostuacepsubsidiossin) as cantidadpostuacepsubsidiossin,sum(postulantesacepsubsidiossin) as postulantesacepsubsidiossin,
sum(cantidadsaludartsubasigsin) as cantidadsaludartsubasigsin,sum(saludartsubasigsin) as saludartsubasigsin,sum(cantidadalimentacionartsubasigsin) as cantidadalimentacionartsubasigsin,
sum(alimentacionartsubasigsin) as alimentacionartsubasigsin,sum(cantidadeducacionartsubasigsin) as cantidadeducacionartsubasigsin,sum(educacionartsubasigsin) as educacionartsubasigsin,
sum(cantidadsaluddepsubasigsin) as cantidadsaluddepsubasigsin,sum(saluddepsubasigsin) as saluddepsubasigsin,sum(cantidadalimentaciondepsubasigsin) as cantidadalimentaciondepsubasigsin,
sum(alimentaciondepsubasigsin) as alimentaciondepsubasigsin,sum(cantidadeducaciondepsubasigsin) as cantidadeducaciondepsubasigsin,sum(educaciondepsubasigsin) as educaciondepsubasigsin,
sum(cantidadsaludescrisubasigsin) as cantidadsaludescrisubasigsin,sum(saludescrisubasigsin) as saludescrisubasigsin,sum(cantidadalimentacionescrisubasigsin) as cantidadalimentacionescrisubasigsin,
sum(alimentacionescrisubasigsin) as alimentacionescrisubasigsin,sum(cantidadeducacionescrisubasigsin) as cantidadeducacionescrisubasigsin,
sum(educacionescrisubasigsin) as educacionescrisubasigsin,sum(cantidadsaludotrsubasigsin) as cantidadsaludotrsubasigsin,sum(saludotrsubasigsin) as saludotrsubasigsin,
sum(cantidadalimentacionotrsubasigsin) as cantidadalimentacionotrsubasigsin,sum(alimentacionotrsubasigsin) as alimentacionotrsubasigsin,
sum(cantidadeducacionotrsubasigsin) as cantidadeducacionotrsubasigsin,sum(educacionotrsubasigsin) as educacionotrsubasigsin,sum(cantidadsaludsubpagsin) as cantidadsaludsubpagsin,
sum(saludsubpagsin) as saludsubpagsin,sum(cantidadalimentacionsubpagsin) as cantidadalimentacionsubpagsin,sum(alimentacionsubpagsin) as alimentacionsubpagsin,
sum(cantidadeducacionsubpagsin) as cantidadeducacionsubpagsin,sum(educacionsubpagsin) as educacionsubpagsin,sum(saldoinicialcap) as saldoinicialcap,
sum(foviscap) as foviscap,sum(reintegroscap) as reintegroscap,sum(apropiacioncap) as apropiacioncap,sum(cantidadsolicitudescap) as cantidadsolicitudescap,
sum(cantidadcursosejecap) as cantidadcursosejecap,sum(cursosejecutadoscap) as cursosejecutadoscap,sum(cantidadbeneficiarioscap) as cantidadbeneficiarioscap,
sum(saldoinicialadm) as saldoinicialadm,sum(fovisadm) as fovisadm,sum(tranfeotrascajaadm) as tranfeotrascajaadm,sum(tranfeinternaadm) as tranfeinternaadm,
sum(apropiacionadm) as apropiacionadm,sum(gastosadministrativos) as gastosadministrativos from aportes236 where periodo<'".$periodo."' and vigencia='".$vigencia."'";
$rs=$db->querySimple($sql);

$con=0;
$filas=array();
while ($row= $rs->fetch()){
	$filas[]= $row;
	$con++;
}
if($con==0){
	echo 0;
	exit();
}

echo json_encode($filas);

?>