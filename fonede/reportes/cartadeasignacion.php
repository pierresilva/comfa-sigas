<link type="text/css" href="../../css/css.1.8/jquery-ui-1.8.17.custom.css" rel="stylesheet" />
<link type="text/css" href="../../css/Estilos.css" rel="stylesheet"/>
<link type="text/css" rel="stylesheet" href="../../css/marco.css" />

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3c.org/TR/1999/REC-html401-19991224/loose.dtd">
<!-- saved from url=(0020)http://www.2mdc.com/ -->
<head>
<meta http-equiv=Content-Type content="text/html; charset=utf-8" />
<meta http-equiv=content-language content=es />
<title>Carta Asignaci&oacute;n Fonede</title>


<script languaje="javascript" src="js/reportes.js"></script>
<script type="text/javascript" src="../../js/comunes.js"></script>

<script language="javascript" src="../../js/1.6/jquery-1.6.2.min.js"></script>
<script language="javascript" src="../../js/1.6/jquery-ui-1.8.16.custom.min.js"></script>

<link media=screen href="estilo-screen.css" type=text/css rel=stylesheet />

<script>
function verasignadoscartaasignacion(seccional,vinculacion,resolucion,fecha,asunto,enunciado1,enunciado2,att,cargo){

var page = 'cartadeasignacion.php?seccional='+seccional
         +'&vinculacion='+vinculacion
		 +'&resolucion='+resolucion
		 +'&fecha='+fecha	
		 +'&asunto='+asunto			
		 +'&enunciado1='+enunciado1	
		 +'&enunciado2='+enunciado2	
		 +'&att='+att	
		 +'&cargo='+cargo		 	 	 		
		 +'&ok=ok';
         window.location.href=page;   	
	
}//fin function verasignados(){ 
</script>    

<script>
function cargarfecha()
{
	
	var now = new Date();
    var strDateTime = [[AddZero(now.getHours()), AddZero(now.getMinutes())].join(":"), now.getHours() >= 12 ? "p.m" : "a.m"].join(" ");

    //Pad given value to the left with "0"
    function AddZero(num) {
    return (num >= 0 && num < 10) ? "0" + num : num + "";
}
	
	var meses = new Array ("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
	var diasSemana = new Array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sabado");
	var f=new Date();
	//document.write(diasSemana[f.getDay()] + ", " + f.getDate() + " de " + meses[f.getMonth()] + " de " + f.getFullYear());
	//document.getElementById('fechainvitacion').value=diasSemana[f.getDay()] + " " + f.getDate() + " de " + meses[f.getMonth()] + " de " + f.getFullYear() +" a las "	+ strDateTime;
	
	document.getElementById('fecha').value=f.getDate() + " de " + meses[f.getMonth()] + " de " + f.getFullYear()
}
</script>   
<script>

function limita(area_texto,max,nombre)
{
	var error=0;
  if(area_texto.value.length>=max)
  {
	  area_texto.value=area_texto.value.substring(0,max);	 
	  $(nombre).addClass("ui-state-error");
	 
  }
  else
  {
	   $("table.tablero :input.ui-state-error,select.ui-state-error").removeClass("ui-state-error");//Limpiar la clase de error
  }

}
</script>
</head>

<body onLoad="cargarfecha();">
<?php
ini_set('display_errors','Off');
date_default_timezone_set("America/Bogota");
include_once '../../rsc/pdo/IFXDbManejador.php';
include_once '../../rsc/pdo/IFXerror.php';


$db = IFXDbManejador::conectarDB(); 
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();	
}

$sql="SELECT * FROM aportes500";
$rs_seccional=$db->querySimple($sql);

$sql="SELECT * FROM aportes221";
$rs_vinculacion=$db->querySimple($sql);

$sqlap222="SELECT * FROM aportes222";
$rsap222=$db->querySimple($sqlap222);


$anoactual=date('Y');
$mesactual=date('m');


       if ($_GET['ok'] == 'ok') {
		               
            $seccional=$_GET['seccional'];
			$vinculacion=$_GET['vinculacion'];
			$resolucion=$_GET['resolucion'];
			$fecha=$_GET['fecha'];
			$asunto=$_GET['asunto'];			
			$enunciado1=$_GET['enunciado1'];
			$enunciado2=$_GET['enunciado2'];
			$att=$_GET['att'];
			$cargo=$_GET['cargo'];		
			
			
         
            $sql = "select aportes015.identificacion,aportes015.idpersona,aportes015.pnombre,aportes015.snombre,
			        aportes015.papellido,aportes015.sapellido,aportes015.direccion,aportes015.telefono,
					aportes015.iddepresidencia,aportes015.idciuresidencia,
					aportes221.idconvenio,aportes221.idagencia,aportes500.agencia,
                    aportes220.idconvenio,aportes220.descripcion,aportes222.consecutivo				
			        from   aportes004,aportes221,aportes015,aportes500,aportes220,aportes222
                    where 1=1			
                    and aportes004.numero=aportes015.identificacion 
                    and aportes015.idpersona=aportes221.idpersona 
					and aportes221.idagencia=aportes500.codigo
                    and aportes221.idconvenio=aportes220.idconvenio
					and aportes221.idapropiacion=aportes222.idapropiacion 
	                and aportes221.estado='A' ";		        
          
            if ($seccional !='0') {
            $sql = $sql . " and aportes221.idagencia=".$seccional; 			 
            }
			
			if ($vinculacion !='0') {
            $sql = $sql . " and aportes221.vinculacion=".$vinculacion; 			 
            }
			
			if ($resolucion !='0') {
            $sql = $sql . " and aportes222.consecutivo=".$resolucion; 			 
            }
            
            $sql = $sql . " order by especial DESC,fechagrabacion";      
			
			//echo "SQL---->".$sql."<br>";
        
            $rs=$db->querySimple($sql);
			$rs1=$db->querySimple($sql);
								
			$encontrodatos=0;
			while($registro1=$rs1->fetch()){	
			++$encotrodatos;			 
			 }
if($encotrodatos!=0)
{	
		 
	 header('Content-type: application/vnd.ms-word');
	 header("Content-Disposition: attachment; filename=cartasasignacion.doc");
	 header("Pragma: no-cache");
	 header("Expires: 0");
	 
           
         $cont_asignado=0;
         while($registro=$rs->fetch()){
         ++$cont_asignado;
		 $agencia=strtolower(utf8_encode($registro['agencia']));		 
		 $departamento=$registro['iddepresidencia'];
		 $ciudad=$registro['idciuresidencia'];
		 $idpersona=$registro['idpersona'];
		 
		 $sqlap012="SELECT * FROM aportes012 where periodo=$anoactual$mesactual";
         $rsap012=$db->querySimple($sqlap012);
		 while($rowap012=$rsap012->fetch()){
			  $valorsubsidiofonede=$rowap012['valorsubsidiofonede'];
			 	 
		 }        				 
		 
		 $sql_municipio ="SELECT distinct codmunicipio,municipio from aportes089 where coddepartamento='".$departamento."' and codmunicipio='".$ciudad."' order by municipio";
	     $rsmuni=$db->querySimple($sql_municipio);  
		 	 while($rowmuni=$rsmuni->fetch()){		
		
$myHTML = '

<div class="contenedor">
<div class="contenido_ini">


<p style="text-align:justify"><span style="font-family:&#39;Arial&#39;;font-size:12pt">'.ucfirst($agencia).', '.$fecha.'</span></p> 

<p style="text-align:justify"><span style="font-family:&#39;Arial&#39;;font-size:12pt">Se&ntilde;or (a)</span></p>

<p style="text-align:justify"><span style="font-family:&#39;Arial&#39;;font-size:12pt"><strong>'.utf8_encode($registro['pnombre']." ".$registro['snombre']." ".$registro['papellido']." ".$registro['sapellido']).'</strong></span></p> 
<p style="text-align:justify"><span style="font-family:&#39;Arial&#39;;font-size:12pt">'.$rowmuni['municipio'].'</span></p>

<p style="text-align:justify"><span style="font-family:&#39;Arial&#39;;font-size:12pt"><strong>ASUNTO:' .$asunto.'</strong></p>


<p style="text-align:justify"><span style="font-family:&#39;Arial&#39;;font-size:12pt">'.$enunciado1.'</span></p>

<br><strong>Vigencia del Subsidio:</strong> 6 meses</br>
<br><strong>Beneficiario: '.utf8_encode($registro['pnombre']." ".$registro['snombre']." ".$registro['papellido']." ".$registro['sapellido']).'</strong></br>
<br><strong>C&eacute;dula de Ciudadan&iacute;a:</strong>' .$registro['identificacion'].' </br>
<br><strong>Monto del subsidio asignado:</strong> $'.number_format($valorsubsidiofonede*6).'</br>
<br><strong>Forma de pago:</strong> 6 cuotas iguales mensuales de $'.number_format($valorsubsidiofonede).'</br>
<br><strong>Aplicaci&oacute;n:</strong> '.utf8_encode($registro['descripcion']).'</br>


<p style="text-align:justify"><span style="font-family:&#39;Arial&#39;;font-size:12pt">'.$enunciado2.'</span></span></p>

<p style="text-align:justify"><span style="font-family:&#39;Arial&#39;;font-size:12pt">Cordialmente,</span></p>
<p style="text-align:justify"><span style="font-family:&#39;Arial&#39;;font-size:12pt"><img height="50" width="60" alt="mapa" src="http://localhost/sigas/fonede/imagenes/firma.png"></span></p>

<p style="text-align:justify"><span style="font-family:&#39;Arial&#39;;font-size:12pt"><strong>'.$att.'</strong></span></p>
<p style="text-align:justify"><span style="font-family:&#39;Arial&#39;;font-size:12pt">'.$cargo.'</span></p>
</div>
</div>
<pre>
</pre>
';
echo $myHTML;
			
                 }//fin while				
		 	}//fin while
		}// fin encontrodatos
		else
			{
				echo "<script language='JavaScript'>alert('No se Encontraron Datos de Asignados'); location.href='cartadeasignacion.php';</script>";
			}
         
      }//fin if $_POST['ok'] == 'ok'
      else {
        ?>         		      
        <!--<div  id="divpostulados">-->
        <form name="form" method="post" action="">
          <table width="459" height="446" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
              <td height="29" class="arriba_iz">&nbsp;</td>
              <td class="arriba_ce"><span class="letrablanca">::&nbsp;::</span></td>
              <td class="arriba_de" >&nbsp;</td>
            </tr>
            <tr>
              <td class="cuerpo_iz">&nbsp;</td>
              <td class="cuerpo_ce"><div id="error" style="color:#FF0000"></div></td>
              <td class="cuerpo_de">&nbsp;</td>
            </tr>
            <tr>
              <td class="cuerpo_iz">&nbsp;</td>
              <!-- TABLAS DE FORMULARIO -->
              <td class="cuerpo_ce"><center>
                <table width="404" height="358" border="0" cellspacing="0" class="tablero">
                  <tr bgcolor="#EBEBEB">
                    <td colspan="4" style="text-align:center" ></td>
                  </tr>
                  <tr bgcolor="#EBEBEB">
                    <td colspan="4" style="text-align:center" ><strong>CARTAS ASIGNACI&Oacute;N FONEDE</strong></td>
                  </tr>
                  <tr>
                    <td><strong>Seccional:</strong></td>
                    <td colspan="3"><select name="seccional" size="1" id="seccional" class="box1">
                      <option value="0">::Todos::</option>
                      <?php
						while($row=$rs_seccional->fetch()){
						
						  echo "<option value=".$row['codigo'] ." >".$row['agencia']."</option>";
						}
					   ?>
                    </select></td>
                  </tr>
                  <tr>
                    <td><strong>Tipo de Vinculaci&oacute;n:</strong></td>
                    <td colspan="3"><select name="vinculacion" size="1" id="vinculacion" class="box1">
                      <option value="0" selected="selected">::Todos::</option>
                      <option value="1">Sin Vinculacion</option>
                      <option value="2">Con Vinculacion</option>
                    </select></td>
                  </tr>
                  <tr>
                    <td><strong>Resoluci&oacute;n:</strong></td>
                    <td colspan="3"><select name="resolucion" size="1" id="resolucion" class="box1" >
                      <option value="0">::Todos::</option>
                      <?php
						while($rowap222=$rsap222->fetch()){					   					
							echo "<option value=".$rowap222['consecutivo'] ." >Resoluci&oacute;n ".$rowap222['consecutivo']." </option>";							
						}// fin if
					  ?>
                    </select></td>
                  </tr>
                  <tr>
                    <td colspan="4">&nbsp;</td>
                  </tr>
                  <tr bgcolor="#EBEBEB">
                    <td colspan="4" style="text-align:center" ><strong>ESTRUCTURA DE LA CARTA</strong></td>
                  </tr>
                  <tr>
                    <td><strong>Fecha:</strong></td>
                    <td colspan="3"><input name="fecha" type="text" id="fecha" style="color: #000" size="32" /></td>
                  </tr>
                  <tr>
                    <td><strong>Asunto:</strong></td>
                    <td colspan="3"><input name="asunto" type="text" id="asunto" value="Asignaci&oacute;n Subsidio al desempleo (FONEDE)" size="45" onKeyUp="limita(this,42,asunto);" onKeyDown="limita(this,42,asunto);"/></td>
                  </tr>
                  <tr>
                    <td><strong>Enunciado 1:</strong></td>
                    <td colspan="3"><label for="enunciado1"></label>
                    <textarea name="enunciado1" id="enunciado1" cols="45" rows="6" onKeyUp="limita(this,278,enunciado1);" onKeyDown="limita(this,278,enunciado1);">Me es grato comunicarle que de conformidad con la normatividad vigente, especialmente la establecida en la Ley 789 de 2002, Decreto 2340 de 2003, LA CAJA DE COMPENSACI&Oacute;N FAMILIAR DEL HUILA, le asigna un subsidio al desempleo, cuyo monto y caracter&iacute;stica se detallan como sigue.</textarea></td>
                  </tr>
                  <tr>
                    <td><strong>Enunciado 2:</strong></td>
                    <td colspan="3"><label for="enunciado2"></label>
                    <textarea name="enunciado2" id="enunciado2" cols="45" rows="10" onKeyUp="limita(this,900,enunciado2);" onKeyDown="limita(this,900,enunciado2);">Es importante recordarle que el subsidio de desempleo se termina por obtenci&oacute;n de nuevo empleo, por rechazo del beneficiario de una oferta de trabajo acorde a su formaci&oacute;n acad&eacute;mica, por ser llamado el beneficiario a prestar el servicio militar, por recibir remuneraci&oacute;n alguna por trabajo, por condena penal privativa de la libertad, por cumplimiento de los (6) meses de recibir subsidio, por haber presentado documentos falsos para acreditar derecho, por muerte del beneficiario del subsidio.Este subsidio no se entrega en dinero en efectivo, se dar&aacute; en bonos alimenticios a partir del d&iacute;a 5 de cada mes, los cuales podr&aacute;n ser utilizados &uacute;nicamente en productos b&aacute;sicos de la canasta familiar, no se podr&aacute;n adquirir productos como licor y tabacos; en bonos de salud y educaci&oacute;n los cuales ser&aacute;n girados directamente a la EPS, Colegio o Instituto escogido por el beneficiario.
                    </textarea></td>
                  </tr>
                  <tr>
                    <td><strong>Att:</strong></td>
                    <td colspan="3"><input name="att" type="text" id="att" value="ARMANDO ARIZA QUINTERO" size="32"/></td>
                  </tr>
                  <tr>
                    <td><strong>Cargo:</strong></td>
                    <td colspan="3"><input name="cargo" type="text" id="cargo" value="Director Administrativo" size="32"/></td>
                  </tr>
                  <tr>
                    <td colspan="4">&nbsp;</td>
                  </tr>
                  <tr>
                    <td colspan="4"><center>
                      <input name="button" id="button"  type="button"  onclick="listarasignadoscartaasignacion();" value="Generar Cartas" />
                    </center></td>
                  </tr>
                </table>
              </center></td>
              <td class="cuerpo_de"></td>
              <!-- FONDO DERECHA -->
            </tr>
            <tr>
              <td height="41" class="abajo_iz">&nbsp;</td>
              <td class="abajo_ce">&nbsp;</td>
              <td class="abajo_de">&nbsp;</td>
            </tr>
          </table>
</form>
      <?php
      }//fin else if $_POST['ok'] == 'ok'
      ?>
</body>
</html>