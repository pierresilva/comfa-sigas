<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Documento sin título</title>
<style type="text/css">
#form1 table tr td h5 {
	/* [disabled]text-align: left; */
}
</style>

<link href="ckEstilo_imp.css" rel="stylesheet" type="text/css" />
 
 </head>
  <body>
<?php

//ini_set('display_errors','Off');
       require_once ('../../../modulos/sitio.php');
	   require_once ('../../../../software/class/formularioica.php');
	   require_once ('../../../../software/class/Actividad.php');
       require_once ('../../../../software/class/TipoDocumento.php');
	   require_once ('../../../../software/class/Usuario.php');
	   require_once ('../../../../software/class/FormularioIndyCom.php');
	
	 
$id_formulario=$_GET['id_formulario'];
//echo'<br>id_formulario='.$id_formulario;	

  $url_archivos=$sitio."/software/modulos";
  
   $objFormularioIndustriayComercio = new FormularioIndustriayComercio();
   $objFormularioIndustriayComercio = new FormularioIndustriayComercio();
   $objFormularioIndustriayComercio->buscar_opciones_de_uso();
   $objFormularioIndustriayComercio->obtenerFormularioIndustriayComercio(); 
   
   $objActividad = new Actividad();
   $objActividad->buscar_actividades();
   $objActividad->obtenertipos_actividad(); 
 
   $objTipoDocumento = new TipoDocumento();
   $objTipoDocumento->buscar_tipos_documentos();
   $objTipoDocumento->obtener_tipos_documentos(); 
 
?>
<div  id="divTablaIndustriayComercio">
<form id="form1" name="form1" method="post" action="">

  <table width="549" border="0">
    <tr>
      <td>&nbsp;</td>
      <td align="center">&nbsp;</td>
      <td>&nbsp;</td>
      <td>Formulario No.</td>
      <td> <?php echo $numero_formulario; ?>&nbsp;</td>
    </tr>
    <tr>
      <td><img src="../../../modulos/neiva.jpg" alt="" /></td>
      <td align="center"><h1>MUNICIPIO DE NEIVA</h1>
        <p>SECRETARIA DE HACIENDA MUNICIPAL</p>
        <h5>DECLARACION UNICA DEL<br />
          IMPUESTO DE INDUSTRIA Y<br />
          COMERCIO Y COMPLEMENTARIOS<br />
      DE AVISOS Y TABLEROS</h5></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td align="center">Periodo Gravable: <?php echo $ano; ?></td>
      <td>&nbsp;</td>
      <td colspan="2">&nbsp;</td>
    </tr>
  </table>
  
  
  
   <table width="549" border="1" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td colspan="11" align="left"><strong>OPCIONES DE USO</strong></td>
    </tr>
    <tr>
      <td align="center"><h6>Declaraci&oacute;n Inicial</h6></td>
      <td align="center">Correcci&oacute;n Voluntaria</td>
      <td align="center">Correcci&oacute;n Aplaza/to</td>
      <td align="center">Correcci&oacute;n Req. Especial</td>
      <td align="center">Correcci&oacute;n Liq. Revisi&oacute;n</td>
      <td align="center">Correcci&oacute;n Aritm&eacute;tica</td>
      <td align="center">Correcci&oacute;n Deci. No pres</td>
      <td align="center">Clausura</td>
    </tr>
    <tr>
      <td colspan="11" align="left">NUMERO DEL FORMULARIO DE DECLARACION QUE CORRIGE: <?php echo $num_radica; ?></td>
    </tr>
    <tr>
      <td colspan="11" align="left"><strong>IDENTIFICACION DEL CONTRIBUYENTE Y ACTIVIDAD</strong></td>
    </tr>
    <tr>
      <td colspan="11" align="left" >1.Apellidos y Nombres o Razon Social : <?php echo $nom_raz; ?></td>
    </tr>
    <tr>
      <td width="84" align="left" >2.Identificaci&oacute;n</td>
      <td width="167" colspan="2" align="center" ><?php echo $descripcion_tipo_identificacion; ?></td>
      <td width="144" colspan="2" align="center" >Numero: <?php echo $numero_identificacion; ?></td>
      <td width="144" colspan="3" align="center" >D.V : <?php echo $digiveri; ?></td>
    </tr>
    <tr>
      <td colspan="11" align="left" >3. Telefono: <?php echo $telefono; ?></td>
    </tr>
    <tr>
      <td colspan="11" align="left" >4. Direcci&oacute;n de Notificaci&oacute;n: <?php echo $direccion_pone; ?></td>
    </tr>
    <tr>
      <td colspan="11" align="left" >5. Actividad Economica Principal:</td>
    </tr>
    <tr>
      <td colspan="5" align="left" >6. Numero de Establecimientos</td>
      <td colspan="2" align="center" >Neiva: <?php echo $numero_estab_neiva; ?></td>
      <td colspan="2" align="center" >Municipios: <?php echo $numero_estab_otrosmun; ?></td>
    </tr>
    <tr>
      <td colspan="11" align="left" ><strong>BASE GRAVABLE (Aproxime los valores al m&uacute;ltiplo de mil m&aacute;s cercano)</strong></td>
    </tr>
    <tr>
      <td colspan="11" align="left" >7. Total ingresos  ordinarios y extraordinarios del periodo gravable </td>
    </tr>
    <tr>
      <td colspan="11" align="left" >8. Menos: ingresos fuera  de Neiva (Declarados y pagados en respectivos Municipios)</td>
    </tr>
    <tr>
      <td colspan="11" align="left" >9. Total ingresos brutos  en Neiva</td>
    </tr>
    <tr>
      <td colspan="11" align="left" >10. Menos: devoluci&oacute;n y descuentos</td>
    </tr>
    <tr>
      <td colspan="11" align="left" >11. Menos: deducciones y exenciones y act.  Que no causan el impuesto</td>
    </tr>
    <tr>
      <td colspan="11" align="left" >12. Ingresos netos  gravables</td>
    </tr>
    <tr>
      <td colspan="2" align="center" ><strong>Relacione en anexo mas actividades</strong></td>
      <td colspan="2" align="center" ><strong>Codigo actividad (Art. 74 E.T.M.)</strong></td>
      <td colspan="2" align="center" ><strong>Tarifa X Mil (Art. 74 E.T.M.)</strong></td>
      <td colspan="5" align="center" ><strong>Ingresos netos gravables</strong></td>
      </tr>
    <tr>
      <td colspan="2" align="left" >Actividad  Econ&oacute;mica Principal</td>
      <td colspan="2" align="center" >&nbsp;</td>
      <td colspan="2" align="center" >&nbsp;</td>
      <td colspan="5" align="center" >&nbsp;</td>
    </tr>
    <tr>
      <td colspan="2" align="left" >Actividad  Secundaria 1</td>
      <td colspan="2" align="center" >&nbsp;</td>
      <td colspan="2" align="center" >&nbsp;</td>
      <td colspan="5" align="center" >&nbsp;</td>
    </tr>
    <tr>
      <td colspan="2" align="left" >Actividad  Secundaria 2</td>
      <td colspan="2" align="center" >&nbsp;</td>
      <td colspan="2" align="center" >&nbsp;</td>
      <td colspan="5" align="center" >&nbsp;</td>
    </tr>
    <tr>
      <td colspan="2" align="left" >Actividad  Secundaria 3</td>
      <td colspan="2" align="center" >&nbsp;</td>
      <td colspan="2" align="center" >&nbsp;</td>
      <td colspan="5" align="center" >&nbsp;</td>
    </tr>
    <tr>
      <td colspan="11" align="left" ><strong>Liquidaci&oacute;n  Privada Del Impuesto (Aproxime los valores al m&uacute;ltiple de mil m&aacute;s cercano)</strong></td>
    </tr>
    <tr>
      <td colspan="10" align="left" >13. Impuesto anual de industria y  comercio </td>
      <td align="left" >&nbsp;</td>
    </tr>
    <tr>
      <td colspan="10" align="left" >14. Mas: Impuesto anual de avisos (15%  rengl&oacute;n anterior)</td>
      <td align="left" >&nbsp;</td>
    </tr>
    <tr>
      <td colspan="10" align="left" >15. Mas: Oficinas adicionales sector  financiero (45 u.v.t por cada oficina, anual)</td>
      <td align="left" >&nbsp;</td>
    </tr>
    <tr>
      <td colspan="10" align="left" >16. Total impuestos a cargo (Sume rengl&oacute;n  13 + 14 + 15)</td>
      <td align="left" >&nbsp;</td>
    </tr>
    <tr>
      <td colspan="10" align="left" >17. Mas: sobre tasa bomberil (3% sobre  rengl&oacute;n 13)</td>
      <td align="left" >&nbsp;</td>
    </tr>
    <tr>
      <td colspan="10" align="left" >18. Menos: valor que le retuvieron a  titulo del impuesto de industria y comercio</td>
      <td align="left" >&nbsp;</td>
    </tr>
    <tr>
      <td colspan="10" align="left" >19. Mas: anticipo impuesto presente  a&ntilde;o (40% rengl&oacute;n 16)-valor rengl&oacute;n 18</td>
      <td align="left" >&nbsp;</td>
    </tr>
    <tr>
      <td colspan="10" align="left" >20. Menos: anticipo a&ntilde;o anterior</td>
      <td align="left" >&nbsp;</td>
    </tr>
    <tr>
      <td colspan="10" align="left" >21. Menos: saldo a favor periodo  anterior</td>
      <td align="left" >&nbsp;</td>
    </tr>
    <tr>
      <td colspan="10" align="left" >22. M&aacute;s sanciones (extemporaneidad,  correcci&oacute;n, inexactitud, etc.)</td>
      <td align="left" >&nbsp;</td>
    </tr>
    <tr>
      <td colspan="10" align="left" >23. Total saldo a cargo (rengl&oacute;n 16 +  17 - 18 + 19 - 20 - 21 + 22)</td>
      <td align="left" >&nbsp;</td>
    </tr>
    <tr>
      <td colspan="10" align="left" >24. Total saldo a favor</td>
      <td align="left" >&nbsp;</td>
    </tr>
    <tr>
      <td colspan="11" align="left" ><strong>PAGO</strong></td>
      </tr>
    <tr>
      <td colspan="10" align="left" >25. Valor a pagar  (renglón 23 - renglón 22)</td>
      <td align="left" >&nbsp;</td>
    </tr>
    <tr>
      <td colspan="10" align="left" >26. Mas: sanciones (igual  renglón 22)</td>
      <td align="left" >&nbsp;</td>
    </tr>
    <tr>
      <td colspan="10" align="left" >27. Intereses de mora</td>
      <td align="left" >&nbsp;</td>
    </tr>
    <tr>
      <td colspan="10" align="left" >28. Total a pagar:  (renglones 25 + 26 + 27)</td>
      <td align="left" >&nbsp;</td>
    </tr>
    <tr>
      <td colspan="11" align="left" ><strong>FIRMAS</strong>
        </td></td>
      </tr>
    <tr>
      <td colspan="11" align="left" >&nbsp; </td>
    </tr>
    <tr>
      <td colspan="11" align="left" >&nbsp;</td>
    </tr>
   </table>
</form>
</div>
</body>
</html>
