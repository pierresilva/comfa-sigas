<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Giro Fonede</title>

<link type="text/css"
	href="../../css/css.1.8/jquery-ui-1.8.17.custom.css" rel="stylesheet" />
<script language="javascript" src="../../js/1.6/jquery-1.6.2.min.js"></script>
<script language="javascript"
	src="../../js/1.6/jquery-ui-1.8.16.custom.min.js"></script>

<link href="../../css/Estilos.css" rel="stylesheet" type="text/css" />
<link type="text/css" href="../../css/marco.css" rel="stylesheet" />
<script languaje="javascript" src="js/giro.js"></script>
<script type="text/javascript" src="../../js/comunes.js"></script>

<script>
function generareportegirado(id) {

		var page = '../pdf/pdf_reporte_Girado.php?ok=ok&variable_girado='+id;
		//window.location.href=page;
		window.open(page,'_blank');			
        //location.href='pdf/pdf_reportes_Reteica.php?id_formulario='+id+'&estado=2';
}
</script>

<script>
function verpostulados(vinculacion_postulados,tipo_giropostulado,numeroidentificacion_postulado,fechacorte_postulado,periodo){

var page = 'Giro.php?vinculacion_postulados='+vinculacion_postulados
		 +'&tipo_giropostulado='+tipo_giropostulado
         +'&numeroidentificacion_postulado='+numeroidentificacion_postulado
		 +'&fechacorte_postulado='+fechacorte_postulado
		 +'&periodo='+periodo		
		 +'&ok=ok';
         window.location.href=page; 

		  	
}//fin function verportulados(){
 
</script>

<script>
 buscarperiodo();
</script>
</head>

<body>
<?php
// ini_set('display_errors','Off');
date_default_timezone_set ( 'America/Bogota' );
$root = $_SERVER ['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once $root;

$url = $_SERVER ['PHP_SELF'];
include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'auditoria.php';
auditar ( $url );

include_once '../../rsc/pdo/IFXDbManejador.php';
include_once '../../rsc/pdo/IFXerror.php';

$db = IFXDbManejador::conectarDB ();
if ($db->conexionID == null) {
	$cadena = $db->error;
	echo msg_error ( $cadena );
	exit ();
}

$usuario = $_SESSION ['USUARIO'];

function cambiarFormatoFecha2($fecha) {
	list ( $anio, $mes, $dia ) = explode ( "-", $fecha );
	return $dia . "-" . $mes . "-" . $anio;
}

?>
<?php

if($_GET ['ok'] == 'ok'){
	$vinculacion_postulados = $_GET ['vinculacion_postulados'];
	$numeroidentificacion_postulado = $_GET ['numeroidentificacion_postulado'];
	$tipo_giropostulado = $_GET ['tipo_giropostulado'];
	$fechacorte_postulado = $_GET ['fechacorte_postulado'];
	$periodo = $_GET ['periodo'];
	
	$anterior = "As";
	$actual = "Gi";
	
	$sql = "select count(*)
	from aportes221, aportes015
	where aportes015.idpersona = aportes221.idpersona
	and (proceso='" . $anterior . "' or proceso='" . $actual . "')";	
	
	if ($numeroidentificacion_postulado != "")
		$sql .= " and aportes015.identificacion='" . $numeroidentificacion_postulado . "'";
	
	$rs = $db->querySimple ( $sql );
	$rowcount = $rs->fetchColumn ();
	
	if($rowcount>0){
		?>
		<script>
		function iniciargiro(){
			var i =0;
			for(i=0;i<<?php echo $rowcount/50,0; ?>;i++){
			    $.ajax({
			    		url: '../grabacion/validacionfonedenueva.php',
			    		async: false,
			    		data: {v0:'giro',v1:'<?php echo $numeroidentificacion_postulado; ?>',v2:i},		
			    		success: function(idRadicacionG){
			    			
			    		}
			   	});
			}
			var page = 'Giro.php?vinculacion_postulados=<?php echo $vinculacion_postulados; ?>'
			 +'&tipo_giropostulado=<?php echo $tipo_giropostulado; ?>'
	         +'&numeroidentificacion_postulado=<?php echo $numeroidentificacion_postulado; ?>'
			 +'&fechacorte_postulado=<?php echo $fechacorte_postulado; ?>'
			 +'&periodo=<?php echo $periodo; ?>'		
			 +'&ok=ook';
	         window.location.href=page;
		}
		iniciargiro();
		</script>
		<?php 
	} else {
		echo "<script language='JavaScript'>alert('No se Encontraron Registros que procesar'); location.href='Giro.php'</script>";
	}
	
} elseif ($_GET ['ok'] == 'ook') {	
	$vinculacion_postulados = $_GET ['vinculacion_postulados'];
	$numeroidentificacion_postulado = $_GET ['numeroidentificacion_postulado'];
	$tipo_giropostulado = $_GET ['tipo_giropostulado'];
	$fechacorte_postulado = $_GET ['fechacorte_postulado'];
	$periodo = $_GET ['periodo'];
	
	$sqle = "delete from aportes231 where periodo='" . $periodo . "'";
	$rse = $db->querySimple ( $sqle, 'aportes231' );
	
	$sql = "select aportes221.idradicacion,aportes221.idconvenio,aportes221.estado,aportes221.fecharadicacion,aportes221.vinculacion,aportes015.identificacion,aportes015.pnombre,aportes015.snombre,
	        aportes015.papellido,aportes015.sapellido,aportes015.sexo,aportes221.idconvenio,aportes221.estado,aportes221.idagencia,
						   aportes221.apto,aportes221.idpersona,aportes220.descripcion,aportes220.valoralimentacion, 
						   aportes220.valorsalud,aportes220.valoreducacion, aportes222.idapropiacion,
						   aportes500.agencia	 
                    from   aportes221,aportes015,aportes220,aportes222,aportes500
                    where 1=1			
                    and aportes015.idpersona=aportes221.idpersona 
                    and aportes221.idconvenio=aportes220.idconvenio					
					and aportes221.idagencia=aportes500.codigo 
					and aportes221.vinculacion=aportes222.vinculacion
					and aportes221.estado='G'";
	
	if ($vinculacion_postulados != 0) {
		$sql = $sql . " and aportes221.vinculacion='" . $vinculacion_postulados . "'";
	}
	if ($numeroidentificacion_postulado != '') {
		$sql = $sql . " and aportes015.identificacion='" . $numeroidentificacion_postulado . "'";
	}
	$sql = $sql . " order by especial DESC, aportes221.idradicacion ASC";
	
	$rs = $db->querySimple ( $sql );
	$rs1 = $db->querySimple ( $sql );
	
	$encontrodatos = 0;
	while ( $registro1 = $rs1->fetch () ) {
		++ $encotrodatos;
		$vinculacion = $registro1 ['vinculacion'];
	}
	
	if ($encotrodatos != 0) {
		
		if ($numeroidentificacion_postulado != '') {
			if ($vinculacion == 1) {
				$tituloconosin = "POSTULADO GIRADO SIN VINCULACI&Oacute;N";
			} else if ($vinculacion == 2) {
				$tituloconosin = "POSTULADO GIRADO CON VINCULACI&Oacute;N";
			}
		}
		
		if ($vinculacion_postulados == 1) {
			$tituloconosin = "LISTA DE POSTULADOS GIRADOS SIN VINCULACI&Oacute;N";
		} else if ($vinculacion_postulados == 2) {
			$tituloconosin = "LISTA DE POSTULADOS GIRADOS CON VINCULACI&Oacute;N";
		}
		
		?>
<table height="172" border="0" align="center" cellpadding="0"
		cellspacing="0">
		<tr>
			<td height="29" class="arriba_iz">&nbsp;</td>
			<td class="arriba_ce"><span class="letrablanca">::&nbsp;<strong><?php echo $tituloconosin;?></strong>&nbsp;::
			</span></td>
			<td class="arriba_de">&nbsp;</td>
		</tr>
		<tr>
			<td class="cuerpo_iz">&nbsp;</td>
			<td class="cuerpo_ce"><a href="Giro.php" title="Volver"><img
					src="../imagenes/left_green.png" width="20" height="20" /></a><a
				href="../Excel/Excel_ReporteGirado.php?ok=ok&amp;vinculacion_postulados=<?php echo $_GET['vinculacion_postulados'];?>&amp;
     numeroidentificacion_postulado=<?php echo $_GET['numeroidentificacion_postulado'];?>&amp;  
     tipo_giropostulado=<?php echo $_GET['tipo_giropostulado'];?>&amp; 
     fechacorte_postulado=<?php echo $_GET['fechacorte_postulado'];?>&amp; 
     periodo=<?php echo $_GET['periodo'];?>&amp;         
     contadorgrio=<?php echo $contadorgrio;?>&amp;   
     "
				target="_blank" title="Excel"><img src="../imagenes/excel.png"
					width="20" height="20" /></a>
      <?php $variable_girado = $_GET['vinculacion_postulados'].'*'.$_GET['numeroidentificacion_postulado'].'*'.$_GET['tipo_giropostulado'].'*'.$_GET['fechacorte_postulado'].'*'.$_GET['periodo'].'*'.$contadorgrio;?>
    <img src="../imagenes/pdf.png"
				name="<?php echo $variable_girado; ?>" width="20" height="20"
				id="<?php echo $variable_girado; ?>" title="Generar Pdf"
				onclick="generareportegirado(this.id);" /></td>
			<td class="cuerpo_de">&nbsp;</td>
		</tr>
		<tr>
			<td class="cuerpo_iz">&nbsp;</td>
			<!-- TABLAS DE FORMULARIO -->
			<td class="cuerpo_ce"><center>
					<table border="0" cellspacing="0" class="tablero">
						<tr>
							<td><center>
									<table class="tablero" align="center">
										<tr>
											<th align="center">conse</th>
											<th align="center">N&uacute;mero radicado</th>
											<th align="center">Identificaci&oacute;n</th>
											<th align="center">Nombre y Apellido</th>
											<th align="center">Fecha de Solicitud</th>
											<th align="center">Periodo</th>
											<th align="center">Seccional</th>
											<th align="center">Giro</th>
											<th align="center">Valor Alimentaci&oacute;n</th>
											<th align="center">Valor Salud</th>
											<th align="center">Valor Educaci&oacute;n</th>
											<th align="center">Estado</th>
										</tr>
              
              <?php
		$cont_postulados = 0;
		$Talimento = 0;
		$Tsalud = 0;
		$Teducacion = 0;
		
		while ( $registro = $rs->fetch () ) {
			++ $cont_postulados;
			
			$idradicacion = $registro ['idradicacion'];
			$identificacion = $registro ['identificacion'];
			$nombrecompleto = utf8_encode ( $registro ['pnombre'] . " " . $registro ['snombre'] . " " . $registro ['papellido'] . " " . $registro ['sapellido'] );
			$agencia = utf8_encode ( $registro ['agencia'] );
			
			$valorsalud = $registro ['valorsalud'];
			$valoreducacion = $registro ['valoreducacion'];
			$valoralimentacion = $registro ['valoralimentacion'];
			$idconvenio = $registro ['idconvenio'];
			$idperso = $registro ['idpersona'];
			
			$Talimento = $Talimento + $registro ['valoralimentacion'];
			$Teducacion = $Teducacion + $registro ['valoreducacion'];
			$Tsalud = $Tsalud + $registro ['valorsalud'];
			
			$sqlAp235b = "SELECT COUNT(idpersona) FROM aportes235 where idpersona='" . $idperso . "' and  periodo='" . $periodo . "'";
			$rsAp235b = $db->querySimple ( $sqlAp235b );
			$encontro = $rsAp235b->fetchColumn ();
			
			if ($encontro == 0) {
				$sqlap235 = "insert into aportes235 (idpersona,valorsalud,valoreducacion,valoralimentacion,idconvenio,fechagiro,periodo,usuario,fechasistema) 
			values('" . $idperso . "','" . $valorsalud . "','" . $valoreducacion . "','" . $valoralimentacion . "','" . $idconvenio . "','" . $fechacorte_postulado . "'
			,'" . $periodo . "','" . $usuario . "',cast(GetDate() as date))";
				$rsap235 = $db->queryActualiza ( $sqlap235, 'aportes235' );
			
			} 			// fin if
			else {
				$encontro = $encontro + 1;
			}
			
			$sqlap235b = "select * from aportes235 where idpersona='" . $idperso . "' and periodo='" . $periodo . "'";
			$rsap235b = $db->querySimple ( $sqlap235b );
			while ( $rowap235b = $rsap235b->fetch () ) {
				$fechagiro = $rowap235b ['fechagiro'];
			} // fin while
			
			$sqlap231 = "insert into aportes231 (idpersona,periodo,usuario,fechasistema) values('" . $idperso . "','" . $periodo . "','" . $usuario . "',cast(GetDate() as date))";
			$rsap231 = $db->queryInsert ( $sqlap231, 'aportes231' );
			
			$contadorgrio = 0;
			$sqlcontadorgrio = "select * from aportes225 where idpersona ='" . $idperso . "' and fechagiro is not null and periodo is not null ";
			$rscontadorgrio = $db->querySimple ( $sqlcontadorgrio );
			while ( $row = $rscontadorgrio->fetch () ) {
				++ $contadorgrio;
			}
			if ($contadorgrio == 6) {
				$sqlbandera = "update aportes221 set estado='T',apto='0',usuario='$usuario',fechasistema= cast(GetDate() as date) where idpersona ='" . $idperso . "' ";
				$rsbandera = $db->queryActualiza ( $sqlbandera, 'aportes221' );
			}
			
			?>
              <tr>
											<td align="center"><?php echo $cont_postulados; ?> &nbsp;</td>
											<td align="center"><?php echo $idradicacion; ?>&nbsp;</td>
											<td align="center"><?php echo $identificacion; ?>&nbsp;</td>
											<td align="center"><?php echo $nombrecompleto; ?>&nbsp;</td>
											<td align="center"><?php if($fechagiro!=0){echo $fechagiro;}else{echo $fechacorte_postulado;}?>
                  &nbsp;</td>
											<td align="center"><?php echo $periodo; ?>&nbsp;</td>
											<td align="center"><?php echo $agencia; ?>&nbsp;</td>
											<td align="center"><?php echo $contadorgrio; ?>&nbsp;</td>
											<td align="center"><?php echo number_format($registro['valoralimentacion'], 0, '', '.'); ?>&nbsp;</td>
											<td align="center"><?php echo number_format($registro['valorsalud'], 0, '', '.'); ?>&nbsp;</td>
											<td align="center"><?php echo number_format($registro['valoreducacion'], 0, '', '.'); ?>&nbsp;</td>
											<td align="center"><?php echo $registro['estado']; ?>&nbsp;</td>
										</tr>
              <?php
		} // fin while
		?>
               <tr>
											<td colspan="8" align="center" bgcolor="#F7D358"
												style="border: 0"><strong>TOTAL</strong></td>
											<td align="center" bgcolor="#F7D358" style="border: 0"><strong><?php echo number_format($Talimento, 0, '', '.'); ?>&nbsp;</strong></td>
											<td align="center" bgcolor="#F7D358" style="border: 0"><strong><?php echo number_format($Tsalud, 0, '', '.'); ?>&nbsp;</strong></td>
											<td align="center" bgcolor="#F7D358" style="border: 0"><strong><?php echo number_format($Teducacion, 0, '', '.'); ?>&nbsp;</strong></td>
											<td align="center" bgcolor="#F7D358" style="border: 0">&nbsp;</td>
										</tr>
      <?php
		if ($encontro != 0) {
			echo "<script language='JavaScript'>alert('Periodo de Giro ya Realizado');</script>";
		} // fin if
		if ($numeroidentificacion_postulado != '') {
			
			if ($alertaasocajaspostulado == 'asocajaspostulado') {
				echo "<script language='JavaScript'>alert('Postulado Encontrado en Asocajas');</script>";
			}
			if ($alertafosygapostulado == 'fosygapostulado') {
				echo "<script language='JavaScript'>alert('Postulado Encontrado en Fosyga');</script>";
			}
			if ($alertainactivopostulado == 'inactivo') {
				echo "<script language='JavaScript'>alert('Postulado Activo');</script>";
			}
		}
		?>
            </table>
								</center></td>
						</tr>
					</table>
				</center></td>
			<td class="cuerpo_de"></td>
			<!-- FONDO DERECHA -->
		</tr>
		<tr>
			<td height="41" class="abajo_iz">&nbsp;</td>
			<td class="abajo_ce">&nbsp;</td>
			<td class="abajo_de">&nbsp;</td>
		</tr>
	</table>
	<div align="center"></div>
<?php
	} 	// fin encontrodatos
	else {
		
		if ($vinculacion_postulados == 1) {
			echo "<script language='JavaScript'>alert('No se Encontraron Postulados Sin Vinculacion Para Girar'); location.href='Giro.php'</script>";
		} else if ($vinculacion_postulados == 2) {
			echo "<script language='JavaScript'>alert('No se Encontraron Postulados Con Vinculacion Para Girar'); location.href='Giro.php'</script>";
		} else if ($numeroidentificacion_postulado != '') {
			echo "<script language='JavaScript'>alert('No se Encontro Postulado Para Girar'); location.href='Giro.php'</script>";
		}
	
	}

} // fin if $_POST['ok'] == 'ok'
else {
	?> 
<form name="form_postulado" method="post" action="Postulacion.php">
		<table width="481" border="0" align="center" cellpadding="0"
			cellspacing="0">
			<tr>
				<td height="29" class="arriba_iz">&nbsp;</td>
				<td class="arriba_ce"><span class="letrablanca">::&nbsp;<strong>GIRO</strong>&nbsp;::
				</span></td>
				<td class="arriba_de">&nbsp;</td>
			</tr>
			<tr>
				<td class="cuerpo_iz">&nbsp;</td>
				<td class="cuerpo_ce"><div id="error" style="color: #FF0000"></div></td>
				<td class="cuerpo_de">&nbsp;</td>
			</tr>
			<tr>
				<td class="cuerpo_iz">&nbsp;</td>
				<!-- TABLAS DE FORMULARIO -->
				<td class="cuerpo_ce"><center>
						<table width="367" border="0" cellspacing="0" class="tablero">
							<tr bgcolor="#EBEBEB">
								<td colspan="4" style="text-align: center"></td>
							</tr>
							<tr bgcolor="#EBEBEB">
								<td colspan="4" style="text-align: center"><strong>GIRO</strong></td>
							</tr>
							<tr>
								<td><strong>Girar Por(General o Persona):</strong></td>
								<td colspan="3"><select name="tipo_giropostulado" size="1"
									id="tipo_giropostulado" class="box1"
									onchange="generaloporpersona();">
										<option value="0">::Seleccione::</option>
										<option value="1">General</option>
										<option value="2">Persona</option>
								</select></td>
							</tr>
							<tr>
								<td><strong>Tipo de Vinculaci&oacute;n:</strong></td>
								<td colspan="3"><select name="vinculacion_postulados" size="1"
									id="vinculacion_postulados" class="box1" disabled="disabled">
										<option value="0">::Seleccione::</option>
										<option value="1">Sin Vinculacion</option>
										<option value="2">Con Vinculacion</option>
								</select></td>
							</tr>
							<tr>
								<td><strong>N&uacute;mero de C&eacute;dula:</strong></td>
								<td colspan="3"><input name="numeroidentificacion_postulado"
									type="text" id="numeroidentificacion_postulado"
									onkeydown='solonumeros(this);' onkeyup='solonumeros(this);'
									value="" disabled="disabled" /></td>
							</tr>
							<tr>
								<td><strong>Periodo:</strong></td>
								<td colspan="3"><input name="periodo" type="text" id="periodo"
									onkeydown='solonumeros(this);' onkeyup='solonumeros(this);'
									value="" readonly="readonly" /></td>
							</tr>
							<tr>
								<td colspan="4">&nbsp;
									<center>
										<input name="button" id="button" type="button"
											onclick="if(confirm('Realmente desea realizar el Giro?')){listarpostulado();}"
											value="Generar Giro" />
									</center>
								</td>
							</tr>
						</table>
					</center></td>
				<td class="cuerpo_de"></td>
				<!-- FONDO DERECHA -->
			</tr>
			<tr>
				<td height="41" class="abajo_iz">&nbsp;</td>
				<td class="abajo_ce">&nbsp;</td>
				<td class="abajo_de">&nbsp;</td>
			</tr>
		</table>
		<input name="fechacorte_postulado" type="hidden"
			id="fechacorte_postulado" value="<?php echo date("Y-m-d"); ?>" />
	</form>
<?php
} // fin else if $_POST['ok'] == 'ok'
?>
</body>
</html>