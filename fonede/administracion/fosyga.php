<?php
ini_set('display_errors','Off');
include_once '../../rsc/pdo/IFXDbManejador.php';
include_once '../../rsc/pdo/IFXerror.php';

$db = IFXDbManejador::conectarDB(); 
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Registro Fosyga</title>

<link type="text/css" href="../../css/css.1.8/jquery-ui-1.8.17.custom.css" rel="stylesheet" />
<link type="text/css" href="../../css/Estilos.css" rel="stylesheet"/>
<link type="text/css" rel="stylesheet" href="../../css/marco.css" />

<script language="javascript" src="../../js/1.6/jquery-1.6.2.min.js"></script>
<script language="javascript" src="../../js/1.6/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="../../js/comunes.js"></script>

<script languaje="javascript" src="js/Administracion.js"></script>

</head>

<body>


<form id="form1" name="form1" method="post" action="">
<table border="0" cellspacing="0" cellpadding="0" align="center"> 
<tr>
<td width="13" height="29" class="arriba_iz">&nbsp;</td>
<td class="arriba_ce"><span class="letrablanca">::&nbsp;Registrar Reportados en Fosyga&nbsp;::</span></td>
<td width="13" class="arriba_de" >&nbsp;</td>
</tr>
<tr>     
<td class="cuerpo_iz">&nbsp;</td>
<td class="cuerpo_ce">
<img src="../../imagenes/menu/informacion.png" width="16" height="16" style="border: none; cursor: pointer" title="Manual" onClick="mostrarAyuda();" /> 
<img src="../../imagenes/menu/notas.png" width="16" height="16" style="cursor: pointer" title="Colaboraci&oacute;n en linea" onClick="notas();" /> <img src="../../imagenes/menu/devolver.png" width="16" height="16" style="cursor: pointer" title="Eliminar" onclick="if(confirm('¿Realmente desea eliminar esta persona?')){eliminarfosyga();}" />
<img src="../../imagenes/menu/grabar.png" width="16" height="16" style="cursor: pointer" title="Guardar" onclick="if(confirm('¿Realmente desea registrar esta persona?')){guardarfosyga();}"/>

<div id="error" style="color:#FF0000"></div>
</td>
<td class="cuerpo_de">&nbsp;</td>
</tr>
<tr>
<td class="cuerpo_iz">&nbsp;</td>
<!-- TABLAS DE FORMULARIO -->
<td class="cuerpo_ce">
<center>

     <table border="0" cellspacing="0" class="tablero">
            <tr bgcolor="#EBEBEB">
              <td colspan="4" style="text-align:center" ></td>
            </tr>
            <tr>
              <td width="168"><strong>Identificaci&oacute;n:</strong></td>
              <td width="565"><input name="identificacion" type="text" id="identificacion" onkeydown='solonumeros(this);'
              onkeyup='solonumeros(this);'  value=""/>
              <span class="abajo_ce"><img src="../../imagenes/menu/obligado.png" align="middle"/></span></td>
            </tr>
            <tr>
              <td colspan="4" style="text-align:center" >&nbsp;</td>
            </tr>
          </table>
</center>

<td class="cuerpo_de"></td><!-- FONDO DERECHA -->
<tr>
<td height="41" class="abajo_iz">&nbsp;</td>
<td class="abajo_ce">&nbsp;<font color="#FF0000">Campos Obligatorios</font><img src="../../imagenes/menu/obligado.png" align="middle"/></td>
<td class="abajo_de">&nbsp;</td>
</tr>
</table>
</form>
</body>
</html>