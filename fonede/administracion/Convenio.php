
<?php
ini_set('display_errors','Off');
include_once '../../rsc/pdo/IFXDbManejador.php';
include_once '../../rsc/pdo/IFXerror.php';

$db = IFXDbManejador::conectarDB(); 
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}
?>

<?php
$sqlap220="SELECT * FROM aportes220 ORDER BY descripcion ASC";
$rsap220=$db->querySimple($sqlap220);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Convenio</title>

<link type="text/css" href="../../css/css.1.8/jquery-ui-1.8.17.custom.css" rel="stylesheet" />
<link type="text/css" href="../../css/Estilos.css" rel="stylesheet"/>
<link type="text/css" rel="stylesheet" href="../../css/marco.css" />

<script language="javascript" src="../../js/1.6/jquery-1.6.2.min.js"></script>
<script language="javascript" src="../../js/1.6/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="../../js/comunes.js"></script>

<script languaje="javascript" src="js/Administracion.js"></script>

</head>

<body>


<form id="form1" name="form1" method="post" action="">
<table border="0" cellspacing="0" cellpadding="0" align="center"> 
<tr>
<td width="13" height="29" class="arriba_iz">&nbsp;</td>
<td class="arriba_ce"><span class="letrablanca">::&nbsp;Convenio::</span></td>
<td width="13" class="arriba_de" >&nbsp;</td>
</tr>
<tr>     
<td class="cuerpo_iz">&nbsp;</td>
<td class="cuerpo_ce"><table width="285" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="62" style="border:0"><img src="../../imagenes/menu/informacion.png" width="16" height="16" style="border: none; cursor: pointer" title="Manual" onclick="mostrarAyuda();" /> <img src="../../imagenes/menu/notas.png" width="16" height="16" style="cursor: pointer" title="Colaboración en linea" onclick="notas();" /> <img src="../../imagenes/menu/nuevo.png" width="16" height="16" style="cursor: pointer" title="Nuevo" onclick="limpiar();" /></td>
    <td width="223" height="0" style="border:0"><img id="guardar" src="../../imagenes/menu/grabar.png" width="16" height="16" style=" display:block;cursor: pointer" title="Guardar" onclick="validarconvenio();" /><img id="actualizar" src="../../imagenes/menu/modificar.png" width="16" height="16" style=" display:none;cursor: pointer" title="Modificar" onclick="actualizarconvenio();" /></td>
    </tr>
</table>  <div id="error" style="color:#FF0000"></div>
</td>
<td class="cuerpo_de">&nbsp;</td>
</tr>
<tr>
<td class="cuerpo_iz">&nbsp;</td>
<!-- TABLAS DE FORMULARIO -->
<td class="cuerpo_ce">
<center>

     <table border="0" cellspacing="0" class="tablero">
            <tr bgcolor="#EBEBEB">
              <td colspan="4" style="text-align:center" ></td>
            </tr>
            <tr>
              <td>Buscar Convenio:</td>
              <td colspan="3"><select name="BuscarConvenio" id="BuscarConvenio" class="boxmediano" onChange="buscarconvenio();">
                <option value="0">::Seleccione::</option>
                <?php
		  		   while($rowap220=$rsap220->fetch()){
 			   		 echo "<option value=".$rowap220['idconvenio']."*".$rowap220['codigo'].">".$rowap220['descripcion']." ".$rowap220['vigencia']."</option>";
	           	   }// fin while
             	?>
              </select></td>
            </tr>
            <tr bgcolor="#EBEBEB">
              <td colspan="4" style="text-align:center" >&nbsp;</td>
            </tr>
            <tr>
              <td>N&uacute;mero Convenio:</td>
              <td><input name="CodigoConvenio" type="text" id="CodigoConvenio" onkeydown='solonumeros(this);' onkeyup='solonumeros(this);'/>
              <img src="../../imagenes/menu/obligado.png" align="middle"/></td>
              <td>Descripci&oacute;n:</td>
              <td><input name="NombreConvenio" type="text" id="NombreConvenio" size="32" value="" onKeyDown='sololetras(this);' onKeyUp='sololetras(this);'/>
              <img src="../../imagenes/menu/obligado.png" align="middle"/></td>
            </tr>
            <tr>
              <td>Valor Alimento:</td>
              <td><input name="ValorAlimento" type="text" id="ValorAlimento" onkeydown='solonumeros(this);' onkeyup='solonumeros(this);'/>
              <img src="../../imagenes/menu/obligado.png" align="middle"/></td>
              <td>Valor Salud:</td>
              <td><input name="ValorSalud" type="text" id="ValorSalud" onkeydown='solonumeros(this);' onkeyup='solonumeros(this);'/>
              <img src="../../imagenes/menu/obligado.png" align="middle"/></td>
            </tr>
            <tr>
                    
              <td width="133">Valor Educaci&oacute;n:</td>
              <td width="279"><input name="ValorEducacion" type="text" id="ValorEducacion" onkeydown='solonumeros(this);' onkeyup='solonumeros(this);'/>
              <img src="../../imagenes/menu/obligado.png" align="middle"/></td>
              <td width="121">Vigencia:</td>
              <td width="294"><select name="VigenciaConvenio" id="VigenciaConvenio" class="box1">
                <option value="0">::Seleccione::</option>
					<?php
                    $tope = date("Y");
                    $edad_max = $tope-2005;
                    $edad_min = 0;
                    for($a= $tope - $edad_max; $a<=$tope - $edad_min; $a++)
                       {
                         echo "<option value='$a'>$a</option>"; 		
                       }	
                    ?>
            </select><img src="../../imagenes/menu/obligado.png" align="middle"/></td>
            </tr>
          </table>
</center>

<td class="cuerpo_de"></td><!-- FONDO DERECHA -->
<tr>
<td height="41" class="abajo_iz">&nbsp;</td>
<td class="abajo_ce">&nbsp;<font color="#FF0000">Campos Obligatorios</font><img src="../../imagenes/menu/obligado.png" align="middle"/></td>
<td class="abajo_de">&nbsp;</td>
</tr>
</table>
</form>
</body>
</html>