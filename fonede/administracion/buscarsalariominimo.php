<?php
ini_set('display_errors','On');
date_default_timezone_set("America/Bogota");
include_once '../../rsc/pdo/IFXDbManejador.php';
include_once '../../rsc/pdo/IFXerror.php'; 

$db = IFXDbManejador::conectarDB(); 
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();	
}// fin if


$anoactual=date('Y');
$mesactual=date('m');
$sql="SELECT * FROM aportes012 where periodo=$anoactual$mesactual";
$rs=$db->querySimple($sql);

$con=0;
$filas=array();
while ($row= $rs->fetch()){
	$filas[]= $row;
	$con++;	
}// fin while
if($con==0){
	echo 0;
	exit();
}// fin if

echo json_encode($filas);

?>