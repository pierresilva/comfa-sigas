// JavaScript Document
var error=0;

function setCookie(c_name,value,exdays)
{
	var exdate=new Date();
	exdate.setDate(exdate.getDate() + exdays);
	var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
	document.cookie=c_name + "=" + c_value;
}// fin setCookie

// funcion para obtener cookie en javascript
	function getCookie(c_name)
	{
		var i,x,y,ARRcookies=document.cookie.split(";");
		for (i=0;i<ARRcookies.length;i++)
		{
  			x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
  			y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
  			x=x.replace(/^\s+|\s+$/g,"");
  			if (x==c_name)
    			{
    				return unescape(y);
    			}// fin if
		}// fin for
  	}// fin getCookie
//funcion para obtener cookie en javascript



function refrescar(){	
      limpiarCampos();	
	  location.reload(); 	  
    }// fin refrescar
	
function limpiar(){	
      limpiarCampos();	
	  location.reload();
	  
	  document.getElementById("ValorAlimento").disabled = false;
	  document.getElementById("ValorSalud").disabled = false;
	  document.getElementById("ValorEducacion").disabled = false;	 	
	    
    }// fin limpiar
	
function borrarcookiecursos()
{
	setCookie('idapropiacion','',-1);	
	limpiarCampos();	
	location.reload();		
}
//------------------------------------- Apropiaciones-----------------------------------------------------------------------------------------------//
function buscarsalariominimo(){
	    $.getJSON('buscarsalariominimo.php',function(datos){	
			  $.each(datos,function(i,fila){
				  var salariominimo=fila.smlv;				  
				$("#salariominimo_apropiaciones").val(parseInt(salariominimo));
				return;
			});		
		})		
	}// fin buscarsalariominimo
	
function buscarapropiacion(){
		
		if($("#buscarapropiaciones").val()=='0'){
			document.getElementById('guardar').style.display='block';	
			document.getElementById('actualizar').style.display='none';		
			limpiarCampos();			
     		return
		}// fin if
		
		var numperiodo = $("#buscarapropiaciones").val();
		arreglo = numperiodo.split('*');
	    periodo=arreglo[0];
		//alert("periodo="+periodo);
	    vigencia=arreglo[1];
		vinculacion=arreglo[2];	
				
		$.getJSON('buscarapropiaciones.php',{v0:periodo,v1:vigencia,v2:vinculacion},function(datos){
			if (datos==0){				
				limpiarCampos();								
				return false;
			}// fin if			
			$.each(datos,function(i,fila){	
			    $("#periodo_apropiaciones").val(fila.idperiodo);			
				$("#vigencia_apropiaciones").val(fila.vigencia);
				$("#descripcion_apropiaciones").val(fila.descripcion);
				$("#apropiacion_apropiaciones").val(fila.apropiacion);								
				$("#diferencia_apropiacion").val(fila.diferencia);
				$("#sostenimiento_apropiacion").val(fila.sostenimiento);				
				$("#administracion_apropiacion").val(fila.gastosadministrativos);
				$("#cociente_apropiacion").val(fila.cociente);
				$("#disponible_apropiaciones").val(fila.totaldisponible);			
				$("#tipovinculacion_apropiaciones").val(fila.vinculacion);
				$("#salariominimo_apropiaciones").val(fila.salariominimo);				
				document.getElementById('descripcion_apropiaciones').focus();													
				return;
			});
			if(datos==0)
			{
			    document.getElementById('guardar').style.display='block';
				document.getElementById('actualizar').style.display='none';	
			}
			else
			{
				document.getElementById('guardar').style.display='none';
				document.getElementById('actualizar').style.display='block';	
				document.getElementById('ms').style.display='none';
			}
		})
		
	}// fin buscarapropiacion 
	

function validarapropiacion(){

	$("table.tablero :input.ui-state-error,select.ui-state-error").removeClass("ui-state-error");//Limpiar la clase de error
	
	var vigenciaapropiaciones=$("#vigencia_apropiaciones").val(); 
	if($("#vigencia_apropiaciones").val()=='0')	{
		$("#vigencia_apropiaciones").addClass("ui-state-error");
		error++;
		}// fin if
	var descripcionapropiaciones=$("#descripcion_apropiaciones").val(); 
	if($("#descripcion_apropiaciones").val()=='')	{
		$("#descripcion_apropiaciones").addClass("ui-state-error");
		error++;
		}// fin if
	var apropiacionapropiaciones=$("#apropiacion_apropiaciones").val(); 
	if($("#apropiacion_apropiaciones").val()=='')	{
		$("#apropiacion_apropiaciones").addClass("ui-state-error");
		error++;
		}// fin if	 
	var periodoapropiaciones=$("#periodo_apropiaciones").val();
	if($("#periodo_apropiaciones").val()=='0')	{
		$("#periodo_apropiaciones").addClass("ui-state-error");
		error++;
		}// fin if 
	var diferencia=$("#diferencia_apropiacion").val(); 
	var sostenimiento=$("#sostenimiento_apropiacion").val(); 
	var administracion=$("#administracion_apropiacion").val();	
	var cociente=$("#cociente_apropiacion").val();		
	var disponibleapropiaciones=$("#disponible_apropiaciones").val(); 
	var salariominimoapropiaciones=$("#salariominimo_apropiaciones").val(); 
	if($("#salariominimo_apropiaciones").val()=='')	{
		$("#salariominimo_apropiaciones").addClass("ui-state-error");
		error++;
		}// fin if 
	
	var tipovinculacionapropiaciones=$("#tipovinculacion_apropiaciones").val(); 
	if($("#tipovinculacion_apropiaciones").val()=='0')	{
		$("#tipovinculacion_apropiaciones").addClass("ui-state-error");
		error++;
		}// fin if 

    var consecutivo=$("#consecutivo").val();
		
	
	if(vigenciaapropiaciones!=0 && descripcionapropiaciones!="" && apropiacionapropiaciones!="" &&
	   periodoapropiaciones!=0 && salariominimoapropiaciones!="" && tipovinculacionapropiaciones!=0)
		{
		$.getJSON('guardarapropiaciones.php',{v0:vigenciaapropiaciones,v1:descripcionapropiaciones,v2:apropiacionapropiaciones
					,v3:periodoapropiaciones,v4:diferencia,v5:sostenimiento,v6:administracion,v7:cociente,v8:disponibleapropiaciones
					,v9:salariominimoapropiaciones,v10:tipovinculacionapropiaciones,v11:consecutivo},function(datos){
			
			if(datos==0)
			{
				alert("El Registro ya existe");
				document.getElementById('ms').style.display='block';
			
		    }// fin if
			else
			{
			alert("Registro guardado");
			//alert("Registro guardado numero id: " + datos);
			//setTimeout('document.location.reload()',2500);
			location.reload();
			limpiarCampos();
			}
		
		})
	}// fin if
	else
	{
		alert("Falta Completar Datos");
	}// fin else
		 
 }// fin validarapropiacion		

function actualizarapropiacion(){
	
		$("table.tablero :input.ui-state-error,select.ui-state-error").removeClass("ui-state-error");//Limpiar la clase de error
		
		var numperiodo = $("#buscarapropiaciones").val();
		var vigenciaapropiaciones=$("#vigencia_apropiaciones").val(); 
		if($("#vigencia_apropiaciones").val()=='0')	{
			$("#vigencia_apropiaciones").addClass("ui-state-error");
			error++;
			}// fin if
		var descripcionapropiaciones=$("#descripcion_apropiaciones").val(); 
		if($("#descripcion_apropiaciones").val()=='')	{
			$("#descripcion_apropiaciones").addClass("ui-state-error");
			error++;
			}// fin if
		var apropiacionapropiaciones=$("#apropiacion_apropiaciones").val(); 
		if($("#apropiacion_apropiaciones").val()=='')	{
			$("#apropiacion_apropiaciones").addClass("ui-state-error");
			error++;
			}// fin if	 
		var periodoapropiaciones=$("#periodo_apropiaciones").val();
		if($("#periodo_apropiaciones").val()=='0')	{
			$("#periodo_apropiaciones").addClass("ui-state-error");
			error++;
			}// fin if 
		var diferencia=$("#diferencia_apropiacion").val(); 
		var sostenimiento=$("#sostenimiento_apropiacion").val(); 
		var administracion=$("#administracion_apropiacion").val();	
		var cociente=$("#cociente_apropiacion").val();		
		var disponibleapropiaciones=$("#disponible_apropiaciones").val(); 
		var salariominimoapropiaciones=$("#salariominimo_apropiaciones").val(); 
		if($("#salariominimo_apropiaciones").val()=='')	{
			$("#salariominimo_apropiaciones").addClass("ui-state-error");
			error++;
			}// fin if 
		
		var tipovinculacionapropiaciones=$("#tipovinculacion_apropiaciones").val(); 
		if($("#tipovinculacion_apropiaciones").val()=='0')	{
			$("#tipovinculacion_apropiaciones").addClass("ui-state-error");
			error++;
			}// fin if 
		
		arreglo = numperiodo.split('*');
		periodo=arreglo[0];
	    vigencia=arreglo[1];
		vinculacion=arreglo[2];	
		
	if(vigenciaapropiaciones!=0 && descripcionapropiaciones!="" && apropiacionapropiaciones!="" &&
	   periodoapropiaciones!=0 && salariominimoapropiaciones!="" && tipovinculacionapropiaciones!=0)
	   		{
		
		$.getJSON('actualizarapropiaciones.php',{v0:vigenciaapropiaciones,v1:descripcionapropiaciones,v2:apropiacionapropiaciones
					,v3:periodoapropiaciones,v4:diferencia,v5:sostenimiento,v6:administracion,v7:cociente,v8:disponibleapropiaciones
					,v9:salariominimoapropiaciones,v10:tipovinculacionapropiaciones,vp:periodo,vv:vigencia,vvin:vinculacion},function(datos){
			
			if(datos==0)
			{
				alert("No se pudo actualizar los datos por que el total de subsidos ya fue asignado");
			
		    }// fin if	
			else
			if(datos>0)
			{		
			alert("Registro actualizado");
			//alert("Registro guardado numero id: " + datos);
			//setTimeout('document.location.reload()',1500);
			location.reload();
		    limpiarCampos();
			}
		})
		}// fin if
		else
		{
			alert("Falta Completar Datos");
		}// fin else
					
		}// fin actualizarapropiacion
 
	
	
//-----------------------------------------------------------------------------------------------------------------------------------------------------//
//------------------------------------- Curso----------------------------------------------------------------------------------------------------------//	
	function guardarrelacioncursadministrador(){
		$("table.tablero :input.ui-state-error,select.ui-state-error").removeClass("ui-state-error");//Limpiar la clase de error
		
		var valorcurso = new Array();					
		var conse=$("#conse").val();
		//alert("conse= "+conse);
											
		var controlcurso=0;				
		for (x=0; x <= conse; x = x+1)
		  {		
		var periodo = $("#periodo").val();	  
		var idcurso = $("#curso"+x).val();	
		var check = $('[name="check'+x+'"]:checked').val();	

	 
		//alert("identificacion="+identificacion);
		//alert("seleccionar="+seleccionar);
		
		if(check!=undefined && check!="")
		  {
			 controlcurso=controlcurso+1;
			 valorcurso[controlcurso-1]=idcurso;
			 //alert(valorcurso[x-1]);
			 var contar=x;
			 //alert("contar= "+contar);
			 			 
		   }//fin if identificacion	
      }// fin for		  
	  
	  if($("#periodo").val()=='0')	{
		$("#periodo").addClass("ui-state-error");
		var mensaje="y el periodo";
		error++;
		}// fin if
		if(contar==undefined)
		{
			document.getElementById('ms1').style.display='block';
		}	
		else
		{
			document.getElementById('ms1').style.display='none';
		}
	   
	   if(valorcurso[controlcurso-1]!=undefined && valorcurso[controlcurso-1]!="" && periodo!=0 && contar!=undefined)
		{
			
    	$.getJSON('GuardarCursoAdministrador.php',{v0:valorcurso[controlcurso-1],v1:conse,periodo:periodo,valorcurso:valorcurso},function(datos){
			
		})
		
			alert("Cursos Guardados");	
			borrarcookiecursos();
			
		}// fin if	
		else	
			{	
			if(mensaje==undefined)	
				{
				   alert("Seleccione al menos un curso a guardar");		
				}
			else
				{
				   alert("Seleccione al menos un curso a guardar " + mensaje);
				}
			}// fin else	
		
		
	
	  
	}// fin guardarrelacioncursadministrador
	
	
	function buscarperiodo(){
		
		var periodo = $("#periodo").val();
		if($("#periodo").val()=='0')	{		
			document.getElementById('listacursos').style.display='none';
			setCookie('idapropiacion','',-1);				 					
     		return
		}// fin if
		if(periodo!=0)
		{
			
			$.getJSON('buscarperiodocursos.php',{v0:periodo},function(datos){
				if (datos==0){
					alert("Datos no encontrados");
					limpiarCampos();
					return false;
				}// fin if
				$.each(datos,function(i,fila){	
					setCookie("idapropiacion", fila.idapropiacion, 1);	
					$("#listacursos").load('ListadecursosPorApropiacion.php');	
					document.getElementById('listacursos').style.display='block';		 
					return;
				});
			})
		
		}// fin if
		
	}// fin buscarperiodo 
	
	
	
	
	
	
	
//-----------------------------------------------------------------------------------------------------------------------------------------------------//
//-------------------------------------Fosyga----------------------------------------------------------------------------------------------------------//

function guardarfosyga(){

	$("table.tablero :input.ui-state-error,select.ui-state-error").removeClass("ui-state-error");//Limpiar la clase de error
	
	var identificacion=$("#identificacion").val(); 
	if($("#identificacion").val()=='')	{
		$("#identificacion").addClass("ui-state-error");
		error++;
		}// fin if		
	
	if(identificacion!='')
		{
		$.getJSON('guardarfosyga.php',{v0:identificacion},function(datos){
			
			if(datos==0)
			{
				alert("El Registro ya existe");
			
		    }// fin if	
			else
			{	
				alert("Registro guardado");			
				limpiarCampos();
			}
		})
	}// fin if
	else
	{
		alert("Falta Completar Datos");
	}// fin else
		 
 }// fin guardarfosyga		
 
 function eliminarfosyga(){

	$("table.tablero :input.ui-state-error,select.ui-state-error").removeClass("ui-state-error");//Limpiar la clase de error
	
	var identificacion=$("#identificacion").val(); 
	if($("#identificacion").val()=='')	{
		$("#identificacion").addClass("ui-state-error");
		error++;
		}// fin if		
	
	if(identificacion!='')
		{
		$.getJSON('eliminarfosyga.php',{v0:identificacion},function(datos){
			
			if(datos!=0)
			{
				alert("Registro Eliminado");			
				limpiarCampos();
			
		    }// fin if	
			
		})
	}// fin if
	else
	{
		alert("Falta Completar Datos");
	}// fin else
		 
 }// fin eliminarfosyga		

//-----------------------------------------------------------------------------------------------------------------------------------------------------//
//------------------------------------- Convenio------------------------------------------------------------------------------------------------------//
function buscarconvenio(){
		
	    if($("#BuscarConvenio").val()=='0'){
			document.getElementById('guardar').style.display='block';	
			document.getElementById('actualizar').style.display='none';		
			limpiarCampos();			
     		return
		}// fin if
		var numconvenio = $("#BuscarConvenio").val();
	    arreglo = numconvenio.split('*');	
		id=arreglo[0];			
	   	
		$.getJSON('buscarconvenio.php',{v0:id},function(datos){
			if (datos==0){
			    limpiarCampos();	
				return false;
			}// fin if
			$.each(datos,function(i,fila){
				$("#CodigoConvenio").val(fila.codigo);	
				$("#NombreConvenio").val(fila.descripcion);	
				$("#ValorAlimento").val(fila.valoralimentacion);
				$("#ValorSalud").val(fila.valorsalud);
				$("#ValorEducacion").val(fila.valoreducacion);
				$("#VigenciaConvenio").val(fila.vigencia);
					
				//alert("va="+fila.valoralimentacion);
				//alert("vs="+fila.valorsalud);
				//alert("ve="+fila.valoreducacion);
				
				
				if($("#BuscarConvenio").val()!=0)	
				{
				 document.getElementById("ValorAlimento").disabled = true;
				 document.getElementById("ValorSalud").disabled = true;
				 document.getElementById("ValorEducacion").disabled = true;	
				}									
				if($("#ValorAlimento").val()!=0)
				{
				 document.getElementById("ValorAlimento").disabled = false;						
				}
				if($("#ValorSalud").val()!=0)
				{
				 document.getElementById("ValorSalud").disabled = false;						
				}
			    if($("#ValorEducacion").val()!=0)
				{
				 document.getElementById("ValorEducacion").disabled = false;						
				}										
				if($("#ValorAlimento").val()!=0 && $("#ValorSalud").val()!=0)
				{
				 document.getElementById("ValorAlimento").disabled = false;
				 document.getElementById("ValorSalud").disabled = false;				 						
				}
															
				if($("#ValorAlimento").val()!=0 && $("#ValorEducacion").val()!=0)
				{
				 document.getElementById("ValorAlimento").disabled = false;				 
				 document.getElementById("ValorEducacion").disabled = false;			 				
				}
				if($("#ValorAlimento").val()!=0 && $("#ValorSalud").val()!=0 && $("#ValorEducacion").val()!=0)
				{
				 document.getElementById("ValorAlimento").disabled = false;
				 document.getElementById("ValorSalud").disabled = false;
				 document.getElementById("ValorEducacion").disabled = false;
				}	
			    if($("#ValorSalud").val()!=0 && $("#ValorEducacion").val()!=0)
				{				 
				 document.getElementById("ValorSalud").disabled = false;
				 document.getElementById("ValorEducacion").disabled = false;					 
				}	
										
				return;
			});
			if(datos==0)
			{
			    document.getElementById('guardar').style.display='block';
				document.getElementById('actualizar').style.display='none';	
			}
			else
			{
				document.getElementById('guardar').style.display='none';
				document.getElementById('actualizar').style.display='block';				
			}
		})
		
	}// fin buscarconvenio 
	
	
function validarconvenio(){

	$("table.tablero :input.ui-state-error,select.ui-state-error").removeClass("ui-state-error");//Limpiar la clase de error
	
	var codigoconvenio=$("#CodigoConvenio").val(); 
	if($("#CodigoConvenio").val()=='')	{
		$("#CodigoConvenio").addClass("ui-state-error");
		error++;
		}// fin if
	var nombreconvenio=$("#NombreConvenio").val(); 
	if($("#NombreConvenio").val()=='')	{
		$("#NombreConvenio").addClass("ui-state-error");
		error++;
		}// fin if
	var valoralimento=$("#ValorAlimento").val(); 
	var valorsalud=$("#ValorSalud").val(); 
	var valoreducacion=$("#ValorEducacion").val(); 
	
	if($("#ValorAlimento").val()=='' || $("#ValorSalud").val()=='' || $("#ValorEducacion").val()=='' )	{
		$("#ValorAlimento").addClass("ui-state-error");
		$("#ValorSalud").addClass("ui-state-error");
		$("#ValorEducacion").addClass("ui-state-error");		
		error++;
		}// fin if
		else
		{
			$("#ValorAlimento").removeClass("ui-state-error");
			$("#ValorSalud").removeClass("ui-state-error");
			$("#ValorEducacion").removeClass("ui-state-error");				
		}	
		
	var vigenciaconvenio=$("#VigenciaConvenio").val(); 
	if($("#VigenciaConvenio").val()=='0')	{
		$("#VigenciaConvenio").addClass("ui-state-error");
		error++;
		}// fin if
		
	
	if(codigoconvenio!="" && nombreconvenio!="" && (valoralimento!="" || valorsalud!="" || valoreducacion!="") && vigenciaconvenio!=0)
		{
		$.getJSON('guardarconvenio.php',{v0:codigoconvenio,v1:nombreconvenio,v2:valoralimento
		          ,v3:valorsalud,v4:valoreducacion,v5:vigenciaconvenio},function(datos){
			
			if(datos==0)
			{
				alert("No se pudo guardar los datos");
			
		    }// fin if	
			else
			if(datos>0)	
			{
			    alert("El Registro ya Existe");
			}
			else
			{
			    alert("Registro guardado");
			//alert("Registro guardado numero id: " + datos);
			//setTimeout('document.location.reload()',2500);
			//location.reload();
			limpiarCampos();
			}
		})
	}// fin if
	else
	{
		alert("Falta Completar Datos");
	}// fin else
		 
 }// fin validarconvenio
 
function actualizarconvenio(){
	
	$("table.tablero :input.ui-state-error,select.ui-state-error").removeClass("ui-state-error");//Limpiar la clase de error
		
		
		var buscarconvenio=$("#BuscarConvenio").val(); 
		if($("#BuscarConvenio").val()=='0')	{
			$("#BuscarConvenio").addClass("ui-state-error");
			error++;
			}// fin if
			
		arreglo = buscarconvenio.split('*');
		id=arreglo[0];		
		codigoconvenio=arreglo[1];	
			
		var codigoconvenio=$("#CodigoConvenio").val(); 
		if($("#CodigoConvenio").val()=='')	{
			$("#CodigoConvenio").addClass("ui-state-error");
			error++;
			}// fin if
		var nombreconvenio=$("#NombreConvenio").val(); 
		if($("#NombreConvenio").val()=='')	{
			$("#NombreConvenio").addClass("ui-state-error");
			error++;
			}// fin if
		var valoralimento=$("#ValorAlimento").val(); 
		var valorsalud=$("#ValorSalud").val(); 
		var valoreducacion=$("#ValorEducacion").val(); 
		
		if($("#ValorAlimento").val()=='' || $("#ValorSalud").val()=='' || $("#ValorEducacion").val()=='' )	{
			$("#ValorAlimento").addClass("ui-state-error");
			$("#ValorSalud").addClass("ui-state-error");
			$("#ValorEducacion").addClass("ui-state-error");		
			error++;
			}// fin if
			else
			{
				$("#ValorAlimento").removeClass("ui-state-error");
				$("#ValorSalud").removeClass("ui-state-error");
				$("#ValorEducacion").removeClass("ui-state-error");				
			}	
			
		var vigenciaconvenio=$("#VigenciaConvenio").val(); 
		if($("#VigenciaConvenio").val()=='0')	{
			$("#VigenciaConvenio").addClass("ui-state-error");
			error++;
			}// fin if			
			
	if(codigoconvenio!="" && nombreconvenio!="" && (valoralimento!="" || valorsalud!="" || valoreducacion!="") && vigenciaconvenio!=0)
		{
		
		$.getJSON('actualizarconvenio.php',{v:id,v0:codigoconvenio,v1:nombreconvenio,v2:valoralimento
		          ,v3:valorsalud,v4:valoreducacion,v5:vigenciaconvenio},function(datos){
			
			if(datos==0)
			{
				alert("No se pudo actualizar los datos");
			
		    }// fin if			
			alert("Registro actualizado");
			//alert("Registro guardado numero id: " + datos);
			//setTimeout('document.location.reload()',1500);
			location.reload();
		    limpiarCampos();
		})
		}// fin if
		else
		{
			alert("Falta Completar Datos");
		}// fin else
					
		}// fin actualizarconvenio		

//---------------------------------------------------------------------------------------------------------------------------------------------------//	
