<?php
ini_set('display_errors','Off');
include_once '../../rsc/pdo/IFXDbManejador.php';
include_once '../../rsc/pdo/IFXerror.php';

$db = IFXDbManejador::conectarDB(); 
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();	
}

?>  

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Administraci&oacute;n Cursos</title>
<link type="text/css" href="../../css/css.1.8/jquery-ui-1.8.17.custom.css" rel="stylesheet" />
<link type="text/css" rel="stylesheet" href="../../css/marco.css" />

<link href="../../css/Estilos.css" rel="stylesheet" type="text/css" />
<link type="text/css" href="../../css/marco.css" rel="stylesheet"/>
<script language="javascript" src="../../js/1.6/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="../../js/comunes.js"></script>

<script languaje="javascript" src="js/Administracion.js"></script>


</head>
<body>

		<?php
		  $sql_combo="SELECT distinct  aportes222.idapropiacion,aportes222.idperiodo,aportes223.descripcionperiodo,aportes222.vigencia
							FROM aportes221,aportes222,aportes223,aportes226,aportes500
							where 1=1 and aportes500.codigo=aportes221.idagencia 
							and aportes222.idperiodo=aportes223.idperiodo 
							and aportes221.idapropiacion=aportes222.idapropiacion
							and aportes222.idapropiacion=aportes226.idapropiacion
							and aportes221.proceso='As'
							and aportes222.vinculacion=2";
						
        	$rs_combo=$db->querySimple($sql_combo);	
        ?>



<form name="formadm" id="formadm" method="post" action="">
  <center>
    <table width="95%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td class="arriba_iz" >&nbsp;</td>
        <td class="arriba_ce" ><span class="letrablanca">::Administraci&oacute;n Cursos::</span></td>
        <td class="arriba_de" >&nbsp;</td>
      </tr>
      <tr>
        <td class="cuerpo_iz">&nbsp;</td>
        <td class="cuerpo_ce"> <img src="../../imagenes/menu/nuevo.png" width="16" height="16" style="cursor: pointer" title="Nuevo" onclick="borrarcookiecursos();" /> <img src="../../imagenes/menu/grabar.png" width="16" height="16" style="cursor: pointer" title="Guardar" onclick="guardarrelacioncursadministrador();" />      
          <div id="ms1" style="display:none"><strong>&iexcl;</strong><font color="#FF0000"> Es necesario seleccionar al menos un curso</font><strong>!</strong></div>&nbsp;
        <div id="error" style="color:#FF0000"></div></td>
        <td class="cuerpo_de">&nbsp;</td>
      </tr>
      <tr></tr>
      <tr>
        <td class="cuerpo_iz" >&nbsp;</td>
        <td class="cuerpo_ce" ><table width="90%" border="0" align="center" cellspacing="0" class="tablero">
          <tr>
            <td colspan="4" align="center"><strong>Periodo: </strong>
              <select name="periodo" size="1" id="periodo" onchange="buscarperiodo();">
  <option value="0">::Seleccione::</option>
  <?php
          while($row_combo=$rs_combo->fetch()){		  		  
             echo "<option value='".$row_combo['idperiodo']."'>".$row_combo['descripcionperiodo']."/".$row_combo['vigencia']."</option>";
                   }// fin while
				   
                   ?>
                   
</select></td>
          </tr>
          <tr>
            <td colspan="4" align="center"><div id="listacursos" style="display:block"></div></td>             
          </tr>         
        </table></td>
        <td class="cuerpo_de" >&nbsp;</td>
      </tr>
      <tr></tr>
      <tr>
        <td class="cuerpo_iz" >&nbsp;</td>
        <td class="cuerpo_ce" >&nbsp;
        </td>
        <td class="cuerpo_de" >&nbsp;</td>
      </tr>
      <tr>
        <td class="abajo_iz" >&nbsp;</td>
        <td class="abajo_ce" >&nbsp;</td>
        <td class="abajo_de" >&nbsp;</td>
      </tr>
    </table>
  </center>
</form>


</body>
</html>
