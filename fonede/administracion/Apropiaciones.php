
<?php
ini_set('display_errors','Off');
include_once '../../rsc/pdo/IFXDbManejador.php';
include_once '../../rsc/pdo/IFXerror.php';
include_once 'consecutivoapropiacion.php';

$db = IFXDbManejador::conectarDB(); 
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}
?>

<?php
$sql="SELECT * FROM aportes223";
$rs=$db->querySimple($sql);
?>
<?php
$sqlap222="SELECT * FROM aportes222";
$rsap222=$db->querySimple($sqlap222);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Apropiaciones</title>

<link type="text/css" href="../../css/css.1.8/jquery-ui-1.8.17.custom.css" rel="stylesheet" />
<link type="text/css" href="../../css/Estilos.css" rel="stylesheet"/>
<link type="text/css" rel="stylesheet" href="../../css/marco.css" />

<script language="javascript" src="../../js/1.6/jquery-1.6.2.min.js"></script>
<script language="javascript" src="../../js/1.6/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="../../js/comunes.js"></script>

<script languaje="javascript" src="js/Administracion.js"></script>

<script type="text/javascript">
function Sumar(){
      interval = setInterval("calcular()",1);
}
function calcular(){	
	
	  
	  diferencia= document.form1.diferencia_apropiacion.value;
	  sostenimiento = document.form1.sostenimiento_apropiacion.value; 
	  administracion = document.form1.administracion_apropiacion.value;
	  cociente = document.form1.cociente_apropiacion.value;	  
	  apropiacion=document.form1.apropiacion_apropiaciones.value;
	  
	  suma=(diferencia * 1) + (sostenimiento * 1) + (administracion * 1) + (cociente * 1);	 
	  	   
	  total=(apropiacion * 1) - (suma * 1);
	  document.form1.disponible_apropiaciones.value = total;
	  	  	   
}
function NoSumar(){
      clearInterval(interval);
}

</script>
<script>
 buscarsalariominimo();
</script>
</head>

<body>


<form id="form1" name="form1" method="post" action="">
<table border="0" cellspacing="0" cellpadding="0" align="center"> 
<tr>
<td width="13" height="29" class="arriba_iz">&nbsp;</td>
<td class="arriba_ce"><span class="letrablanca">::&nbsp;Apropiaciones&nbsp;::</span></td>
<td width="13" class="arriba_de" >&nbsp;</td>
</tr>
<tr>     
<td class="cuerpo_iz">&nbsp;</td>
<td class="cuerpo_ce"><table width="285" border="0" cellspacing="0" cellpadding="0">
  <tr>
      <td width="62" style="border:0"><img src="../../imagenes/menu/informacion.png" width="16" height="16" style="border: none; cursor: pointer" title="Manual" onclick="mostrarAyuda();" /> <img src="../../imagenes/menu/notas.png" width="16" height="16" style="cursor: pointer" title="Colaboración en linea" onclick="notas();" /> <img src="../../imagenes/menu/nuevo.png" width="16" height="16" style="cursor: pointer" title="Nuevo" onclick="refrescar();" /> </td>
      <td width="223" height="0" style="border:0"><img id="guardar" src="../../imagenes/menu/grabar.png" width="16" height="16" style=" display:block;cursor: pointer" title="Guardar" onclick="validarapropiacion();" /><img id="actualizar" src="../../imagenes/menu/modificar.png" width="16" height="16" style="display:none;cursor: pointer" title="Modificar" onclick="actualizarapropiacion();" /></td>
      </tr>
</table>
<div id="error" style="color:#FF0000"></div>
</td>
<td class="cuerpo_de">&nbsp;</td>
</tr>
<tr>
<td class="cuerpo_iz">&nbsp;</td>
<!-- TABLAS DE FORMULARIO -->
<td class="cuerpo_ce">
<center>

     <table border="0" cellspacing="0" class="tablero">
            <tr bgcolor="#EBEBEB">
              <td colspan="4" style="text-align:center" > <div id="ms" style="display:none"><strong>&iexcl;</strong><font color="#FF0000"> Solo se puede guardar dos apropiaciones por periodo con tipo de vinculaci&oacute;n distintos </font><strong>!</strong></div>
              <input name="consecutivo" type="hidden" class="box" id="consecutivo" value="<?php echo $consecutivo_completo; ?>"/>                &nbsp;</td>
            </tr>
            <tr>
              <td>Buscar Apropiaci&oacute;n:</td>
              <td colspan="3"><select name="buscarapropiaciones" id="buscarapropiaciones" class="boxmediano" onchange="buscarapropiacion();">
                <option value="0">::Seleccione::</option>
                <?php
		  		   while($rowap222=$rsap222->fetch()){
 			   		 echo "<option value=".$rowap222['idperiodo']."*".$rowap222['vigencia']."*".$rowap222['vinculacion'].">".$rowap222['descripcion']." ".$rowap222['vigencia']."</option>";
	           	   }// fin while
             	?>
              </select></td>
            </tr>
            <tr bgcolor="#EBEBEB">
              <td colspan="4" style="text-align:center" >&nbsp;</td>
            </tr>
            <tr>
                    
              <td width="133">Descripci&oacute;n:</td>
              <td width="279"><input name="descripcion_apropiaciones" type="text" id="descripcion_apropiaciones" onfocus="calcular()" size="32" value=""/>
              <img src="../../imagenes/menu/obligado.png" align="middle"/></td>
              <td width="121">Vigencia</span>:
               </td>
              <td width="294"><select name="vigencia_apropiaciones" id="vigencia_apropiaciones" class="box1">
                <option value="0">::Seleccione::</option>
					<?php
                    $tope = date("Y");
                    $edad_max = $tope-2005;
                    $edad_min = 0;
                    for($a= $tope - $edad_max; $a<=$tope - $edad_min; $a++)
                       {
                         echo "<option value='$a'>$a</option>"; 		
                       }	
                    ?>
            </select><img src="../../imagenes/menu/obligado.png" align="middle"/></td>
            </tr>
            <tr>
              <td>Periodo:</td>
              <td><select name="periodo_apropiaciones" id="periodo_apropiaciones" class="box1">
                <option value="0">::Seleccione::</option>
                <?php
				   while($row=$rs->fetch()){
						 echo "<option value=".$row['idperiodo'].">".$row['descripcionperiodo']."</option>";
				    }// fin while
               ?>
              </select>
              <img src="../../imagenes/menu/obligado.png" align="middle"/></td>
              <td>Apropiacion:</td>
              <td><input name="apropiacion_apropiaciones" type="text" id="apropiacion_apropiaciones" value="" onkeydown='solonumeros(this);' onkeyup='solonumeros(this);'/>$<img src="../../imagenes/menu/obligado.png" align="middle"/></td>
            </tr>
            <tr>
              <td>Tipo Vinculaci&oacute;n:</td>
              <td><select name="tipovinculacion_apropiaciones" id="tipovinculacion_apropiaciones" class="box1">
                <option value="0" selected="selected">::Seleccione::</option>
                <option value="1">Sin Vinculacion</option>
                <option value="2">Con Vinculacion</option>
              </select>
              <img src="../../imagenes/menu/obligado.png" align="middle"/></td>
              <td>Salario Minimo:</td>
              <td><input name="salariominimo_apropiaciones" type="text" id="salariominimo_apropiaciones" onkeydown='solonumeros(this),calcular();' onkeyup='solonumeros(this);' value="" readonly="readonly"/>
              $<img src="../../imagenes/menu/obligado.png" align="middle"/></td>
            </tr>
            <tr>              
              <td>Diferencia 55% (19-23)</td>
              <td><input name="diferencia_apropiacion" type="text" id="diferencia_apropiacion" onkeydown='solonumeros(this),calcular();' onkeyup='solonumeros(this);' />
$</td>
              <td>% Sostenimiento</td>
              <td><input name="sostenimiento_apropiacion" type="text" id="sostenimiento_apropiacion" onkeydown='solonumeros(this),calcular();' onkeyup='solonumeros(this);'  />
$</td>
            </tr>
            <tr>
              <td> Administraci&oacute;n:</td>
              <td>
              <input name="administracion_apropiacion" type="text" id="administracion_apropiacion" onkeydown='solonumeros(this),calcular();' onkeyup='solonumeros(this);' />
              $</td>
              <td>Cuociente:</td>
              <td><input name="cociente_apropiacion" type="text" id="cociente_apropiacion" onkeydown='solonumeros(this),calcular();' onkeyup='solonumeros(this);'  />
$</td>
            </tr>
            <tr>
              <td>Disponible:</td>
              <td colspan="3"><input name="disponible_apropiaciones" type="text" id="disponible_apropiaciones" disabled="disabled"/>
              $</td>
            </tr>
          </table>
</center>

<td class="cuerpo_de"></td><!-- FONDO DERECHA -->
<tr>
<td height="41" class="abajo_iz">&nbsp;</td>
<td class="abajo_ce">&nbsp;<font color="#FF0000">Campos Obligatorios</font><img src="../../imagenes/menu/obligado.png" align="middle"/></td>
<td class="abajo_de">&nbsp;</td>
</tr>
</table>
</form>
</body>
</html>