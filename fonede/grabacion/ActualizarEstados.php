<?php
ini_set('display_errors','Off');
date_default_timezone_set('America/Bogota');
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$url=$_SERVER['PHP_SELF'];
include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'auditoria.php';
auditar($url);

include_once '../../rsc/pdo/IFXDbManejador.php';
include_once '../../rsc/pdo/IFXerror.php';

$db = IFXDbManejador::conectarDB(); 
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

session_start();
$usuario=$_SESSION['USUARIO'];


switch ($proceso) {
		case 'radicacion':
			 $anterior = "In";	
			 $actual = "Ra";			 
			break;
		case 'grabacion':
			 $anterior = "Ra";	
			 $actual = "Gr";			 
			break;
		case "postulacion":
			 $anterior = "Gr";	
			 $actual = "Po";			
			break;
		case "asignacion":
			 $anterior = "Po";	
			 $actual = "As";			 
			break;
		case "giro":
			 $anterior = "As";	
			 $actual = "Gi";			 
			break;
	}//fin switch


		 $sql_act="SELECT * FROM aportes221 where 1=1 and (proceso='".$anterior."' or proceso='".$actual."')";
		 
		 if ($idpersona != "")	  
		  	  $sql_act .= " and idpersona='".$idpersona."'";
		 //echo "sql_act= ".$sql_act;
		 $rs_act=$db->querySimple($sql_act);
		 $dato=$proceso;
		 		 
		 while($row_act=$rs_act->fetch() ) {
			 
			 	$idpersona=$row_act['idpersona'];
				$apto=$row_act['apto'];
				$documentos=$row_act['documentos'];
				$activo=$row_act['activo'];	
				$activobeneficiario=$row_act['activobeneficiario'];					
				$fosyga=$row_act['fosyga'];
				$fosygabeneficiario=$row_act['fosygabeneficiario'];
				$asocajas=$row_act['asocajas'];					
				$asocajasbeneficiario=$row_act['asocajasbeneficiario'];			
				$procesofonede=$row_act['proceso'];			
				$estado=$row_act['estado'];			
				$fechapostulacion=$row_act['fechapostulacion'];
				$fechaasignacion=$row_act['fechaasignacion'];
				$fechagiro=$row_act['fechagiro'];				
				 
				switch ($dato) {					
						case 'radicacion':					
							    						
								if ($activo=='I' && $fosyga == 0 && $asocajas == 0)
								{
									$campoestado = 'R';
									
								}//fin if
								if($activo=='A')
								{
									$campoestado='S';
								}								
								if($fosyga == 1 || $asocajas == 1)
								{
									$campoestado = 'X';									  																		
							    }// fin if															
																			
						break;
					
						case 'grabacion':
						      
							  					   
								if ($activo=='I' && $activobeneficiario=='I' && $documentos == 1  && $fosyga == 0 &&
								    $fosygabeneficiario == 0 && $asocajas == 0 && $asocajasbeneficiario == 0){									
									
									$campoapto =1;									
									$campoestado='R';										
									
								}//fin if	
								if($activo=='A' || $activobeneficiario=='A')
								{
									$campoapto =0;          																			
									$campoestado='S';
									
								}												
                                if($fosyga == 1 || $asocajas == 1 || $fosygabeneficiario == 1 || $asocajasbeneficiario == 1)
								{
									$campoapto =0;																									
									$campoestado='X';
																		  																		
							    }// fin if
													
						break;						
						case 'postulacion':		
						if($fechapostulacion=='')
						{				
							$fechaactual=date("Y-m-d");
							$sqla="update aportes221 set fechapostulacion='".$fechaactual."',usuario='$usuario',fechasistema= cast(GetDate() as date) where  idpersona='".$idpersona."' and (estado!='X' or estado!='S')";
							$rsa=$db->queryActualiza($sqla,'aportes221');
						}
						       							
						       if ($activo=='I' && $activobeneficiario=='I' &&  $documentos == 1  && $fosyga == 0 &&
								    $fosygabeneficiario == 0 && $asocajas == 0 && $asocajasbeneficiario == 0){
								    
									$campoapto =1;
									if($estado=='P' && $activo=='I' && $activobeneficiario=='I')
									{
										$campoestado='P';
									}	
									else
									{								
										$campoestado='R';									
									}																		
									
								}//fin if
								if($activo=='A' || $activobeneficiario=='A')
								{
									$campoapto =0;																				
									$campoestado='S';
									
								}		
								if($estado=='S' && $activo=='I' && $activobeneficiario=='I')
								{
									$campoapto =1;
									$campoestado='P';
								}					
                                if($fosyga == 1 || $asocajas == 1 || $fosygabeneficiario == 1 || $asocajasbeneficiario == 1)
								{
									
									$campoapto =0;																								
									$campoestado='X';
																		  																		
							    }// fin if
						break;					
						case 'asignacion':
						if($fechaasignacion=='')
						{
							$fechaactual=date("Y-m-d");
							$sqla="update aportes221 set fechaasignacion='".$fechaactual."',usuario='$usuario',fechasistema= cast(GetDate() as date) where  idpersona='".$idpersona."' and (estado!='X' or estado!='S')";
							$rsa=$db->queryActualiza($sqla,'aportes221');	
						}
							 					        
								if ($activo=='I' && $activobeneficiario=='I' &&  $documentos == 1  && $fosyga == 0 &&
								    $fosygabeneficiario == 0 && $asocajas == 0 && $asocajasbeneficiario == 0){
								    									
									$campoapto =1;										
									$campoestado='A';									
									
								}//fin if						
                                if($fosyga == 1 || $asocajas == 1 || $fosygabeneficiario == 1 || $asocajasbeneficiario == 1 
								   || $activo=='A' || $activobeneficiario=='A')
								{
									
									$campoapto =0;					   																		
									$campoestado='X';									  																		
							    }// fin if
							break;	
								
						case 'giro':
						if($fechagiro=='')
						{
							 $fechaactual=date("Y-m-d");
							 $sqla="update aportes221 set fechagiro='".$fechaactual."',usuario='$usuario',fechasistema= cast(GetDate() as date) where  idpersona='".$idpersona."' and (estado!='X' or estado!='S')";
							$rsa=$db->queryActualiza($sqla,'aportes221');
						}						 
						 
								if ($activo=='I' && $activobeneficiario=='I' && $documentos == 1  && $fosyga == 0 &&
								    $fosygabeneficiario == 0 && $asocajas == 0 && $asocajasbeneficiario == 0){
								    
									$campoapto =1;								
									$campoestado='G';									
									
								}//fin if						
                                if($fosyga == 1 || $asocajas == 1 || $fosygabeneficiario == 1 || $asocajasbeneficiario == 1 
								   || $activo=='A' || $activobeneficiario=='A')
								{
									$campoapto =0;																									
									$campoestado='X';
																		  																		
							    }// fin if
							
							  $contadorgrio=0;
							  $sql = "select * from aportes225 where idpersona ='".$idpersona."' and fechagiro is not null and periodo is not null ";
							  $rs=$db->querySimple($sql);
							  
							  while($row=$rs->fetch() ) {
									++$contadorgrio;
							  }
							if($contadorgrio==6)							 
							{
							   
								$campoapto =0;																											
							    $campoestado='T';			 
							 }
							  								
						break;		
																
				}//fin switch
				
							
		if($fosyga == 1 || $asocajas == 1 || $fosygabeneficiario == 1 || $asocajasbeneficiario == 1 
								   || $activo=='A' || $activobeneficiario=='A')
					{
						
						$sql_update="update aportes221 set estado='$campoestado',apto='$campoapto',proceso='".$anterior."',usuario='$usuario',fechasistema= cast(GetDate() as date) where idpersona ='".$idpersona."'";		
	                	$rs=$db->queryActualiza($sql_update,'aportes221');					
					}
					else				
					{
						$sql_update="update aportes221 set estado='$campoestado',apto='$campoapto',proceso='".$actual."',usuario='$usuario',fechasistema= cast(GetDate() as date) where idpersona ='".$idpersona."'";		
	                	$rs=$db->queryActualiza($sql_update,'aportes221');
					}	
					
							
		
				
				
		}//fin while

?>