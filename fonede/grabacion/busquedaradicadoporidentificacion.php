<?php
ini_set('display_errors','Off');
//date_default_timezone_set("America/Bogota");
include_once '../../rsc/pdo/IFXDbManejador.php';
include_once '../../rsc/pdo/IFXerror.php';

$db = IFXDbManejador::conectarDB(); 
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();	
}

$campo0=$_REQUEST['v0'];
$campo1=$_REQUEST['v1'];

$sql="select aportes004.idradicacion,aportes004.fecharadicacion,aportes015.idpersona,aportes015.idtipodocumento,aportes015.identificacion,
aportes015.pnombre,aportes015.snombre,aportes015.papellido,aportes015.sapellido,aportes015.fechanacimiento,
aportes015.sexo,aportes015.idciuresidencia,aportes015.iddepresidencia,aportes015.direccion,
aportes015.telefono,aportes015.idescolaridad,aportes015.idestadocivil,aportes015.capacidadtrabajo,
aportes015.desplazado,aportes015.reinsertado
from aportes004, aportes015
where aportes004.numero=aportes015.identificacion and  aportes004.idtipodocumentoafiliado=aportes015.idtipodocumento and aportes015.idtipodocumento='$campo0' and aportes015.identificacion='$campo1'";
$rs=$db->querySimple($sql);
$con=0;
$filas=array();
while ($row= $rs->fetch()){
	$filas[]= $row;
	$con++;
}
if($con==0){
	echo 0;
	exit();
}

echo json_encode($filas);
?>