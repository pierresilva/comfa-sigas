<?php
// inicio de tiempo
$start_time = microtime(true); 
 
ini_set('display_errors','Off');
date_default_timezone_set('America/Bogota');
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$url=$_SERVER['PHP_SELF'];
include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'auditoria.php';
auditar($url);

include_once '../../rsc/pdo/IFXDbManejador.php';
include_once '../../rsc/pdo/IFXerror.php';

$db = IFXDbManejador::conectarDB();   
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

session_start();
$usuario=$_SESSION['USUARIO'];

?>
<?php
	switch ($proceso) {
		case 'radicacion':
			 $anterior = "In";	
			 $actual = "Ra";			 
			break;
		case 'grabacion':
			 $anterior = "Ra";	
			 $actual = "Gr";			 
			break;
		case "postulacion":
			 $anterior = "Gr";	
			 $actual = "Po";			
			break;
		case "asignacion":
			 $anterior = "Po";	
			 $actual = "As";			 
			break;
		case "giro":
			 $anterior = "As";	
			 $actual = "Gi";			 
			break;
	}//fin switch

	 
		  $sql="select aportes221.*, aportes015.identificacion
		  from aportes221, aportes015
		  where aportes015.idpersona = aportes221.idpersona
		  and (proceso='".$anterior."' or proceso='".$actual."')";
		  	
		  if ($idpersona != "")	  
		  	  $sql .= " and aportes015.idpersona='".$idpersona."'";		  	
		  //echo "sql=  ".$sql;
		  $rs=$db->querySimple($sql);	
	
$cont_fos=0;
$cont_asoc=0;
$cont_inactivo=0;
$cont_beneficiario=0;
$persona ="postulado";


while($row=$rs->fetch() ) {
	
	$identificacion=$row['identificacion'];	
	$idpersona=$row['idpersona'];	
	$idpostulado=$idpersona;
	$artista=$row['artista'];
	$deportista=$row['deportista'];
	$escritor=$row['escritor'];
	
	
	
	$observacionasocajas='';
	$observacioninactivo='';
	$observacionfosyga='';
	$observacionbeneficiariofosyga='';
	$observacionbeneficiarioasocajas='';
	$observacionbeneficiarioinactivo='';

			if (($proceso =='radicacion') ||  ($proceso=='grabacion') || ($proceso=='postulacion') || ($proceso=='asignacion') || ($proceso=='giro')){
					
					
					$sqlap233="SELECT COUNT(identificacion) FROM aportes233 where identificacion='".$identificacion."'";
					$rsap233=$db->querySimple($sqlap233);
					$rowap233=$rsap233->fetchColumn();	
					if($rowap233!=0)
					{
						$valorfosyga=1;
						$alertafosygapostulado="fosygapostulado";
						$observacionfosyga="Postulado Encontrado en Fosyga.";				
					}	
					else
					{
						$valorfosyga=0;
					}
					
					$sqlfosygap="update aportes221 set fosyga='".$valorfosyga."',usuario='$usuario',fechasistema= cast(GetDate() as date) where idpersona='".$idpostulado."'";
	                $rsfosygap=$db->queryActualiza($sqlfosygap,'aportes221');
					
					
					$sqlap232="SELECT COUNT(identificacion) FROM aportes232 where identificacion='".$identificacion."'";
					$rsap232=$db->querySimple($sqlap232);
					$rowap232=$rsap232->fetchColumn();	
					if($rowap232!=0)
					{
						$valorasocajas=1;
						$alertaasocajaspostulado="asocajaspostulado";	
						$observacionasocajas="Postulado Encontrado en Asocajas.";					
						
					}
					else
					{
						$valorasocajas=0;
					}
					$sqlasocajasp="update aportes221 set asocajas='".$valorasocajas."',usuario='$usuario',fechasistema= cast(GetDate() as date) where idpersona='".$idpostulado."'";
	                $rsasocajasp=$db->queryActualiza($sqlasocajasp,'aportes221');	
					
					
					
					$sqlap016="SELECT COUNT(idpersona) FROM aportes016 where idpersona='".$idpostulado."'";
					$rsap016=$db->querySimple($sqlap016);
					$rowap016=$rsap016->fetchColumn();	
					if($rowap016!=0)
					{
					    $valoractivo="A";
						$alertainactivopostulado="inactivo";
						$observacioninactivo="Postulado Activo.";				
						
					}
					else
					{
						$valoractivo="I";
					}
					$sqlactivop="update aportes221 set activo='".$valoractivo."',usuario='$usuario',fechasistema= cast(GetDate() as date) where idpersona='".$idpostulado."'";
					$rsactivop=$db->queryActualiza($sqlactivop,'aportes221');				
										
			}//del if		
		
					 
			if (($proceso=='grabacion') || ($proceso=='postulacion') || ($proceso=='asignacion') || ($proceso=='giro')) {		

//**************************    VALIDA BENEFICIARIO    ****************************************************************************************							
			
			
			
			//carlos esta variable contrala que no se repita cuando el tiene mas de un beneficiario			 
				$repetido = false; 
				$sql_con="SELECT * FROM aportes224 where idtrabajador ='".$idpersona."'";

				 $rs_con=$db->querySimple($sql_con);
				 while($row_con=$rs_con->fetch()  ) { 
				 	if ($repetido == false ){
							 
							  $idbeneficiario=$row_con['idbeneficiario']; 
							  
							  
							  $sql_fam = "SELECT * FROM aportes015 where idpersona ='".$idbeneficiario."'";
							  $rs_fam=$db->querySimple($sql_fam);
							  
							  while($row_fam=$rs_fam->fetch() ) {
									$identificacionbeneficiario=$row_fam['identificacion'];
							  }
							  
							$sqlap233b="SELECT COUNT(identificacion) FROM aportes233 where identificacion='".$identificacionbeneficiario."'";
							$rsap233b=$db->querySimple($sqlap233b);
							$rowap233b=$rsap233b->fetchColumn();	
							if($rowap233b!=0)
							{
								$valorfosygab=1;
								$alertafosygabeneficiario="fosygabeneficiario";		
							    $observacionbeneficiariofosyga="Beneficiario Encontrado en Fosyga.";							
							}
							else
							{
								$valorfosygab=0;
							}
							$sqlfosygab="update aportes221 set fosygabeneficiario='".$valorfosygab."',usuario='$usuario',fechasistema= cast(GetDate() as date) where idpersona='".$idpostulado."'";
							$rsfosygab=$db->queryActualiza($sqlfosygab,'aportes221');	
							
						    $sqlap232b="SELECT COUNT(identificacion) FROM aportes232 where identificacion='".$identificacionbeneficiario."'";
							$rsap232b=$db->querySimple($sqlap232b);
							$rowap232b=$rsap232b->fetchColumn();	
							if($rowap232b!=0)
							{
								$valorasocajasb=1;
								$alertaasocajasbeneficiario="asocajasbeneficiario";	
								$observacionbeneficiarioasocajas="Beneficiario Encontrado en Asocajas.";								
								
							}
							else
							{
								$valorasocajasb=0;
							}
							$sqlasocajasb="update aportes221 set asocajasbeneficiario='".$valorasocajasb."',usuario='$usuario',fechasistema= cast(GetDate() as date) where idpersona='".$idpostulado."'";
							$rsasocajasb=$db->queryActualiza($sqlasocajasb,'aportes221');
							
							$sqlap016b="SELECT COUNT(idpersona) FROM aportes016 where idpersona='".$idbeneficiario."'";
							$rsap016b=$db->querySimple($sqlap016b);
							$rowap016b=$rsap016b->fetchColumn();	
							if($rowap016b!=0)
							{
								$valoractivob="A";
								$alertainactivobeneficiario="inactivobeneficiario";
							    $observacionbeneficiarioinactivo="Beneficiario Activo.";							
								
							}
							else							
							{
								$valoractivob="I";
							}
							$sqlactivob="update aportes221 set activobeneficiario='".$valoractivob."',usuario='$usuario',fechasistema= cast(GetDate() as date) where idpersona='".$idpostulado."'";
							$rsactivob=$db->queryActualiza($sqlactivob,'aportes221');
							
						    if (($valorfosygab == 1) || ($valorasocajasb == 1) ||($valoractivob != "I")){
								$repetido = true; 
							}
		  
							  
					} //del if repetido
		
								 
		}// fin while while($row=$rs_con->fetch() ) {	
				

} //del fin if ($proceso=='grabacion') {
					
		$observacionesgeneral=$observacionasocajas." ".$observacioninactivo." ".$observacionfosyga." ".$observacionbeneficiariofosyga." ".$observacionbeneficiarioasocajas." ".$observacionbeneficiarioinactivo;	
		$sql_update="update aportes221 set observacionesestado='".trim($observacionesgeneral)."',usuario='$usuario',fechasistema= cast(GetDate() as date) where idpersona = $idpersona";
		$rs_update=$db->queryActualiza($sql_update,'aportes221');			


	   
}//fin while($row=$rs->fetch() ) {		
		
// calculando tiempo
 $tiempofinal=(microtime(true) - $start_time);	
 setcookie("tiempofinal", $tiempofinal, $expire);


?> 