<?php

//ini_set('display_errors','On');
include_once '../../../rsc/pdo/IFXDbManejador.php';
include_once '../../../rsc/pdo/IFXerror.php';

?>

<?php

	function conexion()
	{
	   $db = IFXDbManejador::conectarDB(); 
	   if($db->conexionID==null){
		   $cadena = $db->error;
		   echo msg_error($cadena);
		  exit();	
		}
		return $db;
	 }

 function cargardepartamento()
	{
	  
	  $db=conexion();
	  
	  $sql_departamento ="SELECT distinct coddepartamento,departmento from aportes089 order by departmento";
	  $rsdepar=$db->querySimple($sql_departamento);  
	
	  $departamentos = array();	  
	  while($rowdepar=$rsdepar->fetch())
				{
					$code = $rowdepar["coddepartamento"];
					$name = $rowdepar["departmento"];				
					$departamentos[$code]=$name;						
				}			
			return $departamentos;
			
	}// fin cargardepartamento
	
 function cargarmunicipio($id)
	{
	  
	  $db=conexion();
	  
	  $sql_municipio ="SELECT distinct codmunicipio,municipio from aportes089 where coddepartamento='".$id."' order by municipio";
	  $rsmuni=$db->querySimple($sql_municipio);  
	
	  $municipios = array();	  
	  while($rowmuni=$rsmuni->fetch())
				{
					$code = $rowmuni["codmunicipio"];
					$name =$rowmuni["municipio"];				
					$municipios[$code]=$name;						
				}			
			return $municipios;
			
	}// fin cargarmunicipio
	
  function cargarzona($id)
	{
	  
	  $db=conexion();
	  
	  $sql_zona ="SELECT distinct codzona,zona from aportes089 where codmunicipio='".$id."' order by zona";
	  $rszona=$db->querySimple($sql_zona);  
	
	  $zonas = array();	  
	  while($rowzona=$rszona->fetch())
				{
					$code = $rowzona["codzona"];
					$name =$rowzona["zona"];				
					$zonas[$code]=$name;						
				}			
			return $zonas;
			
	}// fin cargarzona
	
	function cargarbarrio($id)
	{
	  
	  $db=conexion();
	  
	  $sql_barrio ="SELECT distinct idbarrio,barrio from aportes087 where codigozona='".$id."' order by barrio";
	  $rsbarrio=$db->querySimple($sql_barrio);  
	
	  $barrios = array();	  
	  while($rowbarrio=$rsbarrio->fetch())
				{
					$code = $rowbarrio["idbarrio"];
					$name =$rowbarrio["barrio"];				
					$barrios[$code]=$name;						
				}			
			return $barrios;
			
	}// fin cargarbarrio


	
	
?>


