<?php
ini_set('display_errors','On');
//date_default_timezone_set("America/Bogota");
include_once '../../rsc/pdo/IFXDbManejador.php';
include_once '../../rsc/pdo/IFXerror.php';
include_once 'Funcionesvalidaciones.php'; 

$db = IFXDbManejador::conectarDB(); 
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();	
}// fin if
?>
<?php

$campov=$_REQUEST['v'];	

$total_dias=0;
$vinculacion=0;

$sql="select aportes015.idpersona,aportes016.fechaingreso,aportes016.fecharetiro from  aportes004,aportes015,aportes016
where 1=1 and aportes004.numero=aportes015.identificacion and aportes015.idpersona=aportes016.idpersona and aportes004.idradicacion=$campov
UNION
select aportes015.idpersona,aportes017.fechaingreso,aportes017.fecharetiro from  aportes004,aportes015,aportes017
where 1=1 and aportes004.numero=aportes015.identificacion and aportes015.idpersona=aportes017.idpersona and aportes004.idradicacion=$campov";
$rs=$db->querySimple($sql);

$con=0;
$filas=array();
while ($row= $rs->fetch()){
	$filas[]= $row;
	$con++;

	$dias_periodo= calcular_fecha($row['fechaingreso'], $row['fecharetiro']);
	$total_dias +=$dias_periodo;	
		
    if ($total_dias > 360){
			$vinculacion = 2;
		}
		else
		 if($total_dias < 360)
			{
			 $vinculacion = 1;
			}	
	
}// fin while
		
	  //echo "total_dias=".$total_dias;
	  //echo "vinculacion=".$vinculacion;
	  
if($con==0){
	
	$vinculacion=1;
	$total_dias=0;
	//echo $total_dias." ".$vinculacion." "."0";	
	//exit();
}// fin if

echo json_encode($total_dias.'*'.$vinculacion);


?>