<?php
set_time_limit ( 0 );
ini_set ( "display_errors", '1' );
date_default_timezone_set ( "America/Bogota" );

$root = $_SERVER ['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once $root;

include_once $_SESSION ['RAIZ'] . DIRECTORY_SEPARATOR . '/rsc/pdo/IFXDbManejador.php';
include_once $_SESSION ['RAIZ'] . DIRECTORY_SEPARATOR . '/rsc/pdo/IFXerror.php';

$db = IFXDbManejador::conectarDB ();
if ($db->conexionID == null) {
	$cadena = $db->error;
	echo msg_error ( $cadena );
	exit ();
}

$usuario = $_SESSION ['USUARIO'];
$proceso = $_REQUEST ['v0'];
$idpersona = $_REQUEST ['v1'];
$rango = $_REQUEST ['v2'];
$cantidad=50;

switch ($proceso) {
	case 'radicacion' :
		$anterior = "In";
		$actual = "Ra";
		break;
	case 'grabacion' :
		$anterior = "Ra";
		$actual = "Gr";
		break;
	case "postulacion" :
		$anterior = "Gr";
		$actual = "Po";
		break;
	case "asignacion" :
		$anterior = "Po";
		$actual = "As";
		break;
	case "giro" :
		$anterior = "As";
		$actual = "Gi";
		break;
}

$cont_fos = 0;
$cont_asoc = 0;
$cont_inactivo = 0;
$cont_beneficiario = 0;
$persona = "postulado";

$sql = "select * from (
		  select aportes221.*, aportes015.identificacion, ROW_NUMBER() OVER (ORDER BY idfonede) as row 
		  from aportes221, aportes015
		  where aportes015.idpersona = aportes221.idpersona
		  and (proceso='" . $anterior . "' or proceso='" . $actual . "')";

if ($idpersona != "")
	$sql .= " and aportes015.identificacion='" . $idpersona . "'";

$sql .= ") a where row > ($cantidad*$rango) and row <= ($cantidad*$rango+$cantidad)";

$rs = $db->querySimple ( $sql );
//var_dump($rs);
while ( $row = $rs->fetch () ) {
	
	$identificacion = $row ['identificacion'];
	$idpersona = $row ['idpersona'];
	$idpostulado = $idpersona;
	
	$observacionasocajas = '';
	$observacioninactivo = '';
	$observacionfosyga = '';
	$observacionbeneficiariofosyga = '';
	$observacionbeneficiarioasocajas = '';
	$observacionbeneficiarioinactivo = '';
	
	if (($proceso == 'radicacion') || ($proceso == 'grabacion') || ($proceso == 'postulacion') || ($proceso == 'asignacion') || ($proceso == 'giro')) {
		
		$sqlap233 = "SELECT COUNT(identificacion) FROM aportes233 where identificacion='" . $identificacion . "'";
		$rsap233 = $db->querySimple ( $sqlap233 );
		$rowap233 = $rsap233->fetchColumn ();
		if ($rowap233 != 0) {
			$valorfosyga = 1;
			$alertafosygapostulado = "fosygapostulado";
			$observacionfosyga = "Postulado Encontrado en Fosyga.";
		} else {
			$valorfosyga = 0;
		}
		
		$sqlap232 = "SELECT COUNT(identificacion) FROM aportes232 where identificacion='" . $identificacion . "'";
		$rsap232 = $db->querySimple ( $sqlap232 );
		$rowap232 = $rsap232->fetchColumn ();
		if ($rowap232 != 0) {
			$valorasocajas = 1;
			$alertaasocajaspostulado = "asocajaspostulado";
			$observacionasocajas = "Postulado Encontrado en Asocajas.";
		
		} else {
			$valorasocajas = 0;
		}		
		
		$sqlap016 = "SELECT COUNT(idpersona) FROM aportes016 where idpersona='" . $idpostulado . "'";
		$rsap016 = $db->querySimple ( $sqlap016 );
		$rowap016 = $rsap016->fetchColumn ();
		if ($rowap016 != 0) {
			$valoractivo = "A";
			$alertainactivopostulado = "inactivo";
			$observacioninactivo = "Postulado Activo.";
		
		} else {
			$valoractivo = "I";
		}
		
		$sqlfosygap = "update aportes221 set fosyga='" . $valorfosyga . "', asocajas='" . $valorasocajas . "',activo='" . $valoractivo . "',usuario='$usuario',fechasistema= cast(GetDate() as date) where idpersona='" . $idpostulado . "'";
		$rsfosygap = $db->queryActualiza ( $sqlfosygap, 'aportes221' );
	
	}
	
	if (($proceso == 'grabacion') || ($proceso == 'postulacion') || ($proceso == 'asignacion') || ($proceso == 'giro')) {
		
		// ************************** VALIDA BENEFICIARIO
		// ****************************************************************************************
		
		// carlos esta variable contrala que no se repita cuando el tiene mas de
		// un beneficiario
		$sql_con = "SELECT a224.idbeneficiario, a15.identificacion FROM aportes224 a224 INNER JOIN aportes015 a15 ON a15.idpersona=a224.idbeneficiario where idtrabajador ='" . $idpersona . "'";
		
		$rs_con = $db->querySimple ( $sql_con );
		while ( $row_con = $rs_con->fetch () ) {
			$idbeneficiario = $row_con ['idbeneficiario'];
			$identificacionbeneficiario = $row_con ['identificacion'];
			
			$sqlap233b = "SELECT COUNT(identificacion) FROM aportes233 where identificacion='" . $identificacionbeneficiario . "'";
			$rsap233b = $db->querySimple ( $sqlap233b );
			$rowap233b = $rsap233b->fetchColumn ();
			if ($rowap233b != 0) {
				$valorfosygab = 1;
				$alertafosygabeneficiario = "fosygabeneficiario";
				$observacionbeneficiariofosyga = "Beneficiario Encontrado en Fosyga.";
			} else {
				$valorfosygab = 0;
			}
			
			$sqlap232b = "SELECT COUNT(identificacion) FROM aportes232 where identificacion='" . $identificacionbeneficiario . "'";
			$rsap232b = $db->querySimple ( $sqlap232b );
			$rowap232b = $rsap232b->fetchColumn ();
			if ($rowap232b != 0) {
				$valorasocajasb = 1;
				$alertaasocajasbeneficiario = "asocajasbeneficiario";
				$observacionbeneficiarioasocajas = "Beneficiario Encontrado en Asocajas.";
			
			} else {
				$valorasocajasb = 0;
			}
			
			$sqlap016b = "SELECT COUNT(idpersona) FROM aportes016 where idpersona='" . $idbeneficiario . "'";
			$rsap016b = $db->querySimple ( $sqlap016b );
			$rowap016b = $rsap016b->fetchColumn ();
			if ($rowap016b != 0) {
				$valoractivob = "A";
				$alertainactivobeneficiario = "inactivobeneficiario";
				$observacionbeneficiarioinactivo = "Beneficiario Activo.";
			
			} else {
				$valoractivob = "I";
			}
			
			$sqlfosygab = "update aportes221 set fosygabeneficiario='" . $valorfosygab . "', asocajasbeneficiario='" . $valorasocajasb . "', activobeneficiario='" . $valoractivob . "', usuario='$usuario',fechasistema= cast(GetDate() as date) where idpersona='" . $idpostulado . "'";
			$rsfosygab = $db->queryActualiza ( $sqlfosygab, 'aportes221' );
			
			if (($valorfosygab == 1) || ($valorasocajasb == 1) || ($valoractivob != "I")) {
				break;
			}
					
		} // fin while while($row=$rs_con->fetch() ) {
	
	} // del fin if ($proceso=='grabacion') {
	
	$observacionesgeneral = $observacionasocajas . " " . $observacioninactivo . " " . $observacionfosyga . " " . $observacionbeneficiariofosyga . " " . $observacionbeneficiarioasocajas . " " . $observacionbeneficiarioinactivo;
	$sql_update = "update aportes221 set observacionesestado='" . trim ( $observacionesgeneral ) . "',usuario='$usuario',fechasistema= cast(GetDate() as date) where idpersona = $idpersona";
	$rs_update = $db->queryActualiza ( $sql_update, 'aportes221' );
	
	/***** ACTUALIZAR ESTADOS *****/
	$apto=$row['apto'];
	$documentos=$row['documentos'];
	$activo=$valoractivo;
	$activobeneficiario=$valoractivob;
	$fosyga=$valorfosyga;
	$fosygabeneficiario=$valorfosygab;
	$asocajas=$valorasocajas;
	$asocajasbeneficiario=$valorasocajasb;
	$procesofonede=$row['proceso'];
	$estado=$row['estado'];
	$fechapostulacion=$row['fechapostulacion'];
	$fechaasignacion=$row['fechaasignacion'];
	$fechagiro=$row['fechagiro'];
	
	switch ($proceso) {
		case 'radicacion':
			 
			if ($activo=='I' && $fosyga == 0 && $asocajas == 0)
			{
				$campoestado = 'R';
					
			} elseif($fosyga == 1 || $asocajas == 1)
			{
				$campoestado = 'X';
			} elseif($activo=='A')
			{
				$campoestado='S';
			}			
				
			break;
				
		case 'grabacion':	
			 
			if ($activo=='I' && $activobeneficiario=='I' && $documentos == 1  && $fosyga == 0 && $fosygabeneficiario == 0 && $asocajas == 0 && $asocajasbeneficiario == 0){
					
				$campoapto =1;
				$campoestado='R';
					
			} elseif($fosyga == 1 || $asocajas == 1 || $fosygabeneficiario == 1 || $asocajasbeneficiario == 1)
			{
				$campoapto =0;
				$campoestado='X';
	
			} elseif($activo=='A' || $activobeneficiario=='A')
			{
				$campoapto =0;
				$campoestado='S';
					
			}
				
			break;
		case 'postulacion':
			if($fechapostulacion=='')
			{
				$fechaactual=date("Y-m-d");
				$sqla="update aportes221 set fechapostulacion='".$fechaactual."',usuario='$usuario',fechasistema= cast(GetDate() as date) where  idpersona='".$idpersona."' and (estado!='X' or estado!='S')";
				$rsa=$db->queryActualiza($sqla,'aportes221');
			}
	
			if ($activo=='I' && $activobeneficiario=='I' &&  $documentos == 1  && $fosyga == 0 && $fosygabeneficiario == 0 && $asocajas == 0 && $asocajasbeneficiario == 0){
	
				$campoapto =1;
				if($estado=='P')
				{
					$campoestado='P';
				}
				else
				{
					$campoestado='R';
				}
					
			} elseif($fosyga == 1 || $asocajas == 1 || $fosygabeneficiario == 1 || $asocajasbeneficiario == 1)
			{
					
				$campoapto =0;
				$campoestado='X';
	
			} elseif($estado=='S' && $activo=='I' && $activobeneficiario=='I')
			{
				$campoapto =1;
				$campoestado='P';
			} elseif($activo=='A' || $activobeneficiario=='A')
			{
				$campoapto =0;
				$campoestado='S';
					
			}			
			
			break;
		case 'asignacion':
			if($fechaasignacion=='')
			{
				$fechaactual=date("Y-m-d");
				$sqla="update aportes221 set fechaasignacion='".$fechaactual."',usuario='$usuario',fechasistema= cast(GetDate() as date) where  idpersona='".$idpersona."' and (estado!='X' or estado!='S')";
				$rsa=$db->queryActualiza($sqla,'aportes221');
			}
				
			if ($activo=='I' && $activobeneficiario=='I' &&  $documentos == 1  && $fosyga == 0 && $fosygabeneficiario == 0 && $asocajas == 0 && $asocajasbeneficiario == 0){
				 
				$campoapto =1;
				$campoestado='A';
					
			} elseif($fosyga == 1 || $asocajas == 1 || $fosygabeneficiario == 1 || $asocajasbeneficiario == 1 || $activo=='A' || $activobeneficiario=='A')
			{
					
				$campoapto =0;
				$campoestado='X';
			}// fin if
			break;
	
		case 'giro':
			if($fechagiro=='')
			{
				$fechaactual=date("Y-m-d");
				$sqla="update aportes221 set fechagiro='".$fechaactual."',usuario='$usuario',fechasistema= cast(GetDate() as date) where  idpersona='".$idpersona."' and (estado!='X' or estado!='S')";
				$rsa=$db->queryActualiza($sqla,'aportes221');
			}
				
			if ($activo=='I' && $activobeneficiario=='I' && $documentos == 1  && $fosyga == 0 && $fosygabeneficiario == 0 && $asocajas == 0 && $asocajasbeneficiario == 0){
	
				$campoapto =1;
				$campoestado='G';
					
			} elseif($fosyga == 1 || $asocajas == 1 || $fosygabeneficiario == 1 || $asocajasbeneficiario == 1 || $activo=='A' || $activobeneficiario=='A')
			{
				$campoapto =0;
				$campoestado='X';
	
			}// fin if
				
			$contadorgrio=0;
			$sql = "select count(*) from aportes225 where idpersona ='".$idpersona."' and fechagiro is not null and periodo is not null ";
			$rscount=$db->querySimple($sql);
			$contadorgrio = $rscount->fetchColumn ();
			
			if($contadorgrio==6)
			{
	
				$campoapto =0;
				$campoestado='T';
			}
			 
			break;
	
	}//fin switch
	
		
	if($fosyga == 1 || $asocajas == 1 || $fosygabeneficiario == 1 || $asocajasbeneficiario == 1 || $activo=='A' || $activobeneficiario=='A')
	{
	
		$sql_update="update aportes221 set estado='$campoestado',apto='$campoapto',proceso='".$anterior."',usuario='$usuario',fechasistema= cast(GetDate() as date) where idpersona ='".$idpersona."'";
		$rsupdate=$db->queryActualiza($sql_update,'aportes221');
	}
	else
	{
		$sql_update="update aportes221 set estado='$campoestado',apto='$campoapto',proceso='".$actual."',usuario='$usuario',fechasistema= cast(GetDate() as date) where idpersona ='".$idpersona."'";
		$rsupdate=$db->queryActualiza($sql_update,'aportes221');
	}	
}

echo 0;

?>