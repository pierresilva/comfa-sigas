<script languaje="javascript" src="../reportes/EjeFonede.js"></script>
<?php
ini_set('display_errors','Onn'); 
include_once '../../rsc/pdo/IFXDbManejador.php';
include_once '../../rsc/pdo/IFXerror.php'; 
session_start();

$db = IFXDbManejador::conectarDB(); 
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();	
}

//ini_set('display_errors','Off');
	  header ('Content-Type: application/force-download');
      header('Content-type: application/html');
      header('Content-Disposition: attachment; filename=Asignados.xls');
?>
<?php

function cambiarFormatoFecha($fecha){
    list($anio,$mes,$dia)=explode("-",$fecha);
     return $dia."-".$mes."-".$anio;
}

function cambiarFormatoFecha2($fecha){
    list($dia,$mes,$anio)=explode("-",$fecha);
     return $anio."-".$mes."-".$dia;
}
		
function dameFecha($fecha,$dia)
{   list($day,$mon,$year) = explode('-',$fecha);
    return date('d-m-Y',mktime(0,0,0,$mon,$day+$dia,$year));       
}

if ($_GET['ok'] == 'ok') {	

$periodo=$_GET['periodo']; 
$vigencia=$_GET['vigencia'];

$arr_vinculacion[0]= "2"; // 2 con vinculacion
$arr_vinculacion[1]= "1"; // 1 si vinculacion
$arr_genero[0]= "F"; // femenino
$arr_genero[1]= "M"; // masculino

$arr_condicion[0]= "Desplazado"; // compo desplazado
$arr_valor_desp[0]= "S"; // compo desplazado S
$arr_valor_desp[1]= "N"; // compo desplazado N

$arr_condicion[1]= "Discapacitado"; // compo discapacitado
$arr_valor_disc[0]= "I"; // compo discapacitado I
$arr_valor_disc[1]= "N"; // compo discapacitado N

$arr_condicion[2]= "Reinsertado"; // compo discapacitado
$arr_valor_rein[0]= "S"; // compo Reinsertado S
$arr_valor_rein[1]= "N"; // compo Reinsertado N

$arr_condicion[3]= "Ninguno"; // compo discapacitado
$arr_valor_ning[0]= "N"; // compo Ninguno N

$fechaactual = date('Y-m-d');
//echo "fechaactual=".$fechaactual."<br/>";

$nuevafecha5 = strtotime ( '-5 year', strtotime ( $fechaactual ) ) ;
$nuevafecha5 = date ( 'Y-m-d' , $nuevafecha5 );
//echo "nuevafecha5=".$nuevafecha5."<br/>";

$nuevafecha10= strtotime ( '-10 year', strtotime ( $fechaactual ) ) ;
$nuevafecha10 = date ( 'Y-m-d' , $nuevafecha10 );
//echo "nuevafecha10=".$nuevafecha10."<br/>";

$nuevafecha15 = strtotime ( '-15 year', strtotime ( $fechaactual ) ) ;
$nuevafecha15 = date ( 'Y-m-d' , $nuevafecha15 );
//echo "nuevafecha15=".$nuevafecha15."<br/>";

$nuevafecha20= strtotime ( '-20 year', strtotime ( $fechaactual ) ) ;
$nuevafecha20 = date ( 'Y-m-d' , $nuevafecha20 );
//echo "nuevafecha20=".$nuevafecha20."<br/>";

$nuevafecha25= strtotime ( '-25 year', strtotime ( $fechaactual ) ) ;
$nuevafecha25 = date ( 'Y-m-d' , $nuevafecha25 );
//echo "nuevafecha25=".$nuevafecha25."<br/>";

$nuevafecha30= strtotime ( '-30 year', strtotime ( $fechaactual ) ) ;
$nuevafecha30 = date ( 'Y-m-d' , $nuevafecha30 );
//echo "nuevafecha30=".$nuevafecha30."<br/>";

$nuevafecha35= strtotime ( '-35 year', strtotime ( $fechaactual ) ) ;
$nuevafecha35 = date ( 'Y-m-d' , $nuevafecha35 );
//echo "nuevafecha35=".$nuevafecha35."<br/>";

$nuevafecha40= strtotime ( '-40 year', strtotime ( $fechaactual ) ) ;
$nuevafecha40 = date ( 'Y-m-d' , $nuevafecha40 );
//echo "nuevafecha40=".$nuevafecha40."<br/>";

$nuevafecha45= strtotime ( '-45 year', strtotime ( $fechaactual ) ) ;
$nuevafecha45 = date ( 'Y-m-d' , $nuevafecha45 );
//echo "nuevafecha45=".$nuevafecha45."<br/>";

$nuevafecha50= strtotime ( '-50 year', strtotime ( $fechaactual ) ) ;
$nuevafecha50 = date ( 'Y-m-d' , $nuevafecha50 );
//echo "nuevafecha50=".$nuevafecha50."<br/>";

$nuevafecha55= strtotime ( '-55 year', strtotime ( $fechaactual ) ) ;
$nuevafecha55 = date ( 'Y-m-d' , $nuevafecha55 );
//echo "nuevafecha55=".$nuevafecha55."<br/>";

$nuevafecha60= strtotime ( '-60 year', strtotime ( $fechaactual ) ) ;
$nuevafecha60 = date ( 'Y-m-d' , $nuevafecha60 );
//echo "nuevafecha60=".$nuevafecha60."<br/>";

$nuevafecha65= strtotime ( '-65 year', strtotime ( $fechaactual ) ) ;
$nuevafecha65 = date ( 'Y-m-d' , $nuevafecha65 );
//echo "nuevafecha65=".$nuevafecha65."<br/>";

$nuevafecha70= strtotime ( '-70 year', strtotime ( $fechaactual ) ) ;
$nuevafecha70 = date ( 'Y-m-d' , $nuevafecha70 );
//echo "nuevafecha70=".$nuevafecha70."<br/>";

$nuevafecha75= strtotime ( '-75 year', strtotime ( $fechaactual ) ) ;
$nuevafecha75 = date ( 'Y-m-d' , $nuevafecha75 );
//echo "nuevafecha75=".$nuevafecha75."<br/>";

$nuevafecha100= strtotime ( '-100 year', strtotime ( $fechaactual ) ) ;
$nuevafecha100 = date ( 'Y-m-d' , $nuevafecha100 );
//echo "nuevafecha100=".$nuevafecha100."<br/>";

//+//
$arr_grpetereos_ini[0]= $fechaactual; //
//echo "<br/>fechaactual= ".$arr_grpetereos_ini[0];

//-//
$nuevafecha15=cambiarFormatoFecha($nuevafecha15);
$nuevafecha15=dameFecha($nuevafecha15,-1);
$nuevafecha15=cambiarFormatoFecha2($nuevafecha15); 
$arr_grpetereos_fin[0]= $nuevafecha15; // 
//echo "<br/>nuevafecha15fin= ".$arr_grpetereos_fin[0];

//-//
$nuevafecha15=cambiarFormatoFecha($nuevafecha15);
$nuevafecha15=dameFecha($nuevafecha15,-1);
$nuevafecha15=cambiarFormatoFecha2($nuevafecha15); 
$arr_grpetereos_ini[1]= $nuevafecha15; // 
//echo "<br/>nuevafecha15ini= ".$arr_grpetereos_ini[1];

//+//
$nuevafecha20=cambiarFormatoFecha($nuevafecha20);
$nuevafecha20=dameFecha($nuevafecha20,1);
$nuevafecha20=cambiarFormatoFecha2($nuevafecha20); 
$arr_grpetereos_fin[1]= $nuevafecha20; // 
//echo "<br/>nuevafecha20fin= ".$arr_grpetereos_fin[1];

//+//
$nuevafecha20=cambiarFormatoFecha($nuevafecha20);
$nuevafecha20=dameFecha($nuevafecha20,1);
$nuevafecha20=cambiarFormatoFecha2($nuevafecha20); 
$arr_grpetereos_ini[2]= $nuevafecha20; // 
//echo "<br/>nuevafecha20ini= ".$arr_grpetereos_ini[2];

//-//
$nuevafecha25=cambiarFormatoFecha($nuevafecha25);
$nuevafecha25=dameFecha($nuevafecha25,-1);
$nuevafecha25=cambiarFormatoFecha2($nuevafecha25); 
$arr_grpetereos_fin[2]= $nuevafecha25; // 
//echo "<br/>nuevafecha25fin= ".$arr_grpetereos_fin[2];

//-//
$nuevafecha25=cambiarFormatoFecha($nuevafecha25);
$nuevafecha25=dameFecha($nuevafecha25,-1);
$nuevafecha25=cambiarFormatoFecha2($nuevafecha25); 
$arr_grpetereos_ini[3]= $nuevafecha25; // 
//echo "<br/>nuevafecha25ini= ".$arr_grpetereos_ini[3];

//+//
$nuevafecha30=cambiarFormatoFecha($nuevafecha30);
$nuevafecha30=dameFecha($nuevafecha30,1);
$nuevafecha30=cambiarFormatoFecha2($nuevafecha30);
$arr_grpetereos_fin[3]= $nuevafecha30; // 
//echo "<br/>nuevafecha30fin= ".$arr_grpetereos_fin[3];

//+//
$nuevafecha30=cambiarFormatoFecha($nuevafecha30);
$nuevafecha30=dameFecha($nuevafecha30,1);
$nuevafecha30=cambiarFormatoFecha2($nuevafecha30);
$arr_grpetereos_ini[4]= $nuevafecha30; // 
//echo "<br/>nuevafecha30ini= ".$arr_grpetereos_ini[4];

//-//
$nuevafecha35=cambiarFormatoFecha($nuevafecha35);
$nuevafecha35=dameFecha($nuevafecha35,-1);
$nuevafecha35=cambiarFormatoFecha2($nuevafecha35);
$arr_grpetereos_fin[4]= $nuevafecha35; //
//echo "<br/>nuevafecha35fin= ".$arr_grpetereos_fin[4];


//-//
$nuevafecha35=cambiarFormatoFecha($nuevafecha35);
$nuevafecha35=dameFecha($nuevafecha35,-1);
$nuevafecha35=cambiarFormatoFecha2($nuevafecha35);
$arr_grpetereos_ini[5]= $nuevafecha35; // 
//echo "<br/>nuevafecha35ini= ".$arr_grpetereos_ini[5];

//+//
$nuevafecha40=cambiarFormatoFecha($nuevafecha40);
$nuevafecha40=dameFecha($nuevafecha40,1);
$nuevafecha40=cambiarFormatoFecha2($nuevafecha40);
$arr_grpetereos_fin[5]= $nuevafecha40; // 
//echo "<br/>nuevafecha40fin= ".$arr_grpetereos_fin[5];

//+//
$nuevafecha40=cambiarFormatoFecha($nuevafecha40);
$nuevafecha40=dameFecha($nuevafecha40,1);
$nuevafecha40=cambiarFormatoFecha2($nuevafecha40);
$arr_grpetereos_ini[6]= $nuevafecha40; //
//echo "<br/>nuevafecha40ini= ".$arr_grpetereos_ini[6]; 


//-//
$nuevafecha45=cambiarFormatoFecha($nuevafecha45);
$nuevafecha45=dameFecha($nuevafecha45,-1);
$nuevafecha45=cambiarFormatoFecha2($nuevafecha45);
$arr_grpetereos_fin[6]= $nuevafecha45; // 
//echo "<br/>nuevafecha45fin= ".$arr_grpetereos_fin[6]; 

//-//
$nuevafecha45=cambiarFormatoFecha($nuevafecha45);
$nuevafecha45=dameFecha($nuevafecha45,-1);
$nuevafecha45=cambiarFormatoFecha2($nuevafecha45);
$arr_grpetereos_ini[7]= $nuevafecha45; // 
//echo "<br/>nuevafecha45ini= ".$arr_grpetereos_ini[7]; 

//+//
$nuevafecha50=cambiarFormatoFecha($nuevafecha50);
$nuevafecha50=dameFecha($nuevafecha50,1);
$nuevafecha50=cambiarFormatoFecha2($nuevafecha50);
$arr_grpetereos_fin[7]= $nuevafecha50; //
//echo "<br/>nuevafecha50fin= ".$arr_grpetereos_fin[7]; 
 
//+//
$nuevafecha50=cambiarFormatoFecha($nuevafecha50);
$nuevafecha50=dameFecha($nuevafecha50,1);
$nuevafecha50=cambiarFormatoFecha2($nuevafecha50);
$arr_grpetereos_ini[8]= $nuevafecha50; // 
//echo "<br/>nuevafecha50ini= ".$arr_grpetereos_ini[8]; 

//-//
$nuevafecha55=cambiarFormatoFecha($nuevafecha55);
$nuevafecha55=dameFecha($nuevafecha55,-1);
$nuevafecha55=cambiarFormatoFecha2($nuevafecha55);
$arr_grpetereos_fin[8]= $nuevafecha55; // 
//echo "<br/>nuevafecha55fin= ".$arr_grpetereos_fin[8]; 

//-//
$nuevafecha55=cambiarFormatoFecha($nuevafecha55);
$nuevafecha55=dameFecha($nuevafecha55,-1);
$nuevafecha55=cambiarFormatoFecha2($nuevafecha55);
$arr_grpetereos_ini[9]= $nuevafecha55; // 
//echo "<br/>nuevafecha55ini= ".$arr_grpetereos_ini[9];

//+//
$nuevafecha60=cambiarFormatoFecha($nuevafecha60);
$nuevafecha60=dameFecha($nuevafecha60,1);
$nuevafecha60=cambiarFormatoFecha2($nuevafecha60);
$arr_grpetereos_fin[9]= $nuevafecha60; // 
//echo "<br/>nuevafecha60fin= ".$arr_grpetereos_fin[9];


//+//
$nuevafecha60=cambiarFormatoFecha($nuevafecha60);
$nuevafecha60=dameFecha($nuevafecha60,1);
$nuevafecha60=cambiarFormatoFecha2($nuevafecha60);
$arr_grpetereos_ini[10]= $nuevafecha60; // 
//echo "<br/>nuevafecha60ini= ".$arr_grpetereos_ini[10];

//-//
$nuevafecha65=cambiarFormatoFecha($nuevafecha65);
$nuevafecha65=dameFecha($nuevafecha65,-1);
$nuevafecha65=cambiarFormatoFecha2($nuevafecha65);
$arr_grpetereos_fin[10]= $nuevafecha65; // 
//echo "<br/>nuevafecha65fin= ".$arr_grpetereos_fin[10];


//-//
$nuevafecha65=cambiarFormatoFecha($nuevafecha65);
$nuevafecha65=dameFecha($nuevafecha65,-1);
$nuevafecha65=cambiarFormatoFecha2($nuevafecha65);
$arr_grpetereos_ini[11]= $nuevafecha65; // 
//echo "<br/>nuevafecha65ini= ".$arr_grpetereos_ini[11];

//+//
$nuevafecha70=cambiarFormatoFecha($nuevafecha70);
$nuevafecha70=dameFecha($nuevafecha70,1);
$nuevafecha70=cambiarFormatoFecha2($nuevafecha70);
$arr_grpetereos_fin[11]= $nuevafecha70; // 
//echo "<br/>nuevafecha70fin= ".$arr_grpetereos_fin[11];

//+//
$nuevafecha70=cambiarFormatoFecha($nuevafecha70);
$nuevafecha70=dameFecha($nuevafecha70,1);
$nuevafecha70=cambiarFormatoFecha2($nuevafecha70);
$arr_grpetereos_ini[12]= $nuevafecha70; // 
//echo "<br/>nuevafecha70ini= ".$arr_grpetereos_ini[12];

//-//
$nuevafecha100=cambiarFormatoFecha($nuevafecha100);
$nuevafecha100=dameFecha($nuevafecha100,-1);
$nuevafecha100=cambiarFormatoFecha2($nuevafecha100);
$arr_grpetereos_fin[12]= $nuevafecha100; // 
//echo "<br/>nuevafecha100fin= ".$arr_grpetereos_fin[12];


$arr_ragos[0]= "Menor de 15"; // 
$arr_ragos[1]= "15 A 19"; //  
$arr_ragos[2]= "20 A 24"; // 
$arr_ragos[3]= "25 A 29"; // 
$arr_ragos[4]= "30 A 34"; // 
$arr_ragos[5]= "35 A 39"; // 
$arr_ragos[6]= "40 A 44"; // 
$arr_ragos[7]= "45 A 49"; // 
$arr_ragos[8]= "50 A 54"; // 
$arr_ragos[9]= "55 A 59"; //
$arr_ragos[10]= "60 A 64"; //
$arr_ragos[11]= "65 A 69"; //
$arr_ragos[12]= "70 &oacute; MAS"; //

$anoactual=date('Y');
$mesactual=date('m');
$sql="SELECT * FROM aportes012 where periodo=$anoactual$mesactual";
$rs=$db->querySimple($sql);

while($row=$rs->fetch()){
	$salariominimo=$row['smlv'];
	//echo "slmv=".$salariominimo;
}
 
$arr_salario_ini[0]= '0'; // 
$arr_salario_fin[0]= '1'; // 
$arr_salario_ini[1]= '1'; // 
$arr_salario_fin[1]= '2.99'; // 
$arr_salario_ini[2]= '3'; // 
$arr_salario_fin[2]= '4.99'; // 
$arr_salario_ini[3]= '5'; // 
$arr_salario_fin[3]= '6.99'; // 
$arr_salario_ini[4]= '7'; // 
$arr_salario_fin[4]= '100'; // 

$arr_ragossalario[0]= "menos de 1"; // 
$arr_ragossalario[1]= "1 A 2,99 "; //  
$arr_ragossalario[2]= "3 A 4,99"; // 
$arr_ragossalario[3]= "5 A 6,99 "; // 
$arr_ragossalario[4]= "Mas de 7"; // 


$arr_escolaridad[0]= '203'; // 
$arr_escolaridad[1]= '199'; // 
$arr_escolaridad[2]= '200'; //
$arr_escolaridad[3]= '202'; // 
$arr_escolaridad[4]= '201'; // 
 


$arr_ragosescolaridad[0]= "Ninguna"; // 
$arr_ragosescolaridad[1]= "Primaria"; //  
$arr_ragosescolaridad[2]= "Bachillerato"; // 
$arr_ragosescolaridad[3]= "T&eacute;cnico"; // 
$arr_ragosescolaridad[4]= "Profesional"; // 



//+//
$arr_grpetereos_inigrupofamiliar[0]= $fechaactual; // 

//-//
$nuevafecha5=cambiarFormatoFecha($nuevafecha5);
$nuevafecha5=dameFecha($nuevafecha5,-1);
$nuevafecha5=cambiarFormatoFecha2($nuevafecha5); 
$arr_grpetereos_fingrupofamiliar[0]= $nuevafecha5; //

//-// 
$nuevafecha5=cambiarFormatoFecha($nuevafecha5);
$nuevafecha5=dameFecha($nuevafecha5,-1);
$nuevafecha5=cambiarFormatoFecha2($nuevafecha5); 
$arr_grpetereos_inigrupofamiliar[1]= $nuevafecha5; // 

//+//
$nuevafecha10=cambiarFormatoFecha($nuevafecha10);
$nuevafecha10=dameFecha($nuevafecha10,1);
$nuevafecha10=cambiarFormatoFecha2($nuevafecha10); 
$arr_grpetereos_fingrupofamiliar[1]= $nuevafecha10; // 

//+//
$nuevafecha10=cambiarFormatoFecha($nuevafecha10);
$nuevafecha10=dameFecha($nuevafecha10,1);
$nuevafecha10=cambiarFormatoFecha2($nuevafecha10); 
$arr_grpetereos_inigrupofamiliar[2]= $nuevafecha10; // 

$arr_grpetereos_fingrupofamiliar[2]= $nuevafecha15; // 
$arr_grpetereos_inigrupofamiliar[3]= $nuevafecha15; // 
$arr_grpetereos_fingrupofamiliar[3]= $nuevafecha20; // 
$arr_grpetereos_inigrupofamiliar[4]= $nuevafecha20; // 
$arr_grpetereos_fingrupofamiliar[4]= $nuevafecha25; // 
$arr_grpetereos_inigrupofamiliar[5]= $nuevafecha25; // 
$arr_grpetereos_fingrupofamiliar[5]= $nuevafecha30; // 
$arr_grpetereos_inigrupofamiliar[6]= $nuevafecha30; // 
$arr_grpetereos_fingrupofamiliar[6]= $nuevafecha35; // 
$arr_grpetereos_inigrupofamiliar[7]= $nuevafecha35; // 
$arr_grpetereos_fingrupofamiliar[7]= $nuevafecha40; // 
$arr_grpetereos_inigrupofamiliar[8]= $nuevafecha40; // 
$arr_grpetereos_fingrupofamiliar[8]= $nuevafecha45; // 
$arr_grpetereos_inigrupofamiliar[9]= $nuevafecha45; // 
$arr_grpetereos_fingrupofamiliar[9]= $nuevafecha50; // 
$arr_grpetereos_inigrupofamiliar[10]= $nuevafecha50; // 
$arr_grpetereos_fingrupofamiliar[10]= $nuevafecha55; // 
$arr_grpetereos_inigrupofamiliar[11]= $nuevafecha55; // 
$arr_grpetereos_fingrupofamiliar[11]= $nuevafecha60; // 
$arr_grpetereos_inigrupofamiliar[12]= $nuevafecha60; // 
$arr_grpetereos_fingrupofamiliar[12]= $nuevafecha65; // 
$arr_grpetereos_inigrupofamiliar[13]= $nuevafecha65; // 
$arr_grpetereos_fingrupofamiliar[13]= $nuevafecha70; // 
$arr_grpetereos_inigrupofamiliar[14]= $nuevafecha70; // 
$arr_grpetereos_fingrupofamiliar[14]= $nuevafecha100; //




$arr_ragosgrupofamiliar[0]= "0 A 4"; // 
$arr_ragosgrupofamiliar[1]= "5 A 9"; //  
$arr_ragosgrupofamiliar[2]= "10 A 14"; // 
$arr_ragosgrupofamiliar[3]= "15 A 19"; // 
$arr_ragosgrupofamiliar[4]= "20 A 24"; // 
$arr_ragosgrupofamiliar[5]= "25 A 29"; // 
$arr_ragosgrupofamiliar[6]= "30 A 34"; // 
$arr_ragosgrupofamiliar[7]= "35 A 39"; // 
$arr_ragosgrupofamiliar[8]= "40 A 44"; // 
$arr_ragosgrupofamiliar[9]= "45 A 49"; //
$arr_ragosgrupofamiliar[10]= "50 A 54"; //
$arr_ragosgrupofamiliar[11]= "55 A 59"; //
$arr_ragosgrupofamiliar[12]= "60 A 64"; //
$arr_ragosgrupofamiliar[13]= "65 A 69"; //
$arr_ragosgrupofamiliar[14]= "70 &oacute; MAS"; //


$arr_parentesco[0]= '35'; // 
$arr_parentesco[1]= '36'; // 
$arr_parentesco[2]= '37'; //
$arr_parentesco[3]= '34'; // 


$arr_ragosparentesco[0]= "HIJOS"; // 
$arr_ragosparentesco[1]= "PADRES"; //  
$arr_ragosparentesco[2]= "HERMANOS"; // 
$arr_ragosparentesco[3]= "CONYUGE"; // 

?>
<style type="text/css">
.Estilo14 {font-size: 10px; font-weight: bold; }
.Estilo15 {font-size: 10px; }
</style>
<table align="center" border="1" cellpadding="0" cellspacing="0" width="70%" class="tablero">
  <tr>
    <td colspan="4" align="left"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td align="center"><img src="http://localhost/sigas/fonede/imagenes/logocomfamiliar.jpg" width="174" height="49" /></td>
        </tr>
    </table></td>
    <td colspan="13" align="left" valign="middle" class="Estilo14">MINISTERIO DE LA PROTECCION SOCIAL, DIRECCION GENERAL DE PROMOCION DEL TRABAJO<br />
      SUPERINTENDENCIA DEL SUBSIDIO FAMILIAR<br />
      FONDO PARA FOMENTO DEL EMPLEO Y PROTECCION AL DESEMPLEO  (FONEDE) ( Ley 789 2002)<br />
      CARACTERIZACION SOCIODEMOGRAFICA DE LA POBLACION  ( SUBSIDIOS
      ASIGNADOS
      )<br />
    CAJA DE COMPENSACI&Oacute;N FAMILIAR DEL HUILA - COMFAMILIAR HUILA </td>
  </tr>
  <tr height="25">
    <td width="152" height="109" rowspan="5" align="center"><span class="Estilo14">CONCEPTOS</span></td>
    <td colspan="16"><div class="Estilo14" align="center">MES DE: <strong>
      <?php 
	if($periodo=='01'){ echo "ENERO";}
	if($periodo=='02'){ echo "FEBRERO";}
	if($periodo=='03'){ echo "MARZO";}
	if($periodo=='04'){ echo "ABRIL";}
	if($periodo=='05'){ echo "MAYO";}
	if($periodo=='06'){ echo "JUNIO";}
	if($periodo=='07'){ echo "JULIO";}
	if($periodo=='08'){ echo "AGOSTO";}
	if($periodo=='09'){ echo "SEPTIEMBRE";}
	if($periodo=='10'){ echo "OCTUBRE";}
	if($periodo=='11'){ echo "NOVIEMBRE";}
	if($periodo=='12'){ echo "DICIEMBRE";}	
	?>
    </strong> DE <strong><?php echo $vigencia; ?></strong></div></td>
  </tr>
  <tr height="19">
    <td colspan="8" height="19"><div class="Estilo14" align="center">SUBSIDIOS
      ASIGNADOS
    ( 7.8 EJEC    01 FONEDE )</div></td>
    <td colspan="8"><div class="Estilo14" align="center">SUBSIDIOS
      ASIGNADOS
    ( 8.8 EJEC 01 FONEDE )</div></td>
  </tr>
  <tr height="19">
    <td colspan="8" height="19"><div class="Estilo14" align="center">CON VINCULACION ANTERIOR A CAJAS</div></td>
    <td colspan="8"><div class="Estilo14" align="center">SIN VINCULACION ANTERIOR A CAJAS</div></td>
  </tr>
  <tr height="19">
    <td colspan="4" height="19"><div class="Estilo14" align="center">MUJERES</div></td>
    <td colspan="4"><div class="Estilo14" align="center">HOMBRES</div></td>
    <td colspan="4"><div class="Estilo14" align="center">MUJERES</div></td>
    <td colspan="4"><div class="Estilo14" align="center">HOMBRES</div></td>
  </tr>
  <tr height="27">
    <td height="27" width="4%"><div class="Estilo14" align="center">DESPL.</div></td>
    <td width="6%"><div class="Estilo14" align="center">DISCAP.</div></td>
    <td width="5%"><div class="Estilo14" align="center">REINS.</div></td>
    <td width="7%"><div class="Estilo14" align="center">NINGUNO    DE LOS ANTER.</div></td>
    <td width="4%"><div class="Estilo14" align="center">DESPL.</div></td>
    <td width="6%"><div class="Estilo14" align="center">DISCAP.</div></td>
    <td width="5%"><div class="Estilo14" align="center">REINS.</div></td>
    <td width="7%"><div class="Estilo14" align="center">NINGUNO    DE LOS ANTER.</div></td>
    <td width="4%"><div class="Estilo14" align="center">DESPL.</div></td>
    <td width="6%"><div class="Estilo14" align="center">DISCAP.</div></td>
    <td width="9"><div class="Estilo14" align="center">REINS.</div></td>
    <td width="17"><div class="Estilo14" align="center">NINGUNO    DE LOS ANTER.</div></td>
    <td width="34"><div class="Estilo14" align="center">DESPL.</div></td>
    <td width="69"><div class="Estilo14" align="center">DISCAP.</div></td>
    <td width="138"><div class="Estilo14" align="center">REINS.</div></td>
    <td width="276"><div class="Estilo14" align="center">NINGUNO    DE LOS ANTER.</div></td>
  </tr>
  <tr height="19">
    <td colspan="17" height="19"><span class="Estilo14">1. DATOS DEL JEFE CABEZA DE HOGAR</span></td>
  </tr>
  <tr height="19">
    <td colspan="17" height="19"><span class="Estilo14">1.1 SEXO</span></td>
  </tr>
  <tr height="19">
    <td colspan="17" height="19"><span class="Estilo14">1.2 GRUPOS ETAREOS</span></td>
  </tr>
  <?php
	$total= array();
	
	  for ($grupo = 0; $grupo <= 12; $grupo++) {
		  	$contador_array=0;
				  echo' <tr height="19">';
				  	 echo'<td height="19" width="8%"><span class="Estilo15"><strong>';
					echo  $arr_ragos[$grupo];
					 
					  for ($vinc = 0; $vinc <= 1; $vinc++) {
						  	
						  for ($gen = 0; $gen <= 1; $gen++) {
							  for ($cond = 0; $cond <= 3; $cond++) {	
								  echo '<td height="19" width="4%"><div class="Estilo15" align="center">';	
										for ($valor = 0; $valor <= 1; $valor++) {	
										    
											 $desplazado = $arr_valor_desp[$valor]; 
										 	 $discapacitado=  $arr_valor_disc[$valor];
											 $reinsertado= $arr_valor_rein[$valor];
											 $ninguno=$arr_valor_ning[$valor];											 																		
											 $comparar = "S";
							
							 $row=0;								 
							 if($arr_condicion[$cond]=='Desplazado' and $desplazado!='' and $desplazado!='N')
							    {
								$sql= "select count(aportes015.fechanacimiento) from aportes221,aportes015 
            					where aportes015.idpersona=aportes221.idpersona
								and aportes015.sexo='".$arr_genero[$gen]."'
								and aportes221.vinculacion='".$arr_vinculacion[$vinc]."'
								and aportes015.desplazado='".$desplazado."'
								and aportes221.estado='A'
								and aportes015.fechanacimiento
								BETWEEN '".$arr_grpetereos_fin[$grupo]."' and '".$arr_grpetereos_ini[$grupo]."'";
							
								if ($periodo != '' && $vigencia != '') {
									$sql = $sql . " and SUBSTRING(CAST(fechaasignacion AS varchar(20)), 6, 2)='".$periodo."' and SUBSTRING(CAST(fechaasignacion AS varchar(20)), 1, 4)='".$vigencia."'";
								}  
								//echo'sql= '.$sql."<br/>";	
								$rs=$db->querySimple($sql);	
								$row=$rs->fetchColumn();
								echo $row;								
							}
							else
							if($arr_condicion[$cond]=='Discapacitado' and $discapacitado!='' and $discapacitado!='N')
							{
								$sql1= "select count(aportes015.fechanacimiento) from aportes221,aportes015 
            					where aportes015.idpersona=aportes221.idpersona
								and aportes015.sexo='".$arr_genero[$gen]."'
								and aportes221.vinculacion='".$arr_vinculacion[$vinc]."'
								and aportes015.capacidadtrabajo='".$discapacitado."'
								and aportes221.estado='A'
								and aportes015.fechanacimiento
								BETWEEN '".$arr_grpetereos_fin[$grupo]."' and '".$arr_grpetereos_ini[$grupo]."'";
							
								if ($periodo != '' && $vigencia != '') {
									$sql1 = $sql1 . " and SUBSTRING(CAST(fechaasignacion AS varchar(20)), 6, 2)='".$periodo."' and SUBSTRING(CAST(fechaasignacion AS varchar(20)), 1, 4)='".$vigencia."'";
								}  
								//echo'sql1= '.$sql1."<br/>";	
								$rs1=$db->querySimple($sql1);	
								$row=$rs1->fetchColumn();
								echo $row;					
							}
							else
							if($arr_condicion[$cond]=='Reinsertado' and $reinsertado!='' and $reinsertado!='N')
							{
								$sql2= "select count(aportes015.fechanacimiento) from aportes221,aportes015 
            					where aportes015.idpersona=aportes221.idpersona
								and aportes015.sexo='".$arr_genero[$gen]."'
								and aportes221.vinculacion='".$arr_vinculacion[$vinc]."'
								and aportes015.reinsertado='".$reinsertado."'
								and aportes221.estado='A'
								and aportes015.fechanacimiento
								BETWEEN '".$arr_grpetereos_fin[$grupo]."' and '".$arr_grpetereos_ini[$grupo]."'";
							
								if ($periodo != '' && $vigencia != '') {
									$sql2 = $sql2 . " and SUBSTRING(CAST(fechaasignacion AS varchar(20)), 6, 2)='".$periodo."' and SUBSTRING(CAST(fechaasignacion AS varchar(20)), 1, 4)='".$vigencia."'";
								}  
								//echo'sql2= '.$sql2."<br/>";	
								$rs2=$db->querySimple($sql2);	
								$row=$rs2->fetchColumn();
								echo $row;					
							}
							else
							if($arr_condicion[$cond]=='Ninguno' and $ninguno!='')
							{
								$sql3= "select count(aportes015.fechanacimiento) from aportes221,aportes015 
            					where aportes015.idpersona=aportes221.idpersona
								and aportes015.sexo='".$arr_genero[$gen]."'
								and aportes221.vinculacion='".$arr_vinculacion[$vinc]."'
								and aportes015.desplazado='".$ninguno."'
								and aportes015.capacidadtrabajo='".$ninguno."'
								and aportes015.reinsertado='".$ninguno."'	
								and aportes221.estado='A'							
								and aportes015.fechanacimiento
								BETWEEN '".$arr_grpetereos_fin[$grupo]."' and '".$arr_grpetereos_ini[$grupo]."'";
							
								if ($periodo != '' && $vigencia != '') {
									$sql3 = $sql3 . " and SUBSTRING(CAST(fechaasignacion AS varchar(20)), 6, 2)='".$periodo."' and SUBSTRING(CAST(fechaasignacion AS varchar(20)), 1, 4)='".$vigencia."'";
								}  
								//echo'sql3= '.$sql3."<br/>";	
								$rs3=$db->querySimple($sql3);	
								$row=$rs3->fetchColumn();
								echo $row;																					
							}
							if($desplazado!='N' && $discapacitado!='N' && $reinsertado!='N' && $ninguno!='')
							{
							$temp=$total[$contador_array];
							//echo'<br>valor1='.$total[$contador_array];
							$total[$contador_array]=$temp+$row;
							//echo'<br>valor2='.$total[$contador_array];
							$contador_array=$contador_array+1;								
							}

							   				 if ($cond == "3"){
												//if (($desplazado== "N") && 	($discapacitado== "N") & ($reinsertado== "N")){
												$comparar = "N";
											 }
										}							
										        
								echo '</div></td>';							
							   }
						   }
					  }
			 	 	echo'</strong></span></td>';	
		 	 	 echo' </tr>';	
			  }	
	?>
  <tr height="19">
    <td height="19" width="152"><div class="Estilo15"><strong>TOTAL</strong></div></td>
    <?php 
$longitud=count($total);
 for ($i = 0; $i <= $longitud-1; $i++){
	 ?>
    <td height="19" width="4%"><div class="Estilo15" align="center"><?php echo $total[$i]; ?></div></td>
    <?php
 } 
 
$sumadesplcva=$total[0]+$total[4];
$_SESSION['sumadesplcva']  = $sumadesplcva;

$sumadiscapcva=$total[1]+$total[5];
$_SESSION['sumadiscapcva']  = $sumadiscapcva;

$sumareinscva=$total[2]+$total[6];
$_SESSION['sumareinscva']  = $sumareinscva;


$sumadesplsva=$total[8]+$total[12];
$_SESSION['sumadesplsva']  = $sumadesplsva;

$sumadiscapsva=$total[9]+$total[13];
$_SESSION['sumadiscapsva']  = $sumadiscapsva;

$sumareinssva=$total[10]+$total[14];
$_SESSION['sumareinssva']  = $sumareinssva


?>
  </tr>
  <tr height="19">
    <td colspan="17" height="19"><span class="Estilo15"><strong>1.3 ULTIMO SALARIO DEVENGADO (En salarios m&iacute;nimos)</strong></span></td>
  </tr>
  <?php
	$totalsa= array();
	
	  for ($grupo = 0; $grupo <= 4; $grupo++) {
		  	$contador_arraysa=0;
				  echo' <tr height="19">';
				  	 echo'<td height="19" width="8%"><span class="Estilo15"><strong>';
					echo  $arr_ragossalario[$grupo];
					 
					  for ($vinc = 0; $vinc <= 1; $vinc++) {
						  	
						  for ($gen = 0; $gen <= 1; $gen++) {
							  for ($cond = 0; $cond <= 3; $cond++) {	
								  echo '<td height="19" width="4%"><div class="Estilo15" align="center">';	
										for ($valor = 0; $valor <= 1; $valor++) {	
										    
											 $desplazado = $arr_valor_desp[$valor]; 
										 	 $discapacitado=  $arr_valor_disc[$valor];
											 $reinsertado= $arr_valor_rein[$valor];
											 $ninguno=$arr_valor_ning[$valor];											 																		
											 $comparar = "S";
							
							 $rowsa=0;		 
							 if($arr_condicion[$cond]=='Desplazado' and $desplazado!='' and $desplazado!='N')
							    {
								$sqlsa= "select count(aportes221.cantidadsalario) from aportes221,aportes015 
									 where aportes015.idpersona=aportes221.idpersona 
									 and aportes015.sexo='".$arr_genero[$gen]."'
									 and aportes221.vinculacion='".$arr_vinculacion[$vinc]."'
									 and aportes015.desplazado='".$desplazado."'	
									 and aportes221.estado='A'
									 and aportes221.cantidadsalario 
									 BETWEEN '".$arr_salario_ini[$grupo]."' and '".$arr_salario_fin[$grupo]."'";
							
								if ($periodo != '' && $vigencia != '') {
									$sqlsa = $sqlsa . " and SUBSTRING(CAST(fechaasignacion AS varchar(20)), 6, 2)='".$periodo."' and SUBSTRING(CAST(fechaasignacion AS varchar(20)), 1, 4)='".$vigencia."'";
								}  
								//echo'sqlsa= '.$sqlsa."<br/>";	
								$rssa=$db->querySimple($sqlsa);	
								$rowsa=$rssa->fetchColumn();
								echo $rowsa;							
							}
							else
							if($arr_condicion[$cond]=='Discapacitado' and $discapacitado!='' and $discapacitado!='N')
							{
								$sqlsa1= "select count(aportes221.cantidadsalario) from aportes221,aportes015 
									 where aportes015.idpersona=aportes221.idpersona 
									 and aportes015.sexo='".$arr_genero[$gen]."'
									 and aportes221.vinculacion='".$arr_vinculacion[$vinc]."'									 
								     and aportes015.capacidadtrabajo='".$discapacitado."'
								     and aportes221.estado='A'								     
									 and aportes221.cantidadsalario 
									 BETWEEN '".$arr_salario_ini[$grupo]."' and '".$arr_salario_fin[$grupo]."'";
							
								if ($periodo != '' && $vigencia != '') {
									$sqlsa1 = $sqlsa1 . " and SUBSTRING(CAST(fechaasignacion AS varchar(20)), 6, 2)='".$periodo."' and SUBSTRING(CAST(fechaasignacion AS varchar(20)), 1, 4)='".$vigencia."'";
								}  
								//echo'sqlsa1= '.$sqlsa1."<br/>";	
								$rssa1=$db->querySimple($sqlsa1);	
								$rowsa=$rssa1->fetchColumn();
								echo $rowsa;										
							}
							else
							if($arr_condicion[$cond]=='Reinsertado' and $reinsertado!='' and $reinsertado!='N')
							{
								$sqlsa2= "select count(aportes221.cantidadsalario) from aportes221,aportes015 
									 where aportes015.idpersona=aportes221.idpersona 
									 and aportes015.sexo='".$arr_genero[$gen]."'
									 and aportes221.vinculacion='".$arr_vinculacion[$vinc]."'									
								     and aportes015.reinsertado='".$reinsertado."'	
								     and aportes221.estado='A'
									 and aportes221.cantidadsalario 
									 BETWEEN '".$arr_salario_ini[$grupo]."' and '".$arr_salario_fin[$grupo]."'";
							
								if ($periodo != '' && $vigencia != '') {
									$sqlsa2 = $sqlsa2 . " and SUBSTRING(CAST(fechaasignacion AS varchar(20)), 6, 2)='".$periodo."' and SUBSTRING(CAST(fechaasignacion AS varchar(20)), 1, 4)='".$vigencia."'";
								}  
								//echo'sqlsa2= '.$sqlsa2."<br/>";	
								$rssa2=$db->querySimple($sqlsa2);	
								$rowsa=$rssa2->fetchColumn();
								echo $rowsa;										
							}
							else
							if($arr_condicion[$cond]=='Ninguno' and $ninguno!='')
							{
								$sqlsa3= "select count(aportes221.cantidadsalario) from aportes221,aportes015 
									 where aportes015.idpersona=aportes221.idpersona 
									 and aportes015.sexo='".$arr_genero[$gen]."'
									 and aportes221.vinculacion='".$arr_vinculacion[$vinc]."'
									 and aportes015.desplazado='".$ninguno."'
								     and aportes015.capacidadtrabajo='".$ninguno."'
								     and aportes015.reinsertado='".$ninguno."'	
								     and aportes221.estado='A'
									 and aportes221.cantidadsalario 
									 BETWEEN '".$arr_salario_ini[$grupo]."' and '".$arr_salario_fin[$grupo]."'";						
							
								if ($periodo != '' && $vigencia != '') {
									$sqlsa3 = $sqlsa3 . " and SUBSTRING(CAST(fechaasignacion AS varchar(20)), 6, 2)='".$periodo."' and SUBSTRING(CAST(fechaasignacion AS varchar(20)), 1, 4)='".$vigencia."'";
								}  
								//echo'sqlsa3= '.$sqlsa3."<br/>";	
								$rssa3=$db->querySimple($sqlsa3);	
								$rowsa=$rssa3->fetchColumn();
								echo $rowsa;																
							}
							if($desplazado!='N' && $discapacitado!='N' && $reinsertado!='N' && $ninguno!='')
							{
							$tempsa=$totalsa[$contador_arraysa];
							//echo'<br>valor1='.$totalsa[$contador_arraysa];
							$totalsa[$contador_arraysa]=$tempsa+$rowsa;
							//echo'<br>valor2='.$totalsa[$contador_arraysa];
							$contador_arraysa=$contador_arraysa+1;								
							}

							   				 if ($cond == "3"){
												//if (($desplazado== "N") && 	($discapacitado== "N") & ($reinsertado== "N")){
												$comparar = "N";
											 }
										}							
										        
								echo '</div></td>';							
							   }
						   }
					  }
			 	 	echo'</strong></span></td>';	
		 	 	 echo' </tr>';	
			  }	
	?>
  <tr height="19">
    <td height="19" width="152"><div class="Estilo15"><strong>TOTAL</strong></div></td>
    <?php 
$longitudsa=count($totalsa);
 for ($i = 0; $i <= $longitudsa-1; $i++){
	 ?>
    <td height="19" width="4%"><div class="Estilo15" align="center"><?php echo $totalsa[$i]; ?></div></td>
    <?php
 }
?>
  </tr>
  <tr height="19">
    <td colspan="17" height="19"><span class="Estilo15"><strong>1.4 MAXIMO GRADO DE ESCOLARIDAD ALCANZADO</strong></span></td>
  </tr>
        <?php
	$totale= array();
	
	  for ($grupo = 0; $grupo <= 4; $grupo++) {
		  	$contador_arraye=0;
				  echo' <tr height="19">';
				  	 echo'<td height="19" width="8%"><span class="Estilo15"><strong>';
					echo  $arr_ragosescolaridad[$grupo];
					 
					  for ($vinc = 0; $vinc <= 1; $vinc++) {
						  	
						  for ($gen = 0; $gen <= 1; $gen++) {
							  for ($cond = 0; $cond <= 3; $cond++) {	
								  echo '<td height="19" width="4%"><div class="Estilo15" align="center">';	
										for ($valor = 0; $valor <= 1; $valor++) {	
										    
											 $desplazado = $arr_valor_desp[$valor]; 
										 	 $discapacitado=  $arr_valor_disc[$valor];
											 $reinsertado= $arr_valor_rein[$valor];
											 $ninguno=$arr_valor_ning[$valor];											 																		
											 $comparar = "S";
							
							 $rowe=0;		 
							 if($arr_condicion[$cond]=='Desplazado' and $desplazado!='' and $desplazado!='N')
							    {
								$sqle= "select count(aportes015.idescolaridad) from aportes221,aportes015,aportes091 
									   where aportes015.idpersona=aportes221.idpersona
									   and aportes015.idescolaridad=aportes091.iddetalledef
									   and aportes015.sexo='".$arr_genero[$gen]."'
									   and aportes221.vinculacion='".$arr_vinculacion[$vinc]."'
									   and aportes015.desplazado='".$desplazado."'									   
									   and aportes091.iddetalledef='".$arr_escolaridad[$grupo]."'
								       and aportes221.estado='A'";
							
								if ($periodo != '' && $vigencia != '') {
									$sqle = $sqle . " and SUBSTRING(CAST(fechaasignacion AS varchar(20)), 6, 2)='".$periodo."' and SUBSTRING(CAST(fechaasignacion AS varchar(20)), 1, 4)='".$vigencia."'";
								}  
								//echo'sqle= '.$sqle."<br/>";	
								$rse=$db->querySimple($sqle);	
								$rowe=$rse->fetchColumn();
								echo $rowe;							
							}
							else
							if($arr_condicion[$cond]=='Discapacitado' and $discapacitado!='' and $discapacitado!='N')
							{
								$sqle1= "select count(aportes015.idescolaridad) from aportes221,aportes015,aportes091 
									   where aportes015.idpersona=aportes221.idpersona
									   and aportes015.idescolaridad=aportes091.iddetalledef
									   and aportes015.sexo='".$arr_genero[$gen]."'
									   and aportes221.vinculacion='".$arr_vinculacion[$vinc]."'
									   and aportes015.capacidadtrabajo='".$discapacitado."'									   
									   and aportes091.iddetalledef='".$arr_escolaridad[$grupo]."'
								       and aportes221.estado='A'";
							
								if ($periodo != '' && $vigencia != '') {
									$sqle1 = $sqle1 . " and SUBSTRING(CAST(fechaasignacion AS varchar(20)), 6, 2)='".$periodo."' and SUBSTRING(CAST(fechaasignacion AS varchar(20)), 1, 4)='".$vigencia."'";
								}  
								//echo'sqle1= '.$sqle1."<br/>";	
								$rse1=$db->querySimple($sqle1);	
								$rowe=$rse1->fetchColumn();
								echo $rowe;										
							}
							else
							if($arr_condicion[$cond]=='Reinsertado' and $reinsertado!='' and $reinsertado!='N')
							{
								$sqle2= "select count(aportes015.idescolaridad) from aportes221,aportes015,aportes091 
									   where aportes015.idpersona=aportes221.idpersona
									   and aportes015.idescolaridad=aportes091.iddetalledef
									   and aportes015.sexo='".$arr_genero[$gen]."'
									   and aportes221.vinculacion='".$arr_vinculacion[$vinc]."'
									   and aportes015.reinsertado='".$reinsertado."'									   
									   and aportes091.iddetalledef='".$arr_escolaridad[$grupo]."'
								       and aportes221.estado='A'";
							
								if ($periodo != '' && $vigencia != '') {
									$sqle2 = $sqle2 . " and SUBSTRING(CAST(fechaasignacion AS varchar(20)), 6, 2)='".$periodo."' and SUBSTRING(CAST(fechaasignacion AS varchar(20)), 1, 4)='".$vigencia."'";
								}  
								//echo'sqle2= '.$sqle2."<br/>";	
								$rse2=$db->querySimple($sqle2);	
								$rowe=$rse2->fetchColumn();
								echo $rowe;										
							}
							else
							if($arr_condicion[$cond]=='Ninguno' and $ninguno!='')
							{
								$sqle3= "select count(aportes015.idescolaridad) from aportes221,aportes015,aportes091 
									   where aportes015.idpersona=aportes221.idpersona
									   and aportes015.idescolaridad=aportes091.iddetalledef
									   and aportes015.sexo='".$arr_genero[$gen]."'
									   and aportes221.vinculacion='".$arr_vinculacion[$vinc]."'
									   and aportes015.desplazado='".$ninguno."'
								       and aportes015.capacidadtrabajo='".$ninguno."'
								       and aportes015.reinsertado='".$ninguno."'								   
									   and aportes091.iddetalledef='".$arr_escolaridad[$grupo]."'
								       and aportes221.estado='A'";
							
								if ($periodo != '' && $vigencia != '') {
									$sqle3 = $sqle3 . " and SUBSTRING(CAST(fechaasignacion AS varchar(20)), 6, 2)='".$periodo."' and SUBSTRING(CAST(fechaasignacion AS varchar(20)), 1, 4)='".$vigencia."'";
								}  
								//echo'sqle3= '.$sqle3."<br/>";	
								$rse3=$db->querySimple($sqle3);	
								$rowe=$rse3->fetchColumn();
								echo $rowe;																
							}
							if($desplazado!='N' && $discapacitado!='N' && $reinsertado!='N' && $ninguno!='')
							{
							$tempe=$totale[$contador_arraye];
							//echo'<br>valor1='.$totale[$contador_arraye];
							$totale[$contador_arraye]=$tempe+$rowe;
							//echo'<br>valor2='.$totale[$contador_arraye];
							$contador_arraye=$contador_arraye+1;								
							}

							   				 if ($cond == "3"){
												//if (($desplazado== "N") && 	($discapacitado== "N") & ($reinsertado== "N")){
												$comparar = "N";
											 }
										}							
										        
								echo '</div></td>';							
							   }
						   }
					  }
			 	 	echo'</strong></span></td>';	
		 	 	 echo' </tr>';	
			  }	
	?>
    <tr height="19">
      <td height="19" width="152"><div class="Estilo15"><strong>TOTAL</strong></div></td>
      
<?php 
$longitude=count($totale);
 for ($i = 0; $i <= $longitude-1; $i++){
	 ?>
	<td height="19" width="4%"><div class="Estilo15" align="center"><?php echo $totale[$i]; ?></div></td> 
    <?php
 }
?>
  </tr>
  <tr height="19">
    <td colspan="17" height="19"><span class="Estilo15"><strong>2. INFORMACION GRUPO FAMILIAR</strong></span></td>
  </tr>
  <tr height="19">
    <td colspan="17" height="19"><span class="Estilo15"><strong>2.1 NUMERO DE PERSONAS QUE CONFORMAN EL GRUPO FAMILIAR</strong></span></td>
  </tr>
  <tr height="19">
    <td colspan="17" height="19"><span class="Estilo15"><strong>2.2 EDAD</strong></span></td>
  </tr>
  <?php
	$totalgr= array();
	
	  for ($grupo = 0; $grupo <= 14; $grupo++) {
		  	$contador_arraygr=0;
				  echo' <tr height="19">';
				  	 echo'<td height="19" width="8%"><span class="Estilo15"><strong>';
					echo  $arr_ragosgrupofamiliar[$grupo];
					 
					  for ($vinc = 0; $vinc <= 1; $vinc++) {
						  	
						  for ($gen = 0; $gen <= 1; $gen++) {
							  for ($cond = 0; $cond <= 3; $cond++) {	
								  echo '<td height="19" width="4%"><div class="Estilo15" align="center">';	
										for ($valor = 0; $valor <= 1; $valor++) {	
										    
											 $desplazado = $arr_valor_desp[$valor]; 
										 	 $discapacitado=  $arr_valor_disc[$valor];
											 $reinsertado= $arr_valor_rein[$valor];
											 $ninguno=$arr_valor_ning[$valor];											 																		
											 $comparar = "S";
							
							 $rowgr=0;		 
							 if($arr_condicion[$cond]=='Desplazado' and $desplazado!='' and $desplazado!='N')
							    {
								$sqlgr= "select count(aportes015.fechanacimiento) from aportes221,aportes224,aportes015 
								where aportes015.idpersona=aportes224.idbeneficiario 
								and aportes221.idpersona=aportes224.idtrabajador 
								and aportes015.sexo='".$arr_genero[$gen]."'
								and aportes221.vinculacion='".$arr_vinculacion[$vinc]."'
								and aportes015.desplazado='".$desplazado."'	
								and aportes221.estado='A'							
								and aportes015.fechanacimiento 
								BETWEEN '".$arr_grpetereos_fingrupofamiliar[$grupo]."' and '".$arr_grpetereos_inigrupofamiliar[$grupo]."'";
								 
								if ($periodo != '' && $vigencia != '') {
									$sqlgr = $sqlgr . " and SUBSTRING(CAST(fechaasignacion AS varchar(20)), 6, 2)='".$periodo."' and SUBSTRING(CAST(fechaasignacion AS varchar(20)), 1, 4)='".$vigencia."'";
								}  	
								//echo'sqlgr= '.$sqlgr."<br/>";	
								$rsgr=$db->querySimple($sqlgr);	
								$rowgr=$rsgr->fetchColumn();
								echo $rowgr;							
							}
							else
							if($arr_condicion[$cond]=='Discapacitado' and $discapacitado!='' and $discapacitado!='N')
							{
								$sqlgr1= "select count(aportes015.fechanacimiento) from aportes221,aportes224,aportes015 
								where aportes015.idpersona=aportes224.idbeneficiario 
								and aportes221.idpersona=aportes224.idtrabajador 
								and aportes015.sexo='".$arr_genero[$gen]."'
								and aportes221.vinculacion='".$arr_vinculacion[$vinc]."'
								and aportes015.capacidadtrabajo='".$discapacitado."'	
								and aportes221.estado='A'							
								and aportes015.fechanacimiento 
								BETWEEN '".$arr_grpetereos_fingrupofamiliar[$grupo]."' and '".$arr_grpetereos_inigrupofamiliar[$grupo]."'";
								 
								if ($periodo != '' && $vigencia != '') {
									$sqlgr1 = $sqlgr1 . " and SUBSTRING(CAST(fechaasignacion AS varchar(20)), 6, 2)='".$periodo."' and SUBSTRING(CAST(fechaasignacion AS varchar(20)), 1, 4)='".$vigencia."'";
								} 							
								//echo'sqlgr1= '.$sqlgr1."<br/>";	
								$rsgr1=$db->querySimple($sqlgr1);	
								$rowgr=$rsgr1->fetchColumn();
								echo $rowgr;					
							}
							else
							if($arr_condicion[$cond]=='Reinsertado' and $reinsertado!='' and $reinsertado!='N')
							{
								$sqlgr2= "select count(aportes015.fechanacimiento) from aportes221,aportes224,aportes015 
								where aportes015.idpersona=aportes224.idbeneficiario 
								and aportes221.idpersona=aportes224.idtrabajador 
								and aportes015.sexo='".$arr_genero[$gen]."'
								and aportes221.vinculacion='".$arr_vinculacion[$vinc]."'
								and aportes015.reinsertado='".$reinsertado."'	
								and aportes221.estado='A'							
								and aportes015.fechanacimiento 
								BETWEEN '".$arr_grpetereos_fingrupofamiliar[$grupo]."' and '".$arr_grpetereos_inigrupofamiliar[$grupo]."'";
								 
								if ($periodo != '' && $vigencia != '') {
									$sqlgr2 = $sqlgr2 . " and SUBSTRING(CAST(fechaasignacion AS varchar(20)), 6, 2)='".$periodo."' and SUBSTRING(CAST(fechaasignacion AS varchar(20)), 1, 4)='".$vigencia."'";
								} 
								//echo'sqlgr2= '.$sqlgr2."<br/>";	
								$rsgr2=$db->querySimple($sqlgr2);	
								$rowgr=$rsgr2->fetchColumn();
								echo $rowgr;					
							}
							else
							if($arr_condicion[$cond]=='Ninguno' and $ninguno!='')
							{
								$sqlgr3= "select count(aportes015.fechanacimiento) from aportes221,aportes224,aportes015 
								where aportes015.idpersona=aportes224.idbeneficiario 
								and aportes221.idpersona=aportes224.idtrabajador 
								and aportes015.sexo='".$arr_genero[$gen]."'
								and aportes221.vinculacion='".$arr_vinculacion[$vinc]."'
								and aportes015.desplazado='".$ninguno."'
								and aportes015.capacidadtrabajo='".$ninguno."'
								and aportes015.reinsertado='".$ninguno."'	
								and aportes221.estado='A'							
								and aportes015.fechanacimiento 
								BETWEEN '".$arr_grpetereos_fingrupofamiliar[$grupo]."' and '".$arr_grpetereos_inigrupofamiliar[$grupo]."'";
								 
								if ($periodo != '' && $vigencia != '') {
									$sqlgr3 = $sqlgr3 . " and SUBSTRING(CAST(fechaasignacion AS varchar(20)), 6, 2)='".$periodo."' and SUBSTRING(CAST(fechaasignacion AS varchar(20)), 1, 4)='".$vigencia."'";
								} 
								//echo'sqlgr3= '.$sqlgr3."<br/>";	
								$rsgr3=$db->querySimple($sqlgr3);	
								$rowgr=$rsgr3->fetchColumn();
								echo $rowgr;																					
							}
							if($desplazado!='N' && $discapacitado!='N' && $reinsertado!='N' && $ninguno!='')
							{
							$tempgr=$totalgr[$contador_arraygr];
							//echo'<br>valor1='.$totalgr[$contador_arraygr];
							$totalgr[$contador_arraygr]=$tempgr+$rowgr;
							//echo'<br>valor2='.$totalgr[$contador_arraygr];
							$contador_arraygr=$contador_arraygr+1;								
							}

							   				 if ($cond == "3"){
												//if (($desplazado== "N") && 	($discapacitado== "N") & ($reinsertado== "N")){
												$comparar = "N";
											 }
										}							
										        
								echo '</div></td>';							
							   }
						   }
					  }
			 	 	echo'</strong></span></td>';	
		 	 	 echo' </tr>';	
			  }	
	?>
  <tr height="19">
    <td height="19" width="152"><div class="Estilo15"><strong>TOTAL</strong></div></td>
    <?php 
$longitudgr=count($totalgr);
 for ($i = 0; $i <= $longitudgr-1; $i++){
	 ?>
    <td height="19" width="4%"><div class="Estilo15" align="center"><?php echo $totalgr[$i]; ?></div></td>
    <?php
 }
?>
  </tr>
  <tr height="19">
    <td colspan="17" height="19"><span class="Estilo15"><strong>2.3 PARENTESCO</strong></span></td>
  </tr>
  <?php
	$totalp= array();
	
	  for ($grupo = 0; $grupo <= 3; $grupo++) {
		  	$contador_arrayp=0;
				  echo' <tr height="19">';
				  	 echo'<td height="19" width="8%"><span class="Estilo15"><strong>';
					echo  $arr_ragosparentesco[$grupo];
					 
					  for ($vinc = 0; $vinc <= 1; $vinc++) {
						  	
						  for ($gen = 0; $gen <= 1; $gen++) {
							  for ($cond = 0; $cond <= 3; $cond++) {	
								  echo '<td height="19" width="4%"><div class="Estilo15" align="center">';	
										for ($valor = 0; $valor <= 1; $valor++) {	
										    
											 $desplazado = $arr_valor_desp[$valor]; 
										 	 $discapacitado=  $arr_valor_disc[$valor];
											 $reinsertado= $arr_valor_rein[$valor];
											 $ninguno=$arr_valor_ning[$valor];											 																		
											 $comparar = "S";
							
							 $rowe=0;		 
							 if($arr_condicion[$cond]=='Desplazado' and $desplazado!='' and $desplazado!='N')
							    {
								$sqlp= "select count(aportes224.idparentesco) from aportes221,aportes224,aportes015,aportes091  
									   where aportes015.idpersona=aportes224.idbeneficiario 
									   and aportes221.idpersona=aportes224.idtrabajador 
									   and aportes224.idparentesco=aportes091.iddetalledef
									   and aportes015.sexo='".$arr_genero[$gen]."'
									   and aportes221.vinculacion='".$arr_vinculacion[$vinc]."'
									   and aportes015.desplazado='".$desplazado."'								   								   
									   and aportes091.iddetalledef='".$arr_parentesco[$grupo]."'
								       and aportes221.estado='A'";	
							
								if ($periodo != '' && $vigencia != '') {
									$sqlp = $sqlp . " and SUBSTRING(CAST(fechaasignacion AS varchar(20)), 6, 2)='".$periodo."' and SUBSTRING(CAST(fechaasignacion AS varchar(20)), 1, 4)='".$vigencia."'";
								}  
								//echo'sqlp= '.$sqlp."<br/>";	
								$rsp=$db->querySimple($sqlp);	
								$rowp=$rsp->fetchColumn();
								echo $rowp;							
							}
							else
							if($arr_condicion[$cond]=='Discapacitado' and $discapacitado!='' and $discapacitado!='N')
							{
								$sqlp1= "select count(aportes224.idparentesco) from aportes221,aportes224,aportes015,aportes091  
									   where aportes015.idpersona=aportes224.idbeneficiario 
									   and aportes221.idpersona=aportes224.idtrabajador 
									   and aportes224.idparentesco=aportes091.iddetalledef
									   and aportes015.sexo='".$arr_genero[$gen]."'
									   and aportes221.vinculacion='".$arr_vinculacion[$vinc]."'									   
								       and aportes015.capacidadtrabajo='".$discapacitado."'						     								   
									   and aportes091.iddetalledef='".$arr_parentesco[$grupo]."'
								       and aportes221.estado='A'";	
							
								if ($periodo != '' && $vigencia != '') {
									$sqlp1 = $sqlp1 . " and SUBSTRING(CAST(fechaasignacion AS varchar(20)), 6, 2)='".$periodo."' and SUBSTRING(CAST(fechaasignacion AS varchar(20)), 1, 4)='".$vigencia."'";
								}  
								//echo'sqlp1= '.$sqlp1."<br/>";	
								$rsp1=$db->querySimple($sqlp1);	
								$rowp=$rsp1->fetchColumn();
								echo $rowp;										
							}
							else
							if($arr_condicion[$cond]=='Reinsertado' and $reinsertado!='' and $reinsertado!='N')
							{
								$sqlp2= "select count(aportes224.idparentesco) from aportes221,aportes224,aportes015,aportes091  
									   where aportes015.idpersona=aportes224.idbeneficiario 
									   and aportes221.idpersona=aportes224.idtrabajador 
									   and aportes224.idparentesco=aportes091.iddetalledef
									   and aportes015.sexo='".$arr_genero[$gen]."'
									   and aportes221.vinculacion='".$arr_vinculacion[$vinc]."'									   
								       and aportes015.reinsertado='".$reinsertado."'								   
									   and aportes091.iddetalledef='".$arr_parentesco[$grupo]."'
								       and aportes221.estado='A'";	
							
								if ($periodo != '' && $vigencia != '') {
									$sqlp2 = $sqlp2 . " and SUBSTRING(CAST(fechaasignacion AS varchar(20)), 6, 2)='".$periodo."' and SUBSTRING(CAST(fechaasignacion AS varchar(20)), 1, 4)='".$vigencia."'";
								}  
								//echo'sqlp2= '.$sqlp2."<br/>";	
								$rsp2=$db->querySimple($sqlp2);	
								$rowp=$rsp2->fetchColumn();
								echo $rowp;										
							}
							else
							if($arr_condicion[$cond]=='Ninguno' and $ninguno!='')
							{
								$sqlp3= "select count(aportes224.idparentesco) from aportes221,aportes224,aportes015,aportes091  
									   where aportes015.idpersona=aportes224.idbeneficiario 
									   and aportes221.idpersona=aportes224.idtrabajador 
									   and aportes224.idparentesco=aportes091.iddetalledef
									   and aportes015.sexo='".$arr_genero[$gen]."'
									   and aportes221.vinculacion='".$arr_vinculacion[$vinc]."'
									   and aportes015.desplazado='".$ninguno."'
								       and aportes015.capacidadtrabajo='".$ninguno."'
								       and aportes015.reinsertado='".$ninguno."'								   
									   and aportes091.iddetalledef='".$arr_parentesco[$grupo]."'
							           and aportes221.estado='A'";								   
							
								if ($periodo != '' && $vigencia != '') {
									$sqlp3 = $sqlp3 . " and SUBSTRING(CAST(fechaasignacion AS varchar(20)), 6, 2)='".$periodo."' and SUBSTRING(CAST(fechaasignacion AS varchar(20)), 1, 4)='".$vigencia."'";
								}  
								//echo'sqlp3= '.$sqlp3."<br/>";	
								$rsp3=$db->querySimple($sqlp3);	
								$rowp=$rsp3->fetchColumn();
								echo $rowp;																
							}
							if($desplazado!='N' && $discapacitado!='N' && $reinsertado!='N' && $ninguno!='')
							{
							$tempp=$totalp[$contador_arrayp];
							//echo'<br>valor1='.$totalp[$contador_arrayp];
							$totalp[$contador_arrayp]=$tempp+$rowp;
							//echo'<br>valor2='.$totalp[$contador_arrayp];
							$contador_arrayp=$contador_arrayp+1;								
							}

							   				 if ($cond == "3"){
												//if (($desplazado== "N") && 	($discapacitado== "N") & ($reinsertado== "N")){
												$comparar = "N";
											 }
										}							
										        
								echo '</div></td>';							
							   }
						   }
					  }
			 	 	echo'</strong></span></td>';	
		 	 	 echo' </tr>';	
			  }	
	?>
  <tr height="19">
    <td height="19" width="152"><div class="Estilo15"><strong>TOTAL</strong></div></td>
    <?php 
$longitudp=count($totalp);
 for ($i = 0; $i <= $longitudp-1; $i++){
	 ?>
    <td height="19" width="4%"><div class="Estilo15" align="center"><?php echo $totalp[$i]; ?></div></td>
    <?php
 }
?>
  </tr>
  <tr height="19">
    <td colspan="17" height="19"><span class="Estilo15"><strong>2.4 MAXIMO GRADO DE ESCOLARIDAD ALCANZADO</strong></span></td>
  </tr>
        <?php
	$totalegr= array();
	
	  for ($grupo = 0; $grupo <= 4; $grupo++) {
		  	$contador_arrayegr=0;
				  echo' <tr height="19">';
				  	 echo'<td height="19" width="8%"><span class="Estilo15"><strong>';
					echo  $arr_ragosescolaridad[$grupo];
					 
					  for ($vinc = 0; $vinc <= 1; $vinc++) {
						  	
						  for ($gen = 0; $gen <= 1; $gen++) {
							  for ($cond = 0; $cond <= 3; $cond++) {	
								  echo '<td height="19" width="4%"><div class="Estilo15" align="center">';	
										for ($valor = 0; $valor <= 1; $valor++) {	
										    
											 $desplazado = $arr_valor_desp[$valor]; 
										 	 $discapacitado=  $arr_valor_disc[$valor];
											 $reinsertado= $arr_valor_rein[$valor];
											 $ninguno=$arr_valor_ning[$valor];											 																		
											 $comparar = "S";
							
							 $rowegr=0;		 
							 if($arr_condicion[$cond]=='Desplazado' and $desplazado!='' and $desplazado!='N')
							    {
								$sqlegr= "select count(aportes015.idescolaridad) from aportes221,aportes224,aportes015,aportes091 
									   where aportes015.idpersona=aportes224.idbeneficiario 
									   and aportes221.idpersona=aportes224.idtrabajador
									   and aportes015.idescolaridad=aportes091.iddetalledef
									   and aportes015.sexo='".$arr_genero[$gen]."'
									   and aportes221.vinculacion='".$arr_vinculacion[$vinc]."'
									   and aportes015.desplazado='".$desplazado."'									   
									   and aportes091.iddetalledef='".$arr_escolaridad[$grupo]."'
								       and aportes221.estado='A'";
							
								if ($periodo != '' && $vigencia != '') {
									$sqlegr = $sqlegr . " and SUBSTRING(CAST(fechaasignacion AS varchar(20)), 6, 2)='".$periodo."' and SUBSTRING(CAST(fechaasignacion AS varchar(20)), 1, 4)='".$vigencia."'";
								}  
								//echo'sqlegr= '.$sqlegr."<br/>";	
								$rsegr=$db->querySimple($sqlegr);	
								$rowegr=$rsegr->fetchColumn();
								echo $rowegr;							
							}
							else
							if($arr_condicion[$cond]=='Discapacitado' and $discapacitado!='' and $discapacitado!='N')
							{
								$sqlegr1= "select count(aportes015.idescolaridad) from aportes221,aportes224,aportes015,aportes091 
									   where aportes015.idpersona=aportes224.idbeneficiario 
									   and aportes221.idpersona=aportes224.idtrabajador
									   and aportes015.idescolaridad=aportes091.iddetalledef
									   and aportes015.sexo='".$arr_genero[$gen]."'
									   and aportes221.vinculacion='".$arr_vinculacion[$vinc]."'
									   and aportes015.capacidadtrabajo='".$discapacitado."'									   
									   and aportes091.iddetalledef='".$arr_escolaridad[$grupo]."'
								       and aportes221.estado='A'";
							
								if ($periodo != '' && $vigencia != '') {
									$sqlegr1 = $sqlegr1 . " and SUBSTRING(CAST(fechaasignacion AS varchar(20)), 6, 2)='".$periodo."' and SUBSTRING(CAST(fechaasignacion AS varchar(20)), 1, 4)='".$vigencia."'";
								}  
								//echo'sqlegr1= '.$sqlegr1."<br/>";	
								$rsegr1=$db->querySimple($sqlegr1);	
								$rowegr=$rsegr1->fetchColumn();
								echo $rowegr;										
							}
							else
							if($arr_condicion[$cond]=='Reinsertado' and $reinsertado!='' and $reinsertado!='N')
							{
								$sqlegr2= "select count(aportes015.idescolaridad) from aportes221,aportes224,aportes015,aportes091 
									   where aportes015.idpersona=aportes224.idbeneficiario 
									   and aportes221.idpersona=aportes224.idtrabajador
									   and aportes015.idescolaridad=aportes091.iddetalledef
									   and aportes015.sexo='".$arr_genero[$gen]."'
									   and aportes221.vinculacion='".$arr_vinculacion[$vinc]."'
									   and aportes015.reinsertado='".$reinsertado."'									   
									   and aportes091.iddetalledef='".$arr_escolaridad[$grupo]."'
								       and aportes221.estado='A'";
							
								if ($periodo != '' && $vigencia != '') {
									$sqlegr2 = $sqlegr2 . " and SUBSTRING(CAST(fechaasignacion AS varchar(20)), 6, 2)='".$periodo."' and SUBSTRING(CAST(fechaasignacion AS varchar(20)), 1, 4)='".$vigencia."'";
								}  
								//echo'sqlegr2= '.$sqlegr2."<br/>";	
								$rsegr2=$db->querySimple($sqlegr2);	
								$rowegr=$rsegr2->fetchColumn();
								echo $rowegr;										
							}
							else
							if($arr_condicion[$cond]=='Ninguno' and $ninguno!='')
							{
								$sqlegr3= "select count(aportes015.idescolaridad) from aportes221,aportes224,aportes015,aportes091  
										where aportes015.idpersona=aportes224.idbeneficiario 
										and aportes221.idpersona=aportes224.idtrabajador 
										and aportes015.idescolaridad=aportes091.iddetalledef
										and aportes015.sexo='".$arr_genero[$gen]."'
										and aportes221.vinculacion='".$arr_vinculacion[$vinc]."'
									    and aportes015.desplazado='".$ninguno."'
								        and aportes015.capacidadtrabajo='".$ninguno."'
								        and aportes015.reinsertado='".$ninguno."'	
										and aportes091.iddetalledef='".$arr_escolaridad[$grupo]."'
								        and aportes221.estado='A'";								   
							
								if ($periodo != '' && $vigencia != '') {
									$sqlegr3 = $sqlegr3 . " and SUBSTRING(CAST(fechaasignacion AS varchar(20)), 6, 2)='".$periodo."' and SUBSTRING(CAST(fechaasignacion AS varchar(20)), 1, 4)='".$vigencia."'";
								}  
								//echo'sqlegr3= '.$sqlegr3."<br/>";	
								$rsegr3=$db->querySimple($sqlegr3);	
								$rowegr=$rsegr3->fetchColumn();
								echo $rowegr;																
							}
							if($desplazado!='N' && $discapacitado!='N' && $reinsertado!='N' && $ninguno!='')
							{
							$tempegr=$totalegr[$contador_arrayegr];
							//echo'<br>valor1='.$totalegr[$contador_arrayegr];
							$totalegr[$contador_arrayegr]=$tempegr+$rowegr;
							//echo'<br>valor2='.$totalegr[$contador_arrayegr];
							$contador_arrayegr=$contador_arrayegr+1;								
							}

							   				 if ($cond == "3"){
												//if (($desplazado== "N") && 	($discapacitado== "N") & ($reinsertado== "N")){
												$comparar = "N";
											 }
										}							
										        
								echo '</div></td>';							
							   }
						   }
					  }
			 	 	echo'</strong></span></td>';	
		 	 	 echo' </tr>';	
			  }	
	?>
    <tr height="19">
      <td height="19" width="152"><div class="Estilo15"><strong>TOTAL</strong></div></td>
      
<?php 
$longitudegr=count($totalegr);
 for ($i = 0; $i <= $longitudegr-1; $i++){
	 ?>
	<td height="19" width="4%"><div class="Estilo15" align="center"><?php echo $totalegr[$i]; ?></div></td> 
   <?php
 }
?>
  </tr>
</table>
<p>&nbsp;</p>
<table frame="void" rules="none" align="center" border="0" cellspacing="0" cols="15">
  <tbody>
    <tr>
      <td colspan="5" align="center" bgcolor="#FFFFFF" valign="middle">________________________</td>
      <td colspan="7" align="left" bgcolor="#FFFFFF" width="203">&nbsp;</td>
      <td colspan="5" align="center" bgcolor="#FFFFFF" valign="middle">________________________</td>
    </tr>
    <tr>
      <td colspan="5" align="center" bgcolor="#FFFFFF" valign="middle" width="203"><strong>DIRECTOR ADMINISTRATIVO </strong></td>
      <td align="left" bgcolor="#FFFFFF" width="30"><strong><br />
      </strong></td>
      <td align="left" bgcolor="#FFFFFF" width="30"><strong><br />
      </strong></td>
      <td align="left" bgcolor="#FFFFFF" width="30"><strong><br />
      </strong></td>
      <td align="left" bgcolor="#FFFFFF" width="30"><strong><br />
      </strong></td>
      <td align="left" bgcolor="#FFFFFF" width="30"><strong><br />
      </strong></td>
      <td align="left" bgcolor="#FFFFFF" width="30"><strong><br />
      </strong></td>
      <td align="left" bgcolor="#FFFFFF" width="30"><strong><br />
      </strong></td>
      <td colspan="5" align="center" bgcolor="#FFFFFF" valign="middle" width="203"><strong>REVISOR FISCAL</strong></td>
    </tr>
  </tbody>
</table>
<p>&nbsp;</p>
<?php
      }//fin if $_GET['ok'] == 'ok'
      else {
        ?>    
      <?php
      }//fin else if $_GET['ok'] == 'ok'
      ?>