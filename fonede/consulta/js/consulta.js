// JavaScript Document
var error=0;

function setCookie(c_name,value,exdays)
{
	var exdate=new Date();
	exdate.setDate(exdate.getDate() + exdays);
	var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
	document.cookie=c_name + "=" + c_value;
}// fin setCookie

// funcion para obtener cookie en javascript
	function getCookie(c_name)
	{
		var i,x,y,ARRcookies=document.cookie.split(";");
		for (i=0;i<ARRcookies.length;i++)
		{
  			x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
  			y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
  			x=x.replace(/^\s+|\s+$/g,"");
  			if (x==c_name)
    			{
    				return unescape(y);
    			}// fin if
		}// fin for
  	}// fin getCookie
//funcion para obtener cookie en javascript

function borrarcookieconsulta(){	
	
		 document.getElementById('identificacionconsulta').value='';
		 document.getElementById('numeroradicacionconsulta').innerHTML= '';	 
		 document.getElementById('afiliacionconsulta').innerHTML= '';
		 document.getElementById('consefonedeconsulta').innerHTML='';	
		 document.getElementById('horasolicituconsulta').innerHTML='';
		 document.getElementById('fecharadicacionconsulta').innerHTML='';
		 document.getElementById('fechasolicituconsulta').innerHTML='';
		 document.getElementById('seccionalconsulta').innerHTML='';
		 document.getElementById('tiempoconsulta').innerHTML='';
		 document.getElementById('tipovinculacionconsulta').innerHTML='';
		 document.getElementById('documentosconsulta').innerHTML='';
		 document.getElementById('aptoconsulta').innerHTML='';
		 document.getElementById('estadoconsulta').innerHTML='';
		 document.getElementById('observaciones').innerHTML='';		 		 
		 document.getElementById('tipodocumentoconsulta').innerHTML= '';
		 document.getElementById('identificaionconsulta').innerHTML= '';
		 document.getElementById('papellidoconsulta').innerHTML='';
		 document.getElementById('sapellidoconsulta').innerHTML='';
		 document.getElementById('pnombreconsulta').innerHTML='';
		 document.getElementById('snombreconsulta').innerHTML='';
		 document.getElementById('fechanaceconsulta').innerHTML='';
		 document.getElementById('generoconsulta').innerHTML='';
		 document.getElementById('direccionconsulta').innerHTML='';
		 document.getElementById('telefonoconsulta').innerHTML='';
		 document.getElementById('estadocivilconsulta').innerHTML='';
		 document.getElementById('escolaridadconsulta').innerHTML='';		 
		 document.getElementById('artistaconsulta').innerHTML= '';
		 document.getElementById('deportistaconsulta').innerHTML= '';
		 document.getElementById('escritorconsulta').innerHTML= '';
		 document.getElementById('tipodocumentoconsultaconyugue').innerHTML='';
		 document.getElementById('identificaionconsultaconyugue').innerHTML='';
		 document.getElementById('papellidoconsultaconyugue').innerHTML='';
		 document.getElementById('sapellidoconsultaconyugue').innerHTML='';
		 document.getElementById('pnombreconsultaconyugue').innerHTML='';
		 document.getElementById('snombreconsultaconyugue').innerHTML='';
		 document.getElementById('fechanaceconsultaconyugue').innerHTML='';
		 document.getElementById('hijosmenoresconsulta').innerHTML='';
		 document.getElementById('personasacargoconsulta').innerHTML='';
		 document.getElementById('procesoconsulta').innerHTML='';
		 document.getElementById('observacionesgenerales').innerHTML='';
		 setCookie('idpersonaconsulta','',-1);  
		 		 
		 document.getElementById("oculto").style.display="none";	
		 
		 	
	
	     //setTimeout('document.location.reload()',500);
    }// borrarcookieconsulta

function refrescarpagina(){
	location.reload();
}
	
	
	
//------------------------------------- Busqueda Postulado----------------------------------------------------------------------------------------------//

function busquedacompletaconsulta(){
		
   		var identificacionconsulta = $("#identificacionconsulta").val();
		var tipoidentificacionconsulta = $("#tipoidentificacionconsulta").val();	
			

		$.getJSON('busquedaconsulta.php',{v0:identificacionconsulta,v1:tipoidentificacionconsulta},function(datos){
			
			$.each(datos,function(i,fila){						
				
               	document.getElementById('numeroradicacionconsulta').innerHTML=fila.idradicacion;	
				document.getElementById('afiliacionconsulta').innerHTML=fila.afiliacionesotrascajas;
				document.getElementById('consefonedeconsulta').innerHTML=fila.consecutivo;	
				
				var horamodificar=fila.horagrabacion;				
				arreglohora = horamodificar.split('.');
				horaqueda=arreglohora[0];				
				document.getElementById('horasolicituconsulta').innerHTML=horaqueda;	
				
				
				document.getElementById('fecharadicacionconsulta').innerHTML=fila.fecharadicacion;
				document.getElementById('fechasolicituconsulta').innerHTML=fila.fechagrabacion;				
				document.getElementById('seccionalconsulta').innerHTML=fila.agencia;
				document.getElementById('tiempoconsulta').innerHTML=fila.tiempo;
				
				vinculacion=fila.vinculacion;
				
				if(vinculacion==1)
					{
						vinculacion="Sin Vinculacion";
					}// fin if vinculacion==1
				else
				if(vinculacion==2)
				    {
					  vinculacion="Con Vinculacion";	
					}//fin vinculacion==2				
				document.getElementById('tipovinculacionconsulta').innerHTML=vinculacion;
				
				
			    documentos=fila.documentos;				
					if(documentos==1)
					{
						documentos='S';
					}// fin if
					else
						{
							documentos='N';
						}// fin else	
				document.getElementById('documentosconsulta').innerHTML=documentos;
				
				apto=fila.apto;				
					if(apto==1)
					{
						apto='S';
					}// fin if
					else
						{
							apto='N';
						}	// fin else			
				document.getElementById('aptoconsulta').innerHTML=apto;
				
				
				document.getElementById('estadoconsulta').innerHTML=fila.estado;
				document.getElementById('observaciones').innerHTML=fila.observacionesestado;		
				document.getElementById('tipodocumentoconsulta').innerHTML=fila.detalledefinicion;	
				document.getElementById('identificaionconsulta').innerHTML=fila.identificacion;	
				document.getElementById('papellidoconsulta').innerHTML=fila.papellido;
				document.getElementById('sapellidoconsulta').innerHTML=fila.sapellido;
				document.getElementById('pnombreconsulta').innerHTML=fila.pnombre;
				document.getElementById('snombreconsulta').innerHTML=fila.snombre;
				document.getElementById('fechanaceconsulta').innerHTML=fila.fechanacimiento;
				
				genero=fila.sexo;				
					if(genero=='M')
					{
						genero='MASCULINO';
					}// fin if
					else
					if(genero=='F')
						{
							genero='FEMENINO';
						}// fin if	
				document.getElementById('generoconsulta').innerHTML=genero;
				
				
				document.getElementById('direccionconsulta').innerHTML=fila.direccion;
				document.getElementById('telefonoconsulta').innerHTML=fila.telefono;
				busquedadetalleconsultaestadocivil(fila.idestadocivil);
				busquedadetalleconsultaescolaridad(fila.idescolaridad);
				
				document.getElementById('artistaconsulta').innerHTML=fila.artista;
				document.getElementById('deportistaconsulta').innerHTML=fila.deportista;
				document.getElementById('escritorconsulta').innerHTML=fila.escritor;
								
				busquedaconsultaconvenio(fila.idconvenio);	
				busquedadetalleconsultaeps(fila.ideps);		
				busquedadetalleentidadeducativa(fila.identidadeducativa);
				busquedaconsultaconyugue(fila.idpersona);		
				setCookie("idpersonaconsulta", fila.idpersona, 1);
						
				document.getElementById("oculto").style.display="block";
				
				$("#beneficiario").load('busquedabeneficiarios.php');	
				
				document.getElementById('hijosmenoresconsulta').innerHTML=fila.hijosmenores;
		        document.getElementById('personasacargoconsulta').innerHTML=fila.personasacargo;
				var proceso=fila.proceso;
				var nombreproceso;
				
				if(proceso=='In')
				{
				   nombreproceso='Inicial';
				}
				
				if(proceso=='Ra')
				{
				   nombreproceso='Radicacion';
				}				
				if(proceso=='Gr')
				{
				   nombreproceso='Grabacion';
				}
				if(proceso=='Po')
				{
				   nombreproceso='Postulacion';
				}
				if(proceso=='As')
				{
				   nombreproceso='Asignacion';
				}
				if(proceso=='Gi')
				{
				   nombreproceso='Giro';
				}				
				document.getElementById('procesoconsulta').innerHTML=nombreproceso;
				
				document.getElementById('observacionesgenerales').innerHTML=fila.observacionesgenerales;
				
				$("#giros").load('busquedagiros.php');	
				$("#giroactual").load('busquedagiroactual.php');	
		
				return;
			});
		
			if(datos==0)
			{
			   borrarcookieconsulta();			  
			}
		
		})		
	}// fin busquedacompletaconsulta
	
function busquedadetalleconsultaestadocivil(iddetalledefestadocivil){
		    $.getJSON('busquedadetalles.php',{v0:iddetalledefestadocivil},function(datos){		           		
			$.each(datos,function(i,fila){	
			    document.getElementById('estadocivilconsulta').innerHTML=fila.detalledefinicion;
				return;
			});
		})		
	}// fin busquedadetalleconsultaestadocivil
	
function busquedadetalleconsultaescolaridad(iddetalledefescolaridad){
		    $.getJSON('busquedadetalles.php',{v0:iddetalledefescolaridad},function(datos){		           		
			$.each(datos,function(i,fila){	
			    document.getElementById('escolaridadconsulta').innerHTML=fila.detalledefinicion;
				return;
			});
		})		
	}// fin busquedadetalleconsultaescolaridad
	
function busquedadetalleconsultaeps(iddetalledefeps){
		    $.getJSON('busquedaepsarp.php',{v0:iddetalledefeps},function(datos){		           		
			$.each(datos,function(i,fila){	
			    document.getElementById('epsconsulta').innerHTML=fila.razonsocial;
				return;
			});
		})		
	}// fin busquedadetalleconsultaeps
	
function busquedadetalleentidadeducativa(iddetalledefentidadeducativa){
		    $.getJSON('busquedadetalles.php',{v0:iddetalledefentidadeducativa},function(datos){		           		
			$.each(datos,function(i,fila){	
			    document.getElementById('entidadeducativaconsulta').innerHTML=fila.detalledefinicion;
				return;
			});
		})		
	}// fin busquedadetalleentidadeducativa
	
function busquedaconsultaconvenio(idconvenio){
		    $.getJSON('busquedaconvenio.php',{v0:idconvenio},function(datos){		           		
			$.each(datos,function(i,fila){	

   				document.getElementById('tipoconvenioconsulta').innerHTML=fila.descripcion;
				document.getElementById('alimentooconsulta').innerHTML=fila.valoralimentacion;
				document.getElementById('saludconsulta').innerHTML=fila.valorsalud;
				document.getElementById('educacionconsulta').innerHTML=fila.valoreducacion;
				document.getElementById('numeroconvenioconsulta').innerHTML=fila.codigo;
				
				return;
			});
		})		
	}// fin busquedaconsultaconvenio

function busquedaconsultaconyugue(idtrabajador){
	   	    $.getJSON('busquedaconyugue.php',{v0:idtrabajador},function(datos){		           		
			$.each(datos,function(i,fila){	

   				busquedadetalletipodocumentoconyugue(fila.idtipodocumento);
				document.getElementById('identificaionconsultaconyugue').innerHTML=fila.identificacion;
				document.getElementById('papellidoconsultaconyugue').innerHTML=fila.papellido;				
				document.getElementById('sapellidoconsultaconyugue').innerHTML=fila.sapellido;
				document.getElementById('pnombreconsultaconyugue').innerHTML=fila.pnombre;
				document.getElementById('snombreconsultaconyugue').innerHTML=fila.snombre;
				document.getElementById('fechanaceconsultaconyugue').innerHTML=fila.fechanacimiento;
				
				return;
			});
		})		
	}// fin busquedaconsultaconyugue
	
function busquedadetalletipodocumentoconyugue(tipodocumentoconyugue){
		    $.getJSON('busquedadetalles.php',{v0:tipodocumentoconyugue},function(datos){		           		
			$.each(datos,function(i,fila){	
			    document.getElementById('tipodocumentoconsultaconyugue').innerHTML=fila.detalledefinicion;
				return;
			});
		})		
	}// fin busquedadetalletipodocumentoconyugue
