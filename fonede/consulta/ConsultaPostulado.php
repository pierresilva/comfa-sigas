<?php
ini_set('display_errors','On');
include_once '../../rsc/pdo/IFXDbManejador.php';
include_once '../../rsc/pdo/IFXerror.php';
date_default_timezone_set('America/Bogota');

$db = IFXDbManejador::conectarDB(); 
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();	
}


$rs_tipodocumento=$db->Definiciones(1, 2);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Consulta Postulado</title>
<head> 
<link type="text/css" href="../../css/css.1.8/jquery-ui-1.8.17.custom.css" rel="stylesheet" />
<script language="javascript" src="../../js/1.6/jquery-1.6.2.min.js"></script>
<script language="javascript" src="../../js/1.6/jquery-ui-1.8.16.custom.min.js"></script>

<link href="../../css/Estilos.css" rel="stylesheet" type="text/css" />
<link type="text/css" href="../../css/marco.css" rel="stylesheet"/>
<script languaje="javascript" src="js/consulta.js"></script>
<script type="text/javascript" src="../../js/comunes.js"></script>

<script>


function validateEnter(e) {
	var key=e.keyCode || e.which;
	if (key==13){
	
		busquedacompletaconsulta();		
	 } 
}

</script>

</head>

<body> 
<form id="form" name="form" method="post" action="">
  <table width="843" border="0" align="center" cellpadding="0" cellspacing="0"> 
  <tr>
<td width="13" height="29" class="arriba_iz">&nbsp;</td>
<td class="arriba_ce"><span class="letrablanca">::&nbsp;Consulltar Datos del Postulado&nbsp;::</span></td>
<td width="13" class="arriba_de">&nbsp;</td>
</tr>
  <tr>
    <td class="cuerpo_iz">&nbsp;</td>
    <td class="cuerpo_ce" style="text-align:left; color: #000"><img src="../../imagenes/menu/nuevo.png" alt="" width="16" height="16" style="cursor: pointer" title="Nuevo" onclick="borrarcookieconsulta(),refrescarpagina();" /></td>
    <td class="cuerpo_de">&nbsp;</td>
  </tr>
  <tr>
    <td class="cuerpo_iz">&nbsp;</td>
    <td class="cuerpo_ce">&nbsp;</td>
    <td class="cuerpo_de">&nbsp;</td>
  </tr>
  <tr>     
<td class="cuerpo_iz">&nbsp;</td>
<td class="cuerpo_ce" style="text-align:right; color: #000">&nbsp;</td>
<td class="cuerpo_de">&nbsp;</td>
</tr>
<tr>
<td class="cuerpo_iz">&nbsp;</td>
<!-- TABLAS DE FORMULARIO -->
<td class="cuerpo_ce">
<center>
  <table width="100%" border="0" cellspacing="0" class="tablero">
  <tr bgcolor="#EBEBEB">
    <td width="168"><strong>Tipo Identificaci&oacute;n:</strong></td>
    <td width="141"><select name="tipoidentificacionconsulta" id="tipoidentificacionconsulta" class="box" value="<?php echo $idtipodocumento;?>">
      <option value="0">::Seleccione::</option>
      <?php
		   while($row=$rs_tipodocumento->fetch()){
			   if($row['iddetalledef']==1)
			   {
				    echo "<option selected=selected value=".$row['iddetalledef']." >".$row['detalledefinicion']."</option>";
			   }
			   else
			   {
				    echo "<option value=".$row['iddetalledef']." >".$row['detalledefinicion']."</option>";
			   }
 			    			  
			   }// fin while
           ?>
    </select></td>
    <td width="141"><strong>Identificaci&oacute;n:</strong></td>
    <td width="283"><input name="identificacionconsulta" type="text" id="identificacionconsulta" onkeydown='solonumeros(this);'
     onkeyup='solonumeros(this); validateEnter(event);' onblur="busquedacompletaconsulta();" class="box" value=""/></td>
  </tr>
  <tr bgcolor="#EBEBEB">
    <td colspan="4">&nbsp;</td>
    </tr>
  <tr>
    <td colspan="4">
      <div id="oculto" style="display:none">
        <table width="100%" border="0" cellspacing="0" class="tablero">
          <tr bgcolor="#CCCCCC">
            <td colspan="4" style="text-align:center"><strong>INFORMACI&Oacute;N GENERAL</strong></td>
            </tr>
          <tr>
            <td width="168">N&uacute;mero Radicado:
              <label id="numeroradicacionconsulta"></label></td>
            <td width="178">Afiliaciones a Otras Cajas: <strong>
              <label id="afiliacionconsulta"></label>
              </strong></td>
            <td colspan="2">Consecutivo Fonede:
              <label id="consefonedeconsulta"></label></td>
            </tr>
          <tr>
            <td>Hora Solicitud:
              <label id="horasolicituconsulta"></label></td>
            <td>Fecha Radicaci&oacute;n:
              <label id="fecharadicacionconsulta"></label></td>
            <td width="190">Fecha Solicitud:
              <label id="fechasolicituconsulta"></label></td>
            <td>Seccional:
              <label id="seccionalconsulta"></label></td>
            </tr>
          <tr>
            <td>Tiempo: <strong>
              <label id="tiempoconsulta" style="color:#FF0000"></label>
              </strong></td>
            <td colspan="3">Tipo Vinculaci&oacute;n: <strong>
              <label id="tipovinculacionconsulta"></label>
              </strong></td>
            </tr>
          <tr>
            <td>Documentos: <strong>
              <label id="documentosconsulta"></label>
              </strong></td>
            <td>Apto: <strong>
              <label id="aptoconsulta"></label>
              </strong></td>
            <td>Estado: <strong>
              <label id="estadoconsulta"></label>
              </strong></td>
             <td>Proceso: <strong>
              <label id="procesoconsulta"></label>
              </strong></td>
            </tr>
          <tr>
            <td colspan="4">Observaciones de Estado:
              <label id="observaciones"></label></td>
          </tr>
          <tr bgcolor="#CCCCCC">
            <td colspan="4" style="text-align:center"><strong>DATOS PERSONALES</strong></td>
            </tr>
          <tr>
            <td colspan="2">Tipo Documento: <strong>
              <label id="tipodocumentoconsulta"></label>
              </strong></td>
            <td colspan="2">N&uacute;mero Identificaci&oacute;n: <strong>
              <label id="identificaionconsulta"></label>
              </strong></td>
            </tr>
          <tr>
            <td>Primer Apellido: <strong>
              <label id="papellidoconsulta"></label>
              </strong></td>
            <td>Segundo Apellido: <strong>
              <label id="sapellidoconsulta"></label>
              </strong></td>
            <td>Primer Nombre: <strong>
              <label id="pnombreconsulta"></label>
              </strong></td>
            <td width="197">Segundo Nombre: <strong>
              <label id="snombreconsulta"></label>
              </strong></td>
            </tr>
          <tr>
            <td>Fecha Nacimiento:
              <label id="fechanaceconsulta"></label></td>
            <td>Genero:
              <label id="generoconsulta"></label></td>
            <td>Direcci&oacute;n:
              <label id="direccionconsulta"></label></td>
            <td>Tel&eacute;fono:
              <label id="telefonoconsulta"></label></td>
            </tr>
          <tr>
            <td>Estado Civil:
              <label id="estadocivilconsulta"></label></td>
            <td colspan="3">Escolaridad Alcanzada:
              <label id="escolaridadconsulta"></label></td>
          </tr>
          <tr bgcolor="#CCCCCC">
            <td colspan="4" style="text-align:center"><strong>OTROS DATOS</strong></td>
          </tr>
          <tr>
            <td>Artista:
              <label id="artistaconsulta"></label></td>
            <td>Deportista:
             <label id="deportistaconsulta"></label></td>
            <td colspan="2">Escritor:
            <label id="escritorconsulta"></label></td>
            </tr>
          <tr bgcolor="#CCCCCC">
            <td colspan="4" style="text-align:center"><strong>CONVENIO</strong></td>
          </tr>
          <tr>
            <td colspan="4">Tipo de Convenio:<strong>
             <label id="tipoconvenioconsulta"></label></strong></td>
          </tr>
          <tr>
            <td>Valor Alimento:
             <label id="alimentooconsulta"></label></td>
            <td>Valor Salud:
             <label id="saludconsulta"></label></td>
            <td>Valor Educaci&oacute;n:
             <label id="educacionconsulta"></label></td>
            <td>N&uacute;mero Convenio:
             <label id="numeroconvenioconsulta"></label></td>
          </tr>
          <tr>
            <td colspan="2">Eps:
             <label id="epsconsulta"></label></td>
            <td colspan="2">Entidad Educativa:
             <label id="entidadeducativaconsulta"></label></td>
            </tr>         
          <tr bgcolor="#CCCCCC">
            <td colspan="4" style="text-align:center"><strong>DESCRIPCI&Oacute;N DE CONYUGUE O COMPA&Ntilde;ERO DEL DESEMPLEADO</strong></td>
          </tr>
          <tr>
            <td colspan="2">Tipo Documento: <strong>
            <label id="tipodocumentoconsultaconyugue"></label>
            </strong></td>
            <td colspan="2">N&uacute;mero Identificaci&oacute;n: <strong>
            <label id="identificaionconsultaconyugue"></label>
            </strong></td>
            </tr>
          <tr>
            <td>Primer Apellido: <strong>
            <label id="papellidoconsultaconyugue"></label>
            </strong></td>
            <td>Segundo Apellido: <strong>
            <label id="sapellidoconsultaconyugue"></label>
            </strong></td>
            <td>Primer Nombre: <strong>
            <label id="pnombreconsultaconyugue"></label>
            </strong></td>
            <td>Segundo Nombre: <strong>
            <label id="snombreconsultaconyugue"></label>
            </strong></td>
          </tr>
          <tr>
            <td colspan="4">Fecha Nacimiento:
              <label id="fechanaceconsultaconyugue"></label></td>
            </tr>            
          <tr bgcolor="#CCCCCC">
            <td colspan="4" style="text-align:center"><strong>DESCRIPCI&Oacute;N DE BENEFICIARIOS DE AFILIADOS AL FONDO DEL DESEMPLEO</strong></td>
          </tr>
          <tr>
            <td colspan="4"><div id="beneficiario"></div></td>
            </tr>
          <tr>
            <td colspan="2">Hijos Menores de 18 en <strong>FONEDE:
            <label id="hijosmenoresconsulta"></label></strong></td>
            <td colspan="2">Personas a Cargo en <strong>FONEDE:
            <label id="personasacargoconsulta"></label></strong></td>
            </tr>           
          <tr bgcolor="#CCCCCC">
            <td colspan="4" style="text-align:center"><strong>GIRO ACTUAL FONEDE</strong></td>
          </tr>
          <tr>
            <td colspan="4"><div id="giroactual"></div></td>
            </tr>
          <tr bgcolor="#CCCCCC">
            <td colspan="4" style="text-align:center"><strong>HISTORICO GIRO FONEDE</strong></td>
          </tr>
          <tr>
            <td colspan="4"><div id="giros"></div></td>
            </tr>
          <tr bgcolor="#CCCCCC">
            <td colspan="4" style="text-align:center"><strong>OBSERVACIONES GENERALES</strong></td>
          </tr>
          <tr>
            <td colspan="4"><label id="observacionesgenerales"></label></td>           
          </tr>          
          </table>    
        </div>
      </td>
  </tr>
  </table>
</center>

<td class="cuerpo_de"></td><!-- FONDO DERECHA -->
<tr>
<td height="41" class="abajo_iz">&nbsp;</td>
<td class="abajo_ce">&nbsp;</td>
<td class="abajo_de">&nbsp;</td>
</tr>
</table>
</form> 
</body>
</html>