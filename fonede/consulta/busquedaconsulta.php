<?php
ini_set('display_errors','Off');
//date_default_timezone_set("America/Bogota");
include_once '../../rsc/pdo/IFXDbManejador.php';
include_once '../../rsc/pdo/IFXerror.php';

$db = IFXDbManejador::conectarDB(); 
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();	
}

$campo0=$_REQUEST['v0'];
$campo1=$_REQUEST['v1'];

$sql="select aportes221.idradicacion,aportes221.fecharadicacion,
aportes015.idpersona,aportes015.idtipodocumento,aportes015.identificacion,
aportes015.pnombre,aportes015.snombre,aportes015.papellido,aportes015.sapellido,
aportes015.fechanacimiento,aportes015.sexo,aportes015.idciuresidencia,
aportes015.iddepresidencia,aportes015.direccion,aportes015.telefono,aportes015.idescolaridad,aportes015.idprofesion,
aportes015.idestadocivil,aportes015.capacidadtrabajo,aportes015.desplazado,aportes015.reinsertado,
aportes221.idconvenio,aportes221.consecutivo,aportes221.idpersona,aportes221.fechagrabacion,aportes221.horagrabacion,aportes221.ideps,
aportes221.documentos,aportes221.idagencia,aportes221.estado,aportes221.observacionesestado,aportes221.afiliacionesotrascajas,
aportes221.identidadeducativa,aportes221.apto,aportes221.vinculacion,aportes221.tiempo,aportes221.hijosmenores,
aportes221.personasacargo,aportes221.proceso,aportes221.artista,aportes221.deportista,aportes221.escritor,aportes221.observacionesgenerales,
aportes091.detalledefinicion,aportes091.iddetalledef,
aportes500.codigo,aportes500.agencia
from aportes015, aportes221, aportes091, aportes500
where aportes015.idpersona=aportes221.idpersona 
and aportes221.idagencia=aportes500.codigo
and aportes091.iddetalledef=aportes015.idtipodocumento
and aportes015.identificacion='".$campo0."' 
and aportes015.idtipodocumento='".$campo1."'";
$rs=$db->querySimple($sql);
$con=0;
$filas=array();
while ($row= $rs->fetch()){
	$filas[]= $row;
	$con++;
}
if($con==0){
	echo 0;
	exit();
}

echo json_encode($filas);
?>