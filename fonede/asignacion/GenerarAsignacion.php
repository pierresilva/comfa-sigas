
<?php
ini_set('display_errors','Off');
include_once '../../rsc/pdo/IFXDbManejador.php';
include_once '../../rsc/pdo/IFXerror.php';

$db = IFXDbManejador::conectarDB(); 
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}
?>

<?php
$sql="SELECT aportes222.idapropiacion,aportes222.descripcion,aportes222.totalsubsidiosinicial,aportes222.vinculacion
             ,aportes223.descripcionperiodo,aportes222.vigencia
             FROM aportes222,aportes223
             where 1=1 
             and aportes222.idperiodo=aportes223.idperiodo
			 and aportes222.totalsubsidiosinicial IS NULL";
$rs=$db->querySimple($sql);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Generar Asignaci&oacute;n</title>
<link type="text/css" href="../../css/css.1.8/jquery-ui-1.8.17.custom.css" rel="stylesheet" />
<link type="text/css" href="../../css/Estilos.css" rel="stylesheet"/>
<link type="text/css" rel="stylesheet" href="../../css/marco.css" />

<script language="javascript" src="../../js/1.6/jquery-1.6.2.min.js"></script>
<script language="javascript" src="../../js/1.6/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="../../js/comunes.js"></script>

<script languaje="javascript" src="js/Asignacion.js"></script>

<script type="text/javascript">
function Sumar(){
      interval = setInterval("calcular()",1);
}

function calcular(){
	
	if(document.getElementById('descripcion_apropiaciones').value!=0)
	{	
	  salariominimo= document.form1.salariominimo.value; 
	  totaldisponible= document.form1.totaldisponible.value;
	  valor=1.5;
	  salariominimototal=(salariominimo * 1) * (valor * 1);
	  totalpersonas= (totaldisponible * 1) / (salariominimototal * 1);
	  presupuesto=(salariominimototal*1)*(trunc(totalpersonas)*1);	 
	  totalfinal=(totaldisponible*1)-(presupuesto*1)
	   
      //alert("salariominimototal="+salariominimototal);
	  //alert("presupuesto="+presupuesto);	  	
      //alert("totalfinal="+totalfinal);
	  
	  document.form1.totalusuarios.value = trunc(totalpersonas);
	  document.form1.totalfinal.value = totalfinal;
	}
}
function trunc(n){
   return ~~n;
}

function NoSumar(){
      clearInterval(interval);
}

</script>
</head>

<body>


<form id="form1" name="form1" method="post" action="">
<table border="0" cellspacing="0" cellpadding="0" align="center"> 
<tr>
<td width="13" height="29" class="arriba_iz">&nbsp;</td>
<td class="arriba_ce"><span class="letrablanca">::&nbsp;Generar Asignaci&oacute;n&nbsp;::</span></td>
<td width="13" class="arriba_de" >&nbsp;</td>
</tr>
<tr>     
<td class="cuerpo_iz">&nbsp;</td>
<td class="cuerpo_ce">
<img src="../../imagenes/menu/informacion.png" width="16" height="16" style="border: none; cursor: pointer" title="Manual" onClick="mostrarAyuda();" /> 
<img src="../../imagenes/menu/notas.png" width="16" height="16" style="cursor: pointer" title="Colaboraci&oacute;n en linea" onClick="notas();" /> <img src="../../imagenes/menu/nuevo.png" width="16" height="16" style="cursor: pointer" title="Nuevo" onclick="limpiarCampos(),location.reload();" />
<div id="error" style="color:#FF0000"></div>
</td>
<td class="cuerpo_de">&nbsp;</td>
</tr>
<tr>
<td class="cuerpo_iz">&nbsp;</td>
<!-- TABLAS DE FORMULARIO -->
<td class="cuerpo_ce">
<center>

     <table border="0" cellspacing="0" class="tablero">
            <tr bgcolor="#EBEBEB">
              <td colspan="2" style="text-align:center" ></td>
            </tr>
            <tr>
                    
              <td width="112">Descripci&oacute;n:</td>
              <td width="208"><select name="descripcion_apropiaciones" id="descripcion_apropiaciones" class="boxlargo" onchange="buscardescripcionapropiacion();">
                <option value="0" selected="selected">::Seleccione::</option>
                <?php
		   while($row=$rs->fetch()){
 			     if ($row['vinculacion'] == 1) {
					 echo "<option value=".$row['idapropiacion'].">".$row['descripcion']."/".$row['descripcionperiodo']."/".$row['vigencia']."/Sin Vinculaci&oacute;n</option>";
	             }
				  else  {
		             echo "<option value=".$row['idapropiacion'].">".$row['descripcion']."/".$row['descripcionperiodo']."/".$row['vigencia']."/Con Vinculaci&oacute;n</option>";			  
			   }
		   }
           ?>
                
                </select>
                <img src="../../imagenes/menu/obligado.png" align="middle"/></td>
            </tr>
            <tr>
              <td>Total Subsidios:</td>
              <td><input name="totalusuarios" type="text" id="totalusuarios" readonly="readonly"/></td>
            </tr>
            <tr>
              <td>Total Disponible:</td>
              <td><input type="text"  id="totaldisponible" disabled="disabled" />
                <span class="abajo_ce"><input type="hidden" id="totalfinal" value=""/>
              </span></td>
            </tr>
            <tr>
              <td>Salario Minimo:</td>
              <td><input type="text"  id="salariominimo" disabled="disabled" /></td>
            </tr>
            <tr>              
              <td>&nbsp;</td>
              <td><input type="button" name="button" id="button" value="Generar" onclick="calcular(); if(confirm('Realmente desea guardar el total de Subsidios')){actualizartotalusuarios();}" /></td>
            </tr>
          </table>
</center>

<td class="cuerpo_de"></td><!-- FONDO DERECHA -->
<tr>
<td height="41" class="abajo_iz">&nbsp;</td>
<td class="abajo_ce">&nbsp;<font color="#FF0000">Campos Obligatorios</font><img src="../../imagenes/menu/obligado.png" align="middle"/>
  <input type="hidden" id="idapropiacion"/></td>
<td class="abajo_de">&nbsp;</td>
</tr>
</table>
</form>
</body>
</html>