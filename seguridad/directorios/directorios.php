<?php
/* autor:       Orlando Puentes
 * fecha:       Agosto 10 de 2010
* objetivo:    Registrar en la base de datos la recepci?n de documentos con destino a Aportes y Subsidio para la afiliaci?n, modificaci?n o adici?n de informaci?n de los afiliados o empresas aportes de Comfamiliar Huila.
*/
set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");

$root = $_SERVER ['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<head>
	<title>::Directorios::</title>	
	<link href="<?php echo URL_PORTAL; ?>css/sorterStyle.css" rel="stylesheet" type="text/css" media="screen" />
		
	<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/1.6/jquery-1.6.2.min.js"></script>		
	<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/jquery.tablesorter.js"></script>
		
	<script type="text/javascript" src="js/directorios.js"></script>
</head>
<body>	
	<table  cellpadding="0" cellspacing="0" border="0" id="tableDir" class="sortable" width="74%"  >
  		<thead>
	  		<tr >
			    <th width=""><h4><strong>Archivo</strong></h4></th>
			    <th width=""><h4><strong>Fecha Modificacion</strong></h4></th>		    	    
	  		</tr>
  		</thead>
  		<tbody>
  			<?php
  				listar_directorios_ruta($raiz);
  			?>		
		</tbody>
	</table>
</body>
</html>
<?php 
function listar_directorios_ruta($ruta)
{	
	if (is_dir($ruta)){
		if ($dh = opendir($ruta)){			
			while(($file = readdir($dh)) !== false){
				if($file!="." && $file!=".." && $file!=".svn"){
					if (is_dir($ruta.DIRECTORY_SEPARATOR.$file)){
						listar_directorios_ruta($ruta.DIRECTORY_SEPARATOR.$file);
					} else {
						$fecver = date('Ymd h:i:s A',filectime($ruta.DIRECTORY_SEPARATOR.$file));
						echo "<tr>";
						echo "<td>".substr($ruta,50).DIRECTORY_SEPARATOR.$file."</td>";
						echo "<td>".$fecver."</td>";
						echo "</tr>";
					}
				}
			}
			closedir($dh);
		}
	} else {
		$fecver = date('Ymd h:i:s A',filectime($ruta));
		echo "<tr>";
		echo "<td>".substr($ruta,50)."</td>";
		echo "<td>".$fecver."</td>";
		echo "</tr>";
	}
}
?>