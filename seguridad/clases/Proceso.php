<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'ArrayList.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR . 'pdo' . DIRECTORY_SEPARATOR . 'IFXDbManejador.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'rsc' . DIRECTORY_SEPARATOR . 'pdo' . DIRECTORY_SEPARATOR . 'IFXerror.php';

class Proceso {

	//Atributos
	private $_idProceso;
	private $_proceso;
	///////////
	
	//Propiedades
	public function getIdProceso(){
		return $this->_idProceso;
	}
	
	public function setIdProceso($idProceso){
		$this->_idProceso = $idProceso;
	}
	
	public function getProceso(){
		return $this->_proceso;
	}
	
	public function setProceso($proceso){
		$this->_proceso = $proceso;
	}
	/////////////
	
	//Constructor
	function __construct($idProceso=0, $proceso="") {
		$this->_idProceso = $idProceso;
		$this->_proceso = $proceso;
	}
	/////////////
	
	//Metodos estaticos
	public static function consultarPorId($idProceso){
		try {
			$db = IFXDbManejador::conectarDB();
			if($db->conexionID==null){
				$cadena = $db->error;
				echo msg_error($cadena);
				exit();
			}
			$rs = $db->querySimple("SELECT aportes514.idproceso, aportes514.proceso FROM aportes514 WHERE aportes514.idproceso = $idProceso");
			$objUsuario = new Proceso();
			if($row = $rs->fetch()){
				$objProceso = new Proceso($row["idproceso"], $row["proceso"]);
			}
			return $objProceso;
		} catch (Exception $e) {
			echo msg_error($e->getMessage());
			exit();
		}
	}
	
	public static function listarTodos(){
		try {
			$db = IFXDbManejador::conectarDB();
			if($db->conexionID==null){
				$cadena = $db->error;
				echo msg_error($cadena);
				exit();
			}
			$rs = $db->querySimple("SELECT aportes514.idproceso, aportes514.proceso FROM aportes514 ORDER BY aportes514.proceso ASC");
			$listProcesos = new ArrayList();
			while($row = $rs->fetch()){
				$listProcesos->add(new Proceso($row["idproceso"], $row["proceso"]));
			}
			return $listProcesos->toArray();
		} catch (Exception $e) {
			echo msg_error($e->getMessage());
			exit();
		}
	}
	///////////////////
}

?>