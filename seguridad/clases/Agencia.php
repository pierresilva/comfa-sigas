<?php
//modificado 29/08/2012 - $root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
//include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'ArrayList.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';

class Agencia {

	//Atributos
	private $_idAgencia;
	private $_agencia;
	///////////
	
	//Propiedades
	public function getIdAgencia(){
		return $this->_idAgencia;
	}
	
	public function setIdAgencia($idAgencia){
		$this->_idAgencia = $idAgencia;
	}
	
	public function getAgencia(){
		return $this->_agencia;
	}
	
	public function setAgencia($agencia){
		$this->_agencia = $agencia;
	}
	/////////////
	
	//Constructor
	function __construct($idAgencia=0, $agencia="") {
		$this->_idAgencia = $idAgencia;
		$this->_agencia = $agencia;
	}
	/////////////
	
	//Metodos estaticos
	public static function consultarPorId($idAgencia){
		try {
			$db = IFXDbManejador::conectarDB();
			if($db->conexionID==null){
				$cadena = $db->error;
				echo msg_error($cadena);
				exit();
			}
			$rs = $db->querySimple("SELECT aportes500.idagencia, aportes500.agencia FROM aportes500 WHERE aportes500.idagencia = $idAgencia");
			$objAgencia = new Agencia();
			if($row = $rs->fetch()){
				$objAgencia = new Agencia($row["idagencia"], $row["agencia"]);
			}
			return $objAgencia;
		} catch (Exception $e) {
			echo msg_error($e->getMessage());
			exit();
		}
	}
	
	public static function listarTodas(){
		try {
			$db = IFXDbManejador::conectarDB();
			if($db->conexionID==null){
				$cadena = $db->error;
				echo msg_error($cadena);
				exit();
			}
			$rs = $db->querySimple("SELECT aportes500.idagencia, aportes500.agencia FROM aportes500 ORDER BY aportes500.agencia ASC");
			$listAgencias = new ArrayList();
			while($row = $rs->fetch()){
				$listAgencias->add(new Agencia($row["idagencia"], $row["agencia"]));
			}
			return $listAgencias->toArray();
		} catch (Exception $e) {
			echo msg_error($e->getMessage());
			exit();
		}
	}
	///////////////////
}
?>