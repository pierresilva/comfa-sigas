<?php
//modificado 29/08/2012 - $root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
//include_once  $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'ArrayList.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR . 'IFXDbManejador.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'rsc' . DIRECTORY_SEPARATOR . 'pdo' . DIRECTORY_SEPARATOR . 'IFXerror.php';
include_once 'Proceso.php';

class Area {
	
	//Atributos
	private $_idArea;
	private $_area;
	private $_idProceso;
	///////////
	
	//Propiedades
	public function getIdArea(){
		return $this->_idArea;
	}
	
	public function setIdArea($idArea){
		$this->_idArea = $idArea;
	}
	
	public function getArea(){
		return $this->_area;
	}
	
	public function setArea($area){
		$this->_area = $area;
	}
	
	public function getIdProceso(){
		return $this->_idProceso;
	}
	
	public function setIdProceso($idProceso){
		$this->_idProceso = $idProceso;
	}
	
	public function getProceso(){
		return Proceso::consultarPorId($this->_idProceso);
	}
	/////////////
	
	//Constructor
	function __construct($idArea=0, $area="", $idProceso=0) {
		$this->_idArea = $idArea;
		$this->_area = $area;
		$this->_idProceso = $idProceso;
	}
	/////////////
	
	//Metodos estaticos
	public static function consultarPorId($idArea){
		try {
			$db = IFXDbManejador::conectarDB();
			if($db->conexionID==null){
				$cadena = $db->error;
				echo msg_error($cadena);
				exit();
			}
			$rs = $db->querySimple("SELECT aportes501.idarea, aportes501.area, aportes501.idproceso FROM aportes501 WHERE aportes501.idarea = $idArea");
			$objArea = new Area();
			if($row = $rs->fetch()){
				$objArea = new Area($row["idarea"], $row["area"], $row["idproceso"]);
			}
			return $objArea;
		} catch (Exception $e) {
			echo msg_error($e->getMessage());
			exit();
		}
	}
	
	public static function listarPorIdProceso($idProceso){
		try {
			$db = IFXDbManejador::conectarDB();
			if($db->conexionID==null){
				$cadena = $db->error;
				echo msg_error($cadena);
				exit();
			}
			$rs = $db->querySimple("SELECT aportes501.idarea, aportes501.area, aportes501.idproceso FROM aportes501 WHERE aportes501.idproceso=$idProceso ORDER BY aportes501.area ASC");
			$listAreas = new ArrayList();
			while($row = $rs->fetch()){
				$listAreas->add(new Area($row["idarea"], $row["area"], $row["idproceso"]));
			}
			return $listAreas->toArray();
		} catch (Exception $e) {
			echo msg_error($e->getMessage());
			exit();
		}
	}
	
	public static function listarTodas(){
		try {
			$db = IFXDbManejador::conectarDB();
			if($db->conexionID==null){
				$cadena = $db->error;
				echo msg_error($cadena);
				exit();
			}
			$rs = $db->querySimple("SELECT aportes501.idarea, aportes501.area, aportes501.idproceso FROM aportes501 ORDER BY aportes501.area ASC");
			$listAreas = new ArrayList();
			while($row = $rs->fetch()){
				$listAreas->add(new Area($row["idarea"], $row["area"], $row["idproceso"]));
			}
			return $listAreas->toArray();
		} catch (Exception $e) {
			echo msg_error($e->getMessage());
			exit();
		}
	}
	///////////////////
}

?>