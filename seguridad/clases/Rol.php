<?php
//modificado 29/08/2012 - $root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
//include_once  $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'ArrayList.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR . 'pdo' . DIRECTORY_SEPARATOR . 'IFXDbManejador.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'rsc' . DIRECTORY_SEPARATOR . 'pdo' . DIRECTORY_SEPARATOR . 'IFXerror.php';

class Rol {
	
	//Atributos
	private $_idRol;
	private $_rol;
	private $_estado;
	private $_restriccion;
	private $_diasRestid;
	private $_jornadaIni;
	private $_jornadaFin;
	private $_horasRestIni;
	private $_horasRestFin;
	private $_adiciona;
	private $_modifica;
	///////////
	
	//Propiedades
	public function getIdRol(){
		return $this->_idRol;
	}
	
	public function setIdRol($idRol){
		$this->_idRol = $idRol;
	}
	
	public function getRol(){
		return $this->_rol;
	}
	
	public function setRol($rol){
		$this->_rol = $rol;
	}
	
	public function getEstado(){
		return $this->_estado;
	}
	
	public function setEstado($estado){
		$this->_estado = $estado;
	}
	
	public function getRestriccion(){
		return $this->_restriccion;
	}
	
	public function setRestriccion($restriccion){
		$this->_restriccion = $restriccion;
	}
	
	public function getDiasResticcion(){
		return $this->_diasRestid;
	}
	
	public function setDiasResticcion($diasResticcion){
		$this->_diasRestid = $diasResticcion;
	}
	
	public function getJornadaIni(){
		return $this->_jornadaIni;
	}
	
	public function setJornadaIni($jornadaIni){
		$this->_jornadaIni = $jornadaIni;
	}
	
	public function getJornadaFin(){
		return $this->_jornadaFin;
	}
	
	public function setJornadaFin($jornadaFin){
		$this->_jornadaFin = $jornadaFin;
	}
	
	public function getHorasRestIni(){
		return $this->_horasRestIni;
	}
	
	public function setHorasRestIni($horasRestIni){
		$this->_horasRestIni = $horasRestIni;
	}
	
	public function getHorasRestFin(){
		return $this->_horasRestFin;
	}
	
	public function setHorasRestFin($horasRestFin){
		$this->_horasRestFin = $horasRestFin;
	}
	
	public function getAdiciona(){
		return $this->_adiciona;
	}
	
	public function setAdiciona($adiciona){
		$this->_adiciona = $adiciona;
	}
	
	public function getModifica(){
		return $this->_modifica;
	}
	
	public function setModifica($modifica){
		$this->_modifica = $modifica;
	}
	/////////////

	//Constructor
	function __construct($idRol=0, $rol="", $estado="", $restriccion=0, $diasRestid=0, 
						 $jornadaIni="", $jornadaFin="", $horasRestIni="", $horasRestFin="", $adiciona="", $modifica="") {
		$this->_idRol = $idRol;
		$this->_rol = $rol;
		$this->_estado = $estado;
		$this->_restriccion = $restriccion;
		$this->_diasRestid = $diasRestid;
		$this->_jornadaIni = $jornadaIni;
		$this->_jornadaFin = $jornadaFin;
		$this->_horasRestIni = $horasRestIni;
		$this->_horasRestFin = $horasRestFin;
		$this->_adiciona = $adiciona;
		$this->_modifica = $modifica;
	}
	/////////////
	
	//Metodos estaticos
	public static function consultarPorId($idRol){
		try {
			$db = IFXDbManejador::conectarDB();
			if($db->conexionID==null){
				$cadena = $db->error;
				echo msg_error($cadena);
				exit();
			}
			$rs = $db->querySimple("SELECT aportes516.idrol, aportes516.rol, aportes516.estado, aportes516.restriccion, aportes516.diasrestid, aportes516.jornadaini, aportes516.jornadafin, aportes516.horasrestini, aportes516.horasrestfin, aportes516.adiciona, aportes516.modifica FROM aportes516 WHERE idrol='$idRol'");
			$objRol = new Rol();
			if($row = $rs->fetch()){
				$objRol = new Rol($row["idrol"], $row["rol"], $row["estado"], $row["restriccion"], $row["diasrestid"], $row["jornadaini"], $row["jornadafin"], $row["horasrestini"], $row["horasrestfin"], $row["adiciona"], $row["modifica"]);
			}
			return $objRol;
		} catch (Exception $e) {
			echo msg_error($e->getMessage());
			exit();
		}
	}
	
	public static function listarTodos(){
		try {
			$db = IFXDbManejador::conectarDB();
			if($db->conexionID==null){
				$cadena = $db->error;
				echo msg_error($cadena);
				exit();
			}
			$rs = $db->querySimple("SELECT aportes516.idrol, aportes516.rol, aportes516.estado, aportes516.restriccion, aportes516.diasrestid, aportes516.jornadaini, aportes516.jornadafin, aportes516.horasrestini, aportes516.horasrestfin, aportes516.adiciona, aportes516.modifica FROM aportes516 WHERE idrol > 1 ORDER BY rol ASC");
			$listRoles = new ArrayList();
			while($row = $rs->fetch()){
				$listRoles->add(new Rol($row["idrol"], $row["rol"], $row["estado"], $row["restriccion"], $row["diasrestid"], $row["jornadaini"], $row["jornadafin"], $row["horasrestini"], $row["horasrestfin"], $row["adiciona"], $row["modifica"]));
			}
			return $listRoles->toArray();
		} catch (Exception $e) {
			echo msg_error($e->getMessage());
			exit();
		}
	}
	///////////////////
}

?>