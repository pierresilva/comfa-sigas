<?php
/* autor:       orlando puentes
 * fecha:       23/09/2010
 * objetivo:     
 */
 
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'rsc' . DIRECTORY_SEPARATOR . 'conexion.class.php';

class Entidad{
	//constructor
	var $con;
	function Entidad(){
 		$this->con=new DBManager;
 	}	
	
	/*
	 * Contar entidades existentes
	*/
	function Count_Entidades(){
		if($this->con->conectar()==true){
			$sql="SELECT count(*) cant FROM aportes001";
			return mssql_query($sql,$this->con->conect);
		}
	}
	
	/*
	 * Traer todas las entidades existentes
	*/
	function Select_Entidades($campo_de_inicio, $campo_fin){
		if($this->con->conectar()==true){
			$sql="SELECT identidad,entidad,llaveprimaria,modulo,descripcion,fechasistema,usuario FROM ( SELECT identidad,entidad,llaveprimaria,modulo,descripcion,fechasistema,usuario, ROW_NUMBER() OVER (ORDER BY entidad) as row FROM aportes001) a WHERE row > $campo_de_inicio and row <= $campo_fin";
			return mssql_query($sql,$this->con->conect);
		}
	}
}