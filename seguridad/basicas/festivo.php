<?php
/* autor:       orlando puentes
* fecha:       24/09/2010
* objetivo:    
*/
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$url=$_SERVER['PHP_SELF'];
include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'auditoria.php';
//include_once 'phpComunes/auditoria.php';
auditar($url);

$_SESSION['REGISTRO']=0;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Festivo</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<link href="../../css/Estilos.css" rel="stylesheet" type="text/css" />
        <link href="../../css/formularios/base/ui.all.css" rel="stylesheet" type="text/css" />
        <link href="../../css/marco.css" rel="stylesheet" type="text/css" />
		<script language="javascript" src="../../js/ui/jquery-1.4.2.js"></script>
		<script language="javascript" src="../../js/comunes.js"></script>
        <script language="javascript" src="../../js/ui/jq-ui/jquery.ui.core.js"></script>
        <script language="javascript" src="../../js/ui/jq-ui/jquery.ui.datepicker.js"></script>
		<script language="javascript" src="js/festivo.js"></script>
	
    <script type="text/javascript">
	shortcut.add("Shift+F",function() {
		                                var URL=src();
		var url=URL+"aportes/trabajadores/consultaTrabajador.php";
    	window.open(url,"_blank");
    },{
	'propagate' : true,
	'target' : document 
        });        
</script>

    </head>
	<body >
		<form name="forma">
        <br/>
        <center>
            <table width="70%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="13" height="29" class="arriba_iz">&nbsp;</td>
                    <td class="arriba_ce"><span class="letrablanca">:: Festivo ::</span></td>
                    <td width="13" class="arriba_de" align="right">&nbsp;</td>
                </tr>
                <tr>
                    <td class="cuerpo_iz">&nbsp;</td>
                   <td class="cuerpo_ce">
<img src="../../imagenes/tabla/spacer.gif" width="1" height="1">
<img src="../../imagenes/spacer.gif" width="1" height="1">
<img src="../../imagenes/menu/nuevo.png" title="Nuevo" width="16" height="16" style="cursor:pointer" onClick="nuevoR();">
<img src="../../imagenes/spacer.gif" width="1" height="1">
<img src="../../imagenes/menu/grabar.png" title="Guardar" width="16" height="16" onClick="validarCampos(1)" style="cursor:pointer" id= "bGuardar">
<img src="../../imagenes/spacer.gif" width="1" height="1">
<img src="../../imagenes/menu/modificar.png" title="Actualizar" width="16" height="16" onClick="validarCampos(2)" style="cursor:pointer">
<img src="../../imagenes/spacer.gif" width="1" height="1">
<img src="../../imagenes/menu/ico_error.png" width="16" height="16" border="0" title="Eliminar registro" style="cursor:pointer" onClick="eliminarDato()" >
<img src="../../imagenes/spacer.gif" width="1" height="1">
<img src="../../imagenes/menu/imprimir.png" width="16" height="16" style="cursor:pointer" title="Imprimir" onClick="imprimir();" >
<img src="../../imagenes/spacer.gif" width="1" height="1">
<img src="../../imagenes/menu/refrescar.png" width="16" height="16" style="cursor:pointer" title="Limpiar campos" onClick="limpiarCampos(); document.forms[0].elements[1].focus();">
<img src="../../imagenes/spacer.gif" width="1" height="1">
<img src="../../imagenes/menu/buscar.png" title="Buscar" width="16" height="16" style="cursor:pointer" onClick="consultaDatos()">
<img src="../../imagenes/spacer.gif" width="1" height="1">
<a href="../../help/seguridad/manual ayuda festivo.html" target="_blank" onClick="window.open(this.href, this.target, 'width=500,height=550,titlebar=0, resizable=no, scrollbars=yes'); return false;" >
<img src="../../imagenes/menu/informacion.png" width="16" height="16" style="border:none" title="Manual" />
</a>
<img src="../../imagenes/spacer.gif" width="1" height="1">
<img src="../../imagenes/menu/primero.png" width="16" height="16" border="0" title="Primero" style="cursor:pointer" onclick="mostrar(1);" />
<img src="../../imagenes/spacer.gif" width="1" height="1">
<img src="../../imagenes/menu/siguiente.png" width="16" height="16" border="0" title="Siguiente" style="cursor:pointer" onclick="mostrar(2);" />
<img src="../../imagenes/spacer.gif" width="1" height="1">
<img src="../../imagenes/menu/anterior.png" width="16" height="16" border="0" title="Anterior" style="cursor:pointer" onclick="mostrar(3);" />
<img src="../../imagenes/spacer.gif" width="1" height="1">
<img src="../../imagenes/menu/ultimo.png" width="16" height="16" border="0" title="Ultimo" style="cursor:pointer" onclick="mostrar(4);" />
<img src="../../imagenes/spacer.gif" width="1" height="1">
                    </td>
                     <td class="cuerpo_de">&nbsp;</td>
                </tr>
                <tr>
                   <td class="cuerpo_iz">&nbsp;</td>
                   <td class="cuerpo_ce"><div id="resultado" style="color:#FF0000"></div></td>
                   <td class="cuerpo_de">&nbsp;</td>
                </tr>
                <tr>
                   <td class="cuerpo_iz">&nbsp;</td>
                     <td class="cuerpo_ce">
                        <table width="95%" border="0" cellspacing="0" class="tablero">
                            <tr>
                                <td width="25%" align="left">Id Festivo</td>
                                <td align="left"><input name="txtId" class=boxfecha id="txtId" /></td>
                            </tr>
                            <tr>
                                <td align="left">Nombre</td>
                                <td  align="left"><input name="txtNombre" class="box1" id="txtNombre" />
                                    <img src="../../imagenes/menu/obligado.png" width="12" height="12" />
                                    <img src="../../imagenes/find.png" width="16" height="16" title="Se puede buscar por este campo" />
                                </td>
                            </tr>
                            <tr>
                                <td align="left">A�o</td>
                                <td  align="left"><input name="txtAnno" class="box1" id="txtAnno" />
                                    <img src="../../imagenes/menu/obligado.png" width="12" height="12" />
                                </td>
                            </tr>
                            <tr>
                                <td align="left">Fecha Festivo</td>
                                <td  align="left"><input name="txtFestivo" class="boxfecha" id="txtFestivo" />
                                    <img src="../../imagenes/menu/obligado.png" width="12" height="12" />
                                </td>
                            </tr>
                        </table>
                    </td>
                   <td class="cuerpo_de">&nbsp;</td>
                </tr>
                    <tr>
                    <td class="abajo_iz" >&nbsp;</td>
                    <td class="abajo_ce" ></td>
                    <td class="abajo_de" >&nbsp;</td>
                    </tr>
            </table>
        </center>
        </form>
    </body>
	<script language="javascript">
		mostrar(1);
	</script>
</html>