var nuevo=0;
var modifica=0;
var URL=src();

$(function(){
	$( "#txtAgencia" ).autocomplete({
	    source: function( request, response ) {
	    	var nomagencia=$("#txtAgencia").val();
	        $.ajax({
	            url: "agenciasAut.php",
	            data: {v0:nomagencia},
	            dataType: "json",
	            success: function( data ) {
	                response( data );
	            }
	        });
	     },
	    select: function( event, ui ) {
	    	$("#txtId").val(ui.item.id);
	    }
	});
});

function validarCampos(op){
	
	if(nuevo==0){
	alert("Click en NUEVO !");
	return false;
	}
	
	if($("#txtAgencia").val()==''){
		alert("Digite la agencia Barrio!");
		$("#txtAgencia").focus();
		return;
	}
	
	if(op==1){
		guardarDatos();
	}	
	else{
		actualizarDatos();
	}
}

function nuevoR(){
	nuevo=1;
	$("#bGuardar").show();
	$("#btnActualizar").hide();
	$("#bGuardar").show();
	limpiarCampos();
	document.forms[0].elements[1].focus();
	document.forms[0].elements[2].value = fechaHoy();
	document.forms[0].elements[3].value= document.forms[0].elements['usuario'].value;
	}

function guardarDatos(){
	$("#bGuardar").hide();
	var campo0=$("#txtId").val();
	var campo1=$("#txtAgencia").val();
	var campo2=$("#tFecha").val();
	var campo3=$("#tusuario").val();
	$.ajax({
		url: 'agenciaGuardar.php',
		type: "POST",
		data: "submit=&v0="+campo0+"&v1="+campo1+"&v2="+campo2+"&v3="+campo3,
		success: function(datos){
			$("#bGuardar").show();
			alert(datos);
			limpiarCampos();
		}
	});
	nuevo=0;
}

function consultaDatos(){
	if($("#txtAgencia").val()==''){
		alert("Ingrese la agencia a buscar.");
		$("#txtAgencia").focus();
		return false;
	}
	var idagencia = $("#txtId").val(); 
	
	$.ajax({
		url: 'agenciaBuscar.php',
		cache: false,
		type: "POST",
		data: {v0:idagencia},
		dataType:"json",
		success: function(data){
			if(data==0){
				alert("Lo lamento, el registro no existe!");
				return false;
			}
			$("#txtId").val(data.idagencia);				
			$("#txtAgencia").val($.trim(data.agencia));
			$("#tFecha").val($.trim(data.fechasistema));
			$("#tusuario").val($.trim(data.usuario));
			$("#idr").val(data.idagencia);//campo oculto en agencias.php
			$("#btnActualizar").show();
			$("#bGuardar").hide();
		}
	});
}

function actualizarDatos(){
	var campo0=document.forms[0].idr.value;
	var campo1=document.forms[0].elements[1].value;
	var campo2=document.forms[0].elements[2].value;
	var campo3=document.forms[0].elements[3].value;
	
	$.ajax({
		url: 'agenciasA.php',
		cache: false,
		type: "POST",
		data: "submit=&v0="+campo0+"&v1="+campo1+"&v2="+campo2+"&v3="+campo3,
		success: function(datos){
			alert(datos);
			limpiarCampos();
			
		}
	});
	return false;
}

/**MANDA EL REPORTE A EXEL**/
/*function imprimir(){
	var usuario=$("#usuario").val();		
	url="http://10.10.1.105:8080/sigasReportes/seguridad/rptExcel001.jsp?v0="+usuario;
	window.open(url,"_NEW");
}

function imprimir(){
	var url=URL+'centroReportes/seguridad/reporte001.php';
	window.open(url,'_blank');
	}*/	