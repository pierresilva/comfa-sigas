var nuevo=0;
var modifica=0;
var URL=src();

$(function(){
	$( "#txtSubmenu" ).autocomplete({
	    source: function( request, response ) {
	    	var submenus=$("#txtSubmenu").val();
	        $.ajax({
	            url: "formularioAut.php",
	            data: {v0:submenus},
	            dataType: "json",
	            success: function( data ) {
	                response( data );
	            }
	        });
	     },
	    select: function( event, ui ) {
	    	$("#txtId").val(ui.item.id);
	    }
	});
});

function nuevoR(){
	limpiarCampos();
	$("#txtModulo").focus();
	nuevo=1;
	$("#bGuardar").show();
	$("#btnActualizar").hide();
}

function validarCampos(op){
	
	if(nuevo==0){
	alert("Click en NUEVO !");
	return false;
	}
	
	if($("#txtModulo").val()==0){
		alert("Falta Seleccionar Modulo!")
		$("#txtModulo").focus();
		return false;
	}
	if($("#txtMenu").val()==0){
		alert("Falta Menu!")
		$("#txtMenu").focus();
		return false;
	}
	if($("#txtSubmenu").val()==''){
		alert("Falta Escribir el Submenu!")
		$("#txtSubmenu").focus();
		return false;
	}
	if($("#txtFormulario").val()==''){
		alert("Falta formulario!")
		$("#txtFormulario").focus();
		return false;
	}
	if($("#txtUrl").val()==''){
		alert("Falta URL!")
		$("#txtUrl").focus();
		return false;
	}
	if($("#txtDestino").val()==''){
		alert("Falta Destino!")
		$("#txtDestino").focus();
		return false;
	}
	if($("#txtDescripcion").val()==''){
		alert("Falta Descripcion!")
		$("#txtDescripcion").focus();
		return false;
	}
	if(op==1){
		guardarDatos();
	}	
	else{
		actualizarDatos();
	}
}

function guardarDatos(){
	$("#bGuardar").hide();
	
	var campo1=$("#txtModulo").val();
	var campo2=$("#txtMenu").val();
	var campo3=$("#txtSubmenu").val();
	var campo4=$("#txtFormulario").val();
	var campo5=$("#txtUrl").val();	
	var campo6=$("#txtDestino").val();
	var campo7=$("#txtDescripcion").val();
	$.ajax({
		url: 'formularioGuardar.php',
		type: "POST",
		data: "submit=&v1="+campo1+"&v2="+campo2+"&v3="+campo3+"&v4="+campo4+"&v5="+campo5+"&v6="+campo6+"&v7="+campo7,
		success: function(datos){
			$("#bGuardar").show();
			alert(datos);
			limpiarCampos();
		}
	});
	nuevo=0;
}

function consultaDatos(){
	var submenu=$.trim($("#txtSubmenu").val());
	if(submenu.length==0){
		return false
		}
	campo0=$("#txtSubmenu").val();
	$.getJSON('formularioBuscar.php',{v0:campo0},function(datos){
		if(datos==0){
			alert("Lo lamento, no encontre el registro!");
			$("#txtSubmenu").val('');
			return false
		}
		$.each(datos, function(i,f){
			$("#txtIdformulario").val(f.idformulario);
			$("#txtModulo").val(f.idmodulo);
			//$("#txtOrden").val(f.orden);
			$("#txtFormulario").val(f.formulario);
			$("#txtUrl").val(f.url);
			$("#txtDescripcion").val(f.descripcion);
			$("#txtSubmenu").val(f.submenu);
			$("#txtDestino").val(f.destino);
			$("#txtMenu").val(f.idmenu);
			$("#idr").val(f.idformulario);//campo oculto en formularios.php
			$("#btnActualizar").show();
			$("#bGuardar").hide();
			return;
		});
	});
}

function actualizarDatos(){

	var campo0=document.forms[0].idr.value;
	var campo1=document.forms[0].elements[1].value;
	var campo2=document.forms[0].elements[2].value;
	var campo3=document.forms[0].elements[3].value;
	var campo4=document.forms[0].elements[4].value;
	var campo5=document.forms[0].elements[5].value;
	var campo6=document.forms[0].elements[6].value;
	var campo7=document.forms[0].elements[7].value;
	
	$.ajax({
		url: 'formularioA.php',
		cache: false,
		type: "POST",
		data: "submit=&v0="+campo0+"&v1="+campo1+"&v2="+campo2+"&v3="+campo3+"&v4="+campo4+"&v5="+campo5+"&v6="+campo6+"&v7="+campo7,
		success: function(datos){
			alert(datos);
			limpiarCampos();
			
		}
	});
	return false;
}

/*function validarCampos(op){
	if(op==1){
		if(nuevo==0){
			alert("Haga click primero en nuevo!");
			return false;
		}
		var campo1=$("#txtModulo").val();
		var campo2=$("#txtOrden").val();
		var campo3=$("#txtFormulario").val();
		var campo4=$("#txtUrl").val();
		var campo5=$("#txtDescripcion").val();
		var campo6=$("#txtSubmenu").val();
		var campo7=$("#txtDestino").val();
		var campo8=$("#txtMenu").val();
		$.getJSON('formularioGuardar.php', {v0:1,v1:campo1,v2:campo2,v3:campo3,v4:campo4,v5:campo5,v6:campo6,v7:campo7,v8:campo8}, function(datos){
			if(datos>0){
				alert("El registro se grab\u00F3 con Exito!");
				nuevo=0;
				$("#txtId").val('');
				$("#txtModulo").val(0);
				$("#txtModulo").focus(0);
				$("#txtOrden").val('');
				$("#txtFormulario").val('');
				$("#txtUrl").val('');
				$("#txtDescripcion").val('');
				$("#txtSubmenu").val('');
				$("#txtDestino").val('');
				$("#txtMenu").val(0);
			}
			else{
				alert("El registro NO se grab\u00F3 por: "+datos);
			}
		});
	}
	else{
		if(confirm("Esta seguro de modificar el registro?")==true){
			var campo0=$("#txtId").val();
			var campo1=$("#txtModulo").val();
			var campo2=$("#txtOrden").val();
			var campo3=$("#txtFormulario").val();
			var campo4=$("#txtUrl").val();
			var campo5=$("#txtDescripcion").val();
			var campo6=$("#txtSubmenu").val();
			var campo7=$("#txtDestino").val();
			var campo8=$("#txtMenu").val();
			$.getJSON('formularioGuardar.php',{v0:2,v1:campo0,v2:campo1,v3:campo2,v4:campo3,v5:campo4,v6:campo5,v7:campo6,v8:campo7,v9:campo8}, function(datos){
				if(datos>0){
					alert("El registro se grab\u00F3 con Exito!");
					nuevo=0;
					$("#txtId").val('');
					$("#txtModulo").val(0);
					$("#txtModulo").focus(0);
					$("#txtOrden").val('');
					$("#txtFormulario").val('');
					$("#txtUrl").val('');
					$("#txtDescripcion").val('');
					$("#txtSubmenu").val('');
					$("#txtDestino").val('');
					$("#txtMenu").val(0);
				}
				else{
				alert("El registro NO se grab\u00F3 por: "+datos);
				}
				});
			}
	}
}*/
/*function mostrar(op){
	$.getJSON('formularioNavegar.php',{v0:op},function(datos){
		if(datos!=0){
			$.each(datos, function(i,f){
				$("#txtId").val(f.idformulario);
				$("#txtModulo").val(f.idmodulo);
				$("#txtOrden").val(f.orden);
				$("#txtFormulario").val(f.formulario);
				$("#txtUrl").val(f.url);
				$("#txtDescripcion").val(f.descripcion);
				$("#txtSubmenu").val(f.submenu);
				$("#txtDestino").val(f.destino);
				$("#txtMenu").val(f.idmenu);
				return;
				});
			}
		});
	}
	
function eliminarDato(){
	alert("No implementado!");
}*/
	
/*function imprimir(){
	var url=URL+'centroReportes/seguridad/reporte006.php';
	window.open(url,'_blank');
	}*/	