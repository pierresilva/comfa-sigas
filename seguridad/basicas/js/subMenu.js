var nuevo=0;
var modifica=0;
var URL=src();

$(function(){
	$( "#txtSubMenu" ).autocomplete({
	    source: function( request, response ) {
	    	var submenus=$("#txtSubMenu").val();
	        $.ajax({
	            url: "submenusAut.php",
	            data: {v0:submenus},
	            dataType: "json",
	            success: function( data ) {
	                response( data );
	            }
	        });
	     },
	    select: function( event, ui ) {
	    	$("#txtId").val(ui.item.id);
	    }
	});
});
/*function nuevoR(){
	$("#txtId").val('');
	$("#txtModulo").val(0);
	$("#txtMenu").val(0);
	$("#txtSubMenu").val('');
	$("#txtOrden").val(0);
	$("#txtModulo").focus('');
	nuevo=1;
	$("#bGuardar").show();
}*/

function nuevoR(){
	nuevo=1;
	$("#bGuardar").show();
	$("#btnActualizar").hide();
	limpiarCampos();
	document.forms[0].elements[1].focus();
	document.forms[0].elements[4].value = fechaHoy();
	document.forms[0].elements[5].value= document.forms[0].elements['usuario'].value;
	}

function validarCampos(op){
	
	if(nuevo==0){
	alert("Click en NUEVO !");
	return false;
	}
	
	if($("#txtModulo").val()==0){
		alert("Falta Seleccionar Modulo!")
		$("#txtModulo").focus();
		return false;
	}
	if($("#txtMenu").val()==0){
		alert("Falta Menu!")
		$("#txtMenu").focus();
		return false;
	}
	if($("#txtSubMenu").val()==''){
		alert("Falta Escribir el Submenu!")
		$("#txtSubMenu").focus();
		return false;
	}	
	if(op==1){
		guardarDatos();
	}	
	else{
		actualizarDatos();
	}
}

function guardarDatos(){
	$("#bGuardar").hide();

	var campo1=$("#txtMenu").val();
	var campo2=$("#txtSubMenu").val();
	var campo3=$("#tFecha").val();
	var campo4=$("#tusuario").val();
	$.ajax({
		url: 'subMenuGuardar.php',
		type: "POST",
		data: "submit=&v1="+campo1+"&v2="+campo2+"&v3="+campo3+"&v4="+campo4,
		success: function(datos){
			$("#bGuardar").show();
			alert(datos);
			limpiarCampos();
		}
	});
	nuevo=0;
}

function consultaDatos(){
	var submenu=$.trim($("#txtSubMenu").val());
	if(submenu.length==0){
		return false
	}
	campo0=$("#txtSubMenu").val();
	$.getJSON('submenuBuscar.php',{v0:campo0},function(datos){
		if(datos==0){
			alert("Lo lamento, no encontre el registro!");
			return false
		}
		$.each(datos, function(i,f){
			$("#txtId").val(f.idsubmenu);
			$("#txtMenu").val(f.idmenu);
			$("#txtSubMenu").val(f.submenu);
			$("#tFecha").val(f.fechasistema);
			$("#tusuario").val(f.usuario);
			$("#idr").val(f.idsubmenu);//campo oculto en submenu.php
			$("#btnActualizar").show();
			$("#bGuardar").hide();
			return;
		});
		});
	}

function actualizarDatos(){
	var campo0=document.forms[0].idr.value;
	var campo1=document.forms[0].elements[1].value;
	var campo2=document.forms[0].elements[2].value;
	var campo3=document.forms[0].elements[3].value;
	var campo4=document.forms[0].elements[4].value;
	var campo5=document.forms[0].elements[5].value;
	
	$.ajax({
		url: 'submenuA.php',
		cache: false,
		type: "POST",
		data: "submit=&v0="+campo0+"&v1="+campo1+"&v2="+campo2+"&v3="+campo3+"&v4="+campo4+"&v5="+campo5,
		success: function(datos){
			alert(datos);
			limpiarCampos();
			
		}
	});
	return false;
}
	
/*function imprimir(){
	var url=URL+'centroReportes/seguridad/reporte005.php';
	window.open(url,'_blank');
	}*/	