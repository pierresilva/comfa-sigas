var nuevo=0;
var modifica=0;
var URL=src();

$(function(){
	$( "#txtProceso" ).autocomplete({
	    source: function( request, response ) {
	    	var nomproceso=$("#txtProceso").val();
	        $.ajax({
	            url: "procesosAut.php",
	            data: {v0:nomproceso},
	            dataType: "json",
	            success: function( data ) {
	                response( data );
	            }
	        });
	     },
	    select: function( event, ui ) {
	    	$("#txtId").val(ui.item.id);
	    }
	});
});

/*function nuevoR(){
	$("#txtId").val('');
	$("#txtProceso").val('');
	$("#txtProceso").focus();
	nuevo=1;
	$("#bGuardar").show();
}*/


function nuevoR(){
	nuevo=1;
	$("#bGuardar").show();
	$("#btnActualizar").hide();
	$("#bGuardar").show();
	limpiarCampos();
	document.forms[0].elements[1].focus();
	document.forms[0].elements[2].value = fechaHoy();
	document.forms[0].elements[3].value= document.forms[0].elements['usuario'].value;
	}

function validarCampos(op){
	$("#bGuardar").hide();
	var agen=$.trim($("#txtProceso").val());
	if(agen.length==0){
		alert("Falta agencia!")
		$("#txtProceso").focus();
		return false;
	}
	if(op==1){
		if(nuevo==0){
			alert("Haga click primero en nuevo!");
			return false;
		}
		var campo0=$("#txtProceso").val();
		var campo1=$("#tFecha").val();
		var campo2=$("#tusuario").val();
		$.getJSON('procesoGuardar.php', {v0:1,v1:campo0,v2:campo1,v3:campo2}, function(datos){
			if(datos>0){
				alert("El registro se grabó con Exito!");
				nuevo=0;
				limpiarCampos();
				$("#txtProceso").val('');
				}
			else{
				alert("El registro NO se grabó por: "+datos);
			}
		});
	}
	else{
		if(confirm("Esta seguro de modificar el registro?")==true){
			var campo1=$("#txtId").val();
			var campo2=$("#txtProceso").val();
			var campo3=$("#tFecha").val();
			var campo4=$("#tusuario").val();
			$.getJSON('procesoGuardar.php',{v0:2,v1:campo1,v2:campo2,v3:campo3,v4:campo4}, function(datos){
				if(datos>0){
				alert("El registro se grabó con Exito!");
				nuevo=0;
				limpiarCampos();
				$("#txtProceso").val('');
				}
				else{
				alert("El registro NO se grabó por: "+datos);
				}
				});
			}
	}
	
}
/*function mostrar(op){
	$.getJSON('procesoNavegar.php',{v0:op},function(datos){
		if(datos!=0){
			$.each(datos, function(i,f){
				$("#txtId").val(f.idproceso);
				$("#txtProceso").val(f.proceso);
				return;
				});
			}
		});
	}
	
function eliminarDato(){
	alert("No implementado!");
}*/

function consultaDatos(){
	var ag=$.trim($("#txtProceso").val());
	if(ag.length==0){
		return false
		}
	campo0=$("#txtProceso").val();
	$.getJSON('procesoBuscar.php',{v0:campo0},function(datos){
		if(datos==0){
			alert("Lo lamento, no encontre el registro!");
			$("#txtProceso").val('');
			return false
		}
		$.each(datos, function(i,f){
			$("#txtId").val(f.idproceso);
			$("#txtProceso").val(f.proceso);
			$("#tFecha").val(f.fechasistema);
			$("#tusuario").val(f.usuario);
			$("#btnActualizar").show();
			$("#bGuardar").hide();
			return;
		});
		});
	}
	
/*function imprimir(){
	var url=URL+'centroReportes/seguridad/reporte002.php';
	window.open(url,'_blank');
	}*/