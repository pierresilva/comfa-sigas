<?php
//$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
//include_once  $root;

date_default_timezone_set("America/Bogota");
error_reporting( E_ALL & ~E_WARNING & ~E_NOTICE ); 

function f_convertir($fecha){
	$f=explode("/",$fecha);
	$unixtime = mktime(0,0,0,$f[0],$f[1],$f[2]);
	return  $unixtime;
}

function f_reconvertir($timeunix){
	$fecha=date("m/d/Y",$timeunix);
	return $fecha;	
}

function  f_dif_dias($tunix1,$tunix2){
	$dias=($tunix1-$tunix2)/(60*60*24);
	return $dias;	
}

function  f_sumar_dias($timeunix,$dias){
	$nuevafecha = $timeunix + ($dias * 24 * 60 * 60);
	return $nuevafecha;	
}

function fx(){
	if (preg_match("/[0-9]{1,2}\/[0-9]{1,2}\/([0-9][0-9]){1,2}/",$fecha))
       list($dia,$mes,$año)=split("/", $fecha);
    if (preg_match("/[0-9]{1,2}-[0-9]{1,2}-([0-9][0-9]){1,2}/",$fecha))
       list($dia,$mes,$año)=split("-",$fecha);
}

function f_hora(){
	return time();
}
function f_host(){
	 return $_SERVER['REMOTE_ADDR'];
}
function f_versession(){
	session_start();
	$U=array();
	$U=$_SESSION['USERS'];
	$U=str_replace("[","",$U);
	$U=str_replace("]","",$U);
	$U=str_replace('"',"",$U);
	$U=explode(",",$U);
	$usuario=$U[1];
	chdir("/var/www/html/new_seguridad/");
	require_once('../cnn/conexionNS.php');
	$link2=conectar();
	$sql="Select Inicia from Session where Usuario='$usuario'";
	if(!$result=mssql_query($sql,$link2)){
		return false;
	}
	$hinicio=mssql_result($result,0,"Inicia");
	$ahora=time();
	if(($ahora-$hinicio)>60*15){
		return true;
	}
	else
	 {
		$sql="Update Session set Inicia='$ahora' where Usuario='$usuario'";
		if(!$result=mssql_query($sql,$link2)){
		return false;
	}
	return false;
	 }
 }
 
function f_menu($a,$m){
      $h = '<img src="../imagenes/spacer.gif" width="1" height="1">
	  <img src="../imagenes/spacer.gif" width="1" height="1">
	  <img src="../imagenes/iconos/nuevo.png" title="Nuevo" width="16" height="16" style="cursor:pointer" onClick="nuevo()">
	  <img src="../imagenes/spacer.gif" width="1" height="1">';
	  if($a=='S'){
	  	$h.='<img src="../imagenes/iconos/grabar.png" title="Guardar" width="16" height="16" onClick="vCampos(1)" style="cursor:pointer">';}
	  $h.='<img src="../imagenes/spacer.gif" width="1" height="1">';
	  if($m=='S')
	  	$h.='<img src="../imagenes/iconos/modificar.png" title="Actualizar" width="16" height="16" onClick="vCampos(2)" style="cursor:pointer">';
	  $h.='<img src="../imagenes/spacer.gif" width="1" height="1">
	  <img src="../imagenes/iconos/Borrar.png" width="16" height="16" border="0" title="Eliminar registro" style="cursor:pointer" onClick="eliminar()" >
	  <img src="../imagenes/spacer.gif" width="1" height="1">
	  <img src="../imagenes/iconos/imprimir.png" width="16" height="16" border="0" title="Imprimir" style="cursor:pointer" onClick="imprimir()" >
	  <img src="../imagenes/spacer.gif" width="1" height="1">
	  <img src="../imagenes/iconos/refrescar.png" width="16" height="16" style="cursor:pointer" title="Limpiar campos" onClick="actualizar()">
	  <img src="../imagenes/spacer.gif" width="1" height="1">
      <img src="../imagenes/iconos/buscar.png" title="Buscar" width="16" height="16" style="cursor:pointer" onClick="vParametro()">
	  <img src="../imagenes/spacer.gif" width="1" height="1">
	  <img src="../imagenes/iconos/primero.png" width="16" height="16" style="cursor:pointer" title="Primero" onClick="navegar(10)">
	  <img src="../imagenes/iconos/siguiente.png" width="16" height="16" style="cursor:pointer" title="Siguiente" onClick="navegar(11)">
	  <img src="../imagenes/iconos/anterior.png" width="16" height="16" style="cursor:pointer" title="Anterior" onClick="navegar(12)">
	  <img src="../imagenes/iconos/ultimo.png" width="16" height="16" style="cursor:pointer" title="Ultimo" onClick="navegar(13)">';
	  return $h;	  
} 

?>
