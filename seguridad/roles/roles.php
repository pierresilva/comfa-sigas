<?php 
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
include_once $raiz.DIRECTORY_SEPARATOR ."config.php";
$url=$_SERVER['PHP_SELF'];
include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'auditoria.php';
//include_once 'phpComunes/auditoria.php';
auditar($url);

$_SESSION['REGISTRO']=0;
?>
<HTML xmlns="http://www.w3.org/1999/xhtml"><HEAD><TITLE>Roles</TITLE>
<META http-equiv=Content-Type content="text/html; charset=iso-8859-1">
<meta http-equiv="Content-Script-Type" content="text/javascript; charset=iso-8859-1">
<META content="MSHTML 6.00.2900.2180" name=GENERATOR>
<link href="../../css/Estilos.css" rel="stylesheet" type="text/css" />
<link type="text/css" href="../../css/formularios/base/ui.all.css" rel="stylesheet" />
<link href="../../css/marco.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../../js/ui/jquery-1.4.2.js"></script>
<script type="text/javascript" src="../../js/formularios/ui/ui.core.js"></script>
<script type="text/javascript" src="../../js/formularios/ui/ui.dialog.js"></script>
<script language="javascript" src="../../js/formularios/ui/ui.datepicker.js"></script>
<script language="javascript" src="../../js/comunes.js"></script>
<script language="javascript" src="js/roles.js"></script>

<script type="text/javascript">
shortcut.add("Shift+F",function() {
		                                var URL=src();
		var url=URL+"aportes/trabajadores/consultaTrabajador.php";
    	window.open(url,"_blank");
    },{
	'propagate' : true,
	'target' : document 
        });        
</script>


</HEAD>
<body >
<FORM name="forma" >
<center>
<br><br>
<table width="60%" border="0" cellspacing="0" cellpadding="0">
  <tr>
   <td width="13" height="29" class="arriba_iz">&nbsp;</td>
    <td class="arriba_ce"><span class="letrablanca">::&nbsp;Administracion - Roles &nbsp;::</span></td>
    <td width="13" class="arriba_de" align="right">&nbsp;</td>
  </tr>
  <tr>
   <td class="cuerpo_iz">&nbsp;</td>
    <td class="cuerpo_ce"><img src="../../imagenes/spacer.gif" width="1" height="1">
	  <img src="../../imagenes/spacer.gif" width="1" height="1">
	  <img src="../../imagenes/menu/nuevo.png" title="Nuevo" width="16" height="16" style="cursor:pointer" onClick="nuevoR();" >
	  <img src="../../imagenes/spacer.gif" width="1" height="1">
	  <img src="../../imagenes/menu/grabar.png" title="Guardar" width="16" height="16" style="cursor:pointer" onClick="validar();" id= "bGuardar">
	  <img src="../../imagenes/spacer.gif" width="1" height="1">
	  <img src="../../imagenes/menu/modificar.png" title="Actualizar" width="16" height="16" style="cursor:pointer" onClick="actualizar();">
	  <img src="../../imagenes/spacer.gif" width="1" height="1"><img src="../../imagenes/spacer.gif" width="1" height="1">
	  <a target="_new" href="<?php echo $ruta_reportes;?>seguridad/reporte011.jsp"> <img src="../../imagenes/menu/imprimir.png" width="16" height="16" style="cursor:pointer" title="Imprimir" ></a>
	  <img src="../../imagenes/spacer.gif" width="1" height="1">
	  <img src="../../imagenes/menu/refrescar.png" width="16" height="16" style="cursor:pointer" title="Limpiar campos" onClick="limpiarCampos();$('#txtRol').focus();modifica=1;nuevo=0;" >
	  <img src="../../imagenes/spacer.gif" width="1" height="1">
      <img src="../../imagenes/menu/buscar.png" title="Buscar" width="16" height="16" style="cursor:pointer" onClick="consultar();" >
      <img src="../../imagenes/spacer.gif" width="1" height="1">
      <a href="../../help/seguridad/manual ayuda roles.html" target="_blank" onClick="window.open(this.href, this.target, 'width=500,height=550,titlebar=0, resizable=no, scrollbars=yes'); return false;" >
      <img src="../../imagenes/menu/informacion.png" width="16" height="16" style="cursor:pointer" title="ayuda" border="0" ></a>
	  <img src="../../imagenes/spacer.gif" width="1" height="1">
	  <img src="../../imagenes/menu/primero.png" width="16" height="16" style="cursor:pointer" title="Primero" onClick="navegarRegistro(1);" >
	  <img src="../../imagenes/menu/siguiente.png" width="16" height="16" style="cursor:pointer" title="Siguiente" onClick="navegarRegistro(2);" >
	  <img src="../../imagenes/menu/anterior.png" width="16" height="16" style="cursor:pointer" title="Anterior" onClick="navegarRegistro(3);" >
	  <img src="../../imagenes/menu/ultimo.png" width="16" height="16" style="cursor:pointer" title="Ultimo" onClick="navegarRegistro(4);" ></td>
   <td class="cuerpo_de">&nbsp;</td>
  </tr>
  <tr>
   <td class="cuerpo_iz">&nbsp;</td>
   <td class="cuerpo_ce"><div id="resultado" style="color:#FF0000"></div></td>
   <td class="cuerpo_de">&nbsp;</td>
  </tr>
  <tr>
    <td class="cuerpo_iz">&nbsp;</td>
    <td class="cuerpo_ce">
	<table width="95%" border="0" cellspacing="0" class="tablero">
      <tr>
        <td width="25%">Id Rol</td>
        <td><input name="txtIdRol" class=boxfecha id="txtIdRol" readonly >
        <img width="12" height="12" src="../../imagenes/menu/obligado.png"></td>
        </tr>
      <tr>
      <td>Rol</td>
        <td><label>
        <input name="txtRol" type="text" class="boxmediano" id="txtRol" >
        <img width="12" height="12" src="../../imagenes/menu/obligado.png"></label></td>
        </tr>
      <tr>
      <td>Estado</td>
        <td><select name="txtEstado" id="txtEstado" class="box1">
          <option value="A">Activo</option>
          <option value="I">Inactivo</option>
        </select><img width="12" height="12" src="../../imagenes/menu/obligado.png"></td>
        </tr>
	  <tr>
	    <td>INSERT (Registro nuevo)</td>
	    <td><select name="txtInsert" id="txtInsert"  class="box1">
		<option value="S" selected>SI</option>
		<option value="N">NO</option>
		</select><img width="12" height="12" src="../../imagenes/menu/obligado.png"></td>
	    </tr>
	  <tr>
	    <td>UPDATE (Actualiza)</td>
	    <td>
		<select name="txtUpdate" class="box1" id="txtUpdate" >
		<option value="S" selected>SI</option>
		<option value="N">NO</option>
        </select><img width="12" height="12" src="../../imagenes/menu/obligado.png"></td>
	  </tr>
	  <tr>
	 <td>Restricciones</td>
        <td><label>
          <select name="txtRestricciones" class="box1" id="txtRestricciones">
		<option value="S">SI</option>
		<option value="N" selected>NO</option>
          </select><img width="12" height="12" src="../../imagenes/menu/obligado.png"></label></td>
		</tr>	
       </table>
      <div id="div_restricciones" style="display:none">
      <table width="95%" border="0" cellspacing="0" class="tablero">
      <tr>
        <td>Dias Laborables</td>
        <td><label></label><label>
          <select name="txtDias" class="box1" id="txtDias">
            <option value=0 selected>Seleccione</option>

          </select><img width="12" height="12" src="../../imagenes/menu/obligado.png"></label></td>
        </tr>
      <tr>
        <td>Jornada Uno</td>
        <td><input type="text" name="txtFechaInicio" id="txtFechaInicio" class="boxfecha" readonly/><img width="12" height="12" src="../../imagenes/menu/obligado.png"></td>
        </tr>
      <tr>
        <td height="23">Jornada Dos </td>
        <td><input type="text" name="txtFechaFin" id="txtFechaFin" class="boxfecha" readonly /><img width="12" height="12" src="../../imagenes/menu/obligado.png"></td>
      </tr>
    </table></div></td>
    <td class="cuerpo_de">&nbsp;</td>
  </tr>
  <tr>
    <td class="abajo_iz" >&nbsp;</td>
    <td class="abajo_ce" ></td>
    <td class="abajo_de" >&nbsp;</td>
  </tr>
</table>
</center>
<input type="hidden" name="pageNum" value="0">
<input type="hidden" name="txtIdr" value="0">
<input type="hidden" name="txtIdUsuario" id="txtIdUsuario" value="">

</FORM>
</BODY>
<script>
navegarRegistro(1);
</script>
</HTML>
