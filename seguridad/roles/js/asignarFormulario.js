// JavaScript Document
var modifica=0;
var nuevo=0;
var idrol=0;
var URL=src();

function nuevoR(){
	nuevo=1;
	$("#bGuardar").show();
	limpiarCampos();
	$("#txtRol").focus();
	$("#txtModulo").val(1);
	$(".ui-state-error").removeClass("ui-state-error");
	}
	
function validar(){
	$("#bGuardar").hide();
	error=0;
	$(".ui-state-error").removeClass("ui-state-error");//Limpiar la clase de error
	if(nuevo==0){
		alert("Haga click primero en Nuevo");
		return false;
    }
	var idrol=$("#txtRol").val();
	var idmodulo=$("#txtModulo").val();
	var idmenu=$("#cboMenu").val();
	var idsubmenu=$("#cboSubmenu").val();
	var idformulario=$("#cboFormulario").val();
	
	if(idrol==0){
   		$("#txtRol").addClass("ui-state-error");
   		error++;
   	}
	if(idmodulo==0){
   		$("#txtModulo").addClass("ui-state-error");
   		error++;
   	}
	if(idformulario==0){
   		$("#txtFormulario").addClass("ui-state-error");
   		error++;
   	}
	if(idmenu==0){
		$("#cboMenu").addClass("ui-state-error");
   		error++;
		}
	if(idsubmenu==0){
		$("#idsubmenu").addClass("ui-state-error");
   		error++;
		}
	if(error>0){return false; }
	$.getJSON("aFormularioGuardar.php",{v0:idrol,v1:idmodulo,v2:idmenu,v3:idsubmenu,v4:idformulario,v99:1},function(datos){
		if(datos==1){
			MENSAJE("El registro se guardo!");
			limpiarCampos(); 
		}
		else{
			MENSAJE(datos);
			return false;
			}
		});
}	
	
function actualizar(){
	error=0;
	$(".ui-state-error").removeClass("ui-state-error");//Limpiar la clase de error
	if(nuevo==0){
		alert("Haga click primero en Nuevo");
		return false;
    }
	var idrol=$("#txtIdRol").val();
	var modulo=$("#txModulo").val();
	var formulario=$("#txtFormulario").val();
	
	if(rol==0){
   		$("#txtRol").addClass("ui-state-error");
   		error++;
   	}
	if(modulo==0){
   		$("#txtRol").addClass("ui-state-error");
   		error++;
   	}
	if(formulario==0){
   		$("#txtRol").addClass("ui-state-error");
   		error++;
   	}
	if(error>0){return false; }
	$.getJSON('aFormularioGuardar.php',{v0:idrol,v1:modulo,v2:formulario,v99:2},function(datos){
		if(datos==0){
			MENSAJE("Lo lamento, su registro no fue actualizado!");
			return false;
			}
			MENSAJE("El registro se actualizo!");
			limpiarCampos(); 
		})
}

function consultar(){
	if($.trim($("#txtRol").val())==''){
		MENSAJE("Digite el Rol a consultar!");
		$("#txtRol").focus();
		return false;
		}
		var rol=$("#txtRol").val();
	$.getJSON('rolesConsulta.php',{v0:rol},function(datos){
		if(datos==0){
			MENSAJE("Lo lamento, no encontre el registro!");
			return false;
			}
		$.each(datos,function(i,f){
			$("#txtIdRol").val(f.IDROL);
			$("#txtRol").val(f.ROL);
			$("#txtEstado").val(f.ESTADO);
			$("#txtInsert").val(f.ADICIONA);
			$("#txtUpdate").val(f.MODIFICA);
			$("#txtRestricciones").val(f.ESTRICCION);
			});
		})
	}
	
//Controla las opciones de navegacion de registros
function navegarRegistro(op){
	$.getJSON('aFormularioNavegar.php',{v0:op},function(datos){
		if(datos!=0){
			$.each(datos, function(i,f){
				$("#txtIdRol").val(f.IDROL);
				$("#txtRol").val(f.ROL);
				$("#txtEstado").val(f.ESTADO);
				$("#txtInsert").val(f.ADICIONA);
				$("#txtUpdate").val(f.MODIFICA);
				$("#txtRestricciones").val(f.ESTRICCION);
				return;
			});
		}
	});
}
