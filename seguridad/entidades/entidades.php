<?php
$root = $_SERVER ['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';

?>
<HTML xmlns="http://www.w3.org/1999/xhtml">
<HEAD>
<TITLE>::Entidades::</TITLE>
<link href="<?php echo URL_PORTAL; ?>css/sorterStyle.css" rel="stylesheet" type="text/css" media="screen" />
<META http-equiv=Content-Type content="text/html; charset=iso-8859-1">
<meta http-equiv="Content-Script-Type"
	content="text/javascript; charset=iso-8859-1">
<META content="MSHTML 6.00.2900.2180" name=GENERATOR>
<link type="text/css"
	href="../../css/formularios/custom-theme/jquery-ui.css"
	rel="stylesheet" />
<link type="text/css" href="../../css/Estilos.css" rel="stylesheet" />
<link type="text/css" href="../../css/formularios/base/ui.all.css"
	rel="stylesheet" />
<link href="../../css/marco.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../../js/ui/jquery-1.4.2.js"></script>
<script type="text/javascript" src="../../js/formularios/ui/ui.core.js"></script>
<script type="text/javascript"
	src="../../js/formularios/ui/ui.button.js"></script>
<script type="text/javascript"
	src="../../js/formularios/ui/ui.dialog.js"></script>
<script type="text/javascript"
	src="../../js/formularios/ui/ui.datepicker.js"></script>
<script type="text/javascript" src="../../js/effects.Jquery.js"></script>

<script language="javascript" src="../../js/comunes.js"></script>
<script language="javascript" src="../js/combos.js"></script>
<script language="javascript" src="js/entidades.js"></script>

<script type="text/javascript">
shortcut.add("Shift+F",function() {
		                                var URL=src();
		var url=URL+"aportes/trabajadores/consultaTrabajador.php";
    	window.open(url,"_blank");
    },{
	'propagate' : true,
	'target' : document 
        });        
</script>

<script type="text/javascript">
	$(document).ready(function() {    
    		$("#paginar").live("click", function(){
		$("#contenido").html("<br/><div align='center'><img src='../../imagenes/ajax-loader-fon.gif'/></div>");
			var pagina=$(this).attr("data");
			paginaG=pagina;

			listarEntidades(paginaG, filas);
    	});
	});  
</script>

<style type="text/css">
#empresas p{ cursor:pointer; color:#333; width:600px;}
#empresas p:hover{color:#000}
#accordion h3,div{ padding:6px;}
#accordion span.plus{ background:url(../../imagenes/plus.png) no-repeat right center; margin-left:95%;}
#accordion span.minus{background:url(../../imagenes/minus.png) no-repeat right center;  margin-left:95%}
div#wrapTable{padding:0px; margin-bottom:-2px}
div#icon{width:50px; margin:auto;margin-first:5px;padding:10px; text-align:center;display:none; cursor:pointer;}
div#icon span{background:url("../../imagenes/show.png") no-repeat; padding:12px;}
div#icon span.toggleIcon{background:url("../../imagenes/hide.png") no-repeat; padding:12px;}

#controls { margin:0 auto; height:20px;}
#perpage {float:left; width:200px; margin-left:40px;}
#perpage select {float:left; font-size:11px}
#perpage label {float:left; margin:2px 0 0 5px; font-size:10px}
#navigation {float:left;/* width:600px;*/ text-align:center}
#navigation img {cursor:pointer}
#text {float:left; width:200px; text-align:left; margin-first:2px; font-size:10px}

#periodo p {
	text-align: center;
	font-style: italic;
	color: #CCC;
}


</style>

</HEAD>
<body>
	<FORM name="forma">
		<center>
			<br>
			<br>
			<table width="90%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="13" height="29" class="arriba_iz">&nbsp;</td>
					<td class="arriba_ce"><span class="letrablanca">::&nbsp;Administracion
							- Entidades &nbsp;::</span></td>
					<td width="13" class="arriba_de" align="right">&nbsp;</td>
				</tr>
				<tr>
					<td class="cuerpo_iz">&nbsp;</td>
					<td class="cuerpo_ce"><img src="../../imagenes/spacer.gif"
						width="1" height="1"> <img src="../../imagenes/spacer.gif"
						width="1" height="1"> <img src="../../imagenes/menu/nuevo.png"
						title="Nuevo" width="16" height="16" style="cursor: pointer"
						onClick="nuevoR();"> <img src="../../imagenes/spacer.gif"
						width="1" height="1"> <img src="../../imagenes/menu/grabar.png"
						title="Guardar" width="16" height="16" style="cursor: pointer"
						onClick="validar();" id="bGuardar"> <img src="../../imagenes/spacer.gif"
						width="1" height="1"></td>
					<td class="cuerpo_de">&nbsp;</td>
				</tr>
				<tr>
					<td class="cuerpo_iz">&nbsp;</td>
					<td class="cuerpo_ce"><div id="resultado" style="color: #FF0000"></div></td>
					<td class="cuerpo_de">&nbsp;</td>
				</tr>
				<tr>
					<td class="cuerpo_iz">&nbsp;</td>
					<td class="cuerpo_ce">
						<table width="95%" border="0" cellspacing="0" class="tablero">
							<tr>
								<td width="25%">Entidad</td>
								<td><input name="txtEntidad" class=boxfecha id="txtEntidad">
									<img width="12" height="12"
									src="../../imagenes/menu/obligado.png"></td>
							</tr>
							<tr>
								<td>Llave Primaria</td>
								<td><input name="txtPrimaria"class="boxfecha" id="txtPrimaria">
									<img width="12" height="12"
									src="../../imagenes/menu/obligado.png"></td>
							</tr>
							<tr>
								<td>Modulo</td>
								<td>
									<select id="cmbModulo" name="cmbModulo">
										<option value='1'>APORTES Y SUBSIDIO</option>
										<option value='2'>TARJETAS</option>
										<option value='3'>PROMOTORIA</option>
										<option value='4'>FONEDE</option>
										<option value='5'>SEGURIDAD</option>
									</select>
								
									<img width="12" height="12"
									src="../../imagenes/menu/obligado.png"></td>
							</tr>
							<tr>
								<td>Descripcion</td>
								<td><textarea id="txtDescripcion" class="area" name="txtDescripcion"></textarea>
								</td>
							</tr>							
						</table>
					</td>
					<td class="cuerpo_de">&nbsp;</td>
				</tr>
				<tr>
					<td class="abajo_iz">&nbsp;</td>
					<td class="abajo_ce"></td>
					<td class="abajo_de">&nbsp;</td>
				</tr>
			</table>
			<br />
			<div id="contenido"></div>
		</center>
				
	</FORM>

</BODY>
</HTML>
