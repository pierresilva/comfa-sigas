<?php

set_time_limit(0);
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'funcionesComunes'.DIRECTORY_SEPARATOR.'funcionesComunes.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
include_once $raiz .DIRECTORY_SEPARATOR.'seguridad'.DIRECTORY_SEPARATOR.'clases' .DIRECTORY_SEPARATOR. 'Entidad.php';

$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

$objClase = new Entidad();

$consulta_cantidad=$objClase->Count_Entidades();
$resultados_cantidad2=mssql_fetch_array($consulta_cantidad);
$resultados_cantidad=$resultados_cantidad2["cant"];

 if ($resultados_cantidad>0)
 {
    $filas_pagina=1;
    $numero_pagina=1;

    if(isset($_GET["pagina"]))
    {
        sleep(1);
        $numero_pagina=$_GET["pagina"];
    }
    
    if(isset($_GET["rows"]))
    	$filas_pagina=$_GET["rows"];
	
    $total_registros=ceil($resultados_cantidad/$filas_pagina);
    
    if($numero_pagina>$total_registros)
    	$numero_pagina=$total_registros;
    
    $campo_de_inicio=($numero_pagina-1)*$filas_pagina;
    $campo_fin=$campo_de_inicio+$filas_pagina;    
    
    echo "<div align='center'>";
    
    echo "<table width='80%' border='0' cellpadding='0' cellspacing='0' class='sortable' id='tDatos'  >";
    echo "<thead><tr>";    
    echo "<th width='4%' height='20' align='center' > <strong>ID</strong></th>";
    echo "<th width='5%'  align='center' ><strong>Entidad</strong></th>";
    echo "<th width='8%' align='center'><strong>Llave Prim.</strong></th>";
    echo "<th width='10%' align='center'><strong>Modulo</strong></th>";
    echo "<th width='26%' align='center'><strong>Descripcion</strong></th>";
    echo "<th width='5%'><strong>Fecha</strong></th>";
    echo "<th width='5%'><strong>Usuario</strong></th>";    
    echo "</tr></thead>";   
    
    $consulta=$objClase->Select_Entidades($campo_de_inicio, $campo_fin);
    while ($resultados=mssql_fetch_array($consulta))
    {
    	$identidad = $resultados["identidad"];
    	$entidad = $resultados["entidad"];
    	$llaveprimaria = $resultados["llaveprimaria"];
    	$modulo = 'N/A';
    	$descripcion = $resultados["descripcion"];
    	$fechasistema = $resultados["fechasistema"];
    	$usuario = $resultados["usuario"];

    	switch($resultados["modulo"]){
    		case '1': $modulo='APORTES Y SUBSIDIO'; break;
    		case '2': $modulo='TARJETAS'; break;
    		case '3': $modulo='PROMOTORIA'; break;
    		case '4': $modulo='FONEDE'; break;
    		case '5': $modulo='SEGURIDAD'; break;
    	}
    	
    	echo "<tr>";
    	echo "<td>".$identidad."</td>";
    	echo "<td>".$entidad."</td>";
    	echo "<td>".$llaveprimaria."</td>";
    	echo "<td>".$modulo."</td>";
    	echo "<td>".$descripcion."</td>";
    	echo "<td>".$fechasistema."</td>";
    	echo "<td>".$usuario."</td>";
    	echo "</tr>";
    }

    echo "</table>";
    echo "</div>";

    echo "<br/>";
	
    if ($total_registros>1)
    {
    	echo "<div id='controls'>";
    		echo "<div id='perpage'>";
    			echo "<select id='cmbFilas' name='cmbFilas' onChange='cambiarCantRegistros(this.value)'>";
    				echo "<option value='5'>5</option>";
    				echo "<option value='10' selected='selected' >10</option>";
    				echo "<option value='20'>20</option>";
    				echo "<option value='50'>50</option>";
    				echo "<option value='100'>100</option>";
    			echo "</select>";
    			echo "<label>Registros Por P&aacute;gina</label>";
    		echo "</div>";    	
        	echo "<div id='navigation'>";
        		echo "<a id='paginar' data='".(1)."'><img src='../../imagenes/imagesSorter/first.gif' width='16' height='16' alt='Primera Pagina' /></a>";
        		echo "<a id='paginar' data='".($numero_pagina-1)."'><img src='../../imagenes/imagesSorter/previous.gif' width='16' height='16' alt='Pagina Anterior' /></a>";
        		echo "<a id='paginar' data='".($numero_pagina+1)."'><img src='../../imagenes/imagesSorter/next.gif' width='16' height='16' alt='Pagina Siguiente' /></a>";
        		echo "<a id='paginar' data='".($total_registros)."'><img src='../../imagenes/imagesSorter/last.gif' width='16' height='16' alt='Ultima Pagina' /></a>";       
		
        	echo "</div>";
        	echo "<div id='text'>";
        	echo "P&aacute;gina ".$numero_pagina." de ".$total_registros;
        	echo "</div>";
        echo "</div>";
        
    }
}

?>