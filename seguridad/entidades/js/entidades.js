var nuevo=0;
paginaG=1;
filas=10;

$(document).ready(function(){
	nuevo=1;
	listarEntidades(paginaG, filas);
});

function limpiarCampos(){
	$("#txtEntidad").val('');
	$("#txtPrimaria").val('');
	$("#txtModulo").val('');
	$("#txtDescripcion").val('');
}

function nuevoR(){
	nuevo=1;
	$("#bGuardar").show();
	limpiarCampos();	
	$("#txtEntidad").focus();
}

function validar(){
	if(nuevo==0){
		alert("Haga click primero en Nuevo!");
		return false;
	}
	
	var entidad = $("#txtEntidad").val();
	var primaria = $("#txtPrimaria").val();
	var modulo = $("#cmbModulo").val();	
	var descripcion = $("#txtDescripcion").val();
	
	if(entidad.length==0){
		alert("Digite nombre de la entidad!");
		return false;
	}
	
	if(entidad.slice(0,7).toUpperCase()!="APORTES"){
		alert("La entidad debe empezar por aportes!");
		return false;
	}
	
	if(primaria.length==0){
		alert("Digite el nombre de la llave primaria!");
		return false;
	}	
	
	$.ajax({
		url: 'entidadGuardar.php',
		type: 'POST',
		async: false,
		dataType: "json",
		data: {v0:entidad,v1:primaria,v2:modulo,v3:descripcion},
		success: function(datos){
			if(parseInt(datos)>0){
				alert("Entidad fue almacenada correctamente. \nCodigo Entidad="+datos);
				listarEntidades(paginaG, filas);
				nuevo=0;
			} else {
				alert("Entidad no fue guardada");
			}
		}
	});
		
}

function listarEntidades(pag, rows){
	var cadena="pagina="+pag+"&rows="+rows;	
	
	$.ajax({
		type:"GET",
		url:"listar_entidades.php",
		data:cadena,
		success:function(data)
		{
			$("#contenido").fadeIn(1000).html(data);
			$("table.tablaR tr:even").addClass("zebra");
			$("#cmbFilas").val(rows);
		}
	});
}

function cambiarCantRegistros(cant){
	filas=cant;
	
	listarEntidades(paginaG, filas);
}