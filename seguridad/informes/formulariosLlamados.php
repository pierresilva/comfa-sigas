<?php

ini_set("display_errors",'1');
date_default_timezone_set('America/Bogota'); 
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}
$fecha = date("m/d/Y");
//$sql="SELECT * FROM aportes504 WHERE TO_CHAR(fecha, '%m/%d/%Y')='$fecha' order by usuario";
$sql="SELECT * FROM aportes504 WHERE fecha ='$fecha' order by usuario";
$result=$db->querySimple($sql);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Documento sin título</title>
<link type="text/css" href="../../css/Estilos.css" rel="stylesheet"/>
<link type="text/css" href="../../css/marco.css" rel="stylesheet"/>
<link type="text/css" href="../../css/sorterStyle.css" rel="stylesheet"/>
<script type="text/javascript" src="../../js/ui/jquery-1.4.2.js"></script>
<script type="text/javascript" src="../../js/jquery.tablesorter.js"></script>
<script type="text/javascript" src="../../js/jquery.tablesorter.pager.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$("#tFormularios tbody tr:odd").addClass("vzebra-odd");
	$("#tFormularios").tablesorter();
	//$("#tFormularios").tablesorterPager({container: $("#pager"),seperator:" de "}).trigger("updateCell");
	//$("#tFormularios").trigger("update");
	});
</script>
</head>

<body>
<center>
<table width="80%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td class="arriba_iz" >&nbsp;</td>
<td class="arriba_ce" ><span class="letrablanca">:: Formularios llamados&nbsp;::</span></td>
<td class="arriba_de" >&nbsp;</td>
</tr>
<tr>
<td class="cuerpo_iz">&nbsp;</td>
<td class="cuerpo_ce">&nbsp;</td>
<td class="cuerpo_de">&nbsp;</td>
<tr>
<tr>
<td class="cuerpo_iz" >&nbsp;</td>
<td class="cuerpo_ce" >

<table cellpadding="0" cellspacing="0"  border="0" class="sortable hover" id="tFormularios" width="90%">
      <caption style="text-align:left; padding:5px; font-size:11px;"><span></span></caption>
      <thead>
      <tr>
        <th class="head"><h3><strong>Usuario</strong></h3></th>
        <th class="head"><h3><strong>Equipo</strong></h3></th>
        <th class="head"><h3><strong>Fecha</strong></h3></th>
        <th class="head"><h3><strong>Formulario</strong></h3></th>
      </tr>
      </thead>
      <tbody>
<?php
while ($row=$result->fetch()){
?>       
     <tr> 
    <td><?php echo $row['usuario']; ?></td>
      <td><?php echo $row['host']; ?></td>
      <td><?php echo $row['fecha']; ?></td>
      <td><?php echo $row['url']; ?></td>
      </tr>
<?php } ?>   
  </tbody>   
  </table>
<div id="pager" class="pager">
   <div id="perpage">
	<select class="pagesize">
			<option selected="selected"  value="50">50</option>
			<option value="100">100</option>
			<option value="200">200</option>
			<option  value="300">300</option>
		</select>
        <label>Registros Por P&aacute;gina</label>
        </div>
        <div id="navigation">
		<img src="../../imagenes/imagesSorter/first.gif" class="first"/>
		<img src="../../imagenes/imagesSorter/previous.gif" class="prev"/>
        <img src="../../imagenes/imagesSorter/next.gif" class="next"/>
		<img src="../../imagenes/imagesSorter/last.gif" class="last"/> 
        <label>P&aacute;gina</label>
		<input type="text" class="pagedisplay boxfecha" readonly="readonly" />
       </div>
	</div>  
</td>
<td class="cuerpo_de" >&nbsp;</td>
<tr>
<tr>
<td class="cuerpo_iz" >&nbsp;</td>
<td class="cuerpo_ce" >&nbsp;</td>
<td class="cuerpo_de" >&nbsp;</td>
<tr>
<td class="abajo_iz" >&nbsp;</td>
<td class="abajo_ce" ></td>
<td class="abajo_de" >&nbsp;</td>
</tr>
</table>
</center>
</body>
</html>

