<?php
/**
 * 
 * @Autor: Oscar Uriel Rodriguez T.
 * 
 */
set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");
$raiz = "";
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$url=$_SERVER['PHP_SELF'];
include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'auditoria.php';
auditar($url);

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit(); 
}
$fecver = date('Ymd h:i:s A',filectime('presupuesto.php'));
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<title>SIGAS | Presupuesto</title>
		<link type="text/css" rel="stylesheet" href="../css/Estilos.css" />
		<link type="text/css" rel="stylesheet" href="../css/marco.css" />
		<link type="text/css" rel="stylesheet" href="../css/css.1.8/jquery-ui-1.8.17.custom.css" />
		<script type="text/javascript" src="../js/1.6/jquery-1.6.2.min.js"></script>
		<script type="text/javascript" src="../js/1.6/jquery-ui-1.8.16.custom.min.js"></script>
		<script type="text/javascript" src="../js/comunes.js"></script>
		<script type="text/javascript" src="presupuesto.js"></script>
		<style >
			.tablero input{
				text-align: right;
			}
			.tablero .centro{text-align: center;}
			.point{cursor:pointer}
			.botton{margin:0 auto;}
		</style>
	</head>
	<body> 
		<form>
			<table width="80%" border="0" cellspacing="0" cellpadding="0" align="center">
				<tr>
					<td class="arriba_iz" >&nbsp;</td>
					<td class="arriba_ce" ><span class="letrablanca">::Desarrollo Finaciero Aportes y Subsidio::</span>
						<div style="text-align:right;float:right;">
							<?php echo 'Versi&oacute;n: ' . $fecver; ?>
						</div>
					</td>
					<td class="arriba_de" >&nbsp;</td>
				</tr>
				<tr>
					<td class="cuerpo_iz">&nbsp;</td>
					<td class="cuerpo_ce">
					</td>
					<td class="cuerpo_de">&nbsp;</td>
				</tr>
				<tr>
					<td class="cuerpo_iz" >&nbsp;</td>
					<td class="cuerpo_ce" >
						<table border="0" class="tablero" cellspacing="0" width="90%" align="center" >
							<tr>
								<td colspan="7" style="text-align:center;" >
									<b>A�o</b><br/>
									<input type="text" name="txtAnno" id="txtAnno" size="6" class="ui-state-default"/>
									<!-- <img src="../imagenes/find.png" title="Buscar" id="btnBuscar" class="point" width="20" height="20"/> -->
								<h3>Primer Semestre</h3></td>
							</tr>
							<tbody id="tBody">
			  				<?php 
			  					$arrayInputId = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
			  					$tr = "<tr><th>Aporte</th>";
			  					echo $tr;
			  					for($i=0;$i<count($arrayInputId);$i++){
			  						
			  						if($i>5)
			  							$tr .= "<th>".$arrayInputId[$i]."</th>";
			  						else 
			  							echo "<th>".$arrayInputId[$i]."</th>";
			  					}
			  					echo "</tr>";
			  					$tr .= "</tr>";
			  					
			  					$arrayAportes = array(2);
			  					//$arrayAportes[0] = array("Empresas","Facultativos","Pensionados","Recuperaci�n de Aportes","Recaudo Aportes de Contratistas");
			  					$arrayAportes = array("Empresa","Facultativo","Pensionado","Total");
			  					$contenido = "";
			  					for($i=0;$i<count($arrayAportes);$i++){
			  						echo  "<tr id='tr".$arrayAportes[$i]."'><th>".$arrayAportes[$i]."</th>";
			  						$tr .= "<tr id='tr".$arrayAportes[$i]."'><th>".$arrayAportes[$i]."</th>";
			  						
			  						for($a=0;$a<count($arrayInputId);$a++){
			  							if($a>5)
			  								$tr .="<td><input type='text' class='box1' id='txt".$arrayInputId[$a]."' /></td>";
			  							else
			  								echo "<td><input type='text' class='box1' id='txt".$arrayInputId[$a]."' /></td>";
			  						}
			  						echo "</tr>";
			  						$tr .="</tr>";
			  					}
			  					
			  				?>
			  				<tr>
								<td colspan="7" style="text-align:center;"><h3>Segundo Semestre</h3></td>
							</tr>
							<?php 
								echo $tr;
							?>
							</tbody>
			  			</table>
			  			<br/>
			  			<table border="0" class="tablero" cellspacing="0" width="25%" align="center">
			  				<tr>
			  					<th colspan="2">Totales</th>
			  				</tr>
			  				<tr id="trEmpresaTotal">
			  					<th>Empresas</th>
			  					<td><input type="text" name="txtTotal" id="txtTotal" size="30" class="ui-state-default" readonly="readonly"/></td>
			  				</tr>
			  				<tr id="trFacultativoTotal">
			  					<th>Facultativo</th>
			  					<td><input type="text" name="txtTotal" id="txtTotal" size="30" class="ui-state-default" readonly="readonly"/></td>
			  				</tr>
			  				<tr id="trPensionadoTotal">
			  					<th>Pensionado</th>
			  					<td><input type="text" name="txtTotal" id="txtTotal" size="30" class="ui-state-default" readonly="readonly"/></td>
			  				</tr>
			  				<tr id="trTotalTotal">
			  					<th>Total</th>
			  					<td><input type="text" name="txtTotal" id="txtTotal" size="30" class="ui-state-default" readonly="readonly"/></td>
			  				</tr>
			  			</table>
			  			<br/>
			 		</td>
					<td class="cuerpo_de" ></td>
				</tr>
				<tr>
					<td class="cuerpo_iz" >&nbsp;</td>
					<td class="cuerpo_ce" align="center" >
				  			<button name="btnGuardar" id="btnGuardar" value="Guardar" type="button" class="btnButton" onclick="guardar();" >Guardar</button>
				  			<!-- <button name="btnActualizar" id="btnActualizar" value="Actualizar" type="button" class="btnButton">Actualizar</button> -->
				  			<button name="btnLimpiar" id="btnLimpiar" value="Limpiar" type="button" class="btnButton">Limpiar</button>
			 			
					</td>
					<td class="cuerpo_de" >&nbsp;</td>
				</tr>
				<tr>
					<td class="abajo_iz" >&nbsp;</td>
					<td class="abajo_ce" ></td>
					<td class="abajo_de" >&nbsp;</td>
				</tr>
			</table>
		</form>
	</body>
</html>