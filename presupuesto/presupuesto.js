$(document).ready(function(){
	$(".btnButton").button({
		icons: {
			primary: "ui-icon-disk"
		}
	}).next().button({
		icons:false
	});
	
	$("#trTotal input").addClass("ui-state-default");
	$("#trTotal input").attr("readonly","readonly");
	$("input").attr("align","right");
	
	$("#txtAnno").datepicker({
		dateFormat:"yy",
		changeYear:true,
		showButtonPanel:true,
		onClose: function(dateText, inst) {
			var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
			$(this).datepicker('setDate', new Date(year, 1, 1));
		} 
	});
	
	$("#txtAnno").focus(function () {
		$(".ui-datepicker-calendar").hide();
		$("#ui-datepicker-div").position({
			my: "center top",
			at: "center bottom",
			of: $(this)
		});
	}); 
	
	$("#txtAnno").blur(function(){
		if($(this).val()!=0 && ( verificarAnno($(this).val().trim()) == false )){
			$(this).val("");
		}			
	});
	
	$("#btnLimpiar").click(function(){
		$("input").val("");
		$("input").removeClass("ui-state-error");
	});
	$("#tBody input").focus(function(){
		$("input").removeClass("ui-widget-content");
		$(this).addClass("ui-widget-content");
	});
	
	$("#tBody input").keyup(function(){
		valorR = formatMoneda($(this).val());
		$(this).val(valorR);
		if(parseInt(valorR)>0)
			$(this).removeClass("ui-state-error");
	});
	
	$("#tBody input").blur(function(){
		$(this).remove("blur");
		var total = 0,valor,valorTotal;
		var totalTotal=0,idTotal,total1;
		$("tr #"+$(this).get(0).id).each(function(){
			total1=0;
			idTotal = $(this).parent().parent().get(0).id;
			valor = formatNormal($(this).val());
			valor = parseInt(valor);
			
			$("#"+idTotal+" input").each(function(){
				valorTotal = formatNormal($(this).val());
				valorTotal = parseInt(valorTotal);
				total1 += isNaN(valorTotal)?0:valorTotal; 
			});
			
			if(idTotal!="trTotal"){
				total += isNaN(valor)?0:valor;
				$("tr #"+idTotal+"Total #txtTotal").val(formatMoneda(total1.toString()));
				totalTotal +=total1;
			}
		});
		total = formatMoneda(total.toString());
		totalTotal = formatMoneda(totalTotal.toString());
		$("#trTotal #"+$(this).get(0).id).val(total);
		$("#trTotalTotal #txtTotal").val(totalTotal);
		$(this).val(formatMoneda($(this).val().toString()));		
	});
	
	$("#btnBuscar").click(function(){
		alert("No se encuentra disponible");
	});
});

function verificarAnno(anno){
	var resultado = "";
	$.ajax({
		url:"presupuesto.log.php",
		type:"POST",
		data:{v0:"CA",v1:anno},
		//dataType:"json",
		async:false,
		success:function(datos){
			if(datos=="OK"){
				return true;
			}		
			resultado = datos;
		}
	});
	if(resultado!=""){
		alert(resultado);
		return false;
	}
}

function formatMoneda(value){
	var num = value.replace(/\./g,'');
	if(!isNaN(num)){
		num = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1.');
		num = num.split('').reverse().join('').replace(/^[\.]/,'');
		return num;
	}
}
	
function formatNormal(input){
	var txt=input.replace(/\./g,'');
	return (txt.length > 0 && !isNaN(txt) &&  txt>0)?parseInt(txt):0;	
}

function guardar(){
	var arrayInputId = new Array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
	var cont = 0; 
	var anno = $("#txtAnno").val().trim();
	
	//validar campos vacios
	$("input").each(function(){	
		if($(this).val().trim()=="" || isNaN(parseInt(formatNormal($(this).val().trim())))){
		    $(this).addClass("ui-state-error");
		    cont++;
		}
		else{
            $(this).removeClass("ui-state-error");
		}
	});
	
	if(cont!=0)
		return false;
	
	var datos = new Array();
	$.each(arrayInputId,function(i,row){
		datos[i] = new Array();
		datos[i][0]= formatNormal($("tr#trEmpresa #txt"+row).val().trim());
		datos[i][1]= formatNormal($("tr#trFacultativo #txt"+row).val().trim());
		datos[i][2]= formatNormal($("tr#trPensionado #txt"+row).val().trim());
	});

	var datosArray=JSON.stringify(datos);
	
	//peticion ajax, para realizar el respectivo proceso 
	$.ajax({
		url:"presupuesto.log.php",
		type:"POST",
		data:{v0:"G",v1:datosArray,v2:anno},
		//dataType:"json",
		async:false,
		success:function(datos){
			if(datos=="OK"){
				alert("El presupuesto del a�o "+anno+" se guardo correctamente");
				$("input").val("");
			}
			else
				alert(datos);
		}
	});
}
